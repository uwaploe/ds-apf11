#ifndef MODEM_H
#define MODEM_H (0x1000U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define modemChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* Dial string for the MLF2 RUDICS gateway */
#define MLF2_RUDICS "ATDT00881600005347"

/* prototypes for external functions */
int connect(const struct SerialPort *port,const char *dialstring,time_t sec);
int hangup(const struct SerialPort *port);
int modem_initialize(const struct SerialPort *port);
int m9500cbst_rudics(const struct SerialPort *port);

#endif /* MODEM_H */
#ifdef MODEM_C
#undef MODEM_C

#include <assert.h>
#include <chat.h>
#include <ctype.h>
#include <limits.h>
#include <logger.h>
#include <string.h>

/* prototypes for local functions */
static int modem_at(const struct SerialPort *port);
static int modem_connect(const struct SerialPort *port,const char *dialstring,time_t sec);
static int modem_hangup(const struct SerialPort *port);
static int modem_restore_factory_configuration(const struct SerialPort *port);
static int m9500cbst(const struct SerialPort *port);


/* prototypes for functions with external linkage */
int snprintf(char *str, size_t size, const char *format, ...);

/* time-out period (seconds) for communication with the modem */
const time_t timeout=5;

/* Motorola's ISU AT Command Reference indicates max command length of 128 bytes */
#define MAXCMDLEN 128

/* prototypes for functions with external linkage */
unsigned int sleep(unsigned int seconds);
int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* function to connect to a host computer                                 */
/*------------------------------------------------------------------------*/
/**
   This function attempts to establish a modem-to-modem connection with the
   remote host computer.  Multiple attempts are made to establish a
   connection up to a maximum number of retries.
    
      \begin{verbatim}
      input:
         port.........A structure that contains pointers to machine
                      dependent primitive IO functions.  See the comment
                      section of the SerialPort structure for details.  The
                      function checks to be sure this pointer is not NULL.
         dialstring...The modem command that generates the tones to connect
                      to the remote computer.  Example: ATDT2065436697.
         sec..........The number of seconds allowed to make the connection
                      before this function abandons the connection attempt.
  
      output:
         This function returns a positive number (equal to the number of
         tries) if the exchange was successful.  A negative number (whose
         absolute value is equal to the number of tries) is returned if a
         connection could not be established.
      \end{verbatim}
*/
int connect(const struct SerialPort *port,const char *dialstring,time_t sec)
{
   /* define the logging signature */
   cc *FuncName = "connect()";
   
   /* initialize the return value */
   int i,status=0;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the dialstring */
   else if (!dialstring) {LogEntry(FuncName,"NULL dial string.\n");}

   /* establish communication with the modem */
   else if (modem_at(port)<=0) {LogEntry(FuncName,"Modem attention failure.\n");}
   
   else
   {
      /* define the number of tries allowed */
      const int NRetries=1;

      /* retry loop */
      for (status=0, i=1; i<=NRetries; i++)
      {
         /* write the dialstring to the modem */
         if (modem_connect(port,dialstring,sec)>0) {status=i; break;} else {status=-i;}
      }

      /* make a log entry that the connection attempt failed */
      if (status<0) {LogEntry(FuncName,"Connection attempt failed.\n",NRetries);}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to hang-up the modem to break the connection with the host    */
/*------------------------------------------------------------------------*/
/**
   This function attempts to hang-up the modem in order to break the
   connection with the host.  Multiple attempts are made to hang-up the
   modem up to a maximum number of retries.
   
      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:
         This function returns a positive number if the hang-up was
         confirmed.  Zero is returned if the confirmation failed.  A
         negative number is returned if the function parameters were
         determined to be ill-defined.
      \end{verbatim}

   written by Dana Swift
*/
int hangup(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "hangup()";

   /* initialize the return value */
   int i,status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else for (status=0,i=0; i<3; i++)
   {
      if (modem_hangup(port)>0) {status=1; break;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to acquire the modem's attention                              */
/*------------------------------------------------------------------------*/
/**
   This function acquires the modem's attention in preparation for
   receiving commands.  
  
      \begin{verbatim}
      input:
         port.........A structure that contains pointers to machine
                      dependent primitive IO functions.  See the comment
                      section of the SerialPort structure for details.  The
                      function checks to be sure this pointer is not NULL.

      output:
         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}
*/
static int modem_at(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "modem_at()";

   /* initialize the return value */
   int i,status=-1;

   /* define the number of tries allowed */
   const int NRetries=3;
   
   /* validate the SerialPort object */
   if (port && port->dtr && port->rts)
   {
      /* define sentinel value for serial port config() command */
      #define ComQueryBaudRate (LONG_MIN+1L)

      /* query the serial port for its baud rate */
      long int baud=port->config(ComQueryBaudRate); if (baud<=0) {baud=19200;}

      /* retry loop */      
      for (port->rts(1), status=0, i=0; i<NRetries; i++)
      {
         /* toggle the DTR line */
         port->dtr(0); sleep(1); port->dtr(1); sleep(1);

         /* activate the modem's command loop with a CR */
         port->putb('\r'); Wait(100);

         /* send attention request to modem */
         if (chat(port,"AT","OK",3,"\r")>0) {status=1; break;}
         
         /* power cycle the modem */
         port->config(0); sleep(i*10); port->config(baud); sleep(5);
      }
   }
   else {LogEntry(FuncName,"NULL or invalid serial port.\n");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to connect the modem to a remote computer                     */
/*------------------------------------------------------------------------*/
/**
  This function connects the modem to the remote computer according to the
  dial-string.  Motorola's ISU AT Command Reference (SSP-ISU-CPSW-USER-005
  Version 1.3) was used as the guide document.
   
      \begin{verbatim}
      input:
         port.........A structure that contains pointers to machine
                      dependent primitive IO functions.  See the comment
                      section of the SerialPort structure for details.  The
                      function checks to be sure this pointer is not NULL.
         dialstring...The modem command that generates the tones to connect
                      to the remote computer.  Example: ATDT2065436697.
         sec..........The number of seconds allowed to make the connection
                      before this function abandons the connection attempt.

      output:
         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}

   written by Dana Swift
*/
static int modem_connect(const struct SerialPort *port,const char *dialstring,time_t sec)
{
   /* define the logging signature */
   cc *FuncName = "modem_connect()";

   /* initialize the return value */
   int status=-1;
   int is_rudics = 0;
   
   /* check the length of the dialstring */
   if (dialstring && strlen(dialstring)>MAXCMDLEN-1)
   {
      /* log the message */
      LogEntry(FuncName,"Warning...dialstring (%s) longer "
               "than %d bytes.\n",dialstring,MAXCMDLEN-1);
   }

   is_rudics = strcasecmp(dialstring, MLF2_RUDICS) == 0;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the dialstring */
   else if (!dialstring) {LogEntry(FuncName,"NULL dial string.\n");}

   /* restore the modem's factory defaults */
   else if ((status=modem_initialize(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem configuration failed.\n");
   }
   /* Configure for RUDIS if necessary */
   else if(is_rudics && m9500cbst_rudics(port) <= 0)
   {
       LogEntry(FuncName, "Cannot set bearer service type for RUDICS.\n");
       status = 0;
   }
   /* send the escape string and get the modem's response */
   else if (!Wait(50) || chat(port,dialstring,"CONNECT",sec,"\r")<=0)
   {
      /* make a log entry */
      LogEntry(FuncName,"Modem command [%s] failed.\n",dialstring);

      /* indicate failure */
      status=0;
   }

   /* set return value to indicate success */
   else status=1;

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize modem using AT command string                   */
/*------------------------------------------------------------------------*/
/**
   This function initializes the modem with a specified command string.
   Motorola's ISU AT Command Reference (SSP-ISU-CPSW-USER-005 Version 1.3)
   was used as the guide document.
   
      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:
         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}

   written by Dana Swift
*/
static int modem_hangup(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "modem_hangup()";
   
   /* initialize return value */
   int status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else
   {
      /* a longer timeout is needed when hanging up the phone */
      const time_t timeout=10;

      /* define the hang-up command string */
      const char *cmd = "+++~~~ATH0";
      
      /* set an assertion on the maximum allowed length of the command string */
      assert(strlen(cmd)<MAXCMDLEN);

      /* re-initialize return value */
      status=1;
      
      /* send the command string and get the modem's response */
      if (!Wait(50) || chat(port,cmd,"OK",timeout,"\r")<=0)
      {
         /* make a log entry */
         LogEntry(FuncName,"Modem command [%s] failed.\n",cmd);

         /* indicate failure */
         status=0;
      } 
      else if (debuglevel>=5) {LogEntry(FuncName,"Success.\n");}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize modem using AT command string                   */
/*------------------------------------------------------------------------*/
/**
   This function initializes the modem with a specified command string.
   Motorola's ISU AT Command Reference (SSP-ISU-CPSW-USER-005 Version 1.3)
   was used as the guide document.
   
      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
         
      \end{verbatim}

   written by Dana Swift
*/
int modem_initialize(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "modem_initialize()";
   
   int status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* restore the modem's factory defaults */
   else if ((status=modem_restore_factory_configuration(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Unable to restore modem's factory defaults.\n");
   }
    
   else
   {
      /* specify initialization command string */
      const char *cmd = "AT &C1 &D2 &K3 &R1 &S1 E1 Q0 S0=0 S7=45 S10=100 V1 X4";

      /* set an assertion on the maximum allowed length of the command string */
      assert(strlen(cmd)<MAXCMDLEN);
      
      /* re-initialize the return value of this function */
      status=1;

      /* send the command string and get the modem's response */
      if (!Wait(50) || chat(port,cmd,"OK",timeout,"\r")<=0)
      {
         /* make a log entry */
         LogEntry(FuncName,"Modem command [%s] failed.\n",cmd);

         /* indicate failure */
         status=0;
      } 

      /* query the modem to determine if it is an Iridium LBT */
      else if (!Wait(50) || (chat(port,"AT I4","IRIDIUM",timeout,"\r")>0 && m9500cbst(port)<=0))
      {
         /* log the message */
         LogEntry(FuncName,"Selection of bearer service type failed.\n");

         /* indicate failure */
         status=0;
      }

      /* log the message */
      else if (debuglevel>=5) {LogEntry(FuncName,"Success.\n");}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to restore the modem's factory configuration                  */
/*------------------------------------------------------------------------*/
/**
   This function uses the modem AT\&F command function to restore the modem
   to the factory default configuration.  Motorola's ISU AT Command
   Reference (SSP-ISU-CPSW-USER-005 Version 1.3) was used as the guide
   document. 
   
      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:
         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}

   written by Dana Swift
*/
static int modem_restore_factory_configuration(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "modem_restore_factory_configuration()";

   /* initialize the return value */
   int status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else
   {
      /* define the command */
      const char *cmd = "AT&F0";
      
      /* set an assertion on the maximum allowed length of the command string */
      assert(strlen(cmd)<MAXCMDLEN); 

      /* re-initialize the return value of this function */
      status=1;

      /* send the command to restore factory defaults and get response */
      if (!Wait(50) || chat(port,cmd,"OK",timeout,"\r")<=0)
      {
         /* make a log entry */
         LogEntry(FuncName,"Modem command [%s] failed.\n",cmd);

         /* indicate failure */
         status=0;
      }
      
      /* log the message */
      else if (debuglevel>=5) {LogEntry(FuncName,"Success.\n");}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to set the bearer service type                                */
/*------------------------------------------------------------------------*/
/**
   This function selects the Bearer Service Type to be 4800 baud on the
   remote computer.  I have found this command to be necessary in order for
   logins to be successful.  Motorola's ISU AT Command Reference
   (SSP-ISU-CPSW-USER-005 Version 1.3) was used as the guide document.
   
      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:
         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}

   written by Dana Swift
*/
static int m9500cbst(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "m9500cbst()";

   /* initialize the return value */
   int status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else
   {
      /* define the command */
      const char *cmd = "AT+CBST=6,0,1;+IPR=6";
      
      /* set an assertion on the maximum allowed length of the command string */
      assert(strlen(cmd)<MAXCMDLEN); 

      /* re-initialize the return value of this function */
      status=1;

      /* send the command to restore factory defaults and get response */
      if (!Wait(50) || chat(port,cmd,"OK",timeout,"\r")<=0)
      {
         /* make a log entry */
         LogEntry(FuncName,"Modem command [%s] failed.\n",cmd); 

         /* indicate failure */
         status=0;
      }

      /* log the message */
      else if (debuglevel>=5) {LogEntry(FuncName,"Success.\n");}
   }
   
   return status;
}

/**
   This function selects the Bearer Service Type to be 9600 baud to support
   a RUDICS connection. Arguments and return value are the same as m9500cbst().
*/
int m9500cbst_rudics(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "m9500cbst()";

   /* initialize the return value */
   int status=-1;
      
   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else
   {
      /* define the command */
      const char *cmd = "AT+CBST=71,0,1";
      
      /* set an assertion on the maximum allowed length of the command string */
      assert(strlen(cmd)<MAXCMDLEN); 

      /* re-initialize the return value of this function */
      status=1;

      /* send the command to restore factory defaults and get response */
      if (!Wait(50) || chat(port,cmd,"OK",timeout,"\r")<=0)
      {
         /* make a log entry */
         LogEntry(FuncName,"Modem command [%s] failed.\n",cmd); 

         /* indicate failure */
         status=0;
      }

      /* log the message */
      else if (debuglevel>=5) {LogEntry(FuncName,"Success.\n");}
   }
   
   return status;
}

#endif /* MODEM_C */
