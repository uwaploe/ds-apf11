#ifndef SBE41CP_H
#define SBE41CP_H (0x0200U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define sbe41ChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>
#include <stdio.h>

/* function prototypes */
int    Sbe41cpBinAverage(void);
int    Sbe41cpConfig(float PCutOff);
int    Sbe41cpEnablePts(void);
int    Sbe41cpEnterCmdMode(void);
int    Sbe41cpExitCmdMode(void);
int    Sbe41cpFwRev(int *major, int *minor, int *micro, char *FwVariant,
                    char *FwBuild, unsigned int size);
int    Sbe41cpGetP(float *p);
int    Sbe41cpGetPt(float *p, float *t);
int    Sbe41cpGetPts(float *p, float *t, float *s);
int    Sbe41cpLogCal(void);
int    Sbe41cpPowerCycle(unsigned int milliseconds);
int    Sbe41cpSerialNumber(void);
int    Sbe41cpStartCP(time_t FlushSec);
int    Sbe41cpStatus(float *pcutoff, unsigned *serno,
                     unsigned *nsample, unsigned *nbin, time_t *tswait, 
                     float *topint, float *topsize, float *topmax,
                     float *midint, float *midsize, float *midmax,
                     float *botint, float *botsize);
int    Sbe41cpStopCP(void);
int    Sbe41cpTsWait(time_t sec);
int    Sbe41cpUploadCP(FILE *dest);

/* define the return states of the SBE41CP API */
extern const char Sbe41cpTooFew;          /* Too few CP samples */
extern const char Sbe41cpChatFail;        /* Failed chat attempt. */
extern const char Sbe41cpNoResponse;      /* No response received from SBE41CP. */
extern const char Sbe41cpRegExceptn;      /* Response received, regexec() exception */
extern const char Sbe41cpRegexFail;       /* response received, regex no-match */
extern const char Sbe41cpNullArg;         /* Null function argument. */
extern const char Sbe41cpFail;            /* General failure */
extern const char Sbe41cpOk;              /* response received, regex match */
extern const char Sbe41cpPedanticFail;    /* response received, pedantic regex no-match */
extern const char Sbe41cpPedanticExceptn; /* response received, pedantic regex exception */

#endif /* SBE41CP_H */
#ifdef SBE41CP_C
#undef SBE41CP_C

#include <assert.h>
#include <chat.h>
#include <ctdio.h>
#include <ctype.h>
#include <extract.h>
#include <logger.h>
#include <math.h>
#include <nan.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if defined (__arm__)
   #include <apf11.h>
#else
   #include <linux.h>
   #define StackOk() 1
#endif /* __arm__ */

/* define the maximum length of the SBE41CP response */
#define MAXLEN 80

/* define a buffer for communications with the CTD serial port */
static char buf[MAXLEN+1];

/* define a nonpedantic regex pattern for a field */
#define FIELD "([^,]*)"

/* define nonpedantic regex pattern for float */
#define FLOAT "[^-+0-9.]*([-+0-9.]+)[^0-9]*"

/* define pedantic regex patterns for P, T, S, and O */
#define P "[ ]+(-?[0-9]{1,4}\\.[0-9]{2})"
#define T "[ ]+(-?[0-9]{1,2}\\.[0-9]{4})"
#define S "[ ]+(-?[0-9]{1,2}\\.[0-9]{4})"
#define O "[ ]+([0-9]{1,5})"
  
/* define the return states of the SBE41CP API */
const char Sbe41cpTooFew          = -6; /* Too few CP samples */
const char Sbe41cpChatFail        = -5; /* Failed chat attempt. */
const char Sbe41cpNoResponse      = -4; /* No response received from SBE41CP. */
const char Sbe41cpRegExceptn      = -3; /* Response received, regexec() exception */
const char Sbe41cpRegexFail       = -2; /* response received, regex no-match */
const char Sbe41cpNullArg         = -1; /* Null function argument. */
const char Sbe41cpFail            =  0; /* General failure */
const char Sbe41cpOk              =  1; /* General success */
const char Sbe41cpPedanticFail    =  2; /* response received, pedantic regex no-match */
const char Sbe41cpPedanticExceptn =  3; /* response received, pedantic regex exception */

/* external function declarations */
extern int CtdPowerOff(void);
extern int CtdPowerOn(void);
extern int snprintf(char *str, size_t size, const char *format, ...);
extern int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* function to command the SBE41CP to compute bin-averages of samples     */
/*------------------------------------------------------------------------*/
/**
   This function uses the SBE41CP's command mode to compute the bin-averages
   of the samples stored in nvram.  Note: This function does not exit
   command mode upon completion.

      \begin{verbatim}
      output:

         This function returns a positive value on success and a zero or
         negative value on failure.  Here are the possible return values of
         this function:

         Sbe41cpTooFew............Too few samples collected in CP mode.
         Sbe41cpRegexFail.........Response received, regex no-match
         Sbe41cpChatFail..........Execution of configuration commands failed.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                    an exceptional error.
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpFail..............Post-configuration verification failed.
         Sbe41cpOk................Configuration attempt was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpBinAverage(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpBinAverage()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      unsigned int nsample;
      
      /* reinitialize the return value */
      status=Sbe41cpFail;

      /* extract the number of samples from the status request */
      if (Sbe41cpStatus(NULL, NULL, &nsample, NULL, NULL, NULL, 
                        NULL, NULL, NULL, NULL, NULL, NULL, NULL)<=0)
      {
         /* make logentry */
         LogEntry(FuncName,"Status request failed.\n");
      }

      /* bin averages aren't computed for fewer than 10 samples */
      else if (nsample<10)
      {
         /* make logentry */
         LogEntry(FuncName,"Too few samples: %u.\n",nsample);

         /* reinitialize the return value */
         status=Sbe41cpTooFew;
      }
      
      else 
      {
         /* define buffer for output of binaverage command */
         char buf[64];
            
         /* get the current time and define the timeout period */
         const time_t To=time(NULL), TimeOut=3600, timeout=15;
         
         /* execute the command to bin-average the samples */
         if (chat(&ctdio, "binaverage\r", "samples =", 5, "")>0)
         {
            /* wait for the data to be uploaded to the Ctd fifo */
            while (CtdActiveIo(timeout) && difftime(time(NULL),To)<TimeOut) {}

            /* put the SBE41CP back to sleep and enable console io */
            pputs(&ctdio,"qsr\r",2,""); Wait(2000);

            /* parse the output of the SBE41CP's binaverage command */
            while (pgets(&ctdio, buf, sizeof(buf)-1, 5, "\r\n")>0 &&
                   difftime(time(NULL),To)<TimeOut) 
            {
               /* check for end of SBE41CP's bin-binaverage operations */
               if (!strncmp("done",buf,4))
               {
                  if (debuglevel>=2 || (debugbits&SBE41CP_H))
                  {
                     /* make logentry */
                     LogEntry(FuncName,"Finished averaging %d samples in %0.0f "
                              "seconds.\n",nsample,difftime(time(NULL),To));
                  }
                  
                  break;
               }

               /* log any detected STP range exceptions */
               else if (!strncmp("bad: ",buf,5))
               {
                  /* make logentry */
                  LogEntry(FuncName,"Range exception: %s\n",buf+5);
               }
            }
            
            /* indicate success */
            status=Sbe41cpOk;
         }
      }
   }

   /* flush the IO buffers for the CTD serial port */
   if (ctdio.ioflush) ctdio.ioflush();
   
   /* put the SBE41 back to sleep */
   if (status<=0 && chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();

   if (status<=0) {LogEntry(FuncName,"Attempt to bin-average the SBE41CP "
                            "continuous profile failed.\n");}

   return status;
   
   #undef MaxBufLen
}

/*------------------------------------------------------------------------*/
/* function to configure the SBE41CP                                      */
/*------------------------------------------------------------------------*/
/**
   This function configures the SBE41CP for 2-decibar bins over the whole
   water column.  It does this by entering the SBE41CP's command mode and
   executing commands that set the configuration parameters.

   \begin{verbatim}
   input:
      PCutOff...The continuous profile is automatically halted when the
                pressure falls below this cut-off pressure.  In addition,
                the SBE41CP will automatically be powered down.
   
   output:

      This function returns a positive number if the configuration
      attempt was successful.  Zero is returned if a response was
      received but it failed the regex match.  A negative return value
      indicates failure due to an exceptional condition.  Here are the
      possible return values of this function:

      Sbe41cpRegexFail.........Response received, regex no-match
      Sbe41cpChatFail..........Execution of configuration commands failed.
      Sbe41cpRegExceptn........Response received but regexec() failed with
                                 an exceptional error.
      Sbe41cpNoResponse........No response received from SBE41CP.
      Sbe41cpNullArg...........Null function argument.
      Sbe41cpFail..............Post-configuration verification failed.
      Sbe41cpOk................Configuration attempt was successful.
                              
   On success, the normal return value for this function will be 'Sbe41cpOk'.
   \end{verbatim}
*/
int Sbe41cpConfig(float PCutOff)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpConfig()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* define the timeout period */
   const time_t TimeOut = 2;
   
   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      #define MaxBufLen 31
      char buf[MaxBufLen+1];
      float topint, topsize, topmax, midint, midsize, midmax, botint, botsize, pcutoff;
         
      /* reinitialize the return value */
      status=Sbe41cpOk;

      /* write the command to set the pressure cutoff */
      snprintf(buf,MaxBufLen,"pcutoff=%0.1f\r",PCutOff);
     
      /* initialize the control parameters of the SBE41CP */
      if (chat(&ctdio, buf,                        "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "autobinavg=n\r",           "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "top_bin_interval=2\r",     "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "top_bin_size=2\r",         "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "top_bin_max=10\r",         "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "middle_bin_interval=2\r",  "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "middle_bin_size=2\r",      "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "middle_bin_max=20\r",      "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "bottom_bin_interval=2\r",  "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "bottom_bin_size=2\r",      "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "includetransitionbin=n\r", "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "includenbin=y\r",          "S>", TimeOut, "")<=0 ||
          chat(&ctdio, "outputpts=n\r",            "S>", TimeOut, "")<=0  )
      {
         /* log the configuration failure */
         LogEntry(FuncName,"chat() failed.\n");

         /* indicate failure */
         status=Sbe41cpChatFail;
      }

      /* analyze the query response to verify expected configuration */
      else if ((status=Sbe41cpStatus(&pcutoff, NULL, NULL, NULL, NULL, &topint, &topsize, &topmax,
                                     &midint, &midsize, &midmax, &botint, &botsize))>0)
      { 
         /* verify the configuration parameters */
         if (fabs(pcutoff-PCutOff)>0.1 ||
             fabs(topint-2)>0.1 || fabs(topsize-2)>0.1 || fabs(topmax-10)>0.1 ||
             fabs(midint-2)>0.1 || fabs(midsize-2)>0.1 || fabs(midmax-20)>0.1 ||
             fabs(botint-2)>0.1 || fabs(botsize-2)>0.1 )
            {
               /* log the configuration failure */
               LogEntry(FuncName,"Configuration failed.\n");

               /* indicate failure */
               status=Sbe41cpFail;
            }

         /* log the configuration success */
         else if (debuglevel>=2 || (debugbits&SBE41CP_H))
         {
            /* log the configuration failure */
            LogEntry(FuncName,"Configuration successful.\n");
         }
      }
   }
  
   /* put the SBE41 back to sleep */
   if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();

   /* make a logentry */
   if (status<=0) {LogEntry(FuncName,"Attempt to set up SBE41CP failed.\n");}

   return status;
   
   #undef NSUB
   #undef MaxBufLen
}

/*------------------------------------------------------------------------*/
/* function to configure PTS output during CP mode                        */
/*------------------------------------------------------------------------*/
/**
   This function configures the SBE41CP to stream PTS data while in CP mode.

      \begin{verbatim}
      output:

         This function returns a positive number if the configuration
         attempt was successful.  Zero is returned if a response was
         received but it failed the regex match.  A negative return value
         indicates failure due to an exceptional condition.  Here are the
         possible return values of this function:

         Sbe41cpChatFail..........Execution of configuration commands failed.
         Sbe41cpOk................Configuration attempt was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpEnablePts(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpEnablePts()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;
   
   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      /* reinitialize the return value */
      status=Sbe41cpOk;
      
      /* initialize the control parameters of the SBE41CP */
      if (chat(&ctdio, "outputpts=y\r","S>", 4, "")<=0)
      {
         /* log the configuration failure */
         LogEntry(FuncName,"chat() failed.\n");

         /* indicate failure */
         status=Sbe41cpChatFail;
      }

      /* verify the configuration attempt */
      else if (chat(&ctdio,"~ds\r","output is PTS",10,"")<=0)
      {
         /* log the configuration failure */
         LogEntry(FuncName,"Failed attempt to verify configuration.\n");

         /* indicate failure */
         status=Sbe41cpChatFail;
      }

      /* log the configuration success */
      else if (debuglevel>=2 || (debugbits&SBE41CP_H))
      {
         /* log the configuration failure */
         LogEntry(FuncName,"PTS output enabled during CP mode.\n");
      }
   }
  
   /* put the SBE41 back to sleep */
   if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();

   /* log the message */
   if (status<=0) {LogEntry(FuncName,"Failed attempt to enable "
                            "PTS output during CP mode.\n");}

   return status;
   
   #undef NSUB
   #undef MaxBufLen
}

/*------------------------------------------------------------------------*/
/* function to enter the SBE41CP's command mode                           */
/*------------------------------------------------------------------------*/
/**
   This function wakes the SBE41CP and places it in command mode.  It does
   this by asserting the wake pin for 1 full second in order to induce the
   SBE41CP into command mode.  Experience shows that the mode-select line must
   be low when this command is executed or else it initiates a full CTD
   sample.  This will waste energy and throw off timing.

      \begin{verbatim}
      output:

         On success, this function returns a positive number that is the
         SBE41CP serial number if the response matched a regex for the float
         serial number.  Zero is returned if a response was received but it
         failed the regex match.  A negative return value indicates failure
         due to an exceptional condition.  Here are the possible return
         values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                     an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match the
                                     regex pattern for the serial number.
      \end{verbatim}
*/
int Sbe41cpEnterCmdMode(void)
{ 
   /* define the logging signature */
   cc *FuncName = "Sbe41cpEnterCmdMode()";
   
   /* initialize the return value */
   int n,status = Sbe41cpNullArg;
 
   /* validate the CTD serial ports ioflush() function */
   if (!(ctdio.ioflush)) {LogEntry(FuncName,"NULL ioflush() function "
                                   "for serial port.\n");}

   else
   {
      /* define the number of subexpressions in the regex pattern */
      #define NSUB 1

      /* define objects needed for regex matching */
      regex_t regex; regmatch_t regs[NSUB+1]; int errcode;

      /* define the the nonpedantic pattern that will match a float */
      const char *pattern = "SERIAL NO\\.[ ]*([0-9]{4,5})";
      
      /* initialize the communications timeout periods */
      time_t To=time(NULL); const time_t TimeOut=30, timeout=2;
    
      /* compile the regex pattern */
      assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

      /* protect against segfaults */
      assert(NSUB==regex.re_nsub);

      /* reinitialize the return value */
      status=Sbe41cpNoResponse;

      /* clear the mode-select line */
      CtdClearModePin(); 

      /* enable Tx-communications with the CTD */
      if (!CtdIsEnableTx()) {CtdAssertWakePin(); Wait(10); CtdEnableTx();}

      /* enable Rx-interrupts on the CTD serial port */
      CtdEnableRx();
      
      /* fault tolerance loop - keep trying if ctd is busy */
      for (status=0,n=1; status<=0 && difftime(time(NULL),To)<TimeOut; n++)
      {
         /* initiate the wake-up cycle */
         CtdAssertWakePin(); Wait(10); sleep(1); CtdClearWakePin(); Wait(100); ctdio.iflush(); Wait(100);
    
         /* get the SBE41CP command prompt to confirm that SBE41CP is ready for commands */
         if ((status=chat(&ctdio,"\r","S>",3,""))>0)
         {
            /* initialize the reference time */
            const time_t To=time(NULL);

            /* flush the IO buffers of the CTD serial port */
            ctdio.iflush();
      
            /* send the command to display status */
            pputs(&ctdio,"ds\r",timeout,"");

            /* get the response */
            while (pgets(&ctdio,buf,MAXLEN,timeout,"\r\n")>0 && difftime(time(NULL),To)<TimeOut)
            {
               /* log the string received from the CTD serial port */
               if (debuglevel>=4) {LogEntry(FuncName,"[%s]\n",buf);}
               
               /* check the current response against the regex */
               if (!(errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
               {
                  /* extract the serial number from the response */
                  status = atoi(extract(buf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));

                  /* read & discard the remainder of the DS command */
                  while (pgets(&ctdio,buf,MAXLEN,1,"\r\n")>0 && difftime(time(NULL),To)<TimeOut) {}
                     
                  break;
               }

               /* indicate that the response did not match the regex pattern */
               else  {status = (errcode==REG_NOMATCH) ? Sbe41cpRegexFail : Sbe41cpRegExceptn;}
            }

            /* flush the IO queues */
            chat(&ctdio,"\r","S>",2,"");
            
            break;
         }

         /* execute the 'stopprofile' command in case the SBE41CP is in CP mode */
         else {pputs(&ctdio,"stopprofile\r",timeout,""); sleep(2*n);} 
      }
   
      /* clean up the regex pattern buffer */
      regfree(&regex);
   }
   
   return status;

   #undef NSUB
}

/*------------------------------------------------------------------------*/
/* function to exit the SBE41CP's command mode                            */
/*------------------------------------------------------------------------*/
/**
   This function sends the SBE41CP a command to exit command mode and power
   down.  Experience shows that the mode-select line must be low when this
   command is executed or else it initiates a full CTD sample.  This will
   waste energy and throw off timing.
   
      \begin{verbatim}
      output:
         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exceptional error.
      \end{verbatim}
*/
int Sbe41cpExitCmdMode(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpExitCmdMode()";
   
   /* initialize the return value */
   int i,status = -1;
 
   /* validate the CTD serial ports ioflush() function */
   if (!(ctdio.ioflush)) {LogEntry(FuncName,"NULL ioflush() function "
                                   "for serial port.\n");}
   
   else
   {
      /* initialize the communications timeout periods */
      const time_t To=time(NULL), TimeOut=30, timeout=2;

      /* clear the mode-select line */
      CtdClearModePin();
 
      /* enable Tx-communications with the CTD */
      if (!CtdIsEnableTx()) {CtdEnableTx();}

      /* fault tolerance loop - keep trying if ctd is busy */
      for (status=0; status<=0 && difftime(time(NULL),To)<TimeOut;)
      {
         /* flush the CTD's IO buffers */
         ctdio.ioflush();
      
         /* get the SBE41CP command prompt to confirm that SBE41CP is ready for commands */
         if (chat(&ctdio,"\r","S>",timeout,"")>0)
         {
            for (i=0; i<3; i++)
            {
               /* command the SBE41CP to power down and get the expected response */   
               if (chat(&ctdio,"qsr\r","powering down",timeout,"")>0) {status=1; break;}
            }
 
            /* exit the fault tolerance loop on success */
            if (status>0) break;
         }

         /* assert the wake pin to initiate a wake-up cycle */
         CtdAssertWakePin();

         /* pause for 1 second */
         sleep(1);

         /* clear the wake pin */
         CtdClearWakePin();
      }

      /* pause to drain bulk capacitance of CTD controller */
      sleep(3);
      
      /* disable Rx-interrupts */
      if (status>0) {CtdDisableRx();}

      /* log the error message */
      else {LogEntry(FuncName,"Command to power down the SBE41CP failed.\n");}
      
      /* flush the IO buffers for the CTD serial port */
      ctdio.ioflush();
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SBE41CP for its firmware revision                */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41CP for its firmware revision.  It parses the
   response to a 'ds' command using a regex and extracts the firmware
   revision.  The firmware revision consists of major, minor, and
   micro revision identifiers separated by periods, such as: "7.2.3".
   The significance of each of these identifies is provided below.

      \begin{verbatim}
      input:

         size.....This is the maximum number of bytes (including the 0x0
                  string terminator) that will be written into the FwRev
                  function argument.

      output:

         major....The major firmware revision refers to the Sbe41cp
                  controller's hardware.  Significant revisions of the
                  hardware will be associated with increments of the
                  firmware's major revision identifier.

         minor....The minor firmware revision is associated with
                  features of the float firmware that users might be
                  interested in.  Revisions to user-features will be
                  associated with increments of the firmware's minor
                  revision identifier.

         micro....The micro firmware revision is associated with
                  elements of the firmware that are intended to be not
                  relevant to the user.  

         FwRev....The firmware revision string will be written into this
                  buffer.

         On success, this function returns a positive value and stores the
         firmware revision in its function argument.  Zero is returned if a
         response was received but it failed the regex match.  A negative
         return value indicates failure due to an exceptional condition.
         Here are the possible return values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match the
                                     regex pattern for the serial number.
      \end{verbatim}
*/
int Sbe41cpFwRev(int *major, int *minor, int *micro, char *FwVariant, char *FwBuild, unsigned int size)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpFwRev()";

   int status=Sbe41cpNullArg;

   /* initialize function arguments */
   if (major) (*major)=-1;
   if (minor) (*minor)=-1;
   if (micro) (*micro)=-1;
   if (size>0) {FwBuild[0]=0; FwVariant[0]=0;}
   
   /* validate the function argument */
   if (!major || !minor || !micro || !FwBuild || !FwVariant || size<22)
   {
      /* log the message */
      LogEntry(FuncName,"NULL or invalid function argument.\n");
   }

   /* validate the CTD serial ports ioflush() function */
   else if (!(ctdio.iflush))
   {
      /* log the message */
      LogEntry(FuncName,"NULL iflush() function for serial port.\n");
   }

   /* enter SBE41CP command mode */
   else if ((status=Sbe41cpEnterCmdMode())>0)
   {
      /* create a temporary buffer to receive the SBE41CP response string */
      int n; char *p,buf[64];

      /* initialize the return values */
      status=Sbe41cpNoResponse;
      
      /* get the SBE41CP command prompt to confirm that SBE41CP is ready for commands */
      if (chat(&ctdio,"ds\r","SBE 41CP",2,"")>0)
      {
         /* define the number of subexpressions in the regex pattern */
         #define NSUB 3
          
         /* define objects needed for regex matching */
         regex_t fwrev; regmatch_t regs[NSUB+1];

         /* define the the regex pattern for firmware revision */
         const char *fwrev_pattern = "[ ]+V[ ]+([0-9]+)\\.([0-9]+)\\.([0-9]+)[ ]+SERIAL NO";

         /* define the search string for fw-build-date and fw-variant */
         const char *fwbuild = "firmware compilation date: ", *fwvariant="variant: ";

         /* compile the regex pattern */
         assert(!regcomp(&fwrev,fwrev_pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(fwrev.re_nsub==NSUB);

         /* loop through each string of the DS command */
         for (n=0; pgets(&ctdio,buf,sizeof(buf)-1,3,"\r\n")>0 && n<20; n++)
         {
            /* search for the fw-build-date */
            if ((p=strstr(buf,fwbuild)))
            {
               /* extract the fw-build-date */
               p+=strlen(fwbuild); strncpy(FwBuild,p,size); FwBuild[size-1]=0;
            }

            /* search for the fw-variant */
            else if ((p=strstr(buf,fwvariant)))
            {
               /* extract the fw-variant */
               p+=strlen(fwvariant); strncpy(FwVariant,p,size); FwVariant[size-1]=0;
            }
            
            /* search for the fw-revision */
            else if (!regexec(&fwrev,buf,fwrev.re_nsub+1,regs,0))
            {
               /* extract the major, minor, and micro version identifiers */
               (*major) =  atoi(extract(buf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
               (*minor) =  atoi(extract(buf,regs[2].rm_so+1,regs[2].rm_eo-regs[2].rm_so));
               (*micro) =  atoi(extract(buf,regs[3].rm_so+1,regs[3].rm_eo-regs[3].rm_so));
            }

            /* check if any of the search data are still missing */
            if ((*major)==-1 || (*minor)==-1 || (*micro)==-1 || !FwVariant[0] || !FwBuild[0])
            {
               status=Sbe41cpRegexFail;
            }
            
            /* all search data found; read & discard the remainder of DS command then exit the search loop */
            else {status=Sbe41cpOk; for (n=0; pgets(&ctdio,buf,sizeof(buf)-1,1,"\r\n")>0 && n<20; n++) {}; break;}
         }
         
         /* clean up the regex pattern buffer */
         regfree(&fwrev);
      }
   }
   
   /* put the SBE41CP back to sleep */
   if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();
   
   return status;
   #undef NSUB
}

/*------------------------------------------------------------------------*/
/* function to get one pressure measurement from the SBE41CP              */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41CP CTD for a single pressure measurement.

      \begin{verbatim}
      input:
         ctdio....A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
         p........This is where the pressure will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).

         This function returns a positive number if the response received
         from the CTD serial port matched (for each of p, t, and s) a regex
         for a float.  Zero is returned if a response was received but it
         failed the regex match.  A negative return value indicates failure
         due to an exceptional condition.  Here are the possible return
         values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                     an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match the
                                     regex pattern for a float.
         Sbe41cpOk................Response received that matched the pedantic
                                     regex pattern.
         Sbe41cpPedanticFail......Response received that matched a float but
                                     failed to match the pedantic regex pattern.
         Sbe41cpPedanticExceptn...Response received that matched a float but
                                     the pedantic regex experienced an exception.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpGetP(float *p)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpGetP()";
   
   int status=Sbe41cpNullArg;

   /* make sure that floats are 4 bytes in length */
   assert(sizeof(float)==4);
   
   /* validate the function argument */
   if (!p) {LogEntry(FuncName,"NULL function argument.\n");}

   /* validate the CTD serial ports ioflush() function */
   else if (!(ctdio.iflush)) {LogEntry(FuncName,"NULL iflush() function "
                                       "for serial port.\n");}
 
   else
   {
      int errcode;

      /* reinitialize the return values */
      status=Sbe41cpOk;

      /* initialize the return value of 'p' to IEEE NaN */
      *p = NaN();
      
      /* activate the SBE41CP with the hardware control lines */
      errcode=CtdPSample(buf,MAXLEN);

      /* log the data received from the SBE41CP */
      if (debuglevel>=4) {LogEntry(FuncName,"Received: [%s]\n",buf);}
      
      /* check if an error was detected */
      if (errcode<=0)
      {
         /* log the message */
         LogEntry(FuncName,"No response from SBE41CP.\n");

         /* indicate failure */
         status=Sbe41cpNoResponse;
      }

      /* subject the SBE41CP response to lexical analysis */
      else
      {
         /* define the number of subexpressions in the regex pattern */
         #define NSUB 1

         /* define objects needed for regex matching */
         regex_t regex; regmatch_t regs[NSUB+1]; int errcode;

         /* define the the nonpedantic pattern that will match a float */
         const char *pattern = "^" FLOAT;
         
         /* compile the regex pattern */
         assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(NSUB==regex.re_nsub);

         /* check if the current line matches the regex */
         if (!(errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
         {
            /* extract the pressure from the buffer */
            *p = atof(extract(buf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
               
            /* define the pedantic form of the expected response */
            pattern = "^" P;

            /* jettison the nonpedantic regex */
            regfree(&regex);

            /* compile the regex pattern */
            assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

            /* protect against segfaults */
            assert(NSUB==regex.re_nsub);

            /* check if the response matches exactly the expected form */
            if ((errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
            {
               /* log the message */
                LogEntry(FuncName,"Violation of pedantic regex: \"%s\\r\\n\"\n",buf);

               /* reinitialize the return value */
               status = (errcode==REG_NOMATCH) ? Sbe41cpPedanticFail : Sbe41cpPedanticExceptn;
            }
         }

         else
         {
            /* log the message */
            LogEntry(FuncName,"Violation of nonpedantic regex: [%s\\r\\n]\n",buf);
              
            /* indicate that the response from the SBE41CP violated even the nonpedantic regex */
            status = (errcode==REG_NOMATCH) ? Sbe41cpRegexFail : Sbe41cpRegExceptn; 
         }
         
         /* clean up the regex pattern buffer */
         regfree(&regex); 
      }
   }

   return status;
   
   #undef NSUB
}

/*------------------------------------------------------------------------*/
/* function to get a low-power PT sample from the SBE41CP                 */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41CP CTD for a low-power PT measurement.

      \begin{verbatim}
      input:
         ctdio....A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
      
         p........This is where the pressure will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).
      
         t........This is where the temperature will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).

         This function returns a positive number if the response received
         from the CTD serial port matched (for each of p and t) a regex for
         a float.  Zero is returned if a response was received but it failed
         the regex match.  A negative return value indicates failure due to
         an exceptional condition.  Here are the possible return values of
         this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                  an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match (for
                                  each of p, t, and s) the regex pattern for a
                                  float.
         Sbe41cpOk................Response received that matched the pedantic
                                  regex pattern.
         Sbe41cpPedanticFail......Response received that matched a float but
                                  failed to match the pedantic regex pattern.
         Sbe41cpPedanticExceptn...Response received that matched a float but
                                  the pedantic regex experienced an exception.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpGetPt(float *p, float *t)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpGetPt()";

   int status=Sbe41cpNullArg;

   /* make sure that floats are 4 bytes in length */
   assert(sizeof(float)==4);
   
   /* validate the function argument */
   if (!p || !t) {LogEntry(FuncName,"NULL function argument(s).\n");}

   /* validate the CTD serial ports ioflush() function */
   else if (!(ctdio.ioflush))
   {
      /* log the message */
      LogEntry(FuncName,"NULL ioflush() function for serial port.\n");
   }

   else
   {
      int errcode;

      /* reinitialize the return values */
      status=Sbe41cpOk;

      /* initialize the return value of 'p' and 't' to IEEE NaN */
      *p=NaN(); *t=NaN();

      /* activate the SBE41CP with the hardware control lines */
      errcode=CtdPtSample(buf,MAXLEN);

      /* log the data received from the SBE41CP */
      if (debuglevel>=4 || (debugbits&SBE41CP_H))
      {
         /* log the message */
         LogEntry(FuncName,"Received: [%s]\n",buf);
      }
      
      /* check if an error was detected */
      if (errcode<=0)
      {
         /* log the message */
         LogEntry(FuncName,"No response from SBE41CP.\n");

         /* indicate failure */
         status=Sbe41cpNoResponse;
      }
      
      /* subject the SBE41CP response to lexical analysis */
      else
      {
         /* define the number of subexpressions in the regex pattern */
         #define NSUB 2
          
         /* define objects needed for regex matching */
         regex_t regex; regmatch_t regs[NSUB+1]; int errcode;

         /* define the the nonpedantic pattern that will match a float */
         const char *pattern = "^" FIELD "," FIELD;
         
         /* compile the regex pattern */
         assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(regex.re_nsub==NSUB);

         /* log the string received from the CTD */
         if (debuglevel>=4 || (debugbits&SBE41CP_H))
         {
            /* log the message */
            LogEntry(FuncName,"[%s]\n",buf);
         }
         
         /* check if the current line matches the regex */
         if (!(errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
         {
            /* initialize pointers to the P,T fields */
            const char *pbuf=0, *tbuf=0;

            /* define the pedantic form of the expected response */
            pattern = "^" P "," T; 

            /* jettison the nonpedantic regex */
            regfree(&regex);

            /* compile the pedantic regex pattern */
            assert(!regcomp(&regex,pattern,REG_NOSUB|REG_EXTENDED|REG_NEWLINE));

            /* check if the response matches exactly the expected form */
            if ((errcode=regexec(&regex,buf,0,0,0)))
            {
               /* log the message */
               LogEntry(FuncName,"Violation of pedantic regex: [%s\\r\\n]\n",buf);

               /* reinitialize the return value */
               status = (errcode==REG_NOMATCH) ? Sbe41cpPedanticFail : Sbe41cpPedanticExceptn;
            }

            /* segment the buffer into separate P and T fields */
            if (regs[1].rm_so>=0 && regs[1].rm_eo>=0) {pbuf=buf+regs[1].rm_so; buf[regs[1].rm_eo]=0;}
            if (regs[2].rm_so>=0 && regs[2].rm_eo>=0) {tbuf=buf+regs[2].rm_so; buf[regs[2].rm_eo]=0;}

            /* jettison the pedantic regex */
            regfree(&regex);

            /* compile the regex pattern for a float */
            assert(!regcomp(&regex,FLOAT,REG_EXTENDED|REG_NEWLINE));

            /* protect against segfaults */
            assert(regex.re_nsub==1);

            /* check the pressure-field against a nonpedantic float regex pattern */
            if (pbuf && !regexec(&regex,pbuf,regex.re_nsub+1,regs,0)) 
            {
               /* extract pressure from the buffer */
               *p = atof(extract(pbuf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
            }

            /* check the temperature-field against a nonpedantic float regex pattern */
            if (tbuf && !regexec(&regex,tbuf,regex.re_nsub+1,regs,0))
            {
               /* extract temperature from the buffer */               
               *t = atof(extract(tbuf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
            }
         }
         
         else
         {
            /* make logentry */
            LogEntry(FuncName,"Violation of 2-fields regex: [%s\\r\\n]\n",buf);
            
            /* indicate that the response from the SBE41CP violated even the nonpedantic regex */
            status = (errcode==REG_NOMATCH) ? Sbe41cpRegexFail : Sbe41cpRegExceptn; 
         }
         
         /* clean up the regex pattern buffer */
         regfree(&regex); 
      }
   }
   
   return status;
   
   #undef NSUB 
}

/*------------------------------------------------------------------------*/
/* function to get a full PTS sample from the SBE41CP                     */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41CP CTD for a full PTS measurement.

      \begin{verbatim}
      input:
         ctdio....A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
      
         p........This is where the pressure will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).
      
         t........This is where the temperature will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).
      
         s........This is where the salinity will be stored when the
                  function returns.  If an error occurs, this is set to NaN
                  (according to IEEE floating point format).

         This function returns a positive number if the response received
         from the CTD serial port matched (for each of p, t, and s) a regex
         for a float.  Zero is returned if a response was received but it
         failed the regex match.  A negative return value indicates failure
         due to an exceptional condition.  Here are the possible return
         values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                     an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match (for
                                     each of p, t, and s) the regex pattern for a
                                     float.
         Sbe41cpOk................Response received that matched the pedantic
                                     regex pattern.
         Sbe41cpPedanticFail......Response received that matched a float but
                                     failed to match the pedantic regex pattern.
         Sbe41cpPedanticExceptn...Response received that matched a float but
                                     the pedantic regex experienced an exception.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpGetPts(float *p, float *t, float *s)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpGetPts()";
   
   int status=Sbe41cpNullArg;

   /* make sure that floats are 4 bytes in length */
   assert(sizeof(float)==4);
   
   /* validate the function argument */
   if (!p || !t || !s)
   {
      /* log the message */
      LogEntry(FuncName,"NULL function argument(s).\n");
   }

   /* validate the CTD serial ports ioflush() function */
   else if (!(ctdio.ioflush))
   {
      /* log the message */
      LogEntry(FuncName,"NULL ioflush() function for serial port.\n");
   }

   else
   {
      int errcode;
      
      /* initialize the communications timeout period */
      const time_t timeout=70;

      /* reinitialize the return values */
      status=Sbe41cpOk;

      /* initialize the return value of 'p', 't', and 's' to IEEE NaN */
      *p=NaN(); *t=NaN(); *s=NaN();
 
      /* activate the SBE41CP with the hardware control lines */
      errcode=CtdPtsSample(buf,MAXLEN,timeout);

      /* log the data received from the SBE41CP */
      if (debuglevel>=4 || (debugbits&SBE41CP_H))
      {
         /* make logentry */
         LogEntry(FuncName,"Received: [%s]\n",buf);
      }
      
      /* check if an error was detected */
      if (errcode<=0)
      {
         /* make logentry */
         LogEntry(FuncName,"No response from SBE41CP.\n");

         /* indicate failure */
         status=Sbe41cpNoResponse;
      }
      
      /* subject the SBE41CP response to lexical analysis */
      else
      {
         /* define the number of subexpressions in the regex pattern */
         #define NSUB 3
          
         /* define objects needed for regex matching */
         regex_t regex; regmatch_t regs[NSUB+1]; int errcode;

         /* define the the nonpedantic pattern that will match a float */
         const char *pattern = "^" FIELD "," FIELD "," FIELD;
         
         /* compile the regex pattern */
         assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(regex.re_nsub==NSUB);
         
         /* check if the current line matches the regex */
         if (!(errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
         {
            /* initialize pointers to the P,T,S fields */
            const char *pbuf=0, *tbuf=0, *sbuf=0;

            /* define the pedantic form of the expected response */
            pattern = "^" P "," T "," S "$"; 

            /* jettison the nonpedantic regex */
            regfree(&regex);

            /* compile the pedantic regex pattern */
            assert(!regcomp(&regex,pattern,REG_NOSUB|REG_EXTENDED|REG_NEWLINE));

            /* check if the response matches exactly the expected form */
            if ((errcode=regexec(&regex,buf,0,0,0)))
            {
               /* make logentry */
               LogEntry(FuncName,"Violation of pedantic regex: [%s\\r\\n]\n",buf);

               /* reinitialize the return value */
               status = (errcode==REG_NOMATCH) ? Sbe41cpPedanticFail : Sbe41cpPedanticExceptn;
            }

            /* segment the buffer into separate P,T, and S fields */
            if (regs[1].rm_so>=0 && regs[1].rm_eo>=0) {pbuf=buf+regs[1].rm_so; buf[regs[1].rm_eo]=0;}
            if (regs[2].rm_so>=0 && regs[2].rm_eo>=0) {tbuf=buf+regs[2].rm_so; buf[regs[2].rm_eo]=0;}
            if (regs[3].rm_so>=0 && regs[3].rm_eo>=0) {sbuf=buf+regs[3].rm_so; buf[regs[3].rm_eo]=0;}

            /* jettison the pedantic regex */
            regfree(&regex);

            /* compile the regex pattern for a float */
            assert(!regcomp(&regex,FLOAT,REG_EXTENDED|REG_NEWLINE));

            /* protect against segfaults */
            assert(regex.re_nsub==1);

            /* check the pressure-field against a nonpedantic float regex pattern */
            if (pbuf && !regexec(&regex,pbuf,regex.re_nsub+1,regs,0)) 
            {
               /* extract pressure from the buffer */
               *p = atof(extract(pbuf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
            }

            /* check the temperature-field against a nonpedantic float regex pattern */
            if (tbuf && !regexec(&regex,tbuf,regex.re_nsub+1,regs,0))
            {
               /* extract temperature from the buffer */               
               *t = atof(extract(tbuf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
            }
            
            /* check the salinity-field against a nonpedantic float regex pattern */
            if (sbuf && !regexec(&regex,sbuf,regex.re_nsub+1,regs,0))
            {
               /* extract salinity from the buffer */               
               *s = atof(extract(sbuf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
            }
         }
         
         else
         {
            /* log the message */
            LogEntry(FuncName,"Violation of 3-fields regex: [%s\\r\\n]\n",buf);
            
            /* indicate that the response from the SBE41CP violated even the nonpedantic regex */
            status = (errcode==REG_NOMATCH) ? Sbe41cpRegexFail : Sbe41cpRegExceptn; 
         }
         
         /* clean up the regex pattern buffer */
         regfree(&regex); 
      }
   }
   
   return status;
   
   #undef NSUB 
}

/*------------------------------------------------------------------------*/
/* function to log the SBE41's calibration coefficents                    */
/*------------------------------------------------------------------------*/
/**
   This function uses the SBE41 communications mode to log its calibration
   coefficients via the SBE41 'dc' command.
   
      \begin{verbatim}
      input:
         ctdio....A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
      
         This function returns a positive number if a response was received
         from the CTD serial port.  Zero is returned if an unexpected
         response was received.  A negative return value indicates failure
         due to an exceptional condition.  Here are the possible return
         values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                     an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match the
                                     regex pattern for the serial number.
         Sbe41cpOk................Response received.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpLogCal(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpLogCal()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* enter SBE41 command mode */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      /* define the timeout period for communications mode */
      const time_t To=time(NULL), timeout=2, TimeOut=60;

      /* reinitialize the return value */
      status = Sbe41cpOk;

      /* flush the Rx queue of the ctd serial port */
      if (ctdio.iflush) ctdio.iflush();
      
      /* send the command to display calibration coefficients */
      pputs(&ctdio,"dc\r",timeout,"");

      /* wait for the data to be uploaded to the Ctd fifo */
      while (CtdActiveIo(timeout) && difftime(time(NULL),To)<TimeOut/2) {}

      /* read the SBE41 response from the serial port */
      while (pgets(&ctdio,buf,MAXLEN,timeout,"\r\n")>0 && difftime(time(NULL),To)<TimeOut)
      {
         /* ignore "dc" and log the current coefficients */
         if (buf[0] && !strstr(buf,"S>") && !strstr(buf,"dc"))
         {
            /* make logentry */
            LogEntry(FuncName,"%s\n",buf);
         }
      }
   }
   else {LogEntry(FuncName,"Attempt to enter command mode failed "
                  "[errcode: %d] - aborting.\n",status);}
   
   /* put the SBE41 back to sleep */
   if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to power cycle the Sbe41cp                                    */
/*------------------------------------------------------------------------*/
/**
   This function initiates a power-cycle of the Sbe41cp by
   switching-off battery power to the Apf11's CTD header (CN1),
   pausing for a specified time, and then switching-on batter power to
   the CTD header.

   \begin{verbatim}
   input:

       milliseconds...This specifies the pause after switching-off
                      battery power to the Apf11's CTD header (CN1).
                      Experience shows that the CTD should be powered
                      down for at least two seconds before powering
                      back up in order to ensure correct CTD behavior.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Sbe41cpPowerCycle(unsigned int milliseconds)
{
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* switch-off battery power to the Apf11's CTD header (CN1) */
   if (CtdPowerOff()<=0) status=Sbe41cpFail;

   /* pause for a specified period */
   Wait(milliseconds);

   /* switch-on battery power to the Apf11's CTD header (CN1) */
   if (CtdPowerOn()<=0) status=Sbe41cpFail;

   /* short pause after switching-on battery power */ 
   Wait(100);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query an SBE41CP for its serial number                     */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41CP for its serial number. 

      \begin{verbatim}
      input:
         ctdio....A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
      
         This function returns a positive number if the response received
         from the CTD serial port matched (for each of p, t, and s) a regex
         for a 4-digit integer.  Zero is returned if a response was received
         but it failed the regex match.  A negative return value indicates
         failure due to an exceptional condition.  Here are the possible
         return values of this function:
         
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                     an exceptional error.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not matchthe
                                     regex pattern for a 4-digit integer.
         Sbe41cpOk................Response received that matched the pedantic
                                     regex pattern.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpSerialNumber(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpSerialNumber()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Attempt to query SBE41CP serial number failed.\n");
   }
      
   /* exit the SBE41CP's command mode */
   Sbe41cpExitCmdMode();

   return status;
}

/*------------------------------------------------------------------------*/
/* function to start the continuous profiling mode of the SBE41CP         */
/*------------------------------------------------------------------------*/
/**
   This function initiates the continuous profiling mode of the SBE41CP.

      \begin{verbatim}
      input:
         FlushSec...This specifies the length of time (seconds) that the
                    pump is allowed to flush the conductivity cell before
                    data acquisition is started.
      
      output:

         This function returns a positive number if continuous profile was
         successfully initiated.  Zero is usually returned if the attempt
         failed.  A negative return value indicates failure due to an
         exceptional condition.  Here are the possible return values of this
         function:

         Sbe41cpRegExceptn........Response received but regexec() failed with
                                    an exceptional error.
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpRegexFail.........Response received but it did not match the
                                    expected regex pattern.
         Sbe41cpFail..............Attempt to start profile failed.
         Sbe41cpOk................Attempt to start profile was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpStartCP(time_t FlushSec)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpStartCP()";
   
   /* initialize the return value */
   int i,status=Sbe41cpNullArg;

   /* define the timeout period */
   const time_t TimeOut = 15;

   /* make sure the pre-profile flush period is reasonable */
   if (FlushSec<10) FlushSec=10; else if (FlushSec>60) FlushSec=60;
   
   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      /* define the minimum sample rate */
      const float SecPerSmpl=1.2;

      /* compute the number of samples in the flush period */
      int NSamples=(int)(((float)FlushSec)/SecPerSmpl);
      
      /* create the command to start the profile */
      char buf[32]; snprintf(buf,31,"startprofile%d\r",NSamples);

      /* retry loop */
      for (status=Sbe41cpFail, i=0; i<3; i++)
      {
         /* execute the command to start the continuous profile */
         if (chat(&ctdio,buf,"profile started",TimeOut, "")>0) 
         {
            /* pause long enough to flush the conductivity cell */
            sleep(FlushSec+15);

            /* log the start of the continuous profile */
            if (debuglevel>=2 || (debugbits&SBE41CP_H))
            {
               /* log the message */
               LogEntry(FuncName,"Continuous profile started.\n");
            }
            
            /* indicate success */
            status=Sbe41cpOk; break;
         }
      }
   }
      
   /* exit the SBE41CP's command mode */
   else Sbe41cpExitCmdMode();
 
   if (status<=0) {LogEntry(FuncName,"Attempt to start the SBE41CP "
                            "continuous profile failed.\n");}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SBE41CP for configuration/status parameters      */
/*------------------------------------------------------------------------*/
/**
   This function executes an SBE41CP status query and then extracts various
   configuration/status parameters from the response.
   
     \begin{verbatim}
     output:
        serno.....The serial number of the SBE41CP.
        nsample...The number of samples presently stored in the SBE41CP nvram.
        nbin......The number of bins created by the bin-averaging scheme.
        tswait....The prelude period (seconds) before samples are collected.
        topint....The pressure bin interval (dbar) for the shallow sampling regime.
        topsize...The pressure bin size (dbar) for the shallow sampling regime.
        topmax....The maximum pressure (dbar) for the shallow sampling regime.
        midint....The pressure bin interval (dbar) for the mid-depth sampling regime.
        midsize...The pressure bin size (dbar) for the mid-depth sampling regime.
        midmax....The maximum pressure (dbar) for the mid-depth sampling regime.
        botint....The pressure bin interval (dbar) for the deep sampling regime.
        botsize...The pressure bin size (dbar) for the deep sampling regime.
        pcutoff...The cut-off pressure where the SBE41CP pump is shut down
                     in order to avoid contamination by surface film.

     This function returns a positive return value on success and a zero or
     negative value on failure.  Here are the possible return values of this
         function:

         Sbe41cpRegExceptn........Response received but regexec() failed with
                                    an exceptional error.
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpFail..............Attempt to start profile failed.
         Sbe41cpOk................Attempt to start profile was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
   \end{verbatim}
*/
int Sbe41cpStatus(float *pcutoff, unsigned *serno,
                  unsigned *nsample, unsigned *nbin, time_t *tswait, 
                  float *topint, float *topsize, float *topmax,
                  float *midint, float *midsize, float *midmax,
                  float *botint, float *botsize)
{
   #define MaxBufLen 79
   char *p,buf[MaxBufLen+1];

   /* define the parameter tokens */
   const char *const token[]=
   {
      "SERIAL NO.",
      "stop profile when pressure is less than =",
      "number of samples =",
      "number of bins =",
      "top bin interval =",
      "top bin size =",
      "top bin max =",
      "middle bin interval =",
      "middle bin size =",
      "middle bin max =",
      "bottom bin interval =",
      "bottom bin size =",
      "take sample wait time =",
   };

   /* initialize the return value */
   int i,status=Sbe41cpNoResponse;

   /* define the timeout period */
   const time_t TimeOut = 2;
   
   /* initialize the function parameters */
   if (serno)   *serno   = (unsigned)(-1);
   if (nsample) *nsample = (unsigned)(-1);
   if (nbin)    *nbin    = (unsigned)(-1);
   if (tswait)  *tswait  = (unsigned)(-1);
   if (topint)  *topint  = NaN();
   if (topsize) *topsize = NaN(); 
   if (topmax)  *topmax  = NaN(); 
   if (midint)  *midint  = NaN(); 
   if (midsize) *midsize = NaN(); 
   if (midmax)  *midmax  = NaN(); 
   if (botint)  *botint  = NaN(); 
   if (botsize) *botsize = NaN(); 
   if (pcutoff) *pcutoff = NaN();

   /* enable Tx-communications with the CTD */
   if (!CtdIsEnableTx()) {CtdEnableTx();}

   for (i=0; i<3 && status!=Sbe41cpOk; i++)
   {
      /* flush the IO queues */
      if (ctdio.ioflush) {Wait(10); ctdio.ioflush(); Wait(10);}

      if (chat(&ctdio,"\r","S>",2,"")>0)
      {
         /* query the SBE41CP for its current configuration */
         pputs(&ctdio,"ds\r",TimeOut,"");

         /* analyze the query response to verify expected configuration */
         while (pgets(&ctdio,buf,MaxBufLen,TimeOut,"\r\n")>0)
         {
            if      (serno   && (p=strstr(buf,token[ 0]))) {*serno   = atoi(p+strlen(token[ 0]));} 
            else if (pcutoff && (p=strstr(buf,token[ 1]))) {*pcutoff = atof(p+strlen(token[ 1]));}
            else if (nsample && (p=strstr(buf,token[ 2]))) {*nsample = atoi(p+strlen(token[ 2]));}
            else if (nbin    && (p=strstr(buf,token[ 3]))) {*nbin    = atoi(p+strlen(token[ 3]));} 
            else if (topint  && (p=strstr(buf,token[ 4]))) {*topint  = atof(p+strlen(token[ 4]));} 
            else if (topsize && (p=strstr(buf,token[ 5]))) {*topsize = atof(p+strlen(token[ 5]));} 
            else if (topmax  && (p=strstr(buf,token[ 6]))) {*topmax  = atof(p+strlen(token[ 6]));} 
            else if (midint  && (p=strstr(buf,token[ 7]))) {*midint  = atof(p+strlen(token[ 7]));} 
            else if (midsize && (p=strstr(buf,token[ 8]))) {*midsize = atof(p+strlen(token[ 8]));} 
            else if (midmax  && (p=strstr(buf,token[ 9]))) {*midmax  = atof(p+strlen(token[ 9]));} 
            else if (botint  && (p=strstr(buf,token[10]))) {*botint  = atof(p+strlen(token[10]));} 
            else if (botsize && (p=strstr(buf,token[11]))) {*botsize = atof(p+strlen(token[11]));} 
            else if (tswait  && (p=strstr(buf,token[12]))) {*tswait  = atol(p+strlen(token[12]));} 
            
            if (strstr(buf,"take sample wait time")) break;
            
            status=Sbe41cpOk;
         }
      }
   }
   
   /* validate each requested parameter */
   if (status>0)
   {
      if (serno   && (*serno   == (unsigned int)(-1))) status=Sbe41cpFail;
      if (nsample && (*nsample == (unsigned int)(-1))) status=Sbe41cpFail;
      if (nbin    && (*nbin    == (unsigned int)(-1))) status=Sbe41cpFail;
      if (tswait  && (*tswait  == (time_t)(-1)))       status=Sbe41cpFail;
      if (topint  && isNaN(*topint))                   status=Sbe41cpFail;
      if (topsize && isNaN(*topsize))                  status=Sbe41cpFail; 
      if (topmax  && isNaN(*topmax))                   status=Sbe41cpFail; 
      if (midint  && isNaN(*midint))                   status=Sbe41cpFail; 
      if (midsize && isNaN(*midsize))                  status=Sbe41cpFail; 
      if (midmax  && isNaN(*midmax))                   status=Sbe41cpFail; 
      if (botint  && isNaN(*botint))                   status=Sbe41cpFail; 
      if (botsize && isNaN(*botsize))                  status=Sbe41cpFail; 
      if (pcutoff && isNaN(*pcutoff))                  status=Sbe41cpFail;
      Wait(100);
   }
   
   return status;

   #undef MaxBufLen
}

/*------------------------------------------------------------------------*/
/* function to stop the continuous profiling mode of the SBE41CP          */
/*------------------------------------------------------------------------*/
/**
   This function stops the continuous profiling mode of the SBE41CP.  The
   mode-select line is set for p-only mode to prevent the pump motor from
   running if continuous mode has already been deactivated (eg.,
   automatically by the SBE41CP).

      \begin{verbatim}
      output:

         This function returns a positive number if continuous profile was
         successfully stopped.  Zero is usually returned if the attempt
         failed.  A negative return value indicates failure due to an
         exceptional condition.  Here are the possible return values of this
         function:

         Sbe41cpNullArg...........Null function argument.
         Sbe41cpFail..............Attempt to stop profile failed.
         Sbe41cpOk................Attempt to stop profile was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpStopCP(void)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpStopCP()";
   
   /* initialize the return value */
   int i,status=Sbe41cpNullArg;

   /* define the timeout period */
   const time_t TimeOut = 5;

   /* clear the mode-select line */
   CtdClearModePin(); 

   /* enable Tx-communications with the CTD */
   if (!CtdIsEnableTx()) {CtdEnableTx();}

   /* retry loop */
   for (status=Sbe41cpFail, i=0; i<3; i++)
   {
      /* execute the command to stop the continuous profile */
      if (chat(&ctdio,"stopprofile\r","profile stopped",TimeOut,"")>0) 
      {
         /* log the end of the continuous profile */
         if (debuglevel>=2 || (debugbits&SBE41CP_H))
         {
            /* make logentry */
            LogEntry(FuncName,"Continuous profile stopped.\n");
         }
 
         /* exit the SBE41CP's command mode */ 
         Wait(100); if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();

         /* indicate success */
         status=Sbe41cpOk; break;
      }
   }

   if (status<=0) {LogEntry(FuncName,"Attempt to stop SBE41CP "
                            "continuous profile failed.\n");}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the presample pump period of the SBE41CP         */
/*------------------------------------------------------------------------*/
/**
   This function configures the presample pumping period of the SBE41CP.  It
   does this by entering the SBE41CP's command mode and executing the
   SBE41CP's 'tswait' command.  This configuration parameter applies only to
   the spot-sampled PTS mode on SBE41CPs without an oxygen sensor.  The
   tswait command determines the number of seconds after pump activation
   before a full PTS sample is collected.  

      \begin{verbatim}
      input:
         sec...The number of seconds after pump activation before the full
               PTS sample is collected.
      
      output:

         This function returns a positive number if the configuration
         attempt was successful.  Zero is returned if a response was
         received but it failed the regex match.  A negative return value
         indicates failure due to an exceptional condition.  Here are the
         possible return values of this function:

         Sbe41cpRegexFail.........Response received, regex no-match
         Sbe41cpChatFail..........Execution of configuration commands failed.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                    an exceptional error.
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpFail..............Post-configuration verification failed.
         Sbe41cpOk................Configuration attempt was successful.
                                 
      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpTsWait(time_t sec)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpTsWait()";
   
   /* initialize the return value */
   int status=Sbe41cpNullArg;

   /* define the timeout period */
   const time_t TimeOut = 2;

   /* make sure the wait period is well conditioned */
   if (sec<2) sec=2; else if (sec>60) sec=60;
   
   /* SBE41CP serial number is the return value of Sbe41cpEnterCmdMode() */
   if ((status=Sbe41cpEnterCmdMode())>0)
   {
      #define MaxBufLen 31
      char buf[MaxBufLen+1];
      
      time_t tswait;
         
      /* reinitialize the return value */
      status=Sbe41cpOk;

      /* write the command to set the pressure cutoff */
      snprintf(buf,MaxBufLen,"tswait=%ld\r",sec);
      
      /* initialize the control parameters of the SBE41CP */
      if (chat(&ctdio, buf,"S>", TimeOut,"")<=0)
      {
         /* log the configuration failure */
         LogEntry(FuncName,"chat() failed.\n");

         /* indicate failure */
         status=Sbe41cpChatFail;
      }

      /* analyze the query response to verify expected configuration */
      else if ((status=Sbe41cpStatus(NULL, NULL, NULL, NULL, &tswait, NULL, NULL,
                                     NULL, NULL, NULL, NULL, NULL, NULL))>0)
      { 
         /* verify the configuration parameters */
         if (tswait!=sec)
         {
            /* log the configuration failure */
            LogEntry(FuncName,"Configuration failed.\n");

            /* indicate failure */
            status=Sbe41cpFail;
         }

         /* log the configuration success */
         else if (debuglevel>=2 || (debugbits&SBE41CP_H))
         {
            /* log the configuration failure */
            LogEntry(FuncName,"Configuration successful.\n");
         }
      }
   }
  
   /* put the SBE41 back to sleep */
   if (chat(&ctdio,"\r","S>",2,"")>0) Sbe41cpExitCmdMode();

   if (status<=0) {LogEntry(FuncName,"Attempt to set up SBE41CP failed.\n");}

   return status;
   
   #undef NSUB
   #undef MaxBufLen
}

/*------------------------------------------------------------------------*/
/* function to upload the continuous profile from the SBE41CP             */
/*------------------------------------------------------------------------*/
/**
   This function uploads the continuous profile from the SBE41CP and writes
   it to a file.  

      \begin{verbatim}
      input:
         dest....The file to which the profile will be written.
         
      output:

         This function returns a positive number if continuous profile was
         successfully stopped.  Zero is usually returned if the attempt
         failed.  A negative return value indicates failure due to an
         exceptional condition.  Here are the possible return values of this
         function:

         Sbe41cpTooFew............Too few samples collected in CP mode.
         Sbe41cpRegexFail.........Response received, regex no-match
         Sbe41cpChatFail..........Execution of configuration commands failed.
         Sbe41cpRegExceptn........Response received but regexec() failed with
                                    an exceptional error.
         Sbe41cpNoResponse........No response received from SBE41CP.
         Sbe41cpNullArg...........Null function argument.
         Sbe41cpFail..............General failure.
         Sbe41cpOk................Configuration attempt was successful.

      On success, the normal return value for this function will be 'Sbe41cpOk'.
      \end{verbatim}
*/
int Sbe41cpUploadCP(FILE *dest)
{
   /* define the logging signature */
   cc *FuncName = "Sbe41cpUploadCP()";

   #ifdef IDO        
      #define ZEROS "000000000000000000"
   #else
      #define ZEROS "00000000000000"
   #endif

   #define MaxStrLen 80
   char buf[MaxStrLen+1];
   unsigned int n=0,zeros,serno,nsample,nbin;
   const int NRetry = 2;
   
   /* initialize the return value */
   int i, status=Sbe41cpNullArg;
   
   /* enable Tx-communications with the CTD */
   if (!CtdIsEnableTx()) {CtdEnableTx();}

   for (i=0; i<NRetry && status<=0; i++)
   {
      /* make sure that the profile is stopped and the 'S>' prompt is found */
      chat(&ctdio, "stopprofile\r","S>", 5,""); 
         
      /* execute commands to bin-average the samples */
      if ((status=Sbe41cpBinAverage())>0)
      {
         /* reinitialize return value */
         status=Sbe41cpFail; 

         /* flush the CTD IO buffers */
         if (ctdio.ioflush) ctdio.ioflush();
         
         /* make sure that command mode is still active */
         Sbe41cpEnterCmdMode();
            
         /* query for the serial number and number of samples \& bins */
         if (Sbe41cpStatus(NULL,&serno,&nsample,&nbin,NULL,NULL,
                           NULL,NULL,NULL,NULL,NULL,NULL,NULL)<=0)
         {
            /* log the message */
            LogEntry(FuncName,"Status request failed.\n");
         }

         else
         {
            /* log the number of samples and bins */
            if (debuglevel>=2 || (debugbits&SBE41CP_H))
            {
               /* log the message */
               LogEntry(FuncName,"Sbe41cpSerNo[%04u] NSample[%u] NBin[%u]\n",
                        serno,nsample,nbin); 
            }
            
            /* bin averages aren't computed for fewer than 10 samples */
            if (nsample<10)
            {
               /* log the message */
               LogEntry(FuncName,"Too few samples.\n");
         
               /* reinitialize the return value */
               status=Sbe41cpTooFew;
            }

            /* make sure that bins have been defined */
            else if (!nbin) {LogEntry(FuncName,"No bins defined; "
                                      "execute Sbe41cpBinAverage().\n");}

            /* flush the CTD IO queues */
            else if (!ctdio.ioflush || ctdio.ioflush()<=0)
            {
               /* log the message */
               LogEntry(FuncName,"Unable to flush CTD IO buffers.\n");
            }

            /* execute the command to upload the hex data */         
            else if (chat(&ctdio,"dah\r","dah\r\n",2,"")<=0)
            {
               /* log the message */
               LogEntry(FuncName,"Unable to upload data from SBE41CP.\n");
            }
            
            else
            {
               /* define quantities to implement time-out protection */
               const time_t timeout=15, TimeOut=900;  time_t To=time(NULL);
                        
               /* wait for the data to be uploaded to the Ctd fifo */
               while (CtdActiveIo(timeout) && difftime(time(NULL),To)<TimeOut) {}
      
               /* send the command to shutdown the SBE41CP */
               pputs(&ctdio,"qsr",2,"\r"); 
         
               /* write the time-stamp, nmuber of samples, and number of bins */
               if (dest)
               { 
                  char buf[32]; time_t To=time(NULL);
                  strftime(buf,31,"%b %d %Y %H:%M:%S",gmtime(&To));
                  fprintf(dest,"# %s Sbe41cpSerNo[%04u] NSample[%u] NBin[%u]\n",
                          buf,serno,nsample,nbin);
               }
               
               /* loop through the uploaded lines */
               for (To=time(NULL), zeros=0 ,n=0; pgets(&ctdio,buf,MaxStrLen,5,"\r\n")>0 &&
                       difftime(time(NULL),To)<TimeOut; n++)
               {
                  /* check if upload is complete */
                  if (strstr(buf,"upload complete")) {status=Sbe41cpOk; break;}

                  /* check if the current line is all zeros */
                  else if (!strcmp(buf,ZEROS)) zeros++;
         
                  else
                  {
                     /* write the current uploaded line(s) to the output file */
                     if (dest)
                     {
                        if (zeros>0) {fprintf(dest,ZEROS"[%d]\n",zeros);}
                        fprintf(dest,"%s\n",buf);
                     }
                     
                     /* log the current uploaded line(s) */
                     if (debuglevel>=4) 
                     {
                        if (zeros>0) {LogEntry(FuncName,"%s\n",ZEROS"[%d]\n");}

                        /* log the buffer */
                        LogEntry(FuncName,"%s\n",buf);
                     }
                     
                     /* reinitialize the zeros-counter */
                     zeros=0; 
                  }
               }

               /* write the remaining uploaded lines to the output file */
               if (dest && zeros>0) {fprintf(dest,ZEROS"[%d]\n",zeros);}

               /* log the remaining uploaded lines */
               if (debuglevel>=4 && zeros>0) {LogEntry(FuncName,ZEROS"[%d]\n",zeros);}

               fflush(dest);
            }
         }
         
         /* check to see if too few samples were collected in CP mode */
         if (status==Sbe41cpTooFew) {break;}

         /* check for other failures */
         else if (status<=0 && i<(NRetry-1))
         {
            /* log the message */
            LogEntry(FuncName,"Upload[%d] failed [errcode: %d].  Waiting "
                     "3 minutes before retrying.\n",i,status);

            /* pause for retry */
            sleep(180);
         }
      }
   }

   /* check for command prompt and put the SBE41CP back to sleep */
   if (status<0 && chat(&ctdio,"\r","S>",2,"")) Sbe41cpExitCmdMode();

   if (status<=0) {LogEntry(FuncName,"Upload failed [errcode: %d].\n",status);}
   
   else if (debuglevel>=2 || (debugbits&SBE41CP_H))
   {
      /* log the message */
      LogEntry(FuncName,"Continuous profile uploaded [%d lines].\n",n);
   }
   
   return status;
   
   #undef MaxStrLen
}

#endif /* SBE41CP_C */
