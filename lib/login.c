#ifndef LOGIN_H
#define LOGIN_H (0x4000U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define loginChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* function prototypes for external functions */
int login(const struct SerialPort *port, const char *user, const char *pwd);
int logout(const struct SerialPort *port);
int rudics_gw_login(const struct SerialPort *port,const char *imei);

#endif /* LOGIN_H */
#ifdef LOGIN_C
#undef LOGIN_C

#include <cd.h>
#include <ctype.h>
#include <string.h>
#include <chat.h>
#include <expect.h>
#include <logger.h>

/* define the prompt to set on the host computer */
#define CMD "cmd>"

/* define the time-out period for CD detection */
#define CdTimeOut (3.0)

/* function prototypes */
unsigned int sleep(unsigned int seconds);

/**
   This function logs into the MLF2 RUDICS gateway connected via a serial port.

      \begin{verbatim}
      input:
         
         port...A structure that contains pointers to machine dependent
                primitive IO functions.  See the comment section of the
                SerialPort structure for details.  The function checks to be
                sure this pointer is not NULL.

         imei...The Iridium modem IMEI number, this string is transmitted in
                response to the gateway's "irsn: " prompt.

      output:

         This function returns a positive number if the login was
         successful.  Zero is returned if the login failed.  A negative
         number is returned if the function arguments are determined to be
         ill-defined. 
      
      \end{verbatim}
*/
int rudics_gw_login(const struct SerialPort *port,const char *imei)
{
   /* function name for log entries */
   cc *FuncName = "rudics_gw_login()";
   
   /* initialize the return value of this function */
   int status = -1;

   /* validate the serialport */
   if (!port) {LogEntry(FuncName,"Serial port not ready.\n");}
   /* verify IMEI */
   else if (!imei) {LogEntry(FuncName,"NULL pointer to the IMEI.\n");}
   /* validate the serial port's putb() function */
   else if (!port->putb) {LogEntry(FuncName,"NULL putb() function for serial port.\n");}
   else if (expect(port,"irsn: ",imei,10,"\n")<=0)
   {
       LogEntry(FuncName,"IMEI prompt not received.\n");
   }
   else
   {
       LogEntry(FuncName,"RUDICS gateway login successful.\n");
       /* indicate success */
       status=1;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to log into a remote computer via the serial port             */
/*------------------------------------------------------------------------*/
/**
   This function logs into a remote computer connected via a serial port.
   It reads from the serial port searching for the log-in and password
   prompts.  Each prompt is responded to with a user-specified response
   strings.

      \begin{verbatim}
      input:
         
         port...A structure that contains pointers to machine dependent
                primitive IO functions.  See the comment section of the
                SerialPort structure for details.  The function checks to be
                sure this pointer is not NULL.

         user...The string transmitted when responding to the login prompt.

         pwd....The string transmitted when responding to the password prompt.

      output:

         This function returns a positive number if the login was
         successful.  Zero is returned if the login failed.  A negative
         number is returned if the function arguments are determined to be
         ill-defined. 
      
      \end{verbatim}
*/
int login(const struct SerialPort *port,const char *user, const char *pwd)
{
   /* function name for log entries */
   cc *FuncName = "login()";
   
   /* initialize the return value of this function */
   int status = -1;
  
   /* validate the serialport */
   if (!port) {LogEntry(FuncName,"Serial port not ready.\n");}
 
   /* verify user name */
   else if (!user) {LogEntry(FuncName,"NULL pointer to the user name.\n");}

   /* verify the password */
   else if (!pwd) {LogEntry(FuncName,"NULL pointer to the password.\n");}

   /* validate the serial port's putb() function */
   else if (!port->putb) {LogEntry(FuncName,"NULL putb() function for serial port.\n");}

   else
   {
      /* define the timeout period (sec) */
      const time_t timeout=30;

      /* define the login prompt */
      const char *login_prompt="login:";

      /* define the password prompt */
      const char *pwd_prompt="Password:";
      
      /* re-initialize the function's return value */
      status=0;

      /* flush the IO buffers as an initialization step */
      if ((status=pflushio(port))<=0)
      {
         LogEntry(FuncName,"Attempt to flush IO buffers failed.\n");
      }

      else
      {
         int i; char User[32],Pwd[32];

         /* copy and terminate the user and password */
         snprintf(User,sizeof(User)-1,"%s\r",user);
         snprintf(Pwd,sizeof(Pwd)-1,"%s\r",pwd);
         
         /* give 2 chances to login before aborting */
         for (status=0, i=0; status<=0 && i<2; i++)
         {
            /* check if carrier-dectect enabled and CD line not asserted */
            if (Cd(port,CdTimeOut)<=0)
            {
               /* make the logentry */
               LogEntry(FuncName,"No carrier detected.\n");

               break;
            }
      
            /* look for the login prompt and enter the user name */
            if (expect(port,login_prompt,User,timeout,"")<=0)
            {
               /* make the logentry */
               LogEntry(FuncName,"Login prompt not received.\n");
            }
      
            /* look for the password prompt */
            else if (expect(port,pwd_prompt,Pwd,timeout,"")<=0)
            {
               /* make the logentry */
               LogEntry(FuncName,"Password prompt not received.\n");
            }

            /* verify that the login was successful */
            else if (expect(port,"Last login:","",timeout,"\n")<=0)
            {
               /* make the logentry */
               LogEntry(FuncName,"Login failed.\n");
            }
            
            /* re-initialize return value */
            else
            {
               /* make the logentry */
               LogEntry(FuncName,"Login successful.\n");

               /* indicate success */
               status=1;
            }
         }

         /* give 3 chances to reset the command prompt */
         for (i=0; status>0 && i<3; i++)
         {
            /* check if carrier-dectect enabled and CD line not asserted */
            if (Cd(port,CdTimeOut)<=0)
            {
               /* make the logentry */
               LogEntry(FuncName,"No carrier detected.\n");

               /* indicate failure */
               status=0; break;
            }

            if (chat(port,"set prompt = \"" CMD "\"\n",CMD,5,"\n")>0) {sleep(1); break;}
            else {LogEntry(FuncName,"Attempt to set the command prompt failed.\n");}
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to logout of the remote computer                              */
/*------------------------------------------------------------------------*/
/**
   This function logs out of the remote computer attached to the serial
   port.  Unfortunately, there is no way to verify that the logout was
   actually successful.

      \begin{verbatim}
      input:
               
         port...A structure that contains pointers to machine dependent
                primitive IO functions.  See the comment section of the
                SerialPort structure for details.  The function checks to be
                sure this pointer is not NULL.

      output:

         This function returns a positive number if successful.  Zero is
         returned if the attempt failed.  A negative number is returned if
         the function argument was determined to be ill-defined.
      
      \end{verbatim}
*/
int logout(const struct SerialPort *port)
{
   /* function name for log entries */
   cc *FuncName = "logout()";

   /* initialize return value */
   int status = -1;
   
   /* validate the serialport */
   if (!port) {LogEntry(FuncName,"Serial port not ready.\n");}

   /* validate the serial port's pputb() function */
   else if (!port->putb)
   {
      /* make the logentry */
      LogEntry(FuncName,"NULL pputb() function for serial port.\n");
   }
   
   /* write the logout command */
   else
   {
      int i;

      /* define the timeout period (sec) */
      const time_t timeout=5;

      for (status=0,i=0; i<3; i++)
      {
         /* check if carrier-dectect enabled and CD line not asserted */
         if (port->cd && !port->cd())
         {
            /* make the logentry */
            LogEntry(FuncName,"No carrier detected.\n");

            /* indicate failure */
            status=0; break;
         }

         if (sleep(1) || chat(port,"\x18\x18\x18\x18\x18\r~",CMD,timeout,"\n")<=0)
         {
            /* make the logentry */
            LogEntry(FuncName,"Can't get command prompt.\n");
         }
         
         if (chat(port,"exit\r","logout",timeout,"\n")<=0)
         {
            /* make the logentry */
            LogEntry(FuncName,"Attempt to log-out failed.\n");
         }
         else
         {
            /* make the logentry */
            LogEntry(FuncName,"Logout successful.\n");

            /* indicate success */
            status=1; sleep(1); break;
         }
      }

      /* drop the DTR signal */
      if (port->dtr) {port->dtr(0);}
   }
   
   return status;
}

#endif /* LOGIN_C */
