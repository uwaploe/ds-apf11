#ifndef EXTRACT_H
#define EXTRACT_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define extractChangeLog "$RCSfile$  $Revision$  $Date$"

char *extract(const char *source,int index,int n);

#endif /* EXTRACT_H */
#ifdef EXTRACT_C
#undef EXTRACT_C

#include <string.h>

/*========================================================================*/
/*   function to extract a substring from source string                   */
/*========================================================================*/
/**
   This function extracts n characters from the source string starting
   at the index(th) position into a global character buffer "scrbuf".  If
   either index<=0 or n=0 then the function returns the null character
   If (n<0) then the validity check which ensures that the value of
   index is less than the string length is suppressed...this saves time
   since calculation of the string length is not required but opens up
   the possibility that garbage is returned if the user is not careful.
   This function terminates extraction and returns to the calling function
   if the null character is encountered in the  source string.

      Proper usage: character_pointer = extract(source,index,n)
*/
char *extract(const char *source,int index,int n)
{
   #define INFINITE (32767)
   #define MAXSTRLEN (1024)
   static char scrbuf[MAXSTRLEN+1]="";
   int i,len;
   
   /* setting n<0 suppresses time-consuming calculation of string length */
   if (n<0) {len=INFINITE; n *= -1;}
   else len = (index<=1) ? INFINITE : strlen(source);

   if (!source || !(*source) || index>len || index<1 || !n)
   {
      *scrbuf = 0;
      return(scrbuf);
   }

   for (i=0; i<n && i<MAXSTRLEN; i++)
   {
      *(scrbuf+i) = *(source+index+i-1);
      if (!(*(scrbuf+i))) break;
   }
   *(scrbuf+i)=0;

   return(scrbuf);

   #undef INFINITE
}

#endif /* EXTRACT_C */
