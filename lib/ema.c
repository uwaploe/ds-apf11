#ifndef EMA_H
#define EMA_H (0x0200U)

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#include <gmtime.h>
#include <ematypes.h>

#define EMA_BUFFER_EMPTY -10

#define EMA_SAMPLE_WINDOW_SIZE 50
#define EMA_SAMPLE_SLIDE_SIZE 25

#define EMA_SAMPLE_NONE 0
#define EMA_SAMPLE_ASCENT 1
#define EMA_SAMPLE_DESCENT 2
#define EMA_SAMPLE_BOTH 3

int EMAWake(unsigned char* buffer, size_t max);
int EMASleep(unsigned char* buffer, size_t max);

int EMAGetDataFrame(EMADataFrame* frame, unsigned short piston);
int EMAGetAllDataFrames(EMADataFrame frames[], size_t max);
int EMAWriteAllDataFramesToFile(FILE* fp);
void EMAWriteHeaderToASCIIFile(FILE* fp);
int EMAWriteAllDataFramesToASCIIFile(FILE* fp);
int EMADemodulateDataFromFile(FILE* raw, FILE* out, int windowSize, int slideSize);
void EMAPrintDataFrameToString(EMADataFrame* frame, char* buffer, size_t max);

int EMAGetCompassCalibration(EMACompassCalibration* calib);
void EMAPrintCompassCalibrationToString(EMACompassCalibration* calib, char* buffer, size_t max);

#endif /* EMA_H */

#ifdef EMA_C
#undef EMA_C

#include <apf11.h>
#include <apf11ad.h>
#include <apf11com.h>
#include <demodulate.h>
#include <logger.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// note we try to use the Com1 functions directly from afp11com when available, but since
// some functions have static linkage, we use the indirect com1 object in some cases
// it probably makes sense to use the com1 object in all cases if possible

/*
 * Seems we need CRC16_CCITT (a.k.a, CRC-16/KERMIT) and this seems to work but might not
 * be accurate in all cases.  Here's a breakdown of crc algorithms:
 *   https://reveng.sourceforge.io/crc-catalogue/16.htm
 * 
 * And here's where the algorithm came from:
 *   https://stackoverflow.com/questions/17196743/crc-ccitt-implementation#44947877
 * 
 * Returns 0 on success.
 */
static int crc16(unsigned char* ptr, int count) {
	int crc = 0;
	char i;
	
	while(--count >= 0) {
		crc = crc ^ (int) *ptr++ << 8;
		i = 8;
		
		do {
			if(crc & 0x8000) {
				crc = crc << 1 ^ 0x1021;
				
			} else {
				crc = crc << 1;
			}
		} while(--i);
	}
	
	return crc;
}

// do the common initialization on the com port
// returns positive on success
static int EMAComInit(void) {

	// make sure the com port is available
	int status = Com1IsEnable();

	// if not available try to enable it with config
	if (status <= 0) {

		// initialize port by sending in baud rate
		status = Com1Enable(38400);

		if(status <= 0) {
			LogEntry("EMAComInit()", "Com1 port disabled and couldn't be enabled: %d.\n", status);
			return -1;
		}
	}

	// success if we get this far
	return 1;
}

static int EMAToggleRts(void)
{
    const char* fname = "EMAToggleRTS()";

    usleep(2 * 1000);
    if (Com1RtsClear() <= 0)
    {
        LogEntry(fname, "Unable to clear RTS");
        return -1;
    }

    usleep(2 * 1000);
    if (Com1RtsAssert() <= 0)
    {
        LogEntry(fname, "Unable to assert RTS");
        return -1;
    }

    usleep(2 * 1000);
    if (Com1RtsClear() <= 0)
    {
        LogEntry(fname, "Unable to clear RTS");
    }

    usleep(2 * 1000);
    return 1;
}

// Write the command to the EMA board
// Return the amount of bytes read in response from the EMA board.
static int EMAComWrite(unsigned char cmd, unsigned char* buf, size_t expected, bool strict)
{
    const char* fname = "EMAComWrite()";
    const int retries = 2;

    int status = 0;
    int attempts = 0;

    // can't proceed without com
    if (EMAComInit() <= 0)
    {
        return -1;
    }

    while (attempts <= retries)
    {
        status = com1.putb(cmd);
        if (status <= 0)
        {
            LogEntry(fname, "Com1 port write problem: %d.\n", status);
            return -1;
        }

        if (buf != NULL)
        {
            status = pgetbuf(&com1, buf, expected, 2);
            if (status < 0)
            {
                return -1;
            }
            else if (status == expected)
            {
                return 1;
            }
            else if (status == 9)
            {
                // Check for special responses
                if (memcmp(buf + 1, "\033[H\033[J\017E", 8) == 0)
                {
                    LogEntry(fname, "Received the boot message, retrying...\n");
                }
                else if (cmd == 'F')
                {
                    // Could be the EMPTY message, return
                    return 1;
                }
            }
            else if (status > 0 && !strict)
            {
                // If we don't care about how much is being read, just return
                return 1;
            }

            LogEntry(
                fname,
                "Attempt %d: Read %d bytes but expected %d in response to cmd: '%c'\n",
                attempts,
                status,
                expected,
                cmd);
            attempts++;
        }
        else
        {
            LogEntry(fname, "Attempting to flush since no response buffer given.\n");
            return com1.ioflush();
        }
    }

    LogEntry(fname, "Failed to write and receive a command from the EMA after %d retries\n", retries);
    return -1;
}

// wake the ema board from sleep
// the ema board will sleep if not queried after some number of minutes so it needs to be woken up
// before data is available; this is done by setting the EMA IRQ line high for 1ms, or asserting
// the rts line within the apex
// note this shouldn't cause problems if the board is already awake
// returns positive on success
// pass in a non-null string (and max length) to obtain ema response
// max length of response should be about 128 bytes and contains firmware version
// but can also contain other messages
// note if ema already awake then there will be no response
int EMAWake(unsigned char* buffer, size_t max)
{
    const int retries = 2;

	// make sure the com is initialized
	int status = EMAComInit();

	// can't proceed without functioning com
	if (status <= 0) {
		return 0;
	}

    for (int i = 0; i < retries; i++)
    {
        // Attempt to wake the board by setting RTS high
    	status = EMAToggleRts();
        if (status <= 0)
        {
            LogEntry("EMAWake()", "Failed to toggle RTS with the EMA board\n");
            continue;
        }

    	// read from stream if buffer provided
    	if (buffer != NULL)
        {
            status = pgetbuf(&com1, buffer, max - 1, 2);
            if (status < 0)
            {
                LogEntry("EMAWake()", "Failed to read from the Com1 FIFO\n");
                continue;
            }

            buffer[status] = 0;
    	}
        else
        {
            // Give the EM board time to wake and send its start-up message
            usleep(1000 * 1000);
        }

        // Flush the buffer to discard the start-up message, if present
        return com1.ioflush();
    }

    return -1;
}

// put the ema board in a "very low power stop"
// it will need be be awakened if it is put to sleep
// returns positive on success
// pass in a non-null string (and max length) to obtain ema response
// response should say 'STOP1' and other chars max of about 40 bytes
int EMASleep(unsigned char* buffer, size_t max) {

    int status = EMAWake(NULL, -1);
    if (status <= 0)
    {
        return 0;
    }

	// send the sleep signal
	status = EMAComWrite('S', buffer, max, false /* strict */);

	// can't proceed without functioning com
	if (status <= 0) {
		return 0;
	}

	return 1;
}

// returns positive on success
// returns EMA_BUFFER_EMPTY when buffer empty
// otherwise check logs for details
// note the current piston position is passed in because it is typically the same value over a number of frame reads
int EMAGetDataFrame(EMADataFrame* frame, unsigned short piston) {
	const char* fname = "EMAGetDataFrame()";

    int status = 1;
    unsigned char bytes[64];
    status = EMAComWrite('F', bytes, 64, true /* strict */);
    if (status <= 0)
    {
        return -1;
    }

    // empty is normal operation when buffer is exhausted
    if (memcmp(bytes, "F EMPTY\r\n", 9) == 0)
    {
        return EMA_BUFFER_EMPTY;
    }

    // Validate the first byte
    if (bytes[0] != 'F')
    {
        LogEntry(fname, "Expected 'F' but got '%c' as the first byte in the response.\n", bytes[0]);
        return -1;
    }

    unsigned char* data = bytes + 1;

	// validate the crc
	status = crc16(data, 63);
	if(status != 0) {
		LogEntry(fname, "EMA CRC failure: %d.\n", status);
		return -1;
	}

	// and then unpack into the data frame

	// first two are not part of the stream
	frame->timestamp = time(NULL);
	frame->piston = piston;

	frame->age = data[0]; // number of frames still in FIFO
	frame->overflow = data[1] << 8 | data[2];
	frame->seqno = data[3] << 8 | data[4]; // starts at zero and wraps at 2^16-1

	frame->zr_nsum = data[5]; // should be zero
	frame->zr_sum = data[6] << 8 | data[7];
	frame->zr_pk2pk = data[8] << 8 | data[9];

	frame->bt_nsum = data[10]; // battery voltage
	frame->bt_sum = data[11] << 8 | data[12];
	frame->bt_pk2pk = data[13] << 8 | data[14];

	frame->hz_nsum = data[15]; // vertical magnetic field
	frame->hz_sum = data[16] << 8 | data[17];
	frame->hz_pk2pk = data[18] << 8 | data[19];

	frame->hy_nsum = data[20]; // horizontal magnetic field
	frame->hy_sum = data[21] << 8 | data[22];
	frame->hy_pk2pk = data[23] << 8 | data[24];

	frame->hx_nsum = data[25]; // horizontal magnetic field
	frame->hx_sum = data[26] << 8 | data[27];
	frame->hx_pk2pk = data[28] << 8 | data[29];

	frame->az_nsum = data[30]; // vertical acceleration (gravity)
	frame->az_sum = data[31] << 8 | data[32];
	frame->az_pk2pk = data[33] << 8 | data[34];

	frame->ay_nsum = data[35]; // horizontal acceleration
	frame->ay_sum = data[36] << 8 | data[37];
	frame->ay_pk2pk = data[38] << 8 | data[39];

	frame->ax_nsum = data[40]; // horizontal acceleration
	frame->ax_sum = data[41] << 8 | data[42];
	frame->ax_pk2pk = data[43] << 8 | data[44];

	frame->e1_nsum = data[45]; // channel 1 electric field
	frame->e1_sum = data[46] << 24 | data[47] << 16 | data[48] << 8 | data[49];
	frame->e1_pk2pk = data[50] << 16 | data[51] << 8 | data[52];

	frame->e2_nsum = data[53]; // channel 2 electric field
	frame->e2_sum = data[54] << 24 | data[55] << 16 | data[56] << 8 | data[57];
	frame->e2_pk2pk = data[58] << 16 | data[59] << 8 | data[60];

	frame->crc = data[61] << 8 | data[62]; // 16-bit Xmodem CRC

	// if we get this far it was a success
	return 1;
}

// get all the data frames available on the stream
// returns the number of frames captured on success
// generally, the ema board shouldn't be able to hold more than about 30 frames
// returns negative on error, but note zero is allowable
int EMAGetAllDataFrames(EMADataFrame frames[], size_t max) {

	// allow for a couple errors to account for sleep/wake issues
	int errorcount = 0;

	// collect the piston position to be stored with the ema frame data
	unsigned short piston = PistonPositionAdc();

    // Attempt to wake the EMA
    if (EMAWake(NULL, -1) <= 0)
    {
        return -1;
    }

	// grab all of the frames we can given the limit
	int count, status;
	for(count = 0; count < max; count++) {
		status = EMAGetDataFrame(&frames[count], piston);

		// we got a good frame so keep asking
		if(status > 0)
        {
			errorcount = 0;
		}
        else if(status == EMA_BUFFER_EMPTY)
        {
			break;
		}
        else
        {
			errorcount += 1;
			if(errorcount >= 3) {
				LogEntry("EMAGetAllDataFrames()", "Max errors reached when reading frames.\n");

				// some frames could be valid but indicate the problem
				return -1;
			}

            // Sleep a bit in between errors.
            usleep(100 * 1000);
		}
	}

	return count;
}

// get all the available data frames on the stream and write to provided file
// returns the number of frames captured on success, zero included, so negative indicates error
// the file position should be at the end of the file for appending
int EMAWriteAllDataFramesToFile(FILE* fp) {
	// there should never be more than about 25 frames available in the buffer
	EMADataFrame frames[30];

	// grab all of the frames checking for error or empty
	int count = EMAGetAllDataFrames(frames, 30);
	if (count <= 0)
    {
		return count;
	}

	fwrite(frames, sizeof(EMADataFrame), count, fp);
	return count;
}

// write the ascii csv header to the given file
// note it assumes this is at the top of the file
void EMAWriteHeaderToASCIIFile(FILE* fp) {
	 fprintf(
			fp,
			"timestamp,age,overflow,seqno,zr_nsum,zr_sum,zr_pk2pk,bt_nsum,bt_sum,bt_pk2pk,hz_nsum,hz_sum,hz_pk2pk,hy_nsum,hy_sum,hy_pk2pk,hx_nsum,hx_sum,hx_pk2pk,az_nsum,az_sum,az_pk2pk,ay_nsum,ay_sum,ay_pk2pk,ax_nsum,ax_sum,ax_pk2pk,e1_nsum,e1_sum,e1_pk2pk,e2_nsum,e2_sum,e2_pk2pk,crc\n"
		);
}

// this is a variation on the default binary file writer to instead write an ascii record of the raw sample
// returns the number of frames captured on success, zero included, so negative indicates error
// the file position should be at the end of the file for appending
int EMAWriteAllDataFramesToASCIIFile(FILE* fp) {

	// there should never be more than about 25 frames available in the buffer
	EMADataFrame frames[30];
	
	// grab all of the frames checking for error or empty
	int count = EMAGetAllDataFrames(frames, 30);
	if(count <= 0) {
		return count;
	}

	// now print out a line per frame
	for(int i = 0; i < count; i += 1) {
		fprintf(
			fp,
			"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
			frames[i].timestamp,
			frames[i].piston,
			frames[i].age,
			frames[i].overflow,
			frames[i].seqno,
			frames[i].zr_nsum,
			frames[i].zr_sum,
			frames[i].zr_pk2pk,
			frames[i].bt_nsum,
			frames[i].bt_sum,
			frames[i].bt_pk2pk,
			frames[i].hz_nsum,
			frames[i].hz_sum,
			frames[i].hz_pk2pk,
			frames[i].hy_nsum,
			frames[i].hy_sum,
			frames[i].hy_pk2pk,
			frames[i].hx_nsum,
			frames[i].hx_sum,
			frames[i].hx_pk2pk,
			frames[i].az_nsum,
			frames[i].az_sum,
			frames[i].az_pk2pk,
			frames[i].ay_nsum,
			frames[i].ay_sum,
			frames[i].ay_pk2pk,
			frames[i].ax_nsum,
			frames[i].ax_sum,
			frames[i].ax_pk2pk,
			frames[i].e1_nsum,
			frames[i].e1_sum,
			frames[i].e1_pk2pk,
			frames[i].e2_nsum,
			frames[i].e2_sum,
			frames[i].e2_pk2pk,
			frames[i].crc
		);
	}

	return count;
}

// EMA Demodulation Variables.
static EMADataFrame emaframes[EMA_SAMPLE_WINDOW_SIZE];
static EFPDataFrame empro;

// once all of the raw samples have been written to file the data needs to be processed, 
// or demodulated, and written to a second file for telemetry transfer
// the binary raw file position should be at the start and the output file should ready to append for ascii write
// returns the number of demodulated records written
int EMADemodulateDataFromFile(FILE* raw, FILE* out, int windowSize, int slideSize) {
    // Initialize the static variables to make sure that we are not using values
    // from last time.
    memset(&empro, 0, sizeof(EMADataFrame));
    memset(emaframes, 0, sizeof(EMADataFrame) * EMA_SAMPLE_WINDOW_SIZE);

	// total number of demodulated records
	int procount = 0;

	// we start reading a full window of frames, but then read the slide size after that
	int readcount = windowSize;

	// first write the header line
	fprintf(
		out,
		"# vars = demod-window, ROTP_HX,ROTP_HY,PISTON_C0, E1MEAN4,E1SDEV4,E1COEF40,E1COEF41, E2MEAN4,E2SDEV4,E2COEF40,E2COEF41, HX_SDEV,HY_SDEV,BT_MEAN,HZ_MEAN, AX_MEAN,AX_SDEV,AY_MEAN,AY_SDEV, AZ_MEAN,HX_MEAN,HY_MEAN, UXT\n"
	);

	while(1) {
		// read either the full window size or the slide size, but note we want to hit the end of the array with new data
		int count = fread(&emaframes[windowSize - readcount], sizeof(EMADataFrame), readcount, raw);

		// if we happen to read zero then we're immedidately done
		if(count <= 0) {
			break;
		}

		// need to determine our window size based on how many were read and the current state
		int window = 0;

		// if we read as many as we asked for then this is a full window
		if(count == readcount) {
			window = windowSize;
		
		// otherwise it depends on how many were requested
		} else {
			// if this is the first pass then read all we have
			if(readcount == windowSize) {
				window = count;

			// otherwise add what we read to the previous slide
			} else {
				window = slideSize + count;
			}
		}
		
		// do the demodulation
		demodulate_ema(emaframes, window, &empro, 1);

		// write the demodulated record to file
		fprintf(
			out,
			"%d, %5.3f,%5.3f,%.1f, %.1f,%.1f,%.1f,%.1f, %.1f,%.1f,%.1f,%.1f, %.2f,%.2f,%.2f,%.2f, %.2f,%.2f,%.2f,%.2f, %.2f,%.2f,%.2f, %ld\n",
			procount,
			empro.rotp_hx, empro.rotp_hy, empro.piston_c0,
			empro.e1_mean4, empro.e1sdev4, empro.e1coef40, empro.e1coef41,
			empro.e2_mean4, empro.e2sdev4, empro.e2coef40, empro.e2coef41,
			empro.hxdt_sdev, empro.hydt_sdev, empro.bt_mean, empro.hz_mean,
			empro.ax_mean, empro.ax_sdev, empro.ay_mean, empro.ay_sdev,
			empro.az_mean, empro.hx_mean, empro.hy_mean,
			empro.time
		);

		// update that we demodulated a record
		procount += 1;

		// if we didn't have a full window then we're done
		if(window < windowSize) {
			break;
		}

		// otherwise we need to shift the high records to low to perform the slide
		for(int i = slideSize; i < windowSize; i += 1) {
			emaframes[i - slideSize] = emaframes[i];
		}

		// after the initial read we only need to read by the slide size
		readcount = slideSize;
	}

	return procount;
}

void EMAPrintDataFrameToString(EMADataFrame* frame, char* str, size_t max) {
	// how many characters were used for every print
	int count;

	// the current offset position in the string
	int position = 0;

	// how many characters remaining in the buffer
	size_t remaining = max;

	// for every print we substract the number of characters appended and check to see
	// if we've reached our limit; if not update the position and do another print
	// we can't do this in a loop because there doesn't seem to be a way to access struct members

	char timebuf[20];
	strftime(timebuf, sizeof(timebuf), "%Y-%m-%dT%H:%M:%S", Tm(frame->timestamp, 0, NULL));

	count = snprintf((str + position), remaining, "Timestamp: %s\n", timebuf);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "Piston: %d\n", frame->piston);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "Age: %d\n", frame->age);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "OverFlow: %d\n", frame->overflow);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "SeqNo: %d\n", frame->seqno);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "ZR_nsum: %d\n", frame->zr_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "ZR_sum: %d\n", frame->zr_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "ZR_pk2pk: %d\n", frame->zr_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "BT_nsum: %d\n", frame->bt_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "BT_sum: %d\n", frame->bt_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "BT_pk2pk: %d\n", frame->bt_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HZ_nsum: %d\n", frame->hz_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HZ_sum: %d\n", frame->hz_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HZ_pk2pk: %d\n", frame->hz_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HY_nsum: %d\n", frame->hy_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HY_sum: %d\n", frame->hy_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HY_pk2pk: %d\n", frame->hy_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HX_nsum: %d\n", frame->hx_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HX_sum: %d\n", frame->hx_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "HX_pk2pk: %d\n", frame->hx_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AZ_nsum: %d\n", frame->az_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AZ_sum: %d\n", frame->az_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AZ_pk2pk: %d\n", frame->az_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AY_nsum: %d\n", frame->ay_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AY_sum: %d\n", frame->ay_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AY_pk2pk: %d\n", frame->ay_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AX_nsum: %d\n", frame->az_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AX_sum: %d\n", frame->ax_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "AX_pk2pk: %d\n", frame->ax_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E1_nsum: %d\n", frame->e1_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E1_sum: %d\n", frame->e1_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E1_pk2pk: %d\n", frame->e1_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E2_nsum: %d\n", frame->e2_nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E2_sum: %d\n", frame->e2_sum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "E2_pk2pk: %d\n", frame->e2_pk2pk);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "CRC: %d\n", frame->crc);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;
}

int EMAGetCompassCalibration(EMACompassCalibration* calib) {
	const char* fname = "EMAGetCompassCalibration()";

    // Make sure the board is awake.
    if (EMAWake(NULL, -1) <= 0)
    {
        return -1;
    }

    // keep running status
    int status = 1;
    unsigned char bytes[117];
    if (EMAComWrite('M', bytes, 117, true /* strict */) <= 0)
    {
        return -1;
    }

    if (bytes[0] != 'M' || bytes[115] != '\r' || bytes[116] != '\n')
    {
        LogEntry(fname, "Did not get expected response\n");
        bytes[115] = '\n';
        bytes[116] = 0;
        LogEntry(fname, "Response: \t%s\n", (const char*)bytes);
        return -1;
    }

	// we got the 116 bytes so now we need to convert them to numbers
	// note the last two characters are CR/LF and can be ignored
	// otherwise, every two bytes are hex characters to be converted to a single numeric byte
	// the result should be 57 total bytes (i.e., 114 / 2)
	unsigned char data[57];

	// we know these are characters so no problem converting between signed and unsigned
	// this is to hold the hex string, e.g., 'A' '1' '\n'
	// this can probably fail but the crc will catch it
	// TODO: probably easier to just do this with math
	char hex[3];
	for(int i = 1; i < 115; i += 2) {
		hex[0] = (char) bytes[i];
		hex[1] = (char) bytes[i + 1];
		hex[2] = 0;
		
		data[i / 2] = (unsigned char) strtol(hex, NULL, 16);
	}

	// validate the crc
	status = crc16(data, 57);
	if(status != 0) {
		LogEntry(fname, "EMA CRC failure: %d.\n", status);
		return -1;
	}

	calib->timestamp = time(NULL);

	int nsum = data[0];
	
	calib->nsum = nsum;

	calib->hzavg_bef = (float)(data[1] << 8 | data[2]) / (float)nsum;
	calib->hyavg_bef = (float)(data[3] << 8 | data[4]) / (float)nsum;
	calib->hxavg_bef = (float)(data[5] << 8 | data[6]) / (float)nsum;
	calib->hzavg_dur = (float)(data[7] << 8 | data[8]) / (float)nsum;
	calib->hyavg_dur = (float)(data[9] << 8 | data[10]) / (float)nsum;
	calib->hxavg_dur = (float)(data[11] << 8 | data[12]) / (float)nsum;
	calib->hzavg_aft = (float)(data[13] << 8 | data[14]) / (float)nsum;
	calib->hyavg_aft = (float)(data[15] << 8 | data[16]) / (float)nsum;
	calib->hxavg_aft = (float)(data[17] << 8 | data[18]) / (float)nsum;

	calib->hzmax_bef = data[19] << 8 | data[20];
	calib->hymax_bef = data[21] << 8 | data[22];
	calib->hxmax_bef = data[23] << 8 | data[24];
	calib->hzmax_dur = data[25] << 8 | data[26];
	calib->hymax_dur = data[27] << 8 | data[28];
	calib->hxmax_dur = data[29] << 8 | data[30];
	calib->hzmax_aft = data[31] << 8 | data[32];
	calib->hymax_aft = data[33] << 8 | data[34];
	calib->hxmax_aft = data[35] << 8 | data[36];

	calib->hzmin_bef = data[37] << 8 | data[38];
	calib->hymin_bef = data[39] << 8 | data[40];
	calib->hxmin_bef = data[41] << 8 | data[42];
	calib->hzmin_dur = data[43] << 8 | data[44];
	calib->hymin_dur = data[45] << 8 | data[46];
	calib->hxmin_dur = data[47] << 8 | data[48];
	calib->hzmin_aft = data[49] << 8 | data[50];
	calib->hymin_aft = data[51] << 8 | data[52];
	calib->hxmin_aft = data[53] << 8 | data[54];

	calib->crc = data[55] << 8 | data[56];
	
	// success if we get this far
	return 1;
}

void EMAPrintCompassCalibrationToString(EMACompassCalibration* calib, char* str, size_t max) {
	// see data frame printing for details

	int count;
	int position = 0;
	size_t remaining = max;

	char timebuf[20];
	strftime(timebuf, sizeof(timebuf), "%Y-%m-%dT%H:%M:%S", Tm(calib->timestamp, 0, NULL));

	count = snprintf((str + position), remaining, "Timestamp: %s\n", timebuf);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "nsum: %d\n", calib->nsum);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzavg_bef: %.3f\n", calib->hzavg_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hyavg_bef: %.3f\n", calib->hyavg_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxavg_bef: %.3f\n", calib->hxavg_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzavg_dur: %.3f\n", calib->hzavg_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hyavg_dur: %.3f\n", calib->hyavg_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxavg_dur: %.3f\n", calib->hxavg_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzavg_aft: %.3f\n", calib->hzavg_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hyavg_aft: %.3f\n", calib->hyavg_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxavg_aft: %.3f\n", calib->hxavg_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmax_bef: %d\n", calib->hzmax_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymax_bef: %d\n", calib->hymax_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmax_bef: %d\n", calib->hxmax_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmax_dur: %d\n", calib->hzmax_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymax_dur: %d\n", calib->hymax_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmax_dur: %d\n", calib->hxmax_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmax_aft: %d\n", calib->hzmax_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymax_aft: %d\n", calib->hymax_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmax_aft: %d\n", calib->hxmax_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmin_bef: %d\n", calib->hzmin_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymin_bef: %d\n", calib->hymin_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmin_bef: %d\n", calib->hxmin_bef);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmin_dur: %d\n", calib->hzmin_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymin_dur: %d\n", calib->hymin_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmin_dur: %d\n", calib->hxmin_dur);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hzmin_aft: %d\n", calib->hzmin_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hymin_aft: %d\n", calib->hymin_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "hxmin_aft: %d\n", calib->hxmin_aft);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;

	count = snprintf((str + position), remaining, "crc: %d\n", calib->crc);
	remaining -= count;
	if(remaining <= 0) {
		return;
	}
	position += count;
}

#endif /* EMA_C */
