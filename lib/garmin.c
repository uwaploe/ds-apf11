#ifndef GARMIN_H
#define GARMIN_H (0x8000U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define garminChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* function prototypes for global functions */
int    ConfigGarmin15(const struct SerialPort *port);
int    DisableNmeaSentence(const struct SerialPort *port,const char *id);
int    EnableNmeaSentence(const struct SerialPort *port,const char *id);
int    Garmin15Cmds(const struct SerialPort *port,const char *fname);
int    GpsAlmanacInitialize(const struct SerialPort *port);
int    GpsAlmanacRefresh(const struct SerialPort *port);
int    GpsAlmanacReport(const struct SerialPort *port);
time_t GpsAlmanacUpLoad(const struct SerialPort *port);
int    LogNmeaSentences(const struct SerialPort *port);

#endif /* GARMIN_H */
#ifdef GARMIN_C
#undef GARMIN_C

#include <assert.h>
#include <extract.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <logger.h>
#include <nmea.h>
#include <regex.h>
#include <stdlib.h>
#include <unistd.h>

/* function prototypes for local functions */
static int pgrmc(const struct SerialPort *port);
static int pgrmc1(const struct SerialPort *port);
static int pgrmo(const struct SerialPort *port,const char *msg,int mode);

/* prototypes for functions with external linkage */
size_t GpsFifoLen(void);
size_t GpsFifoSize(void);
int    GpsDisable(void);
int    GpsEnable(long int BaudRate);
int    MagSwitchReset(void);
int    MagSwitchToggled(void);
int    Wait(unsigned long int millisec);
int    WatchDogPet(void);

/*------------------------------------------------------------------------*/
/* function to configure the Garmin GPS15 gps engine                      */
/*------------------------------------------------------------------------*/
/**
   This function configures the Garmin GPS15 OEM gps engine.  It does so by
   sending the PGRMC, PGRMC1, and PGRMO configuration strings to the gps
   engine.  For details refer to the technical specification: Garmin GPS 15xH
   \& 15xL Technical Specifications (Document No. 190-00266-03, Rev. A, Dec
   2009).

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an invalid function argument.
      \end{verbatim}

   written by Dana Swift
*/
int ConfigGarmin15(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "ConfigGarmin15()";

   /* initialize this function's return value */
   int i,status=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers");
   }

   /* configure the Garmin GPS15 using the PGRMC and PGRMC1 messages */
   else
   {
      /* make a log entry if the verbosity is high enough */
      if (debuglevel>=2)
      {
         LogEntry(FuncName,"Garmin GPS15 configuration initiated.\n");
      }

      for (status=0,i=0; i<3; i++)
      {
         /* disable all output */
         if (pgrmo(port,"",2)<=0)
         {
            LogEntry(FuncName,"Attempt to disable all NMEA sentences failed.\n");
         }
         else
         {
            /* pause for a second and then flush the IO queues */
            Wait(5000); port->ioflush();

            if (!GpsFifoLen()) {status=1; break;}
            else {LogEntry(FuncName,"Attempt to suspend output failed-retrying.\n");}
         }
      }

      /* configure the fix mode and baud rate */
      if (pgrmc(port)<=0)
      {
         LogEntry(FuncName,"Configuration of fix-mode and baud rate failed.\n");

         /* indicate failure */
         status=0;
      }

      /* configure the NMEA output frequency */
      else if (pgrmc1(port)<=0)
      {
         LogEntry(FuncName,"Configuration of NMEA output frequency failed.\n");

         /* indicate failure */
         status=0;
      }

      /* enable default NMEA messages */
      else if (pgrmo(port,"",4)<=0)
      {
         LogEntry(FuncName,"Attempt to enable default NMEA sentences failed.\n");

         /* indicate failure */
         status=0;
      }

      /* enable NMEA GGA sentence */
      else if (pgrmo(port,"GPGGA",1)<=0)
      {
         LogEntry(FuncName,"Attempt to enable NMEA GGA sentence failed.\n");

         /* indicate failure */
         status=0;
      }

      /* enable NMEA RMC sentence */
      else if (pgrmo(port,"GPRMC",1)<=0)
      {
         LogEntry(FuncName,"Attempt to enable NMEA RMC sentence failed.\n");

         /* indicate failure */
         status=0;
      }

      /* enable NMEA RMT sentence */
      else if (pgrmo(port,"PGRMT",1)<=0)
      {
         LogEntry(FuncName,"Attempt to enable PGRMT sentence failed.\n");

         /* indicate failure */
         status=0;
      }

      /* indicate success */
      else {status=1;}
   }

   /* make a log entry if the configuration failed */
   if (status<=0) {LogEntry(FuncName,"Garmin GPS15 configuration failed.\n");}

   /* make a log entry if verbosity high enough */
   else if (debuglevel>=2)
   {
      LogEntry(FuncName,"Garmin GPS15 successfully configured.\n");
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable a specific NMEA sentence                           */
/*------------------------------------------------------------------------*/
int DisableNmeaSentence(const struct SerialPort *port,const char *id)
{
   return pgrmo(port,id,0);
}

/*------------------------------------------------------------------------*/
/* function to enable a specific NMEA sentence                            */
/*------------------------------------------------------------------------*/
int EnableNmeaSentence(const struct SerialPort *port,const char *id)
{
   return pgrmo(port,id,1);
}

/*------------------------------------------------------------------------*/
/* function to write garmin commands to the GPS15                         */
/*------------------------------------------------------------------------*/
/**
   This function reads the Garmin commands in a file and writes them
   to the serial port connected to a Garmin GPS15.  Lines containing
   only whitespace are ignored as are comments (ie., lines whose first
   non-whitespace character is '#').

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

         fname......The name of the file containing the Garmin
                    commands; one command per line.  Comments start
                    with a '#' character but may be preceded by
                    arbitrary whitespace.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an exception was encountered.
      \end{verbatim}
*/
int Garmin15Cmds(const struct SerialPort *port,const char *fname)
{
    /* define the logging signature */
   cc *FuncName = "Garmin15Cmds()";

   /* initialize the return value */
   int n,len,status=1;

   /* local stack-based objects */
   char cmd[MaxNmeaStrLen+1]; FILE *source=NULL;

   /* validate the SerialPort object */
   if (!port || !fname) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   /* open the file */
   else if (!(source=fopen(fname,"r")))
   {
      LogEntry(FuncName,"Open failed: %s\n",fname); status=0;
   }

   /* loop over all of the commands in the file */
   else while (fgets(cmd,sizeof(cmd),source))
   {
      /* ignore lines with nothing but whitespace */
      if ((n=strspn(cmd," \t\r\n"))==(len=strlen(cmd))) {continue;}

      /* ignore comments */
      else if (cmd[n-1]=='#')  {continue;}

      /* prune the LF at the end of the line */
      else if (cmd[len-1]=='\n') {cmd[len-1]=0;}

      /* write the command to the serial port */
      pputs(port,cmd,2,"\r\n");

      /* add the command to the engineering logs */
      LogEntry(FuncName,"%s\n",cmd); Wait(250);
   }

   /* close the command file */
   if (source) {fclose(source);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to reinitialize the almanac's GPS week                        */
/*------------------------------------------------------------------------*/
/**
   This function reads the almanac from the Garmin GPS15, resets the
   GPS week to equal 1, and writes the synthesized almanac back to the
   GPS15.

   The objective is to work-around a garmin firmware bug that is
   triggered by the GPS-week rollover that happened on April 6, 2019.
   For almanac elements received from satellites, the garmin firmware
   allows updates to eeprom only if the GPS week is greater than data
   in the eeprom.  After the rollover, the GPS week from satellites is
   smaller than that stored in eeprom and so the garmin fimrware
   (incorrectly) rejects new almanac elements.  This causes the stored
   almanac to remain static and suffer aging.  Eventually, the GPS15
   will no longer be able to track satellites and so will not be able
   to acquire a fix.

   By synthesizing a new almanac with GPS week equal to 1 then the
   garmin firmware bug is avoided until the GPS week rolls-over again
   in the year 2038.

      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine
                    dependent primitive IO functions.  See the comment
                    section of the SerialPort structure for details.
                    The function checks to be sure this pointer is not
                    NULL.

      output:
         This function returns a positive value if successful, zero
         indicates failure, and -1 indicates an exception was
         encountered.
      \end{verbatim}
*/
int GpsAlmanacInitialize(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "GpsAlmanacInitialize()";

   /* initialize the return value */
   int err,status=0;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n"); status=-1;}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n"); status=-1;
   }

   /* disable all NMEA sentences prior to reinitializing the almanac */
   else if ((err=pgrmo(port,"",2))<=0)
   {
      LogEntry(FuncName,"Attempt to disable NMEA sentences failed. [err=%d]\n",err);
   }

   else
   {
      /* create buffers for receiving and sending garmin strings */
      char msg[MaxNmeaStrLen+1],cmd[MaxNmeaStrLen+1];

      /* define the number of subexpressions in the regex */
      #define NSUB (3)

      /* define regex objects needed to match the pattern */
      regex_t regex; regmatch_t regs[NSUB+1];

      /* construct the regex pattern to isolate the GPS week (ie., 4th field) */
      const char *pattern = "^.*(\\$GPALM,[0-9]*,[0-9]*,[0-9]*,)([0-9]*)(,[^*]+)\\*";

      /* compile the option pattern */
      assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

      /* protect against segfaults */
      assert(NSUB==regex.re_nsub);

      /* flush the gps serial port buffers */
      port->ioflush();

      /* execute the garmin command to display the almanac */
      pputs(port,"$PGRMO,GPALM,1",3,"\r\n"); sleep(10);

      /* read each almanac string from the GPS input buffer */
      for (status=1; pgets(port,msg,sizeof(msg),2,"\r\n")>0;)
      {
         /* quietly ignore the echo of the PGRMO command */
         if (strstr(msg,"$PGRMO,GPALM,1")) {continue;}

         /* check the current garmin string satisfies the regex */
         else if (!(err=regexec(&regex,msg,regex.re_nsub+1,regs,0)))
         {
            /* extract the portion of the almanac entry before the GPS week */
            strncpy(cmd,msg+regs[1].rm_so,regs[1].rm_eo-regs[1].rm_so);

            /* terminate the leading substring and reinitizliate the GPS week to 1 */
            cmd[regs[1].rm_eo-regs[1].rm_so]=0; strcat(cmd,"1");

            /* append the trailing substring of the almanac element */
            strncat(cmd,msg+regs[3].rm_so,regs[3].rm_eo-regs[3].rm_so);

            /* configure the GPS with reinitialized almanac element */
            pputs(port,cmd,3,"\r\n"); LogEntry(FuncName,"%s\n",cmd); Wait(100);
         }

         /* warn about strings that fail regex for almanac element */
         else {LogEntry(FuncName,"Warning, regex failure: %s\n",msg); status=0;}
      }

      /* reset the default configuration */
      if (ConfigGarmin15(port)<=0) {status=0;}

      /* clean-up the regex */
      regfree(&regex);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to refresh the Garmin GPS15 almanac                           */
/*------------------------------------------------------------------------*/
/**
   This function refreshes the GPS15 almanac by first initializing gps
   with a synthesized almanac for GPS-week one and then allowing this
   almanac to be updated by satellites.  To successfully refresh the
   almanac, the gps antenna must be able to see the sky so that the
   new almanac can be received from GPS satellites.  After
   initializing the almanac, this function monitors the progress of
   the GPS15 almanac update.  This function exits when either the
   time-out expires or else when all available almanac elements have
   been updated.

      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine
                    dependent primitive IO functions.  See the comment
                    section of the SerialPort structure for details.
                    The function checks to be sure this pointer is not
                    NULL.

      output:
         This function returns a positive value if successful, zero
         indicates failure, and -1 indicates an exception was
         encountered.
      \end{verbatim}
*/
int GpsAlmanacRefresh(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "GpsAlmanacRefresh()";

   /* define number of seconds in a minute */
   #define Min (60L)

   /* initialize the return value */
   int err,status=0;

   /* define objects needed to implement a timeout and pause features */
   time_t pause,To,Tref=time(NULL); const time_t TimeOut=3600;

   LogEntry(FuncName,"Executing procedure to refresh the GPS15 almanac.\n");

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n"); status=-1;}

   /* power-cycle the GPS15 */
   else if ((err=GpsDisable())<=0 || sleep(10) || (err=GpsEnable(4800))<=0)
   {
      LogEntry(FuncName,"GPS15 power-cycle failed. [err=%d]\n",err);
   }

   /* reinitialize the GPS15 almanac */
   else if ((err=GpsAlmanacInitialize(port))<=0)
   {
      LogEntry(FuncName,"Attempt to initialize GPS15 "
               "almanac failed. [err=%d]\n",err);
   }

   /* power-cycle the GPS15 */
   else if ((err=GpsDisable())<=0 || sleep(10) || (err=GpsEnable(4800))<=0)
   {
      LogEntry(FuncName,"GPS15 power-cycle failed. [err=%d]\n",err);
   }

   /* report the current state of the almanac */
   else if ((err=GpsAlmanacReport(port))<=0)
   {
      LogEntry(FuncName,"GPS15 almanac report failed. [err=%d]\n",err);
   }

   /* disable all NMEA sentences prior to monitoring the almanac's update */
   else if ((err=pgrmo(port,"",2))<=0)
   {
      LogEntry(FuncName,"Attempt to disable NMEA "
               "sentences failed. [err=%d]\n",err);
   }

   /* power-cycle the GPS15 */
   else if ((err=GpsDisable())<=0 || sleep(10) || (err=GpsEnable(4800))<=0)
   {
      LogEntry(FuncName,"GPS15 power-cycle failed. [err=%d]\n",err);
   }

   else
   {
      /* create buffer for receiving garmin strings */
      char msg[MaxNmeaStrLen+1];

      /* define the number of subexpressions in the regex */
      #define NSUB (3)

      /* define regex objects needed to match the pattern */
      regex_t regex; regmatch_t regs[NSUB+1];

      /* construct the regex pattern to isolate the GPS week (ie., 4th field) */
      const char *pattern = "^.*(\\$GPALM,[0-9]*,[0-9]*,[0-9]*,)([0-9]*)(,[^*]+)\\*";

      /* compile the option pattern */
      assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

      /* protect against segfaults */
      assert(NSUB==regex.re_nsub);

      /* initialize the mag-switch flip-flop */
      MagSwitchReset();

      /* loop to monitor the state of the updated almanac */
      for (pause=15,Tref=time(NULL); difftime(time(NULL),Tref)<TimeOut; pause=5)
      {
         /* announce the sleep-period */
         LogEntry(FuncName,"Pausing for %d minutes to download "
                  "the almanac from satellites.\n",pause);

         /* pet the watchdog */
         WatchDogPet();

         /* loop to implement time-out with abort via magswitch */
         for (To=time(NULL); difftime(time(NULL),To)<pause*Min;)
         {
            /* check for abort request via magswitch */
            if (MagSwitchToggled()) {break;} else {sleep(2);}
         }

         /* check for abort request via magswitch */
         if (MagSwitchToggled()) {status=0; break;}

         /* flush the gps serial port buffers */
         port->ioflush();

         /* execute the garmin command to display the almanac */
         pputs(port,"$PGRMO,GPALM,1",3,"\r\n"); sleep(10);

         /* read each almanac string from the GPS input buffer */
         for (status=1; pgets(port,msg,sizeof(msg),2,"\r\n")>0;)
         {
            /* quietly ignore the echo of the PGRMO command */
            if (strstr(msg,"$PGRMO,GPALM,1")) {continue;}

            /* check the current garmin string satisfies the regex */
            else if (!(err=regexec(&regex,msg,regex.re_nsub+1,regs,0)))
            {
               /* extract the GPS week from the current element of the almanac */
               int GpsWeek = atoi(extract(msg,regs[2].rm_so+1,regs[2].rm_eo-regs[2].rm_so));

               /* check if the current almanac element has been updated */
               if (GpsWeek==1) {status=0;}

               /* make a logentry of the current almanac element */
               LogEntry(FuncName,"%5s: %s\n",((GpsWeek==1)?"stale":"ok"),msg);
            }

            /* warn about strings that fail regex for almanac element */
            else {LogEntry(FuncName,"Warning, regex failure: %s\n",msg); status=0;}
         }

         /* check if the almanac has been fully refreshed */
         if (status>0) break;
      }

      /* clean-up the regex */
      regfree(&regex);
   }

   /* reset the default configuration */
   if (ConfigGarmin15(port)<=0) {status=0;}

   /* report success or failure of the almanac refresh attempt */
   LogEntry(FuncName,"GPS15 almanac refresh %s after %d minutes. [status=%d]\n",
            ((status>0)?"successful":"failed"),(int)(difftime(time(NULL),Tref)/Min),status);

   return status;
   #undef Min
}

/*------------------------------------------------------------------------*/
/* function to report the current state of the GPS15 almanac              */
/*------------------------------------------------------------------------*/
/**
   This function queries the GPS15 for the current state of its
   almanac and writes it into the engineering logs.

      \begin{verbatim}
      input:
         port.......A structure that contains pointers to machine
                    dependent primitive IO functions.  See the comment
                    section of the SerialPort structure for details.
                    The function checks to be sure this pointer is not
                    NULL.

      output:
         This function returns a positive value if successful, zero
         indicates failure, and -1 indicates an exception was
         encountered.
      \end{verbatim}
*/
int GpsAlmanacReport(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "GpsAlmanacReport()";

   /* initialize the return value */
   int err,status=0;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n"); status=-1;}

   /* disable all NMEA sentences prior to querying the GPS15 for its almanac */
   else if ((err=pgrmo(port,"",2))<=0)
   {
      LogEntry(FuncName,"Attempt to disable NMEA "
               "sentences failed. [err=%d]\n",err);
   }

   else
   {
      /* create buffer for receiving garmin strings */
      char msg[MaxNmeaStrLen+1];

      /* flush the gps serial port buffers */
      port->ioflush();

      /* write a header */
      LogEntry(FuncName,"Current state of GPS15 almanac:\n");

      /* execute the garmin command to display the almanac */
      pputs(port,"$PGRMO,GPALM,1",3,"\r\n"); sleep(10);

      /* read each almanac string from the GPS input buffer */
      while (pgets(port,msg,sizeof(msg),4,"\r\n")>0)
      {
         /* quietly ignore the echo of the PGRMO command */
         if (strstr(msg,"$PGRMO,GPALM,1")) {continue;}

         /* check the current garmin string satisfies the search pattern */
         else if (strstr(msg,"$GPALM"))
         {
            /* make a logentry of the current almanac element */
            LogEntry(FuncName,"%s\n",msg); status=1;
         }
      }
   }

   /* reset the default configuration */
   if (ConfigGarmin15(port)<=0) {status=0;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to upload the almanac                                         */
/*------------------------------------------------------------------------*/
/**
   This function allows time for the almanac to be uploaded into the GPS.
   It does nothing except pause for 15 minutes while the almanac is
   uploaded.  If successful, this function returns the number of seconds
   spent waiting for the upload.  Otherwise, this function returns a zero or
   negative value.
*/
time_t GpsAlmanacUpLoad(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "GpsAlmanacUpLoad()";

   /* initialize the return value */
   time_t AwakeTime=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n");
   }

   else
   {
      /* record the starting time */
      time_t To=time(NULL);

      /* set the length of the pause-period */
      AwakeTime=900;

      if (debuglevel>=2) {LogEntry(FuncName,"Pausing for %ld minutes to upload "
                                   "GPS almanac.\n",AwakeTime/60);}

      /* initialize the mag-switch flip-flop */
      MagSwitchReset();

      do
      {
         /* check if NMEA strings are to be logged */
         if (debuglevel>=3)
         {
            /* define a buffer to hold the NMEA strings read from the GPS engine */
            char buf[MaxNmeaStrLen+1];

            /* read the next string from the port buffer */
            if (pgets(port,buf,sizeof(buf)-1,12,"\r\n")>0)
            {
               LogEntry(FuncName,"%s\n",buf);
            }

            /* check if buffer overflow is imminent */
            if (GpsFifoLen()>(0.9*GpsFifoSize()))
            {
               /* flush and re-sync the buffer */
               port->ioflush(); pgets(port,buf,sizeof(buf),2,"\r\n");
            }
         }

         /* logging disabled so pause for a second before continuing */
         else sleep(1);
      }
      while (difftime(time(NULL),To)<AwakeTime && !MagSwitchToggled());

      if (debuglevel>=2) {LogEntry(FuncName,"Pause complete.\n");}

      /* flush the GPS IO buffers */
      port->ioflush();
   }

   /* initialize the mag-switch flip-flop */
   MagSwitchReset();

   return AwakeTime;
}

/*------------------------------------------------------------------------*/
/* function to log NEMA sentences output from the Garmin GPS15            */
/*------------------------------------------------------------------------*/
/**
   This function was created as a way of providing information that could be
   used to monitor the health and efficacy of the antenna and GPS engine.
   It receives NMEA sentences from the GPS engine and writes them to the log
   file.  For more information on the NMEA sentences, refer to the Garmin
   documentation: Garmin GPS 15xH \& 15xL Technical Specifications (Document
   No. 190-00266-03, Rev. A, Dec 2009)

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an invalid function argument.
      \end{verbatim}

   written by Dana Swift
*/
int LogNmeaSentences(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "LogNmeaSentences()";

   /* initialize this function's return value */
   int status=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n");
   }

   else
   {
      /* set the time-out periods for receiving a NMEA sentence from GPS engine */
      const time_t TimeOut=12, MinWatchTime=30, MaxWatchTime=90;

         /* define objects to store the reference time and watch-time */
      time_t To; float dT; int n;

      /* define a buffer to hold the NMEA strings read from the GPS engine */
      char buf[MaxNmeaStrLen+1];

      /* reinitialize the function's return value */
      status=0;

      /* flush the IO port */
      port->ioflush();

      /* initialize watch-time variables and monitor fail-safe exit criteria */
      for (n=0, To=time(NULL), dT=0; dT<=MaxWatchTime && n<65; n++)
      {
         /* read a string from the serial port */
         if (pgets(port,buf,MaxNmeaStrLen,TimeOut,"\r\n")>0)
         {
            /* compute the length of the string */
            int i,len=strlen(buf);

            /* timestamp the log entry */
            LogEntry(FuncName,"%s","");

            /* write the string to the log */
            for (i=0; i<len; i++)
            {
               if (buf[i]=='\r') LogAdd("\\r");
               else if (buf[i]=='\n') LogAdd("\\n");
               else if (isprint((unsigned char)(buf[i]))) LogAdd("%c",buf[i]);
               else LogAdd("[0x%02x]",buf[i]);
            }

            /* terminate the log entry */
            LogAdd("\n");
         }

         /* check if the Rx buffer is in danger of overflowing */
         if (GpsFifoLen()>0.9*GpsFifoSize())
         {
            /* empty the buffer and resync to the output */
            port->iflush(); pgets(port,buf,MaxNmeaStrLen,TimeOut,"\r\n");
         }

         /* compute the length of the logging period so far */
         dT=difftime(time(NULL),To);

         /* check if the sensor status sentence was received */
         if (!strncmp(buf,"$PGRMT",6)) {status=1;}

         /* check exit criteria */
         if (status && dT>MinWatchTime) break;
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Garmin GPS15 gps engine                      */
/*------------------------------------------------------------------------*/
/**
   This function configures the fix mode, altitude, earth-datum,
   differential mode, baud rate, velocity filter, PPS mode, and
   dead-reckoning time for the Garmin GPS15 gps engine.  The following
   information was taken from page 18 of the technical specification: Garmin
   GPS 15xH \& 15xL Technical Specifications (Document No. 190-00266-03,
   Rev. A, Dec 2009)

      \begin{verbatim}
      $PGRMC,<1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,<10>,<11>,<13>,<13>,<14>*HH\r\n

       <1> Fix mode, A=Automatic, 2=2D exclusively (host system must supply
              altitude).
       <2> Altitude above/below mean sea level, -1500.0 to 18000.0 meters.
       <3> Earth datim index.  Refer to technical specification for details.
       <4> User earth datum semi-mafor axis.  Refer to technical
              specification for details.
       <5> User earth datum inverse flattening factor.  Refer to technical
              specification for details.
       <6> User earth datum delta x earth centered coordinate.
       <7> User earth datum delta y earth centered coordinate.
       <8> User earth datum delta z earth centered coordinate.
       <9> Differential mode, A=Automatic (output DGPS data when available,
              non-DGPS otherwise), D=differential exclusively (output only
              differential fixes).
      <10> NMEA baud rate, 1=1200, 2=2400, 3=4800, 4=9600, 5=19200, 6=300,
              7=600.
      <11> Velocity filter, 0 = no filter, 1 = automatic filter, 2-255 =
              filter time constant (10 = 10 second filter).
      <12> PPS mode, 1=No PPS, 2 = 1Hz.
      <13> PPS pulse lenth, 0-48 = (n+1)*20msec. Example n=4 => 100 msec
              pulse.
      <14> Dead reckoning valid time 1-30 seconds.

        HH Hexidecimal checksum.
      \end{verbatim}

   This function does not implement a parameterized configuration ability.
   The configuration string is hardwired into the source code.  Note also
   that the configuration changes are not effective until the next power-up
   cycle or external reset of the gps engine.

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an invalid function argument.
      \end{verbatim}

   written by Dana Swift
*/
static int pgrmc(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "pgrmc()";

   /* initialize this function's return value */
   int status=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n");
   }

   else
   {
      /* specify the timeout period (seconds) for serial puts and gets */
      const time_t TimeOut=5;

      /* specify the configuration command */
      const char *cmd = "PGRMC,A,,,,,,,,A,3,1,2,4,30";

      /* compute the NMEA checksum of the configuration command */
      unsigned char chksum = nmea_checksum(cmd,0,strlen(cmd));

      /* create the NMEA configuration command */
      char str[MaxNmeaStrLen+1],buf[MaxNmeaStrLen+1]; sprintf(buf,"$%s*%02X",cmd,chksum);

      /* reinitialize return value */
      status=0;

      /* make a log entry about writing the config string to the GPS engine */
      if (debuglevel>=3) {LogEntry(FuncName,"Configuring Garmin "
                                   "GPS engine: %s\n",buf);}

      /* flush the IO port */
      port->ioflush();

      /* write the configuration command to the Garmin GPS engine */
      if (pputs(port,buf,2,"\r\n")<=0)
      {
         LogEntry(FuncName,"Attempt to write configuration "
                  "string failed: %s\n",buf);
      }

      /* read the configuration echo */
      else
      {
         /* set the reference for the timeout period */
         time_t T0=time(NULL);

         /* read strings from the GPS engine until the PGRMC message is found */
         do
         {
            /* read a string from the GPS serial port */
            pgets(port,str,sizeof(str)-1,5,"\r\n");

            /* check if current string is the PGRMC message */
            if (strstr(str,cmd)) status=1;

            if (debuglevel>=3)
            {
               LogEntry(FuncName,"%s","");
               LogAdd("%s %s\n",((status)?"Accept:":"Reject:"),str);
            }
         }

         /* keep reading strings until PGRMC message found or timeout expires */
         while (status<=0 && difftime(time(NULL),T0)<TimeOut);

         /* if status is false then the config attempt failed */
         if (!status) {LogEntry(FuncName,"Attempt to configure Garmin GPS15 failed.\n");}

         /* make a log entry if the verbosity is high enough */
         else if (debuglevel>=3)
         {
            LogEntry(FuncName,"Garmin GPS15 configured: %s\n",str);
         }
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Garmin GPS15 gps engine                      */
/*------------------------------------------------------------------------*/
/**
   This function configures the NMEA output rate, binary phase output mode,
   position pinning, DGPS config parameters, and NMEA mode.  The following
   information was taken from page 18 of the technical specification: Garmin
   GPS 15xH \& 15xL Technical Specifications (Document No. 190-00266-03,
   Rev. A, Dec 2009).

      \begin{verbatim}
      $PGRMC1,<1>,<2>,<3>,<4>,<5>,<6>,<7>*HH\r\n

       <1> NMEA output time: 1-900 seconds.
       <2> Binary phase output data, 1=off, 2=on.
       <3> Position pinning, 1=off, 2=on.
       <4> DGPS beacon frequency. Refer to technical specification for
              details.
       <5> DGPS beacon rate. Refer to technical specification for details.
       <6> DGPS beacon auto tune on station loss, 1=off, 2=on.
       <7> Activate NMEA 2.30 mode indicator, 1=off, 2=on.

        HH Hexidecimal checksum.
      \end{verbatim}

   This function does not implement a parameterized configuration ability.
   The configuration string is hardwired into the source code.

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an invalid function argument.
      \end{verbatim}

   written by Dana Swift
*/
static int pgrmc1(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "pgrmc1()";

   /* initialize this function's return value */
   int status=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n");
   }

   else
   {
      char str[MaxNmeaStrLen+1],buf[MaxNmeaStrLen+1];

      /* specify the timeout period (seconds) for serial puts and gets */
      const time_t TimeOut=10;

      /* specify the configuration command */
      const char *cmd = "PGRMC1,10,1,1,0.0,0,1,1,N,N";

      /* compute the NMEA checksum of the configuration command */
      unsigned char chksum = nmea_checksum(cmd,0,strlen(cmd));

      /* create the configuration command */
      snprintf(buf,sizeof(buf),"$%s*%02X",cmd,chksum);

      /* reinitialize return value */
      status=0;

      /* make a log entry about writing the config string to the GPS engine */
      if (debuglevel>=3)
      {
         LogEntry(FuncName,"Configuring Garmin GPS engine: %s\n",buf);
      }

      /* flush the IO port */
      port->ioflush();

      /* write the configuration command to the Garmin GPS engine */
      if (pputs(port,buf,2,"\r\n")<=0)
      {
         LogEntry(FuncName,"Attempt to write configuration "
                  "string failed: %s\n",buf);
      }

      /* read the configuration echo */
      else
      {
         /* set the reference for the timeout period */
         time_t T0=time(NULL);

         /* read strings from the GPS engine until the PGRMC message is found */
         do
         {
            /* read a string from the GPS serial port */
            pgets(port,str,sizeof(str)-1,2,"\r\n");

            /* check if current string is the PGRMC message */
            if (strstr(str,cmd)) status=1;

            if (debuglevel>=3)
            {
               LogEntry(FuncName,"%s","");
               LogAdd("%s %s\n",((status)?"Accept:":"Reject:"),str);
            }
         }

         /* keep reading strings until PGRMC message found or timeout expires */
         while (status<=0 && difftime(time(NULL),T0)<TimeOut);

         /* if status is false then the config attempt failed */
         if (!status)
         {
            LogEntry(FuncName,"Attempt to configure Garmin GPS15 failed.\n");
         }

         /* make a log entry if the verbosity is high enough */
         else if (debuglevel>=3)
         {
            LogEntry(FuncName,"Garmin GPS15 configured: %s\n",str);
         }
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Garmin GPS15 gps engine                      */
/*------------------------------------------------------------------------*/
/**
   This function configures which NMEA and Garmin sentences are enabled or
   disabled.  The following information was taken from page 18 of the
   technical specification: Garmin GPS 15xH \& 15xL Technical Specifications
   (Document No. 190-00266-03, Rev. A, Dec 2009).

      \begin{verbatim}
      $PGRMO,<1>,<2>*HH\r\n

       <1> Target sentence description (eg., PGRMT, GPGSV, etc).
       <2> Target sentence mode:
              0 = disable specific sentence.
              1 = enable specific sentence.
              2 = disable all output sentences.
              3 = enable all output sentences (except GPALM).

        HH Hexidecimal checksum.

        The following notes apply to the PGRMO input sentence:
           1) If the target sentence mode is '2' (disable all) or '3'
              (enable all), the target sentence description is not checked
              for validity.  In this case, an empty field is allowed (eg,
              $PGRMO,,3), or the descrition field may contain from 1 to 5
              characters.

           2) If the target sentence is '0' (disable) or '1' (enable), the
              target sentence description field must be an identifier for
              one of the sentences being output by the GPS15LP.

           3) If either the target sentence mode field or the target
              sentence description field is not valid, the PGRMO sentence
              will have no effect.

           4) $PGRMO,GPALM,1 will cause the sensor board to transmit all
              stored almanac information.  All other NMEA sentence
              transmission will be temporarily suspended.
      \end{verbatim}

   This function implements a parameterized configuration ability.

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

      output:

         This function returns a positive value if successful, zero indicates
         failure, and -1 indicates an invalid function argument.
      \end{verbatim}

   written by Dana Swift
*/
static int pgrmo(const struct SerialPort *port,const char *msg,int mode)
{
   /* define the logging signature */
   cc *FuncName = "pgrmo()";

   /* initialize this function's return value */
   int status=-1;

   /* validate the SerialPort object */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* flush IO buffers */
   else if (!port->ioflush || port->ioflush()<=0)
   {
      LogEntry(FuncName,"Unable to flush GPS IO buffers.\n");
   }

   else if (!msg) {LogEntry(FuncName,"NULL message identifier.\n");}

   else if (strlen(msg)>5) {LogEntry(FuncName,"Invalid message "
                                     "identifier: %s\n",msg);}

   else
   {
      unsigned char chksum;
      char str[MaxNmeaStrLen+1],cmd[MaxNmeaStrLen+1],buf[MaxNmeaStrLen+1];

      /* specify the timeout period (seconds) for serial puts and gets */
      const time_t TimeOut=10;

      /* reinitialize return value */
      status=0;

      /* construct the configuration command */
      snprintf(cmd,sizeof(cmd),"PGRMO,%s,%d",msg,mode);

      /* compute the NMEA checksum of the configuration command */
      chksum = nmea_checksum(cmd,0,strlen(cmd));

      /* complete the configuration command */
      snprintf(buf,sizeof(buf),"$%s*%02X",cmd,chksum);

      /* make a log entry about writing the config string to the GPS engine */
      if (debuglevel>=3)
      {
         LogEntry(FuncName,"Configuring Garmin GPS engine: %s\n",buf);
      }

      /* flush the IO port */
      port->ioflush();

      /* write the configuration command to the Garmin GPS engine */
      if (pputs(port,buf,2,"\r\n")<=0)
      {
         LogEntry(FuncName,"Attempt to write configuration "
                  "string failed: %s\n",buf);
      }

      /* read the configuration echo */
      else
      {
         /* set the reference for the timeout period */
         time_t T0=time(NULL);

         /* read strings from the GPS engine until the PGRMO message is found */
         do
         {
            /* read a string from the GPS serial port */
            pgets(port,str,sizeof(str)-1,TimeOut,"\r\n");

            /* check if current string is the PGRMO message */
            if (strstr(str,cmd)) status=1;

            if (debuglevel>=3)
            {
               LogEntry(FuncName,"%s","");
               LogAdd("%s %s\n",((status)?"Accept:":"Reject:"),str);
            }
         }

         /* keep reading strings until PGRMC message found or timeout expires */
         while (status<=0 && difftime(time(NULL),T0)<TimeOut);

         /* if status is false then the config attempt failed */
         if (!status) {LogEntry(FuncName,"Attempt to configure "
                                "Garmin GPS15 failed.\n");}

         /* make a log entry if the verbosity is high enough */
         else if (debuglevel>=3)
         {
            LogEntry(FuncName,"Garmin GPS15 configured: %s\n",str);
         }
      }
   }

   return status;
}

#endif /* GARMIN_C */
