#ifndef FIFO_H
#define FIFO_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define fifoChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stdlib.h>

/* define a FIFO structure */
struct Fifo
{
      /* define a constant pointer to the fifo buffer */
      unsigned char *buf;

      /* object to record the size of the fifo buffer */
      const size_t size;

      /* object to record the number of bytes in the fifo queue */
      volatile size_t length;

      /* object to record the number of bytes lost to buffer overflow */
      volatile size_t BufOverflowCount;
      
      /* define the head and tail of the fifo */
      volatile long int head, tail;
};
 
/* function prototypes */
int del(struct Fifo *fifo);
int flush(struct Fifo *fifo);
int push(struct Fifo *fifo, unsigned char byte);
int pop(struct Fifo *fifo, unsigned char *byte);

#endif /* FIFO_H */
#ifdef FIFO_C
#undef FIFO_C

#include <logger.h>

/*------------------------------------------------------------------------*/
/* function to delete the head of the fifo queue                          */
/*------------------------------------------------------------------------*/
/**
   This function deletes the last byte in the FIFO queue - it undoes the
   last push stored in the queue.

      \begin{verbatim}
      input:
         fifo....The Fifo object from which to delete.

      output:
         This function will return a positive value if successful.  A zero
         will be returned if the fifo queue is empty.  If the Fifo object is
         invalid then this function will return a negative value.
      \end{verbatim}
*/
int del(struct Fifo *fifo)
{
   /* define the logging signature */
   cc *FuncName = "del()";
   
   int status=-1;

   /* assert that the Fifo object is not null */
   if (!fifo || !fifo->buf) {LogEntry(FuncName,"NULL pointer to fifo.\n");}
   
   /* validate the Fifo's head */
   else if (fifo->head<0 || fifo->head>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid head.\n");
   }
   
   /* validate the Fifo's tail */
   else if (fifo->tail<0 || fifo->tail>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid tail.\n");
   }

   else
   {
      /* reinitialize the return value */
      status=0;
      
      /* ignore the delete request if the fifo is empty */
      if (fifo->head != fifo->tail) 
      {
         /* compute the new position of the fifo's head-pointer */
         fifo->head = (fifo->head>0) ? fifo->head-1 : fifo->size-1;

         /* terminate the buffer with a NULL */
         fifo->buf[fifo->head]=0; --fifo->length;

         /* indicate success */
         status=1; 
      }
   
      /* reset the overflow counter - always more room after a delete request */
      fifo->BufOverflowCount=0;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to flush the Fifo queue                                       */
/*------------------------------------------------------------------------*/
/**
   This function flushes the FIFO queue.

      \begin{verbatim}
      input:
         fifo...The Fifo object to be flushed.

      output:
         This function returns a positive number on success and zero on
         failure. If the Fifo object is invalid then this function will
         return a negative value.
      \end{verbatim}
*/
int flush(struct Fifo *fifo)
{
   /* define the logging signature */
   cc *FuncName = "flush()";
   
   int status=-1;
  
   /* validate the Fifo object */
   if (!fifo) {LogEntry(FuncName,"NULL pointer to fifo.\n");}

   else
   {
      /* reinitialize return value */
      status=1;
      
      /* reset the fifo pointers and overflow counter */
      fifo->head=0; fifo->tail=0; fifo->length=0; fifo->BufOverflowCount=0;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to pop a byte from the FIFO queue                             */
/*------------------------------------------------------------------------*/
/**
   This function pops one byte from a Fifo queue.  

      \begin{verbatim}
      input:
         fifo....The FIFO from which 'byte' will be popped.

      output:
          byte....This will contain the popped byte.
          
         This function returns a positive value on success and zero on
         failure.  If the Fifo object is invalid then this function will
         return a negative value.
      \end{verbatim}
*/
int pop(struct Fifo *fifo, unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "pop()";
   
   int status=-1;

   /* initialize the return value */
   if (byte) *byte=0;
   
   /* make sure that the pointer is valid */
   if (!byte) {LogEntry(FuncName,"NULL pointer to byte.\n");}

   /* assert that the Fifo object is not null */
   else if (!fifo || !fifo->buf)
   {
      /* log the message */
      LogEntry(FuncName,"NULL pointer to fifo.\n");
   }
   
   /* validate the Fifo's head */
   else if (fifo->head<0 || fifo->head>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid head.\n");
   }
   
   /* validate the Fifo's tail */
   else if (fifo->tail<0 || fifo->tail>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid tail.\n");
   }

   else
   {
      /* reinitialize the function's return value */
      status=1;
            
      /* check if the fifo buffer is empty */
      if (fifo->tail != fifo->head)
      {
         /* read the next byte from the fifo buffer */
         *byte = fifo->buf[fifo->tail]; --fifo->length;

         /* increment the tail - wrap around if necessary */
         if ((++fifo->tail)>=fifo->size) fifo->tail=0;
      }

      /* fifo buffer is empty; reinitialize length counter */
      else {fifo->length=0; status=0;}

      /* reset the buffer overflow counter */
      fifo->BufOverflowCount=0;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to push a byte into the FIFO queue                            */
/*------------------------------------------------------------------------*/
/**
   This function pushes one byte into a Fifo queue.  

      \begin{verbatim}
      input:
         fifo....The FIFO into which 'byte' will be pushed.
         byte....This is the byte to be pushed into the fifo.

      output:
         This function returns a positive value on success and zero on
         failure.  If the Fifo object is invalid then this function will
         return a negative value.
      \end{verbatim}
*/
int push(struct Fifo *fifo, unsigned char byte)
{
   /* define the logging signature */
   cc *FuncName = "push()";
   
   int status=-1;

   /* assert that the Fifo object is not null */
   if (!fifo || !fifo->buf) {LogEntry(FuncName,"NULL pointer to fifo.\n");}
   
   /* validate the Fifo's head */
   else if (fifo->head<0 || fifo->head>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid head.\n");
   }
   
   /* validate the Fifo's tail */
   else if (fifo->tail<0 || fifo->tail>=fifo->size)
   {
      /* log the message */
      LogEntry(FuncName,"Corrupt fifo: invalid tail.\n");
   }

   else
   {
      /* compute where the head should look next */
      long int next = fifo->head+1; if (next>=fifo->size) next=0;

      /* check if the fifo buffer is full */
      if (next != fifo->tail)
      {
         /* insert the byte in the fifo buffer and increment the head pointer */
         fifo->buf[fifo->head]=byte; fifo->head=next; ++fifo->length;

         /* indicate success */
         status=1;
      }

      /* terminate the buffer and increment the fifo overflow counter */
      else {fifo->BufOverflowCount++; status=0;}
   }
   
   return status;
}

#endif /* FIFO_C */
