#ifndef CD_H
#define CD_H (0x0200U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
   This translation unit is a drop-in replacement for the time-related
   services offered in the ANSI/ISO Standard C Library.  It is compliant
   with the ANSI/ISO standard except that multibyte characters are not
   supported and the "C" locale is the only locale implemented.  It has been
   tested against the output of the GNU Standard C Library over the full
   range of inputs for time.

   Many details about how these functions ought to be implemented were
   gleaned from the book, "The Standard C Library" by P.J. Plauger published
   by Prentice Hall (ISBN# 0-13-131509-9).

   written by Dana Swift
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define cdChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* declare prototypes for functions with external linkage */
int Cd(const struct SerialPort *com, float TimeOut);

#endif /* CD_H */
#ifdef CD_C
#undef CD_C

#include <auxtime.h>
#include <logger.h>
#include <sys/time.h>
#include <unistd.h>

/* declare external functions that are used locally */
int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* wait until a SerialPort's CD signal is asserted                        */
/*------------------------------------------------------------------------*/
/**
   This function monitors the state of a SerialPort's carrier-detect
   (CD) signal and pauses until the CD signal is asserted or else a
   user-specified time-out expires, whichever happens first.  This
   function returns immediately upon detection of an asserted CD
   signal.

   \begin{verbatim}
   input:
      com.......The SerialPort object whose CD signal is to be detected.

      TimeOut...The time-out period (seconds) for detection of an
                asserted CD signal.  The valid range if [1,300]
                seconds is enforced by this function.

   output:
      This function returns a positive value if an asserted CD signal
      was detected or else zero if CD is not asserted.  A negative
      return value indicates an exception was encountered.
   \end{verbatim}
*/
int Cd(const struct SerialPort *com, float TimeOut)
{
   /* define the logging signature */
   static const char FuncName[] = "Cd()";
   
   /* initialize the return value */
   int cd=-1;

   /* validate the SerialPort */
   if (com)
   {
      /* validate the carrier-detect function */
      if (com->cd)
      {
         /* define local objects to implement timeout */
         struct timeval T,To;

         /* check if CD is asserted */
         if (com->cd()) {cd=1;}
         
         /* get the reference time */
         else if (!gettimeofday(&To,NULL))
         {
            /* define the pacing of the carrier detection algorithm */
            int n,N; const int pause=500;
         
            /* condition the timeout period */
            if (TimeOut<1.0) {TimeOut=1.0;} else if (TimeOut>300.0) {TimeOut=300.0;}

            /* define objects needed for fail-safe exit of the CD loop */
            N=(TimeOut+1)/(pause*1e-3);
            
            /* loop to wait until CD goes high */
            for (cd=0, n=0; !gettimeofday(&T,NULL) && difftime_tv(&T,&To)<=TimeOut && n<N; n++)
            {
               /* exit the loop if CD high */
               if (com->cd()) {cd=1; break;} else {Wait(pause);}
            }
         }
      }

      /* degenerate CD detection */
      else {cd=1;}
   } 
   else {LogEntry(FuncName,"Invalid COM port.\n");}
   
   return cd;
}

#endif /* CD_C */
