#ifndef INRANGE_H
#define INRANGE_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define inrangeChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes */
int inRange(double a,double x,double b);
int inCRange(double a,double x,double b);

#endif /* INRANGE_H */
#ifdef INRANGE_C
#undef INRANGE_C

/*------------------------------------------------------------------------*/
/* function to determine if a number is within a closed interval          */
/*------------------------------------------------------------------------*/
/*
  This function determines whether a number (x) is in the closed interval
  of [a,b].  The function returns true if (x) is within the range and false
  otherwise.

  written by Dana Swift, October 1995
*/
int inCRange(double a,double x,double b)
{
   return ( ((x-a)*(x-b)<=0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to determine if a number is within a semiclosed range         */
/*------------------------------------------------------------------------*/
/*
  This function determines whether a number (x) is in the semi-closed range
  of [a,b).  The function returns true if (x) is within the range and false
  otherwise.

  written by Dana Swift, March 1993
*/
int inRange(double a,double x,double b)
{
   return ( ((x-a)*(x-b)<=0 && x!=b) ? 1 : 0);
}

#endif /* INRANGE_C */
