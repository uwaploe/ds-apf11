#ifndef AUXTIME_H
#define AUXTIME_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
   This translation unit is a drop-in replacement for the time-related
   services offered in the ANSI/ISO Standard C Library.  It is compliant
   with the ANSI/ISO standard except that multibyte characters are not
   supported and the "C" locale is the only locale implemented.  It has been
   tested against the output of the GNU Standard C Library over the full
   range of inputs for time.

   Many details about how these functions ought to be implemented were
   gleaned from the book, "The Standard C Library" by P.J. Plauger published
   by Prentice Hall (ISBN# 0-13-131509-9).

   written by Dana Swift
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define auxtimeChangeLog "$RCSfile$  $Revision$  $Date$"

#include <sys/time.h>

/* declare prototypes for functions with external linkage */
double difftime_tv(struct timeval *time1, struct timeval *time0);

#endif /* AUXTIME_H */
#ifdef AUXTIME_C
#undef AUXTIME_C

#include <nan.h>
#include <time.h>

/*------------------------------------------------------------------------*/
/* function to compute the time difference between two timeval's          */
/*------------------------------------------------------------------------*/
double difftime_tv(struct timeval *time1, struct timeval *time0)
{
   double dT=NaN();

   /* validate the function arguements */
   if (time1 && time0)
   {
      /* compute the difference between epochs (in seconds) */
      dT  = difftime(time1->tv_sec,time0->tv_sec);

      /* add the difference between fractions of seconds */
      dT += ((double)(time1->tv_usec - time0->tv_usec)/1e6);
   }

   return dT;
}

/* the following section implements missing C standard library functions */
#if defined (__arm__)
   
   #include <errno.h>

   /* prototypes for external functions that are defined locally */
   int gettimeofday(struct timeval *tv, void *tz);

   /* prototypes for external functions that are used locally */
   int RtcGet(time_t *sec, unsigned char *tics);
   
   /*------------------------------------------------------------------------*/
   /* get the time from the RTC                                              */
   /*------------------------------------------------------------------------*/
   /**
      This function reads the time from the RTC which has a resolution of
      3.9msec. It then populates the members of the timeval structure.
   
         \begin{verbatim}
         output:
            tv.....A timeval structure to contain the integral and
                   fractional parts of the RTC's time.  The fractional
                   parts are expressed as microseconds but the resolution
                   of the RTC is only 3.9 milliseconds.
             tz....This argument is deprecated; it was once used to
                   express timezone information.  This function ignores
                   it completely.
   
         If successful, this function returns zero.  If an error is
         encountered, this function returns -1 and errno is set to
         communicate the nature of the error.
        \end{verbatim}
   */
   int gettimeofday(struct timeval *tv, void *tz)
   {
      /* initialize the return value*/
      int status=-1;
   
      /* create some local work objects */
      time_t sec; unsigned char tics;
   
      /* check for invalid timeval structure */
      if (!tv) {errno=EINVAL;}
   
      /* read the system RTC */
      else if (RtcGet(&sec,&tics)<=0) {errno=ENOMSG;}
   
      /* compute the members of the timeval structure */
      else {tv->tv_sec=sec; tv->tv_usec=(tics*3906L + (tics>>2)); status=0;}
      
      return status;
   }
   
#endif /* __arm__ */

#endif /* AUXTIME_C */

