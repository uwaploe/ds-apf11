#ifndef LOGGER_H
#define LOGGER_H (0x0200U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define loggerChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <stdarg.h>
#include <stdio.h>

/* define a shorthand for const char */
typedef const char cc;

/* external prototypes of functions for the logging module */
long int LogAdd(const char *format, ...);
int      LogAttach(int handle);
long int LogBuf(const char *function_name, const char *buf, int buflen, const char *format, ...);
int      LogClose(void);
int      LogEnable(unsigned char logenable);
long int LogEntry(const char *function_name, const char *format,...);
int      LogHandle(void);
int      LogOpen(const char *fname,char mode);
int      LogPredicate(unsigned char lp);
long int LogSize(void);

/* functions for formatted output to UNIX file descriptors */
int hprintf(int handle, const char *format, ...);
int vhprintf(int handle, const char *format, va_list ap);

/* external declaration for maximum allowed log size */
extern persistent long int MaxLogSize;

/* external declaration of debug_level */
extern persistent unsigned int debugbits;

/* external declare the handle for the default message stream */
extern persistent int loghandle;

extern persistent char log_path[32];

#define VERBOSITYMASK (0x07)
#define debuglevel (debugbits&VERBOSITYMASK)

#endif /* LOGGER_H */
#ifdef LOGGER_C
#undef LOGGER_C

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <gmtime.h>
#include <math.h>
#include <stdio.h>
#include <stream.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* define the maximum length of the format string */
#define MAXBUFLEN (1023)

/* define the maximum allowed size of the log file (bytes) */
persistent long int MaxLogSize;

/* persistent boolean object to allow user to inhibit logentry */
persistent static unsigned char log_predicate;

/* nonpersistent boolean object to allow user to inhibit logentry */
static unsigned char log_enable=1;

/* declare the handle for the default message stream */
persistent int loghandle;

/*
  Define and initialize the debugbits.  Despite that the debugbits is not
  used by any function in the logger module, it is defined in this module
  because it is used to control the type and verbosity of logging
  operations.  The intention is that logging can be made more terse by
  reducing the debugbits such that debugbits=0 produces the fewest log
  entries and debugbits=5 produces the maximum number of log entries.  Here
  is an example of its typical use:

     if (debugbits>=3) {LogEntry("foo()","Situation normal: FUBAR\n");}

  The default is debugbits=2.
*/
persistent unsigned int debugbits;

/* local declarations of function prototypes */
time_t itimer(void);
#if defined (__APF11__)
int snprintf(char *str, size_t size, const char *format, ...);
#endif
int streamok(int handle);

/*------------------------------------------------------------------------*/
/* Function to print a message to the log file.                           */
/*------------------------------------------------------------------------*/
/**
   This function appends a non-timestamped message to the engineering
   log file.  The arguments of this function match the arguments to
   the printf() function.

   The return value of this function is the number of bytes written to the
   logstream.
*/
long int LogAdd(const char *format,...)
{
   /* initialize the return value */
   long int len=0;

   /* check if logging is suppressed */
   if (debuglevel>0 && log_enable>0)
   {
      /* validate the format */
      if (!format || !(*format))
      {
         /* define the error message */
         const char *msg = "Missing format specification in LogAdd().\n";

         /* warn of the missing format specification */
         if (stderr)
         {
            fprintf(stderr, "%s", msg);
         }

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* make a logentry of the missing format specification */
            hprintf(loghandle,msg);
         }

         /* indicate failure */
         len=-1;
      }

      else
      {
         /* object needed to manage variable argument list */
         va_list arg_pointer;

         /* write the logentry to stderr */
         if (stderr)
         {
            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            vfprintf(stderr,format,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);

            /* flush the output */
            fflush(stderr);
         }

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            len=vhprintf(loghandle,format,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);
         }
      }
   }

   /* return the number of bytes written to the engineering log */
   return len;
}

/*------------------------------------------------------------------------*/
/* function to attach the log stream to a user-supplied stream            */
/*------------------------------------------------------------------------*/
/**
   This function attaches the logstream to a user-suppled stream.  The
   stream is defined in terms of the file descriptor associated with
   the user-supplied stream.

   \begin{verbatim}
   input:
      handle....The file descriptor for the user-supplied stream to
                which the logstream will be attached.
   \end{verbatim}

   This function returns one if the logstream was successfully attached to
   the user-supplied stream, zero otherwise.
*/
int LogAttach(int handle)
{
   /* define logentry signature */
   cc *FuncName = "LogAttach()";

   /* initialize return value */
   int status=0;

   /* validate the stream before attaching to it */
   if (!streamok(handle))
   {
      /* log the message */
      LogEntry(FuncName,"Enabling logging functions.\n");

      /* attach the log stream */
      loghandle=handle; status=1;
   }

   /* make a log entry */
   else {LogEntry(FuncName,"Attempt to attach log stream failed.\n");}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enter buffer into the engineering logs                     */
/*------------------------------------------------------------------------*/
/**
   This function makes an engineering log entry that augments the services
   of LogEntry() with the subsequent inclusion of the contents of a
   string buffer.  The logentry has four fields as shown in the
   following form:
      <time-stamp> <function-name> <formatted-message> [<buffer-contents>]
   Angled brackets '<>' are nonliteral characters that frame a
   description of contents of the field; the square brackets '[]' are
   literal characters.

   \begin{verbatim}
   input:
      function_name...The name of the function that generated the
                      logentry.
      buf.............The contents of this buffer will be included
                      within square brackets '[]'.  This buffer may
                      include both printable and nonprintable
                      characters; nonprintable characters are
                      represented as hex numbers within square
                      brackets: [0xhh].
      buflen..........The number of bytes in the buffer to be included
                      in the logentry.
      format..........A format string as might be used with one of the
                      printf-family of functions. The variable
                      argument list provides arguments for the
                      format-specifiers in the format string.

   ouput:
      This function returns the number of bytes written to the
      engineering log file.
   \end{verbatim}
*/
long int LogBuf(const char *function_name, const char *buf, int buflen, const char *format, ...)
{
   /* initialize return value */
   long int len=0;

   /* a debuglevel of zero suppresses all logging */
   if (debuglevel>0 && log_enable>0)
   {
      /* validate the format */
      if (!format || !(*format))
      {
         /* define the error message */
         const char *msg = "Missing format specification in LogBuf().\n";

         /* warn of the missing format specification */
         if (stderr) {
            fprintf(stderr, "%s", msg);
         }

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* make a logentry of the missing format specification */
            hprintf(loghandle,msg);
         }

         /* indicate failure */
         len=-1;
      }

      else
      {
         va_list arg_pointer; int i;

         /* start the log entry */
         len=LogEntry(function_name,"%s","");

         /* write the logentry to stderr */
         if (stderr)
         {
            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            vfprintf(stderr,format,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);

            /* flush the output */
            fflush(stderr);
         }

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            len+=vhprintf(loghandle,format,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);
         }

         /* bracket the log entry */
         len+=LogAdd("[");

         /* write up to 1K of the buffer to the engineering logs */
         for (i=0; i<buflen && i<1024; i++,len++)
         {
            if (buf[i]=='\r') LogAdd("\\r");
            else if (buf[i]=='\n') LogAdd("\\n");
            else if (isprint((unsigned char)(buf[i]))) LogAdd("%c",buf[i]);
            else LogAdd("[0x%02x]",(unsigned char)buf[i]);
         }

         /* terminate the log entry */
         len+=LogAdd("]\n");
      }
   }

   /* return the number of bytes written to the engineering log */
   return len;
}

/*------------------------------------------------------------------------*/
/* function to close the log file.                                        */
/*------------------------------------------------------------------------*/
/**
   This function closes the logstream. The return value of this function is
   one if the logstream was successfully closed, zero otherwise.
*/
int LogClose(void)
{
   /* define logentry signature */
   static char *FuncName = "LogClose()";

   /* initialize the return value */
   int status=1;

   /* validate the log stream */
   if (streamok(loghandle))
   {
      /* local objects needed for time stamps */
      char buf[128]; time_t t=time(NULL), itime=itimer();

      /* hack to prevent stack overflow via indirect self-referencing */
      int handle=loghandle; loghandle=-1;

      /* write the time string */
      strftime(buf,127,"%b %d %Y %H:%M:%S",Tm(t,0,NULL));

      /* Make a log entry before closing the log file */
      if (debuglevel>=3)
      {
         /* print the function identifier */
         hprintf(handle,"(%s, %7ld sec) %-20s Disabling logging functions.\n",buf,itime,FuncName);
      }

      /* close the current log file */
      if (close(handle))
      {
         /* create the message */
         fprintf(stderr,"(%s, %7ld sec) %-20s Attempt to close log file failed.\n",buf,(long int)itime,FuncName);

         /* indicate failed attempt to close current log file */
         status=0;
      }
   }

   /* invalidate the loghandle */
   loghandle=-1;

   return status;
}

/*------------------------------------------------------------------------*/
/* function to set the value of the log-enable predicate                  */
/*------------------------------------------------------------------------*/
/**
   This function sets the value of the engineering log-enable
   predicate which allows the user to (nonpersistently) independently
   enable/disable placement of log entries into the file that is
   stored in the file system.

   \begin{verbatim}
   input:

      logen....A nonzero value will enable placement of the log
               entries into the file that is stored on the filesystem
               and later telemetered.  A zero value will disable log
               entries.  The sentinel value 0xff queries the current
               status of the log-enable predicate.

   output:
      This function returns a positive value on success or zero on
      failure.  If the function argument is the sentinel value 0xff
      then this function returns a positive value if log entries are
      currently enabled or zero if log entries are currently disabled.
   \end{verbatim}
*/
int LogEnable(unsigned char logen)
{
   /* initialize the return value */
   int status=1;

   /* check for query of log_enable */
   if (logen==0xff) status=(log_enable)?1:0;

   /* assign the log predicate */
   else {log_enable=(logen)?1:0;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to append a timestamped entry to the logstream                */
/*------------------------------------------------------------------------*/
/**
   This function appends a timestamped entry to the logstream.  The
   logentry has three fields as shown in the following form:
      <time-stamp> <function-name> <formatted-message>
   Angled brackets '<>' are nonliteral characters that frame a
   description of contents of the field.

   \begin{verbatim}
   input:
      function_name...The name of the function that generated the
                      logentry.
      format..........A format string as might be used with one of the
                      printf-family of functions. The variable
                      argument list provides arguments for the
                      format-specifiers in the format string.

   ouput:
      This function returns the number of bytes written to the
      engineering log file.
   \end{verbatim}
*/
long int LogEntry(const char *function_name,const char *format,...)
{
   /* initialize the return value */
   long int len=0;

   /* a debuglevel of zero suppresses all logging */
   if (debuglevel>0 && log_enable>0)
   {
      /* validate the format */
      if (!format || !(*format))
      {
         /* define logentry signature */
         const char *FuncName = "LogEntry()";

         /* define the error message */
         const char *msg = "%-20s Missing format specification.";

         /* warn of the missing format specification */
         if (stderr) fprintf(stderr,msg,FuncName);

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* make a logentry of the missing format specification */
            hprintf(loghandle,msg,FuncName);
         }

         /* indicate failure */
         len=-1;
      }

      else
      {
         /* local objects needed for time stamps */
         va_list arg_pointer; char fmt[MAXBUFLEN+1], func[21], buf[32];

         /* get the current calendar time and the interval-timer */
         time_t t=time(NULL), itime=itimer();

         /* check for interval-timer value that exceeds 7 characters */
         if (itime<-999999L || itime>9999999L) itime=(time_t)(-1);

         /* copy the format from code ROM */
         strncpy(fmt,format,MAXBUFLEN);

         /* copy the function name from code ROM */
         if (function_name) {int len=sizeof(func)-1; strncpy(func,function_name,len); func[len]=0;}
         else snprintf(func,sizeof(func),"<NULL>");

         /* write the date and time */
         strftime(buf,sizeof(buf),"%b %d %Y %H:%M:%S",Tm(t,0,NULL));

         /* write the logentry to stderr */
         if (stderr)
         {
            /* print the date and time */
            fprintf(stderr,"(%s, %7ld sec) ",buf,itime);

            /* print the function identifier */
            fprintf(stderr,"%-20s ",func);

            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            vfprintf(stderr,fmt,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);

            /* flush the output */
            fflush(stderr);
         }

         /* check criteria for making a logentry */
         if (loghandle>0 && loghandle!=STDERR_FILENO && LogSize()<MaxLogSize && log_predicate)
         {
            /* print the date and time */
            len=hprintf(loghandle,"(%s, %7ld sec) ",buf,(long int)itime);

            /* print the function identifier */
            len+=hprintf(loghandle,"%-20s ",func);

            /* get the format argument from the argument list */
            va_start(arg_pointer,format);

            /* print the message */
            len+=vhprintf(loghandle,fmt,arg_pointer);

            /* clean up call */
            va_end(arg_pointer);
         }
      }
   }

   /* return the number of bytes written to the engineering log */
   return len;
}

/*------------------------------------------------------------------------*/
/* function to open the log file for storing run-time messages.           */
/*------------------------------------------------------------------------*/
/**
   This function is designed to open a file into which the logstream will be
   written.

   \begin{verbatim}
   input:
      fname....The name of the file into which the logstream will be
               written.
      mode.....The mode used to open the file.  Possible choices are 'w'
               or 'a'.  The 'w' mode opens a file and discards any
               contents if the file previously existed.  The 'a' mode
               appends to a file if it already exists, otherwise it opens
               the file for writing.

   ouput:
      This function returns one if the file was successfully opened,
      zero otherwise.
    \end{verbatim}
*/
int LogOpen(const char *fname,char mode)
{
   /* define logentry signature */
   cc *FuncName = "LogOpen()";

   /* initialize the return value */
   int status=-1;

   /* check for NULL function argument */
   if (fname)
   {
      /* define the permission settings (ie., applies to linux use only) */
      #define PERM (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)

      /* initialize the mode string */
      int flags = (mode=='w') ? (O_WRONLY|O_CREAT|O_TRUNC) : (O_WRONLY|O_CREAT|O_APPEND);

      /* check for an existing log file to close */
      if (streamok(loghandle)) {LogClose();}

      /* open the log file */
      if ((loghandle=open(fname,flags,PERM))<=STDERR_FILENO)
      {
         /* open failed */
         loghandle=-1; status=0;

         /* make a log entry about the failed attempt to open the log file */
         LogEntry(FuncName,"Unable to open \"%s\" using mode \"%c\".\n",fname,mode);
      }
      else status=1;

      /* make a log entry */
      if (debuglevel>=3) {LogEntry(FuncName,"Enabling logging functions.\n");}
   }

   /* make the log entry */
   else {LogEntry(FuncName,"Pathname for log file required.\n");}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to set the value of the log predicate                         */
/*------------------------------------------------------------------------*/
/**
   This function sets the value of the engineering log predicate which allows
   the user to independently enable/disable placement of log entries into the
   file that is stored in the file system.

   \begin{verbatim}
   input:
      lp....A nonzero value will enable placement of the log entries
            into the file that is stored on the filesystem and later
            telemetered.  A zero value will disable log entries.  The
            sentinel value 0xff queries the current status of the log
            predicate.

   output:
      This function returns a positive value on success or zero on
      failure.  If the function argument is the sentinel value 0xff
      then this function returns a positive value if log entries are
      currently enabled or zero if log entries are currently disabled.
   \end{verbatim}
*/
int LogPredicate(unsigned char lp)
{
   /* initialize the return value */
   int status=1;

   /* check for query of log_predicate */
   if (lp==0xff) status=(log_predicate)?1:0;

   /* assign the log predicate */
   else {log_predicate=lp;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the current size of the log file                    */
/*------------------------------------------------------------------------*/
/**
   This function measures the size (in bytes) and controls the maximum
   size of the engineering log file.  If the log-stream is being
   written to an engineering log file then the size of the file is
   measured.  In addition, the current size is compared to the maximum
   allowed size and logging is terminated if the maximum size is
   exceeded.

   \begin{verbatim}
   output:
      This function returns the current size (in bytes) of the
      engineering log.  A negative return value indicates that the
      log-stream is not being written to a file.
   \end{verbatim}
*/
long int LogSize(void)
{
   /* local objects needed to create timestamp */
   time_t t=0;
   char buf[64];
   char log_flush_path[128];

   /* define a persistent one-time switch for a high-water warning */
   persistent static int WarnTooBig;

   /* initialize return value */
   long int size=-1;

   /* validate the file name */
   if (loghandle>STDERR_FILENO && streamok(loghandle))
   {
      /* measure the size of the engineering log file */
      if ((size=lseek(loghandle,0,SEEK_END))>=0)
      {
         /* initialize the one-time switch */
         if (size>=0 && size<256) WarnTooBig=0;

         /* position the file pointer to the end of the stream */
         if (size>=0 && !WarnTooBig && MaxLogSize>0 &&
            (size>MaxLogSize*0.95 || size>(MaxLogSize-256)))
         {
            /* set the one-time switch */
            WarnTooBig=1;

            /* get the date & time */
            t=time(NULL); strftime(buf,127,"%b %d %Y %H:%M:%S",Tm(t,0,NULL));

            /* print the function identifier */
            size+=hprintf(loghandle,"\n\n(%s) %-20s Log file is %ld bytes.  "
                          "Limit is %ld bytes.\n\n",buf,"LogSize()", size, MaxLogSize);
         }

         /* close the logfile at the high-water mark */
         if (size >= MaxLogSize - 256)
         {
            // Flush the log to disk, then reopen the log path in append mode.
            // TODO rmarver: If this doesn't work, then just create new logfiles
            // with the flush count.
            LogPredicate(0);
            LogClose();
            t = time(NULL);
            strftime(buf, sizeof(buf),"%y%m%d%H%M",gmtime(&t));
            snprintf(log_flush_path, sizeof(log_flush_path), "%s.%s.log", log_path, buf);
            link(log_path, log_flush_path);
            LogOpen(log_path, 'w');
            LogPredicate(1);
         }
      }
   }

   /* return the current size of the engineering log file */
   return size;
}

/*------------------------------------------------------------------------*/
/* function to return handle of the logstream                             */
/*------------------------------------------------------------------------*/
/**
   This function returns the handle (ie., file descriptor) of the
   engineering log stream if the log stream being written to a file.
   If the log stream is not being written to a file then this function
   returns to the file descriptor of the STDERR stream.
*/
int LogHandle(void)
{
   return (loghandle>0 && streamok(loghandle)) ? loghandle : STDERR_FILENO;
}

/*------------------------------------------------------------------------*/
/* write formatted string to a file associated with a file descriptor     */
/*------------------------------------------------------------------------*/
/**
   This function writes a formatted string to a file associated with a
   handle (ie., file descriptor).

   \begin{verbatim}
   input:
      handle....The file descriptor associated with the file into
                which the formatted string should be written.

   output:
      This function returns the number of bytes written to the
      stream associated with the file descriptor.
   \end{verbatim}
*/
int hprintf(int handle, const char *format, ...)
{
   /* initialize the return value */
   int n=0;

   /* object needed to manage variable argument list */
   va_list arg_pointer;

   /* initialize the argument pointer */
   va_start(arg_pointer,format);

   /* pass the formatting off to vhprintf() */
   n=vhprintf(handle, format, arg_pointer);

   /* clean-up the argument pointer */
   va_end(arg_pointer);

   /* return the number of bytes written to the stream */
   return n;
}

/*------------------------------------------------------------------------*/
/* write formatted string to a file associated with a file descriptor     */
/*------------------------------------------------------------------------*/
/**
   This function writes a formatted string to a file associated with a
   handle (ie., file descriptor).

   \begin{verbatim}
   input:
      handle....The file descriptor associated with the file into
                which the formatted string should be written.

   output:
      This function returns the number of bytes written to the
      stream associated with the file descriptor.
   \end{verbatim}
*/
int vhprintf(int handle, const char *format, va_list ap)
{
   /* define a buffer for constructing the formatted string */
   char buf[MAXBUFLEN+1];

   /* construct the formatted string */
   int n=vsnprintf(buf,sizeof(buf),format,ap);

   /* determine if the formatted string was truncated */
   if (n>=sizeof(buf)) {n=(sizeof(buf)-1);}

   /* write the formatted string to the stream */
   if ((n=write(handle,buf,n))==-1) {n=0;}

   /* return the number of bytes written to the stream */
   return n;
}

#endif /* LOGGER_C */
