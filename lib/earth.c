#ifndef EARTH_H
#define EARTH_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define earthChangeLog "$RCSfile$  $Revision$  $Date$"

/* prototypes for functions with external linkage */
float GeocentricLat(float gdlat);
float GeodeticLat(float gclat);
int Geo2Xyz(float lat, float lon, float R, float *X, float *Y, float *Z);
int Xyz2Geo(float X, float Y, float Z, float *lat, float *lon, float *R);

#endif /* EARTH_H */
#ifdef EARTH_C
#undef EARTH_C

#include <logger.h>
#include <math.h>
#include <inrange.h>
#include <nan.h>

/* define lengths of semimajor and semiminor axes */
static const float Re=6378.1, Rp=6356.8;

/* define conversion factors between radians and degrees */
#define DegToRad (0.017453293)
#define RadToDeg (57.29577951)

/*------------------------------------------------------------------------*/
/* convert from geodetic to geocentric latitude                           */
/*------------------------------------------------------------------------*/
/**
   This function uses an earth model that is an oblate spheroid with
   semi-major (equatorial) and semi-minor (polar) axes of length
   Re=6378.1km and Rp=6356.8km, respectively.  For a point on the
   earth's surface, the geodetic latitude represents the angle between
   the equatorial plane and vector that is locally normal to the
   surface (ie., geoid).  For the same point, the geocentric latitude
   represents the angle between the equatorial plane and a vector from
   earth's center to the point on the surface. Refer to SWJ28:100-104
   for derivations and model details.

   \begin{verbatim}
   input:
      gdlat...The geodetic latitude of a point on the earth's surface.

   output:
      If successful, this function returns the geocentric latitude
      associated with the specified geodetic latitude.  NaN is
      returned, if the geodetic latitude is not in the domain
      [-90,90].
   \end{verbatim}
*/
float GeocentricLat(float gdlat)
{
   /* initialize the return value */
   float gclat=NaN();

   /* qualify the domain */
   if (inCRange(-90,gdlat,90))
   {
      /* compute the geocentric latitude [SWJ28:104;eq(25),p(155)] */
      gclat = gdlat - ((Re-Rp)/Rp)*sin(2*gdlat*DegToRad)*RadToDeg;
   }
   
   return gclat;
}

/*------------------------------------------------------------------------*/
/* convert from geocentric to geodetic latitude                           */
/*------------------------------------------------------------------------*/
/**
   This function uses an earth model that is an oblate spheroid with
   semi-major (equatorial) and semi-minor (polar) axes of length
   Re=6378.1km and Rp=6356.8km, respectively.  For a point on the
   earth's surface, the geodetic latitude represents the angle between
   the equatorial plane and vector that is locally normal to the
   surface (ie., geoid).  For the same point, the geocentric latitude
   represents the angle between the equatorial plane and a vector from
   earth's center to the point on the surface. Refer to SWJ28:100-104
   for derivations and model details.

   \begin{verbatim}
   input:
      gclat...The geocentric latitude of a point on the earth's surface.

   output:
      If successful, this function returns the geodetic latitude
      associated with the specified geocentric latitude.  NaN is
      returned, if the geocentric latitude is not in the domain
      [-90,90].
   \end{verbatim}
*/
float GeodeticLat(float gclat)
{
   /* initialize the return value */
   float gdlat=NaN();

   /* qualify the domain */
   if (inCRange(-90,gclat,90))
   {
      /* compute the geodetic latitude [SWJ28:104;eq(25),p(155)] */
      gdlat = gclat + ((Re-Rp)/Rp)*sin(2*gclat*DegToRad)*RadToDeg;
   }
   
   return gdlat;
}

/*------------------------------------------------------------------------*/
/* convert geodetic coordinates to ECEF cartesian coordinates             */
/*------------------------------------------------------------------------*/
/**
   This function converts geodetic (latitude,longitude,radius)
   coordinates into an earth-centered earth-fixed (ECEF) rotating
   cartesian reference frame, (x,y,z).  The z-axis is coincident with
   the rotation-axis, the xy-plane is the equatorial plane, and the
   positive z,x axes go through the north pole and (lat=0,lon=0),
   respectively.  The coordinates (x,y,z) are measured in kilometers
   and are in the range: -6378km < (x,y,z) < 6378km.

   The earth model is an oblate spheroid with semi-major (equatorial)
   and semi-minor (polar) axes of length Re=6378.1km and Rp=6356.8km,
   respectively.  The latitude is geodetic (not geocentric). Refer to
   SWJ28:100-104 for derivations and model details.

   \begin{verbatim}
   input:
      lat....The geodetic latitude (degrees) in the range [-90,90].
      lon....The longitude (degrees) in the range [-180,360).
      R......The distance (km) from the center of the earth.  The
             sentinel value (NaN) specifies that the cartesian
             coordinates on the surface of the earth are to be
             computed. Otherwise, the value of R must be positive and
             specify a location within 10% of the earth's surface.

    output:
       X.....The x-coordinate (km) of the ECEF cartesian reference frame. 
       Y.....The y-coordinate (km) of the ECEF cartesian reference frame. 
       Z.....The z-coordinate (km) of the ECEF cartesian reference frame. 

       This function returns a positive value if the cartesian
       coordinates were successfully computed; zero indicates
       failure. A negative return value indicates that an exception
       was encountered.
   \end{verbatim}
*/
int Geo2Xyz(float lat, float lon, float R, float *X, float *Y, float *Z)
{
   /* define the logging signature */
   cc *FuncName = "IrGeo2Xyz()";

   /* initialize the return value */
   int status=1;

   /* initialize the function arguments */
   if (X) {(*X) = NaN();}
   if (Y) {(*Y) = NaN();}
   if (Z) {(*Z) = NaN();}

   /* validate the spherical latitude and longitude */
   if (!inCRange(-90,lat,90) || !inRange(-180,lon,360))
   {
      LogEntry(FuncName,"Domain error: lat(-90<=%g<=90) "
               "lon(-180<=%g<360)\n",lat,lon); status=-1;
   }

   /* check sentinel value for calculation on earth's surface */
   else if (isNaN(R))
   {
      /* convert geodetic latitude to spherical latitude [SWJ28:104;eq(25),p(155)] */
      lat -= ((Re-Rp)/(2*Rp))*sin(2*lat*DegToRad)*RadToDeg;
      
      /* compute cartesian coordinates [SWJ28:104;eq(1),p(151)] */
      if (X) {(*X) = Re*cos(lat*DegToRad)*cos(lon*DegToRad);}
      if (Y) {(*Y) = Re*cos(lat*DegToRad)*sin(lon*DegToRad);}
      if (Z) {(*Z) = Rp*sin(lat*DegToRad);}
   }

   /* allow calculation of cartesian coordinates within 10% of earth's surface */
   else if (Finite(R) && inCRange(0.9*Rp,R,1.1*Re))
   {
      /* stack-based objects for local use */
      float r[3],n[3],L;
      
      /* convert geodetic latitude to spherical latitude */
      lat -= ((Re-Rp)/(2*Rp))*sin(2*lat*DegToRad)*RadToDeg;

      /* compute a position vector from earth's center to surface [SWJ28:104;eq(1),p(151)] */
      r[0] = Re*cos(lat*DegToRad)*cos(lon*DegToRad);
      r[1] = Re*cos(lat*DegToRad)*sin(lon*DegToRad);
      r[2] = Rp*sin(lat*DegToRad);

      /* compute a zenith vector normal to geoid [SWJ28:104;eq(7),p(153)] */
      n[0] = cos(lat*DegToRad)*cos(lon*DegToRad)/Re;
      n[1] = cos(lat*DegToRad)*sin(lon*DegToRad)/Re;
      n[2] = sin(lat*DegToRad)/Rp;

      /* scale the zenith vector to be a unit vector */
      L = hypot(hypot(n[0],n[1]),n[2]); n[0]/=L; n[1]/=L; n[2]/=L;
      
      /* compute the distance from earth's center to its surface */
      L = hypot(Re*cos(lat*DegToRad),Rp*sin(lat*DegToRad));
      
      /* compute cartesian coordinates along the local zenith vector */
      if (X) {(*X) = r[0] + (R-L)*n[0];}
      if (Y) {(*Y) = r[1] + (R-L)*n[1];}
      if (Z) {(*Z) = r[2] + (R-L)*n[2];}
   }

   /* log the conformancy violation */
   else {LogEntry(FuncName,"Iridium geo-location conformancy "
                  "violation. R(0.9Rp,%gkm,1.1Re)\n",R); status=0;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* convert iridium cartesian coordinates to geodetic coordinates          */
/*------------------------------------------------------------------------*/
/**
   Iridium's coordinates <x>, <y>, <z> form a geolocation grid code
   from an earth centered cartesian coordinate system, using
   dimensions, x, y, and z, to specify location. The coordinate system
   is aligned such that the z-axis is a ligned with the north and
   south poles, leaving the x-axis and y-axis to lie in the plane
   containing the equator.  The axes are aligned such that at 0
   degrees latitude and 0 degrees longitude, both y and z are zero and
   x is positive (x = +6376, representing the nominal earth radius in
   kilometers).  Each dimension of the geolocation grid code is
   displayed in decimal form using units of kilometres.  Each
   dimension of the geolocation grid code has a minimum value of
   –6376, a maximum value of +6376, and a resolution of 4.

   The earth model is an oblate spheroid with semi-major (equatorial)
   and semi-minor (polar) axes of length Re=6378.1km and Rp=6356.8km,
   respectively.  The latitude is geodetic (not geocentric). Refer to
   SWJ28:100-104 for derivations and model details.

   \begin{verbatim}
   input:
       X.....The x-coordinate (km) of the cartesian reference frame. 
       Y.....The y-coordinate (km) of the cartesian reference frame. 
       Z.....The z-coordinate (km) of the cartesian reference frame. 

    output:
      lat....The geodetic latitude (degrees) in the range [-90,90].
      lon....The longitude (degrees) in the range [-180,360).
      R......The distance (km) from the center of the earth.

       This function returns a positive value if the geodetic
       coordinates were successfully computed; zero indicates
       failure. A negative return value indicates that an exception
       was encountered.
   \end{verbatim}
*/
int Xyz2Geo(float X, float Y, float Z, float *lat, float *lon, float *R)
{
   /* define the logging signature */
   cc *FuncName = "IrXyz2Geo()";

   /* initialize the return value */
   int status=1;

   /* compute nondimensional coordinates [SWJ28:100;eq(7),p(137)] */
   float x=X/Re, y=Y/Re, z=Z/Rp, r=hypot(x,y), rho=hypot(r,z);

   /* initialize the function arguments */
   if (R)   {(*R)   = NaN();}
   if (lat) {(*lat) = NaN();}
   if (lon) {(*lon) = NaN();}

   /* verify radius of iridium fix is within 10% of the earth's surface */
   if (!inCRange(0.9, rho, 1.1))
   {
      /* make a logentry */
      LogEntry(FuncName,"Iridium geo-location conformancy violation. "
               "(%0.0fkm, %0.0fkm, %0.0fkm, %0.2f)\n",X,Y,Z,rho);

      /* indicate failure */
      status=0;
   }

   else
   {
      /* compute the spherical latitude & longitude [SWJ28:104;eq(1),p(151)] */
      float slat=atan2f(z,r), slon=atan2f(y,x);
      
      /* compute the distance from earth's center to its surface */
      float Rs = hypot(Re*cos(slat),Rp*sin(slat));

      /* compute a vector normal to geoid [SWJ28:104;eq(7),p(153)] */
      float n[3] = {cos(slat)*cos(slon)/Re, cos(slat)*sin(slon)/Re, sin(slat)/Rp};

      /* scale the normal vector to be a unit vector */
      float L = hypot(hypot(n[0],n[1]),n[2]); n[0]/=L; n[1]/=L; n[2]/=L;
      
      /* compute distance from earth center */
      L = hypot(r*Re,z*Rp);

      /* adjust the ECEF coordinates to the earth's surface */
      X+=(Rs-L)*n[0]; Y+=(Rs-L)*n[1]; Z+=(Rs-L)*n[2];

      /* recompute the nondimensional coordinates */
      x=X/Re, y=Y/Re, z=Z/Rp, r=hypot(x,y);
      
      /* compute distance from earth center */
      if (R) {(*R)=L;}
      
      /* compute longitude [SWJ28:100;eq(13),p(139)] */
      if (lon) {(*lon) = slon*RadToDeg;}
      
      /* compute latitude of oblate spheroidal earth model [SWJ28:100;eq(13),p(139)] */
      if (lat) {(*lat) = atan2f(z/Rp,r/Re)*RadToDeg;}
   }
   
   return status;
}

#endif /* EARTH_C */
