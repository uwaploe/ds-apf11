#ifndef STREAM_H
#define STREAM_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define streamChangeLog "$RCSfile$  $Revision$  $Date$"

#include <fatsys.h>
#include <stdio.h>

/* define a structure to contain a list of FILE streams */
typedef struct {int n; FILE *fp[NFIO+NSTD];} Streams;

/* define a structure to contain a const list of FILE streams */
typedef const Streams cStreams;

/* nonstandard functions to augment the stdio library */
int       fcloseall(void);
int       fclose_streams(void);
int       fformat(void);
int       fnameok(const char *fname);
cStreams *fstreams(void);

#endif /* STREAM_H */
#ifdef STREAM_C
#undef STREAM_C

#include <reent.h>
#include <unistd.h>

#ifndef _EXFUN
# define _EXFUN(N,P) N P
#endif

/* static object to contain a list of FILE streams */
static Streams streams;

/* prototypes for functions with static linkage */
static int fclose_nonstdio(FILE *fp);
static int flist(FILE *fp);

/* prototypes for unpublished functions with external linkage */
extern int _closeall(void);

/* prototypes for NewLib functions with external linkage */
extern int _EXFUN(_fwalk, (struct _reent *, int (*)(FILE *)));

/*------------------------------------------------------------------------*/
/* construct a list of pointers to valid and open FILE objects            */
/*------------------------------------------------------------------------*/
/**
   This function constructs a list of pointers to valid and open FILE
   objects in the NewLib libc library.  It uses the NewLib _fwalk()
   function to traverse a linked list of pointers to structures that
   contain the FILE objects.

   \begin{verbatim}
   output:
      This function returns a pointer to a Streams object. The Streams
      object contains an array pointers to FILE objects together with
      an integer count of the number of valid elements in the array:
         Streams.fp[]: An array of pointers to FILE objects.  Each
                       nonzero element the array represents a pointer to
                       a valid and open FILE object.
         Streams.n:    The number of nonzero elements in the fp[] array.
   \end{verbatim}
*/
cStreams *fstreams(void)
{
   /* initialize the return value */
   int status=-1;

   /* initialize the list of FILE streams */
   memset((void *)(&streams),0,sizeof(Streams));

   /* NewLib _fwalk() function traverses the linked list of FILE streams */
   status = _fwalk(_GLOBAL_REENT, flist);

   /* return a pointer to the Streams object */
   return ((status) ? NULL : (&streams));
}

/*------------------------------------------------------------------------*/
/* function to add a pointer to a FILE object to the Streams structure    */
/*------------------------------------------------------------------------*/
/**
   This function is used by fstreams() and _fwalk() to add a pointer
   to a FILE object to the Streams object; it's static linkage
   reflects the fact that it is the predicate function for _fwalk().

   On success, this function returns zero.  A return value of EOF
   indicates that the function argument was NULL or the Streams.fp[]
   is already at full capacity.
*/
static int flist(FILE *fp)
{
   /* initialize the return value */
   int status=EOF;

   /* compute the number of elements in the Streams.fp[] array */
   const int N=sizeof(streams.fp)/sizeof(FILE*);

   /* add the current FILE pointer to the list */
   if (fp && streams.n<N) {streams.fp[streams.n++]=fp; status=0;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to close all non-stdio streams and files                      */
/*------------------------------------------------------------------------*/
/**
   This function closes all non-stdio streams and files.  The stdio
   streams (STDIN,STDOUT,STDERR) remain open, valid, and unmolested.
   On success, this function returns zero or else EOF, on failure.
*/
int fcloseall(void)
{
   /* traverse the linked list of FILE pointers to close non-stdio streams */
   int status = _fwalk(_GLOBAL_REENT, fclose_nonstdio);

   /* close all non-buffered files */
   int err=_closeall();

   /* return zero on success */
   return ((status || err) ? EOF : 0);
}

/*------------------------------------------------------------------------*/
/* function to close all non-stdio streams                                */
/*------------------------------------------------------------------------*/
/**
   This function closes all non-stdio streams.  The stdio streams
   (STDIN,STDOUT,STDERR) remain open, valid, and unmolested.  On
   success, this function returns zero or else EOF, on failure.
*/
int fclose_streams(void)
{
   /* traverse the linked list of FILE pointers to close non-stdio streams */
   int status = _fwalk(_GLOBAL_REENT, fclose_nonstdio);

   /* return zero on success */
   return ((status) ? EOF : 0);
}

/*------------------------------------------------------------------------*/
/* function to close non-stdio FILE streams                               */
/*------------------------------------------------------------------------*/
/**
   This function tests a FILE object to determine if the stream is
   open, valid, and non-stdio; the stdio streams (STDIN,STDOUT,STDERR)
   remain open, valid, and unmolested.  The static linkage reflects
   the fact that it is the predicate function for _fwalk().  On
   success, this function returns zero or else EOF, on failure.
*/
static int fclose_nonstdio(FILE *fp)
{
   /* initialize the return value */
   int status=0;

   /* test if the FILE is open, valid, and not stdio; close non-stdio FILEs */
   if (fp->_flags != 0 && fp->_flags != 1 && fp->_file > STDERR_FILENO)
   {
      /* close the FILE stream */
      status = fclose(fp);
   }

   return status;
}

#endif /* STREAM_C */
