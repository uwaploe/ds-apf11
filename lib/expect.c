#ifndef EXPECT_H
#define EXPECT_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define expectChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>
#include <serial.h>

/* function prototypes */
int expect(const struct SerialPort *port, const char *prompt, const char *response, time_t sec, const char *trm);

#endif /* EXPECT_H */
#ifdef EXPECT_C
#undef EXPECT_C

#include <string.h>
#include <ctype.h>
#include "logger.h"

/*------------------------------------------------------------------------*/
/* function to respond to expected prompts                                */
/*------------------------------------------------------------------------*/
/**
   This function reads from a serial port until it receives an expected
   prompt and then it replies with a specified response.  It was designed to
   allow software to log into a computer.  If the expected prompt is not
   received then this function times out and returns.

      \begin{verbatim}
      input:

         port.......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks
                    to be sure this pointer is not NULL.

         prompt.....Bytes are read from the serial port until this
                    prompt-string is detected.

         response...Once the prompt-string is read, this response-string is
                    transmitted to the serial port.

         sec........The number of seconds this function will attempt to
                    match the prompt-string.

         trm........A termination string transmitted immediately after the
                    response string.  For example, if the termination string
                    is "\r\n" then the response string will be transmitted
                    first and followed immediately by the termination
                    string.  If trm=NULL or trm="" then no termination
                    string is transmitted.

      output:

         This function returns a positive number if the exchange was
         successful.  Zero is returned if the exchange failed.  A negative
         number is returned if the function parameters were determined to be
         ill-defined. 
      \end{verbatim}

   written by Dana Swift
*/
int expect(const struct SerialPort *port, const char *prompt,
           const char *response, time_t sec, const char *trm)
{
   /* define the logging signature */
   cc *FuncName = "expect()";

   /* initialize the return value */
   int status = -1;
  
   /* verify the serialport */
   if (!port) {LogEntry(FuncName,"Serial port not ready.\n");}

   /* verify the prompt */
   else if (!prompt) {LogEntry(FuncName,"NULL pointer to the prompt.\n");}

   /* verify that the prompt is not the empty string */
   else if (!(*prompt)) {LogEntry(FuncName,"Empty prompt string.\n");}

   /* verify the response */
   else if (!response) {LogEntry(FuncName,"NULL pointer to "
                                 "the response.\n");}

   /* verify the serial port's putb() function */
   else if (!port->putb) {LogEntry(FuncName,"NULL putb() function "
                                   "for serial port.\n");}

   /* verify the serial port's getb() function */
   else if (!port->getb) {LogEntry(FuncName,"NULL getb() function "
                                   "for serial port.\n");}

   /* verify the timeout period */
   else if (sec<=0) {LogEntry(FuncName,"Invalid time-out period: %ld\n",sec);}
   
   else
   {
      unsigned char byte; int n=0;

      /* get the reference time */
      time_t To=time(NULL),T=To;
         
      /* compute the length of the prompt string */
      int len=strlen(prompt);
      
      /* define the index of prompt string */
      int i=0;
      
      /* re-initialize the return value */
      status=0;

      /* make a log entry */
      if (debuglevel>=3 || (debugbits&EXPECT_H)) {LogEntry(FuncName,"Received: ");}

      do 
      {
         /* read the next byte from the serial port */
         if (port->getb(&byte)>0)
         {
            /* write the current byte to the logstream */
            if (debuglevel>=3 || (debugbits&EXPECT_H))
            {
               if (byte=='\r') LogAdd("\\r");
               else if (byte=='\n') LogAdd("\\n");
               else if (isprint(byte)) LogAdd("%c",byte);
               else LogAdd("[0x%02x]",byte);
            }
 
            /* check if the current byte matches the expected byte from the prompt */
            if (byte==prompt[i]) {i++;} else i=0;

            /* prompt string has been found if the index (i) matches its length */
            if (i>=len) {status=1; break;}

            /* don't allow too many bytes to be read between time checks */
            if (n<0 || n>25) {T=time(NULL); n=0;} else n++;
         }
         
         /* get the current time */
         else T=time(NULL);
      }

      /* check the termination conditions */
      while (T>=0 && To>=0 && difftime(T,To)<sec);
      
      /* write the response string if the prompt was found */
      if (status<=0)
      {
            /* terminate the prompt string */
            if (debuglevel>=3 || (debugbits&EXPECT_H)) LogAdd("\n");
         
            /* log the message */
            LogEntry(FuncName,"Attempt to read prompt "
                     "string [%s] failed.\n",prompt);
      }
      else 
      {
         /* validate the termination string */
         if (!trm) trm="";
         
         /* write the response string to the serial port */
         if (!response[0]) status=1;
         else status = (pputs(port,response,sec,trm)>0) ? 1 : 0;
         
         /* echo the response to the log stream */
         if (debuglevel>=3 || (debugbits&EXPECT_H))
         {
            int len=strlen(response);
            for (i=0; i<len; i++) 
            {
               char byte = response[i];
               if (byte=='\r') LogAdd("\\r");
               else if (byte=='\n') LogAdd("\\n");
               else if (isprint(byte)) LogAdd("%c",byte);
               else LogAdd("[0x%02x]",byte);
            }
         
            LogAdd("\n");
         }
      }
   }

   return status;
}

#endif /* EXPECT_C */
