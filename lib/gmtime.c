#ifndef GMTIME_H
#define GMTIME_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define gmtimeChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>

/* prototypes for functions with external linkage */
#if defined(__APF11__)
struct tm *gmtime(const time_t *sec);
#endif
struct tm *Tm(time_t sec, int isdst, struct tm *t);

#endif /* GMTIME_H */
#ifdef GMTIME_C
#undef GMTIME_C

#if defined(__APF11__)
#include <assert.h>
#include <string.h>

/* prototypes for local functions */
static int DaysTo(int year, int mon);

/* define the number of seconds between 1900 and the UNIX EPOCH */
static const unsigned long EPOCH = ((70*365UL + 17)*86400UL); 

/* define some conversion factors */
static const time_t DaysPerYear = 365L;
static const time_t SecsPerDay  = 86400L;
static const time_t SecsPerHour = 3600L;
static const time_t SecsPerMin  = 60L;

/* define the number of days to the beginning of each month of a leap-year */
static const short lmos[]={0,31,60,91,121,152,182,213,244,274,305,335};

/* define the number of days to the beginning of each month of a non leap-year */
static const short  mos[]={0,31,59,90,120,151,181,212,243,273,304,334};

/* macro to determine if a year is a leap year and return the correct table */
#define MONTAB(year) ((((year)&0x03) || (year)==0) ? mos : lmos)

/* definition used to get the weekday right */
#define WDAY 1

/*------------------------------------------------------------------------*/
/* function to compute the number of leap-days + year-days between dates  */
/*------------------------------------------------------------------------*/
/**
   This function computes the number of leap-days plus year-days between
   January 1, 1900 and a specified month & year.  This is an internal
   (static) function that is called by several other functions but is
   unlikely to be of direct use to users.

      \begin{verbatim}
      input:
      
         mon,year....These parameters represent the month and year of one
                     endpoint of the timespan of interest.  The other
                     endpoint is fixed at Jan 1, 1900.

      output:
      
         This function returns the number of leap-days plus the number of
         year-days between the endpoints of the timespan of interest.  The
         number of year-days is equal to the number of days between Jan 1
         and the first day of the month of 'year'.
         
      \end{verbatim}
*/
static int DaysTo(int year, int mon)
{
   int days=0;

   /* validate the month */
   assert(mon>=0 && mon<12);
   
   if (mon>=0 && mon<12) 
   {
      if (year>0) days = (year-1)/4;
      else if (year <= -4) days = 1 + (4-year)/4;
      
      days += MONTAB(year)[mon];
   }
      
   return days;
}


/*------------------------------------------------------------------------*/
/* function to convert calendar time to broken-down UTC time              */
/*------------------------------------------------------------------------*/
/**
   This function converts the calendar time (seconds) to broken-down time
   representation, expressed in Coordinated Universal Time (UTC).

      \begin{verbatim}
      input:
      
         seconds...When this pointer is dereferenced, the value represents
                   the number of seconds elapsed since 00:00:00 on Jan 1,
                   1970, Coordinated Universal Time (UTC).
         
      output:

         This function returns the broken-down time in a communal static
         data object.
         
      \end{verbatim}
*/
struct tm *gmtime(const time_t *seconds)
{
   /* define the calendar time to use if the function argument is NULL */
   const time_t Jan1970=0;

   /* make sure the pointer is valid */
   if (!seconds) seconds=&Jan1970;

   /* compute the broken-down time */
   return Tm(*seconds,0,NULL);
}

/*------------------------------------------------------------------------*/
/* function to convert calendar time to broken-down time                  */
/*------------------------------------------------------------------------*/
/**
   This function converts the calendar time (seconds) to broken-down time
   representation.

      \begin{verbatim}
      input:
      
         seconds...When this pointer is dereferenced, the value represents
                   the number of seconds elapsed since 00:00:00 on Jan 1,
                   1970, Coordinated Universal Time (UTC).

         isdst.....This parameter determines if daylight savings is in
                   effect.  A non-zero value indicates that DST is in effect
                   whereas a zero indicates that DST is not in effect.
         
      output:

         t.........If non-NULL, this structure will contain the broken-down
                    time corresponding to the 'seconds' argument.

         This function returns a pointer the object containing the
         broken-down time.  If the function parameter 't' is non-NULL then
         the this function returns a pointer to t.  Otherwise, broken-down
         time is stored in a communal static data object and the pointer to
         this static data object is returned.
         
      \end{verbatim}
*/
struct tm *Tm(time_t seconds, int isdst, struct tm *t)
{
   int year,mon;
   long int i,days=0;
   unsigned long sec;
   const short *pm;
   static struct tm ts;

   /* make sure that time_t has 4 bytes */
   assert(sizeof(time_t)==4);
   
   /* check if the conversion should be stored locally or passed back to the caller */
   if (!t) t = &ts;

   /* reference the time to the Jan 1, 1900 */
   if (seconds == (time_t)(-1)) {memset((void *)t,1,sizeof(struct tm)); return t;}
   else if (seconds<=0) {days=0; sec = EPOCH + seconds;}
   else {days = seconds/SecsPerDay; sec = EPOCH + seconds%SecsPerDay;}

   /* set the DST field */
   t->tm_isdst = (isdst>0) ? 1 : 0;

   /* correct for DST if it's in effect */
   if (t->tm_isdst) sec+=SecsPerHour;
   
   /* compute the number of days */
   days+=sec/SecsPerDay; 

   /* determine the weekday */
   t->tm_wday = (days+WDAY)%7;

   /* compute the year */
   for (year=days/365; days<(i=DaysTo(year,0)+DaysPerYear*year);) {--year;}

   /* subtract the number of days up to Jan 1 of the year determined above */
   days-=i;
      
   /* initialize the year and the day in the tm structure */
   t->tm_year=year; t->tm_yday=days; 

   /* compute the month */
   for (pm=MONTAB(year), mon=12; days < pm[--mon];) {} 

   /* initialize the month and the day in the tm structure */
   t->tm_mon=mon; t->tm_mday = days - pm[mon] + 1;

   /* compute the seconds left in the day */
   sec %= SecsPerDay;

   /* compute the number of hours left in the day */
   t->tm_hour = sec/SecsPerHour;

   /* compute the number of seconds left in the hour */
   sec %= SecsPerHour;

   /* compute the number of minutes left in the hour */
   t->tm_min = sec/SecsPerMin;

   /* compute the number of seconds left in the minute */
   t->tm_sec = sec % 60;

   return t;
}
#else

/*
 * Provide the functionality Tm() using standard POSIX gmtime function. This version will
 * be used when compiling the support software natively.
 */
struct tm *Tm(time_t seconds, int isdst, struct tm *t)
{
    return gmtime(&seconds);
}
#endif /* defined(__arm__) */

#endif /* GMTIME_C */
