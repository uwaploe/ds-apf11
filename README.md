# Apf-11 Software

This repository contains a modified version of Dana Swift's APF-11 software intended for the next generation EM-APEX floats.

## Download

``` shellsession
git clone git@bitbucket.org:uwaploe/ds-apf11.git
```

## Building

See BUILDING.md.


