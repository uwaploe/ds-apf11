ema-description.txt
2018-04-25 John Dunlap
2020-12-27 updated
2020-12-28 updated
2021-01-25 updated
2021-02-21 fixed mag straps 'M' description
2021-07-19 ZBS added description of EMA variables

Overview
--------

The "EMA" board set was designed to convert an APEX float to an
EM-APEX float made by Teledyne Webb Research.  The board set provides
electric field (EF) and compass data which can be processed to provide
estimates of east and north velocity components.  Data is provided in
64-byte binary data frames at about 1 Hz.  Communications are RS-232
compatible at 38,400 bps.  The EMA board set will run continuously
for more than two weeks using twelve (12) alkaline AA cells: 6 in series
for the micro and 6 center-tapped for the preamps.  When it is not
running it sleeps at very low power.

Description
-----------

The "EMA" controller board switches power to both electric field (EF)
preamps, operates the compass board and runs the analog to digital
converters (ADCs). The microprocessor is a Motorola (now Freescale)
MC9S08GB60 with 64KB flash memory for code and 2KB of random access
memory (RAM) for a data FIFO (first-in/first-out) buffer.

Two channels of orthogonal electric field (EF) are sampled using two
Analog Devices AD7791 24-bit sigma-delta ADCs.  Compass channels are
sampled using the internal ADCs of the MC9S08GB60.

The first stage of the two EF preamplifiers each use two Analog
Devices AD8551 operational amplifier chips in an instrumentation
amplifier configuration.  The two EF channels are each sampled using
separate Analog Devices AD7791 24-bit sigma-delta ADCs every 0.1024
s. Every 10 samples are block averaged every 1.024 s.

The compass is composed of magnetic field sensors acceleration sensors.
One each of Honeywell HMC1002 and HMC1001 giant magneto-resistive
(GMR) sensors provide three orthogonal channels of magnetic field components.
An Analog Devices ADXL311 provides three orthogonal channels of
acceleration which can be used as tilt sensors.  To reduce average
power the compass is turned on briefly and burst sampled.  It's turned
on every 1.024 s for about 100 ms while all sensors are sampled 7
times then averaged.

The above averages and associated standard deviations are stored in
a first-in-first-out (FIFO) buffer which uses most of the 2KB RAM.
The FIFO fills in about 25 s.  The host computer requests each 1.024-s
record separately and receives a packet with binary values
followed by a 16-bit cyclic redundancy check (CRC).

Optical isolation from the host is provided using three Fairchild
6N139 opto-couplers.  Two optos handle bidirectional data (TxD) and
control (RxD) compatible with RS-232.  The other opto is used to wake
the microprocessor from its very low-power sleep using an interrupt
request (IRQ) line.

Both RxD and IRQ inputs use XN4312 chips.
The TxD output uses an 74LVC1G04.

Protocol
-------

Host sends 'S' character to put the EMA controller in very low power stop.
  Host responds with "\r\nSTOP1\r\nxxxxx\r\n"

The EMA controller will also enter very low power stop if it receives
no requests for 8 minutes.

Every 10 to 20 s get all available data frames from EMA controller:
The EMA controller FIFO overflows in 25 s.

  Set EMA IRQ line low
  Apply power to the EMA controller 
  Turn on host's RS232 buffers
  Wait 50 ms
  Enable interrupts for host's UART
  Set EMA IRQ line high for 1 ms to wake it from very low-power sleep

  If the controller was stopped it responds with ASCII version 
  terminated with CR/LF.  Save this to the logging file with time stamp.

  loop until EMA FIFO empty:
    Host sends 'F' character to request a data frame.
    Host reads 1 byte into buf[0], if not 'F' then error.
    Host reads adding next 8 bytes starting at buf[1].
    if buf is "F EMPTY\r\n" or "\033[H\033[J\017E" then exit loop.
    Host reads adding next 55 bytes into buf adding them after the first 9 bytes.
    Compute CRC16_CCITT on 63 bytes starting at buf[1] -- should be zero.
    Record entire response including CRC along with host's time-stamp.
    See 'F' data frame description below.

  Just after receiving "F EMPTY\r\n" and 60 s or more have elapsed:
    Host sends 'M' character to request magnetic compass calibration.
    Host reads 1 byte into buf[0], if not 'M' then error.
    Host reads buffer starting at buf[1] until CR/LF -- buf should 
      have 117 bytes including the initial 'M' and final CR/LF.
    Convert the 114 bytes between the 'M' and the CR from hex to binary.
    Compute CRC16_CCITT of the resulting 57 binary bytes -- expect zero.
    Record entire response along along with host's time-stamp.
    See 'M' data frame description below.

  After getting each group of the data from the EMA one may turn off
  the power to the opto-couplers on the EMA board.

'F' Data Frame -- 63 bytes
--------------------------

  Contents of each unsigned char buf[63]
    Age      = buf[0]             -- number of frames still in FIFO
    OverFlow = buf[1]<<8 | buf[2]
    SeqNo    = buf[3]<<8 | buf[4] -- starts at zero and wraps at 2^16-1

    ZR_nsum  = buf[5]                -- should be zero
    ZR_sum   = buf[6]<<8 | buf[7]
    ZR_pk2pk = buf[8]<<8 | buf[9]

    BT_nsum  = buf[10]               -- battery voltage
    BT_sum   = buf[11]<<8 | buf[12]
    BT_pk2pk = buf[13]<<8 | buf[14]

    HZ_nsum  = buf[15]               -- vertical magnetic field
    HZ_sum   = buf[16]<<8 | buf[17]
    HZ_pk2pk = buf[18]<<8 | buf[19]

    HY_nsum  = buf[20]               -- horizontal magnetic field
    HY_sum   = buf[21]<<8 | buf[22]  
    HY_pk2pk = buf[23]<<8 | buf[24]

    HX_nsum  = buf[25]               -- horizontal magnetic field
    HX_sum   = buf[26]<<8 | buf[27]
    HX_pk2pk = buf[28]<<8 | buf[29]

    AZ_nsum  = buf[30]               -- vertical acceleration (gravity)
    AZ_sum   = buf[31]<<8 | buf[32]
    AZ_pk2pk = buf[33]<<8 | buf[34]

    AY_nsum  = buf[35]               -- horizontal acceleration
    AY_sum   = buf[36]<<8 | buf[37]
    AY_pk2pk = buf[38]<<8 | buf[39]

    AX_nsum  = buf[40]               -- horizontal acceleration
    AX_sum   = buf[41]<<8 | buf[42]
    AX_pk2pk = buf[43]<<8 | buf[44]
    
    E1_nsum  = buf[45]               -- channel 1 electric field
    E1_sum   = buf[46]<<24 | buf[47]<<16 | buf[48]<<8 | buf[49]
    E1_pk2pk = buf[50]<<16 | buf[51]<<8  | buf[52] 

    E2_nsum  = buf[53]               -- channel 2 electric field
    E2_sum   = buf[54]<<24 | buf[55]<<16 | buf[56]<<8 | buf[57]
    E2_pk2pk = buf[58]<<16 | buf[59]<<8  | buf[60] 

    CRC = buf[61]<<8 | buf[62]       -- 16-bit Xmodem CRC

    ZBS clarification (from emaxg-20160218/src/emapro1.c, lines 559-680
    The variables ending in _sum are the sum of that variable for the
      sampling interval.
    The variables ending in _nsum are the number of terms added together,
      such that X_sum/X_nsum gives the average value (see 
    The variables ending in _pk2pk are the peak-to-peak values over the
      sampling interval, or the maximum value in the interval minus the
      minimum value.

    ZBS: clarification from gemcvel.py:
    Because age ("number of frames remaining in the buffer") is close to
    the 1.024 Hz averaging interval, it can be used to subtract from the time
    the EMA buffer is dumped (created by the receiving micro) to generate the
    time the sample was taken.

'M' Data Frame -- 57 bytes
--------------------------

  avg -- average of nsum A/D conversions in each state
  max -- maximum
  min -- minimum

  _bef -- before first mag strap current pulse
  _dur -- between first and second current pulses
  _aft -- after second opposite current pulse

  nsum = buf[0]

  hzavg_bef = float(buf[1] <<8 | buf[2])  / float(nsum)
  hyavg_bef = float(buf[3] <<8 | buf[4])  / float(nsum)
  hxavg_bef = float(buf[5] <<8 | buf[6])  / float(nsum)
  hzavg_dur = float(buf[7] <<8 | buf[8])  / float(nsum)
  hyavg_dur = float(buf[9] <<8 | buf[10]) / float(nsum)
  hxavg_dur = float(buf[11]<<8 | buf[12]) / float(nsum)
  hzavg_aft = float(buf[13]<<8 | buf[14]) / float(nsum)
  hyavg_aft = float(buf[15]<<8 | buf[16]) / float(nsum)
  hxavg_aft = float(buf[17]<<8 | buf[18]) / float(nsum)

  hzmax_bef = buf[19]<<8 | buf[20]
  hymax_bef = buf[21]<<8 | buf[22]
  hxmax_bef = buf[23]<<8 | buf[24]
  hzmax_dur = buf[25]<<8 | buf[26]
  hymax_dur = buf[27]<<8 | buf[28]
  hxmax_dur = buf[29]<<8 | buf[30]
  hzmax_aft = buf[31]<<8 | buf[32]
  hymax_aft = buf[33]<<8 | buf[34]
  hxmax_aft = buf[35]<<8 | buf[36]

  hzmin_bef = buf[37]<<8 | buf[38]
  hymin_bef = buf[39]<<8 | buf[40]
  hxmin_bef = buf[41]<<8 | buf[42]
  hzmin_dur = buf[43]<<8 | buf[44]
  hymin_dur = buf[45]<<8 | buf[46]
  hxmin_dur = buf[47]<<8 | buf[48]
  hzmin_aft = buf[49]<<8 | buf[50]
  hymin_aft = buf[51]<<8 | buf[52]
  hxmin_aft = buf[53]<<8 | buf[54]

  crc = buf[55]<<8 | buf[56]

Using the data to compute velocity
----------------------------------

See emavel.py in https://ohm.apl.uw.edu/~emapex/software/emapy.zip
  

EM-APEX code
------------

Source code for EM-APEX shows processing details in the float. See:
https://ohm.apl.uw.edu/~dunlap/wrc/emaxag-20200910/emaxag-src.tgz
Interaction with EMA hardware -- EmaAcquire() in src/emapro4.c
Sinusoidal least squares fitting -- ema_process(), efiq_pro() in src/efpro.c
Xmodem CRC -- updcrc() in lib/crctab.c

Shore-side processing can be found at:
https://ohm.apl.uw.edu/~emapex/software/emapy.zip
Decode binary data sent via Iridium -- emadec.py
Velocity computation -- emavel.py

EMA hardware
------------

Hardware schematics are at:
https://ohm.apl.uw.edu/~dunlap/pdf/emapex/EMA-3.9/
This one shows the digital interface:
https://ohm.apl.uw.edu/~dunlap/pdf/emapex/EMA-3.9/EMAPEX3.9_schem4.pdf
For other boards see:
https://ohm.apl.uw.edu/~dunlap/pdf/emapex/XFER-to-WRC/

Cable for connection to PC serial port
--------------------------------------

Here is the pinout for a cable which I've used between a PC and the EMA
board.  The power for the EMA board isolation circuits come from DTR.
RTS is used to wake the EMA board from its super-low power sleep.

PC DE9P  DE9S            J5 EMA 
                         Molex 14-60-0072
--------------------------------
COM 5 --- 5  --- GRN ---  2 COM
RxD 2 --- 2  <-- WHT <--  3 TxD
CTS 8 --- 8  <-- BLU <--  4 n/c
TxD 3 --- 3  --> ORG -->  5 RxD
RTS 7 --- 7  --> BRN -->  6 IRQ
DTR 4 --- 4  --> RED -->  7 PWR
