// dsleq.c
/* from dsleq.f -- translated by f2c (version of 16 May 1991  13:06:06).  */

#include <stdlib.h>
#include <math.h>

#include "dsleq.h"

/*       DSLEQ        DUMMY VERSION CARD-LIB SYST START   WJS  77/05/06 */
void dsleq (aprxfloat *a, short *i1, short *i2, short *n, aprxfloat *asol, short *npq) {
  
  /* System generated locals */
  short a_dim1, a_offset, i__1, i__2;
  aprxfloat d__1;

  /* Local variables */
  short ibig, jbig, idum, i, j, k, l, k1;
  aprxfloat ab;
  short ic[28], kk;
  aprxfloat ax, ay;
  short nm, nn, mm, nm1, np1;
  aprxfloat coe[29];
  short kkk, nnn;
  aprxfloat sum;

/*SOLVES THE MATRIX EQUATION  (A)X(ASOL)=(B)  WHERE A IS NXN, ASOL IS   DSLE0001*/
/*  NX1, B IS NX1                                                       DSLE0002*/
/*    A IS (N)X(N+1) COEFFICIENT MATRIX; COLUMN N+1 IS B                DSLE0003*/
/*    I1,I2 ARE DIMENSIONS OF ARRAY A AS DECLARED IN USER'S MAIN PROGRAMDSLE0004*/
/*    ASOL IS SOLUTION                                                  DSLE0005*/
/*    N IS ORDER OF MATRIX, MUST BE NO GREATER THAN 28                  DSLE0006*/
/*    NPQ IS  0 IF MATRIX IS SINGULAR                                   DSLE0007*/
/*           +1 OTHERWISE                                               DSLE0008*/
/*      IF NPQ= 0, ASOL(I) ARE SET TO 0.0, ALL I                        DSLE0009*/
/*ARRAYS MUST BE DIMENSIONED AT LEAST AS FOLLOWS: ASOL(N),COE(N+1),IC(N)DSLE0010*/
/* A IS DESTROYED IN COMPUTATION                                        DSLE0011*/
/*TAKEN FROM UNIVERSITY OF KANSAS COMPUTER CONTRIBUTION 3, O'LEARY,     DSLE0012*/
/*  LIPPERT, SPITZ, 1966                                                DSLE0013*/

  /* Parameter adjustments */
  --asol;
  a_dim1 = *i1;
  a_offset = a_dim1 + 1;
  a -= a_offset;

  /* Function Body */
  *npq = 1;
/* L12: */
  nm = *n;
  nn = 0;
  kk = 0;
  mm = 0;
  np1 = *n + 1;
  nm1 = *n - 1;
  i__1 = *n;
  for (j = 1; j <= i__1; ++j)
  {
/* L3: */
    a[j + np1 * a_dim1] = -a[j + np1 * a_dim1];
  }
/*INITIALIZE SUBSCRIPT COLUMN                                           DS
LE0026*/
/* L799: */
  i__1 = *n;
  for (j = 1; j <= i__1; ++j)
  {
/* L800: */
    ic[j - 1] = j;
  }
  kkk = 0;
/*MATRIX ORDERING ROUTINE                                               DS
LE0030*/
/* REARRANGE MATRIX SO THAT FOR FIRST STEP LARGEST COEFFICIENT IS IN    DS
LE0031*/
/* POSITION (1,1).  FOR SECOND STEP LARGEST COEFFICIENT IN REMAINING    DS
LE0032*/
/* ROWS AND COLUMNS IS IN POSITION (2,2).  ETC.                         DS
LE0033*/
L999:
  ++kkk;
  ab = (d__1 = a[kkk + kkk * a_dim1], fabs (d__1));
  ibig = kkk;
  jbig = kkk;
/* FIND COEFFICIENT LARGEST IN ABSOLUTE VALUE                           DS
LE0038*/
  i__1 = *n;
  for (i = kkk; i <= i__1; ++i)
  {
    i__2 = *n;
    for (j = kkk; j <= i__2; ++j)
    {
      if (ab - (d__1 = a[i + j * a_dim1], fabs (d__1)) >= 0.)
      {
        goto L901;
      }
      else
      {
        goto L900;
      }
    L900:
      ab = (d__1 = a[i + j * a_dim1], fabs (d__1));
      ibig = i;
      jbig = j;
    L901:
      ;
    }
  }
/* INTERCHANGE ROWS KKK AND IBIG                                        DS
LE0046*/
/* L910: */
  i__2 = np1;
  for (i = 1; i <= i__2; ++i)
  {
    ax = a[kkk + i * a_dim1];
    a[kkk + i * a_dim1] = a[ibig + i * a_dim1];
/* L920: */
    a[ibig + i * a_dim1] = ax;
  }
/* INTERCHANGE COLUMNS KKK AND JBIG                                     DS
LE0051*/
  i__2 = *n;
  for (j = 1; j <= i__2; ++j)
  {
    ay = a[j + kkk * a_dim1];
    a[j + kkk * a_dim1] = a[j + jbig * a_dim1];
/* L930: */
    a[j + jbig * a_dim1] = ay;
  }
/* IC(J)  IS ORIGINAL INDEX OF COLUMN WHICH IS NOW IN J-TH POSITION     DS
LE0056*/
/* L940: */
  idum = ic[kkk - 1];
  ic[kkk - 1] = ic[jbig - 1];
  ic[jbig - 1] = idum;
  if (nm1 - kkk <= 0)
  {
    goto L71;
  }
  else
  {
    goto L999;
  }
L71:
L75:
  ++nn;
  nnn = nn + 1;
  ++mm;
/*CHECK FOR SINGULAR MATRIX                                             DS
LE0065*/
  if (a[nn + nn * a_dim1] != 0.)
  {
    goto L77;
  }
  else
  {
    goto L1700;
  }
/*MATRIX SOLUTION ROUTINE                                               DS
LE0067*/
/* DIVIDE REST OF EACH ROW BY NN COEFFICIENT FOR THAT ROW               DS
LE0068*/
L77:
  i__2 = *n;
  for (i = nn; i <= i__2; ++i)
  {
    if (a[i + nn * a_dim1] != 0.)
    {
      goto L79;
    }
    else
    {
      goto L81;
    }
  L79:
    i__1 = np1;
    for (j = nnn; j <= i__1; ++j)
    {
      a[i + j * a_dim1] /= a[i + nn * a_dim1];
/* L80: */
    }
  L81:
    ;
  }
  ++kk;
  if (kk - nm1 <= 0)
  {
    goto L85;
  }
  else
  {
    goto L100;
  }
/* SUBTRACT REST OF ROW NN FROM SUCCEEDING ROWS                         DS
LE0077*/
L85:
  i__2 = *n;
  for (i = nnn; i <= i__2; ++i)
  {
    if (a[i + nn * a_dim1] != 0.)
    {
      goto L89;
    }
    else
    {
      goto L95;
    }
  L89:
    i__1 = np1;
    for (j = nnn; j <= i__1; ++j)
    {
      a[i + j * a_dim1] -= a[nn + j * a_dim1];
/* L90: */
    }
  L95:
    ;
  }
  if (kk - nm1 + 1 <= 0)
  {
    goto L92;
  }
  else
  {
    goto L75;
  }
L92:
  kkk = mm;
  goto L999;
/*BACK SOLVE UPPER TRIANGULAR MATRIX                                    DS
LE0087*/
L100:
  coe[np1 - 1] = (aprxfloat) 1.;
  i__2 = nm;
  for (k = 1; k <= i__2; ++k)
  {
    sum = (aprxfloat) 0.;
    j = np1 - k;
    l = j + 1;
    i__1 = np1;
    for (i = l; i <= i__1; ++i)
    {
/* L109: */
      sum -= a[j + i * a_dim1] * coe[i - 1];
    }
/* L110: */
    coe[j - 1] = sum;
  }
/*REORDER ANSWER COLUMN                                                 DS
LE0096*/
  i__2 = nm;
  for (i = 1; i <= i__2; ++i)
  {
    k1 = ic[i - 1];
    asol[k1] = coe[i - 1];
/* L1005: */
  }
/* L1600: */
  return;
L1700:
  *npq = 0;
/* L1601: */
  i__2 = *n;
  for (i = 1; i <= i__2; ++i)
  {
/* L1900: */
    asol[i] = (aprxfloat) 0.;
  }
  return;
}                               /* dsleq */
