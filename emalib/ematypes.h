#ifndef EMA_TYPES_H
#define EMA_TYPES_H

// these are the raw types coming out of the ema device

#include <time.h>

typedef struct {
  // the timestamp and piston are not part of the data stream
  time_t timestamp;
  unsigned short piston;

  unsigned char age;
  short overflow;
  short seqno;

  unsigned char zr_nsum;
  short zr_sum;
  short zr_pk2pk;

  unsigned char bt_nsum;
  short bt_sum;
  short bt_pk2pk;

  unsigned char hz_nsum;
  short hz_sum;
  short hz_pk2pk;

  unsigned char hy_nsum;
  short hy_sum;
  short hy_pk2pk;

  unsigned char hx_nsum;
  short hx_sum;
  short hx_pk2pk;

  unsigned char az_nsum;
  short az_sum;
  short az_pk2pk;

  unsigned char ay_nsum;
  short ay_sum;
  short ay_pk2pk;

  unsigned char ax_nsum;
  short ax_sum;
  short ax_pk2pk;

  unsigned char e1_nsum;
  int e1_sum;
  int e1_pk2pk;

  unsigned char e2_nsum;
  int e2_sum;
  int e2_pk2pk;

  short crc;
  
} EMADataFrame;

typedef struct {
  // the timestamp is not part of the data stream
	time_t timestamp;

	unsigned char nsum;
	
	float hzavg_bef;
	float hyavg_bef;
	float hxavg_bef;
	float hzavg_dur;
	float hyavg_dur;
	float hxavg_dur;
	float hzavg_aft;
	float hyavg_aft;
	float hxavg_aft;

	short hzmax_bef;
	short hymax_bef;
	short hxmax_bef;
	short hzmax_dur;
	short hymax_dur;
	short hxmax_dur;
	short hzmax_aft;
	short hymax_aft;
	short hxmax_aft;

	short hzmin_bef;
	short hymin_bef;
	short hxmin_bef;
	short hzmin_dur;
	short hymin_dur;
	short hxmin_dur;
	short hzmin_aft;
	short hymin_aft;
	short hxmin_aft;

	short crc;
	
} EMACompassCalibration;

#endif
