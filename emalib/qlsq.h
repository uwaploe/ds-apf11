#ifndef QLSQ_H
#define QLSQ_H

typedef float aprxfloat;
void qlsq(
  aprxfloat *d,
  short *i1, short *n,
  aprxfloat *r,
  short *ne, short *no,
  aprxfloat *sfac, aprxfloat *sig,
  short *lop, short *lmax,
  aprxfloat *smin,
  aprxfloat *smax,
  short *ist, short *mt,
  aprxfloat *a,
  short *noi,
  aprxfloat *f, short *ndim, short *nfun, aprxfloat *bad
);

#endif /* QLSQ_H */
