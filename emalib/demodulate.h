#ifndef DEMODULATE_H
#define DEMODULATE_H

#include "ematypes.h"

// the data structure used to input to demodulate
typedef struct {
  time_t uxt;
  
  int pc; // unknown size
  
  unsigned char age;
  short overflow;
  short seqno;
  
  float zr;
  short zr_pk2pk;

  float bt;
  short bt_pk2pk;

  float hz;
  short hz_pk2pk;

  float hy;
  short hy_pk2pk;

  float hx;
  short hx_pk2pk;

  float az;
  short az_pk2pk;

  float ay;
  short ay_pk2pk;

  float ax;
  short ax_pk2pk;

  float e1;
  int e1_pk2pk;

  float e2;
  int e2_pk2pk;
  
  int pr; // unknown size
  int uxtpr; // unknown size
  
} EFRDataFrame;

// the processed result of the demodulation
typedef struct {
  time_t time;

  float rotp_hx;
  float rotp_hy;

  float piston_c0;

  float e1_mean4;
  float e2_mean4;
  float e1sdev4;
  float e2sdev4;

  float e1coef40;
  float e1coef41;

  float e2coef40;
  float e2coef41;

  float hxdt_sdev;
  float hydt_sdev;

  float bt_mean;
  float hz_mean;
  float ax_mean;
  float ay_mean;
  float az_mean;

  float ax_sdev;
  float ay_sdev;

  float hx_mean;
  float hy_mean;
  
} EFPDataFrame;

void demodulate(EMADataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount);
void demodulate_efr(EFRDataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount);
void demodulate_ema(EMADataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount);
int getProcessedCountFromRawCount(int count);

#endif