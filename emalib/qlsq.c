/* from dqlsq1.f -- translated by f2c (version of 16 May 1991  13:06:06).  */
#include <math.h>

#include "qlsq.h"
#include "dsleq.h"

/* Table of constant values */

# if 0                          // JHD
# define MFUN10 10
# define MFUN11 11
# define MFUN_C 60
# define MFUN_G 110
# else // JHD for apf9
# define MFUN10 4
# define MFUN11 5
# define MFUN_C 11              // MFUN10 factorial + 1
# define MFUN_G 20              // MFUN10 * MFUN11
# endif

static short c__10 = MFUN10;
static short c__11 = MFUN11;

void qlsq(
  aprxfloat * d,
  short *i1, short *n,
  aprxfloat * r,
  short *ne, short *no,
  aprxfloat * sfac, aprxfloat * sig,
  short *lop, short *lmax,
  aprxfloat * smin,
  aprxfloat * smax,
  short *ist, short *mt,
  aprxfloat * a,
  short *noi, aprxfloat * f, short *ndim, short *nfun, aprxfloat * bad
) {
  
  /* System generated locals */
  short i__1, i__2, i__3;
  aprxfloat d__1;

  /* Local variables */
  aprxfloat slim;
  aprxfloat b[MFUN10], c[MFUN_C], g[MFUN_G] /* was [10][11] */ ;
  short i, j, k, l;
  aprxfloat s;
  short i2;
  aprxfloat aa[MFUN10], dd, ff;
  short ij, ik;
  aprxfloat av;
  short nt;
  aprxfloat rr, ss;
  short ind;
  aprxfloat var;
  short imax;

/* double precision version of qlsq */

/*qlsq computes the coefficients, a(j), of approximating functions, f(i,j)
*/
/*    which best fit the data, d(i). */
/* d ... input data.  Bad data is flagged by with a value equal bad. */
/* n ... number of values of d to use. */
/* r ... residue array = data minus fit */
/* ne .. number of good values needed for a good return status. */
/* no .. number of good (ok) values used in a fit. */
/* sfac  standard deviation factor used in eliminating wild points. */
/*       any residue value larger than sfac*sig is flagged bad and */
/*       eliminated from the fit. */
/* sig . standard deviation returned. */
/* lop . number of loops/fits executed. */
/* lmax  maximum number of loops/fits allowed. */
/* ist . status: 1=good return */
/*               2=not enough good data */
/*               3=determinant of fitting matrix zero. */
/*               4=sig>smax after lmax */
/*               5=bad is zero */
/*               6=nfun too large */
/* mt .. number of d values tossed out */
/* a ... coeffients of f returned which best fit d. */
/* noi . number of values ok initially */
/* f ... approximating functions */
/* ndim  first fortran dimension of f */
/* nfun  number of functions;  limited to 4 by the dimensions of aa,b,c,g.
 */
/* bad . bad-flag value.  CANNOT be zero. */
/* initialize */
  /* Parameter adjustments */
  --f;
  --a;
  --r;
  --d;

  /* Function Body */
  if (*nfun <= MFUN10)
  {
    goto L1;
  }
  *ist = 6;
  return;
L1:
  if (*bad != 0.)
  {
    goto L2;
  }
  *ist = 5;
  return;
L2:
  i2 = *i1 - 1 + *n;
  l = 1;
  *lop = 0;
  nt = 0;
  *mt = 0;
  *no = 0;
  *sig = (aprxfloat) 0.;
  *ist = 0;

  i__1 = *nfun;
  for (j = 1; j <= i__1; ++j)
  {
    a[j] = (aprxfloat) 0.;
    b[j - 1] = (aprxfloat) 0.;
    i__2 = j;
    for (k = 1; k <= i__2; ++k)
    {
      //WatchDog ();
      c[l - 1] = (aprxfloat) 0.;
      ++l;
      /* L9: */
    }
    /* L10: */
  }
  if (l > MFUN_C)
  {
    *ist = 7;
    return;
  }

/* first calc of c,b */

  i__1 = i2;
  for (i = *i1; i <= i__1; ++i)
  {
    //WatchDog ();
    dd = d[i];
    if (dd != *bad)
    {
      goto L12;
    }
    r[i] = *bad;
    goto L30;
  L12:
    l = 1;
    ij = i;
    ++(*no);
    r[i] = (aprxfloat) 0.;
    i__2 = *nfun;
    for (j = 1; j <= i__2; ++j)
    {
      ff = f[ij];
      ij += *ndim;
      b[j - 1] += ff * dd;
      ik = i;
      i__3 = j;
      for (k = 1; k <= i__3; ++k)
      {
        //WatchDog ();
        c[l - 1] += ff * f[ik];
        ik += *ndim;
        ++l;
        /* L19: */
      }
      /* L20: */
    }
  L30:
    ;
  }
  *noi = *no;
L35:
/* Computing MAX */
  i__1 = *ne, i__2 = *nfun + 1;
  if (i__1 > i__2)
    imax = i__1;
  else
    imax = i__2;
  if (*no >= imax)
  {
    goto L36;
  }
  *ist = 2;
  return;
L36:
/* load c and b into g */
  l = 1;
  i__1 = *nfun;
  for (j = 1; j <= i__1; ++j)
  {
    g[j + (*nfun + 1) * MFUN10 - MFUN11] = b[j - 1];
    i__2 = j;
    for (k = 1; k <= i__2; ++k)
    {
      //WatchDog ();
      g[j + k * MFUN10 - MFUN11] = c[l - 1];
      g[k + j * MFUN10 - MFUN11] = g[j + k * MFUN10 - MFUN11];
      ++l;
/* L39: */
    }
/* L40: */
  }
/* invert matrix g and calc coeff aa */
  dsleq (g, &c__10, &c__11, nfun, aa, &ind);
  if (ind == 1)
  {
    goto L41;
  }
  *ist = 3;
  return;
L41:
/* convert aa to aprxfloat: a. */
  i__1 = *nfun;
  for (j = 1; j <= i__1; ++j)
  {
    a[j] = aa[j - 1];
/* L50: */
  }

/* calculate residues */

  ss = (aprxfloat) 0.;
  s = (aprxfloat) 0.;
  i__1 = i2;
  for (i = *i1; i <= i__1; ++i)
  {
    //WatchDog ();
    if (r[i] == *bad)
    {
      goto L70;
    }
    ij = i;
    rr = d[i];
    i__2 = *nfun;
    for (j = 1; j <= i__2; ++j)
    {
      //WatchDog ();
      rr -= a[j] * f[ij];
      ij += *ndim;
/* L60: */
    }
    ss += rr * rr;
    s += rr;
    r[i] = rr;
  L70:
    ;
  }
/* standard deviation */
  av = s / *no;
  var = ss / (*no - *nfun) - av * s / (*no - *nfun);
  *sig = (aprxfloat) 0.;
  if (var > 0.)
  {
    *sig = sqrt (var);
  }
  slim = *sfac * *sig;
/* tests for terminating computation */
  if (*lop < *lmax)
  {
    goto L100;
  }
L80:
  if (*sig < *smax)
  {
    goto L200;
  }
  *ist = 4;
  return;
/* test fabs(residues) .lt. slim.  if not set res = bad */
L100:
  if (*sig < *smin)
  {
    goto L140;
  }
  nt = 0;
  i__1 = i2;
  for (i = *i1; i <= i__1; ++i)
  {
    //WatchDog ();
    rr = r[i];
    if (rr == *bad)
    {
      goto L130;
    }
    if ((d__1 = rr - av, fabs (d__1)) < slim)
    {
      goto L130;
    }
/* discard a point by modifying c,b and redoing matrix inversion */
    ++nt;
    --(*no);
    l = 1;
    ij = i;
    r[i] = *bad;
    i__2 = *nfun;
    for (j = 1; j <= i__2; ++j)
    {
      ff = f[ij];
      b[j - 1] -= ff * d[i];
      ij += *ndim;
      ik = i;
      i__3 = j;
      for (k = 1; k <= i__3; ++k)
      {
        //WatchDog ();
        c[l - 1] -= ff * f[ik];
        ik += *ndim;
        ++l;
/* L119: */
      }
/* L120: */
    }
  L130:
    ;
  }
L140:
  *mt += nt;
  if (nt <= 0)
  {
    goto L131;
  }
  ++(*lop);
  goto L35;
L131:
  goto L80;
/* good return */
L200:
  *ist = 1;
  return;
}                               /* dqlsq_ */
