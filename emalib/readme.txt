This commit is to verify numbers before adding to the main float build.

Compile with:

 $> gcc -o demodulate qlsq.c dsleq.c demodulate.c demodmain.c

The executable can take two different kinds of files: efr and csv, e.g.,
ema-7496a-0001-efr.txt and ema-19700101T000128.csv, respectively.  Both
will produce the same demodulated output to stdout which can then be
redirected to a file.

If using the 'efr' type file then add a 1 to the command line:

 $> ./demodulate ema-7496a-0001-efr.txt 1
 
And if using the 'csv' type then add a 2:

 $> ./demodulate ema-19700101T000128.csv 2
 
For the 'efr' type there are currently 'efp' (i.e., processed) examples
in the module directory to compare against.

Changes:

 * cleaned up the code to have a more consistent style (but barely touched
 the dsleq and qlsq files)
 
 * removed any extraneous files, functions, and comments (but I'm sure there
 are more)
 
 * rewrote the efr parsing, including adding fields as necessary, but barely
 touched any of the algorithms other than for style
 
 * passed in the unfilled processed data structures to the demodulate routines
 
 * printing out results instead of writing to a file
 
 * separated the main() into a separate file so we can use demodulate.c
 directly on the float
 
Future:

 * once the algorithms are confirmed correct turn this into a real module for
 the float to use (so remove any of the test files)
 
 * create a separate directory storing test files and include some sort of
 test makefile
 
 * move the existing ema.c file into this module


===> below was for initial commit

compile with:
gcc -o demodulate demodulate.c dsleq.c qlsq.c

The program demodulate.c takes an input file that is in EMA output format, 
performs demodulation (numerically accuracy remains to be tested), and outputs a 
filename that is the input filename + '.demod' that is close to the 'efp' format 
from John's APF-9 firmware, e.g. proc/dec/7496a/ema-7496a-0001-efp.txt.


the files efpro.c, dsleq.c/.h, and qlsq.c/.h are from John Dunlap's APF-9 
firmware, specifically from emaxag-20160218/src/.  These are the starting 
functions for getting a new demodulation function working.  The additional file 
emaproc.h is from emaxag-20200430/src/emaproc.h - I don't know why it is included 
in the emaxg-20160218/src/ c programs but isn't found in the same directory, maybe 
John put it in a different directory tree for that build?

efpro_pruned.c is a modified version of efpro.c that removes all lines that are 
removed the compiler, for a cleaner file for reading.   

The files ema-7496a-0001-efr.txt and ema-7496a-0001-efp.txt (plus -ctd.txt, 
-hol.txt, and -scp.txt) are from a historic mission, and are respectively the raw 
data direct from the EMA board, and the demodulated version of the same data (but 
full profile).  They will be used to confirm the calculation performed by 
demodulate.c in future , which will require a different input format to parse the 
efr data stream.  The efr data is from UXT 1492683838 to 1492684241, which, 
compared against ema-7496a-0001-ctd.txt, is during the descent for depths 37 to 99 
dbar.  

archive created with:
tar -czf demodulate_archive.txt demodulate.c demodulate_readme dsleq.c dsleq.h 
efpro.c efpro_pruned.c ema-19700101T00* ema-7496a-0001-* emaproc.h qlsq.c qlsq.h 
