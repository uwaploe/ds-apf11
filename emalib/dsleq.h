#ifndef DSLEQ_H
#define DSLEQ_H

typedef float aprxfloat;
void dsleq (aprxfloat *a, short *i1, short *i2, short *n, aprxfloat *asol, short *npq);

#endif /* DSLEQ_H */
