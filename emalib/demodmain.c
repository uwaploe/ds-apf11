#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "demodulate.h"

typedef enum { EFR = 1, CSV } RawType;
typedef enum { STREAM = 1, FULL } ProcessType;

#define SAMPLE_WINDOW_SIZE 50
#define SAMPLE_SLIDE_SIZE 25

#define EFR_MAX_LINE 150
#define EFR_COL_COUNT 27
#define CSV_COL_COUNT 35

void printProcessedDataFrame(int idx, EFPDataFrame* efp) {
  printf(
    "%d, %5.3f,%5.3f,%.1f, %.1f,%.1f,%.1f,%.1f, %.1f,%.1f,%.1f,%.1f, %.2f,%.2f,%.2f,%.2f, %.2f,%.2f,%.2f,%.2f, %.2f,%.2f,%.2f,%ld\n",
    idx,
    efp->rotp_hx, efp->rotp_hy, efp->piston_c0,
    efp->e1_mean4, efp->e1sdev4, efp->e1coef40, efp->e1coef41,
    efp->e2_mean4, efp->e2sdev4,efp->e2coef40, efp->e2coef41,
    efp->hxdt_sdev, efp->hydt_sdev, efp->bt_mean, efp->hz_mean,
    efp->ax_mean, efp->ax_sdev, efp->ay_mean, efp->ay_sdev,
    efp->az_mean, efp->hx_mean, efp->hy_mean, efp->time
  );
}

void storeEFRColumnDataToRawDataFrame(float columns[], EFRDataFrame* frame) {
  frame->uxt = (time_t) columns[0];
  frame->pc = (int) columns[1];
  frame->age = (char) columns[2];
  frame->overflow = (short) columns[3];
  frame->seqno = (short) columns[4];
  
  frame->zr = columns[5];
  frame->bt = columns[6];
  frame->hz = columns[7];
  frame->hy = columns[8];
  frame->hx = columns[9];
  frame->az = columns[10];
  frame->ay = columns[11];
  frame->ax = columns[12];
  frame->e1 = columns[13];
  frame->e2 = columns[14];
  
  frame->zr_pk2pk = (short) columns[15];
  frame->bt_pk2pk = (short) columns[16];
  frame->hz_pk2pk = (short) columns[17];
  frame->hy_pk2pk = (short) columns[18];
  frame->hx_pk2pk = (short) columns[19];
  frame->az_pk2pk = (short) columns[20];
  frame->ay_pk2pk = (short) columns[21];
  frame->ax_pk2pk = (short) columns[22];
  frame->e1_pk2pk = (int) columns[23];
  frame->e2_pk2pk = (int) columns[24];

  frame->pr = (int) columns[25];
  frame->uxtpr = (int) columns[26];
}

// this will read in only 50 samples at a time, with a sliding window of 25,
// only creating 1 (or 2?) processed results at a time
int processStream(FILE* fp, RawType filetype) {
  if(filetype == CSV) {
    // never implemented this because tests were complete
    printf("CSV file type not currently implemented\n");
    return 1;
  }
  
  // read in the lines
  // comment lines can be longer but they can be truncated
  char buffer[EFR_MAX_LINE];
  
  // we need n=window frames to demodulate
  EFRDataFrame emraws[SAMPLE_WINDOW_SIZE];
  
  // we only ever have a single processed sample
  EFPDataFrame empro;
  
  // keep track of how many samples we've seen
  // this gets reset as samples are processed
  unsigned short rawcount = 0;
  
  // keep track of how many processed samples we've created
  // this never gets reset
  unsigned short procount = 0;
  
  // set up the tokenizing
  char* token;
  char* delimeter = ",";
  unsigned char colcount;
  
  float columns[EFR_COL_COUNT];

  // print out a header line
  printf("# vars = demod-window, ROTP_HX,ROTP_HY,PISTON_C0, E1MEAN4,E1SDEV4,E1COEF40,E1COEF41, E2MEAN4,E2SDEV4,E2COEF40,E2COEF41, HX_SDEV,HY_SDEV,BT_MEAN,HZ_MEAN, AX_MEAN,AX_SDEV,AY_MEAN,AY_SDEV, AZ_MEAN,HX_MEAN,HY_MEAN,UXT\n");
  
  // loop over every line and keep count
  while(fgets(buffer, EFR_MAX_LINE, fp)) {
    
    // skip comment lines
    if(buffer[0] == '#') {
      continue;
    }
    
    // store the int values of the line into an array
    colcount = 0;

    // awkward because strtok retains state
    token = strtok(buffer, delimeter);
    while(token != NULL) {

      // save the token as a float
      columns[colcount] = (float) atof(token);

      // if we've seen enough columns then quit
      colcount += 1;
      if(colcount == EFR_COL_COUNT) {
        break;
      }

      // otherwise grab the next token
      token = strtok(NULL, delimeter);
    }

    // sanity check
    if(colcount != EFR_COL_COUNT) {
      printf("invalid column count (%d) on line: %s\n", colcount, buffer);
      continue;
    }

    // now that we've read a line save it as an efr frame
    storeEFRColumnDataToRawDataFrame(columns, &emraws[rawcount]);
    rawcount += 1;
    
    if(rawcount == SAMPLE_WINDOW_SIZE) {
      
      // ready to demodulate
      demodulate_efr(emraws, rawcount, &empro, 1);
      printProcessedDataFrame(procount, &empro);
      
      procount += 1;
      
      // now we need to shift everything down by the slide
      for(int i = SAMPLE_SLIDE_SIZE; i < SAMPLE_WINDOW_SIZE; i += 1) {
        emraws[i - SAMPLE_SLIDE_SIZE] = emraws[i];
      }
      
      // reset the count to the slide size to build up to the window
      rawcount = SAMPLE_SLIDE_SIZE;
    }
  }
  
  return 0;
}

// this will read in all the samples from the file at once and then process them all at once
int processFull(FILE* fp, RawType filetype) {
  
  // start reading the file line-by-line into a buffer
  char buffer[EFR_MAX_LINE];
  
  // make two passes over the file to count the lines
  // this way we don't have to deal with dynamic memory
  int count = 0;

  // loop over every line and keep count
  while(fgets(buffer, EFR_MAX_LINE, fp)) {
    // skip comment lines
    if(buffer[0] != '#') {
      count += 1;
    }
  }
  
  if(count <= 0) {
    printf("no raw frames found\n\n");
    return 0;
  }

  // seek back to the start of the file
  fseek(fp, 0, SEEK_SET);
  
  // from the raw count we know how many processed frames we'll need
  int procount = getProcessedCountFromRawCount(count);
  
  if(procount <= 0) {
    printf("not enough data frames to process\n\n");
    return 1;
  }
  
  // setup the processed frames
  EFPDataFrame empros[procount];
  
  clock_t start;
  clock_t end;
  
  // set up the tokenizing
  char* token;
  char* delimeter = ",";
  int colcount;
  
  if(filetype == CSV) {

    // create array of structs; at most one for each line
    EMADataFrame emraws[count];
    
    // reuse this as the number of lines processed
    count = 0;
    
    // we'll store every value, or token, into an array
    int columns[CSV_COL_COUNT];

    // loop over the lines again, parsing the data this time
    while(fgets(buffer, EFR_MAX_LINE, fp)) {
      
      // skip comment lines
      if(buffer[0] == '#') {
        continue;
      }

      // store the int values of the line into an array
      colcount = 0;

      // awkward because strtok retains state
      token = strtok(buffer, delimeter);
      while(token != NULL) {

        // save the token as an integer
        columns[colcount] = atoi(token);

        // if we've seen enough columns then quit
        colcount += 1;
        if(colcount == CSV_COL_COUNT) {
          break;
        }

        // otherwise grab the next token
        token = strtok(NULL, delimeter);
      }

      // sanity check
      if(colcount != CSV_COL_COUNT) {
        printf("invalid column count (%d) on line: %d\n", colcount, count + 2);
      }

      // now that we've read a line save it as a data frame
      emraws[count].timestamp = (time_t) columns[0];

      emraws[count].age = (unsigned char) columns[1];
      emraws[count].overflow = (short) columns[2];
      emraws[count].seqno = (short) columns[3];

      emraws[count].zr_nsum = (unsigned char) columns[4];
      emraws[count].zr_sum = (short) columns[5];
      emraws[count].zr_pk2pk = (short) columns[6];

      emraws[count].bt_nsum = (unsigned char) columns[7];
      emraws[count].bt_sum = (short) columns[8];
      emraws[count].bt_pk2pk = (short) columns[9];

      emraws[count].hz_nsum = (unsigned char) columns[10];
      emraws[count].hz_sum = (short) columns[11];
      emraws[count].hz_pk2pk = (short) columns[12];

      emraws[count].hy_nsum = (unsigned char) columns[13];
      emraws[count].hy_sum = (short) columns[14];
      emraws[count].hy_pk2pk = (short) columns[15];

      emraws[count].hx_nsum = (unsigned char) columns[16];
      emraws[count].hx_sum = (short) columns[17];
      emraws[count].hx_pk2pk = (short) columns[18];

      emraws[count].az_nsum = (unsigned char) columns[19];
      emraws[count].az_sum = (short) columns[20];
      emraws[count].az_pk2pk = (short) columns[21];

      emraws[count].ay_nsum = (unsigned char) columns[22];
      emraws[count].ay_sum = (short) columns[23];
      emraws[count].ay_pk2pk = (short) columns[24];

      emraws[count].ax_nsum = (unsigned char) columns[25];
      emraws[count].ax_sum = (short) columns[26];
      emraws[count].ax_pk2pk = (short) columns[27];

      emraws[count].e1_nsum = (unsigned char) columns[28];
      emraws[count].e1_sum = columns[29];
      emraws[count].e1_pk2pk = columns[30];

      emraws[count].e2_nsum = (unsigned char) columns[31];
      emraws[count].e2_sum = columns[32];
      emraws[count].e2_pk2pk = columns[33];

      emraws[count].crc = (short) columns[34];

      count += 1;
    }
    
    // do the demodulating work
    start = clock();
    demodulate(emraws, count, empros, procount);
    end = clock();
    
  } else if(filetype == EFR) {
    
    // create array of structs; at most one for each line
    EFRDataFrame emraws[count];
    
    // reuse this as the number of lines processed
    count = 0;
    
    // we'll store every value, or token, into an array
    float columns[EFR_COL_COUNT];
    
    // loop over the lines again, parsing the data this time
    while(fgets(buffer, EFR_MAX_LINE, fp)) {
      
      // skip comment lines
      if(buffer[0] == '#') {
        continue;
      }

      // store the int values of the line into an array
      colcount = 0;

      // awkward because strtok retains state
      token = strtok(buffer, delimeter);
      while(token != NULL) {

        // save the token as a double
        columns[colcount] = (float) atof(token);

        // if we've seen enough columns then quit
        colcount += 1;
        if(colcount == EFR_COL_COUNT) {
          break;
        }

        // otherwise grab the next token
        token = strtok(NULL, delimeter);
      }

      // sanity check
      if(colcount != EFR_COL_COUNT) {
        printf("invalid column count (%d) on line: %d\n", colcount, count + 2);
      }

      // now that we've read a line save it as an efr frame
      storeEFRColumnDataToRawDataFrame(columns, &emraws[count]);
      count += 1;
    }
    
    // do the demodulating work
    start = clock();
    demodulate_efr(emraws, count, empros, procount);
    end = clock();
    
  } else {
    
    printf("unknown file type: %d\n", filetype);
    return 1;
  }
  
  // now print the demodulated result
  
  // print headers
  printf("# processing time=%.9f sec\n", ((double)(end - start)) / CLOCKS_PER_SEC);
  printf("# count=%d\n", count);
  printf("# vars = demod-window, ROTP_HX,ROTP_HY,PISTON_C0, E1MEAN4,E1SDEV4,E1COEF40,E1COEF41, E2MEAN4,E2SDEV4,E2COEF40,E2COEF41, HX_SDEV,HY_SDEV,BT_MEAN,HZ_MEAN, AX_MEAN,AX_SDEV,AY_MEAN,AY_SDEV, AZ_MEAN,HX_MEAN,HY_MEAN,UXT\n");
  
  // loop over every demodulation window
  for(int i = 0; i < procount; i += 1) {
    printProcessedDataFrame(i, &empros[i]);
  }
  
  printf("\n");
  
  return 0;
}

int main(int argc, char** argv) {
  
  // make sure the filename was passed in
  char* filename;
  RawType filetype = EFR;
  ProcessType processtype = STREAM;
  
  if(argc >= 2) {
    filename = argv[1];
    
    if(argc >= 3) {
      filetype = atoi(argv[2]);
      if(filetype < 1 || filetype > 2) {
        filetype = EFR;
      }
      
      if(argc >= 4) {
        processtype = atoi(argv[3]);
        if(processtype < 1 || processtype > 2) {
          processtype = STREAM;
        }
      }
    }

  } else {
    printf("usage: demodulate <filename> [<efr=1*,csv=2>] [<stream=1*,full=2>]\n\n");
    return 1;
  }

  // open file and make sure it is available
  FILE* fp = fopen(filename, "r");
  if(fp == NULL) {
    printf("file not found: %s\n\n", filename);
    return 1;
  }
  
  int result = 1;
  if(processtype == STREAM) {
    result = processStream(fp, filetype);
    
  } else if(processtype == FULL) {
    result = processFull(fp, filetype);
    
  } else {
    printf("Unknown process type: %d\n", processtype);
    result = 1;
  }
  
  fclose(fp);
  return result;
}
