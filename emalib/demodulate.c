#include <math.h>
#include <string.h>
#include "qlsq.h"
#include "dsleq.h"
#include "demodulate.h"

 // max number of aprx functions
#define MFUN_EFPRO 5

#define MAX_EMA_PROCESS_NVALS 70

// hard-code these values, come from mission parameters EmaProcessNslide and EmaProcessNvals
#define EMA_PROCESS_NVALS 50
#define EMA_PROCESS_NSLIDE 25

struct efiq_struct {
  time_t timeRmA_mid;
  float tim_avg;
  float seq_avg;

  float nzc_hx;
  float nzc_hy;
  float rotp_hx;
  float rotp_hy;
  float rotp_std_hx;
  float rotp_std_hy;

  float e1_avg;
  float e1_rms;
  float e1_c0;
  float e1_c1;
  float e1_c2;

  float e2_avg;
  float e2_rms;
  float e2_c0;
  float e2_c1;
  float e2_c2;

  float eia_avg;
  float eqa_avg;
  float eib_avg;
  float eqb_avg;

  float eia_rms;
  float eqa_rms;
  float eib_rms;
  float eqb_rms;

  float e1_mean4;
  float e2_mean4;
  float e1sdev4;
  float e2sdev4;

  float e1coef4[MFUN_EFPRO];
  float e2coef4[MFUN_EFPRO];

  float piston_c0;
  float piston_c1;
  float piston_c2;
  float piston_rms;

  float hxdt_sdev;
  float hydt_sdev;

  float bt_mean;
  float hz_mean;
  float ax_mean;
  float ay_mean;
  float az_mean;

  float bt_sdev;
  float hz_sdev;
  float ax_sdev;
  float ay_sdev;
  float az_sdev;

  float hx_mean;
  float hy_mean;
};

// add function definitions from efpro.c

static int efiq_pro(
  short nvals, time_t * timeR, unsigned char *piston,
  int *age, unsigned short *seqno,
  aprxfloat *e1dat, aprxfloat *e2dat, aprxfloat *btdat,
  aprxfloat *hxdat, aprxfloat *hydat, aprxfloat *hzdat,
  aprxfloat *axdat, aprxfloat *aydat, aprxfloat *azdat,
  struct efiq_struct *efiq
);

static int make_aprox_poly(aprxfloat *aprox, aprxfloat *x, short nvals, short nfun);

static int make_aprox_sinusoid(
  aprxfloat *aprox,
  aprxfloat *x, aprxfloat *c, aprxfloat *s,
  short nvals, short nfun
);

static int dofit(
  aprxfloat *aprox, aprxfloat *x,
  short nvals, short nfun,
  aprxfloat *y, aprxfloat *res,
  aprxfloat *mean, aprxfloat *coefs, aprxfloat *sdev
);

static int meansdev(aprxfloat *x, short nvals, aprxfloat *mean, aprxfloat *sdev);

static int rotation_period(
  float *y, int nvals,
  float *zc, int *nzc_ret, float *rotp_ret,
  float *rotp_sdev_ret
);
  
////

int getProcessedCountFromRawCount(int count) {
  return (count / EMA_PROCESS_NSLIDE) - 1;
}
  
////

// this is the function you should implement for demodulating:
//  * emaraws: the data frames array that need demodulating
//  * rawcount: the number of frames in the array
//  * emapros: output storage for the processed frames
//  * procount: size of the storage array
//  
void demodulate(EMADataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount) {
  if(rawcount <= 0 || procount <= 0) {
    return;
  }

  struct efiq_struct efiq;
  int seqerr;
  int emaR_fifo_pick;
  short nvals;
  int i;
  int j;
  
  time_t timeR_efp[EMA_PROCESS_NVALS];
  unsigned char piston_efp[EMA_PROCESS_NVALS];

  int age_e2v[EMA_PROCESS_NVALS];
  unsigned short seqno_e2v[EMA_PROCESS_NVALS];

  aprxfloat e1_e2v[EMA_PROCESS_NVALS];
  aprxfloat e2_e2v[EMA_PROCESS_NVALS];

  aprxfloat bt_e2v[EMA_PROCESS_NVALS];

  aprxfloat hx_e2v[EMA_PROCESS_NVALS];
  aprxfloat hy_e2v[EMA_PROCESS_NVALS];
  aprxfloat hz_e2v[EMA_PROCESS_NVALS];

  aprxfloat ax_e2v[EMA_PROCESS_NVALS];
  aprxfloat ay_e2v[EMA_PROCESS_NVALS];
  aprxfloat az_e2v[EMA_PROCESS_NVALS];

  int emaRfifoLEN = rawcount;

  int emaR_fifo_tail;
  nvals = EMA_PROCESS_NVALS;

  emaR_fifo_tail = 0; // set to start
  for(j = 0; j < procount; j++) {

    // copy data to process buffers
    emaR_fifo_pick = emaR_fifo_tail;
    for(i = 0; i < nvals; i++) {

      timeR_efp[i] = emraws[emaR_fifo_pick].timestamp;
      piston_efp[i] = 0; // no data input for this here

      age_e2v[i] = emraws[emaR_fifo_pick].age;
      seqno_e2v[i] = emraws[emaR_fifo_pick].seqno;

      e1_e2v[i] = (double) emraws[emaR_fifo_pick].e1_sum / (double) emraws[emaR_fifo_pick].e1_nsum;
      e2_e2v[i] = (double) emraws[emaR_fifo_pick].e2_sum / (double) emraws[emaR_fifo_pick].e2_nsum;

      bt_e2v[i] = (double) emraws[emaR_fifo_pick].bt_sum / (double) emraws[emaR_fifo_pick].bt_nsum;
    
      hx_e2v[i] = (float) emraws[emaR_fifo_pick].hx_sum / (float) emraws[emaR_fifo_pick].hx_nsum;
      hy_e2v[i] = (float) emraws[emaR_fifo_pick].hy_sum / (float) emraws[emaR_fifo_pick].hy_nsum;
      hz_e2v[i] = (float) emraws[emaR_fifo_pick].hz_sum / (float) emraws[emaR_fifo_pick].hz_nsum;

      ax_e2v[i] = (float) emraws[emaR_fifo_pick].ax_sum / (float) emraws[emaR_fifo_pick].ax_nsum;
      ay_e2v[i] = (float) emraws[emaR_fifo_pick].ay_sum / (float) emraws[emaR_fifo_pick].ay_nsum;
      az_e2v[i] = (float) emraws[emaR_fifo_pick].az_sum / (float) emraws[emaR_fifo_pick].az_nsum;
    
      // don't use continue in this for loop --
      // must do this at end of each loop
      if(++emaR_fifo_pick >= emaRfifoLEN) {
        emaR_fifo_pick = 0;
      }
    }

    // test sequence numbers to check that fifo stuff works
    seqerr = 0;
    for(i = 1; i < nvals; i++) {
      if(seqno_e2v[i] - 1 != seqno_e2v[i - 1]) {
        seqerr++;
      }
    }

    i = 0;

    // EF in-phase and quadrature processing
    efiq_pro(
      nvals, timeR_efp, piston_efp,
      age_e2v, seqno_e2v,
      e1_e2v, e2_e2v, bt_e2v,
      hx_e2v, hy_e2v, hz_e2v, ax_e2v, ay_e2v, az_e2v,
      &efiq
    );

    // save processed data in persistent far arrays
    empros[j].time = efiq.timeRmA_mid;

    empros[j].rotp_hx = efiq.rotp_hx;
    empros[j].rotp_hy = efiq.rotp_hy;

    empros[j].e1coef40 = efiq.e1coef4[0];
    empros[j].e1coef41 = efiq.e1coef4[1];

    empros[j].e2coef40 = efiq.e2coef4[0];
    empros[j].e2coef41 = efiq.e2coef4[1];

    empros[j].e1_mean4 = efiq.e1_mean4;
    empros[j].e2_mean4 = efiq.e2_mean4;

    empros[j].e1sdev4 = efiq.e1sdev4;
    empros[j].e2sdev4 = efiq.e2sdev4;

    empros[j].piston_c0 = efiq.piston_c0;

    empros[j].hx_mean = efiq.hx_mean;
    empros[j].hy_mean = efiq.hy_mean;

    empros[j].hxdt_sdev = efiq.hxdt_sdev;
    empros[j].hydt_sdev = efiq.hydt_sdev;

    empros[j].bt_mean = efiq.bt_mean;

    empros[j].hz_mean = efiq.hz_mean;

    empros[j].ax_mean = efiq.ax_mean;
    empros[j].ax_sdev = efiq.ax_sdev;

    empros[j].ay_mean = efiq.ay_mean;
    empros[j].ay_sdev = efiq.ay_sdev;

    empros[j].az_mean = efiq.az_mean;

    // advance demodulation window forward
    emaR_fifo_tail += EMA_PROCESS_NSLIDE;
  }
}

static time_t timeR_efp[EMA_PROCESS_NVALS];
static unsigned char piston_efp[EMA_PROCESS_NVALS];
static int age_e2v[EMA_PROCESS_NVALS];
static unsigned short seqno_e2v[EMA_PROCESS_NVALS];
static aprxfloat e1_e2v[EMA_PROCESS_NVALS];
static aprxfloat e2_e2v[EMA_PROCESS_NVALS];
static aprxfloat bt_e2v[EMA_PROCESS_NVALS];
static aprxfloat hx_e2v[EMA_PROCESS_NVALS];
static aprxfloat hy_e2v[EMA_PROCESS_NVALS];
static aprxfloat hz_e2v[EMA_PROCESS_NVALS];
static aprxfloat ax_e2v[EMA_PROCESS_NVALS];
static aprxfloat ay_e2v[EMA_PROCESS_NVALS];
static aprxfloat az_e2v[EMA_PROCESS_NVALS];

// this is the function you should implement for demodulating:
//  * emaraws: the data frames array that need demodulating
//  * rawcount: the number of frames in the array
//  * emapros: output storage for the processed frames
//  * procount: size of the storage array
//  
void demodulate_efr(EFRDataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount) {
  if(rawcount <= 0 || procount <= 0) {
    return;
  }

    memset(timeR_efp, 0, sizeof(time_t) * EMA_PROCESS_NVALS);
    memset(piston_efp, 0, EMA_PROCESS_NVALS);
    memset(age_e2v, 0, sizeof(int) * EMA_PROCESS_NVALS);
    memset(seqno_e2v, 0, sizeof(unsigned short) * EMA_PROCESS_NVALS);
    memset(e1_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(e2_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(bt_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(hx_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(hy_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(hz_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(ax_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(ay_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
    memset(az_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);

  struct efiq_struct efiq;
  int emaR_fifo_pick;
  short nvals;
  int i;
  int j;

  int emaRfifoLEN = rawcount;

  int emaR_fifo_tail;
  nvals = EMA_PROCESS_NVALS;

  emaR_fifo_tail = 0; // set to start
  for(j = 0; j < procount; j++) {

    // copy data to process buffers
    emaR_fifo_pick = emaR_fifo_tail;
    for(i = 0; i < nvals; i++) {

      timeR_efp[i] = emraws[emaR_fifo_pick].uxt;
      //piston_efp[i] = 0.0; // no data input for this here
      piston_efp[i] = emraws[emaR_fifo_pick].pc;

      age_e2v[i] = emraws[emaR_fifo_pick].age;
      seqno_e2v[i] = emraws[emaR_fifo_pick].seqno;

      e1_e2v[i] = emraws[emaR_fifo_pick].e1;
      e2_e2v[i] = emraws[emaR_fifo_pick].e2;

      bt_e2v[i] = emraws[emaR_fifo_pick].bt;
  
      hx_e2v[i] = emraws[emaR_fifo_pick].hx;
      hy_e2v[i] = emraws[emaR_fifo_pick].hy;
      hz_e2v[i] = emraws[emaR_fifo_pick].hz;

      ax_e2v[i] = emraws[emaR_fifo_pick].ax;
      ay_e2v[i] = emraws[emaR_fifo_pick].ay;
      az_e2v[i] = emraws[emaR_fifo_pick].az;

      // don't use continue in this for loop --
      // must do this at end of each loop
      if(++emaR_fifo_pick >= emaRfifoLEN) {
        emaR_fifo_pick = 0;
      }
    }

    i = 0;

    // EF in-phase and quadrature processing
    efiq_pro(
      nvals, timeR_efp, piston_efp,
      age_e2v, seqno_e2v,
      e1_e2v, e2_e2v, bt_e2v,
      hx_e2v, hy_e2v, hz_e2v,
      ax_e2v, ay_e2v, az_e2v, &efiq
    );

    // save processed data in persistent far arrays
    empros[j].time = efiq.timeRmA_mid;

    empros[j].rotp_hx = efiq.rotp_hx;
    empros[j].rotp_hy = efiq.rotp_hy;

    empros[j].e1coef40 = efiq.e1coef4[0];
    empros[j].e1coef41 = efiq.e1coef4[1];

    empros[j].e2coef40 = efiq.e2coef4[0];
    empros[j].e2coef41 = efiq.e2coef4[1];

    empros[j].e1_mean4 = efiq.e1_mean4;
    empros[j].e2_mean4 = efiq.e2_mean4;

    empros[j].e1sdev4 = efiq.e1sdev4;
    empros[j].e2sdev4 = efiq.e2sdev4;

    empros[j].piston_c0 = efiq.piston_c0;

    empros[j].hx_mean = efiq.hx_mean;
    empros[j].hy_mean = efiq.hy_mean;

    empros[j].hxdt_sdev = efiq.hxdt_sdev;
    empros[j].hydt_sdev = efiq.hydt_sdev;

    empros[j].bt_mean = efiq.bt_mean;

    empros[j].hz_mean = efiq.hz_mean;

    empros[j].ax_mean = efiq.ax_mean;
    empros[j].ax_sdev = efiq.ax_sdev;

    empros[j].ay_mean = efiq.ay_mean;
    empros[j].ay_sdev = efiq.ay_sdev;

    empros[j].az_mean = efiq.az_mean;

    // advance demodulation window forward
    emaR_fifo_tail += EMA_PROCESS_NSLIDE;
  }
}

// the same code as demodulate_efr() but with the true raw data type and the sums are computed to floats as needed
void demodulate_ema(EMADataFrame emraws[], int rawcount, EFPDataFrame empros[], int procount) {
  if(rawcount <= 0 || procount <= 0) {
    return;
  }

  // Initialize static variables
  memset(timeR_efp, 0, sizeof(time_t) * EMA_PROCESS_NVALS);
  memset(piston_efp, 0, EMA_PROCESS_NVALS);
  memset(age_e2v, 0, sizeof(int) * EMA_PROCESS_NVALS);
  memset(seqno_e2v, 0, sizeof(unsigned short) * EMA_PROCESS_NVALS);
  memset(e1_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(e2_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(bt_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(hx_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(hy_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(hz_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(ax_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(ay_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);
  memset(az_e2v, 0, sizeof(aprxfloat) * EMA_PROCESS_NVALS);

  struct efiq_struct efiq;
  int emaR_fifo_pick;
  short nvals;
  int i;
  int j;

  int emaRfifoLEN = rawcount;

  int emaR_fifo_tail;
  nvals = EMA_PROCESS_NVALS;

  emaR_fifo_tail = 0; // set to start
  for(j = 0; j < procount; j++) {

    // copy data to process buffers
    emaR_fifo_pick = emaR_fifo_tail;
    for(i = 0; i < nvals; i++) {

      timeR_efp[i] = emraws[emaR_fifo_pick].timestamp;
      piston_efp[i] = emraws[emaR_fifo_pick].piston;

      age_e2v[i] = emraws[emaR_fifo_pick].age;
      seqno_e2v[i] = emraws[emaR_fifo_pick].seqno;

      e1_e2v[i] = (float)emraws[emaR_fifo_pick].e1_sum / (float)emraws[emaR_fifo_pick].e1_nsum;
      e2_e2v[i] = (float)emraws[emaR_fifo_pick].e2_sum / (float)emraws[emaR_fifo_pick].e2_nsum;

      bt_e2v[i] = (float)emraws[emaR_fifo_pick].bt_sum / (float)emraws[emaR_fifo_pick].bt_nsum;
  
      hx_e2v[i] = (float)emraws[emaR_fifo_pick].hx_sum / (float)emraws[emaR_fifo_pick].hx_nsum;
      hy_e2v[i] = (float)emraws[emaR_fifo_pick].hy_sum / (float)emraws[emaR_fifo_pick].hy_nsum;
      hz_e2v[i] = (float)emraws[emaR_fifo_pick].hz_sum / (float)emraws[emaR_fifo_pick].hz_nsum;

      ax_e2v[i] = (float)emraws[emaR_fifo_pick].ax_sum / (float)emraws[emaR_fifo_pick].ax_nsum;
      ay_e2v[i] = (float)emraws[emaR_fifo_pick].ay_sum / (float)emraws[emaR_fifo_pick].ay_nsum;
      az_e2v[i] = (float)emraws[emaR_fifo_pick].az_sum / (float)emraws[emaR_fifo_pick].az_nsum;

      // don't use continue in this for loop --
      // must do this at end of each loop
      if(++emaR_fifo_pick >= emaRfifoLEN) {
        emaR_fifo_pick = 0;
      }
    }

    i = 0;

    // EF in-phase and quadrature processing
    efiq_pro(
      nvals, timeR_efp, piston_efp,
      age_e2v, seqno_e2v,
      e1_e2v, e2_e2v, bt_e2v,
      hx_e2v, hy_e2v, hz_e2v,
      ax_e2v, ay_e2v, az_e2v, &efiq
    );

    // save processed data in persistent far arrays
    empros[j].time = efiq.timeRmA_mid;

    empros[j].rotp_hx = efiq.rotp_hx;
    empros[j].rotp_hy = efiq.rotp_hy;

    empros[j].e1coef40 = efiq.e1coef4[0];
    empros[j].e1coef41 = efiq.e1coef4[1];

    empros[j].e2coef40 = efiq.e2coef4[0];
    empros[j].e2coef41 = efiq.e2coef4[1];

    empros[j].e1_mean4 = efiq.e1_mean4;
    empros[j].e2_mean4 = efiq.e2_mean4;

    empros[j].e1sdev4 = efiq.e1sdev4;
    empros[j].e2sdev4 = efiq.e2sdev4;

    empros[j].piston_c0 = efiq.piston_c0;

    empros[j].hx_mean = efiq.hx_mean;
    empros[j].hy_mean = efiq.hy_mean;

    empros[j].hxdt_sdev = efiq.hxdt_sdev;
    empros[j].hydt_sdev = efiq.hydt_sdev;

    empros[j].bt_mean = efiq.bt_mean;

    empros[j].hz_mean = efiq.hz_mean;

    empros[j].ax_mean = efiq.ax_mean;
    empros[j].ax_sdev = efiq.ax_sdev;

    empros[j].ay_mean = efiq.ay_mean;
    empros[j].ay_sdev = efiq.ay_sdev;

    empros[j].az_mean = efiq.az_mean;

    // advance demodulation window forward
    emaR_fifo_tail += EMA_PROCESS_NSLIDE;
  }
}

// insert functions from efpro.c
static aprxfloat flt_tmp[MAX_EMA_PROCESS_NVALS];
static aprxfloat tim[MAX_EMA_PROCESS_NVALS];
static aprxfloat seq[MAX_EMA_PROCESS_NVALS];
static aprxfloat aprox[MAX_EMA_PROCESS_NVALS * MFUN_EFPRO];
static aprxfloat pi_poly[MFUN_EFPRO];
static aprxfloat hx_poly[MFUN_EFPRO];
static aprxfloat hy_poly[MFUN_EFPRO];
static aprxfloat hxres[MAX_EMA_PROCESS_NVALS];
static aprxfloat hyres[MAX_EMA_PROCESS_NVALS];
static aprxfloat e1res[MAX_EMA_PROCESS_NVALS];
static aprxfloat e2res[MAX_EMA_PROCESS_NVALS];
static aprxfloat pires[MAX_EMA_PROCESS_NVALS];
static aprxfloat e1coef4[MFUN_EFPRO + 1];
static aprxfloat e2coef4[MFUN_EFPRO + 1];
static float zctmp[MAX_EMA_PROCESS_NVALS];

static int efiq_pro(
  short nvals, time_t *timeR, unsigned char *piston,
  int *age, unsigned short *seqno,
  aprxfloat *e1dat, aprxfloat *e2dat, aprxfloat *btdat,
  aprxfloat *hxdat, aprxfloat *hydat, aprxfloat *hzdat,
  aprxfloat *axdat, aprxfloat *aydat, aprxfloat *azdat,
  struct efiq_struct *efiq
) {

    memset(flt_tmp, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(tim, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(seq, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(aprox, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS * MFUN_EFPRO);
    memset(pi_poly, 0, sizeof(aprxfloat) * MFUN_EFPRO);
    memset(hx_poly, 0, sizeof(aprxfloat) * MFUN_EFPRO);
    memset(hy_poly, 0, sizeof(aprxfloat) * MFUN_EFPRO);
    memset(hxres, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(hyres, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(e1res, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(e2res, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(pires, 0, sizeof(aprxfloat) * MAX_EMA_PROCESS_NVALS);
    memset(e1coef4, 0, sizeof(aprxfloat) * MFUN_EFPRO + 1);
    memset(e2coef4, 0, sizeof(aprxfloat) * MFUN_EFPRO + 1);
    memset(zctmp, 0, sizeof(float) * MAX_EMA_PROCESS_NVALS);
  
  // for initial polynomial fits
  //      int nfun_poly_e12 = 1; // just mean
  int nfun_poly_hxy = 1;        // 1 = just mean, 2 = mean & trend
  int nfun_poly_pi = 3;         // 3 = mean and trend and quadratic

  // for sinusoidal fitting
  int nfun_sin4_e12 = 4;        // allow for mean and linear


  aprxfloat seq_mid, seq_off;
  aprxfloat seq_sum, tim_sum;
  short imid;
  short i;


  // polynomial coefs


  // standard deviations
  aprxfloat pi_sdev, hx_sdev, hy_sdev;
  aprxfloat pi_mean, hx_mean, hy_mean;



  aprxfloat hxdt_mean, hxdt_sdev;
  aprxfloat hydt_mean, hydt_sdev;
  aprxfloat hx_norm;
  aprxfloat hy_norm;

  aprxfloat e1_mean4;
  aprxfloat e2_mean4;
  aprxfloat e1sdev4;
  aprxfloat e2sdev4;


  int nzc_hx, nzc_hy;

  float rotp_hx, rotp_hy, rotp_std_hx, rotp_std_hy;

  if(nvals < 2) {
    return -1;
  }

  imid = nvals / 2;

  efiq->timeRmA_mid = timeR[imid] - age[imid];

  seq_mid = (aprxfloat) seqno[imid];
  seq_off = 0.0;
  seq_sum = 0.0;
  tim_sum = 0.0;

  for(i = 0; i < nvals; i++) {
    tim[i] = (aprxfloat) (timeR[i] - (time_t) age[i] - efiq->timeRmA_mid);
    seq[i] = (aprxfloat) seqno[i] - seq_mid + seq_off;

    if(i > 0) {
      aprxfloat ds = seq[i] - seq[i - 1];
      if((ds > -65535.5) && (ds < -65534.5)) {
        seq_off += 65536.0;
        seq[i] += 65536.0;
      }
    }
    
    tim_sum += tim[i];
    seq_sum += seq[i];
  }
  
  efiq->tim_avg = tim_sum / (aprxfloat) nvals;
  efiq->seq_avg = seq_sum / (aprxfloat) nvals;

  make_aprox_poly(aprox, seq, nvals, nfun_poly_pi);
  
  for(i = 0; i < nvals; i++) {
    flt_tmp[i] = (float) piston[i];
  }

  for(i = 0; i < MFUN_EFPRO; i++) {
    pi_poly[i] = 0.0;
  }
  
  pi_mean = 0.0;
  pi_sdev = 0.0;

  dofit(aprox, seq, nvals, nfun_poly_pi, flt_tmp, pires, &pi_mean, pi_poly, &pi_sdev);
  pi_poly[0] += pi_mean;

  for(i = 0; i < MFUN_EFPRO; i++) {
    hx_poly[i] = 0.0;
    hy_poly[i] = 0.0;
  }
  
  hx_mean = 0.0;
  hx_sdev = 0.0;
  hy_mean = 0.0;
  hy_sdev = 0.0;

  make_aprox_poly(aprox, seq, nvals, nfun_poly_hxy);
  dofit(aprox, seq, nvals, nfun_poly_hxy, hxdat, hxres, &hx_mean, hx_poly, &hx_sdev);
  dofit(aprox, seq, nvals, nfun_poly_hxy, hydat, hyres, &hy_mean, hy_poly, &hy_sdev);

  // rotation period for each hx & hy
  rotation_period(hxres, nvals, zctmp, &nzc_hx, &rotp_hx, &rotp_std_hx);
  rotation_period(hyres, nvals, zctmp, &nzc_hy, &rotp_hy, &rotp_std_hy);

  efiq->nzc_hx = nzc_hx;
  efiq->nzc_hy = nzc_hy;
  efiq->rotp_hx = rotp_hx;
  efiq->rotp_hy = rotp_hy;
  efiq->rotp_std_hx = rotp_std_hx;
  efiq->rotp_std_hy = rotp_std_hy;

  efiq->hx_mean = hx_mean;
  efiq->hy_mean = hy_mean;

  // std dev of mag fields to normalize them for use as sin(), cos()
  if(meansdev(hxres, nvals, &hxdt_mean, &hxdt_sdev) <= 0) {
    return -3;
  }
  
  if(meansdev(hyres, nvals, &hydt_mean, &hydt_sdev) <= 0) {
    return -4;
  }

  if(hxdt_sdev > 0.0) {
    hx_norm = 0.7071068 / hxdt_sdev;
    
  } else {
    hx_norm = 1.0;
  }

  if(hydt_sdev > 0.0) {
    hy_norm = 0.7071068 / hydt_sdev;
    
  } else {
    hy_norm = 1.0;
  }

  efiq->hxdt_sdev = hxdt_sdev;
  efiq->hydt_sdev = hydt_sdev;
  
  for(i = 0; i < nvals; i++) {
    // normalize
    hxres[i] = hxres[i] * hx_norm;
    hyres[i] = hyres[i] * hy_norm;
  }

  // synchronously demodulate and average

  for(i = 0; i < MFUN_EFPRO; i++) {
    e1coef4[i] = 0.0;
    e2coef4[i] = 0.0;
  }
  
  e1_mean4 = 0.0;
  e1sdev4 = 0.0;
  e2_mean4 = 0.0;
  e2sdev4 = 0.0;

  make_aprox_sinusoid(aprox, seq, hxres, hyres, nvals, nfun_sin4_e12);
  dofit(aprox, seq, nvals, nfun_sin4_e12, e1dat, e1res, &e1_mean4, e1coef4, &e1sdev4);
  dofit(aprox, seq, nvals, nfun_sin4_e12, e2dat, e2res, &e2_mean4, e2coef4, &e2sdev4);

  efiq->piston_rms = pi_sdev;
  efiq->piston_c0 = pi_poly[0];
  efiq->piston_c1 = pi_poly[1];
  efiq->piston_c2 = pi_poly[2];

  efiq->e1_mean4 = e1_mean4;
  efiq->e2_mean4 = e2_mean4;
  efiq->e1sdev4 = e1sdev4;
  efiq->e2sdev4 = e2sdev4;

  for(i = 0; i < MFUN_EFPRO; i++) {
    efiq->e1coef4[i] = e1coef4[i];
    efiq->e2coef4[i] = e2coef4[i];
  }

  if(meansdev(hzdat, nvals, &(efiq->hz_mean), &(efiq->hz_sdev)) <= 0) {
    return -4;
  }

  if(meansdev(axdat, nvals, &(efiq->ax_mean), &(efiq->ax_sdev)) <= 0) {
    return -4;
  }
  
  if(meansdev(aydat, nvals, &(efiq->ay_mean), &(efiq->ay_sdev)) <= 0) {
    return -4;
  }
  
  if(meansdev(azdat, nvals, &(efiq->az_mean), &(efiq->az_sdev)) <= 0) {
    return -4;
  }

  if(meansdev(btdat, nvals, &(efiq->bt_mean), &(efiq->bt_sdev)) <= 0) {
    return -4;
  }

  return 1;
}

static int make_aprox_poly(aprxfloat *aprox, aprxfloat *x, short nvals, short nfun) {
  
  unsigned short i;
  unsigned short j;
  aprxfloat xi;
  aprxfloat *ptr;
  aprxfloat val;

  for(i = 0; i < nvals; i++) {
    xi = x[i];
    ptr = aprox + i;
    val = 1.0;
    
    for(j = 0; j < nfun; j++) {
      *ptr = val;
      val *= xi;
      ptr += nvals;
    }
  }
  
  return 1;
}

static int make_aprox_sinusoid(
  aprxfloat *aprox,
  aprxfloat *x, aprxfloat *c, aprxfloat *s, short nvals,
  short nfun
) {
  
  unsigned short i;
  unsigned short n = (unsigned short) nvals;
  aprxfloat *p = aprox;
  aprxfloat *xx = x;
  aprxfloat xi;

  for(i = 0; i < n; i++) {
    *p++ = *c++;
  }

  for(i = 0; i < n; i++) {
    *p++ = *s++;
  }

  for(i = 0; i < n; i++) {
    *p++ = 1.0;
  }

  for(i = 0; i < n; i++) {
    *p++ = *xx++;
  }
  
  if(nfun >= 5) {
    // quadradic
    xx = x;

    for(i = 0; i < n; i++) {
      xi = *xx++;
      *p++ = xi * xi;
    }
  }
  
  return 1;
}

static int dofit(
  aprxfloat *aprox, aprxfloat *x,
  short nvals, short nfun,
  aprxfloat *y, aprxfloat *res,
  aprxfloat *avg, aprxfloat *coefs, aprxfloat *sdev
) {
  
  short i1 = 1;
  aprxfloat sfac = 1e7;
  aprxfloat smax = 1e7;
  aprxfloat smin = 1e-2;
  aprxfloat bad_flag = -1e30;
  short need = nvals * 3 / 4;
  short lmax = 1;

  short nokinit;
  short loops;
  short status;
  short ntossed;
  short nused;

  short i;
  aprxfloat sum;

  if(nfun > MFUN_EFPRO) {
    return -1;
  }
  
  if(nvals < MFUN_EFPRO) {
    return -1;
  }

  for(i = 0; i < nfun; i++) {
    coefs[i] = 0.0;
  }

  *avg = 0.0;
  *sdev = 0.0;

  sum = 0.0;
  for(i = 0; i < nvals; i++) {
    sum += y[i];
  }
  
  *avg = sum / nvals;
  for(i = 0; i < nvals; i++) {
    y[i] -= *avg;
  }

  qlsq(
    y, &i1, &nvals, res, &need, &nused,
    &sfac, sdev, &loops, &lmax, &smin, &smax,
    &status, &ntossed, coefs, &nokinit, aprox, &nvals, &nfun, &bad_flag
  );
  
  // qlsq status is 1 for good, greater than 1 for faults
  if(status != 1) {
    return -status;
  }

  return 1;
}

static int meansdev(aprxfloat *x, short nvals, aprxfloat *mean, aprxfloat *sdev) {
  
  aprxfloat sum = 0.0;
  aprxfloat ssq = 0.0;
  aprxfloat xi;
  int i;
  aprxfloat var, avg, rms;

  if(nvals < 2) {
    *mean = 0.0;
    *sdev = 0.0;
    return -1;
  }

  for(i = 0; i < nvals; i++) {
    xi = x[i];
    sum += xi;
    ssq += xi * xi;
  }

  avg = sum / (aprxfloat) nvals;
  var = (ssq - sum * avg) / (aprxfloat) (nvals - 1);

  if(var > 0) {
    rms = sqrt(var);
    
  } else {
    rms = 0.0;
  }

  *mean = avg;
  *sdev = rms;

  return 1;
}

static int rotation_period(
  float *y, int nvals,
  float *zc, int *nzc_ptr, float *rotp_ptr,
  float *rotp_std_ptr
) {
  
  int i, i1, i2;
  float y1, y2;
  int nzc, ndt;
  float di;
  float sum, ssq, var, avg, std;

  *nzc_ptr = 0;
  *rotp_ptr = 0.0;
  *rotp_std_ptr = 0.0;

  nzc = 0;
  for(i2 = 1; i2 < nvals; i2++) {

    i1 = i2 - 1;
    y1 = y[i1];
    y2 = y[i2];

    // get zero crossing times
    if(y1 == 0) {
      zc[nzc++] = i1;
      
    } else if(y2 == 0) {
      zc[nzc++] = i2;
    
    } else if((y1 < 0 && y2 > 0) || (y1 > 0 && y2 < 0)) {
      zc[nzc++] = i1 - y1 / (y2 - y1);
    }
  }

  *nzc_ptr = nzc;

  if(nzc < 2) {
    return -1;
  }

  ssq = 0.0;
  sum = 0.0;
  for(i = 1; i < nzc; i++) {
    di = zc[i] - zc[i - 1];
    sum += di;
    ssq += di * di;
  }
  
  ndt = nzc - 1;
  avg = sum / ndt;
  *rotp_ptr = avg * 2.0;

  if(ndt == 1) {
    return -2;
  }

  var = (ssq - sum * avg) / (ndt - 1);

  if(var > 0.0) {
    std = sqrt(var);
    
  } else {
    std = 0.0;
  }

  *rotp_std_ptr = std * 2.0;

  return 1;
}
