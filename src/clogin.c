#ifndef CLOGIN_H
#define CLOGIN_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define cloginChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

int CLogin(const struct SerialPort *modem);

#endif /* CLOGIN_H */
#ifdef CLOGIN_C
#undef CLOGIN_C

#include <cd.h>
#include <logger.h>
#include <modem.h>
#include <config.h>
#include <stdlib.h>
#include <stdbool.h>
#include <control.h>
#include <login.h>
#include <lbt9523.h>
#include <string.h>

#if defined (__arm__)
   #include <apf11.h>
   #include <apf11rf.h>
#endif /* __arm__ */

/* store engineering data in persistent far ram */
extern persistent struct EngineeringData vitals;

/* define the time-out period for CD detection */
#define CdTimeOut (3.0)

/* prototypes for external functions that are used locally */
int TransmitGpsFix(const struct SerialPort *modem);

static int attempt_connection(
    const struct SerialPort* modem, const char* FuncName, char* atStr);

/*------------------------------------------------------------------------*/
/* function to establish a connection/login session with a remote host    */
/*------------------------------------------------------------------------*/
/**
   This function checks for a current connection/login session with a remote
   host and establishes a new one if one does not exist.  This function
   relies on correct operation of the modem's carrier detect (CD) signal.
   If the CD signal is asserted then a current connection is indicated and
   the function immediately returns.  If the CD signal is not asserted then
   this function establishes a session by first connecting to the remote
   host's modem and then logging into the remote host.  If successful, the
   net result will be an active communications session with the remote host.

      \begin{verbatim}
      input:
         modem......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks to
                    be sure this pointer is not NULL.

      output:
         This function returns a positive value if an active communications
         session with the remote host exists when this function returns.  If
         an session could not be established then this function returns
         zero.  A negative return value indicates either an invalid serial
         port or unimplemented modem carrier-detect functionality.
      \end{verbatim}
*/
int CLogin(const struct SerialPort *modem)
{
    /* function name for log entries */
    cc *FuncName = "CLogin()";
    int status = -1;
    if (!modem)
    {
        LogEntry(FuncName, "NULL serial port.\n");
        return status;
    }

    if (!modem->cd)
    {
        LogEntry(FuncName, "Nonfunctional modem CD signal.\n");
        return -1;
    }

    if (Cd(modem, CdTimeOut) > 0)
    {
        // Connection already exists.
        return 1;
    }

    if (debuglevel >= 2 || (debugbits & CLOGIN_H))
    {
        LogEntry(FuncName,"Connecting to primary host.\n");
    }

    status = attempt_connection(modem, FuncName, mission.at);
    if (!status)
    {
        /* make a log entry about connecting to alternate host */
        if (debuglevel >= 2 || (debugbits & CLOGIN_H))
        {
            /* log the message */
            LogEntry(
                FuncName,
                "Attempt to connect to primary host failed after 2 tries. "
                "Connecting to alternate host.\n");
        }

        status = attempt_connection(modem, FuncName, mission.alt);
    }

    if (status > 0)
    {
        TransmitGpsFix(modem);
    }

    if (status <= 0)
    {
        LogEntry(
            FuncName,
            "Attempt to connect to primary and alternate hosts failed - aborting.\n");
        ModemDtrClear();
        Wait(250);
    }

    return status;
}

static int attempt_connection(
    const struct SerialPort* modem, const char* FuncName, char* atStr)
{
    const int nRetry = 2;
    time_t To = time(NULL);
    time_t Tc;
    int status = 0;
    char imei[32];
    bool isRudics = false;

    imei[0] = '\0';
    isRudics = strcasecmp(atStr, MLF2_RUDICS) == 0;
    if (isRudics)
    {
        if (IrModemImei(modem, imei, sizeof(imei)) <= 0)
        {
            LogEntry(FuncName, "IMEI lookup failed.\n");
            return -1;
        }
    }

    for (int n = 0; n < nRetry; n++)
    {
        vitals.ConnectionAttempts++;
        if (connect(modem, atStr, mission.ConnectTimeOut) > 0)
        {
            Tc = time(NULL);
            vitals.Connections++;
            if (debuglevel>= 2 || (debugbits&CLOGIN_H))
            {
                /* log the message */
                LogEntry(
                    FuncName,
                    "Connection %d established in %0.0f seconds.\n",
                    vitals.Connections,
                    difftime(Tc, To));
            }

            if (isRudics)
            {
                if (rudics_gw_login(modem, imei) < 0)
                {
                    // Failed to supply the IMEI
                    continue;
                }
            }

            if (login(modem, mission.user, mission.pwd) > 0)
            {
                status = 1;
                if (debuglevel>=2 || (debugbits&CLOGIN_H))
                {
                    LogEntry(
                        FuncName,
                        "Logged in to host.  [Login required %0.0f seconds]\n",
                        difftime(time(NULL),Tc));
                }

                break;
            }
        }
    }

    return status;
}

#endif /* CLOGIN_C */
