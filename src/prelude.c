#ifndef PRELUDE_H
#define PRELUDE_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define preludeChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes */
int Prelude(void);
int PreludeInit(void);
int PreludeTerminate(void);

#endif /* PRELUDE_H */
#ifdef PRELUDE_C
#undef PRELUDE_C

#include <config.h>
#include <control.h>
#include <eeprom.h>
#include <engine.h>
#include <gps.h>
#include <logger.h>
#include <recovery.h>
#include <Stm32f103Pwr.h>
#include <Stm32f103Rtc.h>
#include <stream.h>
#include <telemetry.h>

/*------------------------------------------------------------------------*/
/* execute tasks during the mission prelude                               */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the mission
   prelude.
*/
int Prelude(void)
{
   /* function name for log entries */
   cc *FuncName = "Prelude()";

   /* initialize the return value */
   int status=0;

   /* log the message */
   LogEntry(FuncName,"Logging configuration and initiating telemetry.\n");

   /* log the mission parameters */
   LogConfiguration(&mission,"Mission");
   
   /* execute recovery functionality */
   status=Recovery();
   
   /* parse if mission configuration was successfully downloaded */
   if (!(vitals.status&DownLoadCfg)) {configure(&mission,config_path,0);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the mission prelude                             */
/*------------------------------------------------------------------------*/
/**
   This function initializes the mission prelude.
*/
int PreludeInit(void)
{
   /* function name for log entries */
   cc *FuncName = "PreludeInit()";

   /* initialize the return value */
   int status=1;

   /* store the vacuum locally before reinitializing engineering data */
   unsigned int vac=vitals.Vacuum;
      
   /* get the current time and the current profile id */
   time_t now=itimer(); int pid=PrfIdGet(); 

   /* set the state to the mission prelude */
   StateSet(PRELUDE);

   /* initialize the profile id to zero */
   status=PrfIdSet(0);

   /* reset the interval timer */
   IntervalTimerSet(0,0);
   
   /* make a log entry */
   if (debuglevel>=2 || (debugbits&PRELUDE_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"Mission restarted. [pid=%d, itime=%ld]\n",pid,now);
   }
           
   /* initialize the engineering data */
   InitVitals();

   /* reinitialize the vacuum */
   vitals.Vacuum=vac;

   /* initialize the GPS fix */
   GpsFix=GpsInit;
   
   /* move the piston to full extenstion */
   PistonMoveAbs(mission.PistonFullExtension);
   
   return status;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int PreludeTerminate(void)
{
   /* define the logging signature */
   cc *FuncName = "PreludeTerminate()";

   /* initialize the return value */
   int status=1;

   /* local objects for MSG creation */
   FILE *fp;

   /* create a log file name */
   snprintf(prf_path,sizeof(prf_path),"%05u.000.msg",mission.FloatId);
   
   /* make sure the file system is valid and not full */
   if (!(fp=fopen(prf_path,"w")))
   {
      /* reformat the file system */
      LogClose(); fcloseall(); fformat(); LogPredicate(1);

      /* open the log file */
      LogOpen(log_path,'a'); LogEntry(FuncName,"Reformatted RAM file system.\n");

      /* open the msg file */
      fp=fopen(prf_path,"w");
   }

   /* write the mission configuration to the MSG file */
   if (fp) {WriteMissionConfig(fp,prf_path); fclose(fp);}

   /* initialize the telemetry phase */
   TelemetryInit();

   /* execute telemetry functions */
   status=Telemetry();
   
   return status;
}

#endif /* PRELUDE_C */
