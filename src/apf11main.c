#include <apf11.h>
#include <logger.h>
#include <control.h>
#include <cmds.h>
#include <eeprom.h>
#include <max7301.h>
#include <stream.h>
#include "revision_info.h"

/* function prototypes */
int FwStartUp(void);

/* the firmware revision is supplied in revision_info.h */
const unsigned long FwRev=FW_REV;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *
 *========================================================================*/
/* firmware to control an Apex Sbe41 profiler                             */
/*========================================================================*/
void apf11main(void)
{
   /* run firmware startup code */
   FwStartUp();

   /* check for user request to enter command mode */
   if (CmdModeRequest()) CmdMode();
  
   /* check magnetic switch user interface for mission launch request */
   if (MagSwitchToggled())
   {
      /* get the current state of the mission */
      enum State state = StateGet();
      
      /* if a mission is active then toggle to pressure-activation mode */
      if (state>INACTIVE && state<EOS) {StateSet(PACTIVATE);}
      
      /* launch the mission */
      else MissionLaunch();
      
      /* reset the magnetic reset-switch */
      MagSwitchReset();
   }

   /* transfer to mission control agent */
   MissionControlAgent();
}

/*------------------------------------------------------------------------*/
/* function to initialize the mission on program startup                  */
/*------------------------------------------------------------------------*/
/**
   This function initializes the mission at program startup.  A positive
   value is returned on success; otherwise zero is returned.
*/
int FwStartUp(void)
{
   /* define the logging signature */
   cc *FuncName = "FwStartUp()";

   /* initialize the return value */
   int status=0;

   /* accumulate the length of standby-time for the current profile cycle */
   vitals.SleepSec+=(Apf11WakeTime()-Apf11StandbyTime());

   /* validate the mission currently stored in nonvolatile ram */
   if (MissionValidate()<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Attempt to validate mission failed.  "
               "Initializing with default mission.\n");

      /* initialize the mission with the default mission */
      mission=DefaultMission;
   }
   else {status=1;}

   /* determine if the FatFs system is enabled or disabled */
   FatFsDisable=(!mission.FatFsEnable);
   
   /* validate the logstream */
   if (streamok(loghandle)<=0) loghandle=-1;

   /* set the debugging verbosity to match the mission configuration */
   debugbits=mission.DebugBits;

   /* validate the debuglevel */
   if (debuglevel>5) {debugbits=2; LogPredicate(1);}

   return status;
}
