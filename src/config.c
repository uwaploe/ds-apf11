#ifndef CONFIG_H
#define CONFIG_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define configChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <time.h>

#define CfgBufLen (63)

/*------------------------------------------------------------------------*/
/* structure to contain the parameters that define the float mission      */
/*------------------------------------------------------------------------*/
struct MissionParameters
{
   unsigned int   DebugBits;                   // Log Verbosity
   unsigned char  FatFsEnable;                 // Enable the FatFs filesystem
   unsigned int   FloatId;                     // Float ID
   unsigned short MaxAirBladder;               // Max Air Bladder ADC Count
   unsigned short OkVacuumCount;               // N/A
   unsigned short PistonBuoyancyNudge;         // Buoyancy Count to nudge the float
   unsigned short PistonDeepProfilePosition;   // Deep Profiling Piston Count
   unsigned short PistonFullExtension;         // Count to mark the full extension piston position
   unsigned short PistonFullRetraction;        // Count to mark the full retraction piston position
   unsigned short PistonInitialBuoyancyNudge;  // Piston nudge for coming up to the surface
   unsigned short PistonInitialDescentNudge;   // Piston nudge for descending
   unsigned short PistonPActivatePosition;     // Piston position to activate a mission
   unsigned short PistonParkHyperRetraction;   // Compensatory hyper retraction
   unsigned short PistonParkPosition;          // Park piston position
   unsigned short PnpCycleLength;              // How many park dives before a deep profile
   float          PressurePark;                // Park pressure activation
   float          PressureProfile;             // Deep profile pressure activation
   float          PressureCP;                  // Cp Activation pressure
   float          ActivationPressure;          // Pressure that activates the float
   time_t         ActivationPressurePeriod;    // Number of seconds between pressure samples
   time_t         TimeDeepProfileDescent;      // Sequence time for deep profile
   time_t         TimeDown;                    // Sequence time for park descent
   time_t         TimeOutAscent;               // Sequence time for ascending
   time_t         TimeParkDescent;             // Sequence time for descending
   time_t         TimePrelude;                 // Sequence time for the prelude
   time_t         TimeTelemetryRetry;          // Telemetry retry in minutes
   time_t         TimeUp;                      // Profile and telemetry time in minutes
   time_t         ToD;                         // Time of Day
   time_t         ConnectTimeOut;              // Seconds allowed to establish host connection
   time_t         ZModemTimeOut;               // Seconds allowed for ZModem transfers
   char           alt[CfgBufLen+1];            // Alternate modem AT dialstring
   char           at[CfgBufLen+1];             // Modem AT dialstring
   char           pwd[16];                     // Password for logging in to host computer
   char           user[16];                    // Login name on host computer
   unsigned char  EMASample;                   // Sampling option for EMA
   unsigned char  EMAMinVelocity;              // Minimum vertical velocity for ascent and descent when EMA sampling
   unsigned int   crc;
};

/* external prototypes */
int  configure(struct MissionParameters *config, const char *fname, int bypassSupervisor);
int  LogConfiguration(const struct MissionParameters *config,const char *id);
int  ConfigSupervisor(struct MissionParameters *cfg);

/* define some constants of nature */
#define Hour        (3600L)
#define Day         (86400L)
#define Min         (60L)
#define HeartBeat   (10L)

/* store the mission parameters in persistent ram to facilitate CRC computations */
extern persistent struct MissionParameters mission;

/* declare an initialization object for use when the APFx loses its mind */
extern const struct MissionParameters DefaultMission;

/* declare the mission configuration filename */
extern const char config_path[];

/* object to store the encoded firmware revision */
#if defined(__APF11__)
extern const unsigned long FwRev;
#endif

/* unit of time that defines the timescale of the mission (ie., 60sec or 3600sec) */
extern const time_t TQuantum;

/* the units of the time-quantization above (ie., minutes or hours) */
#define TUnits "Minutes"

/* estimated vertical ascent rate */
extern const float dPdt;

/* declarations for variables with external linkage */
extern const float MinN2ParkPressure;

#endif /* CONFIG_H */
#ifdef CONFIG_C
#undef CONFIG_C

/* define the quantum of time (should match TUnits above) */
const time_t TQuantum = Min;

#include <apf11.h>
#include <assert.h>
#include <crc16bit.h>
#include <ctype.h>
#include <ds2740.h>
#include <ema.h>
#include <extract.h>
#include <fatio.h>
#include <inrange.h>
#include <limits.h>
#include <logger.h>
#include <regex.h>
#include <stdlib.h>
#include <stream.h>
#include <string.h>
#include "revision_info.h"

#if defined (__arm__)
   #include <Stm32f103Rtc.h>
#endif /* __arm__ */

/* structure to store the mission configuration */
persistent struct MissionParameters mission;

/* default mission configuration */
const struct MissionParameters DefaultMission =
{
   .DebugBits = 2,
   .FatFsEnable = 1,
   .FloatId = 0,
   .MaxAirBladder = 2456,
   .OkVacuumCount = 3672,
   .PistonBuoyancyNudge = 152,
   .PistonDeepProfilePosition = 248,
   .PistonFullExtension = 3688,
   .PistonFullRetraction = 136,
   .PistonInitialBuoyancyNudge = 344,
   .PistonInitialDescentNudge = 792,
   .PistonPActivatePosition = 248,
   .PistonParkHyperRetraction = 0,
   .PistonParkPosition = 1048,
   .PnpCycleLength = 1,
   .PressurePark = 1000,
   .PressureProfile = 2020,
   .PressureCP = 985,
   .ActivationPressure = 25.0,
   .ActivationPressurePeriod = 5 * Min,
   .TimeDeepProfileDescent = 5*Hour,
   .TimeDown = 10*Day,
   .TimeOutAscent = 9*Hour,
   .TimeParkDescent = 5*Hour,
   .TimePrelude = 6*Hour,
   .TimeTelemetryRetry = 15*Min,
   .TimeUp = 11*Hour,
   .ToD = -1,
   .ConnectTimeOut = 1*Min,
   .ZModemTimeOut = 80,
   .alt = "ATDT0012066163256",
   .at = "ATDT0012066859312",
   .pwd = "e=2.718",
   .user = "iridium",
   .EMASample = EMA_SAMPLE_BOTH,
   .EMAMinVelocity = 12
};

/* define the name of the configuration file */
const char config_path[] = "mission.cfg";

/* define the name of the configuration file */
persistent char log_path[32];

/* define the estimated vertical ascent rate */
const float dPdt=0.08;

/* define the minimum park pressure for N2 floats */
const float MinN2ParkPressure=850;

/* prototypes for functions with static linkage */
static unsigned char atouc(const char *s);
static int fatal(const char *expr, const char *why);
static int warn(const char *expr, const char *why);

/* prototypes for functions with external linkage */
int    fcloseall(void);
int    fformat(void);
time_t ialarm(void);
time_t itimer(void);
int    MissionParametersWrite(struct MissionParameters *mission);
void   PowerOff(time_t AlarmSec);
int    RecoveryInit(void);

/*------------------------------------------------------------------------*/
/* function to parse a configuration file                                 */
/*------------------------------------------------------------------------*/
/**
   This function is designed a parse a configuration file and install
   configurator arguments into a MissionParameters object.  The ability to
   accomplish 2-way communication and remote control via the Iridium system
   was the major motivator for implementing configurable operation.  Remote
   control of the mission is accomplished by creating a configuration file
   on the float's host computer.  The syntax of configuration files is
   tightly controlled and accomplished through the use of "configurators".

   Strict syntax rules are rigidly enforced as a protective measure against
   accidental and perhaps fatal misconfiguration.  Every line in the
   configuration file must be either a blank line (ie., all white space), a
   comment (first non-whitespace character must be '\#'), or a well-formed
   configurator.  Configurators have a fixed syntax:

      \begin{verbatim}
      ParameterName(argument) [CRC]
      \end{verbatim}
  
   where ParameterName satisfies the regex "[a-zA-Z0-9]{1,31}" (ie., maximum
   of 31 characters), argument satisfies the regex ".*", and the [CRC] field
   is optional but, if present, then must satisfy the regex
   "\[(0x[0-9a-fA-F]{1,4})\]".  That is, the opening and closing brackets
   are literal characters "[]" that bracket a string that represents a 4-16
   bit hexidecimal number.  If the CRC field is present then it represents
   the 16-bit CRC of the configurator: "ParameterName(argument)".  The CRC
   of the configurator is computed and checked against the CRC specified in
   the configurator.  The CRCs must have the same value or else the
   configuration attempt fails.  The CRC is generated by the CCITT
   polynomial.  See comment section of Crc16Bit() for details.

   It is very important to note that any white space in the argument is
   treated as potentially significant.  Every byte (including white space)
   between the parentheses is considered to be a non-negligible part of the
   argument.  In cases where the argument string is converted to a number
   then the presence of extraneous white space won't matter.  However, if
   the argument represents, say, a login name or a passord then extraneous
   space would be fatal.
   
   Only one configurator per line is allowed and the configurator must be
   the left-most text on the line except that it can be preceeded by an
   arbitrary amount of whitespace.  No text, except for an arbitrary amount
   of white space, is allowed to the right of the rightmost closing
   parenthesis.  The maximum length of a line (including white space) is 126
   bytes and the maximum length of the ParameterName is 31 bytes.

   If any syntax error is detected in the configuration file or if the
   argument of a configurator failes a range check then the configuration
   attempt fails.  In this case then the (const) default configuration will
   be used.  The default configuration is hardwired into the source code to
   prevent it from being tampered with or corrupted in any way.

   \begin{verbatim}
   MissionParameters parameters:

      ActivateRecoveryMode ..... Induce the float into recovery mode and
                                 initiate telemetry at regular intervals
                                 given by the TelemetryRetry.  No argument
                                 is required and the argument, if present,
                                 is ignored.

      ActivationPressure         The float pressure that must be achieved before
                                 the float is activated to start a mission.

      ActivationPressurePeriod   The amount of seconds in between sampling the
                                 pressure during the PACTIVATE phase.

      AscentTimeOut ............ The initial segment of the uptime that is
                                 designated for profiling and vertical
                                 ascent.  If the surface has not been
                                 detected by the time this timeout expires
                                 then the profile will be aborted and the
                                 telemetry phase will begin.  The valid
                                 range is 1 minute to 10 hours.
                  
      AtDialCmd ................ The modem AT dialstring used to connect to
                                 the primary host computer.  
                               
      AltDialCmd ............... The modem AT dialstring used to connect to
                                 the alternate host computer. 

      BuoyancyNudge ............ The piston extension (counts) applied each
                                 time the ascent rate falls below the
                                 user-specified minimum.  This adds buoyancy
                                 to the float in order to maintain the
                                 ascent rate.

      CompensatorHyperRetraction Floats with N2-compensators require the
                                 piston to be hyper-retracted in order to
                                 descend from the surface to the park
                                 level.  The valid range is 0-254 counts.

      ConnectTimeOut ........... The number of seconds allowed after dialing
                                 for a connection to be established with the
                                 host computer.  The valid range is 30-300
                                 sec.

      CoulombModelInit ......... The accumulated charge (kilo-Coulombs)
                                 drained from the battery packs will
                                 be initialized with the argument of
                                 this configurator.  All other charge
                                 consumption model parameters are reset
                                 to zero.

      CpActivationP ............ The pressure where the control firmware
                                 transitions from subsampling the water
                                 column (in the deep water) to where the CP
                                 mode of the SBE41cp is activated for a high
                                 resolution profile to the surface.

                                 Important note: The SBE41CP is not designed
                                 for subsampling in the presence of
                                 significant temperature gradients.  The
                                 pump period for spot samples is
                                 insufficient to drive thermal mass errors
                                 down to an acceptable level.

      DeepProfileDescentTime ... This time determines the maximum amount of
                                 time allowed for the float to descent from
                                 the park pressure to the deep profile
                                 pressure.  The deep profile is initiated
                                 when the DeepProfileDescentTime expires or
                                 else the float reaches the deep profile
                                 pressure, whichever occurs first.  The
                                 valid range is 0-8 hours.

      DeepProfilePistonPos ..... The control firmware retracts the piston to
                                 the DeepProfilePistonPos in order to
                                 descend from the park pressure to the deep
                                 profile pressure.  The DeepProfilePistonPos
                                 should be set so that the float can reach
                                 the deep profile pressure before the deep
                                 profile descent period expires.  The valid
                                 range is 1-254 counts.

      DeepProfilePressure ...... This is the target pressure for deep
                                 profiles.  The valid range is 0-2050 dbar.

      DownTime ................. This determines the length of time that the
                                 float drifts at the park pressure before
                                 initiating a profile.  The valid range is 1
                                 min to 30 days.

      FatFsClean ............... This command requires no argument and
                                 causes the FatFs volume to be
                                 erased. The FatFs system must be
                                 enabled for this to work.  WARNING:
                                 All contents of the uSD card will be
                                 destroyed.

      FatFsCreate .............. This command requires no argument and
                                 causes the FatFs volume to be
                                 created.  This command is relatively
                                 time consuming (~5 minutes).  The
                                 FatFs system must be enabled for this
                                 to work. WARNING: All contents of the
                                 SD card will be destroyed.

      FatFsEnable .............. This command enables or disables the
                                 FatFs file system.  A nonzero
                                 argument enables FatFs; zero disables
                                 FatFs. 

      FloatId .................. The float identifier.

      InitialBuoyancyNudge ..... The piston extension (counts) applied in
                                 order to initiate the vertical ascent at
                                 the beginning of the profile.  This same
                                 nudge is also applied at the end of the
                                 profile to make sure the float reaches the
                                 surface.
      
      InitialDescentNudge ...... The piston retraction (counts) applied in
                                 order to initiate the vertical descent.
                                 This is similar to InitialBuoyancyNudge,
                                 but used during the descent instead of the
                                 ascent. Note it is only consulted when EM
                                 sampling on descent.

      MaxAirBladder ............ The cut-off pressure (in A/D counts)
                                 for air-bladder inflation.  The air
                                 pump will be deactivated when the air
                                 bladder pressure exceeds the cut-off.
                                 The valid range is 1 to 240 counts.
                                 The recommended maximum air bladder
                                 pressure is in the range [17.0,18.2]
                                 PSIA.

      MaxLogKb ................. The maximum size of the logfile in
                                 kilobytes.  Once the log grows beyond this
                                 size, logging is inhibited and the logfile
                                 will be automatically deleted at the start
                                 of the next profile.  The valid range is
                                 5-63KB.

      ParkDescentTime .......... This time determines the maximum amount of
                                 time allowed for the float to descent from
                                 the surface to the park pressure.  The
                                 active ballasting phase is initiated when
                                 the ParkDescentTime expires.  The valid
                                 range is 0-8 hours.

      ParkPistonPos ............ The control firmware retracts the piston to
                                 the ParkPistonPos in order to descend from
                                 the surface to the park pressure.  The
                                 ParkPistonPos should be set so that the
                                 float will become neutrally buoyant at the
                                 park pressure.  The valid range is 1-254
                                 counts.
                                 
      ParkPressure ............. This is the target pressure for the active
                                 ballasting algorithm during the park phase
                                 of the mission cycle.  The valid range is
                                 0-2000 dbar.

      PnPCycleLen .............. A deep profile is initiated when the
                                 profile id is an integral multiple of
                                 PnPCycleLen.  All other profiles will be
                                 collected from the park pressure to the
                                 surface. 
      
      Pwd ...................... The password used to login to the host
                                 computer.

      RetrieveArchive .......... Retrieve file(s) from archive to be
                                 written into the FatFs volume so that
                                 they will be retransmitted via
                                 Iridium.  The argument of this
                                 function is a globbing pattern to use
                                 for file retrieval.  Two
                                 meta-characters (?,*) are
                                 implemented; '?' substitutes for any
                                 single character while '*'
                                 subsititues for any string of length
                                 zero or more.

      TelemetryRetry ........... This determines the time period between
                                 attempts to successfully complete telemetry
                                 tasks after each profile.  The valid range
                                 is 1 minute to 6 hours.

      TimeOfDay ................ This allows the user to specify that the
                                 down-time should expire at a specific time
                                 of day (ToD).  The ToD feature allows the
                                 user to schedule profiles to happen at
                                 night (for example).  

                                 The ToD is expressed as the number of
                                 minutes after midnight (GMT).  The valid
                                 range is 0-1439 minutes.  Any value outside
                                 this range will cause the ToD feature to be
                                 disabled.

      UpTime ................... This determines the maximum amount time
                                 allowed to execute the profile and complete
                                 telemetry.  The valid range is 1 minute to
                                 1 day.

      User ..................... The login name on the host computer that
                                 the float uses to upload and download data.
                                 itself.

      Verbosity ................ An integer in the range [0,4] that
                                 determines the logging verbosity with lower
                                 values producing more terse logging.  A
                                 verbosity of 2 yields standard logging.

      ZModemTimeOut ............ The number of seconds allowed for
                                 expiration of Zmodem packet
                                 transfers.  The valid range is
                                 [30,600] sec.
      
      EMASample ................ Integer in range [0, 3] which determines if and
                                 when EM sampling should be performed. Value of 0 =
                                 no sampling, 1 = sample ascent only, 2 = sample
                                 descent only, and 3 = sample both descent and ascent.

      EMAMinVelocity ........... The minimum velocity allowed, before being nudged,
                                 when EM sampling, in whole centimeters per second.
                                 Reasonable values are about 5 to 20 cm/s.
   \end{verbatim}

   A sample configuration file is below.
   
      \begin{verbatim}
      # Modem AT command to dial the phone
      AtDialCmd(ATDT12066859312) [0x0dc8]
      
      # Time-out period (seconds) for connect-attempt
      # ConnectTimeOut(60) [0xeb41]

      # Password
      # Pwd(e=2.718) [0xb369]
      
      # user name
      # User(iridium) [0x835f]
      
      # Debug level for logging
      Verbosity(2) [0x7afc]

      # Maximum number of kilobytes for the log file
      # MaxLogKb(25) [0xDD06]
      \end{verbatim}

      input:
         config ........... The current mission parameters to be updated if successful.
         fname ............ The filename to parse the new mission parameters.
         bypassSupervisor . Option to bypass the configuration supervisor that checks values
             for range and sanity contraints.  Warning: only bypass this check in testing
             and controlled situations.

      output:
         A positive value is returned on success, i.e., the file was successfully opened and
         parsed and the parameters were validated, and a non-positive value indicates failure.
*/ 
int configure(struct MissionParameters *config, const char *fname, int bypassSupervisor)
{
   /* define the logging signature */
   cc *FuncName = "configure()";

   /* initialize the return value */
   int status=-1;
   FILE *source=NULL;
   
   /* validate pointer to configuration object */
   if (!config)
   {
      /* log the message */
      LogEntry(FuncName,"Invalid configuration object.\n");
   }

   /* validate pointer to configuration file */
   else if (!fname)
   {
      /* log the message */
      LogEntry(FuncName,"Invalid configuration file.\n");
   }

   /* open the configuration file */
   else if (!(source=fopen(fname,"r")))
   {
      /* log the message */
      LogEntry(FuncName,"Attempt to open \"%s\" failed.\n",fname);

      /* reinitialize the function's return value */
      status=0;
   }
   
   else
   {
      #define MAXLEN (126)
      #define NSUB (6)
      int i,k,len;
      unsigned int crc1,crc0;
      char *p,buf[MAXLEN+2],configurator[MAXLEN+2],parm[CfgBufLen+1],
         arg[CfgBufLen+1],glob[FILENAME_MAX+1]={0};
      regex_t regex;
      regmatch_t regs[NSUB+1];
      int errcode;
      int RecoveryMode=0,FsClean=0,FsCreate=0;
      struct MissionParameters cfg;

      #undef  CRC
      #define WS  "[ ]*"
      #define VAR "([a-zA-Z0-9]+)"
      #define VAL "\\((.*)\\)"
      #define CRC "(\\[(0x[0-9a-fA-F]{1,4})\\])?"
   
      /* construct the regex pattern string for files with message locks */ 
      const char *pattern = "^" WS "((" VAR WS VAL ")" WS CRC ")" WS "$";
      
      /* define the configuration parameters */
      enum {ActivateRecoveryMode, ActivationPressure, ActivationPressurePeriod, AscentTimeOut, AtDialCmd, AltDialCmd, BuoyancyNudge,
            CompensatorHyperRetraction, ConnectTimeOut, CoulombModelInit, CpActivationP,
            DeepProfileDescentTime, DeepProfilePistonPos, DeepProfilePressure, DownTime,
            FatFsClean, FatFsCreate, FatFsEnable, FloatId, InitialBuoyancyNudge, InitialDescentNudge,
            MaxAirBladder, MaxLogKb, ParkDescentTime, ParkPistonPos, ParkPressure,
            PnPCycleLen, Pwd, RetrieveArchive, TelemetryRetry, ToD, UpTime, User,
            Verbosity, ZModemTimeOut, EMASample, EMAMinVelocity, NVAR};

      /* define the names of the configuration parameters */ 
      const char *parms[NVAR];

      /* reinitialize the return value */
      status=1;
      
      /* initialize the configuration */
      cfg=*config;
 
      /* create the metacommand table */
      parms[ActivateRecoveryMode]       = "activaterecoverymode";
      parms[ActivationPressure]         = "activationpressure";
      parms[ActivationPressurePeriod]   = "activationpressureperiod";
      parms[AscentTimeOut]              = "ascenttimeout";
      parms[AtDialCmd]                  = "atdialcmd";
      parms[AltDialCmd]                 = "altdialcmd";
      parms[BuoyancyNudge]              = "buoyancynudge";
      parms[CompensatorHyperRetraction] = "compensatorhyperretraction";
      parms[ConnectTimeOut]             = "connecttimeout";
      parms[CoulombModelInit]           = "coulombmodelinit";
      parms[CpActivationP]              = "cpactivationp";
      parms[DeepProfileDescentTime]     = "deepprofiledescenttime";
      parms[DeepProfilePistonPos]       = "deepprofilepistonpos";
      parms[DeepProfilePressure]        = "deepprofilepressure";
      parms[DownTime]                   = "downtime";
      parms[FatFsClean]                 = "fatfsclean";
      parms[FatFsCreate]                = "fatfscreate";
      parms[FatFsEnable]                = "fatfsenable";
      parms[FloatId]                    = "floatid";
      parms[InitialBuoyancyNudge]       = "initialbuoyancynudge";
      parms[InitialDescentNudge]        = "initialdescentnudge";
      parms[MaxAirBladder]              = "maxairbladder";
      parms[MaxLogKb]                   = "maxlogkb";
      parms[ParkDescentTime]            = "parkdescenttime";
      parms[ParkPistonPos]              = "parkpistonpos";
      parms[ParkPressure]               = "parkpressure";
      parms[PnPCycleLen]                = "pnpcyclelen";
      parms[Pwd]                        = "pwd";
      parms[RetrieveArchive]            = "retrievearchive";
      parms[TelemetryRetry]             = "telemetryretry";
      parms[ToD]                        = "timeofday";
      parms[UpTime]                     = "uptime";
      parms[User]                       = "user";
      parms[Verbosity]                  = "verbosity";
      parms[ZModemTimeOut]              = "zmodemtimeout";
      parms[EMASample]                  = "emasample";
      parms[EMAMinVelocity]             = "emaminvelocity";

      /* compile the option pattern */
      assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

      /* protect against segfaults */
      assert(NSUB==regex.re_nsub);
      
      /* make a log entry that the configuration file will be parsed */
      if (debuglevel>=2 || (debugbits&CONFIG_H))
      {
         /* log the message */
         LogEntry(FuncName,"Parsing configurators in \"%s\".\n",fname);
      }
      
      /* read each line of the configuration file */
      while (fgets(buf,MAXLEN+2,source))
      {
         /* make sure the line length doesn't exceed maximum */
         if ((len=strlen(buf))>MAXLEN)
         {
            /* make a log entry */
            LogEntry(FuncName,"Line length exceeds %d bytes: %s\n",MAXLEN,buf);

            /* indicate failure of the configuration attempt */
            status=0; continue;
         }

         /* check if the current line matches the regex */
         if ((errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0))==REG_NOMATCH)
         {
            /* find the location in buf of the first non-whitespace character */
            int index = strspn(buf," \t\r\n");
            
            /* check if current line is all white space */
            if (index!=len && buf[index]!='#')
            {
               /* copy the configurator to the log file */
               if (debuglevel>=2 || (debugbits&CONFIG_H))
               {
                  /* make a log entry */
                  LogEntry(FuncName,"Syntax error: %s",buf);
               }
               
               /* indicate failure of the configuration attempt */
               status=0;
            }
            
            continue;
         }

         /* check for pathological regex conditions */
         else if (errcode)
         {
            /* map the regex error code to an error string */
            char errbuf[128]; regerror(errcode,&regex,errbuf,128);

            /* print the regex error string */
            LogEntry(FuncName,"Exception in regexec(): %s\n",errbuf); 
 
            /* indicate failure of the configuration attempt */
            status=0;
         }

         /* regex match found - process the configurator */
         else
         {
            /* extract the configurator from the line */
            strcpy(configurator,extract(buf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so));
         
            /* copy the configurator to the log file */
            if (debuglevel>=2 || (debugbits&CONFIG_H))
            {
               /* write the configurator to the logfile */
               LogEntry(FuncName,"   %s ",configurator);
            }
            
            /* check length of parameter identifier */
            if ((regs[3].rm_eo-regs[3].rm_so)>CfgBufLen)
            {
              LogAdd("[Configurator name exceeds %d bytes].\n",CfgBufLen);

               /* indicate failure of the configuration attempt */
               status=0; continue;
            } 

            /* extract the parameter from the configurator */
            else strcpy(parm,extract(buf,regs[3].rm_so+1,regs[3].rm_eo-regs[3].rm_so));

            /* check length of parameter argument identifier */
            if ((regs[4].rm_eo-regs[4].rm_so)>CfgBufLen)
            {
               LogAdd("[Configurator argument exceeds %d bytes].\n",CfgBufLen);

               /* indicate failure of the configuration attempt */
               status=0; continue;
            } 

            /* extract the argument from the configurator */
            else strcpy(arg,extract(buf,regs[4].rm_so+1,regs[4].rm_eo-regs[4].rm_so));
            
            /* extract the configurator (less CRC) from the line */
            p = extract(buf,regs[2].rm_so+1,regs[2].rm_eo-regs[2].rm_so);
            
            /* compute the CRC of the configurator */
            crc0 = Crc16Bit((unsigned char *)p,regs[2].rm_eo-regs[2].rm_so);

            /* check for an optional CRC */
            if (regs[5].rm_so!=-1 && regs[5].rm_eo!=-1)
            {
               /* extract the hex CRC from the configurator */
               p = extract(buf,regs[6].rm_so+1,regs[6].rm_eo-regs[6].rm_so);
            
               /* convert the hex CRC to decimal */
               crc1 = strtoul(p,NULL,16);

               /* check if the CRCs match */
               if (crc0!=crc1) 
               {
                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry that the CRC check failed */
                     LogAdd("[CRC check failed: 0x%04X != 0x%04X].\n",crc0,crc1);
                  }

                  /* indicate failure of the configuration attempt */
                  status=0; continue;
               }
            }

            /* add the configurator CRC */
            else if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[CRC=0x%04X] ",crc0);  
         
            /* convert the variable name to lower case */
            for (i=regs[3].rm_so; i<regs[3].rm_eo; i++) {buf[i] = tolower(buf[i]);}

            /* search the configuration for a matching parameter name */
            for (k=0; k<NVAR; k++)
            {
               /* check if the regex parameter matches a configuration parameter */
               if (!strncmp(parms[k],buf+regs[3].rm_so,strlen(parms[k]))) break;
            }

            switch (k)
            {
               /* activate recovery mode */
               case ActivateRecoveryMode:
               {
                  RecoveryMode=1;

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[ActivateRecoveryMode()].\n");

                  break;
               }

               case ActivationPressure:
               {
                  cfg.ActivationPressure = atof(arg);
                  if (debuglevel >= 2 || (debugbits & CONFIG_H))
                  {
                     LogAdd("[ActivationPressure(%0.1f)].\n", cfg.ActivationPressure);
                  }

                  break;
               }

               case ActivationPressurePeriod:
               {
                  cfg.ActivationPressurePeriod = atol(arg);
                  if (debuglevel >= 2 || (debugbits & CONFIG_H))
                  {
                     LogAdd("[ActivationPressurePeriod(%ld)].\n", cfg.ActivationPressurePeriod);
                  }

                  break;
               }

               /* configurator for ascent time-out period */
               case AscentTimeOut: 
               {
                  /* insert the timeout period into configuration */
                  cfg.TimeOutAscent=atol(arg)*Min;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[AscentTimeOut(%ld)].\n",cfg.TimeOutAscent/Min);
                  }
               
                  break;
               }

               /* Modem AT dialstring configurator */
               case AtDialCmd: 
               {
                  /* copy the dialstring into static storage and insert into configuration */
                  strncpy(cfg.at,arg,sizeof(cfg.at)-1);

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[AtDialCmd(%s)].\n",cfg.at);
               
                  break;
               }

               /* Modem AT dialstring configurator for alternate host */
               case AltDialCmd: 
               {
                  /* copy the dialstring into static storage and insert into configuration */
                  strncpy(cfg.alt,arg,sizeof(cfg.alt)-1);

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[AltDialCmd(%s)].\n",cfg.alt);
               
                  break;
               }

               /* configurator for buoyancy nudge */
               case BuoyancyNudge:
               {
                  /* insert the buoyancy nudge into configuration */
                  cfg.PistonBuoyancyNudge=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[BuoyancyNudge(%d)].\n",cfg.PistonBuoyancyNudge);
                  }
                  
                  break;
               }

               /* configurator for descent hyper-retraction */
               case CompensatorHyperRetraction:
               {
                  /* insert the park pressure into configuration */
                  cfg.PistonParkHyperRetraction=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[CompensatorHyperRetraction(%d)].\n",
                            cfg.PistonParkHyperRetraction);
                  }
                  
                  break;
               }
               
               /* configurator for connection time-out period */
               case ConnectTimeOut: 
               {
                  /* insert the timeout period into configuration */
                  cfg.ConnectTimeOut=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[ConnectTimeOut(%ld)].\n",cfg.ConnectTimeOut);
                  }
               
                  break;
               }

               /* configurator to reinitialize the charge consumption model */
               case CoulombModelInit:
               {
                  /* reinitialize the charge consumption model */
                  ChargeModelInit();

                  /* set the accumulated charge consumption */
                  cmodel.Coulombs=atof(arg)*1000;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[CoulombModelInit(%0.1f)].\n",cmodel.Coulombs/1000);
                  }

                  break;
               }

               /* configurator for SBE41cp acrivation pressure */
               case CpActivationP:
               {
                  float p = atof(arg);

                  /* condition the activation pressure */
                  if (p<0) p=0; else if (p>2500) p=2500;
                  
                  /* insert the profile pressure into configuration */
                  cfg.PressureCP=p;

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     LogAdd("[CpActivationP(%g)].\n",cfg.PressureCP);
                  }

                  break;
               }

               /* configurator for profilek descent time */
               case DeepProfileDescentTime:
               {
                  /* insert the downtime into configuration */
                  cfg.TimeDeepProfileDescent=atol(arg)*Min;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[DeepProfileDescentTime(%ld)].\n",
                            cfg.TimeDeepProfileDescent/Min);
                  }
                  
                  break;
               }

               /* configurator for park pressure */
               case DeepProfilePistonPos:
               {
                  /* insert the park pressure into configuration */
                  cfg.PistonDeepProfilePosition=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[DeepProfilePistonPos(%d)].\n",
                            cfg.PistonDeepProfilePosition);
                  }
                  
                  break;
               }

               /* configurator for profile pressure */
               case DeepProfilePressure:
               {
                  /* insert the profile pressure into configuration */
                  cfg.PressureProfile=atof(arg);

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     LogAdd("[ProfilePressure(%g)].\n",cfg.PressureProfile);
                  }

                  break;
               }

               /* configurator for profile cycle downtime */
               case DownTime:
               {
                  /* insert the downtime into configuration */
                  cfg.TimeDown=atol(arg)*Min;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */
                     LogAdd("[DownTime(%ld)].\n",cfg.TimeDown/Min);
                  }
                  
                  break;
               }
               
               /* configurator to erase the FatFs volume */
               case FatFsClean:
               {
                  FsClean=1;

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[FatFsClean()].\n");

                  break;
               }

               /* configurator to create a new FatFs volume on the SD card */
               case FatFsCreate:
               {
                  FsCreate=1;

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[FatFsCreate()].\n");

                  break;
               }

               /* enable or disable the FatFs file system */
               case FatFsEnable:
               {
                  cfg.FatFsEnable=(atoi(arg))?1:0;
                  
                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */
                     LogAdd("[FatFsEnable(%d)].\n",cfg.FatFsEnable);
                  }
 
                  break;
               }

               /* configurator for the float id */
               case FloatId:
               {
                  /* insert the downtime into configuration */
                  cfg.FloatId=atoi(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[FloatId(%04u)].\n",cfg.FloatId);
                  }
                  
                  break;
               }

               /* configurator for initial buoyancy nudge */
               case InitialBuoyancyNudge:
               {
                  /* insert the initial buoyancy nudge into configuration */
                  cfg.PistonInitialBuoyancyNudge=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */
                     LogAdd("[InitialBuoyancyNudge(%d)].\n",
                            cfg.PistonInitialBuoyancyNudge);
                  }
                  
                  break;
               }

               // configurator for initial descent nudge
               case InitialDescentNudge: {

                  // insert the initial descent nudge into configuration
                  cfg.PistonInitialDescentNudge = atol(arg);

                  if(debuglevel >= 2 || (debugbits & CONFIG_H)) {
                     LogAdd("[InitialDescentNudge(%d)].\n", cfg.PistonInitialDescentNudge);
                  }

                  break;
               }
               
               /* configurator for maximum air bladder pressure */
               case MaxAirBladder:
               {
                  /* insert the timeout period into configuration */
                  cfg.MaxAirBladder = (unsigned short)atoi(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[MaxAirBladder(%u)].\n",cfg.MaxAirBladder);
                  }
               
                  break;
               }
 
               /* Maximum log size configurator */
               case MaxLogKb:
               {
                  /* convert the string to an integer */
                  unsigned int maxlogkb = atoi(arg);

                  /* validate the maximum log size */
                  if (maxlogkb>=5 && (maxlogkb*KB)<=(BUFSIZE-(KB/4)))
                  {
                     /* insert maximum log file size configuration */
                     MaxLogSize=(maxlogkb*KB);

                     /* make a log entry that maximum log size */
                     if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[MaxLogKb(%d)].\n",maxlogkb);
                  }
                  else {LogAdd("[Ignoring invalid log size limit: %d Kbytes].\n",maxlogkb);}
                  
                  break;
               }

               /* configurator for park descent time */
               case ParkDescentTime:
               {
                  /* insert the park descent time into configuration */
                  cfg.TimeParkDescent=atol(arg)*Min;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[ParkDescentTime(%ld)].\n",cfg.TimeParkDescent/Min);
                  }
                  
                  break;
               }

               /* configurator for park piston position */
               case ParkPistonPos:
               {
                  /* insert the park piston position into configuration */
                  cfg.PistonParkPosition=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[ParkPistonPos(%d)].\n",cfg.PistonParkPosition);
                  }
                  
                  break;
               }

               /* configurator for park pressure */
               case ParkPressure:
               {
                  /* insert the park pressure into configuration */
                  cfg.PressurePark=atof(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[ParkPressure(%g)].\n",cfg.PressurePark);
                  }
                  
                  break;
               }

               /* configurator for PnP cycle length */
               case PnPCycleLen:
               {
                  /* insert the PnP cycle length into configuration */
                  cfg.PnpCycleLength=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[PnpCycleLength(%d)].\n",cfg.PnpCycleLength);
                  }
                  
                  break;
               }

               /* password configurator */
               case Pwd: 
               {
                  /* copy the password into the configuration */
                  strncpy(cfg.pwd,arg,sizeof(cfg.pwd)-1);

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[Pwd(%s)].\n",cfg.pwd);
               
                  break;
               }

               /* retrieve files from the archive */
               case RetrieveArchive:
               {
                  /* copy the glob */
                  strncpy(glob,arg,FILENAME_MAX);
                          
                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[RetrieveArchive(%s)].\n",arg);
                  }
                  
                  break;
               }

               /* configurator for telemetry retry period */
               case TelemetryRetry:
               {
                  /* insert the telemetry retry into configuration */
                  cfg.TimeTelemetryRetry=atol(arg)*Min;

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[TelemetryRetry(%ld)].\n",cfg.TimeTelemetryRetry/Min);
                  }
                  
                  break;
               }

               /* configurator for profile time-of-day specification */
               case ToD:
               {
                  long int tod=atol(arg);
                  
                  /* insert the time-of-day specification into configuration */
                  cfg.ToD=(inRange(0,tod,1440)) ? (tod*Min) : -1;
                  
                  if (!inRange(0,cfg.ToD,Day))
                  {
                     LogAdd("Time-of-day specification disabled.\n");
                  }
                  
                  else if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */
                     LogAdd("[TimeOfDay(%ld)].\n",cfg.ToD/Min);
                  }
                     
                  break;
               }

               /* configurator for profile cycle uptime */
               case UpTime:
               {
                  /* insert the uptime into configuration */
                  cfg.TimeUp=atol(arg)*Min;

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[UpTime(%ld)].\n",cfg.TimeUp/Min);

                  break;
               }
               
               /* user configurator */
               case User: 
               {
                  /* copy the username into the configuration */
                  strncpy(cfg.user,arg,sizeof(cfg.user)-1);

                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[User(%s)].\n",cfg.user);
               
                  break;
               }

               /* Logging verbosity configurator */
               case Verbosity: 
               {
                  /* convert the string to an integer */
                  long int verbosity=strtoul(arg,NULL,16);

                  /* validate the verbosity specification */
                  if (verbosity>=0 && (verbosity&VERBOSITYMASK)<=4 && verbosity<=UINT_MAX)
                  {
                     if (debuglevel>=2 || (debugbits&CONFIG_H))
                     {
                        /* make a log entry warning of terse debugging mode */
                        if (!verbosity) LogAdd("[Warning: terse mode enabled - "
                                               "Verbosity(0x%04x)].\n",verbosity);

                        /* make a log entry */
                        else LogAdd("[Verbosity(0x%04x)].\n",verbosity);
                     }
                     
                     /* insert debug level into configuration */
                     cfg.DebugBits=verbosity; LogPredicate(1);
                  }
                  else {LogAdd("[Ignoring invalid verbosity: 0x%04x].\n",verbosity);}
                     
                  break;
               }
               
               /* configurator for Zmodem time-out period */
               case ZModemTimeOut: 
               {
                  /* insert the timeout period into configuration */
                  cfg.ZModemTimeOut=atol(arg);

                  if (debuglevel>=2 || (debugbits&CONFIG_H))
                  {
                     /* make a log entry */                     
                     LogAdd("[ZModemTimeOut(%ld)].\n",cfg.ZModemTimeOut);
                  }
               
                  break;
               }

               // for the sample ema parameter
               case EMASample: {

                  // only update if in range [0, 3]
                  unsigned char flag = atouc(arg);
                  if(flag >= EMA_SAMPLE_NONE && flag <= EMA_SAMPLE_BOTH) {
                     cfg.EMASample = flag;

                     if (debuglevel>=2 || (debugbits & CONFIG_H)) {
                        LogAdd("[EMASample(%d)].\n", cfg.EMASample);

                     } else {
                        LogAdd("[Ignoring invalid EMASample: %d].\n", flag);
                     } 
                  }

                  break;
               }

               // for the ema minimum vertical velocity
               case EMAMinVelocity: {

                  // only update if in the range [5, 20]
                  unsigned char velocity = atouc(arg);
                  if(velocity >= 5 && velocity <= 20) {
                     cfg.EMAMinVelocity = velocity;

                     if (debuglevel>=2 || (debugbits&CONFIG_H)) {
                        LogAdd("[EMAMinVelocity(%d)].\n", cfg.EMAMinVelocity);

                     } else {
                        LogAdd("[Ignoring invalid EMAMinVelocity: %d].\n", velocity);
                     }
                  }

                  break;
               }
               
               default:
               {
                  /* make a log entry */
                  if (debuglevel>=2 || (debugbits&CONFIG_H)) LogAdd("[Unrecognized configurator].\n");

                  /* indicate configuration failure */
                  status=0;
               }
            }
         }
      }

      /* close the configuration file */
      fclose(source);
      
      /* clean up the regex pattern buffer */
      regfree(&regex);

      /* validate config file against CRC and syntax errors */
      if (status>0)
      {
         if (debuglevel>=2 || (debugbits&CONFIG_H))
         {
            /* log the message */
            LogEntry(FuncName,"Configuration CRCs and syntax OK.\n");
         }

         /* subject the proposed configuration to the mission enforcer */
         // but bypass the supervisor if input flag is set
         if (bypassSupervisor || ((status=ConfigSupervisor(&cfg)) == 0))
         {
            // if we bypass the supervisor we also have to compute the crc
            // because the supervisor does this on a valid config
            if(bypassSupervisor) {
               cfg.crc = Crc16Bit((unsigned char *)(&cfg), sizeof(cfg) - sizeof(cfg.crc));
            }

            /* determine if the configurations are different */
            if (config->crc != cfg.crc)
            {
               /* activate the local/temporary configuration */
               *config=cfg; debugbits=cfg.DebugBits;

               /* write the new mission configuration to EEPROM */
               if (MissionParametersWrite(config)<=0)
               {
                  LogEntry(FuncName,"Attempt to write mission configuration to EEPROM failed.\n");
               }

               /* Log the new mission configuration */
               else LogConfiguration(config,"Mission");
            }
         }

         /* determine if the FatFs should be enabled or disabled */
         if (FatFsDisable!=(!cfg.FatFsEnable)) {FatFsDisable=(!cfg.FatFsEnable);}
         
         /* check if the FatFs volume should be rebuilt */
         if (FsCreate>0)
         {
            LogEntry(FuncName,"Creating the FatFs volume.\n");

            /* rebuild the FatFs volume */
            fcloseall();
            
            if (fioCreate()>0) {LogEntry(FuncName,"FatFs volume and archive created.\n");}
            else if (FatFsDisable) {LogEntry(FuncName,"Abort creation, FatFs system disabled.\n");}
            else {LogEntry(FuncName,"FatFs system creation failed.\n");}
         }

         /* erase the FatFs volume */
         else if (FsClean>0)
         {
            /* close all files and recondition the RamFs volume */
            fcloseall(); fformat();

            /* recondition the FatFs volume */
            if (fioClean()>0) {LogEntry(FuncName,"All files in FatFs volume and archive erased.\n");}
            else if (FatFsDisable) {LogEntry(FuncName,"Abort cleaning, FatFs system disabled.\n");}
            else {LogEntry(FuncName,"FatFs reconditioning failed.\n");}
         }

         /* execute file retrieval from archive */
         else if (glob[0])
         {
            if (!FatFsDisable) 
            {
               LogEntry(FuncName,"Archive->FatFs glob: %s\n",glob);
               k=fmove(glob,FatArchive);
               LogEntry(FuncName,"Files moved from archive to FatFs: %d\n",k);
            }

            else {LogEntry(FuncName,"Abort archive operations, FatFs system disabled.\n");}
         }
      }
      
      /* warn that configuration file is invalid due to syntax, CRC, or validation errors */
      else {LogEntry(FuncName,"Configuration file invalid.\n");}
               
      /* check if recovery mode is enabled */
      if (RecoveryMode>0) 
      {
         time_t SeqTime=itimer(),alarm=ialarm();

         /* make a logentry */
         RecoveryInit(); LogEntry(FuncName,"Recovery mode activated. "
                                  "[itimer:%ld, alarm:%ld]\n",SeqTime,alarm);
               
         /* get the sequence time */ 
         SeqTime=itimer() + HeartBeat; 
             
         /* compute the next regularly scheduled alarm-time */
         alarm = SeqTime - SeqTime%cfg.TimeTelemetryRetry + cfg.TimeTelemetryRetry;
               
         /* set the alarm and go to sleep */
         PowerOff(alarm);
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to analyze the mission parameters for internal consistency    */
/*------------------------------------------------------------------------*/
/**
   This function is the mission enforcer that analyzes the mission configuration
   for internal consistency, prints warnings when problems are detected, and
   computes a status metric that can be used to determine if the mission
   should be allowed to proceed.  Two classes of violations are currently
   implemented; those that are fatal and those that are merely insane.
   Mission configurations that have fatal violations should not be allowed
   to proceed.  Mission configurations that are computed to be insane should
   proceed only after careful consideration of each warning.
*/
int ConfigSupervisor(struct MissionParameters *cfg)
{
   cc *FuncName="ConfigSupervisor()";
   int status=0;

   /* assertion-like macro to trap fatal violations of mission constraints */
   #define Fatal(why,expr) ((expr) ? 0 : fatal(#expr,why))

   /* assertion-like macro to trap violations of mission sanity checks */
   #define Warn(why,expr)  ((expr) ? 0 : warn(#expr,why))

   /* these are absolute range constraints that should be self explanatory */
   if (Fatal(NULL, inCRange(          0,  debuglevel,                          5))) status|=0x01;
   if (Fatal(NULL, inCRange(       2000,  cfg->ActivationPressure,          0))) status|=0x01;
   if (Fatal(NULL, inCRange(   1 * Hour,  cfg->ActivationPressurePeriod,    HeartBeat))) status|=0x01;
   if (Fatal(NULL, inCRange(        3832, cfg->MaxAirBladder,               1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056, cfg->OkVacuumCount,               1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonBuoyancyNudge,         1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonDeepProfilePosition,   1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonFullExtension,         1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonFullRetraction,        1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonInitialBuoyancyNudge,  1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonInitialDescentNudge,   1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonParkHyperRetraction,   0))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonParkPosition,          1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PistonPActivatePosition,     1))) status|=0x01;
   if (Fatal(NULL, inCRange(        4056,  cfg->PnpCycleLength,              1))) status|=0x01;
   if (Fatal(NULL, inCRange(       2000,  cfg->PressurePark,                0))) status|=0x01;
   if (Fatal(NULL, inCRange(       2050,  cfg->PressureProfile,             0))) status|=0x01;
   if (Fatal(NULL, inCRange(     8*Hour,  cfg->TimeDeepProfileDescent,      0))) status|=0x01;
   if (Fatal(NULL, inCRange(     30*Day,  cfg->TimeDown,                    0))) status|=0x01;
   if (Fatal(NULL, inCRange(    10*Hour,  cfg->TimeOutAscent,               0))) status|=0x01;
   if (Fatal(NULL, inCRange(     8*Hour,  cfg->TimeParkDescent,             0))) status|=0x01;
   if (Fatal(NULL, inCRange(     6*Hour,  cfg->TimePrelude,                 0))) status|=0x01;
#if defined (__arm__)
   if (Fatal(NULL, inCRange(WDogTimeOut, cfg->TimeTelemetryRetry,          0))) status|=0x01;
#endif
   if (Fatal(NULL, inCRange(    24*Hour, cfg->TimeUp,                      0))) status|=0x01;
   if (Fatal(NULL, inCRange(        300, cfg->ConnectTimeOut,             30))) status|=0x01;
   if (Fatal(NULL, inCRange(        600, cfg->ZModemTimeOut,              30))) status|=0x01;

   /****** Constraints to protect against potentially fatal pilot errors *********/

   float minVelocity = (float)cfg->EMAMinVelocity * 60.0 / 100.0; // convert to decibars/minute
   /* make sure there's enough uptime to do the deep profile and the telemetry */
   if (Fatal("The up-time must allow for a deep profile plus 2 hours for telemetry.",
             cfg->TimeUp >= cfg->PressureProfile / minVelocity)) status|=0x01;

   /* make sure there's enough uptime to do the park profile and the telemetry */
   if (Fatal("The up-time must allow for a park profile plus 2 hours for telemetry.",
             cfg->TimeUp >= cfg->PressurePark / minVelocity)) status|=0x01;

   /* the up-time has to be greater than the ascent time-out period */
   if (Fatal("The up-time has to be greater than the ascent time-out period.",
             cfg->TimeUp > cfg->TimeOutAscent)) status|=0x01;

   /* the down-time has to be greater than the park-descent time + deep-profile descent time */
   if (Fatal("The down-time has to be greater than the park-descent time plus deep-profile descent time.",
             cfg->TimeDown > cfg->TimeParkDescent + cfg->TimeDeepProfileDescent)) status|=0x01;

   /* make sure the profile pressure is not less than the park pressure */
   if (Fatal("The profile pressure must be greater than (or equal to) the park pressure.",
             cfg->PressureProfile >= cfg->PressurePark)) status|=0x01;

   /* validate the dial commands */
   if (Fatal("The primary dial command must begin with AT.",!strncmp(cfg->at, "AT",2))) status|=0x01;
   if (Fatal("The alternate dial command must begin with AT.",!strncmp(cfg->alt,"AT",2))) status|=0x01;

   /****** Sanity checks *********/

   /* recommend CP mode in upper layers */
   if (Warn("SBE41CP not designed for spot-sampling the main thermocline - CP mode recommended.",
            cfg->PressureCP >= 750)) status|=0x02;

   /* allow time for completion of deep profile */
   if (Warn("Ascent time insufficient for a deep profile.",
            cfg->TimeOutAscent >= cfg->PressureProfile / minVelocity)) status|=0x02;

   /* allow time for completion of park profile */
   if (Warn("Ascent time insufficient for a park profile.",
            cfg->TimeOutAscent >= cfg->PressurePark / minVelocity)) status|=0x02;

   /* allow time for descent to park depth */
   if (Warn("Park descent period incompatible with park pressure.",
            cfg->TimeParkDescent >= cfg->PressurePark / minVelocity)) status|=0x02;

   /* check for overly conservative park-descent period */
   if (Warn("Park descent period excessive.",
            cfg->TimeParkDescent <= 1.5 * cfg->PressurePark / minVelocity + 1 * Hour)) status|=0x02;

   /* allow time for descent to profile depth */
   if (Warn("Deep-profile descent period incompatible with profile pressure.",
            cfg->TimeDeepProfileDescent >= (cfg->PressureProfile - cfg->PressurePark) / minVelocity)) status|=0x02;

   /* check for overly conservative deep-profile-descent period */
   if (Warn("Deep-profile descent period excessive.",
            cfg->TimeDeepProfileDescent <= 1.5 * (cfg->PressureProfile - cfg->PressurePark) / minVelocity + 1 * Hour)) status|=0x02;

   /* check for sensible piston postions */
   if (Warn("Profile piston position incompatible with park piston position.",
            cfg->PistonDeepProfilePosition <= cfg->PistonParkPosition)) status|=0x02;

   /* check for N2 compressee */
   if (cfg->PistonParkHyperRetraction)
   {
      /* check for sensible piston postions */
      if (Warn("Compensator instability: N2 float should be parked deeper.",
               cfg->PressurePark >= MinN2ParkPressure)) status|=0x02;
   }

   /* check for sensible piston postions */
   if (Warn("Warning: Pressure-activation piston position exceeds default.  Expertise recommended.",
            DefaultMission.PistonPActivatePosition >= cfg->PistonPActivatePosition)) status|=0x02;

   /* verify that maximum air bladder pressure is in range [17.0,18.2] PSIA */
   if (Warn("Maximum air-bladder pressure seems insane.",
            inCRange(2312, cfg->MaxAirBladder, 2552))) status|=0x02;

   /* make sure the float id is valid */
   if (Warn("The float serial number should be greater than zero.",
             cfg->FloatId > 0)) status|=0x02;

   #undef Fatal
   #undef Warn

   if (status == 0)
   {
      LogEntry(FuncName,"Configuration accepted.\n");

      /* compute the signature of the mission program */
      cfg->crc = Crc16Bit((unsigned char *)(cfg),sizeof(*cfg)-sizeof(cfg->crc));
   }
   else {LogEntry(FuncName,"Configuration rejected.\n");}
   return status;
}

/*------------------------------------------------------------------------*/
/* convert a string to an unsigned 8-bit integer (with range limits)      */
/*------------------------------------------------------------------------*/
/**
   This function converts the initial portion of a string to an integer.

      \begin{verbatim}
      input:
         s....The string to be converted.

      output:   
         This function returns the value of the string up to the first
         nonconvertable character.  Range limits are applied so that
         negative values are truncated to zero and positive values greater
         than 255 are truncated to 255.
      \end{verbatim}
*/
static unsigned char atouc(const char *s)
{
   int i=atoi(s);

   /* apply range limits */
   if (i<0) i=0; else if (i>255) i=255;

   return ((unsigned char)i);
}

/*------------------------------------------------------------------------*/
/* function to notify the user of a mission sanity check violation        */
/*------------------------------------------------------------------------*/
/*
   This function is called by ConfigSupervisor() to notify the user of a
   mission sanity check violation.
*/
static int warn(const char *expr, const char *why)
{
   cc *FuncName="warn()";
   
   if (!expr) {LogEntry(FuncName,"NULL function argument.\n");}
   
   else
   {
      LogEntry("ConfigSupervisor()","Sanity check violated: %s\n",expr);

      if (why && *why) {LogEntry(FuncName,"  (%s)\n",why);}
   }
   
   return 1;
}

/*------------------------------------------------------------------------*/
/* function to notify the user of a fatal mission constraint violation    */
/*------------------------------------------------------------------------*/
/*
   This function is called by ConfigSupervisor() to notify the user of a fatal
   mission constraint violation.
*/
static int fatal(const char *expr, const char *why)
{
   cc *FuncName="warn()";
   
   if (!expr) {LogEntry(FuncName,"NULL function argument.\n");}
   else
   {
      LogEntry("ConfigSupervisor()","Constraint violated: %s\n",expr);

      if (why && *why) {LogEntry(FuncName,"  (%s)\n",why);}
   }
   
   return 1;
}

/*------------------------------------------------------------------------*/
/* function to write the configuration to the log stream                  */
/*------------------------------------------------------------------------*/
int LogConfiguration(const struct MissionParameters *config,const char *id)
{
   cc *FuncName = "LogConfiguration()";
   int status=-1;

   /* validate the MissionParameters pointer */
   if (!config)
   {
      /* log the message */
      LogEntry(FuncName,"NULL configuration.\n");
   }

   /* validate the configuration id */
   else if (!id) 
   {
      /* log the message */
      LogEntry(FuncName,"NULL configuration id.\n");
   }

   else
   {
      cc *indent="   ";
      
      /* write the configuration to the log file */
      LogEntry(FuncName,"%s configuration for Apf11 FwRev %08lx FwDate %s:\n",id,FW_REV,__DATE__);
      LogEntry(FuncName, indent); LogAdd("ActivationPressure(%0.1f) [dbar]\n", config->ActivationPressure);
      LogEntry(FuncName, indent); LogAdd("ActivationPressurePeriod(%ld) [sec]\n", config->ActivationPressurePeriod);
      LogEntry(FuncName,indent); LogAdd("AscentTimeOut(%ld) [min]\n",config->TimeOutAscent/Min);
      LogEntry(FuncName,indent); LogAdd("AtDialCmd(%s) [primary]\n",config->at);
      LogEntry(FuncName,indent); LogAdd("AltDialCmd(%s) [alternate]\n",config->alt);
      LogEntry(FuncName,indent); LogAdd("BuoyancyNudge(%u) [count]\n",config->PistonBuoyancyNudge);
      LogEntry(FuncName,indent); LogAdd("BuoyancyNudgeInitial(%u) [count]\n",config->PistonInitialBuoyancyNudge);
      LogEntry(FuncName,indent); LogAdd("DescentNudgeInitial(%u) [count]\n",config->PistonInitialDescentNudge);
      LogEntry(FuncName,indent); LogAdd("CompensatorHyperRetraction(%d) [count]\n",config->PistonParkHyperRetraction);
      LogEntry(FuncName,indent); LogAdd("ConnectTimeOut(%ld) [sec]\n",config->ConnectTimeOut);
      LogEntry(FuncName,indent); LogAdd("CpActivationP(%g) [dbar]\n",config->PressureCP);
      LogEntry(FuncName,indent); LogAdd("DeepProfileDescentTime(%ld) [min]\n",config->TimeDeepProfileDescent/Min);
      LogEntry(FuncName,indent); LogAdd("DeepProfilePistonPos(%d) [count]\n",config->PistonDeepProfilePosition);
      LogEntry(FuncName,indent); LogAdd("DeepProfilePressure(%g) [dbar]\n",config->PressureProfile);
      LogEntry(FuncName,indent); LogAdd("DownTime(%ld) [min]\n",config->TimeDown/Min);
      LogEntry(FuncName,indent); LogAdd("FatFsEnable(%d)\n",config->FatFsEnable);
      LogEntry(FuncName,indent); LogAdd("FloatId(%04u)\n",config->FloatId);
      LogEntry(FuncName,indent); LogAdd("FullExtension(%u) [count]\n",config->PistonFullExtension);
      LogEntry(FuncName,indent); LogAdd("FullRetraction(%u) [count]\n",config->PistonFullRetraction);
      LogEntry(FuncName,indent); LogAdd("MaxAirBladder(%u) [count]\n",config->MaxAirBladder);
      LogEntry(FuncName,indent); LogAdd("MaxLogKb(%ld) [KByte]\n",MaxLogSize/1024);
      LogEntry(FuncName,indent); LogAdd("MissionPrelude(%ld) [min]\n",config->TimePrelude/Min);
      LogEntry(FuncName,indent); LogAdd("OkVacuum(%u) [count]\n",config->OkVacuumCount);
      LogEntry(FuncName,indent); LogAdd("PActivationPistonPosition(%u) [count]\n",config->PistonPActivatePosition);
      LogEntry(FuncName,indent); LogAdd("ParkDescentTime(%ld) [min]\n",config->TimeParkDescent/Min);
      LogEntry(FuncName,indent); LogAdd("ParkPistonPos(%d) [count]\n",config->PistonParkPosition);
      LogEntry(FuncName,indent); LogAdd("ParkPressure(%g) [dbar]\n",config->PressurePark);
      LogEntry(FuncName,indent); LogAdd("PnPCycleLen(%d)\n",config->PnpCycleLength);
      LogEntry(FuncName,indent); LogAdd("Pwd(0x%04x)\n",
                                        Crc16Bit((const unsigned char *)config->pwd,strlen(config->pwd)));
      LogEntry(FuncName,indent); LogAdd("TelemetryRetry(%ld) [min]\n",config->TimeTelemetryRetry/Min);

      LogEntry(FuncName,indent); if (inRange(0,config->ToD,Day)) LogAdd("TimeOfDay(%ld) [min]\n",config->ToD/Min);
                                 else LogAdd("TimeOfDay(DISABLED) [min]\n");
      
      LogEntry(FuncName,indent); LogAdd("UpTime(%ld) [min]\n",config->TimeUp/Min);
      LogEntry(FuncName,indent); LogAdd("User(%s)\n",config->user);
      LogEntry(FuncName,indent); LogAdd("ZModemTimeOut(%ld) [sec]\n",config->ZModemTimeOut);
      LogEntry(FuncName,indent); LogAdd("Verbosity(%d)\n",debuglevel);
      LogEntry(FuncName,indent); LogAdd("DebugBits(0x%04x)\n",debugbits);
      LogEntry(FuncName,indent); LogAdd("EMASample(%u) [flag]\n", config->EMASample);
      LogEntry(FuncName,indent); LogAdd("EMAMinVelocity(%u) [cm/s]\n", config->EMAMinVelocity);

      /* reinitialize the function's return value */
      status=1;
   }
   
   return status;
}

#endif /* CONFIG_C */
