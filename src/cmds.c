#ifndef CMDS_H
#define CMDS_H

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define cmdsChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes for exported functions */
void CmdMode(void);
int  CmdModeRequest(void);
int  KbdEscRequest(void);

#endif /* CMDS_H */
#ifdef CMDS_C
#undef CMDS_C

#include <apf11.h>
#include <apf11ad.h>
#include <apf11com.h>
#include <apf11rf.h>
#include <assert.h>
#include <at25eeprom.h>
#include <chat.h>
#include <config.h>
#include <conio.h>
#include <control.h>
#include <crc16bit.h>
#include <ctdio.h>
#include <ctype.h>
#include <demodulate.h>
#include <diskio.h>
#include <ds2740.h>
#include <download.h>
#include <eeprom.h>
#include <ema.h>
#include <engine.h>
#include <errno.h>
#include <extract.h>
#include <fatio.h>
#include <garmin.h>
#include <gps.h>
#include <inrange.h>
#include <lbt9523.h>
#include <limits.h>
#include <logger.h>
#include <login.h>
#include <math.h>
#include <nmea.h>
#include <pactivate.h>
#include <pram.h>
#include <recovery.h>
#include <regex.h>
#include <sbe41cp.h>
#include <serial.h>
#include <stdio.h>
#include <stdlib.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Rtc.h>
#include <Stm32f103Reg.h>
#include <stream.h>
#include <string.h>
#include <telemetry.h>
#include <tmparse.h>
#include <upload.h>
#include <zconf.h>
#include <zlib.h>

const char trm[]="\n";
const char cr[]="\r";
const char ns[]="";

/* define some ascii characters special meaning */
#define LF   0x0a /* line feed */
#define CR   0x0d /* carriage return */
#define BKSP 0x08 /* backspace */
#define DEL  0x7f /* DEL */
#define ESC  0x1b /* escape character */

/* define the termination string for writes to the console serial port */
static cc *ctrm = "\n";

/* define the vertical ascent and descent rates (dbar/sec) */
float AscentRate = 0.08, DescentRate = 0.10; 

/* define a global switch to turn on/off the display of the password */
static int ShowPwd=0;

/* prototypes for functions with static linkage */
static void BuoyancyControlAgent(void);
static void ChargeConsumptionAgent(void);
static void cputs(cc *msg,cc *trm);
static void Diagnostics(void);
static void FatFsAgent(void);
static void FloatVitalsAgent(void);
static void GarminGps15Agent(void);
static void HostAgent(void);
static void ManufacturerDiagnosticsAgent(void);
static void MissionProgrammingAgent(void);
static void MissionTimingAgent(void);
static void ModemAgent(void);
static void EMAAgent(void);
static void ModemGateWay(void);
static void RegisterInspectionAgent(void);
static void Sbe41cpAgent(void);
static void Sbe41cpGateWay(void);
static void ShowParameters(void);

/* external variable declarations */
extern persistent time_t AlmanacTimeStamp;

/*------------------------------------------------------------------------*/
/* buoyancy control agent                                                 */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for configuring buoyancy
   related mission parameters. 
*/
static void BuoyancyControlAgent(void)
{
   /* define stack-based objects for local use */
   unsigned char key; long int val; char buf[80];

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";
   
   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the apf11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm); key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');
      
   switch (tolower(key))
   {
      case 'd':
      {
        pgets(&conio, buf, sizeof(buf) - 1, 60, cr);
        if (*buf && (val = atoi(buf)) >= 50 && val < 4072)
        {
            mission.PistonInitialDescentNudge = val;
            snprintf(buf, sizeof(buf), "Initial descent nudge: %d piston counts.", mission.PistonInitialDescentNudge);
            pputs(&conio, buf, 2, trm);
        }
        else
        {
            cputs(RangeError, ctrm);
        }

        break;
      }

      /* get the compressee hyper-retraction for park descent */
      case 'h':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=0 && val<4072)
         {
            mission.PistonParkHyperRetraction = val;
            snprintf(buf,sizeof(buf),"Compensator hyper-retraction: %d counts.",
                     mission.PistonParkHyperRetraction);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the initial buoyancy nudge of the piston */
      case 'i':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=5 && val<4072)
         {
            mission.PistonInitialBuoyancyNudge = val;
            snprintf(buf,sizeof(buf),"Buoyancy nudge for ascent initiation: "
                     "%d piston counts.",mission.PistonInitialBuoyancyNudge);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the piston position for deep profiles */
      case 'j':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            mission.PistonDeepProfilePosition = val;
            snprintf(buf,sizeof(buf),"Piston position for deep profiles: "
                     "%d counts.",mission.PistonDeepProfilePosition);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the buoyancy nudge of the piston during the profile */
      case 'n':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=5 && val<4072)
         {
            mission.PistonBuoyancyNudge = val;
            snprintf(buf,sizeof(buf),"Buoyancy nudge for ascent maintenance: "
                     "%d piston counts.",mission.PistonBuoyancyNudge);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the piston park position */
      case 'p':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            mission.PistonParkPosition = val;
            snprintf(buf,sizeof(buf),"Piston park position: %d counts.",
                     mission.PistonParkPosition);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of buoyancy control parameters.",                                ctrm);
         cputs("?  Print this menu.",                                                 ctrm);
         cputs("Bd Initial descent nudge. [392 - 4056] (counts)",                     ctrm);
         cputs("Bh Compensator hyper-retraction for park descent. [0-4056] (counts)", ctrm);
         cputs("Bi Ascent initiation buoyancy nudge. [192-4056] (piston counts)",     ctrm);
         cputs("Bj Deep-profile piston position. [1-4056] (counts)",                  ctrm);
         cputs("Bn Ascent maintenance buoyancy nudge. [32-4056] (piston counts)",     ctrm);
         cputs("Bp Park piston position. [1-4056] (counts)",                          ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* Exercise the charge consumption model agent                            */
/*------------------------------------------------------------------------*/
static void ChargeConsumptionAgent(void)
{
   unsigned char key; char buf[80];

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* adjust the number of Coulombs */
      case 'c':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* adjust the number of kilo-Coulombs */
         if (*buf) {cmodel.Coulombs=atof(buf)*1000;}

         break;
      }

      /* exercise the charge consumption model */
      case 'e':
      {
         /* measure the battery voltage */
         float charge,ocv=BatVolts(ADC);

         /* compute the charge consumed by the batteries */
         if (ChargeModelCompute(&charge)>0)
         {
            snprintf(buf,sizeof(buf),"Battery charge consumption: "
                     "%0.1fV  %0.1fkC",ocv,charge);
            pputs(&conio,buf,2,trm);
         }
         else {cputs("Charge consumption model computation failed.",ctrm);}

         break;
      }

      /* initialize the charge consumption model */
      case 'i':
      {
         cputs("Warning: The charge consumption model will be initialized to zeros.",ctrm);
         cputs("Type 'Y' to proceed or any other key to abort. ","");
         conio.ioflush();

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            ChargeModelInit();
            cputs("\nCharge consumption model has been initialized.",ctrm);
         }
         else cputs("\nAborted; current charge consumption model will continue.",ctrm);

         break;
      }

      /* report the accumulated charge parameters */
      case 'p':
      {
         /* measure the battery voltage */
         float ocv=BatVolts(ADC);

         /* report the accumulated charge parameters */
         snprintf(buf,sizeof(buf),"Charge model parameters: OCV:%0.1fV  "
                  "Wake-Cycles:%lu  Sleep:%0.1fdays  Charge:%0.1fkC",ocv,
                  cmodel.WakeCycles,(float)cmodel.StandbySec/SecPerDay,
                  cmodel.Coulombs/1000);
         pputs(&conio,buf,2,trm);

         break;
      }

      /* adjust the standby seconds */
      case 's':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* adjust the seconds spent in standby mode */
         if (*buf) {cmodel.StandbySec=strtoul(buf,NULL,10);}

         break;
      }

      /* adjust the number of wake cycles */
      case 'w':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* adjust the seconds spent in standby mode */
         if (*buf) {cmodel.WakeCycles=strtoul(buf,NULL,10);}

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu for charge consumption model.",                 ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("c  Adjust accumulated charge (kC).",                 ctrm);
         cputs("e  Exercise the charge consumption model.",          ctrm);
         cputs("i  Initialize the charge consumption model.",        ctrm);
         cputs("p  Report the charge consumption model parameters.", ctrm);
         cputs("s  Adjust seconds spent in standby mode.",           ctrm);
         cputs("w  Adjust the number of wake cycles.",               ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* function to implement command mode                                     */
/*------------------------------------------------------------------------*/
/**
   This function is the main control loop that implements the command mode
   for mission programming and float control.
*/
void CmdMode(void)
{
   int i,n,status=1; long int val;
   enum State state; char buf[150]; unsigned char key;
   const time_t TimeOut=300; time_t alarm,To;

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* show the firmware revision */
   snprintf(buf, 80, "Entering Command Mode [ApfId %05u, FwRev %08lx, FwDate %s]", mission.FloatId, FwRev, __DATE__);
   pputs(&conio,buf,2,trm);

   do
   {
      // record the current time to implement loop timeout
      To=time(NULL);

      /* flush the console's Rx buffer */
      ConioEnable(); conio.ioflush();

      /* write a prompt to stdout */
      cputs("> ","");

      /* wait for the next keyboard-hit */
      while ((key=kbdhit())<=0)
      {
         /* arrange for the APF11 to be put in hibernate mode */
         if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
         {
            cputs(" (timeout)",ctrm); key='q'; break;
         }
      }

      /* acknowledge key entry by spacing over */
      if (key!=CR) conio.putb(' ');

      switch (tolower(key))
      {
         /* initiate pressure-activation of mission */
         case 'a':
         {
            /* read the mission state from EEPROM */
            state = StateGet();

            /* check if the mission is presently inactive */
            if (state<=INACTIVE)
            {
               cputs("Initiating pressure-activation of mission.",ctrm);

               /* put the float into surface diagnostics mode */
               if (PActivateInit()>0)
               {
                  /* notify that the mission has been activated */
                  cputs("Pressure-activation of mission has been initiated.",ctrm);

                  /* set the float to exit the command loop */
                  SetAlarm(itimer()+7); status=0;
               }
               else cputs("Attempt to initiate pressure-activation failed.",ctrm);
            }

            /* notify the user that the mission was already active */
            else cputs("The mission is already active.",ctrm);

            break;
         }

         /* display the battery voltage and vacuum */
         case 'c':
         {
            /* measure the battery voltage, system current, and internal pressure */
            unsigned short Vad12 = BatVoltsAdc();
            unsigned short Iad16 = BatAmpsAdc();

            /* measure the internal pressure and pneumatic pressure */
            unsigned short Bad12 = BarometerAdc();
            unsigned short Pad12 = AirBladderPressureAdc();

            /* measure the humidity */
            unsigned short HumAd12 = HumidityAdc();

            /* create the calibration report */
            snprintf(buf,sizeof(buf),"Battery [%4dcnt, %4.1fV]  Current [%4dcnt, %4.1fmA]  "
                     "Barometer [%4ucnt, %5.1fpsi]  Pneumometer [%4dcnt, %5.1fpsi]  "
                     "Humidity [%3dcnt, %5.1f%%]",Vad12,BatVolts(Vad12),
                     Iad16,BatAmps(Iad16)*1000,Bad12,
                     Barometer(Bad12),Pad12,
                     AirBladderPressure(Pad12),HumAd12,Humidity(HumAd12));

            /* write the calibration report to the console */
            pputs(&conio,buf,2,trm);

            break;
         }

         /* set the logging verbosity */
         case 'd':
         {
            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            /* process the user-input */
            if (*buf && (val=strtoul(buf,NULL,16))>=0L && (val&VERBOSITYMASK)<=5L && val<=UINT_MAX)
            {
               /* assign the new verbosity level */
               debugbits=(unsigned int)val; mission.DebugBits=debugbits; LogPredicate(1);

               /* compute the signature of the mission program */
               mission.crc = Crc16Bit((unsigned char *)(&mission), sizeof(mission)-sizeof(mission.crc));

               /* write the new mission configuration to EEPROM */
               if (MissionParametersWrite(&mission)<=0)
               {
                  cputs("WARNING: Attempt to write mission parameters to EEPROM failed.",ctrm);
               }

               /* create a confirmation for the advanced user */
               if (debugbits&(~VERBOSITYMASK))
               {
                  /* create a confirmation for the user */
                  snprintf(buf,sizeof(buf),"Logging verbosity,bits: %u,0x%04x",
                            debuglevel,debugbits);
               }

               /* create a confirmation for the user */
               else {snprintf(buf,sizeof(buf),"Logging verbosity: %u",debuglevel);}

               /* write the calibration report to the console */
               pputs(&conio,buf,2,trm);
            }

            /* notify user of input error */
            else cputs(RangeError,ctrm);

            break;
         }

         /* execute the mission */
         case 'e':
         {
            /* read the mission state from EEPROM */
            state = StateGet();

            /* check if the mission is already active */
            if (state>INACTIVE && state<EOS) cputs("Useless mission activation request; "
                                                   "the mission is already active.",ctrm);

            /* put the float into surface diagnostics mode */
            else
            {
               cputs("Executing mission activation sequence.",ctrm);

               /* execute self-tests and launch mission */
               if (MissionLaunch()>0)
               {
                  /* notify that the mission has been activated */
                  cputs("Mission activated.",ctrm);

                  /* set the float to exit the command loop */
                  SetAlarm(itimer()+7); status=0;
               }

               /* notify the user that the activation failed */
               else cputs("Attempt to activate mission failed.",ctrm);
            }

            break;
         }

         /* exercise the Garmin GPS15 */
         case 'g': {GarminGps15Agent(); break;}

         /* exercise the Modem */
         case 'h': {ModemAgent(); break;}

         /* branch to diagnostics loop */
         case 'i': {Diagnostics(); break;}

         /* branch to the FatFs agent */
         case 'j': {FatFsAgent(); break;}

         /* kill the mission */
         case 'k':
         {
            /* read the mission state from EEPROM */
            state = StateGet();

            if (state==PACTIVATE)
            {
               /* notify that the mission was already inactive */
               cputs("Useless mission deactivation request; "
                     "the mission is already inactive.",ctrm);
            }

            /* check if the mission is presently active */
            else if (state>INACTIVE && state<EOS)
            {
               /* warn the user that terminating the mission makes the float non-deployable */
               cputs("",ctrm);
               cputs("WARNING: Mission deactivation request pending.","\n");
               cputs("         Type 'Y' to kill the mission or any other key to abort."," ");
               conio.ioflush();

               /* get confirmation that the deactivation request is intentional */
               if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
               {
                  /* warn that the float is not deployable until the mission is reactivated */
                  cputs("\nWARNING: Mission deactivated; Enabling pressure-activation monitor.",ctrm);

                  /* put the float mission into inactive mode and open the air valve */
                  MissionKill();
               }

               /* abort the deactivation request */
               else cputs("\nMission deactivation aborted; "
                          "the mission in progress will continue.",ctrm);
            }

            /* check for undefined state */
            else
            {
               cputs("Enabling pressure-activation monitor.",ctrm);

               /* put the float mission into inactive mode and open the air valve */
               MissionKill();
            }

            break;
         }

         /* list the mission parameters */
         case 'l':
         {
           /* get the current state of the mission */
            state = StateGet();

            if (state>INACTIVE && state<EOS) {cputs("Mission active - service unavailable.",ctrm);}

            else {cputs("",ctrm); ShowParameters();}

            break;
         }

         /* specify mission programming */
         case 'm':
         {
            /* get the current state of the mission */
            state = StateGet();

            if (state>INACTIVE && state<EOS) {cputs("Mission active - service unavailable.",ctrm);}

            else MissionProgrammingAgent();

            break;
         }

         /* display the float serial number */
         case 'n':
         {
            snprintf(buf,sizeof(buf),"Float serial number: %05u",mission.FloatId);
            pputs(&conio,buf,2,trm);
            break;
         }

         /* print the pressure table */
         case 'p':
         {
            cputs("Pressure table:",ctrm);

            /* loop through each element of the pressure table */
            for (n=0,i=0; i<pTableSize; i++,n++)
            {
               /* terminate the line */
               if (n>=10) {cputs("",ctrm); n=0;}

               /* write the pressure to a buffer */
               snprintf(buf,sizeof(buf)-1," %4.0f",pTable[i]);

               /* write the buffer to the console */
               pputs(&conio,buf,2,ns);
            }

            /* terminate the table */
            cputs("",ctrm);

            break;
         }

         /* key to exit command mode */
         case 'q':
         {
            /* get the current state of the mission */
            state = StateGet();

            /* resume p-activation monitoring */
            if (state==PACTIVATE) cputs("Resuming pressure-activation monitor.",ctrm);

            /* resume p-activation monitoring */
            else if (state==RECOVERY) cputs("Resuming recovery mode.",ctrm);

            /* float is in an active state - resume the mission */
            else if (state>INACTIVE && state<EOS) cputs("Resuming mission already in progress.",ctrm);

            /* enter p-activation monitoring */
            else if (state==INACTIVE)
            {
               cputs("WARNING: Inactive mission - entering pressure-activation monitor.",ctrm);

               /* enter pressure activation monitor */
               MissionKill();
            }

            /* float is in an undefined state - induce pressure activation mode */
            else
            {
               cputs("WARNING: Undefined mission - entering pressure activation mode.",ctrm);

               /* enter pressure activation monitor */
               PActivateInit();
            }

            status=0; break;
         }

         /* recovery mode */
         case 'r':
         {
            cputs("Activating recovery mode.",ctrm);

            /* activate recovery mode */
            fformat(); RecoveryInit();

            /* set the alarm for 8 seconds from now and exit command mode */
            SetAlarm(itimer()+8); status=0; break;
         }

         /* exercise the SBE41 */
         case 's': {Sbe41cpAgent(); break;}

         /* get/set the time */
         case 't':
         {
            /* get user input from the console */
            if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0)
            {
               /* process the user-input */
               if (*buf && ParseTime(buf,&val)>0)
               {
                  /* set the Apf11's RTC */
                  if (RtcSet(val)<=0) cputs("Attempt to set RTC failed.",ctrm);
               }

               /* get the current time */
               val=time(NULL);

               /* construct the response to the user */
               snprintf(buf,sizeof(buf),"Real time clock: %s",ctime(&val));

               /* write the response to the console */
               pputs(&conio,buf,2,ns);
            }

            break;
         }

         /* open a logfile */
         case 'u':
         {
            cputs("Enter log file name: ","");

            /* get user input from the console */
            if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0)
            {
               /* verify that the filename */
               if (fnameok(buf)>0) {LogOpen(buf,'a');}
               else cputs("Invalid file name.",ctrm);
            }

            break;
         }

         /* close the logfile */
         case 'v': {LogClose(); cputs("Log file closed.",ctrm); break;}

         // start the ema agent
         case 'z': {
            EMAAgent();
            break;
         }

         /* print the menu */
         case '?': cputs("",ctrm);

         /* the default response is to print the menu */
         default:
         {
            cputs("Menu selections are not case sensitive.",             ctrm);
            cputs("?  Print this help menu.",                            ctrm);
            cputs("A  Initiate pressure-activation of mission.",         ctrm);
            cputs("C  Calibrate: battery volts, current, & vacuum.",     ctrm);
            cputs("D  Set logging verbosity. [0-5]",                     ctrm);
            cputs("E  Execute (activate) mission.",                      ctrm);
            cputs("G  GPS module agent.",                                ctrm);
            cputs("G? GPS module menu.",                                 ctrm);
            cputs("H  LBT module agent.",                                ctrm);
            cputs("H? LBT module menu.",                                 ctrm);
            cputs("I  Diagnostics agent.",                               ctrm);
            cputs("I? Diagnostics menu.",                                ctrm);
            cputs("J  FatFs agent.",                                     ctrm);
            cputs("J? FatFs agent menu.",                                ctrm);
            cputs("K  Kill (deactivate) mission.",                       ctrm);
            cputs("L  List mission parameters.",                         ctrm);
            cputs("M  Mission programming agent.",                       ctrm);
            cputs("M? Mission programming menu.",                        ctrm);
            cputs("N  Display float serial number.",                     ctrm);
            cputs("P  Display the pressure table.",                      ctrm);
            cputs("Q  Exit command mode.",                               ctrm);
            cputs("R  Activate recovery mode.",                          ctrm);
            cputs("S  Sbe41cp CTD agent.",                               ctrm);
            cputs("S? Sbe41cp CTD agent menu.",                          ctrm);
            cputs("T  Get/Set RTC time. (format 'mm/dd/yyyy:hh:mm:ss')", ctrm);
            cputs("U  Attach the logstream to a specified file.",        ctrm);
            cputs("V  Close the log file.",                              ctrm);
            cputs("Z  EMA agent.",                                       ctrm);
            cputs("Z? EMA agent menu.",                                  ctrm);
         }
      }
   }
   while (status>0);

   /* make sure the alarm-time is at least 7 seconds in the future */
   alarm=((ialarm()-itimer())<7) ? (itimer()+7) : ialarm();

   /* power down until the previously set alarm expires */
   PowerOff(alarm);
}

/*------------------------------------------------------------------------*/
/* function to check for user request to enter command mode               */
/*------------------------------------------------------------------------*/
/**
   This function checks for a user-request to enter command mode.  Three
   test criteria are applied.  The first is that the magnetic reset switch
   has not been toggled.  The second is that the 20mA current loop is active
   which indicates that a computer is attached to the controller.  The third
   is that the APF11 was awakened asynchronously (ie., not by the RTC alarm).
   If all three criteria are satisfied then this function returns a positive
   number to indicate that command mode should be executed.  If any of the
   criteria are violated then this function returns zero.
*/
int CmdModeRequest(void)
{
   return (!MagSwitchToggled() && ConioActive()>0 && !Apf11WakeUpByRtc()) ? 1 : 0;
}

/*------------------------------------------------------------------------*/
/* function to write a string to the console port                         */
/*------------------------------------------------------------------------*/
static void cputs(cc *msg,cc *trm)
{
   char ch;

   if (msg)
   {
      while ((ch=(*msg++))) {putch(ch);}

      if (trm) while ((ch=(*trm++))) {putch(ch);}
   }
}

/*------------------------------------------------------------------------*/
/* diagnostics interface                                                  */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for executing diagnostic
   functions of APEX floats.
*/
static void Diagnostics(void)
{
   unsigned char key; long int val; char buf[128];

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* run the air pump for a few seconds */
      case 'a':
      {
        unsigned short V;
        unsigned short A;

         /* notify the user that the air pump will run for 6 seconds */
         cputs("Running air pump for 6 seconds.",ctrm);

         /* run the air pump for 6 seconds */
         AirPumpRun(6,&V,&A);

         /* construct response string */
         snprintf(buf,sizeof(buf),"Battery: [%4dcnt, %4.1fV]  "
                  "Current: [%4ucnt, %5.0fmA]", V,
                  BatVolts(V), A, AirPumpAmps(A) * 1000);

         /* write the voltage and current to the console */
         pputs(&conio,buf,2,trm);

         break;
      }

      /* move to the pressure activation piston position */
      case 'b': {PistonMoveAbs(mission.PistonPActivatePosition); break;}

      /* close the air valve */
      case 'c':
      {
         AirValveClose();
         cputs("Air valve closed.",ctrm);
         break;
      }

      /* display the piston position */
      case 'd':
      {
         snprintf(buf,sizeof(buf),"Piston position: %u counts (%0.1fml)",
                  PistonPositionAdc(),PistonVolume(ADC));
         pputs(&conio,buf,2,trm);
         break;
      }

      /* extend the piston by 56 counts */
      case 'e':
      {
         cputs("Extending piston 56 counts.",ctrm);
         PistonMoveRel(56);
         break;
      }

      /* freeze the float into inactive mode */
      case 'f':
      {
         /* warn the user that terminating the mission makes the float non-deployable */
         cputs("",ctrm);
         cputs("WARNING: Freezing the float renders it nondeployable",ctrm);
         cputs("         until the mission is reactivated.  Freeze anyway?","\n\n");
         cputs("         Type 'Y' to freeze the float or any other key to abort."," ");
         conio.ioflush();

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* warn that the float is not deployable until the mission is reactivated */
            cputs("\nWARNING: The float is frozen and is not deployable",ctrm);
            cputs("             until the mission is reactivated.",ctrm);

            /* put the float mission into inactive mode and open the air valve */
            StateSet(INACTIVE);

            /* power down */
            PowerOff(itimer()+10);
         }

         /* abort the deactivation request */
         else cputs("\nFreeze aborted; the mission in progress will continue.",ctrm);

         break;
      }

      /* goto a user specified piston position */
      case 'g':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* process the user-input and move the piston */
         if (*buf && (val=atoi(buf))>=1 && val<=4056)
         {
            snprintf(buf,sizeof(buf),"Seeking piston position: %ld",val);
            pputs(&conio,buf,2,trm);
            PistonMoveAbs(val);
         }

         /* notify user of input error */
         else cputs(RangeError,ctrm);

         break;
      }

      case 'h':
      {
         /* measure the internal humidity using the Apf11 */
         unsigned short HumAdc = HumidityAdc();

         /* create the report */
         snprintf(buf,sizeof(buf),"Internal humidity is %0.1f%% (Adc:%hu counts).",
                  Humidity(HumAdc),HumAdc);
         pputs(&conio,buf,2,trm);
         break;
      }

      /* toggle diagnostic LEDs */
      case 'i':
      {
         /* create an object to record the state of the LEDs */
         static unsigned char led=0;

         /* toggle the state of the diagnostic LEDs */
         if (!led) {cputs("Enabling Apf11 diagnostic LEDs.",ctrm); LedEnable(); led=1;}
         else {cputs("Disabling Apf11 diagnostic LEDs.",ctrm); LedDisable(); led=0;}

         break;
      }

      /* measure the power consumption by the high pressure buoyancy pump */
      case 'j':
      {
         /* define local work objects needed to measure power consumption */
         unsigned short VCnt=0xfff,ACnt=0xfff; float Volts,Amps,Watts;

         /* measure the current piston position */
         const unsigned short PistonMidPoint = ((mission.PistonFullExtension + mission.PistonFullRetraction) / 2);

         /* decide whether to extend or retract the piston to avoid crashes */
         if (PistonPositionAdc() < PistonMidPoint) {HDrive(1);} else {HDrive(-1);}

         /* pause, measure power consumption, and brake the pump */
         Wait(3000); VCnt=BatVoltsAdc(); ACnt=HydraulicPumpAmpsAdc(); HDrive(0);

         /* compute the voltage from the volt-counts */
         Volts = (VCnt!=0xfff) ? BatVolts(VCnt) : 0;

         /* compute the current from the amp-counts */
         Amps  = (ACnt!=0xfff) ? HydraulicPumpAmps(ACnt)  : 0;

         /* compute the power consumption */
         Watts = Volts*Amps;

         /* create the response string */
         snprintf(buf,sizeof(buf),"Buoyancy pump: Potential(%hdCnt,%0.1fVolts) "
                  "Current(%huCnt,%0.3fAmps) Power(%0.2fWatts)",VCnt,
                  Volts,ACnt,Amps,Watts);

         /* write the response string to the console */
         pputs(&conio,buf,2,trm);

         break;
      }

      /* Initiate the Apf11's SOS */
      case 'k':
      {
         cputs("Initiating acoustic Apf11 SOS signal...",ns);
         BuzzerSos(); BuzzerSos(); BuzzerSos();
         cputs("done.",ctrm);

         break;
      }

      /* adjust the maximum log file size */
      case 'l':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* process the user-input and move the piston */
         if (*buf && (val=atoi(buf))>=5 && (val*KB)<=(BUFSIZE-(KB/4)))
         {
            MaxLogSize=(val*KB);
            snprintf(buf,sizeof(buf),"Maximum engineering log "
                     "size is %ld kilobytes.",MaxLogSize/KB);
            pputs(&conio,buf,2,trm);
         }

         /* notify user of input error */
         else cputs(RangeError,ctrm);

         break;
      }

      /* reset the magnetic switch latch circuit */
      case 'm':
      {
         MagSwitchReset();
         cputs("Magnetic switch latch circuit has been reset.",ctrm);
         break;
      }

      /* reset the watch-dog latch circuit */
      case 'n':
      {
         WatchDogLatchReset();
         cputs("Watch dog latch circuit has been reset.",ctrm);
         break;
      }

      /* open the air valve */
      case 'o':
      {
         AirValveOpen();
         cputs("Air valve opened.",ctrm);
         break;
      }

      /* exercise the charge consumption agent */
      case 'q': {ChargeConsumptionAgent(); break;}

      /* retract the piston by 56 counts */
      case 'r':
      {
         cputs("Retracting piston 56 counts.",ctrm);
         PistonMoveRel(-56);
         break;
      }

      /* execute the SelfTest() */
      case 's':
      {
         cputs("Executing SelfTest().",ctrm);
         if (SelfTest(true /* testSky */) > 0)
         {
            cputs("SelfTest() passed.", ctrm);
         }
         else
         {
            cputs("SelfTest() failed.",ctrm);
         }
         break;
      }

      /* exercise ToD computations */
      case 't':
      {
         /* get user input from the console */
         if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0)
         {
            /* process the user-input */
            if (*buf && ParseTime(buf,&val)>0)
            {
               /* construct the response to the user */
               snprintf(buf,sizeof(buf),"Start of profile cycle: %s",ctime(&val));

               /* write the response to the console */
               pputs(&conio,buf,2,ns);

               /* compute the down-time for ToD expiration */
               val += ToD(val,mission.TimeDown,mission.ToD);

               /* construct the response to the user */
               snprintf(buf,sizeof(buf),"User-specified: TimeDown(%ldMin) ToD(%ldMin)\n",
                         mission.TimeDown/Min,mission.ToD/Min);

               /* write the response to the console */
               pputs(&conio,buf,2,ns);

               /* construct the response to the user */
               snprintf(buf,sizeof(buf),"End of down-time: %s",ctime(&val));

               /* write the response to the console */
               pputs(&conio,buf,2,ns);
            }
            else cputs("Enter start of profile cycle: mm/dd/yyyy:hh:mm:ss",ctrm);
         }

         break;
      }

      case 'u':
      {
         cputs("Executing SelfTest() with no-sky.",ctrm);
         if (SelfTest(false /* testSky */) > 0)
         {
            cputs("SelfTest() passed.", ctrm);
         }
         else
         {
            cputs("SelfTest() failed.",ctrm);
         }
         break;
      }

      /* inflate the air bladder */
      case 'z':
      {
         cputs("Activating pneumatic system to inflate bladder.",ctrm);

         /* activate the pneumatic inflation system */
         AirSystem();

         break;
      }

      /* enter protected diagnostics mode */
      case '*': {ManufacturerDiagnosticsAgent(); break;}

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of diagnostics.",                               ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("a  Run air pump for 6 seconds.",                     ctrm);
         cputs("b  Move piston to the pressure activation position.",ctrm);
         cputs("c  Close air valve.",                                ctrm);
         cputs("d  Display piston position",                         ctrm);
         cputs("e  Extend the piston 4 counts.",                     ctrm);
         cputs("f  Freeze into inactive mode.",                      ctrm);
         cputs("g  Goto a specified position. [1-254] (counts)",     ctrm);
         cputs("h  Measure internal humidity.",                      ctrm);
         cputs("i  Enable/disable Apf11 LEDs.",                      ctrm);
         cputs("j  Measure buyancy pump power consumption.",         ctrm);
         cputs("k  Initiate acoustic Apf11 SOS signal.",             ctrm);
         cputs("l  Set maximum engineering log size. [5-63] (KB)",   ctrm);
         cputs("m  Reset the magnetic switch latch circuit.",        ctrm);
         cputs("n  Reset the watch dog latch circuit.",              ctrm);
         cputs("o  Open air valve.",                                 ctrm);
         cputs("q  Charge consumption agent.",                       ctrm);
         cputs("q? Charge consumption menu.",                        ctrm);
         cputs("r  Retract the piston 4 counts.",                    ctrm);
         cputs("s  Execute the SelfTest().",                         ctrm);
         cputs("t  Calculate ToD down-time expiration.",             ctrm);
         cputs("u  Execute the SelfTest() with no-sky.",             ctrm);
         cputs("z  Activate pneumatic inflation system.",            ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* agent to manage the FatFs volume                                       */
/*------------------------------------------------------------------------*/
static void FatFsAgent(void)
{
   /* define the logging signature */
   cc *FuncName="FatFsAgent()";

   unsigned char key;
   int i, n;
   char buf[225];

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the Apf11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* close all open files */
      case 'a':
      {
         cputs("Closing all open files.",ctrm);
         fcloseall(); break;
      }

      /* Print biography of the FatFs volume */
      case 'b':
      {
         if (fat_mount())
         {
            /* define stack-based objects needed for FatFs metadata */
            DRESULT dresult; DWORD size,block; WORD sector;

            /* query the SD card for FatFs metadata */
            if ((dresult=disk_ioctl(0,GET_SECTOR_COUNT,&size))==RES_OK  &&
                (dresult=disk_ioctl(0,GET_SECTOR_SIZE,&sector))==RES_OK &&
                (dresult=disk_ioctl(0,GET_BLOCK_SIZE,&block))==RES_OK)
            {
               /* write biographical parameters of FatFs */
               snprintf(
                  buf,
                  sizeof(buf),
                  "FatFs biography: version(%u) size(%luMB) sector-size(%uB) block-size(%luB)",
                  FF_DEFINED,
                  size / 2048,
                  sector,
                  block);
               cputs(buf, ctrm);
            }
            else
            {
               snprintf(
                  buf,
                  sizeof(buf),
                  "Panic: FatFs biography failed. [FatErr=%d]\n",
                  dresult);
               cputs(buf, ctrm);
            }
         }
         else {cputs("FatFs mount failure.",ctrm);}

         fat_umount(); break;
      }

      /* create a new FatFs volume */
      case 'c':
      {
          cputs("WARNING: All data in RamFs/FatFs/Archive volumes "
                "will be destroyed.  Proceed? [y/n] ","");

         /* get confirmation that the request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* close all open files */
            cputs("",ctrm); fcloseall();

            /* create the FatFs file system */
            if (fioCreate()<=0) {cputs("Panic: FatFs creation failed.",ctrm);}
         }
         else {cputs("",ctrm); cputs("FaFs creation safely aborted.",ctrm);}

         break;
      }

      /* clean the FatFs volume */
      case 'd':
      {
          cputs("WARNING: All data in RamFs/FatFs/Archive volumes "
                "will be destroyed.  Proceed? [y/n] ","");

         /* get confirmation that the request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* close all open files and inactivate the mission */
            cputs("",ctrm); fcloseall(); fformat();

            /* create the FatFs file system */
            if (fioClean()<=0) {cputs("Panic: FatFs cleaning failed.",ctrm);}
         }
         else {cputs("",ctrm); cputs("Erasure safely aborted for RamFs/"
                                     "FatFs/Archive volumes.\n",ctrm);}

         break;
      }

      /* directory listing of the RamFs/FatFs/Archive volumes */
      case 'l':
      {
         /* define stack-based objects for temporary use */
         FILINFO finfo;
         DIR dj;
         struct tm modtime = {0, 0, 0, 1, 0, 70, 0, 0, 0};

         /* initialize the modification date for files in the RamFs volume */
         char *date = buf + sizeof(buf) / 2;
         strftime(date, sizeof(buf) / 2, "%F %T", &modtime);

         cputs("Directory listing of RamFs/FatFs/Archive volumes:", ctrm);
         for (n = 0, i = 0; i < MAXFILES; i++)
         {
            /* define a buffer to contain a file name */
            char fname[FILENAME_MAX + 1];

            /* read the name associated with the current file descriptor */
            if (fioName(i, fname, sizeof(fname)) > 0)
            {
               /* construct the directory listing */
               snprintf(
                  buf,
                  sizeof(buf),
                  "     RamFs: %5u %s %s",
                  fioLen(i),
                  date,
                  fname);
               pputs(&conio, buf, 2, trm);
               n++;
            }
         }

         /* mount the FatFs volume */
         if (fat_mount())
         {
            /* loop over all files in the root direcotry of the FatFs volume */
            if (f_findfirst(&dj, &finfo, "", "*.*") == FR_OK && finfo.fname[0]) do
            {
               /* construct the modification date/time string */
               strftime(date, sizeof(buf) / 2, "%F %T", ftime(&finfo, &modtime));

               /* construct the directory listing */
               snprintf(
                  buf,
                  sizeof(buf),
                  "     FatFs: %5lu %s %s",
                  finfo.fsize,
                  date,
                  finfo.fname);
               pputs(&conio, buf, 2, trm);
               n++;
            }

            /* find the next matchine file name */
            while (f_findnext(&dj, &finfo) == FR_OK && finfo.fname[0]);

            /* close the directory */
            f_closedir(&dj);

            /* loop over all files in the archive direcotry of the FatFs volume */
            if (f_findfirst(&dj, &finfo, arcdir, "*.*") == FR_OK && finfo.fname[0]) do
            {
               /* construct the modification date/time string */
               strftime(date, sizeof(buf) / 2, "%F %T", ftime(&finfo, &modtime));

               /* construct the directory listing */
               snprintf(
                  buf,
                  sizeof(buf),
                  "   Archive: %5lu %s %s",
                  finfo.fsize,
                  date,
                  finfo.fname);
                pputs(&conio, buf, 2, trm);
                n++;
            }

            /* find the next matchine file name */
            while (f_findnext(&dj, &finfo) == FR_OK && finfo.fname[0]);

            /* close the directory */
            f_closedir(&dj);
         }

         /* unmount the FatFs volume */
         fat_umount();

         /* write the total number of files in the directory listing */
         snprintf(buf, sizeof(buf), "Total files: %d", n);
         pputs(&conio, buf, 2, trm);

         break;
      }

      /* move files from the archive to the root directory */
      case 'm':
      {
         /*  query for the glob pattern */
         cputs("(Archive->FatFs) Enter glob: ","");

         /* get user input from the console */
         if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0 && buf[0])
         {
            if ((n=fmove(buf,FatArchive))>=0)
            {
               snprintf(buf,sizeof(buf),"Files moved from %s: %d",arcdir,n);
               cputs(buf,ctrm);
            }
         }

         break;
      }

      /* move files from the root directory to the archive */
      case 'n':
      {
         /*  query for the glob pattern */
         cputs("(FatFs->Archive) Enter glob: ","");

         /* get user input from the console */
         if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0 && buf[0])
         {
            if ((n=fmove(buf,FatRoot))>=0)
            {
               snprintf(buf,sizeof(buf),"Files moved to %s: %d",arcdir,n);
               cputs(buf,ctrm);
            }
         }

         break;
      }

      /* copy a FatFs file to the console port */
      case 'p':
      {
         /* define a buffer to contain the globbing pattern */
         char globbuf[FILENAME_MAX+1];

         /*  query for the file name */
         cputs("(FatFs->Console) Enter glob: ","");

         /* get user input from the console */
         if (pgets(&conio,globbuf,sizeof(globbuf)-1,60,cr)>=0 && globbuf[0])
         {
            /* define static objects to support repeated calls */
            cc *fname,*glob; FRESULT fstatus; FIL fp; UINT nb;

            /* close all buffered streams (ie., FILE objects) */
            fclose_streams();

            /* loop over all files that match the globbing pattern */
            if (fat_mount()) for (glob=globbuf; (fname=fglob(glob,NULL)); glob=NULL)
            {
               /* open the next file that matches the glob */
               if ((fstatus=f_open(&fp,fname,FA_READ|FA_OPEN_EXISTING))==FR_OK)
               {
                  snprintf(buf,sizeof(buf),"\nFatFs->Console [%luB]: %s",
                           f_size(&fp),fname); cputs(buf,ctrm);

                  /* read the next buffer-length from the file */
                  do if ((fstatus=f_read(&fp,buf,sizeof(buf),&nb))==FR_OK && nb>0)
                  {
                     /* write the buffer to the console port */
                     fwrite(buf,1,nb,stdout);
                  }

                  /* check for end of file */
                  while (nb>0 && !f_eof(&fp));
               }

               /* log the failed attempt to open the file */
               else {LogEntry(FuncName,"Open failed: %s [FatErr=%d]\n",fname,fstatus);}

               /* close the file */
               f_close(&fp);
            }

            /* unmount the FatFs volume */
            fat_umount();
         }

         break;
      }

      /* print RamFs file to serial port */
      case 'q':
      {
         /*  query for the file name */
         cputs("(RamFs->Console) Enter file name: ","");

         /* get user input from the console */
         if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0 && buf[0])
         {
            int fdes; unsigned short int n;

            if ((fdes=fioFindRamFs(buf))>=0 && fdes<NFIO)
            {
               snprintf(buf,sizeof(buf),"\nRamFs->Console [%uB]: %s",
                        fioblk_[fdes].len,fioblk_[fdes].fname); cputs(buf,ctrm);

               for (n=0; n<fioblk_[fdes].len && n<FILESIZE; n++)
               {
                  fputc(fioblk_[fdes].buf[n],stdout);
               }
            }
            else {cputs("File not found.",ctrm);}
         }

         break;
      }

      /* report state of the FatFs file system */
      case 's':
      {
         /* report whether the FatFs file system is enabled or disabled */
         snprintf(buf,sizeof(buf),"FatFs file system is %s.",
                  (FatFsDisable)?"disabled":"enabled"); cputs(buf,ctrm);

         break;
      }

      /* delete a file */
      case 'x':
      {
         /* query for the file name */
         cputs("Enter file name to delete from RamFs/FatFs volume: ","");

         /* get user input from the console */
         if (pgets(&conio,buf,sizeof(buf)-1,60,cr)>=0 && buf[0])
         {
            /* error handler */
            if (remove(buf)<0)
            {
               switch (errno)
               {
                  case ENXIO:  {cputs("Delete failed: Invalid file name.",ctrm);   break;}
                  case ENOENT: {cputs("Delete failed: File does not exist.",ctrm); break;}
                  case EBUSY:  {cputs("Delete failed: File is open.",ctrm);        break;}
                  default:
                  {
                     snprintf(buf,sizeof(buf),"Delete failed: Uncaught exception. [errno=%d]",errno);
                     pputs(&conio,buf,2,trm);
                  }
               }
            }
         }
         else cputs("Missing file name.",ctrm);

         break;
      }

      /* Zip the files */
      case 'z':
      {
         if (PrepareUpload(2) == 0)
         {
            cputs("Failed to zip all files properly.", ctrm);
         }
         else
         {
            cputs("Zipped all the files in the FS.", ctrm);
         }

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of FatFs file system functions.",                 ctrm);
         cputs("?  Print this menu.",                                  ctrm);
         cputs("Ja Close all open files.",                             ctrm);
         cputs("Jb Biography of FatFs volume.",                        ctrm);
         cputs("Jc Create new RamFs/FatFs file sytems (destructive).", ctrm);
         cputs("Jd Clean RamFs/FatFs/Archive volumes (destructive).",  ctrm);
         cputs("Jl Directory listing of RamFs/FatFs/Archive volumes.", ctrm);
         cputs("Jm Move files from Archive to FatFs volume.",          ctrm);
         cputs("Jn Move files from FatFs volume to Archive.",          ctrm);
         cputs("Jp Print FatFs file(s) to console.",                   ctrm);
         cputs("Jq Print RamFs file to console.",                      ctrm);
         cputs("Js Report if FatFs system is enabled or disabled.",    ctrm);
         cputs("Jx Delete file from the RamFs/FatFs volume.",          ctrm);
         cputs("Jz Zip the current crop of files and archive them.",   ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* float vitals agent                                                     */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for configuring mission
   parameters related to float personality.
*/
static void FloatVitalsAgent(void)
{
   unsigned char key; char buf[80];
   long int val; float p;

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* get the activation pressure */
      case 'a':
      {
         pgets(&conio, buf, sizeof(buf) - 1, 60, cr);
         if (*buf && (p = atof(buf)) > 0 && p < 2000)
         {
            mission.ActivationPressure = p;
            snprintf(buf, sizeof(buf), "Activation pressure (%0.2f) dbars", p);
            pputs(&conio, buf, 2, trm);
         }
         else
         {
            cputs(RangeError, ctrm);
         }

         break;
      }

      /* get the maximum air-bladder pressure */
      case 'b':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            /* compute pneumatic pressure inside air bladder */
            p = AirBladderPressure(val);

            /* assign the user-input to the mission parameters */
            mission.MaxAirBladder = val;

            /* create feedback string for user */
            snprintf(buf,sizeof(buf),"Full-bladder detection criteria: "
                     "[%4d counts, %1.1f PSI]",mission.MaxAirBladder,p);

            /* write the feedback string to the console */
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the piston full-extension position */
      case 'f':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            mission.PistonFullExtension = val;
            snprintf(buf,sizeof(buf),"Piston full-extension position: %d counts.",
                     mission.PistonFullExtension);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* display the float serial number */
      case 'n':
      {
         snprintf(buf,sizeof(buf),"Float serial number: %05u",mission.FloatId);
         pputs(&conio,buf,2,trm);
         break;
      }

      /* get the sampling period for the pressure activation phase */
      case 'p':
      {
         pgets(&conio, buf, sizeof(buf) - 1, 60, cr);
         if (*buf && (val = atoi(buf)) > 0 && val < 1 * Hour)
         {
            mission.ActivationPressurePeriod = val;
            snprintf(buf, sizeof(buf), "Activation Pressure Period: (%ld) seconds.", val);
            pputs(&conio, buf, 2, trm);
         }
         else
         {
            cputs(RangeError, ctrm);
         }

         break;
      }

      /* get the piston position for pressure activation */
      case 's':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            mission.PistonPActivatePosition = val;
            snprintf(buf,sizeof(buf),"Piston position during pressure-activation mode: %d counts.",
                     mission.PistonPActivatePosition);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the OK vacuum count */
      case 'v':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>0 && val<4072)
         {
            /* compute barometric pressure inside float */
            p = Barometer(val);

            /* assign the user-input to the mission parameters */
            mission.OkVacuumCount = val;

            /* create feedback string for user */
            snprintf(buf,sizeof(buf),"Mission aborted if internal "
                     "pressure at reset exceeds %d counts (%1.1f \"Hg).",
                     mission.OkVacuumCount,p);

            /* write the feedback string to the console */
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of float parameters.",                          ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("Fa Activation pressure.          [0-2000) (dbars)",  ctrm);
         cputs("Fb Maximum air-bladder pressure. [1-4056] (counts)", ctrm);
         cputs("Ff Piston full extension. [1-4056] (counts)",        ctrm);
         cputs("Fn Display float serial number.",                    ctrm);
         cputs("Fp Activation pressure period    [0, 3600) (secs)",  ctrm);
         cputs("Fs P-Activation piston position. [1-4056] (counts)", ctrm);
         cputs("Fv OK vacuum threshold. [1-4056] (counts)",          ctrm);
      }
   }
}

#define EFR_MAX_LINE 150
#define EFR_COL_COUNT 27

/*------------------------------------------------------------------------*/
/* agent to exercise the EMA board (cribbed from garmin agent)            */
/*------------------------------------------------------------------------*/
static void EMAAgent(void)
{
   unsigned char key;
   char buf[80];

   /* define the current time and the time-out period */
   const time_t To = time(NULL);
   const time_t TimeOut = 60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while((key = kbdhit()) <= 0) {

      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || (difftime(time(NULL), To) > TimeOut)) {
         cputs(" (timeout)", ctrm);
         key = CR;
         break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key != CR) {
      conio.putb(' ');
   }

   switch(tolower(key)) {

      // print the mission state
      case 'a': {
         cputs("", ctrm);

         enum State state = StateGet();

         // buffer for the state as a string
         char statestr[10];
         StateGetString(state, statestr, sizeof(statestr));

         snprintf(buf, sizeof(buf), "Current mission state [%d]: %s", state, statestr);
         pputs(&conio, buf, 2, trm);

         break;
      }

      // print the simulation status
      case 'b': {
         cputs("", ctrm);
         cputs("No simulation flags:", ctrm);
         break;
      }

      // capture compass calibration frame
      case 'c': {
         cputs("", ctrm);

         EMACompassCalibration calib;
         int emacode = EMAGetCompassCalibration(&calib);
         if(emacode <= 0) {
            snprintf(buf, sizeof(buf), "EMA compass calib fail; return code: %d", emacode);
            pputs(&conio, buf, 2, trm);

         } else {
            // need a bigger buffer for ema
            char emabuf[2048];
            EMAPrintCompassCalibrationToString(&calib, emabuf, sizeof(emabuf) - 1);

            cputs("EMA Compass Calib:", ctrm);
            cputs(emabuf, ctrm);
         }
         break;
      }

      // parse the config file 'mission.cfg' but skip the validation step
      case 'e': {
         cputs("", ctrm);

         // note this will probably complain about stack space as the ema agent function (i.e., this
         // function) reserves a ton of stack and the regex function called by configure does a stack check
         cputs("Parsing configuration file 'mission.cfg' ...", ctrm);
         configure(&mission, config_path, 1);
         break;
      }

      // capture ema data frame
      case 'f': {
         cputs("", ctrm);

         int emacode = EMAWake(NULL, -1);
         if (emacode > 0) {
            snprintf(buf, sizeof(buf), "Response from EMAWake [%d]", emacode);
            pputs(&conio, buf, 2, trm);
         }
         else
         {
            break;
         }

         EMADataFrame frame;
         emacode = EMAGetDataFrame(&frame, 0);
         if (emacode <= 0) {
            snprintf(buf, sizeof(buf), "EMA data frame fail; return code: %d", emacode);
            pputs(&conio, buf, 2, trm);

         } else {
            // need a bigger buffer for ema
            char emabuf[2048];
            EMAPrintDataFrameToString(&frame, emabuf, sizeof(emabuf) - 1);

            cputs("EMA Data Frame:", ctrm);
            cputs(emabuf, ctrm);
         }

         break;
      }

      case 'l': {
         cputs("", ctrm);
         LogConfiguration(&mission, "Mission");

         break;
      }

      case 'm': {
         cputs("", ctrm);
         cputs("Memory Map:", ctrm);
         Apf11MemoryMap();

         cputs("", ctrm);
         long heapavailable = Apf11HeapAvailable();
         unsigned long heapused = Apf11HeapUsed();

         snprintf(buf, sizeof(buf), "Heap available: %ld bytes, used: %lu bytes", heapavailable, heapused);
         pputs(&conio, buf, 2, trm);

         break;
      }

      // collect raw ema data at 10 second intervals, save to file, and then demodulate (process)
      case 'p': {
         cputs("Amount of time in seconds for capturing frames: ", "");
         buf[0] = 0;
         pgets(&conio, buf, sizeof(buf) - 1, 60, cr);

         if (buf[0] == 0)
         {
            break;
         }

         time_t endtime = atoi(buf);
         if (endtime <= 0)
         {
            break;
         }

         time_t runtime = 0;
         bool fail = false;
         char rawfilename[32];
         strftime(rawfilename, sizeof(rawfilename), "ema-%Y%m%dT%H%M%S.bin", Tm(time(NULL), 0, NULL));

         char procfilename[32];
         strftime(procfilename, sizeof(procfilename), "ema-%Y%m%dT%H%M%S.txt", Tm(time(NULL), 0, NULL));

         FILE* rawfile = NULL;
         FILE* procfile = NULL;
         if ((rawfile = fopen(rawfilename, "wb+")))
         {
            snprintf(buf, sizeof(buf), "Opening raw EMA file for writing: %s", rawfilename);
            pputs(&conio, buf, 2, trm);
            while (runtime < endtime)
            {
                time_t dT = itimer();
                int count = EMAWriteAllDataFramesToFile(rawfile);
                if (count < 0 && count != EMA_BUFFER_EMPTY)
                {
                    fail = true;
                    break;
                }
                else if (count == 0)
                {
                    continue;
                }

                sleep(10);
                runtime += difftime(itimer(), dT);
                if (ConioActive())
                {
                    snprintf(buf, sizeof(buf), "Wrote %d frames to the raw file.", count);
                    pputs(&conio, buf, 2, trm);
                }
            }
         }
         else
         {
            fail = true;
            cputs("Failed to open the raw file.", ctrm);
         }

         if (!fail)
         {
            // seek the binary file back to the beginning
           fseek(rawfile, 0, SEEK_SET);

           // open the processed file
           if (ConioActive())
           {
                snprintf(buf, sizeof(buf), "Opening processed EMA file for writing: %s", procfilename);
                pputs(&conio, buf, 2, trm);
           }

           if((procfile = fopen(procfilename, "w"))) {
              int count = EMADemodulateDataFromFile(rawfile, procfile, 10, 5);
              if (count < 0)
              {
                 if (ConioActive())
                 {
                    cputs("Problem demodulating data", ctrm);
                 }

                 fail = true;
              }

              if (ConioActive())
              {
                 snprintf(buf, sizeof(buf), "Wrote %d demodulated records to file", count);
                 pputs(&conio, buf, 2, trm);
              }

              fclose(procfile);
           } else if (ConioActive()) {
              cputs("Problem opening processed file", ctrm);
           }

           fclose(rawfile);
         }

         if (ConioActive() && !fail)
         {
            FIL fp;
            if (fat_mount() && f_open(&fp, procfilename, FA_READ | FA_OPEN_EXISTING) == FR_OK)
            {
                uint count = 0U;
                do
                {
                    if (f_read(&fp, buf, sizeof(buf), &count) == FR_OK && count > 0U)
                    {
                        fwrite(buf, 1, count, stdout);
                    }
                }
                while (count > 0U && !f_eof(&fp));
                f_close(&fp);
            }

            fat_umount();
         }

         break;
      }

      case 'r': {
         char rawfilename[32];
         strftime(rawfilename, sizeof(rawfilename), "ema-%Y%m%dT%H%M%S.bin", Tm(time(NULL), 0, NULL));
         FILE* rawfile = NULL;
         if ((rawfile = fopen(rawfilename, "wb+")))
         {
            snprintf(buf, sizeof(buf), "Opening raw EMA file for writing: %s", rawfilename);
            pputs(&conio, buf, 2, trm);
            int count = EMAWriteAllDataFramesToFile(rawfile);
            if (ConioActive())
            {
                snprintf(buf, sizeof(buf), "Wrote %d frames to the raw file.", count);
                pputs(&conio, buf, 2, trm);
            }

            fclose(rawfile);
         }

         break;
      }

      // put the ema board to sleep (i.e., a very low power stop)
      case 's': {
         cputs("", ctrm);

         unsigned char sleepbuffer[40];
         int emacode = EMASleep(sleepbuffer, 40);
         if(emacode > 0) {
            snprintf(buf, sizeof(buf), "Response from EMASleep [%d]: %s", emacode, sleepbuffer);
            pputs(&conio, buf, 2, trm);
            break;
         }
      }

      // random test cases that might be worth saving
      case 't': {
         cputs("", ctrm);

         char filepath[30];
         FILE* fp = NULL;

         for(int i = 0; i < 5; i += 1) {
            snprintf(filepath, sizeof(filepath), "test.%02d.txt", i);

            if(!(fp = fopen(filepath, "w"))) {
               cputs("Problem opening raw file for reading", ctrm);
               continue;
            }

            fprintf(fp, "File content for: %d\n\n", i);
            fclose(fp);
         }

         break;
      }

      // wake the ema board
      case 'w': {
         cputs("", ctrm);

         unsigned char wakebuffer[128];
         int emacode = EMAWake(wakebuffer, 128);
         if(emacode > 0) {
            snprintf(buf, sizeof(buf), "Response from EMAWake [%d]: %s", emacode, wakebuffer);
            pputs(&conio, buf, 2, trm);
         }

         break;
      }

      // demodulate binary data frame file '00000.001.efr' into '00000.001.efp'
      case 'x': {
         cputs("", ctrm);

         FILE* rawfile = NULL;
         if(!(rawfile = fopen("00000.001.efr", "rb"))) {
            cputs("Problem opening raw file for reading", ctrm);
            break;
         }

         FILE* outfile = NULL;
         if(!(outfile = fopen("00000.001.efp", "w"))) {
            cputs("Problem opening demod file for writing", ctrm);
            break;
         }

         int count = EMADemodulateDataFromFile(rawfile, outfile, 10, 5);
         snprintf(buf, sizeof(buf), "Wrote %d demodulated records to file", count);
         pputs(&conio, buf, 2, trm);

         fclose(rawfile);

         fflush(outfile);
         fclose(outfile);

         break;
      }

      // compress sample file to gzip using zlib
      case 'z': {
         char* infile = "sample.msg";
         char* outfile = "sample.msg.gz";

         FILE* infp = NULL;
         if(!(infp = fopen(infile, "r"))) {
            cputs("Problem opening message file for reading", ctrm);
            break;
         }

         FILE* outfp = NULL;
         if(!(outfp = fopen(outfile, "wb"))) {
            cputs("Problem opening binary file for writing", ctrm);

            fclose(infp);
            break;
         }

         z_stream strm;
         strm.zalloc = Z_NULL;
         strm.zfree = Z_NULL;
         strm.opaque = Z_NULL;

         // this creates the gzip format; docs from the function:
	      // Add 16 to windowBits to write a simple gzip header and trailer around the compressed data instead of a zlib wrapper
         // the apf board reported having only about 15K heap space so had to reduce the zlib options to

         int zlibcode = deflateInit2(
            &strm,
            Z_DEFAULT_COMPRESSION,
            Z_DEFLATED,

            // the memory usage of windowBits and memLevel is described in this equation:
            //    deflate memory usage (bytes) = (1 << (windowBits+2)) + (1 << (memLevel+9))
            // a wB of 10 and a mL of 3 yields exactly 8192 bytes so we're using that given the ~16KB limit

            // this is the windowBits, with a range of 9-15 (default 15), where the higher the number
            // the better compression at the expense of higher memory usage
            // the added 16 is to add a gzip wrapper around the zlib payload, from the docs:
            //    Add 16 to windowBits to write a simple gzip header and trailer around the compressed data instead of a zlib wrapper
            10 | 16,

            // this is the memLevel and is related to window bits; the range is 1 - 9 (default 8)
            // the higher the level the better compression, faster to finish, and uses more memory
            3,
            Z_DEFAULT_STRATEGY
         );

         if(zlibcode != Z_OK) {
            cputs("Problem initializing deflate object", ctrm);

            fclose(outfp);
            fclose(infp);
            break;
         }

         unsigned int have;
         int chunk = 1024;
         unsigned char inbuffer[chunk];
         unsigned char outbuffer[chunk];
         int iseof = 0;

         do {
            strm.avail_in = fread(inbuffer, 1, chunk, infp);
            strm.next_in = inbuffer;

            iseof = feof(infp) ? Z_FINISH : Z_NO_FLUSH;
            do {
               strm.avail_out = chunk;
               strm.next_out = outbuffer;

               zlibcode = deflate(&strm, iseof);
               if(zlibcode == Z_STREAM_ERROR) {
                  cputs("Deflate error during deflation", ctrm);

                  fclose(outfp);
                  fclose(infp);
                  break;
               }

               have = chunk - strm.avail_out;
               if(fwrite(outbuffer, 1, have, outfp) != have || ferror(outfp)) {
                  cputs("Write error to output stream", ctrm);

                  fclose(outfp);
                  fclose(infp);
                  break;
               }

            } while(strm.avail_out == 0);
         } while(iseof != Z_FINISH);

	      deflateEnd(&strm);

         fclose(outfp);
         fclose(infp);

         cputs("Successfully deflated message file to: sample.msg.gz", ctrm);
         break;
      }

      // print the menu
      case '?': {
         cputs("", ctrm);
      }

      // the default response is to print the menu
      default: {
         cputs("Menu of EMA functions.", ctrm);
         cputs("Z? Print this menu.", ctrm);
         cputs("Za Show the current mission state.", ctrm);
         cputs("Zb Print the simulation flags.", ctrm);
         cputs("Zc Get raw compass calibration sample.", ctrm);
         cputs("Ze Parse config file 'mission.cfg' but bypass validation.", ctrm);
         cputs("Zf Get raw data frame sample.", ctrm);
         cputs("Zl Print configuration to log.", ctrm);
         cputs("Zm Show memory information.", ctrm);
         cputs("Zp Collect raw data frames at 10 second intervals, save to file, and then demodulate (process).", ctrm);
         cputs("Zr Collect raw data frames and save to a file.", ctrm);
         cputs("Zs Put the EMA in a very low power stop (sleep).", ctrm);
         cputs("Zt Test case(s).", ctrm);
         cputs("Zw Wake the EMA if it is asleep (no-op if awake).", ctrm);
         cputs("Zx Demodulate binary data frame file '00000.001.efr' into '00000.001.efp'.", ctrm);
         cputs("Zz Compress sample file to gzip with zlib.", ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* agent to exercise the Garmin GPS15 engine                              */
/*------------------------------------------------------------------------*/
static void GarminGps15Agent(void)
{
   unsigned char key; char buf[80];
   time_t T,dT,timeout;
   struct NmeaGpsFields gpsfix;

   /* define error message for  out-of-range entries */
   cc *GpsError = "ERROR: Attempt to query GPS failed.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* upload the almanac into the GPS */
      case 'a':
      {
         cputs("Uploading the almanac into the Garmin GPS15L-W.",ctrm);
         GpsEnable(4800); GpsAlmanacUpLoad(&gps); GpsDisable();
         AlmanacTimeStamp=time(NULL);
         cputs("Upload finished.",ctrm);
         break;
      }

      /* configure the Garmin GPS15L-W */
      case 'c':
      {
         /* save current debuglevel and reset for maximum verbosity */
         unsigned int verbosity=debugbits; debugbits=5;
         cputs("Configuring the Garmin GPS15L-W.",ctrm);
         GpsEnable(4800);
         if (ConfigGarmin15(&gps)>0) cputs("Configuration successful.",ctrm);
         else cputs(GpsError,ctrm);
         GpsDisable();
         debugbits=verbosity;
         break;
      }

      /* get a fix from the Garmin GPS15L-W */
      case 'f':
      {
         /* set the timeout to get the gps fix */
         timeout=360;

         cputs("Acquiring a fix from the Garmin GPS15L-W.",ctrm);

         /* power-up gps engine and flush IO ports */
         GpsEnable(4800); if (gps.ioflush) gps.ioflush();

         /* get a GPS fix */
         if ((dT=GetGpsFix(&gps,&gpsfix,timeout))>0)
         {
            /* store the current fix for later use */
            GpsFix=gpsfix;

            /* write the GPS fix data to console */
            snprintf(buf,sizeof(buf),"# GPS fix obtained in %ld seconds.",dT);
            pputs(&conio,buf,2,trm);
            snprintf(buf,sizeof(buf),"#    %8s %7s %2s/%2s/%4s %6s %4s",
                     "lon","lat","mm","dd","yyyy","hhmmss","nsat");
            pputs(&conio,buf,2,trm);
            snprintf(buf,sizeof(buf),"Fix: %8.3f %7.3f %02d/%02d/%04d %06ld %4d",
                      gpsfix.lon,gpsfix.lat,gpsfix.mon,gpsfix.day,gpsfix.year,
                      gpsfix.hhmmss,gpsfix.nsat);
            pputs(&conio,buf,2,trm);
         }
         else cputs(GpsError,ctrm);

         /* power-down the GPS */
         GpsDisable();

         break;
      }

      /* synthesize an almanac */
      case 'i':
      {
         cputs("Initialize the almanac with GPS-week <1>:",ctrm);
         GpsEnable(4800); GpsAlmanacInitialize(&gps); GpsDisable();
         break;
      }

      /* log output from the Garmin GPS15L-W */
      case 'l':
      {
         cputs("Logging NMEA sentences from the Garmin GPS15L-W.",ctrm);
         GpsEnable(4800);
         if (LogNmeaSentences(&gps)<0) cputs(GpsError,ctrm);
         GpsDisable();
         break;
      }

      /* transfer the synthetic almanac from file to GPS */
      case 'm':
      {
         cputs("Enter file name for synthetic almanac: ",ns);
         pgets(&conio,buf,sizeof(buf)-1,60,cr);
         cputs("Installing synthetic almanac into GPS.",ctrm);
         GpsEnable(4800); Garmin15Cmds(&gps,buf);
         ConfigGarmin15(&gps);GpsDisable();
         break;
      }

      /* read the almanac from the GPS */
      case 'p':
      {
         cputs("Reading current almanac from GPS.",ctrm);
         GpsEnable(4800); GpsAlmanacReport(&gps); GpsDisable();
         break;
      }

      /* refresh the almanac */
      case 'r':
      {
         cputs("Refreshing the almanac.",ctrm);
         GpsEnable(4800); GpsAlmanacRefresh(&gps); GpsDisable();
         break;
      }

      case 't':
      {
         timeout=360;
         cputs("Synchronizing the Apf11 clock with GPS time.",ctrm);

         /* power-up the GPS engine */
         GpsEnable(4800);

         /* get the time from the GPS */
         if ((T=GetGpsTime(&gps,timeout))>0)
         {
            /* set the Apf11's DS2404 RTC */
            if (RtcSet(T)>0)
            {
               /* get the current time */
               T=time(NULL);

               /* construct the response to the user */
               snprintf(buf,sizeof(buf),"Real time clock: %s",ctime(&T));

               /* write the response to the console */
               pputs(&conio,buf,2,ns);
            }
            else cputs("Attempt to set RTC failed.",ctrm);
         }
         else cputs(GpsError,ctrm);

         /* power-down the GPS engine */
         GpsDisable();

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of Garmin GPS15L-W functions.",                 ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("Ga Upload almanac to GPS15L-W.",                     ctrm);
         cputs("Gc Configure the GPS15L-W.",                         ctrm);
         cputs("Gf Get GPS15L-W fix.",                               ctrm);
         cputs("Gi Initialize the GPS15xL with synthetic almanac.",  ctrm);
         cputs("Gl Log NMEA sentences from GPS15L-W.",               ctrm);
         cputs("Gm Install synthetic almanac into GPS15xL.",         ctrm);
         cputs("Gp Read almanac from GPS15xL.",                      ctrm);
         cputs("Gr Refresh the GPS15xL almanac. (20min).",           ctrm);
         cputs("Gt Synchronize the Apf11 clock with GPS.",           ctrm);
      }
   }

   /* disable the GPS interface */
   GpsDisable();
}

/*------------------------------------------------------------------------*/
/* agent to configure telemetry to the remote host                        */
/*------------------------------------------------------------------------*/
/**
   This function allows configuration of the telemetry to a remote host.
*/
static void HostAgent(void)
{
   unsigned char key; long val; char buf[80];

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* get the dial command for the alternate host */
      case 'a':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf)
         {
            snprintf(mission.alt,sizeof(mission.alt)-1,"%s",buf);
            snprintf(buf,sizeof(buf)-1,"Dial command to alternater host: %s",mission.alt);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the dial command for the primary host */
      case 'p':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf)
         {
            snprintf(mission.at,sizeof(mission.at)-1,"%s",buf);
            snprintf(buf,sizeof(buf)-1,"Dial command to primary host: %s",mission.at);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the telemetry-retry period */
      case 'r':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*Min)>=Min && val<=WDogTimeOut)
         {
            mission.TimeTelemetryRetry = val;
            snprintf(buf,sizeof(buf),"The telemetry retry interval is %ld minutes.",
                     mission.TimeTelemetryRetry/Min);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the time-out period for host connect attempts */
      case 't':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=30 && val<=300)
         {
            mission.ConnectTimeOut = val;
            snprintf(buf,sizeof(buf),"The time-out period for connecting to remote "
                     "host is %ld seconds.",mission.ConnectTimeOut);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the ZModem time-out period */
      case 'z':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=30 && val<=600)
         {
            mission.ZModemTimeOut = val;
            snprintf(buf,sizeof(buf),"The ZModem time-out period is %ld seconds.",
                     mission.ZModemTimeOut);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu for telemetry configuration to remote host.", ctrm);
         cputs("?  Print this menu.",                              ctrm);
         cputs("Ha  Dial command for alternate host.",             ctrm);
         cputs("Hp  Dial command for primary host.",               ctrm);
         cputs("Hr  Telemetry retry interval. [1-105 minutes].",   ctrm);
         cputs("Ht  Host-connect time-out. [30-300 seconds].",     ctrm);
         cputs("Hz  ZModem time-out. [30-600 seconds].",           ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* function to check for a keyboard escape request                        */
/*------------------------------------------------------------------------*/
/**
   This function checks for a keyboard ESCAPE request to enter command mode.
   Two test criteria are applied.  The first is that the 20mA current loop is
   active which indicates that a computer is attached to the controller.  The
   second is that the ESCAPE key was hit as one of the first 256 characters in
   the console IO buffer.  If both criteria are satisfied then this function
   returns a positive number to indicate an affirmative keyboard escape
   reqeuest.  If any of the criteria are violated then this function returns
   zero.
*/
int KbdEscRequest(void)
{
   int i,byte,status=0;

   /* check if 20mA loop is active */
   if (ConioActive()>0)
   {
      /* read one byte at a time from the console buffer */
      for (i=0; (byte=kbdhit())>0 && i<256; i++)
      {
         /* check for escape character (0x1B) */
         if (byte==0x1B) {status=1; break;}
      }

      /* flush the IO buffers */
      conio.ioflush();
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* interface for protected diagnostics mode                               */
/*------------------------------------------------------------------------*/
/**
   This function implements a float-manufacturer user interface for
   executing diagnostic functions of APEX floats that are unlikely or
   dangerous to be used by ordinary users.
*/
static void ManufacturerDiagnosticsAgent(void)
{
   char buf[80]; unsigned char key,byte;
   long int val; unsigned int crc; int i,n,err,status;
   FILE *dest; char fname[32],mode[2]={'w',0};

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm); key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* write useless files to the FatFs volume */
      case 'a':
      {
         cputs("WARNING: All of the data in RamFs file system "
               "will be destroyed.  Proceed? [y/n] ","");

         /* get confirmation that the request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* close all files and reformat the file system */
            cputs("",ctrm); fcloseall(); fformat(); Wait(250);

            cputs("Filling FatFs with test files...",ctrm);
            for (i=0; i<MAXFILES; i++)
            {
               snprintf(fname,sizeof(fname)-1,"file.%03d",i);
               if ((dest=fopen(fname,mode)))
               {
                  if (fprintf(dest,fname)<0) snprintf(buf,sizeof(buf)-1,"Failed attempt to write to: %s",fname);
                  else if (fclose(dest)) snprintf(buf,sizeof(buf)-1,"Failed attempt to close: %s [errno=%d]",fname,errno);
                  else snprintf(buf,sizeof(buf)-1,"Created test file: %s",fname);
                  Wait(250);
               }
               else {snprintf(buf,sizeof(buf)-1,"Failed attempt to open: "
                               "%s [errno=%d].",fname,errno);}

               pputs(&conio,buf,2,trm);
            }
         }
         else {cputs("",ctrm); cputs("File system test safely aborted.",ctrm);}

         break;
      }

      /* parse the mission configuration file */
      case 'b':
      {
         cputs("Parsing the mission configuration file.",ctrm);

         /* process the configuration file */
         configure(&mission,config_path,0);

         break;
      }

      /* load the mission configuration with defaults */
      case 'd':
      {
         cputs("Mission configuration will be loaded "
               "with defaults.  Proceed? [y/n] ","");

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* load the mission configuration with defaults */
            cputs("",ctrm); mission=DefaultMission;

            /* compute the signature of the mission program */
            mission.crc = Crc16Bit((unsigned char *)(&mission),
                                   sizeof(mission)-sizeof(mission.crc));

            /* write the new mission configuration to EEPROM */
            if (MissionParametersWrite(&mission)>0)
            {
               cputs("Default mission parameters written to EEPROM.",ctrm);
            }
            else cputs("WARNING: Attempt to write mission "
                     "parameters to EEPROM failed.",ctrm);

            /* initialize the charge consumption model and engineering data */
            ChargeModelInit(); InitVitals(); GpsFix=GpsInit;

            /* initialize the logging facility and enable the FatFs system */
            LogPredicate(1); loghandle=-1; debugbits=2; FatFsDisable=0;
         }
         else {cputs("",ctrm); cputs("Pending default mission "
                    "configuration aborted.",ctrm);}

         break;
      }

      /* execute the eeprom test */
      case 'e':
      {
         cputs("WARNING: All of the data in EEPROM "
               "will be destroyed.  Proceed? [y/n] ","");

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* execute the EEPROM test */
            cputs("",ctrm); At25Test();

            /* write the mission parameters back to EEPROM */
            MissionParametersWrite(&mission);

            /* set the mission state to inactive */
            StateSet(UNDEFINED);
         }
         else {cputs("",ctrm); cputs("EEPROM test safely aborted.",ctrm);}

         break;
      }

      /* set the float serial number */
      case 'f':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf))>=0 && val<=32767)
         {
            mission.FloatId = val;
            snprintf(buf,sizeof(buf),"Float id: %05u",mission.FloatId);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* display the EEPROM map */
      case 'g': {cputs("EEPROM map:",trm); EepromMap(); break;}

      /* inactivate the float */
      case 'i':
      {
         /* warn the user that terminating the mission makes the float non-deployable */
         cputs("",ctrm);
         cputs("WARNING: Inactivating the float renders it nondeployable",ctrm);
         cputs("         until the mission is reactivated.  Inactivate anyway?","\n\n");
         cputs("         Type 'Y' to inactivate the float or any other key to abort."," ");
         conio.ioflush();

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            /* warn that the float is not deployable until the mission is reactivated */
            cputs("\nWARNING: The float is inactivated and is not deployable",ctrm);
            cputs("             until the mission is reactivated.",ctrm);

            /* put the float mission into inactive mode and open the air valve */
            StateSet(INACTIVE);
         }

         /* abort the deactivation request */
         else cputs("\nInactivation aborted; the mission in progress will continue.",ctrm);

         break;
      }

      /* query the Apf11's Coulomb counter for its serial number */
      case 'j':
      {
         /* local work objects required to query the DS2740's net address */
         unsigned long int serno; unsigned short int group; unsigned char family,crc;

         /* query the DS2740's family and 48-bit serial number */
         if (DowNetAddress(&serno,&group,&family,&crc)>0)
         {
            snprintf(buf,sizeof(buf),"Apf11: Family:0x%02x Group:0x%04hx SerNo:0x%08lx "
                     "Crc:0x%02x SeqNo:%03lu",family,group,serno,crc,serno%1000);
            pputs(&conio,buf,2,trm);
         }
         else cputs("Attempt to query Coulomb counter failed.",ctrm);

         break;
      }

      /* list the mission parameters */
      case 'l':
      {
         cputs("",ctrm);
         ShowPwd=1; ShowParameters();
         break;
      }

      /* specify mission programming */
      case 'm':
      {
         cputs("",ctrm);
         MissionProgrammingAgent();
         break;
      }

      /* specify password */
      case 'p':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf)
         {
            snprintf(mission.pwd,sizeof(mission.pwd)-1,"%s",buf);
            snprintf(buf,sizeof(buf)-1,"Password: %s",mission.pwd);
            pputs(&conio,buf,2,trm);

         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* perform the APF11 ram test */
      case 'r':
      {
         cputs("WARNING: All of the data in APF11 far RAM "
               "will be destroyed.  Proceed? [y/n] ","");

         /* get confirmation that the deactivation request is intentional */
         if (pgetbuf(&conio,buf,1,5)>0 && tolower(*buf)=='y')
         {
            unsigned long int nError;

            cputs("\nExecuting APF11 far RAM test.  RAM must "
                  "be reinitialized before deployment.",ctrm);

            /* execute the far ram test */
            if ((nError=RamTest())==0) cputs("APF11 far RAM test passed.",ctrm);
            else
            {
               snprintf(buf,sizeof(buf),"APF11 far RAM test failed with %lu errors.",nError);
               pputs(&conio,buf,2,trm);
            }
         }
         else cputs("\nAPF11 far RAM test safely aborted.",ctrm);

         break;
      }

      /* print some important time diagnostics */
      case 's':
      {
         cputs("",ctrm);
         SequencePointsDisplay();
         break;
      }

      case 't':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         /* reset the mission timer */
         if (*buf && (val=atol(buf))>0)
         {
            /* reset the mission timer and mission alarm */
            IntervalTimerSet(val-10,0); SetAlarm(val);

            /* show the new timing info */
            SequencePointsDisplay();

            /* disable the alarm interrupt until exiting cmd mode */
            IntervalAlarmInterruptOff();
         }
         else cputs("Missing value for mission timer - ignoring.",ctrm);

         break;
      }

      /* load the mission configuration with defaults */
      case 'u':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf)
         {
            snprintf(mission.user,sizeof(mission.user)-1,"%s",buf);
            snprintf(buf,sizeof(buf)-1,"User name: %s",mission.user);
            pputs(&conio,buf,2,trm);

         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* set user name */
      case 'v':
      {
         /* create a default user name based on the float id */
         snprintf(mission.user,sizeof(mission.user)-1,"f%04u",mission.FloatId);
         snprintf(buf,sizeof(buf)-1,"%sUser name: %s",trm,mission.user);
         pputs(&conio,buf,2,trm);

         /* create a default password */
         snprintf(mission.pwd,sizeof(mission.pwd)-1,"e=2.718");
         snprintf(buf,sizeof(buf)-1,"Password: %s",mission.pwd);
         pputs(&conio,buf,2,trm);

         break;
      }

      /* compute persistent SRAM CRC reference vector*/
      case 'w':
      {
         cputs("Computing persistent SRAM CRC reference vector...","");
         PRamCrcRefWrite(PRamCrcRef,PRamBlockCnt);
         cputs("done.",ctrm); break;
      }

      /* check persistent SRAM against reference CRCs */
      case 'x':
      {
         cputs("Checking persistent SRAM against reference CRCs.",ctrm);
         PRamCrcRefRead(PRamCrcRef,PRamBlockCnt);
         PRamCrcCheck(PRamCrcRef,PRamBlockCnt);
         break;
      }

      /* Com1 serial port test */
      case '1':
      {
         cputs("Executing tests of Com1 serial port.",ctrm);

         /* enable Com1 */
         Com1Enable(9600);

         /* execute the hardware control/status line test */
         for (status=1,n=0; n<10; n++)
         {
            Com1RtsAssert(); Wait(10); if (Com1Cts()<=0) status=0;
            Com1RtsClear();  Wait(10); if (Com1Cts()!=0) status=0;
         }

         /* pass/fail report */
         cputs("Com1 hardware control/status line test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* transmit a known test pattern */
         for (n='a'; n<='z'; n++) {com1.putb(n); Wait(10);}

         /* check for a match of the test pattern */
         for (status=1, n='a'; n<='z'; n++)
         {
            byte=0; if ((err=pgetb(&com1,&byte,1))<=0 || byte!=n) {status=0; break;}
         }

         /* pass/fail report */
         cputs("Com1 Tx/Rx test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* exercise the BREAK condition */
         Wait(500); Com1TxBreak(500);

         /* disable com ports 1,2 */
         Com1Disable();

         break;
      }

      /* Com2 serial port test */
      case '2':
      {
         cputs("Executing tests of Com2 serial port.",ctrm);

         /* enable Com2 */
         Com2Enable(9600);

         /* execute the hardware control/status line test */
         for (status=1,n=0; n<10; n++)
         {
            Com2RtsAssert(); Wait(10); if (Com2Cts()<=0) status=0;
            Com2RtsClear();  Wait(10); if (Com2Cts()!=0) status=0;
         }

         /* pass/fail report */
         cputs("Com2 hardware control/status line test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* transmit a known test pattern */
         for (n='a'; n<='z'; n++) {com2.putb(n); Wait(2);}

         /* check for a match of the test pattern */
         for (status=1, n='a'; n<='z'; n++)
         {
            byte=0; if ((err=pgetb(&com2,&byte,1))<=0 || byte!=n) {status=0; break;}
         }

         /* pass/fail report */
         cputs("Com2 Tx/Rx test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* exercise the BREAK condition */
         Wait(500); Com2TxBreak(500);

         /* disable com port 2 */
         Com2Disable();

         break;
      }

      /* Iridium serial port test */
      case '3':
      {
         cputs("Executing tests of the Iridium serial port.",ctrm);

         /* enable Iridium port */
         ModemEnable(19200);

         /* execute the hardware control/status line test */
         for (status=1,n=0; n<10; n++)
         {
            ModemDtrAssert(); Wait(10); if (ModemCd()<=0 || ModemDsr()<=0) status=0;
            ModemDtrClear();  Wait(10); if (ModemCd()!=0 || ModemDsr()!=0) status=0;
         }

         /* pass/fail report */
         cputs("Iridium serial port control/status line tests ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* execute the hardware control/status line test */
         for (status=1,n=0; n<10; n++)
         {
            ModemRtsAssert(); Wait(10); if (ModemCts()<=0) status=0;
            ModemRtsClear();  Wait(10); if (ModemCts()!=0) status=0;
         }

         /* pass/fail report */
         cputs("Iridium serial port RTS/CTS line tests ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* transmit a known test pattern */
         for (n='a'; n<='z'; n++) {modem.putb(n); Wait(2);}

         /* check for a match of the test pattern */
         for (status=1, n='a'; n<='z'; n++)
         {
            byte=0; if (modem.getb(&byte)<=0 || byte!=n) status=0;
         }

         /* pass/fail report */
         cputs("Iridium serial port Tx/Rx test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* disable Iridium port */
         ModemDisable();

         break;
      }

      /* GPS serial port test */
      case '4':
      {
         cputs("Executing tests of the GPS serial port.",ctrm);

         /* enable the GPS port */
         GpsEnable(4800);

         /* transmit a known test pattern */
         for (n='a'; n<='z'; n++) {gps.putb(n); Wait(2);}

         /* check for a match of the test pattern */
         for (status=1, n='a'; n<='z'; n++)
         {
            byte=0; if (gps.getb(&byte)<=0 || byte!=n) status=0;
         }

         /* pass/fail report */
         cputs("GPS serial port Tx/Rx test ","");
         cputs((status)?"passed.":"failed.",ctrm);

         /* disable Iridium port */
         GpsDisable();

         break;
      }

      /* enter the Stm32 register inspection agent */
      case '@': {RegisterInspectionAgent(); break;}

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of manufacturer's diagnostics.",                ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("a  Write test files to FatFs volume.",               ctrm);
         cputs("b  Parse the mission configuration file.",           ctrm);
         cputs("d  Load mission configuration with defaults.",       ctrm);
         cputs("e  EEPROM test (destructive to stored data).",       ctrm);
         cputs("f  Set float id. [0-32767]",                         ctrm);
         cputs("g  EEPROM map.",                                     ctrm);
         cputs("i  Inactivate float.",                               ctrm);
         cputs("j  Apf11 family, group, serial, & sequence number.", ctrm);
         cputs("l  List mission parameters.",                        ctrm);
         cputs("m  Mission programming agent.",                      ctrm);
         cputs("p  Specify password for remote host.",               ctrm);
         cputs("r  Perform APF11 far RAM test.",                     ctrm);
         cputs("s  Display sequence points and timing info.",        ctrm);
         cputs("t  Set the mission timer/alarm.",                    ctrm);
         cputs("u  Specify custom user name.",                       ctrm);
         cputs("v  Set default user name and password.",             ctrm);
         cputs("w  Compute persistent SRAM CRC reference vector.",   ctrm);
         cputs("x  Check persistent SRAM CRC reference vector.",     ctrm);
         cputs("1  Com1 serial port tests.",                         ctrm);
         cputs("2  Com2 serial port tests.",                         ctrm);
         cputs("3  Iridium serial port tests.",                      ctrm);
         cputs("4  GPS serial port tests.",                          ctrm);
         cputs("@  Stm32 register inspection agent.",                ctrm);
         cputs("@? Stm32 register inspection menu.",                 ctrm);
      }
   }

   /* compute the signature of the mission program */
   crc = Crc16Bit((unsigned char *)(&mission),sizeof(mission)-sizeof(mission.crc));

   /* check if the mission programming has changed */
   if (mission.crc!=crc)
   {
      /* assign the new mission crc */
      mission.crc=crc;

      /* write the new mission configuration to EEPROM */
      if (MissionParametersWrite(&mission)<=0)
      {
         cputs("WARNING: Attempt to write mission "
               "parameters to EEPROM failed.",ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* mission programming agent                                              */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for configuring mission
   parameters.
*/
static void MissionProgrammingAgent(void)
{
   int status=1; long int val;
   char buf[80]; unsigned char key;
   const time_t TimeOut=300; time_t To;

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   cputs("Entering Mission Programming Agent",ctrm);

   do
   {
      // record the current time to implement loop timeout
      To=time(NULL);

      /* flush the console's Rx buffer */
      conio.ioflush();

      /* write a prompt to stdout */
      cputs("> ","");

      /* wait for the next keyboard-hit */
      while ((key=kbdhit())<=0)
      {
         /* arrange for the APF11 to be put in hibernate mode */
         if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
         {
            cputs(" (timeout)",ctrm); key=ESC; break;
         }
      }

      /* acknowledge key entry by spacing over */
      if (key!=CR) conio.putb(' ');

      switch (tolower(key))
      {
         /* get piston parameters for buoyancy control */
         case 'b': {BuoyancyControlAgent(); break;}

         /* get the pressure to enable continuous profiling */
         case 'c':
         {
            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            if (*buf)
            {
               if ((val=atoi(buf))<20)
               {
                  snprintf(buf,sizeof(buf)-1,"Continuous profiling disabled.");
                  mission.PressureCP=0;
               }
               else
               {
                  /* set the activation pressure for continuous profiling */
                  mission.PressureCP = (val<5000) ? val : 5000;

                  snprintf(buf,sizeof(buf)-1,"Continuous profile activation "
                            "pressure: %0.0f decibars.",mission.PressureCP);
               }

               pputs(&conio,buf,2,trm);
            }
            else cputs(RangeError,ctrm);

            break;
         }

         /* branch to the loop for float parameters */
         case 'f': {FloatVitalsAgent(); break;}

         /* branch to host configuration agent */
         case 'h': {HostAgent(); break;}

         /* get the deep profile pressure */
         case 'j':
         {
            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            if (*buf && (val=atoi(buf))>0 && val<=2050)
            {
               mission.PressureProfile = val;
               snprintf(buf,sizeof(buf),"Deep-profile pressure: "
                        "%0.0f decibars.",mission.PressureProfile);
               pputs(&conio,buf,2,trm);
            }
            else cputs(RangeError,ctrm);

            break;
         }

         /* get the park pressure */
         case 'k':
         {
            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            if (*buf && (val=atoi(buf))>0 && val<=2000)
            {
               mission.PressurePark = val;
               snprintf(buf,sizeof(buf),"Park pressure: %0.0f decibars.",
                        mission.PressurePark);
               pputs(&conio,buf,2,trm);
            }
            else cputs(RangeError,ctrm);

            break;
         }

         /* list the mission parameters */
         case 'l':
         {
            cputs("",ctrm);
            ShowParameters();
            break;
         }

         /* ignore the m-key - the user is already in the mission agent */
         case 'm': {cputs("",ctrm); break;}

         /* get the PnP cycle length */
         case 'n':
         {
            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            if (*buf && (val=atoi(buf))>0 && val<255)
            {
               mission.PnpCycleLength = val;

               if (val<254)
               {
                  snprintf(buf,sizeof(buf),"The park-and-profile cycle "
                            "length is %d.",mission.PnpCycleLength);
                  pputs(&conio,buf,2,trm);
               }
               else cputs("Park-and-profile feature disabled.",ctrm);
            }
            else cputs(RangeError,ctrm);

            break;
         }

         /* exit the loop and perform sanity checks on mission programming */
         case 'q':
         {
            cputs("",ctrm);

            /* analyze the mission and check for fatal errors by masking with 0x01 */
            status=(ConfigSupervisor(&mission) & 0x01);

            break;
         }

         /* get parameters to control mission timing */
         case 't': {MissionTimingAgent(); break;}

         /* enable or disable the FatFs system */
         case 'w':
         {
            cputs("Enable or disable FatFs file system (1,0): ","");

            /* get user input from the console */
            pgets(&conio,buf,sizeof(buf)-1,60,cr);

            if (*buf)
            {
               mission.FatFsEnable = (atoi(buf))?1:0; FatFsDisable=(!mission.FatFsEnable);
               if (mission.FatFsEnable) {snprintf(buf,sizeof(buf),"FatFs file system enabled.");}
               else {snprintf(buf,sizeof(buf),"WARNING: FatFs file system disabled.");}
               pputs(&conio,buf,2,trm);
            }
            else cputs(RangeError,ctrm);

            break;
         }

         /* perform sanity checks on the current mission programming */
         case 'z': {cputs("",ctrm); ConfigSupervisor(&mission); break;}

         /* exit without analyzing the mission */
         case ESC: {status=0; break;}

         /* print the menu */
         case '?': cputs("",ctrm);

         /* the default response is to print the menu */
         default:
         {
            cputs("Menu selections are not case sensitive.",                              ctrm);
            cputs("?  Print this menu.",                                                  ctrm);
            cputs("B  Buoyancy control agent.",                                           ctrm);
            cputs("B? Buoyancy control menu.",                                            ctrm);
            cputs("C  Continuous profile activation pressure (decibars).",                ctrm);
            cputs("F  Float vitals agent.",                                               ctrm);
            cputs("F? Float vitals menu.",                                                ctrm);
            cputs("H  Host configuration agent.",                                         ctrm);
            cputs("H? Host configuration menu.",                                          ctrm);
            cputs("J  Deep-profile pressure. (0-2050] (decibars)",                        ctrm);
            cputs("K  Park pressure. (0-2000] (decibars)",                                ctrm);
            cputs("L  List mission parameters.",                                          ctrm);
            cputs("N  Park and profile cycle length. [1-4056]",                           ctrm);
            cputs("Q  Quit the mission programming agent.",                               ctrm);
            cputs("T  Mission timing agent.",                                             ctrm);
            cputs("T? Mission timing menu.",                                              ctrm);
            cputs("W  Enable or disable the FatFS file system. [1,0]",                    ctrm);
            cputs("Z  Analyze the current mission programming.",                          ctrm);
         }
      }
   }
   while (status>0);

   /* compute the signature of the mission program */
   mission.crc = Crc16Bit((unsigned char *)(&mission), sizeof(mission)-sizeof(mission.crc));

   /* write the new mission configuration to EEPROM */
   if (MissionParametersWrite(&mission)<=0)
   {
      cputs("WARNING: Attempt to write mission parameters to EEPROM failed.",ctrm);
   }

   cputs("Quiting Mission Programming Agent.",ctrm);
}

/*------------------------------------------------------------------------*/
/* mission timing agent                                                   */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for configuring mission
   parameters related to timing.
*/
static void MissionTimingAgent(void)
{
   unsigned char key; char buf[100]; long int val;

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* define error message for out-of-range entries */
   cc *RangeError = "ERROR: Not in valid range.";

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* get the maximum ascent time */
      case 'a':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>=1*Min && val<=10*Hour)
         {
            mission.TimeOutAscent = val;
            snprintf(buf,sizeof(buf),"The ascent-phase of the profile "
                     "cycle will time-out after %ld " TUnits ".",
                     mission.TimeOutAscent/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the time-of-day for expiration of down-time */
      case 'c':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>=0 && val<1440*Min)
         {
            mission.ToD = val;
            snprintf(buf,sizeof(buf),"The down-time will expire at %ld " TUnits
                     " after midnight.",mission.ToD/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else {mission.ToD=-1; cputs("The time-of-day feature has been disabled.",ctrm);}

         break;
      }

      /* get the down time */
      case 'd':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>0 && val<=30*Day)
         {
            mission.TimeDown = val;
            snprintf(buf,sizeof(buf),"The drift-phase of the profile cycle will "
                     "consume %ld " TUnits ".",mission.TimeDown/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the estimated descent time from park pressure for a deep profile */
      case 'j':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>=0 && val<=8*Hour)
         {
            mission.TimeDeepProfileDescent = val;
            snprintf(buf,sizeof(buf),"Profile will begin no later than "
                     "%ld " TUnits " after descent from park depth.",
                     mission.TimeDeepProfileDescent/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the estimated descent time from surface to park pressure */
      case 'k':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>0 && val<=8*Hour)
         {
            mission.TimeParkDescent = val;
            snprintf(buf,sizeof(buf),"Active ballasting will begin "
                      "%ld " TUnits " after descent from surface.",
                     mission.TimeParkDescent/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the length of the mission prelude */
      case 'p':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>0 && val<=6*Hour)
         {
            mission.TimePrelude = val;
            snprintf(buf,sizeof(buf),"Mission prelude: Transmit on "
                     "the surface for %ld " TUnits " prior to first "
                     "descent.",mission.TimePrelude/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* get the up-time */
      case 'u':
      {
         /* get user input from the console */
         pgets(&conio,buf,sizeof(buf)-1,60,cr);

         if (*buf && (val=atoi(buf)*TQuantum)>0 && val<=24*Hour)
         {
            mission.TimeUp = val;
            snprintf(buf,sizeof(buf),"The profile & telemetry phases of the profile "
                     "cycle will consume %ld " TUnits ".",mission.TimeUp/TQuantum);
            pputs(&conio,buf,2,trm);
         }
         else cputs(RangeError,ctrm);

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of mission timing parameters.",                                ctrm);
         cputs("?  Print this menu.",                                               ctrm);
         cputs("Ta Ascent time-out period. [1-600] (" TUnits ")",                   ctrm);
         cputs("Tc Time-of-day for expiration of down-time [0-1439] (" TUnits ").", ctrm);
         cputs("Td Down time (0-30 days] (" TUnits ").",                            ctrm);
         cputs("Tj Deep-profile descent time. [0-480] (" TUnits ").",               ctrm);
         cputs("Tk Park descent time. (0-480] (" TUnits ").",                       ctrm);
         cputs("Tp Mission prelude. (0-360] (" TUnits ").",                         ctrm);
         cputs("Tu Up time (0-1440] (" TUnits ").",                                 ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* agent to exercise modem functions                                      */
/*------------------------------------------------------------------------*/
static void ModemAgent(void)
{
   unsigned char key; char buf[80],fwrev[32],imei[16],iccid[32],model[16];
   float dbars; int debug,verbosity;

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm); key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* query the LBT for its signal strength */
      case 'b':
      {
         dbars=0;
         cputs("Acquiring","");
         ModemEnable(19200);
         if ((dbars=IrSignalStrength(&modem))>=0)
         {
            snprintf(buf,sizeof(buf)," LBT signal strength: %0.1f bars.",dbars/10);
            pputs(&conio,buf,2,trm);
         }
         else cputs(" :Failed.",ctrm);
         ModemDisable();
         break;
      }

      /* configure the modem */
      case 'c':
      {
         /* save current debuglevel and reset for maximum verbosity */
         verbosity=debugbits; debugbits=5;
         cputs("Configuring the Modem.",ctrm);
         ModemEnable(19200); IrModemConfigure(&modem); ModemDisable();
         debugbits=verbosity;
         break;
      }

      /* query modem's firmware revision */
      case 'f':
      {
         ModemEnable(19200);
         if (IrModemFwRev(&modem,fwrev,sizeof(fwrev))>0)
         {
            snprintf(buf,sizeof(buf),"Modem FwRev: %s",fwrev);
            pputs(&conio,buf,2,trm);
         }
         else {cputs("Attempt to query the modem for its firmware revision failed.",ctrm);}

         ModemDisable();
         break;
      }

      /* gateway mode */
      case 'g':
      {
         /* notify entery into modem gateway mode */
         cputs("Opening modem gateway.",ctrm);

         /* enter into modem gateway mode */
         ModemGateWay();

         break;
      }

      /* query modem's IMEI number */
      case 'i':
      {
         ModemEnable(19200);
         if (IrModemImei(&modem,imei,sizeof(imei))>0)
         {
            snprintf(buf,sizeof(buf),"Modem IMEI: %s",imei);
            pputs(&conio,buf,2,trm);
         }
         else {cputs("Attempt to query the modem for its IMEI number failed.",ctrm);}

         ModemDisable();
         break;
      }

      case 'l':
      {
         /* stack-based objects to contain iridium geolocation */
         float x,y,z,lat,lon; unsigned long int t; int err;

         cputs("Query LBT for geolocation data.",ctrm);

         ModemEnable(19200);

         if (!(err=IrGeoLoc(&modem,&x,&y,&z,&lon,&lat,&t)))
         {
            cputs("Iridium geolocation data not available.",ctrm);
         }

         else if (err<0) {snprintf(buf,sizeof(buf),"Attempt to query the modem for geolocation "
                                   "data failed. [err=%d]\n",err); pputs(&conio,buf,2,trm);}

         ModemDisable(); break;
      }

      /* query modem's model */
      case 'm':
      {
         ModemEnable(19200);
         if (IrModemModel(&modem,model,sizeof(model))>0)
         {
            snprintf(buf,sizeof(buf),"Modem model: %s",model);
            pputs(&conio,buf,2,trm);
         }
         else {cputs("Attempt to query the modem for its model failed.",ctrm);}

         ModemDisable();
         break;
      }

      /* attempt to register the LBT with the Iridium system */
      case 'r':
      {
         ModemEnable(19200);
         debug=debugbits; debugbits=5;
         cputs("Attempting to register LBT with the Iridium system.",ctrm);
         if (IrModemRegister(&modem)>0) {cputs("Iridium registration successful.",ctrm);}
         else  {cputs("Iridium registration failed.",ctrm);}
         debugbits=debug;
         ModemDisable();
         break;
      }

      /* query for the SIM card's ICCID number */
      case 's':
      {
         ModemEnable(19200);
         if (IrModemIccid(&modem,iccid,sizeof(iccid))>=0)
         {
            snprintf(buf,sizeof(buf),"Modem ICCID: %s",iccid);
            pputs(&conio,buf,2,trm);
         }
         else {cputs("Attempt to query the modem for its ICCID number failed.",ctrm);}

         ModemDisable();
         break;
      }

      /* execute download/upload telemetry test */
      case 't':
      {
         cputs("Executing download/upload test.",ctrm);

         /* activate the modem serial port */
         ModemEnable(19200);

         /* register the modem with the iridium system */
         if (IrModemRegister(&modem)>0)
         {
            /* local objects needed for telemetry test */
            FILE *dest; const char *vitals="TelemetryTest.vitals";

            /* download the mission configuration from the remote host */
            DownLoadMissionCfg(&modem,2);

            /* write vital statistics */
            if ((dest=fopen(vitals,"w"))) {WriteVitals(dest,vitals); fclose(dest);}

            PrepareUpload(2);

            /* upload logs and profiles to the remote host */
            UpLoad(&modem,2);

            /* remove the test file */
            if (dest) {remove(vitals);}

            /* logout of the remote host */
            logout(&modem);
         }
         else {cputs("Iridium modem registration failed.",ctrm);}

         /* deactivate the modem serial port */
         ModemDisable();

         break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of modem functions.",                           ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("Hb LBT signal strength (bars).",                     ctrm);
         cputs("Hc Configure the modem.",                            ctrm);
         cputs("Hf Query modem's firmware revision.",                ctrm);
         cputs("Hg Enter modem gateway mode.",                       ctrm);
         cputs("Hi Query modem's IMEI number.",                      ctrm);
         cputs("Hl Query LBT for geolocation.",                      ctrm);
         cputs("Hm Query modem's model.",                            ctrm);
         cputs("Hr Register the LBT with the Iridium system.",       ctrm);
         cputs("Hs Query SIM card's ICCID number.",                  ctrm);
         cputs("Ht Execute download/upload telemetry test.",         ctrm);
      }
   }

   /* disable the LBT interface */
   ModemDisable();
}

/*------------------------------------------------------------------------*/
/* implement a gateway from the console to the modem                      */
/*------------------------------------------------------------------------*/
/**
   This function implements a gateway between the console and the modem.
*/
static void ModemGateWay(void)
{
   unsigned char byte,ibyte; int cr,n;
   const char *trm = "\r\n";

   /* define the current time and the time-out period */
   time_t To=time(NULL), TimeOut=120;

   /* pet the watch dog and flush the console IO buffers */
   conio.ioflush();

   /* notify entery into modem gateway mode */
   cputs("Opening modem gateway.  Hit <ESC> key to exit.",trm);

   /* polling loop to ferry traffic between modem and the console */
   if (ModemEnable(19200)>0 && modem.ioflush()>0 && Wait(500))
   {
      if (chat(&modem,"AT E0\r","OK",4,"\r")>0 && pputs(&modem,"AT",1,"\r")>0) while (1)
      {
         /* check for input from the console */
         if ((byte=kbdhit())>0)
         {
            /* break out of the polling loop */
            if (byte==ESC) {cputs(" ",trm); break;}

            /* transfer the byte to modem and restart the timeout period */
            modem.putb(byte); Wait(5); To=time(NULL);
         }

         /* check for input from modem */
         for (cr=0,n=0; n<100 && modem.getb(&ibyte)>0; n++)
         {
            /* filter out all control characters except CR */
            if (iscntrl(ibyte) && ibyte!=CR && ibyte!=BKSP) continue;

            /* eliminate extraneous CRs */
            if (ibyte==CR && cr>0) {cr=0; continue;}
            else if (ibyte==CR) cr++;
            else if (!iscntrl(ibyte)) cr=0;

            /* echo the byte to the console */
            if (ibyte==CR) putch(LF); else putch(ibyte);

            /* reset the timeout reference */
            To=time(NULL);
         }

         /* exit gateway mode on console disconnection or timeout */
         if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
         {
            cputs(" (timeout)",trm); break;
         }
      }

      /* announce failure to enter gateway mode */
      else {cputs("Modem failed to enable.",trm);}

      /* announce exit from gateway mode */
      cputs("Exiting modem gateway.",trm);

      /* flush the console's and modem' IO buffers */
      conio.ioflush(); modem.ioflush(); ModemDisable();
   }
}

/*------------------------------------------------------------------------*/
/* agent to facilitate current states                                     */
/*------------------------------------------------------------------------*/
static void RegisterInspectionAgent(void)
{
   unsigned char key;

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm);
         key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* report Stm32 register contents */
      case 'a': {cputs("GPIO Register A:",ctrm);             GpioRegMap(GPIOA,stdout); break;}
      case 'b': {cputs("GPIO Register B:",ctrm);             GpioRegMap(GPIOB,stdout); break;}
      case 'c': {cputs("GPIO Register C:",ctrm);             GpioRegMap(GPIOC,stdout); break;}
      case 'd': {cputs("GPIO Register D:",ctrm);             GpioRegMap(GPIOD,stdout); break;}
      case 'e': {cputs("GPIO Register E:",ctrm);             GpioRegMap(GPIOE,stdout); break;}
      case 'f': {cputs("GPIO Register F:",ctrm);             GpioRegMap(GPIOF,stdout); break;}
      case 'g': {cputs("GPIO Register G:",ctrm);             GpioRegMap(GPIOG,stdout); break;}
      case 'h': {cputs("Alternate function register:",ctrm); AfioRegMap(stdout);       break;}
      case 'p': {cputs("Power (PWR) register:",ctrm);        PwrRegMap(stdout);        break;}
      case 'r': {cputs("RCC register:",ctrm);                RccRegMap(stdout);        break;}
      case 't': {cputs("RTC register:",ctrm);                RtcRegMap(stdout);        break;}
      case 'u': {cputs("UART1 register:",ctrm);              Usart1RegMap(stdout);     break;}
      case 'v': {cputs("UART2 (CTD) register:",ctrm);        CtdRegMap(stdout);        break;}
      case 'x': {cputs("EXTI register:",ctrm);               ExtiRegMap(stdout);       break;}
      case 'y': {cputs("SDIO register:",ctrm);               SdioRegMap(stdout);       break;}
      case '1': {cputs("ADC1 Register:",ctrm);               AdcRegMap(ADC1,stdout);   break;}
      case '2': {cputs("ADC2 Register:",ctrm);               AdcRegMap(ADC2,stdout);   break;}
      case '3': {cputs("ADC3 Register:",ctrm);               AdcRegMap(ADC3,stdout);   break;}

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu for inspection of STM32 registers.",            ctrm);
         cputs("?  Print this menu.",                                ctrm);
         cputs("a  GPIOA register.",                                 ctrm);
         cputs("b  GPIOB register.",                                 ctrm);
         cputs("c  GPIOC register.",                                 ctrm);
         cputs("d  GPIOD register.",                                 ctrm);
         cputs("e  GPIOE register.",                                 ctrm);
         cputs("f  GPIOF register.",                                 ctrm);
         cputs("g  GPIOG register.",                                 ctrm);
         cputs("h  Alternate function register.",                    ctrm);
         cputs("p  Power (PWR) register.",                           ctrm);
         cputs("r  RCC register.",                                   ctrm);
         cputs("t  RTC register.",                                   ctrm);
         cputs("u  USART1 register.",                                ctrm);
         cputs("v  USART2 (CTD) register.",                          ctrm);
         cputs("x  EXTI register.",                                  ctrm);
         cputs("y  SDIO register.",                                  ctrm);
         cputs("1  ADC1 register.",                                  ctrm);
         cputs("2  ADC2 register.",                                  ctrm);
         cputs("3  ADC3 register.",                                  ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* agent to exercise the SBE41 ctd                                        */
/*------------------------------------------------------------------------*/
/**
   This function implements a user interface for exercising functions of the
   Sbe41cp ctd.
*/
static void Sbe41cpAgent(void)
{
   /* define stack-based objects for local use */
   unsigned char key; char fwbuild[32],fwvariant[32],buf[80];
   int major,minor,micro,sbe,errcode; float p,t,s;

   /* define error message for  out-of-range entries */
   cc *SbeError = "ERROR: Attempt to query SBE41cp failed.";

   /* define the current time and the time-out period */
   const time_t To=time(NULL), TimeOut=60;

   /* flush the console's Rx buffer */
   conio.ioflush();

   /* wait for the next keyboard-hit */
   while ((key=kbdhit())<=0)
   {
      /* arrange for the APF11 to be put in hibernate mode */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",ctrm); key=CR; break;
      }
   }

   /* acknowledge key entry by spacing over */
   if (key!=CR) conio.putb(' ');

   switch (tolower(key))
   {
      /* activate CP mode */
      case 'a':
      {
         cputs("Activating SBE41cp's CP mode.",ctrm);
         if (Sbe41cpStartCP(1)<=0) cputs("SBE41 CP activation failed.",ctrm);
         break;
      }

      /* bin-average CP data */
      case 'b':
      {
         cputs("Executing SBE41cp's bin-average command.",ctrm);
         if (Sbe41cpBinAverage()<=0) cputs("Bin-average command failed.",ctrm);
         break;
      }

      /* display the SBE41cp calibration coefficients */
      case 'c':
      {
         /* give some user feedback */
         cputs("SBE41cp calibration coefficients.",ctrm);

         /* display the coefficents */
         if (Sbe41cpLogCal()<0) {cputs("Query failed.",ctrm);}

         break;
      }

      /* deactivate CP mode */
      case 'd':
      {
         cputs("Deactivating SBE41cp's CP mode.",ctrm);
         if (Sbe41cpStopCP()<=0) cputs("SBE41 CP deactivation failed.",ctrm);
         break;
      }

      /* display firmware revision */
      case 'f':
      {
         /* query the SBE41CP for firmware revision */
         if ((errcode=Sbe41cpFwRev(&major,&minor,&micro,fwvariant,fwbuild,sizeof(fwbuild)))>0)
         {
            snprintf(buf,sizeof(buf),"SBE41cp firmware revision[%d.%d.%d], build[%s], variant[%s]",
                      major,minor,micro,fwbuild,fwvariant);
            pputs(&conio,buf,2,trm);
         }

         /* create a message describing the failure */
         else
         {
            snprintf(buf,sizeof(buf),"SBE41cp firmware revision "
                      "query failed. [errcode: %d]",errcode);
            pputs(&conio,buf,2,trm);
         }

         break;
      }

      /* gateway mode */
      case 'g':
      {
         /* notify entery into sbe41cp gateway mode */
         cputs("Opening Sbe41cp gateway.",ctrm);

         /* enter into sbe41cp gateway mode */
         Sbe41cpGateWay();

         break;
      }

      /* configure the Sbe41cp sensor */
      case 'k':
      {
         /* give some user feedback */
         cputs("Configuring SBE41cp.",ctrm);

         /* configure the SBE41CP */
         Sbe41cpConfig(2); Sbe41cpTsWait(20);

         break;
      }

      /* measure the voltage and current while the SBE41cp pump is running */
      case 'm':
      {
         /* define local objects needed for power measurements */
         unsigned short V,A; float v,a,w;

         /* give the user some feedback */
         cputs("Measuring SBE41cp power consumption.",ctrm);

         /* measure the power consumption while the SBE41cp is running */
         if (CtdPower(&V,&A,60)>0)
         {
            /* convert counts to engineering units */
            a=CtdAmps(A), v=BatVolts(V), w=a*v;

            /* create the report */
            snprintf(buf,sizeof(buf),"SBE41cp Power consumption: "
                     "%0.3fVolts * %0.3fAmps = %0.2fWatts.",v,a,w);
            pputs(&conio,buf,2,trm);
         }
         else cputs(SbeError,ctrm);

         break;
      }

      /* get the SBE41cp serial number */
      case 'n':
      {
         /* query the SBE41cp for its serial number */
         sbe = Sbe41cpSerialNumber();

         if (sbe>0)
         {
            snprintf(buf,sizeof(buf),"SBE41cp serial number: %d",sbe);
            pputs(&conio,buf,2,trm);
         }
         else cputs(SbeError,ctrm);

         break;
      }

      /* SBE41cp quick-pressure measurement */
      case 'p':
      {
         if (Sbe41cpGetP(&p)>0)
         {
            snprintf(buf,sizeof(buf),"SBE41cp pressure: %1.2f decibars",p);
            pputs(&conio,buf,2,trm);
         }
         else cputs(SbeError,ctrm);

         break;
      }

      /* SBE41cp full-STP sample */
      case 's':
      {
         if (Sbe41cpGetPts(&p,&t,&s)>0)
         {
            snprintf(buf,sizeof(buf),"SBE41cp P,T,S: "
                     "%7.2f decibars, %7.4fC, %7.4fPSU",p,t,s);
            pputs(&conio,buf,2,trm);
         }
         else cputs(SbeError,ctrm);

         break;
      }

      /* SBE41cp low-power TP sample */
      case 't':
      {
         if (Sbe41cpGetPt(&p,&t)>0)
         {
            snprintf(buf,sizeof(buf),"SBE41cp P,T:   "
                     "%7.2f decibars, %7.4fC",p,t);
            pputs(&conio,buf,2,trm);
         }
         else cputs(SbeError,ctrm);

         break;
      }

      /* upload CP data */
      case 'u':
      {
         cputs("Executing SBE41cp commands to upload CP data.",ctrm);
         if (Sbe41cpUploadCP(stdout)<=0) cputs("Attempt to upload SBE41 CP data failed.",ctrm);
         break;
      }

      /* power cycle the Sbe41cp */
      case 'z':
      {
         cputs("Power cycling the Sbe41cp...",ns);
         CtdPowerCycle(3);
         cputs("done.",ctrm); break;
      }

      /* print the menu */
      case '?': cputs("",ctrm);

      /* the default response is to print the menu */
      default:
      {
         cputs("Menu of SBE41cp functions.",                       ctrm);
         cputs("?  Print this menu.",                              ctrm);
         cputs("Sa Activate CP mode.",                             ctrm);
         cputs("Sb Bin-average CP data.",                          ctrm);
         cputs("Sc Display the SBE41cp calibration coefficients.", ctrm);
         cputs("Sd Deactivate CP mode.",                           ctrm);
         cputs("Sf Display SBE41cp firmware revision.",            ctrm);
         cputs("Sg Enter SBE41cp gateway mode.",                   ctrm);
         cputs("Sk Configure the SBE41cp.",                        ctrm);
         cputs("Sm Measure power consumption by SBE41cp.",         ctrm);
         cputs("Sn Display SBE41cp serial number.",                ctrm);
         cputs("Sp Get SBE41cp pressure.",                         ctrm);
         cputs("Ss Get SBE41cp P T & S.",                          ctrm);
         cputs("St Get SBE41cp P & T (low-power).",                ctrm);
         cputs("Su Upload CP data.",                               ctrm);
         cputs("Sz Power cycle the Sbe41cp.",                      ctrm);
      }
   }
}

/*------------------------------------------------------------------------*/
/* implement a gateway from the console to Sbe41cp                        */
/*------------------------------------------------------------------------*/
/**
   This function implements a gateway between the console and the Sbe41cp.
*/
static void Sbe41cpGateWay(void)
{
   unsigned char byte,ibyte; int cr,n;
   const char *trm = "\r\n";

   /* define the current time and the time-out period */
   time_t To=time(NULL), TimeOut=120;

   /* flush the console IO buffers */
   conio.ioflush();

   /* notify entery into sbe41cp gateway mode */
   cputs("Opening Sbe41cp gateway.  Hit <ESC> key to exit.",trm);

   /* polling loop to ferry traffic between sbe41cp and the console */
   if (Sbe41cpEnterCmdMode()>0 && ctdio.ioflush()>0) for(;;)
   {
      /* check for input from the console */
      if ((byte=kbdhit())>0)
      {
         /* break out of the polling loop */
         if (byte==ESC) {Sbe41cpExitCmdMode(); break;}

         /* transfer the byte to sbe41cp and restart the timeout period */
         ctdio.putb(byte); Wait(5); To=time(NULL);
      }

      /* check for input from sbe41cp */
      for (cr=0,n=0; n<100 && ctdio.getb(&ibyte)>0; n++)
      {
         /* filter out all control characters except CR */
         if (iscntrl(ibyte) && ibyte!=CR && ibyte!=BKSP) continue;

         /* eliminate the double-echo caused by the gateway topology */
         if (ibyte==byte) {byte=-1; continue;}

         /* eliminate extraneous CRs */
         if (ibyte==CR && cr>0) {cr=0; continue;}
         else if (ibyte==CR) cr++;
         else if (!iscntrl(ibyte)) cr=0;

         /* echo the byte to the console */
         if (ibyte==CR) putch(LF); else putch(ibyte);

         /* reset the timeout reference */
         To=time(NULL);
      }

      /* exit gateway mode on console disconnection or timeout */
      if (!ConioActive() || difftime(time(NULL),To)>TimeOut)
      {
         cputs(" (timeout)",trm); Sbe41cpExitCmdMode(); break;
      }
   }

   /* announce failure to enter gateway mode */
   else {cputs("Sbe41cp is not responding.",trm);}

   /* announce exit from gateway mode */
   cputs("Exiting Sbe41cp gateway.",trm);

   /* flush the console's and sbe41cp' IO buffers */
   conio.ioflush(); ctdio.ioflush();
}

/*------------------------------------------------------------------------*/
/* function to print the mission parameters to the console                */
/*------------------------------------------------------------------------*/
/**
   This function writes the mission parameters to the console.
*/
static void ShowParameters(void)
{
   char buf[80];
   const char tunits[]="("TUnits")";
   const char minutes[]="(Minutes)";
   const char seconds[]="(Seconds)";
   const char decibars[]="(Decibars)";
   const char counts[]="(Counts)";
   const char enable[] ="ENABLE";
   const char disable[]="DISABL";

   #define print(buf) pputs(&conio,buf,2,trm);

   /* compute the signature of the mission program */
   mission.crc = Crc16Bit((unsigned char *)(&mission), sizeof(mission)-sizeof(mission.crc));

   /* write the firmware revision and the float id to the console */
   snprintf(buf,sizeof(buf),"FwRev: %08lx", FwRev); print(buf);
   snprintf(buf,sizeof(buf),"FwDate: %s", __DATE__); print(buf);
   snprintf(buf,sizeof(buf),"ApfId: %05u", mission.FloatId); print(buf);
   snprintf(buf,sizeof(buf),"User: %-41s",mission.user); print(buf);
   if (ShowPwd)
   {
      snprintf(buf,sizeof(buf),"Pwd: %s  0x%04x",mission.pwd,
               Crc16Bit((unsigned char *)mission.pwd,strlen(mission.pwd)));
      print(buf); ShowPwd=0;
   }
   else
   {
      snprintf(buf,sizeof(buf),"Pwd: 0x%04x",
                Crc16Bit((unsigned char *)mission.pwd,strlen(mission.pwd)));
      print(buf);
   }

   snprintf(buf,sizeof(buf),"Pri: %-45s Mhp", mission.at); print(buf);
   snprintf(buf,sizeof(buf),"Alt: %-45s Mha", mission.alt); print(buf);

   snprintf(buf,sizeof(buf),"%6s uSD file system. %-26s Mw", ((mission.FatFsEnable)?enable:disable),"(FAT32)"); print(buf);

   if (inRange(0,mission.ToD,Day))
   {
      snprintf(buf,sizeof(buf),"  %04ld ToD for down-time expiration. %-12s  Mtc",mission.ToD/TQuantum,tunits); print(buf);
   }
   else
   {
      snprintf(buf,sizeof(buf),"INACTV ToD for down-time expiration. %-12s  Mtc",tunits); print(buf);
   }

   snprintf(buf,sizeof(buf)," %05ld Down time. %-10s                       Mtd",  mission.TimeDown/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Up time. %-10s                         Mtu",  mission.TimeUp/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Ascent time-out. %-10s                 Mta",  mission.TimeOutAscent/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Deep-profile descent time. %-10s       Mtj",  mission.TimeDeepProfileDescent/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Park descent time. %-10s               Mtk",  mission.TimeParkDescent/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Mission prelude. %-10s                 Mtp",  mission.TimePrelude/TQuantum,tunits); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Telemetry retry interval. %-10s        Mhr",  mission.TimeTelemetryRetry/Min,minutes); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Host-connect time-out. %-10s           Mht",  mission.ConnectTimeOut,seconds); print(buf);
   snprintf(buf,sizeof(buf)," %05ld ZModem time-out. %-10s                 Mhz",  mission.ZModemTimeOut,seconds); print(buf);
   snprintf(buf,sizeof(buf)," %5.0f Continuous profile activation. %-10s   Mc",   mission.PressureCP,decibars); print(buf);
   snprintf(buf,sizeof(buf)," %5.0f Park pressure. %-10s                   Mk",   mission.PressurePark,decibars); print(buf);
   snprintf(buf,sizeof(buf)," %5.0f Deep-profile pressure. %-10s           Mj",   mission.PressureProfile,decibars); print(buf);
   snprintf(buf,sizeof(buf)," %5.0f Activation Pressure. %-10s             Map",  mission.ActivationPressure,decibars); print(buf);
   snprintf(buf,sizeof(buf)," %05ld Activation Pressure Period %-10s       Mpp",  mission.ActivationPressurePeriod,seconds); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Park piston position. %-10s            Mbp", mission.PistonParkPosition,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Compensator hyper-retraction. %-10s    Mbh", mission.PistonParkHyperRetraction,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Deep-profile piston position. %-10s    Mbj", mission.PistonDeepProfilePosition,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Ascent buoyancy nudge. %-10s           Mbn", mission.PistonBuoyancyNudge,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Initial buoyancy nudge. %-10s          Mbi", mission.PistonInitialBuoyancyNudge,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Initial descent nudge. %-10s           Mdi", mission.PistonInitialDescentNudge,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Park-n-profile cycle length. %-10s     Mn",  mission.PnpCycleLength,ns); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Maximum air bladder pressure. %-10s    Mfb", mission.MaxAirBladder,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u OK vacuum threshold. %-10s             Mfv", mission.OkVacuumCount,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u Piston full extension. %-10s           Mff", mission.PistonFullExtension,counts); print(buf);
   snprintf(buf,sizeof(buf),"   %04u P-Activation piston position. %-10s    Mfs", mission.PistonPActivatePosition,counts); print(buf);
   snprintf(buf,sizeof(buf)," %5u Logging verbosity. [0-5]                    D", debuglevel); print(buf);
   snprintf(buf,sizeof(buf),"  %04x DebugBits.                                  D",debugbits); print(buf);
   snprintf(buf,sizeof(buf),"   %3u EMA Sample. %-10s                      Mes", mission.EMASample,ns); print(buf);
   snprintf(buf,sizeof(buf),"   %3u EMA Min Velocity. %-10s                Mmv", mission.EMAMinVelocity,ns); print(buf);
   snprintf(buf,sizeof(buf),"  %04x Mission signature (hex).",                    mission.crc); print(buf);

   #undef print
}

#endif /* CMDS_C */
