#ifndef GPS_H
#define GPS_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define gpsChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <stdio.h>
#include <nmea.h>

/* prototypes for functions with external linkage */
int GpsServices(FILE *dest);
int TransmitGpsFix(const struct SerialPort *modem);
time_t UnixTime(const struct NmeaGpsFields *gps);

/* external declarations of persistent GPS objects */
extern persistent time_t AlmanacTimeStamp;
extern persistent struct NmeaGpsFields GpsFix;

#endif /* GPS_H */
#ifdef GPS_C
#undef GPS_C

#include <apf11.h>
#include <apf11rf.h>
#include <config.h>
#include <control.h>
#include <eeprom.h>
#include <garmin.h>
#include <logger.h>
#include <math.h>
#include <nan.h>
#include <nmea.h>
#include <Stm32f103Rtc.h>
#include <string.h>
#include <unistd.h>

/* object to record the time-stamp of the most recent almanac */
persistent time_t AlmanacTimeStamp;

/* object to record the most recent GPS fix */
persistent struct NmeaGpsFields GpsFix;

/*------------------------------------------------------------------------*/
/* function to manage and execute GPS services                            */
/*------------------------------------------------------------------------*/
/**
   This function manages functionality associated with acquiring GPS fixes,
   maintaining a current almanac in the GPS, and correcting APF11 clock
   skew.  This function returns a positive value if a fix was acquired.  A
   zero or negative return value indicates failure.
*/
int GpsServices(FILE *dest)
{
   /* define the logging signature */
   cc *FuncName = "GpsServices()";

   /* initialize the return value */
   int status=0;

   /* define objects used when acquiring GPS fix */
   int i=0; const int NRetry=2;

   /* define objects used when acquiring GPS fix */
   time_t To,dT,GpsTime,timeout=300; struct NmeaGpsFields gpsfix;

   /* define the almanac upload triggers */
   const time_t MaxAlmanacAge=90*Day, MinAlmanacAge=45*Day; 

   /* compute the current age of the almanac */
   time_t AlmanacAge=difftime(time(NULL),AlmanacTimeStamp);
      
   /* initialize the mag-switch flip-flop */
   MagSwitchReset();
 
   /* power-up the GPS engine */
   GpsEnable(4800); if (gps.ioflush) gps.ioflush();

   /* check if the GPS almanac has aged-out */
   if (AlmanacAge<0 || AlmanacAge>MaxAlmanacAge) 
   {
      if (debuglevel>=2 || (debugbits&GPS_H))
      {
         LogEntry(FuncName,"Replacing aged (%ld days) almanac.\n",AlmanacAge/Day);
      }

      /* acquire new almanac */
      if (GpsAlmanacUpLoad(&gps)>0)
      {
         /* time-stamp the new almanac */
         AlmanacTimeStamp=time(NULL);

         if (debuglevel>=2 || (debugbits&GPS_H))
         {
            LogEntry(FuncName,"New almanac acquired.\n");
         }
      }
      else {LogEntry(FuncName,"GPS almanac acquisition failed.\n");}
   }
   else if (debuglevel>=2 || (debugbits&GPS_H)) 
   {
      LogEntry(FuncName,"GPS almanac is current.\n");
   }
   
   if (debuglevel>=2 || (debugbits&GPS_H)) 
   {
      LogEntry(FuncName,"Initiating GPS fix acquisition.\n");
   }

   /* retry loop for GPS fix acquisition */
   for (status=0,dT=0,To=time(NULL),i=1; i<=NRetry && status<=0; i++)
   {
      /* get the GPS fix */
      if (GetGpsFix(&gps,&gpsfix,timeout)>0)
      {
         /* store the current fix for later use */
         GpsFix=gpsfix;
         
         /* indicate success in acquiring a fix */
         status=1; dT=(time_t)difftime(time(NULL),To);
            
         /* write the GPS fix data to the data file */
         if (dest) fprintf(dest,"# GPS fix obtained in %ld seconds.\n"
                           "#    %8s %7s %2s/%2s/%4s %6s %4s\n"
                           "Fix: %8.3f %7.3f %02d/%02d/%04d %06ld %4d\n",
                           dT,"lon","lat","mm","dd","yyyy","hhmmss","nsat",
                           gpsfix.lon,gpsfix.lat,gpsfix.mon,gpsfix.day,
                           gpsfix.year,gpsfix.hhmmss,gpsfix.nsat);
      
         /* store the fix-acquisition time in engineering data */
         vitals.GpsFixTime=dT;
         
         /* write the GPS fix data to the logfile */
         if (debuglevel>=2 || (debugbits&GPS_H)) 
         {
            /* write the fix to the log file */
            LogEntry(FuncName,"Profile %d GPS fix obtained in %ld seconds.\n",PrfIdGet(),dT);
            LogEntry(FuncName,"     %8s %7s %2s/%2s/%4s %6s %4s\n",
                     "lon","lat","mm","dd","yyyy","hhmmss","nsat");
            LogEntry(FuncName,"Fix: %8.3f %7.3f %02d/%02d/%04d %06ld %4d\n",
                     gpsfix.lon,gpsfix.lat,gpsfix.mon,gpsfix.day,gpsfix.year,
                     gpsfix.hhmmss,gpsfix.nsat);
         }
      }
      else if (MagSwitchToggled())
      {
         LogEntry(FuncName,"GPS fix interrupted by swipe of mag-switch.\n");
         break;
      }
      
      else if (i<NRetry)
      {
         dT=(time_t)difftime(time(NULL),To);

         LogEntry(FuncName,"GPS fix not acquired after %lds; power-cycling the GPS.\n",dT);
          
         /* power-cycle the GPS engine */
         GpsDisable(); sleep(2); GpsEnable(4800); if (gps.ioflush) gps.ioflush();
      }
   }

   /* check for successful GPS acquisition */
   if (status>0)
   {
      /* get GPS time to protect against clock skew */
      if ((GpsTime=GetGpsTime(&gps,60))>0)
      {
         /* compute the clock skew */
         time_t RtcSkew=difftime(time(NULL),GpsTime);

         /* store the clock skew in engineering data */
         vitals.RtcSkew=RtcSkew;

         /* check if clock skew should be corrected */
         if (fabs(RtcSkew)>Min/2)
         {
            LogEntry(FuncName,"Excessive RTC skew (%lds) detected.  "
                     "Resetting Apf11's RTC to %s",RtcSkew,ctime(&GpsTime));
               
            /* reset the Apf11's RTC */
            if (RtcSet(GpsTime)>0)
            {
               time_t T=time(NULL);
               LogEntry(FuncName,"Apf11's RTC now reads %s",ctime(&T));
            }
            else {LogEntry(FuncName,"Apf11 RTC skew correction failed.\n");}
         }
         else if (debuglevel>=2 || (debugbits&GPS_H)) 
         {
            LogEntry(FuncName,"APF11 RTC skew (%lds) OK.\n",RtcSkew); 
         }
      }
      else {LogEntry(FuncName,"Apf11 RTC skew check aborted.\n");}
      
      /* check criteria for replacement of almanac based on acquisition performance */
      if (dT>timeout && AlmanacAge>MinAlmanacAge)
      {
         if (debuglevel>=2 || (debugbits&GPS_H))
         {
            LogEntry(FuncName,"Replacing almanac to speed future "
                     "fix-acquisition.\n",AlmanacAge/Day);
         }

         /* acquire new almanac */
         if (GpsAlmanacUpLoad(&gps)>0)
         {
            /* time-stamp the new almanac */
            AlmanacTimeStamp=time(NULL);

            if (debuglevel>=2 || (debugbits&GPS_H))
            {
               LogEntry(FuncName,"New almanac acquired.\n");
            }
         }
         else {LogEntry(FuncName,"GPS almanac acquisition failed.\n");}
      }
   }
   
   else
   {
      LogEntry(FuncName,"GPS fix-acquisition failed after %lds.  "
               "Apf11 RTC skew check by-passed.\n",dT);
      
      /* note the failed attempt in the data file */
      if (dest) fprintf(dest,"\n# Attempt to get GPS fix failed "
                        "after %lds.\n",dT);
   }

   /* check criteria for logging the nmea sentences */
   if (debuglevel>2 || (debugbits&GPS_H) || dT>=3*Min) LogNmeaSentences(&gps);
   
   /* power-down the GPS engine */
   GpsDisable();

   if (debuglevel>=2 || (debugbits&GPS_H)) 
   {
      LogEntry(FuncName,"GPS services complete.\n");
   }
   
   /* initialize the mag-switch flip-flop */
   MagSwitchReset();
 
   return status;
}

/*------------------------------------------------------------------------*/
/* function to transmit a command to record the most recent fix           */
/*------------------------------------------------------------------------*/
/**
   This function transmits a command to the remote host that is
   intended to record the most recent fix.  The command is 'gpsfix'
   which must be in the PATH or else an alias for commands that will
   store the GPS location string.  The following alias uses standard
   unix utilities to store the fix and the timestamp of its receipt by
   the remote host: 
      alias gpsfix 'echo -n `date -u`>> gpsfixes.txt; \
                    echo " \!*" >> gpsfixes.txt;      \
                    echo gpsfix'
   
   \begin{verbatim}
   input:
      modem......A structure that contains pointers to machine dependent
                 primitive IO functions.  See the comment section of the
                 SerialPort structure for details.  The function checks to
                 be sure this pointer is not NULL.

   output:
      This function returns a positive value if the command was
      successfully written to the serial port.  Zero is returned if
      the most recent fix is not available. A negative return value
      indicates an exception was encountered.
   \end{verbatim}
*/
int TransmitGpsFix(const struct SerialPort *modem)
{
   /* function name for log entries */
   cc *FuncName = "TransmitGpsFix()";

   /* initialize the return value */
   int status=0;

   /* stack-based objects used locally */
   char buf[128]; const time_t Tref=time(NULL), TimeOut=30;
   
   /* validate the port */
   if (!modem) {LogEntry(FuncName,"NULL serial port.\n"); status=-1;}

   /* check for nonexistent fix */
   else if (Finite(GpsFix.lon) && Finite(GpsFix.lat))
   {
      /* create the command string to execute on the remote host */
      snprintf(buf,sizeof(buf),"gpsfix \"%8.3f %7.3f %02d/%02d/%04d %06ld %4d\"",
               GpsFix.lon,GpsFix.lat,GpsFix.mon,GpsFix.day,GpsFix.year,
               GpsFix.hhmmss,GpsFix.nsat);

      /* transmit the gpsfix command to the remote host */
      pputs(modem,buf,3,"\r\n");

      /* implement timeout loop */
      while (difftime(time(NULL),Tref)<TimeOut)
      {
         /* get the next response from the remote host */
         if (pgets(modem,buf,sizeof(buf),5,"\r\n")>0)
         {
            /* check for the command echo */
            if (strstr(buf,"gpsfix")) {status=1; break;}
         }
      }
   }

   /* log the nonexistent fix */
   else if (debuglevel>=3 || (debugbits&GPS_H))
   {
      LogEntry(FuncName,"Most recent fix not available.\n");
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to compute time of fix relative to the unix epoch             */
/*------------------------------------------------------------------------*/
/**
   This function computes the time of the GPS fix relative to the Unix epoch.

      \begin{verbatim}
      input:
         gpsfix ... A NmeaGpsFields object that contains the GPS fix.  
      
      output:
         This function returns the time represented in the NmeaGpsFields
         object expressed as the number of seconds since the Unix epoch
         (00:00:00 Jan 1, 1970).  If this object is invalid or if the time
         represented is invalid then this function returns a negative
         number.
      \end{verbatim}
*/
time_t UnixTime(const struct NmeaGpsFields *gpsfix)
{
   /* define the logging signature */
   cc *FuncName = "UnixTime()";

   /* initialize the return value */
   time_t GpsSec=-1;

   /* validate the function argument */
   if (!gpsfix) {LogEntry(FuncName,"NULL function parameter.\n");}

   /* validate the date */
   else if (gpsfix->year<2000 || gpsfix->year>2100 ||
            gpsfix->mon<1     || gpsfix->mon>12    ||
            gpsfix->day<1     || gpsfix->day>31    ||
            gpsfix->hhmmss<0  || gpsfix->hhmmss>235959)
   {
      LogEntry(FuncName,"Invalid GPS date/time: %02d/%02d/%04d %06ld\n",
               gpsfix->mon,gpsfix->day,gpsfix->year,gpsfix->hhmmss);
   }
   
   else
   {
      /* compute the decimal encoded hours and minutes */
      long int hhmm=gpsfix->hhmmss/100;

      /* define a tm structure to contain the broken down date and time */
      struct tm date={0,0,0,0,0,0,0,0,0};

      /* initialize the elements of the broken down time */
      date.tm_sec=gpsfix->hhmmss-hhmm*100;
      date.tm_hour=hhmm/100;
      date.tm_min=hhmm-date.tm_hour*100;
      date.tm_mday=gpsfix->day;
      date.tm_mon=gpsfix->mon-1;
      date.tm_year=gpsfix->year-1900;
      
      /* compute the number of seconds in the epoch */
      GpsSec=mktime(&date);
   }
   
   return GpsSec;
}

#endif /* GPS_C */
