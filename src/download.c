#ifndef DOWNLOAD_H
#define DOWNLOAD_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define downloadChangeLog "$RCSfile$  $Revision$  $Date$"
 
#include <serial.h>

int DownLoadMissionCfg(const struct SerialPort *modem,int NRetries);
extern const char* MISSION_CONFIG_TMP;

#endif /* DOWNLOAD_H */
#ifdef DOWNLOAD_C
#undef DOWNLOAD_C

#include <cd.h>
#include <chat.h>
#include <clogin.h>
#include <config.h>
#include <errno.h>
#include <logger.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <zmodem.h>

#if defined (__arm__)
   #include <apf11.h>
   #include <apf11rf.h>
#endif /* __arm__ */

/* prototypes for external functions that are used locally */
long int crprocheader(const char *header);
int      TxBreak(const struct SerialPort *com,int millisec);

/* define the command to download via xmodem */
static const char *szcmd = "sz";
      
/* create a temporary file name to download the config file */
const char* MISSION_CONFIG_TMP = "RxConfig.tmp";

/* define the time-out period for CD detection */
#define CdTimeOut (3.0)

/*------------------------------------------------------------------------*/
/* function to download the mission configuration from the remote host    */
/*------------------------------------------------------------------------*/
/**
   This function is designed to download the mission configuration file from
   the remote host.  The strategy for dealing with poor or broken
   connections with the remote host is to allow for multiple retries of the
   download session.  Breaks in the connection are automatically detected,
   the connection re-established, and the download reinitiated.  Attempts to
   download the configuration file from the remote host continue until the
   download is successful or else the number of retries is exhausted.

      \begin{verbatim}
      input:
         modem......A structure that contains pointers to machine dependent
                    primitive IO functions.  See the comment section of the
                    SerialPort structure for details.  The function checks to
                    be sure this pointer is not NULL.

      output:
         If the configuration file was successfully downloaded, this
         function returns a positive number indicating the attempts required
         to download the configuration file.  Zero is returned if the
         configuration file was not downloaded.  A negative return value
         indicates either an invalid serial port or unimplemented modem
         carrier-detect functionality.
      \end{verbatim}
*/
int DownLoadMissionCfg(const struct SerialPort *modem,int NRetries)
{
    /* function name for log entries */
    cc *FuncName = "DownLoadMissionCfg()";
    const time_t timeout = 30;
    static char cmd[64];
    static char response[64];

    /* initialize return value */
    int i, status = -1;
    ZModem* zmodem;

    /* validate the port & make the logentry */
    if (!modem)
    {
        LogEntry(FuncName, "NULL serial port.\n");
    }
    else
    {
        status = 0;
        for (i = 1; i <= NRetries; i++)
        {
            if (CLogin(modem) <= 0)
            {
                ModemDtrClear();
                sleep(5);
                LogEntry(
                    FuncName,
                    "Failed mission config download attempt %u. Login failure...\n",
                    i);
                continue;
            }

            if (modem->iflush)
            {
                modem->iflush();
            }

            snprintf(cmd, sizeof(cmd) - 1, "%s %s\n", szcmd, config_path);
            snprintf(response, sizeof(response) - 1, "%s %s\r\n", szcmd, config_path);
            if (chat(modem, cmd, response, timeout, NULL) <= 0)
            {
                ModemDtrClear();
                sleep(5);
                LogEntry(
                    FuncName,
                    "Failed mission config download attempt %u. Failed to initiate sz command...\n",
                    i);
                continue;
            }

            zmodem = ZModemInit(modem, NULL, NULL);
            zmodem = ZModemTimeOut(zmodem, mission.ZModemTimeOut);
            if (zRx(zmodem) <= 0)
            {
                ZModemCleanUp(zmodem);
                ModemDtrClear();
                sleep(5);
                LogEntry(
                    FuncName,
                    "Failed mission config download attempt %u. ZModem Failed...\n",
                    i);
                continue;
            }

            LogEntry(
                FuncName,
                "Download successful: [bytes:%ld, sec:%0.1f, bps:%0.0f]\n",
                zmodem->nbytes,
                zmodem->etime,
                (zmodem->etime > 0) ? (zmodem->nbytes / zmodem->etime) : 0);

            if (zmodem->fp != NULL)
            {
                fclose(zmodem->fp);
            }

            status = 1;
            if (status > 0 && !strcmp(zmodem->fname, MISSION_CONFIG_TMP))
            {
                if (remove(config_path) < 0)
                {
                    LogEntry(
                        FuncName,
                        "Attempt to delete \"%s\" failed [errno=%d].\n",
                        config_path,
                        errno);
                }

                if (rename(MISSION_CONFIG_TMP, config_path) < 0)
                {
                    LogEntry(
                        FuncName,
                        "Attempt to rename \"%s\" file to \"%s\" failed [errno=%d].\n",
                        MISSION_CONFIG_TMP,
                        config_path,
                        errno);
                    status = 0;
                }

                remove(MISSION_CONFIG_TMP);
            }

            break;
        }
    }

    /* make the logentry */
    if (status == 0)
    {
        LogEntry(
            FuncName,
            "Aborting download: Retry limit [%u] exceeded.\n",
            NRetries);
    }

    return status > 0 ? i : status;
}

/*------------------------------------------------------------------------*/
/* process a ZModem Rx header                                             */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function processes a ZModem Rx
   header that contains the name of the file to receive and (optional)
   metadata that reveals the size and date of the file.  The header
   starts with the file name which is NULL-terminated.  The header
   continues with a metadata string that lists the (decimal) number of
   bytes in the file followed by white space followed by an octal
   number that representst the modification time of the file.  The
   metadata string is created by the following C-snippet:

      struct stat filestat; fstat(fileno(fp), &filestat);
      sprintf(header, "%lu %lo", filestat.st_size, filestat.st_mtime);

   This function ignores the metadata; it reads the header, opens the
   file in write-mode, positions the file pointer to the end of the
   file, and returns zero.

   \begin{verbatim}
   input:
      header.....The ZModem Rx header that contains the file name and
                 metadata as described above.

   output:
      This function returns zero, on success.  A return value of -1
      indicates an error and will cause the file transfer to be
      skipped.
   \end{verbatim}
*/
long int crprocheader(const char *header)  
{
   /* define the logging signature */
   cc *FuncName = "crprocheader()";

   /* initialize the return value */
   long int n=-1;
   
   /* validate the ZModem Rx header */
   if (header && header[0])
   {
      /* test if the file is the mission.cfg file */
      if (!strcmp(header,config_path))
      {
         /* download mission.cfg using a temporary name */
         strncpy(zmodem.fname,MISSION_CONFIG_TMP,sizeof(zmodem.fname));
      }
      
      /* copy the file name to the ZModem structure */
      else strncpy(zmodem.fname,header,sizeof(zmodem.fname));

      /* open the file for writing */
      if ((zmodem.fp=fopen(zmodem.fname,"w"))) {n=0;}

      /* log the fopen() failure */
      else {LogEntry(FuncName,"Attempt to open \"%s\" in "
                     "write-mode failed. [errno=%d]\n",
                     zmodem.fname,errno);}
   }
   else {LogEntry(FuncName,"Invalid ZModem Rx header.\n");}

   return n;
}

/*------------------------------------------------------------------------*/
/* function to execute a break signal on the Modem's serial port          */
/*------------------------------------------------------------------------*/
/*
   This function executes a Tx-break signal on the Modem's serial port
   for a specified period of time.

   \begin{verbatim}
   input:
      millisec...This specifies the period of time (millisec) that the
                 break signal is maintained.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int TxBreak(const struct SerialPort *com,int millisec)
{
   /* define the logging signature */
   cc *FuncName = "TxBreak()";

   /* initialize the return value */
   int status=RfNull;

   /* check if break-signal on modem requested */
   if (com==(&modem)) {ModemTxBreak(millisec);}

   /* report that break-signal not implemented on the specified port */
   else {LogEntry(FuncName,"Unimplemented on com=<%04lx>\n",com);}

   return status;
}

#endif /* DOWNLOAD_C */
