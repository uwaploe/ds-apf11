#ifndef CONTROL_H
#define CONTROL_H (0x01f00U)

/*========================================================================*/
/* Design notes for float control firmware                                */
/*========================================================================*/
/**
   The APFx firmware design makes fundamental use of the concept of
   "sequence points" for controlling the flow of the profile cycle.  A
   sequence point is defined to be a point where one phase of the mission
   cycle transitions to the next phase.  Most of the sequence points are
   based on time but there are several sequence points that are event-based.
   Given a properly functioning APFx controller, the firmware guarantees the
   phase transition at each sequence point regardless of the health of any
   other float component.

   The schematics below illustrate four different parts of the mission
   cycle:

      \begin{verbatim}
      1. The pressure-activation phase.
      2. The mission prelude.
      3. A profile from the park level.
      4. A deep profile.
      \end{verbatim}

   Pressure-activation phase
   -------------------------

   The pressure-activation feature is an optional phase of the mission.  It
   was designed to accomodate requests from ship's crew to be able to deploy
   the float without being required to start it with a magnet.  One
   event-based sequence points is implemented that activates the mission
   prelude if the pressure exceeds the activation threshold.
       
   \begin{verbatim}
     |--------------------------- A -----------------------------------|
    -+----------------+-------------+----------------------------------+- Time
    P|                 .           .                                    .    
    r|                   .        .       Sequence Points                 .   
    e|                      .    .        -----------------------           .  
    s|                      B   .         B = Pressure-activation              .
    s|                                    
    u|
    r|
    e|
   \end{verbatim}

   
   Mission prelude                                                        
   ---------------

   The mission prelude is the time period between mission activation and the
   first descent.  The sequence point 'L' is time-based and is the
   transition between the mission prelude and the first descent.  The period
   of the mission prelude is user-defined.
   
   \begin{verbatim}
     |--------------------------- L -----------------------------------|
    -+-----------------------------------------------------------------+- Time
    P|                                                                  .    
    r|                                                 Sequence Points   .   
    e|                                                 ---------------    .  
    s|                                                 L = Prelude          .
    s|                                                                       
    u|
    r|
    e|
   \end{verbatim}
   


   Profile from park depth
   -----------------------

   The profile cycle for a shallow profile consists of four phases.
   
   \begin{verbatim}
     |--------------------------------------------------- C ----------------|     
     |------------------------------ P ---------------|                     |     
     |----------------- K -------------|              |                     |     
     |--- F -----|                     |           S  |                T    |     
    -+-----------+---------------------+-----------+-------------------+----+- Time
    P|.          |                     |         .                      .    
    r| .         |                     |       .       Sequence Points   .   
    e|  .        |                     |     .         ---------------    .  
    s|    .      |                     |   .           F = Descent          .
    s|       .   |                     | .             K = Park             
    u|           . . . . . . . . . . . .               S = SurfaceDetect
    r|                                                 P = Profile           
    e|                                                 T = Telemetry
     |                                                 C = Cycle
   \end{verbatim}                    
   


   Deep profile
   ------------
   
   Test for deep profile:
   (PnpCycleLength<254 && ((!(PrfId%PnpCycleLength)) || PrfId==1) ? Yes : No;

   The profile cycle for a deep profile consists of five phases.
   
   \begin{verbatim}
     |------------------------------------------------------ C -------------|     
     |--------------------------------- P ------------|                     |     
     |----------------- D --------|                   |                     |     
     |------- K -------------|    |                   |                     |     
     |--- F -----|           |    |                S  |                T    |     
    -+-----------+-----------+----+----------------+-------------------+------- Time
    P|.          |           |    |              .                      .    
    r| .         |           |    |            .       Sequence Points   .   
    e|  .        |           |    |          .         ---------------    .  
    s|    .      |           |    |        .           F = Descent          .
    s|       .   |           |    |      .             K = Park                        
    u|           . . . . . . .    |    .               Q = DeepProfile 
    r|                            |  .                 D = GoDeep
    e|                        .   |.                   S = SurfaceDetect
     |                           .                     P = Profile
     |                         .                       T = Telemetry 
     |                         Q                       C = Cycle                             
   \end{verbatim}


   The internal diameter of the cylinder is 1.250(+0.004,-0.000)
   inches and the total gross stroke-length of the piston is L=31.8cm.
   Given that the radius of the piston is R=1.5875cm the total gross
   displacement of the buoyancy engine is V = Pi*L*R^2 = 251.8ml.  The
   pump's lead screw has 31 threads per 151mm which represents a pitch
   of 4.87mm/revolution or 0.775mm/radian.  The piston position at
   full retraction is 9 counts while at full extension is 231
   counts. The following schematic shows 3 important piston
   extensions.
     
   \begin{verbatim}
                    +---Full Extension (231 counts)    Full Retraction (9 counts)---+
                    |          Ballast piston position (16 counts)--+               |
                    |                                               |               |
                    V                                               V               V
     ---------------------------------------------------------------------------------------+
                                                                                            |
     -------------+---+-------------------------------------------+---+-----------+---+-----+
          +-------|   |-------------------------------------------|   |-----------|   |-----------+
         /|       |   |                                           |   |           |   |           |
     ---+ |       |   |                                           |   |           |   |           |
        | |       |   |                                           |   |           |   |           |
        | |     +-|   |-------------------------------------------|   |-----------|   |-+         |
        | |    (  |. .|                                           |. .|           |. .|  )        |
        | |     +-|   |-------------------------------------------|   |-----------|   |-+         |
        | |       +---+                                           +---+           +---+           |
     ---+ |       .   .                                               .               .           |
         \|       |   |                                               |               |           |
          +-------+---+-----------------------------------------------+---------------+-----------+
          .       |   |                                               |               |           .
       -->| 44mm  |<- |<------------------- 318mm ----------------------------------->|<- 56mm -->|
                                        (Total scope)                 |<----- 66mm -------------->|
   \end{verbatim}
*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define controlChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>
#include <profile.h>
#include <config.h>
#include <stdbool.h>

/*------------------------------------------------------------------------*/
/* structure to contain engineering data for a single profile             */
/*------------------------------------------------------------------------*/
struct EngineeringData
{
   #define        ParkDescentPMax 9
   unsigned char  ActiveBallastAdjustments;
   unsigned short AirBladderPressure;
   unsigned short AirPumpAmps;
   unsigned short AirPumpVolts;
   time_t         BuoyancyPumpOnTime;
   unsigned short BuoyancyPumpAmps;
   unsigned short BuoyancyPumpVolts;
   unsigned char  ConnectionAttempts;
   unsigned char  Connections;
   float          CoulombsDescent;
   float          CoulombsPark;
   float          CoulombsPrfDescent;
   float          CoulombsProfile;
   float          CoulombsTelemetry;
   time_t         GpsFixTime;
   unsigned short HumidityAdcPark;
   unsigned short HumidityAdcPneumatic;
   unsigned short HumidityAdcPneumaticRef;
   unsigned short ParkDescentP[ParkDescentPMax];
   unsigned short ParkDescentPCnt;
   char           ParkPOutOfBand;
   struct   Obs   ParkObs;
   unsigned int   ObsIndex;
   unsigned short QuiescentAmps;
   unsigned short QuiescentVolts;
   time_t         RtcSkew;
   unsigned short Sbe41cpAmps;
   unsigned long  Sbe41cpStatus;
   unsigned short Sbe41cpVolts;
   float          SleepSec;
   unsigned long  status;
   unsigned short SurfacePistonPosition;
   float          SurfacePressure;
   time_t         TimeStartDescent;
   time_t         TimeStartSink;
   time_t         TimeStartPark;
   time_t         TimeStartProfileDescent;
   time_t         TimeStartProfile;
   time_t         TimeStartTelemetry;
   time_t         TimeStopProfile;
   unsigned short Vacuum;
   float          WakeSec;
};

/* definition of the 'status' bits in the engineering data above  */
#define DeepPrf            0x00000001UL
#define ShallowWaterTrap   0x00000002UL
#define Obs25Min           0x00000004UL
#define PistonFullExt      0x00000008UL
#define AscentTimeOut      0x00000010UL
#define DownLoadCfg        0x00000020UL
#define BadSeqPnt          0x00000040UL
#define Sbe41cpPFail       0x00000200UL
#define Sbe41cpPtFail      0x00000400UL
#define Sbe41cpPtsFail     0x00000800UL
#define Sbe41cpPUnreliable 0x00001000UL
#define AirSysLimit        0x00002000UL
#define WatchDogAlarm      0x00004000UL
#define PrfIdOverflow      0x00008000UL

/* definition of the Sbe41cpStatus bits in the engineering data above */
#define Sbe41cpPPedanticExceptn    0x00000001UL
#define Sbe41cpPPedanticFail       0x00000002UL
#define Sbe41cpPRegexFail          0x00000004UL
#define Sbe41cpPNullArg            0x00000008UL
#define Sbe41cpPRegExceptn         0x00000010UL
#define Sbe41cpPNoResponse         0x00000020UL
#define Sbe41cpPUncaughtExceptn    0x00000040UL
#define Sbe41cpPDivPts             0x00000080UL
#define Sbe41cpPtPedanticExceptn   0x00000100UL
#define Sbe41cpPtPedanticFail      0x00000200UL
#define Sbe41cpPtRegexFail         0x00000400UL
#define Sbe41cpPtNullArg           0x00000800UL
#define Sbe41cpPtRegExceptn        0x00001000UL
#define Sbe41cpPtNoResponse        0x00002000UL
#define Sbe41cpPtUncaughtExceptn   0x00004000UL
#define Sbe41cpPtsPedanticExceptn  0x00008000UL
#define Sbe41cpPtsPedanticFail     0x00010000UL
#define Sbe41cpPtsRegexFail        0x00020000UL
#define Sbe41cpPtsNullArg          0x00040000UL
#define Sbe41cpPtsRegExceptn       0x00080000UL
#define Sbe41cpPtsNoResponse       0x00100000UL
#define Sbe41cpPtsUncaughtExceptn  0x00200000UL
#define Sbe41cpCpActive            0x10000000UL

/* define the various states that a float can be in */
enum State {UNDEFINED=-3, PACTIVATE, RECOVERY, INACTIVE, PRELUDE, DESCENT,
            PARK, GODEEP, PROFILE, TELEMETRY, EOS};

typedef enum {
    SELF_TEST_SUCCESS   = 0x0,
    E_DS2740            = 0x1,
    E_SD                = 0x2,
    E_FATMNT            = 0x4,
    E_FATUMNT           = 0x8,
    E_FIO               = 0x10,
    E_PISTON            = 0x20,
    E_PSI               = 0x40,
    E_CTD               = 0x80,
    E_CTDCFG            = 0x100,
    E_MODEM             = 0x200,
    E_MODEMCFG          = 0x400,
    E_GARMIN            = 0x800,
    E_STACK             = 0x1000,
    E_EMA               = 0x2000,
    E_MISSIONCFG        = 0x4000,
    E_EEPROM            = 0x8000,
    E_AIRBLADDER        = 0x10000,
    E_HUMIDITY          = 0x20000,
    E_LEAK_DETECTOR     = 0x40000,
    E_ABORT             = 0x80000
} SelfTestFlags;

/* function prototypes */
void StateGetString(enum State state, char outstate[], int maxlen);
int  DeepProfile(void);
void InitVitals(void);
void MissionControlAgent(void);
int  MissionLaunch(void);
int  MissionKill(void);
int  MissionValidate(void);
void PowerOff(time_t AlarmSec);
int  SelfTest(bool testSky);
void SequencePointsDisplay(void);
time_t SequencePointDownTime(void);
int  SequencePointsValidate(void);
time_t ToD(time_t Tref,time_t TimeDown, time_t DayMin);

/* store engineering data in persistent ram */
extern persistent struct EngineeringData vitals;

#endif /* CONTROL_H */
#ifdef CONTROL_C
#undef CONTROL_C

#include <apf11.h>
#include <apf11ad.h>
#include <apf11rf.h>
#include <conio.h>
#include <crc16bit.h>
#include <descent.h>
#include <diskio.h>
#include <ds2740.h>
#include <eeprom.h>
#include <ema.h>
#include <engine.h>
#include <fatio.h>
#include <garmin.h>
#include <godeep.h>
#include <inrange.h>
#include <lbt9523.h>
#include <logger.h>
#include <nan.h>
#include <nmea.h>
#include <pactivate.h>
#include <park.h>
#include <prelude.h>
#include <profile.h>
#include <recovery.h>
#include <sbe41cp.h>
#include <stdlib.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Pwr.h>
#include <Stm32f103Rtc.h>
#include <Stm32f103Sdio.h>
#include <stream.h>
#include <string.h>
#include <telemetry.h>
#include <unistd.h>

/* declare functions with external linkage */
int KbdEscRequest(void);

/*------------------------------------------------------------------------*/
/* structure to contain the time-based sequence points                    */
/*------------------------------------------------------------------------*/
persistent static struct
{
      time_t Descent;
      time_t Park;
      time_t GoDeep;
      time_t Profile;
      time_t Telemetry;
} SeqPoint;

/* define structure to contain engineering data */
persistent struct EngineeringData vitals;

/**
 * Get the state enum as a string passed out as an output parameter. The
 * buffer needs to be at least 10 characters.
 *
 * See StateGet() to acquire the state value.
 */
void StateGetString(enum State state, char outbuffer[], int maxlen) {
   char* states[] = {
      "UNDEFINED", // note this starts at -3
      "PACTIVATE",
      "RECOVERY",
      "INACTIVE",
      "PRELUDE",
      "DESCENT",
      "PARK",
      "GODEEP",
      "PROFILE",
      "TELEMETRY",
      "EOS"
   };

   // the +3 is to accommodate the -3 starting position
   snprintf(outbuffer, maxlen, states[state + 3]);
}

/*------------------------------------------------------------------------*/
/* function to test if the current profile is a deep profile              */
/*------------------------------------------------------------------------*/
/**
   This function tests for a deep profile based on the current profile
   number.   The PnP mode is enabled only if the cycle length is in the
   closed interval [1,253].  Set PnpCycleLength=254 in order to disable PnP
   mode.  If PnP mode is enabled then this function returns a positive value
   if the current profile is a deep profile; otherwise zero is returned.
   The first profile is a deep profile as well as all profile ids that are
   evenly divisible by PnpCycleLength.
*/
int DeepProfile(void)
{
   /* initialize return status */
   int status=0;

   /* get the current profile id */
   int PrfId=PrfIdGet();

   /* check if PnP mode is enabled */
   if (mission.PnpCycleLength>0 && mission.PnpCycleLength<254)
   {
      /* evaluate criteria for a deep profile */
      status=((!(PrfId%mission.PnpCycleLength)) || PrfId==1) ? 1 : 0;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the engineering data                            */
/*------------------------------------------------------------------------*/
void InitVitals(void)
{
   unsigned int i; unsigned char *b;

   /* initialize the engineering data to zeros */
   for (b=(unsigned char *)&vitals, i=0; i<sizeof(vitals); i++) {b[i]=0;}

   /* reset the watchdog and mag-switch latches */
   WatchDogLatchReset(); MagSwitchReset();
}

/*------------------------------------------------------------------------*/
/* main control loop and sequence point enforcement                       */
/*------------------------------------------------------------------------*/
/**
   This function constitutes the main control loop for detecting and
   enforcing phase transitions at sequence points.
*/
void MissionControlAgent(void)
{
   /* define the logging signature */
   cc *FuncName = "MissionControlAgent()";

   // the alarm time gets updated every cycle
   time_t alarm;

   // setup a loop as we might need to simply sleep() instead of powering down
   while(1) {

      /* get the sequence time */
      time_t SeqTime = itimer();

      /* define the default alarm time */
      alarm=itimer()+HeartBeat;

      /* get the current state of the mission */
      enum State state = StateGet();

      if (debuglevel > 3)
      {
         char statestr[10];
         StateGetString(state, statestr, sizeof(statestr));
         LogEntry(FuncName, "****\n");
         LogEntry(FuncName, "** Wake-up state: %s(%d); sequence time: %lds\n", statestr, state, SeqTime);
      }

      /* check if the watch-dog alarm indicates a power-down exception */
      if (WatchDogEvent()>0) vitals.status|=WatchDogAlarm;

      /* apply criteria for sequence point at the end of the mission prelude */
      if (state==PRELUDE && SeqTime>=mission.TimePrelude)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In prelude timeout [seq=%lds], term prelude and init descent\n", mission.TimePrelude);
         }

         /* initiate final telemetry before the mission starts */
         PreludeTerminate(); DescentInit();

         // note these initial values of the sequence points override the sequence points
         // computed in the SequencePointsValidate() (called by DescentInit())
         // in particular, it executes a very short park of only five mins on the first dive after prelude

        /* arrange for a profile immediately after the mission prelude */
        SeqPoint.Descent   = (mission.TimeParkDescent>5*Min) ? mission.TimeParkDescent : 5*Min;
        SeqPoint.Park      = SeqPoint.Descent + 5*Min;
        SeqPoint.GoDeep    = SeqPoint.Park + ((DeepProfile()>0) ? mission.TimeDeepProfileDescent : 0);
        SeqPoint.Profile   = SeqPoint.GoDeep + mission.TimeOutAscent;
        SeqPoint.Telemetry = SeqPoint.GoDeep + mission.TimeUp;
        if (debuglevel > 3)
        {
            LogEntry(FuncName, "** Post-prelude sequence points follow:\n");
            SequencePointsDisplay();
        }
      }

      /* apply criteria for sequence point at the end of the descent phase */
      else if (state==DESCENT && SeqTime>=SeqPoint.Descent)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In descent timeout [seq=%lds], term descent and init park\n", SeqPoint.Descent);
         }

         /* terminate the descent phase */
         DescentTerminate();

         /* initialize the park phase */
         ParkInit();
      }

      /* apply criteria for sequence point at the end of the park phase */
      else if (state==PARK && SeqTime>=SeqPoint.Park)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In park timeout [seq=%lds], term park and init go-deep / profile\n", SeqPoint.Park);
         }

         /* execute the tasks at the end of the park phase */
         ParkTerminate();

         /* sequence point depends whether this is a deep profile */
         (DeepProfile()>0) ? GoDeepInit() : ProfileInit();
      }

      /* apply criteria for sequence point at the end of the deep-descent phase */
      else if (state==GODEEP && SeqTime>=SeqPoint.GoDeep)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In go-deep timeout [seq=%lds], term go-deep(retract) and init profile\n", SeqPoint.GoDeep);
         }

         /* terminate the deep-descent phase */
         GoDeepTerminate(RETRACT);

         /* transition to the profile phase */
         ProfileInit();
      }

      /* apply criteria for sequence point at the end of the profile phase */
      else if (state==PROFILE && SeqTime>=SeqPoint.Profile)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In profile timeout [seq=%lds], term profile and init telemetry\n", SeqPoint.Profile);
         }

         /* make the logentry */
         LogEntry(FuncName,"Ascent time-out exceeded; aborting profile.\n");

         /* record that the ascent period timed out */
         vitals.status|=AscentTimeOut;

         /* record the piston position */
         vitals.SurfacePistonPosition = PistonPositionAdc();

         /* terminate the profile */
         ProfileTerminate();

         /* move the piston to full extension */
         PistonMoveAbs(mission.PistonFullExtension);

         /* transition to the telemetry phase */
         TelemetryInit();
      }

      /* apply criteria for sequence point at the end of the telemetry phase */
      else if (state==TELEMETRY && SeqTime>=SeqPoint.Telemetry)
      {
         if (debuglevel > 3)
         {
            LogEntry(FuncName, "** In telemetry timeout [seq=%lds], term telemetry and init descent\n", SeqPoint.Telemetry);
         }

         /* terminate the telemetry phase and initiate the descent phase */
         TelemetryTerminate(); DescentInit();
      }

      /* mission state might have changed; read from EEPROM */
      switch (StateGet())
      {
         /* sleep for 105 minutes at time in hibernate mode */
         case INACTIVE: {alarm=Apf11WakeTime()+WDogTimeOut; break;}

         case PACTIVATE:
         {
            /* execute the pressure-activation monitor */
            if (PActivate() > 0)
            {
                alarm = itimer() + HeartBeat;
            }
            else
            {
               alarm = itimer() + mission.ActivationPressurePeriod;
            }

            break;
         }

         case PRELUDE:
         {
            /* execute the regularly scheduled tasks for the mission prelude */
            Prelude(); SeqTime=itimer()+HeartBeat;

            /* compute the next regularly scheduled alarm-time */
            alarm = SeqTime + mission.TimeTelemetryRetry;

            /* ensure regularly scheduled alarm-time doesn't exceed the sequence point */
            if (alarm>mission.TimePrelude) alarm=mission.TimePrelude + HeartBeat;

            if (debuglevel > 3)
            {
               LogEntry(
                  FuncName,
                  "** In prelude handler, sleeping for: %lds (end = %lds)\n",
                  alarm - (SeqTime + HeartBeat), mission.TimePrelude
               );
            }

            break;
         }

         case DESCENT:
         {
            /* execute the regularly scheduled tasks for the descent phase */
            time_t sleep = Descent();

            // If we've gone past the park position, then we should just park
            // instead of continuing the dive.
            float pressure = 0.0;
            if (GetP(&pressure) <= 0)
            {
                LogEntry(FuncName, "Failed to get pressure reading from the CTD\n");
                pressure = 0.0;
            }

            if (pressure >= mission.PressurePark)
            {
                DescentTerminate();
                ParkInit();
                alarm = itimer() + HeartBeat;
            }
            else
            {
                SeqTime = itimer();

                // trust that the descent knows how to calculate the sleep timeout
                alarm = SeqTime + sleep;

                /* ensure scheduled alarm-time doesn't exceed the sequence point */
                if (alarm>SeqPoint.Descent) alarm=SeqPoint.Descent + HeartBeat;
            }

            if (debuglevel > 3)
            {
               LogEntry(
                  FuncName,
                  "** In descent handler, sleeping for: %lds (end = %lds)\n",
                  alarm - SeqTime, SeqPoint.Descent
               );
            }

            break;
         }

         case PARK:
         {
            /* execute the regularly scheduled tasks for the park phase */
            Park(); SeqTime=itimer();

            /* compute the next regularly scheduled alarm-time */
            alarm = SeqTime - SeqTime%Hour + Hour;

            /* ensure regularly scheduled alarm-time doesn't exceed the sequence point */
            if (alarm>SeqPoint.Park) alarm=SeqPoint.Park + HeartBeat;

            if (debuglevel > 3)
            {
               LogEntry(
                  FuncName,
                  "** In park handler, sleeping for: %lds (end = %lds)\n",
                  alarm - SeqTime, SeqPoint.Park
               );
            }

            break;
         }

         /* execute the regularly scheduled tasks for the profile-descent phase */
         case GODEEP:
         {
            /* execute the regularly scheduled tasks for the deep-descent phase */
            if (GoDeep()>0) {SeqTime=itimer()+HeartBeat; alarm = SeqTime - SeqTime%(5*Min) + (5*Min);}

            /* transition to profile phase was detected; start profile in one heartbeat */
            else alarm = (itimer()+HeartBeat);

            /* ensure regularly scheduled alarm-time doesn't exceed the sequence point */
            if (alarm>SeqPoint.GoDeep) alarm=SeqPoint.GoDeep + HeartBeat;

            if (debuglevel > 3)
            {
               LogEntry(
                  FuncName,
                  "** In godeep handler, sleeping for: %lds (end = %lds)\n", alarm - itimer(),
                  SeqPoint.GoDeep
               );
            }

            break;
         }

         /* execute the regularly scheduled tasks for the profile phase */
         case PROFILE:
         {
            /* execute the profile monitor */
            time_t sleep = Profile();
            SeqTime = itimer();

            // if we are em sampling then the profile phase might need to do piston adjustments
            // and in that case we can't afford to sleep even a heartbeat length
            if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_ASCENT) {
               alarm = SeqTime + sleep;

            } else {
               /* set the next alarm time based on Profile()'s recommended sleep period */
               alarm = SeqTime + ((sleep>=HeartBeat && sleep<=15*Min) ? sleep : HeartBeat);
            }

            /* ensure regularly scheduled alarm-time doesn't exceed the sequence point */
            if (alarm>SeqPoint.Profile) alarm=SeqPoint.Profile + HeartBeat;

            if (debuglevel > 3)
            {
               LogEntry(
                  FuncName,
                  "** In profile handler, sleeping for [rec=%lds]: %lds (end = %lds)\n",
                  sleep, alarm - SeqTime, SeqPoint.Profile
               );
            }

            break;
         }

         /* execute the regularly scheduled tasks for the telemetry phase */
         case TELEMETRY:
         {
            /* successful completion of telemetry is an event-based sequence point */
            if (Telemetry()>0)
            {
               TelemetryTerminate();
               DescentInit();
               alarm = itimer() + HeartBeat;
            }

            /* execute the regularly scheduled tasks for the telemetry phase */
            else
            {
               /* get the sequence time */
               SeqTime=itimer()+HeartBeat;

               /* compute the next regularly scheduled alarm-time */
               alarm = SeqTime + mission.TimeTelemetryRetry;

               /* ensure regularly scheduled alarm-time doesn't exceed the sequence point */
               if (alarm>SeqPoint.Telemetry) alarm=SeqPoint.Telemetry + HeartBeat;
            }

            if (debuglevel > 3)
            {
               // the telemetry() is always successful in simulation so the alarm is reset during descent init
               // this means the sleep duration is the same as the alarm time
               LogEntry(FuncName, "** In telemetry handler, sleeping for: %lds (end = %lds)\n", alarm, SeqPoint.Telemetry);
            }

            break;
         }

         case RECOVERY:
         {
            /* execute the regularly scheduled tasks for the recovery phase */
            Recovery();

            /* check if mission configuration was successfully downloaded */
            if (!(vitals.status&DownLoadCfg))
            {
               /* process the configuration file */
               configure(&mission,config_path,0);

               /* terminate the recovery mode */
               LogEntry(FuncName,"Terminating recovery mode.\n"); RecoveryTerminate(); alarm=itimer()+HeartBeat;
            }
            else
            {
               /* get the sequence time */
               SeqTime=itimer()+HeartBeat;

               /* compute the next regularly scheduled alarm-time */
               alarm = SeqTime + mission.TimeTelemetryRetry;
            }

            break;
         }

         /* confused state; restart the mission */
         case UNDEFINED: default:
         {
            /* create a logentry */
            LogEntry(FuncName,"Logic error - uncaught state[%d].  "
                     "Restarting mission.\n",StateGet());

            /* restart mission */
            PreludeInit(); alarm=itimer()+HeartBeat;
         }
      }

      // the power down requires at least five seconds which we might not be able to spare
      // so instead restart the loop after the alarm timer
      time_t sleepsec = alarm - itimer();
      if(sleepsec >= 5) {
         break;
      }

      // make sure we don't get stuck
      if(sleepsec < 0) {
         sleepsec = 0;
      }

      LogEntry(FuncName, "** Sleeping for %d sec instead of powering down.\n", sleepsec);
      sleep(sleepsec);
   }

   /* set the alarm and go to sleep */
   PowerOff(alarm);
}

/*------------------------------------------------------------------------*/
/* function to launch a new mission                                       */
/*------------------------------------------------------------------------*/
/**
   This function launches the float on a new mission.
*/
int MissionLaunch(void)
{
   /* define the logging signature */
   cc *FuncName = "MissionLaunch()";

   /* initialize the return value */
   int status=0;

   /* initialize the charge consumption model */
   ChargeModelInit();

   /* reset the magnetic reset-switch */
   MagSwitchReset();

   /* create a log file name */
   snprintf(log_path,sizeof(log_path),"%05u.000.log",mission.FloatId);

   /* format the file system and open the log file */
   LogClose(); fioClean(); fformat(); LogOpen(log_path,'w'); LogPredicate(1);

   /* run the self-test before the mission */
   if (SelfTest(true /* testSky */)<=0)
   {
      /* make the logentry */
      LogEntry(FuncName,"Self-test failed - aborting mission.\n");

      /* kill the mission */
      MissionKill();
   }

   else
   {
      /* make the logentry */
      LogEntry(FuncName,"Self-test passed - initiating mission prelude.\n");

      /* initialize the mission prelude */
      status=PreludeInit();
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to kill the current mission                                   */
/*------------------------------------------------------------------------*/
/**
   This function kills the current mission.
*/
int MissionKill(void)
{
   StateSet(PACTIVATE);

   /* execute a command to stop any active SBE41cp profile */
   if (vitals.Sbe41cpStatus&Sbe41cpCpActive) Sbe41cpStopCP();

   /* read the mission parameters from EEPROM */
   if (!MissionParametersRead(&mission))
   {
      /* copy the default mission to the active mission */
      mission=DefaultMission;
   }

   /* make sure the air valve is open */
   AirValveOpen();

   return 1;
}

/*------------------------------------------------------------------------*/
/* compute the mission signature to validate the mission stored in RAM    */
/*------------------------------------------------------------------------*/
/**
   This function computes the mission signature and compares it to the
   mission stored in RAM in order to validate the mission configuration.
   If the computed signature matches that stored in RAM then the mission is
   valid.  If the match fails then the mission is read from EEPROM.  If the
   mission stored in EEPROM is invalid then a default mission is assigned.

   This function returns a positive number on success and zero otherwise.
*/
int MissionValidate(void)
{
   /* define the logging signature */
   cc *FuncName = "MissionValidate()";

   int status=1;

   /* compute the mission signature */
   unsigned int crc = Crc16Bit((unsigned char *)(&mission), sizeof(mission)-sizeof(mission.crc));

   /* check if the computed signature matches the signature stored in the mission object */
   if (crc!=mission.crc)
   {
      /* make the logentry */
      LogEntry(FuncName,"Reading mission program from EEPROM.\n");

      /* read mission parameters from EEPROM */
      if (!MissionParametersRead(&mission))
      {
         /* make the logentry */
         LogEntry(FuncName,"Failed --- no mission stored in EEPROM.\n");

         /* indicate that the mission could not be read from EEPROM */
         status=0;
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to properly power-down the Apf11 controller                   */
/*------------------------------------------------------------------------*/
void PowerOff(time_t AlarmSec)
{
   /* check if the piston movement was aborted with a magent swipe */
   if (MagSwitchToggled())
   {
      int i; time_t To=time(NULL);

      /* pause (for up to 5s) until the mag-switch is low for 500ms */
      do {for (i=0; i<5; i++) {Wait(100); if (MagSwitchHigh()) break;}}
      while (i<5 && difftime(time(NULL),To)<5);

      /* re-initialize the mag-switch flip-flop */
      MagSwitchReset();
   }

   /* ensure that all buffered streams are closed */
   fclose_streams();

   /* make sure the FatFs volume is unmounted */
   if (fat_ismount()) {fat_umount();}

   /* compute the number of seconds elapsed during the current wake-cycle */
   vitals.WakeSec+=IntervalTimerDiff(IntervalTimerTics(),Stm32RtcTicsAtWake());

   /* power down the APFx controller */
   Apf11PowerOff(AlarmSec);
}

static int CheckKeyBoardEscapeRequest(const char* funcName)
{
    /* check for keyboard escape request */
    if (KbdEscRequest() > 0)
    {
        LogEntry(funcName, "Request detected to abort the self test.\n");
        return E_ABORT;
    }

    return SELF_TEST_SUCCESS;
}

static int AirBladderSelfTest(const char* funcName)
{
    unsigned short volts;
    unsigned short amps;

    /* open the air-valve so that self-test measures internal vacuum */
    if (AirValveOpen() <= 0)
    {
        LogEntry(funcName, "FAILED: Could not open the air valve\n");
        return E_AIRBLADDER;
    }

    /* run the air pump for 1 second as an audible startup signal */
    if (AirPumpRun(1, &volts, &amps) <= 0)
    {
        LogEntry(funcName, "FAILED: Could not run the air pump\n");
    }

    /* save the air pump current and volts */
    vitals.AirPumpVolts = volts;
    vitals.AirPumpAmps = amps;

    LogEntry(
        funcName,
        "Current power consumption of the air pump: [%d %0.5fV], [%d %0.5fa]\n",
        volts,
        BatVolts(volts),
        amps,
        BatAmps(amps));

    /* open the air-valve again and wait a few seconds for bladder to deflate */
    if (AirValveOpen() <= 0)
    {
        LogEntry(funcName, "FAILED: Could not re-open the air valve.\n");
        return E_AIRBLADDER;
    }

    sleep(10);
    LogEntry(funcName, "PASSED: Air Bladder Self Test\n");
    return SELF_TEST_SUCCESS;
}

static int MissionConfigurationSelfTest(const char* funcName)
{
    if (ConfigSupervisor(&mission) != 0)
    {
        LogEntry(funcName, "FAILED: Mission configuration is invalid!\n");
        return E_MISSIONCFG;
    }

    LogEntry(funcName, "PASSED: Mission configuration passed config checks.\n");
    return SELF_TEST_SUCCESS;
}

static int EEPromSelfTest(const char* funcName)
{
    /* check the EEPROM state */
    if (StateGet() == UNDEFINED)
    {
        LogEntry(
            funcName,
            "FAILED: Unable to read the mission state from the EEPROM\n");
        return E_EEPROM;
    }

    LogEntry(funcName, "PASSED: Read the mission state from EEPROM.\n");
    return SELF_TEST_SUCCESS;
}

static int DS2740SelfTest(const char* funcName)
{
    uint32_t serno;
    float charge, amps, volts;
    uint16_t group;
    uint8_t family;
    uint8_t crc;

    /* query the DS2740's family and 48-bit serial number */
    if (DowNetAddress(&serno, &group, &family, &crc) <= 0)
    {
        LogEntry(funcName,"FAILED: Attempt to query Coulomb counter failed\n.");
        return E_DS2740;
    }

    LogEntry(funcName, "Successfully queried the coulomb counter\n");
    LogEntry(funcName,
        "Apf11 Family: 0x%02x Group:0x%04hx SerNo:0x%08lx Crc:0x%02x "
        "SeqNo:%03lu\n",
        family,
        group,
        serno,
        crc,
        serno % 1000);

    charge = BatCharge(ADC);
    amps = BatAmps(ADC);
    volts = BatVolts(ADC);

    if (isNaN(charge) || isInf(charge))
    {
        LogEntry(
            funcName,
            "FAILED: Invalid, under, or overflow of charge reading\n");
        return E_DS2740;
    }

    if (isNaN(amps) || isInf(amps))
    {
        LogEntry(
            funcName,
            "FAILED: Invalid, under, or overflow of amps reading\n");
        return E_DS2740;
    }

    if (isNaN(volts) || isInf(volts))
    {
        LogEntry(
            funcName,
            "FAILED: Invalid, under, or overflow of volts reading\n");
        return E_DS2740;
    }

    LogEntry(
        funcName,
        "PASSED: Successfully read power vitals: [%0.5fC, %0.5fa, %0.5fV]\n",
        charge,
        amps,
        volts);

    return SELF_TEST_SUCCESS;
}

static int LeakDetectorSelfTest(const char* funcName)
{
    uint16_t leakCount = LeakAdc();
    if (leakCount == 0xfff || leakCount == 0xffe)
    {
        LogEntry(funcName, "FAILED: Could not read the leak detector sensor.\n");
        return E_LEAK_DETECTOR;
    }

    LogEntry(
        funcName,
        "PASSED: Successfully read leak detector voltage: [%d, %0.5fV]\n",
        leakCount,
        LeakVolts(leakCount));
    return SELF_TEST_SUCCESS;
}

static int SDCardSelfTest(const char* funcName)
{
    int status = SELF_TEST_SUCCESS;

    // power-up the SDIO peripheral and detect if an SD card is installed
    // Check if FatFsDisable is set if the SD card is operational
    if (Stm32GpioAssert(GPIOB,SD_PWR_EN_Pin) <= 0 ||
        Stm32f103SdCardDetect() <= 0)
    {
        /* log the warning */
        LogEntry(
            funcName,
            "FAILED: SD card not detected; limited RAM file system storage.\n");
        return E_SD;
    }

    if (FatFsDisable)
    {
        /* log the warning */
        LogEntry(
            funcName,
            "FAILED: FatFs system is disabled; limited "
            "RAM file system storage.\n");
        return E_SD;
    }

    if (!fat_mount())
    {
        /* log the failure to mount the FatFs volume */
        LogEntry(
            funcName,
            "FAILED: FatFs volume mount failure. [FatRev %lu]\n",
            FF_DEFINED);
        return E_FATMNT;
    }

    /* define stack-based objects needed for FatFs metadata */
    DRESULT dresult;
    DWORD count, block;
    WORD size;

    /* report that FatFs volume mounted */
    LogEntry(
        funcName,
        "Successfully mounted the FatFs volume. [ChaN/ELM Rev %u]\n",
        FF_DEFINED);

    /* query the SD card for FatFs metadata */
    if ((dresult = disk_ioctl(0, GET_SECTOR_COUNT, &count)) != RES_OK)
    {
        LogEntry(
            funcName,
            "FAILED: Could not read sector count [FatFsErr=%d]\n",
            dresult);
        status |= E_SD;
    }

    if (status == SELF_TEST_SUCCESS &&
        (dresult = disk_ioctl(0, GET_SECTOR_SIZE, &size)) != RES_OK)
    {
        LogEntry(
            funcName,
            "FAILED: Could not read sector size [FatFsErr=%d]\n",
            dresult);
        status |= E_SD;
    }

    if (status == SELF_TEST_SUCCESS &&
        (dresult = disk_ioctl(0, GET_BLOCK_SIZE, &block)) != RES_OK)
    {
        LogEntry(
            funcName,
            "FAILED: Could not read block size [FatFsErr=%d]\n",
            dresult);
        status |= E_SD;
    }

    if (status == SELF_TEST_SUCCESS)
    {
        /* log the FatFs metadata */
        LogEntry(
            funcName,
            "Successfully read FatFs volume: size(%luMB) sector-size(%uB) "
            "block-size(%luB)\n",
            count / 2048,
            size,
            block);
    }

    /* unmount the FatFs volume */
    if (!fat_umount())
    {
        LogEntry(funcName, "FAILED: FatFs volume unmount failure.\n");
        status |= E_FATUMNT;
    }

    /* recondition the FatFs file system */
    if (fioClean() <= 0)
    {
        LogEntry(funcName,"Failed: FatFs volume failed to recondition.\n");
        status |= E_FIO;
    }

    if (status == SELF_TEST_SUCCESS)
    {
        LogEntry(funcName,"PASSED: SD-Card FatFs volume reconditioned.\n");
    }

    return status;
}

static int PistonSelfTest(const char* funcName)
{
    const time_t timeout = 30;
    unsigned short currentPosition = PistonPositionAdc();
    unsigned short newPosition = (currentPosition > (mission.PistonFullExtension / 2)) ?
        (currentPosition - 4) : (currentPosition + 4);

    /* execute a timed piston movement back and forth */
    if (PistonMoveAbsWTO(newPosition, NULL, NULL, timeout) <= 0)
    {
        LogEntry(
            funcName,
            "FAILED: Could not move piston from %d to %d in alloted timeout\n",
            currentPosition,
            newPosition);
        return E_PISTON;
    }

    if (PistonMoveAbsWTO(currentPosition, NULL, NULL, timeout) <= 0)
    {
        LogEntry(
            funcName,
            "FAILED: Could not move piston back to %d from %d in alloted timeout\n",
            currentPosition,
            newPosition);
        return E_PISTON;
    }

    LogEntry(
        funcName,
        "PASSED: Timed piston movements from %d to %d counts and back.\n",
        currentPosition,
        newPosition);

    return SELF_TEST_SUCCESS;
}

static int InternalVacuumSelfTest(const char* funcName)
{
    float actualPsi, expectedPsi;
    vitals.Vacuum = BarometerAdc();
    actualPsi = Barometer(vitals.Vacuum);
    expectedPsi = Barometer(mission.OkVacuumCount);
    if (vitals.Vacuum > mission.OkVacuumCount && mission.OkVacuumCount != 2024)
    {
        LogEntry(
            funcName,
            "FAILED: Internal pressure [%d, %0.1fpsi] exceeds threshold "
            "[%d, %0.1fpsi].\n",
            vitals.Vacuum,
            actualPsi,
            mission.OkVacuumCount,
            expectedPsi);
        return E_PSI;
    }

    LogEntry(
        funcName,
        "PASSED: Internal pressure [%d, %0.1fpsi] less than threshold "
        "[%d, %0.1fpsi].\n",
        vitals.Vacuum,
        actualPsi,
        mission.OkVacuumCount,
        expectedPsi);
    return SELF_TEST_SUCCESS;
}

static int HumiditySelfTest(const char* funcName)
{
    /* verify that the humidity sensor satisfies safe criteria */
    float humidity = Humidity(HumidityAdc());
    if (isNaN(humidity) && isInf(humidity))
    {
        LogEntry(funcName, "FAILED: Humidity sensor reading is invalid\n");
        return E_HUMIDITY;
    }

    if (humidity >= 75.0)
    {
        LogEntry(
            funcName,
            "FAILED: Relative humidity is above 75%%: %5.1f%%\n",
            humidity);
        return E_HUMIDITY;
    }

    LogEntry(
        funcName,
        "PASSED: Relative humidity is below 75%%: %5.1f%%\n",
        humidity);
    return SELF_TEST_SUCCESS;
}

static int CTDSelfTest(const char* funcName)
{
    const int FwMajor = 7, FwMinor = 2, FwMicro = 5;
    const char* FwVariant = "CP UW";
    const char* FwBuildDate = "Oct 14 2015 20:03:56";
    char fwbuild[32];
    char fwvariant[32];
    int sbe41cpSerno, major, minor, micro;
    float p;

    /* verify successful SBE41 communications */
    if (GetP(&p) <= 0)
    {
        LogEntry(funcName,"FAILED: Response not received from the CTD.\n");
        return E_CTD;
    }

    LogEntry(funcName,"Response received from the CTD.\n");

    /* query the Sbe41cp for its serial number */
    if ((sbe41cpSerno = Sbe41cpSerialNumber()) <= 0)
    {
        LogEntry(
            funcName,
            "FAILED: Query for CTD serial number failed.[errcode=%d]\n",
            sbe41cpSerno);
        return E_CTD;

    }

    LogEntry(funcName, "CTD SerNo: %05u\n", sbe41cpSerno);

    /* query the SBE41CP for its firmware revision */
    if (Sbe41cpFwRev(
            &major, &minor, &micro, fwvariant, fwbuild, sizeof(fwbuild)) <= 0)
    {
        LogEntry(funcName, "FAILED: Query for CTD FwRev failed.\n");
        return E_CTD;
    }

    if (major != FwMajor || minor < FwMinor)
    {
        LogEntry(
            funcName,
            "FAILED: CTD FwRev violation: [%d.%d.%d] != [%d.%d.%d] "
            "(expected).\n",
            major,
            minor,
            micro,
            FwMajor,
            FwMinor,
            FwMicro);
        return E_CTD;
    }

    if (minor != FwMinor || micro != FwMicro)
    {
        LogEntry(
            funcName,
            "Warning: CTD FwRev violation: [%d.%d.%d] != [%d.%d.%d] "
            "(expected).\n",
            major,
            minor,
            micro,
            FwMajor,
            FwMinor,
            FwMicro);
    }
    else
    {
        LogEntry(
            funcName,
            "CTD (SerNo %05u) FwRev accepted: [%d.%d.%d]\n",
            sbe41cpSerno,
            major,
            minor,
            micro);
    }

    /* verify the firmware build-date for the SBE41CP */
    if (!fwbuild[0])
    {
        LogEntry(
            funcName,
            "FAILED: Query for CTD firmware build-date failed.\n");
        return E_CTD;
    }

    if (strcmp(fwbuild, FwBuildDate))
    {
        LogEntry(
            funcName,
            "Warning: CTD firmware build-date violation: "
            "[%s] != [%s] (expected).\n",
            fwbuild,
            FwBuildDate);
    }
    else
    {
        LogEntry(
            funcName,
            "CTD (SerNo %05u) firmware build-date accepted: [%s]\n",
            sbe41cpSerno,
            fwbuild);
   }

    /* verify the firmware configuration variant for the SBE41CP */
    if (!fwvariant[0])
    {
        LogEntry(
            funcName,"FAILED: Query for CTD firmware variant failed.\n");
        return E_CTD;
    }

    if (strcmp(fwvariant, FwVariant))
    {
        LogEntry(
            funcName,
            "Warning: CTD firmware variant violation: "
            "[%s] != [%s] (expected).\n",
            fwvariant,
            FwVariant);
    }
    else
    {
        LogEntry(
            funcName,
            "CTD (SerNo %05u) firmware variant accepted: [%s]\n",
            sbe41cpSerno,
            fwvariant);
    }

        /* configure the SBE41CP */
    if (Sbe41cpConfig(2) <= 0 || Sbe41cpTsWait(20) <= 0)
    {
        LogEntry(
            funcName, "FAILED: Attempt to configure the CTD failed.\n");
        return E_CTDCFG;
    }


    LogEntry(funcName,"PASSED: CTD configuration was successful.\n");
    return SELF_TEST_SUCCESS;
}

static int EMSelfTest(const char* funcName)
{
    /* Check the EM board for a compass calibration */
    EMACompassCalibration* calibration =
        (EMACompassCalibration*)malloc(sizeof(EMACompassCalibration));
    if (calibration == NULL)
    {
        LogEntry(
            funcName, "FAILED: Out of heap space, aborting the self test...\n");
        return E_ABORT;
    }

    if (EMAGetCompassCalibration(calibration) <= 0)
    {
        LogEntry(
            funcName,
            "FAILED: Could not read the compass calibration from the EM\n");
        free(calibration);
        return E_EMA;
    }

    LogEntry(
        funcName, "PASSED: Received the compass calibration of the EM");
    LogAdd(" [CRC: %d]\n", calibration->crc);
    free(calibration);
    return SELF_TEST_SUCCESS;
}

static int ModemSelfTest(const char* funcName)
{
    const char* LbtModel = "9523";
    const char* LbtFwRev = "DB17011";
    char buf[64];

    /* power-up the LBT */
    ModemEnable(19200);

    /* check the LBT model */
    if (IrModemModel(&modem, buf, sizeof(buf)) <= 0)
    {
        LogEntry(funcName,"FAILED: Query for LBT modem model failed.\n");
        return E_MODEM;
    }

    if (strncmp(LbtModel, buf, strlen(LbtModel)))
    {
        LogEntry(
            funcName,
            "Warning: Unexpected LBT modem model: [%s] != [%s] (expected).\n",
            buf,
            LbtModel);
    }
    else
    {
        LogEntry(funcName, "LBT modem model: %s\n", buf);
    }

    /* check the LBT firmware revision */
    if (IrModemFwRev(&modem, buf, sizeof(buf)) <= 0)
    {
        LogEntry(
            funcName,
            "FAILED: Query for LBT modem firmware revision failed.\n");
        return E_MODEM;
    }

    if (strcmp(LbtFwRev, buf))
    {
        LogEntry(
            funcName,
            "Warning: Unexpected LBT modem firmare: [%s] != [%s] (expected).\n",
            buf,
            LbtFwRev);
    }
    else
    {
        LogEntry(funcName, "LBT modem firmware revision: %s\n", buf);
    }

    /* configure the LBT */
    if (IrModemConfigure(&modem) <= 0)
    {
        LogEntry(funcName, "FAILED: Attempt to configure the LBT failed.\n");
        return E_MODEMCFG;
    }
    else
    {
        LogEntry(funcName, "LBT modem configuration was successful.\n");
    }

    /* check the LBT IMEI */
    if (IrModemImei(&modem, buf, sizeof(buf)) <= 0)
    {
        LogEntry(funcName, "Warning: Query for LBT IMEI failed.\n");
    }
    else
    {
        /* make the log entry */
        LogEntry(funcName, "LBT IMEI: %s\n", buf);
    }

    /* query the SIM ICCID number */
    if (IrModemIccid(&modem, buf, sizeof(buf)) <= 0)
    {
        /* make the logentry */
        LogEntry(funcName, "Warning: Query for SIM card ICCID failed.\n");
    }
    else
    {
        /* make the log entry */
        LogEntry(funcName, "SIM card ICCID: %s\n", buf);
    }

    /* power-down the LBT */
    ModemDisable();

    LogEntry(funcName, "PASSED: LBT Modem Self Test\n");
    return SELF_TEST_SUCCESS;
}

static int GPSSelfTest(const char* funcName)
{
    // Time out in seconds (5 minutes) for a GPS operation
    const time_t timeout = 300;

    time_t rtcTime;
    struct NmeaGpsFields gpsfix;

    /* power-up the GPS */
    GpsEnable(4800);

    /* configure the GPS engine */
    if (ConfigGarmin15(&gps) <= 0)
    {
        LogEntry(funcName, "FAILED: Attempt to configure GPS failed.\n");
        return E_GARMIN;
    }

    LogEntry(
        funcName,
        "Attempting to get GPS RTC, timeout is %ld seconds\n",
        timeout);
    if ((rtcTime = GetGpsTime(&gps, timeout)) <= 0)
    {
        LogEntry(funcName, "FAILED: Attempt to get GPS RTC failed.\n");
        return E_GARMIN;
    }

    LogEntry(funcName, "GPS time: %s\n", ctime(&rtcTime));
    LogEntry(
        funcName,
        "Attempting to get GPS Fix, timeout is %ld seconds\n",
        timeout);
    if ((rtcTime = GetGpsFix(&gps, &gpsfix, timeout)) <= 0)
    {
        LogEntry(funcName, "FAILED: Attempt to get GPS fix failed.\n");
        return E_GARMIN;
    }

    LogEntry(funcName, "GPS Fix attained in %ld seconds.", rtcTime);
    LogEntry(
        funcName,
        "Fix Format: %8s %7s %2s/%2s/%4s %6s %4s\n",
        "lon",
        "lat",
        "mm",
        "dd",
        "yyyy",
        "hhmmss",
        "nsat");
    LogEntry(
        funcName,
        "Fix: %8.3f %7.3f %02d/%02d/%04d %06ld %4d\n",
        gpsfix.lon,
        gpsfix.lat,
        gpsfix.mon,
        gpsfix.day,
        gpsfix.year,
        gpsfix.hhmmss,
        gpsfix.nsat);

    /* power-down the GPS */
    GpsDisable();
    LogEntry(funcName, "PASSED: GPS Self Test\n");
    return SELF_TEST_SUCCESS;
}

static void ReportSelfTestResult(
    const char* funcName, const char* msg, int status, SelfTestFlags flag)
{
    LogEntry(
        funcName, "\t%s: %s\n", msg, (status & flag) ? "FAILED" : "PASSED");
}

/*------------------------------------------------------------------------*/
/* function to execute a self-test                                        */
/*------------------------------------------------------------------------*/
/**
   This function executes a self-test and returns a positive value if the
   self-test passed.  Zero is returned if the self-test failed.
*/
int SelfTest(bool testSky)
{
    /* define the logging signature */
    cc *FuncName = "SelfTest()";

    /* initialize the return value */
    int status = SELF_TEST_SUCCESS;

    /* set the maximum log size to 40kb */
    MaxLogSize = 40960L;

    /* make a logentry about self-test execution */
    LogEntry(
        FuncName,
        "Executing self-test for ApfId %05u [FwRev %08lx FwDate %s]\n",
        mission.FloatId,
        FwRev,
        __DATE__);

    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the Mission Configuration...\n");
    status |= MissionConfigurationSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the Air Bladder...\n");
    status |= AirBladderSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the EEProm External Memory Device...\n");
    status |= EEPromSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the DS2740 Coulomb Counter...\n");
    status |= DS2740SelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the Leak Detector Sensor...\n");
    status |= LeakDetectorSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the SD Card...\n");
    status |= SDCardSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the internal vacuum pressure...\n");
    status |= InternalVacuumSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the internal humidity...\n");
    status |= HumiditySelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the piston movement...\n");
    status |= PistonSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the CTD comms...\n");
    status |= CTDSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the EM comms...\n");
    status |= EMSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    LogEntry(FuncName, "Testing the Iridium Modem...\n");
    status |= ModemSelfTest(FuncName);
    if ((status |= CheckKeyBoardEscapeRequest(FuncName)) & E_ABORT)
    {
        goto Err;
    }

    if (testSky)
    {
        LogEntry(FuncName, "Testing GPS Services...\n");
        status |= GPSSelfTest(FuncName);
    }

    /* collection point for keyboard escape */
Err:
    GpsDisable();
    ModemDisable();
    sleep(1);

    // Don't print a test summary if the user aborted the test or we ran out of
    // space on the heap.
    if ((status & E_ABORT) == 0)
    {
        LogEntry(FuncName, "Test Summary:\n");
        ReportSelfTestResult(
            FuncName, "Mission Configuration", status, E_MISSIONCFG);
        ReportSelfTestResult(FuncName, "Air Bladder", status, E_AIRBLADDER);
        ReportSelfTestResult(
            FuncName, "EEProm External Memory Device", status, E_EEPROM);
        ReportSelfTestResult(
            FuncName, "DS2740 Coulomb Counter", status, E_EEPROM);
        ReportSelfTestResult(
            FuncName, "Leak Detector", status, E_LEAK_DETECTOR);
        ReportSelfTestResult(
            FuncName, "SD Card", status, E_SD | E_FATMNT | E_FATUMNT | E_FIO);
        ReportSelfTestResult(FuncName, "Internal Vacuum", status, E_PSI);
        ReportSelfTestResult(FuncName, "Internal Humidity", status, E_HUMIDITY);
        ReportSelfTestResult(FuncName, "Piston Movement", status, E_PISTON);
        ReportSelfTestResult(FuncName, "EM Comms", status, E_EMA);
        ReportSelfTestResult(FuncName, "CTD Comms", status, E_CTD | E_CTDCFG);
        ReportSelfTestResult(
            FuncName, "Iridium Modem", status, E_MODEM | E_MODEMCFG);
        if (testSky)
        {
            ReportSelfTestResult(FuncName, "GPS Services", status, E_GARMIN);
        }
    }

    if ((status & E_DS2740)        || /* Failed battery vitals */
        (status & E_SD)            || /* SD card failure (pwr, fatfs, or r/w). */
        (status & E_FATMNT)        || /* Failed to mount fatfs */
        (status & E_FATUMNT)       || /* Failed to unmount fatfs */
        (status & E_FIO)           || /* Failed to recondition the fatfs */
        (status & E_PISTON)        || /* Failed to move the piston */
        (status & E_PSI)           || /* Vacuum sensor failure (pwr, bad seal) */
        (status & E_CTD)           || /* CTD failure (pwr, firmware revision) */
        (status & E_CTDCFG)        || /* Failed to configure the ctd */
        (status & E_MODEM)         || /* Failed to query the modem, revision */
        (status & E_MODEMCFG)      || /* Failed to configure the modem */
        (status & E_GARMIN)        || /* Failed to configure the GPS */
        (status & E_STACK)         || /* Stack check failed */
        (status & E_EMA)           || /* Failed to talk to the EMA board */
        (status & E_MISSIONCFG)    || /* Invalid mission config. */
        (status & E_EEPROM)        || /* Failed to read the mission state */
        (status & E_AIRBLADDER)    || /* Air bladder sensor or pressure failure */
        (status & E_HUMIDITY)      || /* Humidity sensor or reading failure */
        (status & E_LEAK_DETECTOR) || /* Leak detector sensor or reading failure */
        (status & E_ABORT))      /* User aborted the self test */
    {
        return 0;
    }

    // If the error checks pass, return a positive value.
    return 1;
}
 
/*------------------------------------------------------------------------*/
/* function to display the time-based sequence points                     */
/*------------------------------------------------------------------------*/
/**
   This function displays the time-based sequence points.
*/
void SequencePointsDisplay(void)
{
   char buf[256],*phase;
   enum State state = StateGet();

   /* create a string that describes the current mission state */
   switch (state)
   {
      case INACTIVE:  {phase="Hibernate."; break;}
      case PACTIVATE: {phase="Pressure-activation."; break;}
      case RECOVERY:  {phase="Recovery."; break;}
      case PRELUDE:   {phase="Mission Prelude."; break;}
      case DESCENT:   {phase="Park Descent."; break;}
      case PARK:      {phase="Park."; break;}
      case GODEEP:    {phase="Profile Descent."; break;}
      case PROFILE:   {phase="Profile."; break;}
      case TELEMETRY: {phase="Telemetry."; break;}
      case UNDEFINED: {phase="Undefined."; break;}
      default:        {phase="Indeterminant."; break;}
   }

   /* create a string with the mission time, alarm time, and mission phase */
   snprintf(buf,sizeof(buf),"Mission time: %ldsec   Alarm time: %ldsec\n"
            "Phase of mission cycle: %s",itimer(),ialarm(),phase);

   /* write the mission time, alarm time, and mission phase to the console */
   pputs(&conio,buf,2,"\n");
   
   /* create a report for an active mission */
   if (state>INACTIVE && state<EOS)
   {
      /* create a report for the mission prelude */
      if (state==PRELUDE) {snprintf(buf,sizeof(buf),"Sequence Point: Mission "
                                    "prelude at %ldsec.",mission.TimePrelude);}
   
      /* create a report for an active mission */
      else
      {
         snprintf(buf,sizeof(buf),"Sequence Points:\n"
                  "   Park descent:    %6ldsec\n"
                  "   Park:            %6ldsec\n" 
                  "   Profile descent: %6ldsec\n" 
                  "   Profile[%4s]:   %6ldsec\n"
                  "   Telemetry:       %6ldsec",
                  SeqPoint.Descent,SeqPoint.Park,SeqPoint.GoDeep,
                  ((DeepProfile()>0)?"Deep":"Park"),SeqPoint.Profile,
                  SeqPoint.Telemetry);
      }

     /* write the report to the console */
     pputs(&conio,buf,2,"\n");
   }
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
time_t SequencePointDownTime(void) {return SeqPoint.GoDeep;}

/*------------------------------------------------------------------------*/
/* function to validate the sequence points                               */
/*------------------------------------------------------------------------*/
/**
   This function verifies that sequence points satisfies ordering
   constraints.  If the ordering constraints are violated, this function
   adjusts the times of the sequence points as necessary.
*/
int SequencePointsValidate(void)
{
   /* define the logging signature */
   cc *FuncName = "SequencePointsValidate()";

   /* initialize the return value */
   int status=1;

   /* initialize the mission down-time */
   time_t TimeDown=mission.TimeDown;

   /* check if down-time should end at a user-specified time-of-day */
   if (inRange(0,mission.ToD,Day)) TimeDown=ToD(time(NULL),mission.TimeDown,mission.ToD);

   /* compute the sequence points */
   SeqPoint.Descent   = mission.TimeParkDescent;
   SeqPoint.Park      = TimeDown - ((DeepProfile()>0) ? mission.TimeDeepProfileDescent : 0); 
   SeqPoint.GoDeep    = TimeDown;
   SeqPoint.Profile   = TimeDown + mission.TimeOutAscent;
   SeqPoint.Telemetry = TimeDown + mission.TimeUp;
   
   /* validate the Profile sequence point */
   if (SeqPoint.Profile>SeqPoint.Telemetry)
   {
      /* make the logentry */
      LogEntry(FuncName,"Invalid sequence points: "
               "Profile[%ldsec]>Telemetry[%ldsec] (rectified).\n",
               SeqPoint.Profile,SeqPoint.Telemetry);
      
      /* recompute valid sequence points */
      SeqPoint.Profile = TimeDown + mission.TimeUp/2; vitals.status|=BadSeqPnt;
   }
   
   /* validate the Park sequence point */
   if (SeqPoint.Park>SeqPoint.GoDeep)
   {
      /* make the logentry */
      LogEntry(FuncName,"Invalid sequence points: "
               "Park[%ldsec]>GoDeep[%ldsec] (rectified).\n",
               SeqPoint.Park,SeqPoint.GoDeep);
      
      /* recompute valid sequence points */
      SeqPoint.Park=TimeDown; vitals.status|=BadSeqPnt;
   }
   
   /* validate the Descent sequence point */
   if (SeqPoint.Descent>SeqPoint.Park)
   {
      /* make the logentry */
      LogEntry(FuncName,"Invalid sequence points: "
               "Descent[%ldsec]>Park[%ldsec] (rectified).\n",
               SeqPoint.Descent,SeqPoint.Park);
      
      /* recompute valid sequence points */
      SeqPoint.Descent = SeqPoint.Park/2; vitals.status|=BadSeqPnt;
   }

   if (debuglevel > 3)
   {
      LogEntry(FuncName, "** Recomputed sequence points follow:\n");
      SequencePointsDisplay();
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* adjust down-time to start profile at user specified time-of-day        */
/*------------------------------------------------------------------------*/
time_t ToD(time_t Tref,time_t TimeDown, time_t DaySec)
{
   /* validate the function arguments */
   if (TimeDown>=0 && inRange(0,DaySec,Day)) 
   {
      /* compute the estimated date/time for the start of the profile */
      time_t skew,daysec,t=Tref+TimeDown; struct tm T; T = *gmtime(&t);

      /* compute the seconds past midnight for the estimated start time */
      daysec=T.tm_sec + T.tm_min*Min + T.tm_hour*Hour;

      /* validate that the skew is less than one day */
      if (labs((skew=(DaySec-daysec)))<Day)
      {
         /* adjust down-time to end at specified seconds after midnight */
         TimeDown += skew; if (skew<0) TimeDown += Day;
      }
   }
   
   return TimeDown;
}

#endif /* CONTROL_C */
