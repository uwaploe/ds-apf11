#ifndef RECOVERY_H
#define RECOVERY_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define recoveryChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes */
int Recovery(void);
int RecoveryInit(void);
int RecoveryTerminate(void);

#endif /* RECOVERY_H */
#ifdef RECOVERY_C
#undef RECOVERY_C

#include <apf11ad.h>
#include <control.h>
#include <eeprom.h>
#include <engine.h>
#include <fatio.h>
#include <gps.h>
#include <logger.h>
#include <prelude.h>
#include <descent.h>
#include <stream.h>
#include <string.h>
#include <telemetry.h>
#include <time.h>

persistent static enum State StateOnExit;

/*------------------------------------------------------------------------*/
/* implement recovery mode functionality                                  */
/*------------------------------------------------------------------------*/
/**
   This function implements a recovery mode for iridium drifters.  
*/
int Recovery(void)
{
   /* define the logging signature */
   cc *FuncName = "Recovery()";

   /* initialize the return value */
   int i,n,status=0;

   /* local objects for logfile creation */
   FILE *fp; char buf[12]; time_t T;
   
   /* initialize the flag to download the mission configuration */
   vitals.status |= DownLoadCfg;

   /* make sure the piston is fully extended */
   if (PistonPositionAdc() < mission.PistonFullExtension)
   {
      /* put the float into its maximum buoyancy state */
      PistonMoveAbs(mission.PistonFullExtension);
   }

   /* close all streams */
   LogAdd("\n<EOT>\n"); LogClose(); fcloseall(); LogPredicate(1);
   
   /* encode the date/time */
   T=time(NULL); strftime(buf,sizeof(buf),"%y%m%d%H%M",gmtime(&T));

   /* create the recovery filename */
   snprintf(prf_path,sizeof(prf_path),"%05u.%s",mission.FloatId,buf);

   /* create the logfile name */
   snprintf(log_path,sizeof(log_path),"%05u.%s.log",mission.FloatId,buf);

   /* allow 3 tries to open the recovery file */
   for (i=0; i<3 && LogOpen(log_path,'w')<=0; i++) {fformat();}
   
   /* make sure the file system is valid and not full */
   if (!(fp=fopen(prf_path,"w")))
   {
      /* reformat the file system */
      LogClose(); fcloseall(); fformat(); LogPredicate(1);

      /* open the log file */
      LogOpen(log_path,'a'); LogEntry(FuncName,"Reformatted RAM file system.\n");

      /* open the msg file */
      fp=fopen(prf_path,"w");
   }

   /* write the mission configuration to the MSG file */
   if (fp) {WriteMissionConfig(fp,prf_path); fclose(fp);}
   
   /* execute telemetry */
   if ((status=Telemetry())>0) {vitals.ConnectionAttempts=0; vitals.Connections=0;}

   /* check if the file system is full */
   if (fdes()<0)
   {
      /* loop through each fid of the file system */
      for (i=0,n=0; i<MAXFILES && n<5; i++)
      {
         /* create a local buffer to hold file system entries */
         char fname[FILENAME_MAX+1];

         /* check if the current fid contains a file */
         if (fioName(i,fname,FILENAME_MAX)>0)
         {
            /* don't delete the mission configuration file */
            if (!strcmp(fname,config_path)) continue;
         
            /* don't delete the active log file */
            if (!strcmp(fname,log_path)) continue;

            /* delete the current file */
            remove(fname); n++;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to exit recovery mode and re-enter the mission in progress    */
/*------------------------------------------------------------------------*/
int RecoveryTerminate(void)
{
   /* initialize the return value */
   int status=1;

   /* exit to mission prelude or else descent phase */
   if (StateOnExit==PRELUDE) PreludeInit(); else DescentInit();

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize a cycle of the recovery mode                    */
/*------------------------------------------------------------------------*/
int RecoveryInit(void)
{
   /* initialize the return value */
   int status=1;

   /* get the current state of the mission */
   enum State state = StateGet();

   /* initialize StateOnExit only if not already in recovery mode */
   if (state!=RECOVERY)
   {
      /* determine which state to enter upon exiting recovery mode */
      StateOnExit = (state>=DESCENT && state<=TELEMETRY) ? DESCENT : PRELUDE;
   }
   
   /* set the state to the mission prelude */
   StateSet(RECOVERY);

   /* intialize telemetry statistics */
   vitals.ConnectionAttempts=0; vitals.Connections=0;
      
   return status;
}

#endif /* RECOVERY_C */
