#ifndef ENGINE_H
#define ENGINE_H (0x01f0U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define engineChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>

/* function prototypes */
int PistonMoveAbs(unsigned short newPosition);
int PistonMoveAbsWTO(unsigned short newPosition, unsigned short *Volt,
                     unsigned short *Amp, time_t timeout);
int PistonMoveRel(unsigned short counts);
   
#endif /* ENGINE_H */
#ifdef ENGINE_C
#undef ENGINE_C

#include <apf11.h>
#include <apf11ad.h>
#include <control.h>
#include <limits.h>
#include <logger.h>
#include <max7301.h>
#include <stdlib.h>
#include <Stm32f103Rtc.h>
#include <time.h>

#define EXTEND   1
#define RETRACT -1
#define BRAKE    0

/*------------------------------------------------------------------------*/
/* function to move the piston to a specified position                    */
/*------------------------------------------------------------------------*/
/**
   This function moves the piston to a specified position.

      \begin{verbatim}
      input:
         newPosition ... The new position to which the piston should be moved.

      output:
         This function returns a positive number if the piston position, on
         exit from this function, matches the user-specified piston
         position.  Zero is return otherwise.
      \end{verbatim}
*/
int PistonMoveAbs(unsigned short newPosition)
{
   return PistonMoveAbsWTO(newPosition,NULL,NULL,7200L);
}

/*------------------------------------------------------------------------*/
/* function to move the buoyancy engine's piston to a specific count      */
/*------------------------------------------------------------------------*/
/**
   This function moves the buoyancy engine's piston to a specific position
   as determined by the piston position sensor.  The piston is protected
   against crashing against either end-limit.  The piston is prevented from
   being retracted or extended past specified limits.  A dead-man's timeout
   is also implemented so that if the piston position does not change during
   a specified time-out interval, the attempt to reposition the piston is
   aborted.

      \begin{verbatim}
      input:
         newPosition ... The new position to which the piston should be moved.
         timeout.........The maximum time that the motor should be allowed
                         to run.
         
      output:

         Volt...The battery voltage (counts) as measured just before the
                motor was turned off.
         Amp....The battery current (counts) as measured just before the
                motor was turned off.
      
         This function returns a positive number if the piston position, on
         exit from this function, matches the user-specified piston
         position.  Zero is return otherwise.
      \end{verbatim}
*/
int PistonMoveAbsWTO(unsigned short newPosition, unsigned short *Volt,
                     unsigned short *Amp, time_t timeout)
{
   /* define the logging signature */
   cc *FuncName = "PistonMoveAbsWTO()";

   time_t pistonStopTime,dT;
   unsigned short VCnt=0xfff,ACnt=0xfff;

   /* initialize return value */
   int status=1;

   /* initialize the piston position */
   unsigned short pistonPosition=PistonPositionAdc();

   /* get the start time */
   const time_t pistonStartTime=itimer(), TimeOut=30; 

   /* condition the new position to be in the range of unsigned int */
   if (newPosition<0) newPosition=0; else if (newPosition > 4072) newPosition = 4072;
   
   /* make a log entry */
   if (debuglevel>=2 || (debugbits&ENGINE_H))
   {
      /* make the logentry */
      LogEntry(FuncName," %03d->%03d",pistonPosition,newPosition);
   }
   
   /* initialize the mag-switch flip-flop */
   MagSwitchReset();
   
   /* check if the piston can be extended */
   if((newPosition>pistonPosition) && (pistonPosition<mission.PistonFullExtension))
   {
      /* record the current piston position */
      unsigned short Po=pistonPosition; time_t To=pistonStartTime;
      
      /* make sure the piston does not become over-extended */
      if(newPosition > mission.PistonFullExtension) newPosition = mission.PistonFullExtension;

      /* activate the buoyancy engine */
      HDrive(EXTEND);

      do
      {
         /* wait a bit and then measure the new piston position */
         Wait(500); pistonPosition = PistonPositionAdc();

         /* implement a dead-man's timeout for piston movement */
         if (abs(pistonPosition-Po)>0)
         {
            /* record the new piston position and the time */
            Po=pistonPosition; To=itimer();

            /* write some user-feedback to the log */
            if (debuglevel>=2 || (debugbits&ENGINE_H)) LogAdd(" %03d",Po);
         }

         /* compute quantities used in termination criteria */
         dT=(time_t)difftime(itimer(),To);
      }

      /* check criteria for terminating the piston extension */
      while ((pistonPosition<newPosition) && dT<TimeOut &&
             difftime(itimer(),pistonStartTime)<timeout &&
             !MagSwitchToggled());

      /* measure power consumption */
      VCnt=BatVoltsAdc(); ACnt=HydraulicPumpAmpsAdc();
      
      /* deactivate the buoyancy engine */
      HDrive(BRAKE);
   }

   /* check if the piston can be retracted */
   else if((newPosition < pistonPosition) && (pistonPosition > mission.PistonFullRetraction))
   {
      /* record the current piston position */
      unsigned short Po=pistonPosition; time_t To=pistonStartTime;
      
      /* make sure the piston does not become overly-retracted */
      if(newPosition < mission.PistonFullRetraction) newPosition = mission.PistonFullRetraction;
      
      /* activate the buoyancy engine */
      HDrive(RETRACT);

      do
      {
         /* wait a bit and then measure the new piston position */
         Wait(500); pistonPosition = PistonPositionAdc();

         /* implement a dead-man's timeout for piston movement */
         if (abs(Po-pistonPosition)>0)
         {
            /* record new piston position and time */
            Po=pistonPosition; To=itimer();
            
            /* write some user-feedback to the log */
            if (debuglevel>=2 || (debugbits&ENGINE_H)) LogAdd(" %03d",Po);
         } 
 
         /* compute quantities used in termination criteria */
         dT=(time_t)difftime(itimer(),To);
      }
      
      /* check criteria for terminating the piston retraction */
      while((pistonPosition>newPosition) && dT<TimeOut &&
             difftime(itimer(),pistonStartTime)<timeout &&
             !MagSwitchToggled());
      
      /* measure power consumption */
      VCnt=BatVoltsAdc(); ACnt=HydraulicPumpAmpsAdc();

      /* deactivate the buoyancy engine */
      HDrive(BRAKE);
   }
   
   /* record the length of time that the buoyancy pump was running */
   pistonStopTime = itimer();

   /* keep a running tally of the pumping time */
   vitals.BuoyancyPumpOnTime += (pistonStopTime - pistonStartTime); 

   /* make a log entry */
   if (debuglevel>=2 || (debugbits&ENGINE_H))
   {
      /* compute the voltage from the volt-counts */
      float Volt = (VCnt!=0xfff) ? BatVolts(VCnt) : 0;

      /* compute the current from the amp-counts */
      float Amp  = (ACnt!=0xfff) ? HydraulicPumpAmps(ACnt)  : 0;

      LogAdd(" [%ldsec, %0.1fVolts, %0.3fAmps, CPT:%ldsec]\n",
             (pistonStopTime - pistonStartTime),Volt,Amp,
             vitals.BuoyancyPumpOnTime);
   }

   /* assign the power measurement to the function arguments */
   if (Volt) *Volt = VCnt;
   if (Amp) *Amp = ACnt;

   /* check if the request was successful */
   status = (abs(pistonPosition-newPosition)<=1) ? 1 : 0;

   /* check if the piston movement was aborted with a magnet swipe */
   if (MagSwitchToggled())
   {
      int i; time_t To=time(NULL);

      /* pause (for up to 5s) until the mag-switch is low for 500ms */
      do {for (i=0; i<5; i++) {Wait(100); if (MagSwitchHigh()) break;}}
      while (i<5 && difftime(time(NULL),To)<5);
         
      /* re-initialize the mag-switch flip-flop */
      MagSwitchReset();
   }

   return status;
} 

/*------------------------------------------------------------------------*/
/* function to move the piston relative to its current location           */
/*------------------------------------------------------------------------*/
/**
   This function extends or retracts the buoyancy engine's piston by a
   specified number of counts.

      \begin{verbatim}
      input:
         counts ... The number of counts to move the piston relative to its
                    current location.  Positive values extend the piston and
                    negative values retract the piston.

      output:
         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exceptional
         condition.
      \end{verbatim}
*/
int PistonMoveRel(unsigned short counts)
{
   /* compute the new piston position */
   unsigned short newPosition = PistonPositionAdc() + counts;

   /* make sure the piston position stays well conditioned */
   if (newPosition<0) newPosition=0; else if (newPosition>4072) newPosition=4072;

   /* move the piston to the new position */
   return PistonMoveAbsWTO(newPosition, NULL, NULL, 7200L);
}

#endif /* ENGINE_C */
