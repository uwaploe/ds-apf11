#ifndef TELEMETRY_H
#define TELEMETRY_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define telemetryChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stdio.h>
#include <serial.h>

/* prototypes for functions with external linkage */
int AirSystem(void);
int Telemetry(void);
int TelemetryInit(void);
int TelemetryTerminate(void);
int WriteIrFix(FILE *fp, float x, float y, float z, float lon, 
               float lat, unsigned long int t);
int WriteMissionConfig(FILE *fp,const char *path);
int WriteVitals(FILE *fp,const char *path);

#endif /* TELEMETRY_H */
#ifdef TELEMETRY_C
#undef TELEMETRY_C

#include <apf11.h>
#include <apf11ad.h>
#include <apf11rf.h>
#include <assert.h>
#include <control.h>
#include <ctdio.h>
#include <descent.h>
#include <download.h>
#include <ds2740.h>
#include <eeprom.h>
#include <fatio.h>
#include <gps.h>
#include <inrange.h>
#include <limits.h>
#include <logger.h>
#include <login.h>
#include <lbt9523.h>
#include <engine.h>
#include <nan.h>
#include <profile.h>
#include <sbe41cp.h>
#include <Stm32f103Rtc.h>
#include <stream.h>
#include <string.h>
#include <upload.h>
#include <unistd.h>

/*------------------------------------------------------------------------*/
/* air-bladder inflation algorithm                                        */
/*------------------------------------------------------------------------*/
/**
   This function inflates the air bladder using fault-tolerant monitoring to
   avoid false-positives for bladder inflation.  The bladder pressure is
   checked N times to determine if it is below the threshold.  The first
   below-threshold measurement will induce the air pump to inflate the
   bladder.  The air pump is run in 1 second bursts followed by a 1 second
   pause before the bladder pressure is sampled.  The air pump will continue
   to run until N consecutive samples exceed the threshold pressure.

   The air pump on floats that use an Apf11 is different than those
   that were used on Apf9 floats.  The old air pump used on Apf9
   floats pumped 520ml of air in 12 seconds (@15V, 43.3ml/s).  The new
   air pump on Apf11 floats pump 650ml in 24 seconds (@15V, 27.1ml/s).  
   Consequently, the timeout period was increased from 300 seconds
   (for Apf9 floats) to 480 seconds (for Apf11 floats) to account for
   the lower pump rate of the new air pumps.

   This function returns the number of pulses of the air pump.
*/
int AirSystem(void)
{
   /* function name for log entries */
   cc *FuncName = "AirSystem()";

   /* initialize the return value */
   int i, pulses=0; const int N=3;

   /* loop to implement multiple checks of air bladder pressure */
   for (pulses=0,i=1; i<=N; i++) 
   {
      /* check if the current air bladder pressure is less than threshold */
      if ((vitals.AirBladderPressure = AirBladderPressureAdc()) <= mission.MaxAirBladder)
      {
         /* exit loop to inflate bladder */
         pulses=1; break;
      }

      /* pause to sample a different part of the surface wave field */
      else if (i<N) sleep(3);
   }
   
   /* check to see if the air bladder should be inflated */
   if (pulses>0)
   {
      /* define variables to monitor air-system power consumption */
      unsigned short Vad12 = 0U;
      unsigned short Iad12 = 0U;

      /* initialize termination parameters for the air-inflation algorithm */
      const time_t To=time(NULL), timeout=480, dT=1; time_t T=0; 

      /* loop until the air bladder exceeds the cut-off for N consecutive samples */
      for (pulses=0,i=0; i<N && difftime(time(NULL),To)<timeout;)
      {
         /* close the air valve */
         if (!(pulses%10)) AirValveClose();

         /* pulse the air pump */
         AirPumpRun(dT,&Vad12,&Iad12); sleep(dT); T+=dT; pulses++;

         /* check pneumatic inflation limits */
         if (difftime(time(NULL),To)>=timeout) 
         {
            LogEntry(FuncName,"Pneumatic inflation limits imposed. [pulses: %d, "
                     "AirPumpSec: %ld]\n",pulses,T); vitals.status |= AirSysLimit; break;
         }

         /* check if the air bladder pressure exceeds the cut-off */
         if ((vitals.AirBladderPressure = AirBladderPressureAdc()) > mission.MaxAirBladder) {i++;} else {i=0;}
      }
      
      /* test whether to make a log entry */
      if (debuglevel>=2 || (debugbits&TELEMETRY_H))
      {
         /* make a log entry of the power consumption of the air system */
         LogEntry(FuncName,"Battery [%ucnt, %0.1fV]  Current [%ucnt, %0.1fmA]  "
                  "Pneumometer [%dcnt, %0.1fpsi]  Run-Time [%lds]\n",
                  Vad12,BatVolts(Vad12),Iad12,
                  AirPumpAmps(Iad12)*1000,vitals.AirBladderPressure,
                  AirBladderPressure(vitals.AirBladderPressure),T);
      }
   }
   else if (debuglevel>=2 || (debugbits&TELEMETRY_H))
   {
      LogEntry(FuncName,"Air-bladder inflation by-passed. Pneumatic Pressure: "
               "[%dcnt, %0.1fpsi].\n",vitals.AirBladderPressure,
               AirBladderPressure(vitals.AirBladderPressure));
   }

   return pulses;
}

/*------------------------------------------------------------------------*/
/* execute tasks during the telemetry phase of the mission cycle          */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the telemetry phase
   of the mission cycle.  
*/
int Telemetry(void)
{
   /* function name for log entries */
   cc *FuncName = "Telemetry()";

   /* initialize the return value */
   int status=0; FILE *dest=NULL;

   /* execute the air-bladder inflation algorithm */
   AirSystem();
   
   /* seek iridium satellite signals */
   if (IrSkySearch(&modem)>0)
   {
      /* check if the profile file has already been uploaded */
      if (prf_path[0])
      {
         /* validate the file name and open the output file */
         if (fnameok(prf_path)<0 || !(dest=fopen(prf_path,"a")))
         {
            LogEntry(FuncName,"Unable to open \"%s\" in append mode.\n",prf_path);
         }
      }
     
      /* execute gps services */
      GpsServices(dest);

      /* activate the modem serial port */
      ModemEnable(19200);

      /* register the modem with the iridium system */
      if (IrModemRegister(&modem)>0)
      {
         /* stack-based objects needed for iridium geolocation */
         float x,y,z,lat,lon; unsigned long int t; int err;

         /* query the LBT for iridium geolocation data */
         if ((err=IrGeoLoc(&modem,&x,&y,&z,&lon,&lat,&t))>0)
         {
            /* write the iridium fix to the MSG file */
            if (dest) {WriteIrFix(dest,x,y,z,lon,lat,t);}
         }
         
         /* download the mission configuration from the remote host */
         if (DownLoadMissionCfg(&modem,2)>0) vitals.status &= ~DownLoadCfg;

         /* write vital statistics and close the profile */
         if (dest) {WriteVitals(dest,prf_path); fclose(dest); dest=NULL;}

         if (PrepareUpload(2) == 0)
         {
            LogEntry(FuncName, "Failed to zip files before sending them...\n");
         }

         /* upload logs and profiles to the remote host */
         status=UpLoad(&modem,2);

         /* logout of the remote host */
         logout(&modem);

         /* write the number connections and connection attempts */
         LogEntry(FuncName,"Telemetry cycle complete: PrfId=%d "
                  "ConnectionAttempts=%u  Connections=%u\n",PrfIdGet(),
                  vitals.ConnectionAttempts,vitals.Connections); 
      }

      else {LogEntry(FuncName,"Iridium modem registration failed.\n");}
      
      /* deactivate the modem serial port */
      ModemDisable();

      /* make sure the profile file is closed */
      if (dest) fclose(dest);
   }

   /* give the buoyancy pump another nudge */
   else
   {
      LogEntry(FuncName,"SkySearch failed - adding buoyancy "
               "to reach the surface.\n");

      /* give the buoyancy pump another nudge */
      PistonMoveRel(mission.PistonInitialBuoyancyNudge);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the telemetry phase of the profile cycle        */
/*------------------------------------------------------------------------*/
/**
   This function initializes the telemetry phase of the mission cycle.     
*/
int TelemetryInit(void)
{
   /* function name for log entries */
   cc *FuncName = "TelemetryInit()";

   /* define objects needed for charge consumption model  */
   unsigned short Vad12;
   unsigned short Iad12;
   float charge=NaN(),ocv=BatVolts(ADC);

   /* initialize the function's return value */
   int err,status=1;

   /* set the state variable */
   StateSet(TELEMETRY);

   /* create the new logfile name */
   snprintf(log_path,sizeof(log_path)-1,"%05u.%03d.log",mission.FloatId,PrfIdGet()+1);

   /* Add <EOT> marker to logfile */
   if (LogHandle()) hprintf(LogHandle(),"\n<EOT>\n");

   /* close all open files and open the new logfile */
   LogClose(); fcloseall(); LogOpen(log_path,'w'); LogPredicate(1);
   
   /* make a log entry */
   if (debuglevel>=2 || (debugbits&TELEMETRY_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"Profile %d. (Apf11 FwRev: %08lx FwDate: %s)\n",PrfIdGet(),FwRev,__DATE__);
   }
   
   /* save the start time of the telemetry phase */
   vitals.TimeStartTelemetry = time(NULL);

   /* exercise the charge consumption model */
   if ((err=ChargeModelCompute(&charge)>0))
   {
      /* make a logentry */
      LogEntry(FuncName,"Charge consumption model: OCV:%0.1fV wake-cycles:%lu "
               "standby:%0.1fdays Q:%0.1fkC\n",ocv,cmodel.WakeCycles,
               (float)cmodel.StandbySec/SecPerDay,cmodel.Coulombs/1000);
   }
   else {LogEntry(FuncName,"Charge consumption model failed. [err=%d]\n",err);}
   
   /* record the accumulated charge consumption */
   vitals.CoulombsTelemetry=(err>0) ? charge : NaN();

   /* close the air valve and run the air pump for 1 second */
   AirValveOpen(); AirPumpRun(1,&Vad12, &Iad12); AirValveClose();

   /* save the air pump current and volts */
   vitals.AirPumpVolts = Vad12;
   vitals.AirPumpAmps = Iad12;
   
   /* measure the air bladder pressure */
   vitals.AirBladderPressure = AirBladderPressureAdc();

   /* initialize the flag to download the mission configuration */
   vitals.status |= DownLoadCfg;
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to terminate the telemetry phase                              */
/*------------------------------------------------------------------------*/
int TelemetryTerminate(void)
{
   /* function name for log entries */
   cc *FuncName = "TelemetryTerminate()";

   /* initialize return value */
   int i,n,status=1;

   /* check if a mission configuration file was successfully downloaded */
   if (!(vitals.status&DownLoadCfg))
   {
      if (debuglevel>=2 || (debugbits&TELEMETRY_H))
      {
         LogEntry(FuncName,"Parsing new mission configuration.\n");
      }
      
      /* process the configuration file */
      configure(&mission,config_path,0);
   }
   else {LogEntry(FuncName,"By-passing mission configuration.\n");}

   if (debuglevel>=2 || (debugbits&TELEMETRY_H))
   {
      LogEntry(FuncName,"Reconditioning the file system.\n");
   }

   /* close all open files and reopen the logfile */
   LogClose(); fcloseall(); LogOpen(log_path,'a'); LogPredicate(1);

   /* check if the RAM file system is full */
   if (fdes()<0)
   {
      /* loop through each fid of the file system */
      for (i=0,n=0; i<MAXFILES && n<5; i++)
      {
         /* create a local buffer to hold file system entries */
         char fname[FILENAME_MAX+1];

         /* check if the current fid contains a file */
         if (fioName(i,fname,FILENAME_MAX)>0)
         {
            /* don't delete the mission configuration file */
            if (!strcmp(fname,config_path)) continue;
         
            /* don't delete the active log file */
            if (!strcmp(fname,log_path)) continue;

            /* delete the current file */
            remove(fname); n++;
         }
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write the iridium geolocation data to the MSG file         */
/*------------------------------------------------------------------------*/
/**
   This function writes the Iridium geolocation data to a stream.
*/
int WriteIrFix(FILE *fp, float x, float y, float z, float lon,
               float lat, unsigned long int t)
{
   /* function name for log entries */
   cc *FuncName = "WriteIrFix()";

   /* initialize the return value */
   int status=1;

   /* validate the function arguments */
   if (fp)
   {
      /* convert iridium system time to unix epoch */
      time_t epoch; const char *date=IrUnixEpoch(t,&epoch);
      
      /* write the iridium geolocation data */      
      fprintf(fp,"IridiumGeo: <x,y,z>: %5.0fkm %5.0fkm %5.0fkm "
              "IrSysTime: 0x%08lx\n",x,y,z,t);

      /* write the iridium geodetic fix computed from geolocation data */
      fprintf(fp,"IridiumFix: <lon,lat>: %8.3f %7.3f IrEpoch: %ldsec %s\n",
              lon,lat,epoch,date);
   }

   /* log the error */
   else {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write mission configuration to the MSG file                */
/*------------------------------------------------------------------------*/
/**
   This function writes the mission configuration to a stream.
*/
int WriteMissionConfig(FILE *fp,const char *path)
{
   /* function name for log entries */
   cc *FuncName = "WriteMissionConfig()";

   /* initialize the return value */
   int status=1;

   /* validate the function arguments */
   if (!fp || !path) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   else
   {
      /* write the mission configuration to the MSG file */
      fprintf(fp,"$ Mission configuration for Apf11(%04u) FwRev %08lx FwDate %s:\n",mission.FloatId,FwRev,__DATE__);
      fprintf(fp,"$ AscentTimeOut(%ld) [min]\n",mission.TimeOutAscent/Min);
      fprintf(fp,"$ AtDialCmd(%s) [primary]\n",mission.at);
      fprintf(fp,"$ AltDialCmd(%s) [alternate]\n",mission.alt);
      fprintf(fp,"$ BuoyancyNudge(%u) [count]\n",mission.PistonBuoyancyNudge);
      fprintf(fp,"$ CompensatorHyperRetraction(%u) [count]\n",mission.PistonParkHyperRetraction);
      fprintf(fp,"$ ConnectTimeOut(%ld) [sec]\n",mission.ConnectTimeOut);
      fprintf(fp,"$ CpActivationP(%g) [dbar]\n",mission.PressureCP);
      fprintf(fp,"$ DeepProfileDescentTime(%ld) [min]\n",mission.TimeDeepProfileDescent/Min);
      fprintf(fp,"$ DeepProfilePistonPos(%d) [count]\n",mission.PistonDeepProfilePosition);
      fprintf(fp,"$ DeepProfilePressure(%g) [dbar]\n",mission.PressureProfile);
      fprintf(fp,"$ DownTime(%ld) [min]\n",mission.TimeDown/Min);
      fprintf(fp,"$ FatFsEnable(%d)\n",mission.FatFsEnable);
      fprintf(fp,"$ FloatId(%04u)\n",mission.FloatId);
      fprintf(fp,"$ FullExtension(%u) [count]\n",mission.PistonFullExtension);
      fprintf(fp,"$ FullRetraction(%u) [count]\n",mission.PistonFullRetraction);
      fprintf(fp,"$ InitialBuoyancyNudge(%u) [count]\n",mission.PistonInitialBuoyancyNudge);
      fprintf(fp,"$ InitialDescentNudge(%u) [count]\n",mission.PistonInitialDescentNudge);
      fprintf(fp,"$ MaxAirBladder(%u) [count]\n",mission.MaxAirBladder);
      fprintf(fp,"$ MaxLogKb(%ld) [KByte]\n",MaxLogSize/1024);
      fprintf(fp,"$ MissionPrelude(%ld) [min]\n",mission.TimePrelude/Min);
      fprintf(fp,"$ OkVacuum(%u) [count]\n",mission.OkVacuumCount);
      fprintf(fp,"$ PActivationPistonPosition(%u) [count]\n",mission.PistonPActivatePosition);
      fprintf(fp,"$ ParkDescentTime(%ld) [min]\n",mission.TimeParkDescent/Min);
      fprintf(fp,"$ ParkPistonPos(%d) [count]\n",mission.PistonParkPosition);
      fprintf(fp,"$ ParkPressure(%g) [dbar]\n",mission.PressurePark);
      fprintf(fp,"$ PnPCycleLen(%d)\n",mission.PnpCycleLength);
      fprintf(fp,"$ TelemetryRetry(%ld) [min]\n",mission.TimeTelemetryRetry/Min);
      if (inRange(0,mission.ToD,Day)) fprintf(fp,"$ TimeOfDay(%ld) [min]\n",mission.ToD/Min);
      else fprintf(fp,"$ TimeOfDay(DISABLED) [min]\n");
      fprintf(fp,"$ UpTime(%ld) [min]\n",mission.TimeUp/Min);
      fprintf(fp,"$ ZModemTimeOut(%ld) [sec]\n",mission.ZModemTimeOut);
      fprintf(fp,"$ Verbosity(%d)\n",debuglevel);
      fprintf(fp,"$ DebugBits(0x%04x)\n",debugbits);
      fprintf(fp,"$ EMASample(%u)\n", mission.EMASample);
      fprintf(fp,"$ EMAMinVelocity(%u)\n", mission.EMAMinVelocity);
      fprintf(fp,"$\n");
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write engineering data to the profile file                 */
/*------------------------------------------------------------------------*/
/**
   This function writes the engineering data to a stream.
*/
int WriteVitals(FILE *fp,const char *path)
{
   /* function name for log entries */
   cc *FuncName = "WriteVitals()";

   /* initialize the return value */
   int i,status=1; char buf[64]; time_t T;
   
   /* validate the function arguments */
   if (!fp || !path) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   else
   {
      if ((debuglevel>=2 || (debugbits&TELEMETRY_H)) && path)
      {
         LogEntry(FuncName,"Writing vitals to \"%s\".\n",path);
      }

      fprintf(fp,"Apf11FwRev=%08lx\n",FwRev);
      fprintf(fp,"Apf11FwDate=%s\n",__DATE__);
      fprintf(fp,"ActiveBallastAdjustments=%u\n",vitals.ActiveBallastAdjustments);
      fprintf(fp,"AirBladderPressure=%u\n",vitals.AirBladderPressure);
      fprintf(fp,"AirPumpAmps=%u\n",vitals.AirPumpAmps);
      fprintf(fp,"AirPumpVolts=%u\n",vitals.AirPumpVolts);
      fprintf(fp,"BuoyancyPumpOnTime=%ld\n",vitals.BuoyancyPumpOnTime);
      fprintf(fp,"BuoyancyPumpAmps=%u\n",vitals.BuoyancyPumpAmps);
      fprintf(fp,"BuoyancyPumpVolts=%u\n",vitals.BuoyancyPumpVolts);
      fprintf(fp,"ConnectionAttempts=%u\n",vitals.ConnectionAttempts);
      fprintf(fp,"Connections=%u\n",vitals.Connections);
      fprintf(fp,"CurrentPistonPosition=%u\n",PistonPositionAdc());
      fprintf(fp,"Coulombs=%0.1fkC\n",cmodel.Coulombs/1000);
      fprintf(fp,"CoulombsDescent=%0.1fkC\n",vitals.CoulombsDescent);
      fprintf(fp,"CoulombsPark=%0.1fkC\n",vitals.CoulombsPark);
      fprintf(fp,"CoulombsPrfDescent=%0.1fkC\n",vitals.CoulombsPrfDescent);
      fprintf(fp,"CoulombsProfile=%0.1fkC\n",vitals.CoulombsProfile);
      fprintf(fp,"CoulombsTelemetry=%0.1fkC\n",vitals.CoulombsTelemetry);
      fprintf(fp,"DeepProfilePistonPosition=%u\n",mission.PistonDeepProfilePosition);
      fprintf(fp,"GpsFixTime=%ld\n",vitals.GpsFixTime);
      fprintf(fp,"HumidityAdcPark=%hu (%0.1f%%)\n",vitals.HumidityAdcPark,Humidity(vitals.HumidityAdcPark));
      fprintf(fp,"HumidityAdcPneumatic=%hu (%0.1f%%)\n",vitals.HumidityAdcPneumatic,Humidity(vitals.HumidityAdcPneumatic));
      fprintf(fp,"HumidityAdcPneumaticRef=%hu (%0.1f%%)\n",vitals.HumidityAdcPneumaticRef,Humidity(vitals.HumidityAdcPneumaticRef));
      fprintf(fp,"FloatId=%04u\n",mission.FloatId);
      fprintf(fp,"ParkDescentPCnt=%u\n",vitals.ParkDescentPCnt);

      for (i=0; i<vitals.ParkDescentPCnt && i<ParkDescentPMax; i++)
      {
         fprintf(fp,"ParkDescentP[%d]=%u\n",i,vitals.ParkDescentP[i]);
      }

      fprintf(fp,"ParkPistonPosition=%u\n",mission.PistonParkPosition);
      fprintf(fp,"ParkObs={ %3.1fdbar, %5.3fC, %6.4fPSU }\n",
              vitals.ParkObs.p,vitals.ParkObs.t,vitals.ParkObs.s);

      fprintf(fp,"ProfileId=%03d\n",PrfIdGet());
      fprintf(fp,"ObsIndex=%u\n",vitals.ObsIndex);
      fprintf(fp,"QuiescentAmps=%u\n",vitals.QuiescentAmps);
      fprintf(fp,"QuiescentVolts=%u\n",vitals.QuiescentVolts);
      fprintf(fp,"RtcSkew=%ld\n",vitals.RtcSkew);
      fprintf(fp,"Sbe41cpAmps=%u\n",vitals.Sbe41cpAmps);
      fprintf(fp,"Sbe41cpVolts=%u\n",vitals.Sbe41cpVolts);
      fprintf(fp,"Sbe41cpStatus=0x%08lx\n",vitals.Sbe41cpStatus);
      fprintf(fp,"SleepSec=%0.0f\n",vitals.SleepSec);
      fprintf(fp,"StandbySec=%lu\n",cmodel.StandbySec);
      fprintf(fp,"status=0x%08lx\n",vitals.status);
      fprintf(fp,"SurfacePistonPosition=%u\n",vitals.SurfacePistonPosition);
      fprintf(fp,"SurfacePressure=%0.2f\n",vitals.SurfacePressure);

      buf[0]=0; T=vitals.TimeStartDescent; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartDescent=%-10ld         %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStartSink; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartSink=%-10ld         %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStartPark; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartPark=%-10ld            %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStartProfileDescent; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartProfileDescent=%-10ld  %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStartProfile; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartProfile=%-10ld         %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStopProfile; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStopProfile=%-10ld          %s\n",T,buf);

      buf[0]=0; T=vitals.TimeStartTelemetry; if (T>0) {strftime(buf,sizeof(buf),"%b %d %Y %T",gmtime(&T));}
      fprintf(fp,"TimeStartTelemetry=%-10ld       %s\n",T,buf);

      fprintf(fp,"Vacuum=%u\n",vitals.Vacuum);
      fprintf(fp,"WakeCycles=%lu\n",cmodel.WakeCycles);
      fprintf(fp,"WakeSec=%0.0f\n",vitals.WakeSec);
      fprintf(fp,"\n<EOT>\n");
   }
   
   return status;
}

#endif /* TELEMETRY_C */
