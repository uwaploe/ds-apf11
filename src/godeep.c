#ifndef GODEEP_H
#define GODEEP_H (0x0040U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define godeepChangeLog "$RCSfile$  $Revision$  $Date$"

/* enumerate piston-adjustment options when the deep-descent phase is terminated */
enum PistonAdjustment {RETRACT=-1,EXTEND=1};

/* function prototypes */
int GoDeep(void);
int GoDeepInit(void);
int GoDeepTerminate(enum PistonAdjustment pa);

#endif /* GODEEP_H */
#ifdef GODEEP_C
#undef GODEEP_C

#include <apf11ad.h>
#include <control.h>
#include <crc16bit.h>
#include <ds2740.h>
#include <eeprom.h>
#include <engine.h>
#include <logger.h>
#include <nan.h>
#include <profile.h>
#include <Stm32f103Rtc.h>

/*------------------------------------------------------------------------*/
/* execute tasks during the deep-descent phase of the mission cycle       */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the deep-descent
   phase of the mission cycle.  A positive value is returned on success;
   otherwise zero is returned.  
*/
int GoDeep(void)
{
   /* define the logging signature */
   cc *FuncName = "GoDeep()";

   int status=1;
   float p;

   /* bail out of the descent phase if the pressure channel fails */
   if (GetP(&p)<=0) {ProfileInit(); status=0;}

   /* detection criteria for sequence point: Q */
   else if (p>=mission.PressureProfile)
   {
      /* make the logentry */
      LogEntry(FuncName,"Sequence point detected at %0.1fdbar.\n",p);

      /* terminate the deep-descent phase */
      GoDeepTerminate(EXTEND);

      /* initialize the profile phase */
      ProfileInit();

      /* signal the end of the descent phase */
      status=0;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the deep-descent phase of the profile cycle     */
/*------------------------------------------------------------------------*/
/**
   This function initializes the descent phase of the mission cycle.  A
   positive value is returned on success; otherwise zero is returned.
*/
int GoDeepInit(void)
{
   /* define the logging signature */
   cc *FuncName = "GoDeepInit()";

   /* initialize the return value */
   int err,status=1;

   /* define objected needed for charge consumption model  */
   float charge=NaN(),ocv=BatVolts(ADC);

   /* set the state variable */
   StateSet(GODEEP);
      
   /* save the start time of the profile-descent phase */
   vitals.TimeStartProfileDescent = time(NULL);

   /* exercise the charge consumption model */
   if ((err=ChargeModelCompute(&charge)>0))
   {
      /* make a logentry */
      LogEntry(FuncName,"Charge consumption model: OCV:%0.1fV wake-cycles:%lu "
               "standby:%0.1fdays Q:%0.1fkC\n",ocv,cmodel.WakeCycles,
               (float)cmodel.StandbySec/SecPerDay,cmodel.Coulombs/1000);
   }
   else {LogEntry(FuncName,"Charge consumption model failed. [err=%d]\n",err);}
   
   /* record the accumulated charge consumption */
   vitals.CoulombsPrfDescent=(err>0) ? charge : NaN();

   /* make a log entry */
   if (debuglevel>=2 || (debugbits&GODEEP_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"Moving piston.\n");
   }
   
   /* set the 'deep profile' bit in the status word */
   vitals.status |= DeepPrf;
   
   /* move the piston to the profile piston position */
   PistonMoveAbs(mission.PistonDeepProfilePosition);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to terminate the deep-descent phase of the mission cycle      */
/*------------------------------------------------------------------------*/
/**
   This function terminates the deep-descent phase of the mission cycle.  A
   positive value is returned on success; otherwise zero is returned.
*/
int GoDeepTerminate(enum PistonAdjustment pa)
{
   /* define the logging signature */
   cc *FuncName = "GoDeepTerminate()";

   int status=1;

   switch (pa)
   {
      case EXTEND:
      {
         /* see if the deep piston position can be adjusted */
         if (mission.PistonDeepProfilePosition<mission.PistonFullExtension)
         {
            /* increment the deep piston position to slow the descent-rate */
            ++mission.PistonDeepProfilePosition;
         }
         break;
      }

      case RETRACT:
      {
         /* check if the deep piston position can be adjusted */
         if (mission.PistonDeepProfilePosition>mission.PistonFullRetraction)
         {
            /* decrement the deep piston position to speed the descent rate */
            --mission.PistonDeepProfilePosition;
         }
         break;
      }

      /* make the logentry */
      default: {LogEntry(FuncName,"Invalid piston adjustment option: %d\n",pa);}
   }
                  
   /* recompute the signature of the mission configuration */
   mission.crc = Crc16Bit((unsigned char *)(&mission),
                          sizeof(mission)-sizeof(mission.crc));

   return status;
}

#endif /* GODEEP_C */
