#ifndef PACTIVATE_H
#define PACTIVATE_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define pactivateChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>

/* function prototypes */
int    PActivate(void);
int    PActivateInit(void);

#endif /* PACTIVATE_H */
#ifdef PACTIVATE_C
#undef PACTIVATE_C

#include <apf11.h>
#include <apf11ad.h>
#include <eeprom.h>
#include <engine.h>
#include <logger.h>
#include <max7301.h>
#include <prelude.h>
#include <profile.h>
#include <Stm32f103Pwr.h>
#include <Stm32f103Rtc.h>
#include <stream.h>

/*------------------------------------------------------------------------*/
/* function to implement pressure-activation of the float's mission       */
/*------------------------------------------------------------------------*/
/**
   This function implements the pressure-activation algorithm for initiating
   a float mission.  
*/
int PActivate(void)
{
   int err, status = 0;
   float p = 0;
   if ((err = GetP(&p)) <= 0)
   {
      /* if a confirmatory-query fails then initiate mission */
      sleep(5);
      if (GetP(&p) <= 0)
      {
        PreludeInit();
        status = 1;
      }
   }

   /* check the activation criteria */
   if (err > 0 && (p >= mission.ActivationPressure || p < -10))
   {
      /* pause and check activation criteria again */
      sleep(5);
      if (GetP(&p) > 0 && (p >= mission.ActivationPressure || p < -10))
      {
         PreludeInit();
         status = 1;
      }
   }
   else if (PistonPositionAdc() != mission.PistonPActivatePosition)
   {
      PistonMoveAbs(mission.PistonPActivatePosition);
   }

   AirValveOpen();
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the pressure-activation phase                   */
/*------------------------------------------------------------------------*/
/**
   This function initializes the pressure-activation algorithm for
   initiating the float mission.
*/
int PActivateInit(void)
{
   /* create the log signature */
   cc *FuncName = "PActivateInit()";

   /* initialize the return value */
   int status=0;

   /* get the current state of the mission */
   enum State state = StateGet();

   /* check if mission is already active */
   if (state>INACTIVE && state<EOS)
   {
      /* make the logentry */
      LogEntry(FuncName,"Mission already active; "
               "pressure-activation mode unavailable.\n");
   } 
   else
   {
      /* define stack-based objects for local use */
      unsigned short Vad12;
      unsigned short Iad12;
      unsigned char *p; unsigned int i;

      /* initialize the engineering data to zeros */
      for (p=(unsigned char *)&vitals, i=0; i<sizeof(vitals); i++) {p[i]=0;}

      /* set the state for automatic pressure activation */
      StateSet(PACTIVATE);

      /* reset the magnetic reset-switch */
      MagSwitchReset();

      /* open the air-valve so that self-test measures internal vacuum */
      AirValveOpen();
   
      /* run the air pump for 1 second as an audible startup signal */
      AirPumpRun(1,&Vad12,&Iad12); 

      /* save the air pump current and volts */
      vitals.AirPumpVolts = Vad12;
      vitals.AirPumpAmps = Iad12;

      /* open the air-valve again and wait a few seconds for bladder to deflate */
      AirValveOpen(); sleep(10);
      
      /* create a log file name */
      snprintf(log_path,sizeof(log_path),"%05u.000.log",mission.FloatId);

      /* format the file system and open the log file */
      LogClose(); fformat(); LogOpen(log_path,'w'); LogPredicate(1);

      /* run the self-test before the mission */
      if (SelfTest(true /* testSky */)<=0)
      {
         /* make the logentry */
         LogEntry(FuncName,"Self-test failed - aborting mission.\n");

         /* kill the mission */
         MissionKill();
      }
   
      /* initialize the p-activation phase */
      else
      {
         /* note the current mission time */
         time_t To=itimer();

         /* make the logentry */
         LogEntry(FuncName,"Self-test passed.\n");

         /* make a log entry */
         if (debuglevel>=2 || (debugbits&PACTIVATE_H))
         {
            /* make the logentry */
            LogEntry(FuncName,"Pressure activation mode initiated. [itime=%ld]\n",To);
         }
         
         /* reset the interval timer */
         IntervalTimerSet(0,0);

         /* move the piston to the p-activation position */
         PistonMoveAbs(mission.PistonPActivatePosition);

         /* initialize the return value */
         status=1;
      }
   }
   
   return status;
}

#endif /* PACTIVATE_C */
