#ifndef DESCENT_H
#define DESCENT_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define descentChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h> // for time_t

/* function prototypes for statically linked functions */
time_t Descent(void);
int    DescentInit(void);
int    DescentTerminate(void);

#endif /* DESCENT_H */
#ifdef DESCENT_C
#undef DESCENT_C

#include <apf11.h>
#include <apf11ad.h>
#include <control.h>
#include <ds2740.h>
#include <eeprom.h>
#include <ema.h>
#include <engine.h>
#include <inrange.h>
#include <logger.h>
#include <math.h> // for fabs()
#include <max7301.h>
#include <nan.h>
#include <profile.h>
#include <sbe41cp.h>
#include <stdio.h>
#include <Stm32f103Pwr.h>
#include <Stm32f103Rtc.h>
#include <stream.h>
#include <telemetry.h>
#include <unistd.h>

// structure to preserve descent-control parameters
persistent static struct {
   unsigned short PistonPosition;
   float          RefPressure;
   time_t         RefTime;
} DescentControl;

/*------------------------------------------------------------------------*/
/* agent to control the descent rate of the float                         */
/*------------------------------------------------------------------------*/
/**
   This function controls the descent-rate of the float by monitoring time
   and pressure and computing criteria for activating the buoyancy control
   engine.

   Returns a positive number if the piston was nudged in either direction or
   zero if there was no adjustment. Note it never returns negative as there
   is no error condition.
*/
static float DescentControlAgent(float vMin) {

   // logging signature
   cc *FuncName = "DescentControlAgent()";

   // note our piston timeout is always going to be the length of a heartbeat because
   // if we're controlling our descent then we're em sampling
   time_t TimeOut = HeartBeat;

   // get the current mission time
   const time_t t = itimer();
   const float vMax = 0.30;

   float velocity = 0.0;
   float pressure;
   if (GetP(&pressure) > 0)
   {
      velocity = (pressure - DescentControl.RefPressure) / (t - DescentControl.RefTime);
      LogEntry(FuncName, "** Computed descent speed (v=%0.2fdbar/s).\n", velocity);
      LogEntry(FuncName, "** Current pressure: %0.2f, Reference pressure: %0.2f (%ld seconds)\n", pressure, DescentControl.RefPressure, t - DescentControl.RefTime);
      if (fabs(velocity) > vMax)
      {
         vitals.status |= Sbe41cpPUnreliable;
      }
   }
   else
   {
      LogEntry(FuncName, "GetP() failed.\n");
   }

   // Keep moving the piston if we're not at its target.
   // Otherwise, adjust if the velocity is not enough.
   if (PistonPositionAdc() != DescentControl.PistonPosition)
   {
      unsigned short V = 0xfff;
      unsigned short A = 0xfff;
      PistonMoveAbsWTO(DescentControl.PistonPosition, &V, &A, TimeOut);
   }
   else if (vitals.status & Sbe41cpPUnreliable)
   {
      DescentControl.PistonPosition -= mission.PistonBuoyancyNudge;
      if (debuglevel >= 2 || (debugbits & DESCENT_H))
      {
         LogEntry(FuncName, "Bouyancy nudge to %d (Sbe41cpPUnreliable).\n", DescentControl.PistonPosition);
      }
   }
   else if (velocity <= vMin)
   {
      DescentControl.PistonPosition -= mission.PistonBuoyancyNudge;
   }

   // save new references for pressure and time as long as we got valid pressure
   if (!(vitals.status & Sbe41cpPUnreliable))
   {
      DescentControl.RefTime = t;
      DescentControl.RefPressure = pressure;
   }

   // make sure that the new target does not exceed full retraction
   // but note we allow it to retract beyond the park position in order to maintain speed for sampling
   // if the piston retracts beyond park position it will be corrected at the end of the descent phase
   if (DescentControl.PistonPosition < mission.PistonFullRetraction)
   {
      LogEntry(FuncName, "Reset piston position to avoid over retraction.\n");
      DescentControl.PistonPosition = mission.PistonFullRetraction;
   }

   return velocity;
}

/*------------------------------------------------------------------------*/
/* execute tasks during the descent phase of the mission cycle            */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the descent phase
   of the mission cycle.  It returns the recommended sleep period.
*/
time_t Descent(void)
{
   /* define the logging signature */
   cc *FuncName = "Descent()";

   int err;
   float p;

   // sleep duration is determined on a number of conditions
   time_t sleep;

   /* measure the current pressure */
   if ((err=GetP(&p))<=0)
   {
      /* make the logentry */
      LogEntry(FuncName,"Pressure measurement failed [%d].\n",err);

      /* set a status bit */
      vitals.status|=Sbe41cpPFail;
   }
   else
   {
      /* verify the logging verbosity */
      if (debuglevel>=2 || (debugbits&DESCENT_H))
      {
         /* make the logentry */
         LogEntry(FuncName,"Pressure: %0.1f\n",p);
      }

      /* check criteria for early correction of compressee hyper-retraction */
      if (mission.PistonParkHyperRetraction && p>=MinN2ParkPressure &&
          mission.PistonParkPosition!=PistonPositionAdc())
      {
         /* extend to the park piston position */
         PistonMoveAbs(mission.PistonParkPosition);
      }
   }
   
   /* check if the current pressure should be stored */
   if (vitals.ParkDescentPCnt<ParkDescentPMax)
   {
      /* initialize the pressure with a sentinel value */
      unsigned char P=0xfe;

      /* round the pressure to the nearest bar */
      if (err>0) P = (p>0) ? (unsigned int)(p/10.0 + 0.5) : 0;

      /* store the pressure (bars) in the array */
      vitals.ParkDescentP[vitals.ParkDescentPCnt]=P; vitals.ParkDescentPCnt++;
   }

   // collect the em samples for this cycle
   const float vMin = (mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT) ?
    (float) mission.EMAMinVelocity / 100.0 : 0.08;
   time_t buoyancyPumpTimeRef = vitals.BuoyancyPumpOnTime;
   float velocity = DescentControlAgent(vMin);

   if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT)
   {
      // check the descent speed before collecting samples
      // return value is positive if a piston nudge was performed
      // note pressure isn't guaranteed to be valid here
      if (!(vitals.status & Sbe41cpPUnreliable))
      {
        char empath[32];
        snprintf(empath, sizeof(empath), "%05u.%03dd.efr", mission.FloatId, PrfIdGet());

        // open the file for binary appending
        FILE* emfile = NULL;
        if((emfile = fopen(empath, "ab")))
        {
          int count = EMAWriteAllDataFramesToFile(emfile);
          if (count < 0)
          {
            LogEntry(FuncName, "** Problem collecting em data frames");
          }
          else
          {
             LogEntry(FuncName, "** Wrote %d em data frames to file with velocity %0.2f (dbars/sec)\n", count, velocity);
          }

          fclose(emfile);
        }
        else
        {
           LogEntry(FuncName, "** Couldn't open em file for binary appending: %s\n", empath);
        }
      }

      if (buoyancyPumpTimeRef < vitals.BuoyancyPumpOnTime)
      {
         // Don't sleep when we move the piston, since that takes time and we want to sample asap.
         sleep = 0;
      }
      else
      {
         // We didn't move the piston, so give the EM time to collect more frames.
         sleep = HeartBeat;
      }
   } else {
      // if not em sampling use our regularly scheduled decent timeout
      // which tries to wakeup every hour on the hour
      time_t now = itimer();
      sleep = now - (now % Hour) + Hour;
   }
   
   return sleep;
}

/*------------------------------------------------------------------------*/
/* function to initialize the descent phase of the profile cycle          */
/*------------------------------------------------------------------------*/
/**
   This function initializes the descent phase of the mission cycle.  A
   positive value is returned on success; otherwise zero is returned.    
*/
int DescentInit(void)
{
   /* define the logging signature */
   cc *FuncName = "DescentInit()";

   /* initialize the return value */
   int err,status=1;
    
   /* define objected needed for charge consumption model  */
   float charge=NaN(),ocv=BatVolts(ADC);

   /* define local work objects */
   FILE *fp; float p=0; time_t now = itimer();

   /* reset the interval timer */
   IntervalTimerSet(0,0);
   
   /* set the state variable */
   StateSet(DESCENT);
   
   /* increment the profile number */
   status=PrfIdIncrement();

   // reset the engineering data
   vitals.TimeStartSink = -1;
      
   /* create the file to contain the profile data */
   snprintf(prf_path,sizeof(prf_path),"%04u.%03d.msg",mission.FloatId,PrfIdGet());
   
   /* make sure the file system is valid and not full */
   if (!(fp=fopen(prf_path,"w")))
   {
      /* reformat the file system */
      LogClose(); fcloseall(); fformat(); LogPredicate(1);

      /* open the log file */
      LogOpen(log_path,'a'); LogEntry(FuncName,"Reformatted RAM file system.\n");

      /* open the msg file */
      fp=fopen(prf_path,"w");
   }

   /* write the mission configuration to the MSG file */
   if (fp) {WriteMissionConfig(fp,prf_path); fclose(fp);}

   /* make a log entry */
   if (debuglevel>=2 || (debugbits&DESCENT_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"%s profile %d initiated at mission-time %ldsec.\n",
               (DeepProfile()>0)?"Deep":"Park",PrfIdGet(),now);
   }
   
   /* make sure the sequence points satisfy sanity constraints */
   SequencePointsValidate();

   /* get the surface pressure reading just prior to descent */
   if (GetP(&p)<=0) p=vitals.SurfacePressure;

   /* log the surface pressure */
   if (debuglevel>=2 || (debugbits&DESCENT_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"Surface pressure: %0.1fdbars.\n",p);
   }
   
   /* initialize the engineering data */
   InitVitals(); 
   
   /* save the surface pressure reading */
   vitals.SurfacePressure = p;

   /* save the start time of the descent phase */
   vitals.TimeStartDescent = time(NULL);

   /* exercise the charge consumption model */
   if ((err=ChargeModelCompute(&charge)>0))
   {
      /* make a logentry */
      LogEntry(FuncName,"Charge consumption model: OCV:%0.1fV wake-cycles:%lu "
               "standby:%0.1fdays Q:%0.1fkC\n",ocv,cmodel.WakeCycles,
               (float)cmodel.StandbySec/SecPerDay,cmodel.Coulombs/1000);
   }
   else {LogEntry(FuncName,"Charge consumption model failed. [err=%d]\n",err);}
   
   /* record the accumulated charge consumption */
   vitals.CoulombsDescent=(err>0) ? charge : NaN();

   /* check if an 8-bit argos profile counter will overflow */
   if (PrfIdGet()>255) vitals.status|=PrfIdOverflow;

   // if em sampling on descent then we want to keep the air bladder
   // inflated, to stay on the surface, while the piston is retracting
   if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT) {
      // this makes sure the air bladder if fully inflated
      AirSystem();

      // then move the piston the initial descent nudge
      PistonMoveRel(-mission.PistonInitialDescentNudge);

   // otherwise open the air valve before moving the piston
   } else {

      /* open the air valve twice; measure change in humidity */
      sleep(2); vitals.HumidityAdcPneumaticRef = HumidityAdc(); AirValveOpen(); sleep(2);
      AirValveOpen(); sleep(8); vitals.HumidityAdcPneumatic = HumidityAdc();

      /* make a logentry about the change in humidity */
      LogEntry(FuncName,"Pneumatic humidity: %hu(%0.1f%%) -> %hu(%0.1f%%)\n",
               vitals.HumidityAdcPneumaticRef,Humidity(vitals.HumidityAdcPneumaticRef),
               vitals.HumidityAdcPneumatic,Humidity(vitals.HumidityAdcPneumatic));
      
      // record a separate time for when the sink starts
      vitals.TimeStartSink = time(NULL);

      /* move the piston to the park position */
      PistonMoveAbs(mission.PistonParkPosition);

      /* check for N2 compressee hyper-retraction */
      if (mission.PistonParkHyperRetraction)
      {
         PistonMoveRel(-mission.PistonParkHyperRetraction);
      }
   }

   // if em sampling, now is the time to deflate the air bladder
   // note this is copied from above, including the log statement
   if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT) {

      /* open the air valve twice; measure change in humidity */
      sleep(2); vitals.HumidityAdcPneumaticRef=HumidityAdc(); AirValveOpen(); sleep(2);
      AirValveOpen(); sleep(8); vitals.HumidityAdcPneumatic=HumidityAdc();

      /* make a logentry about the change in humidity */
      LogEntry(FuncName,"Pneumatic humidity: %hu(%0.1f%%) -> %hu(%0.1f%%)\n",
               vitals.HumidityAdcPneumaticRef,Humidity(vitals.HumidityAdcPneumaticRef),
               vitals.HumidityAdcPneumatic,Humidity(vitals.HumidityAdcPneumatic));
      
      // record a separate time for when the sink starts
      vitals.TimeStartSink = time(NULL);

      // initialize the descent control structure
      // note we do this after the piston movement and the bladder control
      unsigned char *b = (unsigned char *)(&DescentControl);
      for(int i = 0; i < sizeof(DescentControl); i += 1) {
         b[i] = 0;
      }

      // get the updated pressure after we've started sinking
      // note we should be way deeper than surface pressure but need some init value
      if(GetP(&p) <= 0) {
         p = vitals.SurfacePressure;
      }

      // starting pressure and time for computing speed
      DescentControl.RefPressure = p;
      DescentControl.RefTime = itimer();

      // record the start piston position as value after the initial nudge
      DescentControl.PistonPosition = PistonPositionAdc();
   }

   /* power cycle the Sbe41cp */
   if (CtdPowerCycle(3)<=0)
   {
      LogEntry(FuncName,"Attempt to power cycle the Sbe41cp failed.\n");
   }

   /* check criteria for optional logentries */
   else if (debuglevel>=3 || (debugbits&DESCENT_H))
   {
      LogEntry(FuncName,"Power-cycle of Sbe41cp was successful.\n");
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to terminate the descent phase of the mission cycle           */
/*------------------------------------------------------------------------*/
/**
   This function terminates the descent phase of the mission cycle.  A
   positive value is returned on success; otherwise zero is returned.
*/
int DescentTerminate(void)
{
   // if the current piston position isn't at the mission position it should be adjusted
   int inPosition = mission.PistonParkPosition == PistonPositionAdc();

   /* correct the compressee hyper-retraction */
   if (mission.PistonParkHyperRetraction && !inPosition)
   {
      /* extend to the park piston position */
      PistonMoveAbs(mission.PistonParkPosition);
   }

   // perform one final descent cycle
   // note we disregard the sleep time response
   Descent();

   // done with ema collection so turn off device and reset piston
   if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT) {
      unsigned char embuffer[128];
      int emaresponse = EMASleep(embuffer, 128);
      LogEntry("DescentTerminate()", "** Turned off ema, response (%d): %s\n", emaresponse, embuffer);

      // since the float is kept at a particular speed for sampling
      // it might have retracted beyond park position
      if(!inPosition) {
         PistonMoveAbs(mission.PistonParkPosition);
      }
   }

   return 1;
}

#endif /* DESCENT_C */
