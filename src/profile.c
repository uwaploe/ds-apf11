#ifndef PROFILE_H
#define PROFILE_H (0x0080U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define profileChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <time.h>

/* definition of structure to contain an observation */
struct Obs {float p, t, s;};

/* function prototypes */
int    GetObs(struct Obs *obs);
int    GetP(float *p);
int    GetPt(float *p, float *t);
time_t Profile(void);
int    ProfileInit(void);
int    ProfileTerminate(void);

/* define the number of elements in the pressure table */
extern const int pTableSize;
extern const float pTable[];

extern persistent struct Obs obs[];
extern persistent int pTableIndex;
extern persistent char prf_path[32];

#endif /* PROFILE_H */
#ifdef PROFILE_C
#undef PROFILE_C

#include <apf11.h>
#include <apf11ad.h>
#include <apf11com.h>
#include <ds2740.h>
#include <eeprom.h>
#include <ema.h>
#include <engine.h>
#include <errno.h>
#include <fatsys.h>
#include <limits.h>
#include <logger.h>
#include <math.h>
#include <nan.h>
#include <sbe41cp.h>
#include <Stm32f103Rtc.h>
#include <telemetry.h>

/* structure to preserve ascent-control parameters */
persistent static struct
{
      unsigned short PistonPosition;
      unsigned short InitialExtension;
      float          RefPressure;
      float          SurfacePressure;
      time_t         RefTime;
      time_t         TimeStampObs;
} AscentControl;

/* define where PTS samples should be taken */
const float pTable[] =
{
   2000.0, 1950.0, 1900.0, 1850.0, 1800.0, 1750.0, 1700.0, 1650.0, 1600.0, 1550.0,
   1500.0, 1450.0, 1400.0, 1350.0, 1300.0, 1250.0, 1200.0, 1150.0, 1100.0, 1050.0,
   1000.0,  950.0,  900.0,  850.0,  800.0,  750.0,  700.0,  650.0,  600.0,  550.0,
    500.0,  450.0,  400.0,  380.0,  360.0,  350.0,  340.0,  330.0,  320.0,  310.0,
    300.0,  290.0,  280.0,  270.0,  260.0,  250.0,  240.0,  230.0,  220.0,  210.0,
    200.0,  190.0,  180.0,  170.0,  160.0,  150.0,  140.0,  130.0,  120.0,  110.0,
    100.0,   90.0,   80.0,   70.0,   60.0,   50.0,   40.0,   30.0,   20.0,   10.0,
      6.0,    0.0
};
const int pTableSize = sizeof(pTable)/sizeof(float);

persistent struct Obs obs[sizeof(pTable)/sizeof(float)];
persistent int pTableIndex;
persistent char prf_path[32];

/* function prototypes for statically linked functions */
static int SurfaceDetect(float p);

/*------------------------------------------------------------------------*/
/* agent to control the ascent rate of the float                          */
/*------------------------------------------------------------------------*/
/**
   This function controls the ascent-rate of the float by monitoring time
   and pressure and computing criteria for activating the buoyancy control
   engine.

   Returns a positive number if the piston was nudged in either direction or
   zero if there was no adjustment. Note it never returns negative as there
   is no error condition.
*/
static float AscentControlAgent(float vMin)
{
   /* define the logging signature */
   cc *FuncName = "AscentControlAgent()";
   time_t TimeOut = HeartBeat;

   /* get the current mission time */
   const time_t t = itimer();
   const float Vmax=0.30;
   float velocity = 0.0;
   float pressure = 0.0;

   if (GetP(&pressure) > 0)
   {
      /* compute the vertical ascent rate */
      velocity = -(pressure - AscentControl.RefPressure) / (t - AscentControl.RefTime);

      if (fabs(velocity) > Vmax)
      {
        vitals.status |= Sbe41cpPUnreliable;
      }
      else
      {
         LogEntry(FuncName, "**Calculated ascent velocity: %0.2f\n", velocity);
      }

      LogEntry(FuncName, "PrN=%.2f\n", pressure);
   }
   else
   {
      LogEntry(FuncName,"GetP() failed.\n");
   }

   if (!(vitals.Sbe41cpStatus & Sbe41cpCpActive) &&
          difftime(itimer(), AscentControl.TimeStampObs) > 25 * Min)
   {
      vitals.status |= Obs25Min;
      AscentControl.PistonPosition += mission.PistonBuoyancyNudge;
      if (debuglevel>=2 || (debugbits&PROFILE_H))
      {
         LogEntry(FuncName,"Bouyancy nudge to %d (Obs25Min).\n", AscentControl.PistonPosition);
      }
   }

   if (AscentControl.PistonPosition > mission.PistonFullExtension)
   {
      AscentControl.PistonPosition = mission.PistonFullExtension;
   }

   if (PistonPositionAdc() != AscentControl.PistonPosition)
   {
      unsigned short V = 0xfff, A = 0xfff;
      PistonMoveAbsWTO(AscentControl.PistonPosition, &V, &A, TimeOut);

      /* record power consumption during the initial extension period */
      if (PistonPositionAdc() <= AscentControl.InitialExtension && V != 0xfff && A != 0xfff)
      {
         vitals.BuoyancyPumpVolts = V;
         vitals.BuoyancyPumpAmps = A;
      }
   }
   else if (vitals.status & Sbe41cpPUnreliable)
   {
      AscentControl.PistonPosition += mission.PistonBuoyancyNudge;
      if (debuglevel >= 2 || (debugbits & PROFILE_H))
      {
         LogEntry(FuncName,"Bouyancy nudge to %d (Sbe41cpPUnreliable).\n",
                     AscentControl.PistonPosition);
      }
   }
   else if (velocity <= vMin)
   {
      AscentControl.PistonPosition += mission.PistonBuoyancyNudge;
      if (debuglevel >= 2 || (debugbits & PROFILE_H))
      {
         LogEntry(FuncName,"Bouyancy nudge to %d (v=%0.3fdbar/sec).\n",
                     AscentControl.PistonPosition, velocity);
      }
   }

   if (!(vitals.status & Sbe41cpPUnreliable))
   {
      AscentControl.RefTime = t;
      AscentControl.RefPressure = pressure;
   }

   if (AscentControl.PistonPosition>mission.PistonFullExtension)
   {
      AscentControl.PistonPosition=mission.PistonFullExtension;
   }

   return velocity;
}

/*------------------------------------------------------------------------*/
/* function to query the SBE41 ctd for a full sensor scan                 */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41 ctd a full PTS scan.  The mission status
   flags are also maintained.
*/
int GetObs(struct Obs *obs)
{
   int status = -1;

   /* validate the function parameter */
   if (obs)
   {
      /* query the SBE41 for a full PTS scan */
      if ((status=Sbe41cpGetPts(&(obs->p),&(obs->t),&(obs->s)))<=0)
      {
         vitals.status |= Sbe41cpPtsFail;
      }

      /* maintan the mission status flags */
      if      (status==Sbe41cpPedanticExceptn) {vitals.Sbe41cpStatus |= Sbe41cpPtsPedanticExceptn;}
      else if (status==Sbe41cpPedanticFail)    {vitals.Sbe41cpStatus |= Sbe41cpPtsPedanticFail;}   
      else if (status==Sbe41cpRegexFail)       {vitals.Sbe41cpStatus |= Sbe41cpPtsRegexFail;}      
      else if (status==Sbe41cpNullArg)         {vitals.Sbe41cpStatus |= Sbe41cpPtsNullArg;}        
      else if (status==Sbe41cpRegExceptn)      {vitals.Sbe41cpStatus |= Sbe41cpPtsRegExceptn;}     
      else if (status==Sbe41cpNoResponse)      {vitals.Sbe41cpStatus |= Sbe41cpPtsNoResponse;}     
      else if (status<=0)                      {vitals.Sbe41cpStatus |= Sbe41cpPtsUncaughtExceptn;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SBE41 ctd for a pressure measurment              */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41 ctd a pressure measurement.  The mission
   status flags are also maintained.
*/
int GetP(float *p)
{
   /* define the logging signature */
   cc *FuncName = "GetP()";

   /* initialize the return value */
   int status = -1;

   /* validate the function parameter */
   if (p)
   {
      /* query the SBE41 for a pressure measurment */
      if ((status=Sbe41cpGetP(p))!=Sbe41cpOk)
      {
         LogEntry(FuncName,"Sbe41cpPUnreliable: Sbe41cpGetP()=%d\n",status);
         vitals.status |= Sbe41cpPUnreliable;
      }

      /* check for failure of the pressure measurement */
      if (status<Sbe41cpOk) {vitals.status |= Sbe41cpPFail;}

      /* make range checks on pressure */
      if (!((*p)>=-10 && (*p)<=2200))
      {
         /* make the logentry */
         LogEntry(FuncName,"Pressure (%0.1fdbar) out of range: [-10,2200].\n",(*p));

         /* mark the pressure as unreliable */
         vitals.status |= Sbe41cpPUnreliable;
      }

      /* maintan the mission status flags */
      if      (status==Sbe41cpPedanticExceptn) {vitals.Sbe41cpStatus |= Sbe41cpPPedanticExceptn;}
      else if (status==Sbe41cpPedanticFail)    {vitals.Sbe41cpStatus |= Sbe41cpPPedanticFail;}   
      else if (status==Sbe41cpRegexFail)       {vitals.Sbe41cpStatus |= Sbe41cpPRegexFail;}      
      else if (status==Sbe41cpNullArg)         {vitals.Sbe41cpStatus |= Sbe41cpPNullArg;}        
      else if (status==Sbe41cpRegExceptn)      {vitals.Sbe41cpStatus |= Sbe41cpPRegExceptn;}     
      else if (status==Sbe41cpNoResponse)      {vitals.Sbe41cpStatus |= Sbe41cpPNoResponse;}     
      else if (status<=0)                      {vitals.Sbe41cpStatus |= Sbe41cpPUncaughtExceptn;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SBE41cp ctd for a low-power PT measurment        */
/*------------------------------------------------------------------------*/
/**
   This function queries the SBE41cp ctd for a low-power PT measurement.
   The mission status flags are also maintained.
*/
int GetPt(float *p, float *t)
{
   int status = -1;

   /* validate the function parameter */
   if (p && t)
   {
      /* query the SBE41cp for a low-power PT measurment */
      if ((status=Sbe41cpGetPt(p,t))<=0) {vitals.status |= Sbe41cpPtFail;}
                   
      /* maintan the mission status flags */
      if      (status==Sbe41cpPedanticExceptn) {vitals.Sbe41cpStatus |= Sbe41cpPtPedanticExceptn;}
      else if (status==Sbe41cpPedanticFail)    {vitals.Sbe41cpStatus |= Sbe41cpPtPedanticFail;}
      else if (status==Sbe41cpRegexFail)       {vitals.Sbe41cpStatus |= Sbe41cpPtRegexFail;}
      else if (status==Sbe41cpNullArg)         {vitals.Sbe41cpStatus |= Sbe41cpPtNullArg;}
      else if (status==Sbe41cpRegExceptn)      {vitals.Sbe41cpStatus |= Sbe41cpPtRegExceptn;}
      else if (status==Sbe41cpNoResponse)      {vitals.Sbe41cpStatus |= Sbe41cpPtNoResponse;}
      else if (status<=0)                      {vitals.Sbe41cpStatus |= Sbe41cpPtUncaughtExceptn;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* execute tasks during the descent phase of the mission cycle            */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the profile phase
   of the mission cycle.  It returns the recommended sleep period.
*/
time_t Profile(void)
{
   /* define the logging signature */
   cc *FuncName = "Profile()";

   /* define stack-based objects for local use */
   int i,err,sleep=HeartBeat; float p;
   
   if (GetP(&p)>0)
   {
      /* execute surface detection criteria */
      if (SurfaceDetect(p)>0)
      {
         /* terminate the profile phase and initialize telemetry phase */
         ProfileTerminate(); TelemetryInit(); return 5*Min;
      }

      /* check if CP mode should be activated */
      if (!(vitals.Sbe41cpStatus&Sbe41cpCpActive) && p<=mission.PressureCP && p>=50) 
      {
         /* activate CP mode of the SBE41cp */
         for (i=0; i<3 && !(vitals.Sbe41cpStatus&Sbe41cpCpActive); i++)
         {
            if (Sbe41cpStartCP(60)>0) {vitals.Sbe41cpStatus|=Sbe41cpCpActive; break;}

            /* make the logentry */
            else {LogEntry(FuncName,"Initiation of CP mode failed.\n");}
         }
      }

      /* check sampling criteria */
      if (!(vitals.Sbe41cpStatus&Sbe41cpCpActive) && p<=pTable[pTableIndex]+1)
      {
         struct Obs o;
         
         /* collect a full sample */
         if ((err=GetObs(&o))>0)
         {
            if (debuglevel>=2 || (debugbits&PROFILE_H))
            {
                /* make the logentry */
               LogEntry(FuncName,"Sample %d initiated at %0.1fdbars for bin %d [%0.0fdbars].  "
                        "PTS: %0.1fdbars %0.4fC %0.4fPSU\n",vitals.ObsIndex,p,pTableIndex,
                        pTable[pTableIndex],o.p,o.t,o.s);
            }
                                    
            /* assign the observation to the storage array; increment indexes */ 
            if (vitals.ObsIndex<pTableSize) {obs[vitals.ObsIndex]=o; ++vitals.ObsIndex;}

            /* reposition the table pointer */
            if (pTableIndex<(pTableSize-1)) ++pTableIndex;

            /* record a timestamp for this observation */
            AscentControl.TimeStampObs=itimer();
         }

         /* make the logentry */
         else {LogEntry(FuncName,"GetObs()=%d.\n",err);}
      }
   }
 
   /* make the logentry */
   else {LogEntry(FuncName,"GetP() failed.\n");}
   
   /* execute the ascent-rate control agent */
   // return value is positive if a piston nudge was performed
   const float vMin = (mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_ASCENT) ?
     ((float) mission.EMAMinVelocity / 100.0) :
     0.08;
   time_t buoyancyPumpTimeRef = vitals.BuoyancyPumpOnTime;
   float velocity = AscentControlAgent(vMin);

   // collect the em samples for this cycle
   // note we bypass the sleep extension as we need to check in on em often
   // but note in normal operation the ascent control could run the piston for up to 30 seconds
   if (mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_ASCENT)
   {
      if (!(vitals.status & Sbe41cpPUnreliable))
      {
         char empath[32];
         snprintf(empath, sizeof(empath), "%05u.%03dp.efr", mission.FloatId, PrfIdGet());

         // open the file for binary appending
         FILE* emfile = NULL;
         if((emfile = fopen(empath, "ab"))) {
            int count = EMAWriteAllDataFramesToFile(emfile);
            if(count < 0) {
               LogEntry(FuncName, "** Problem collecting em data frames");

            } else {
               LogEntry(FuncName, "** Wrote %d em data frames to file with velocity %0.2f (dbars/sec)\n", count, velocity);
            }

            fclose(emfile);

         } else {
            LogEntry(FuncName, "** Couldn't open em file for binary appending: %s\n", empath);
         }
     }
     if (buoyancyPumpTimeRef < vitals.BuoyancyPumpOnTime)
     {
        // Don't sleep if we moved the piston, since that takes a bit.
        sleep = 0;
     }
     else
     {
        // Do sleep if the piston didn't move, since we need to capture more frames
        // and make an accurate velocity reading.
        sleep = HeartBeat;
     }
   }
   else
   {
      /* evaluate criteria for extending sleep period to save energy */
      if (vitals.Sbe41cpStatus&Sbe41cpCpActive &&
         !(vitals.status&Sbe41cpPUnreliable)  &&
         p>(AscentControl.SurfacePressure+50) &&
         PistonPositionAdc()==AscentControl.PistonPosition) {sleep=5*Min;}
   }

   return sleep;
}

/*------------------------------------------------------------------------*/
/* function to initialize the profile phase of the profile cycle          */
/*------------------------------------------------------------------------*/
/**
   This function initializes the profile phase of the mission cycle.     
*/
int ProfileInit(void)
{
   /* define the logging signature */
   cc *FuncName = "ProfileInit()";

   /* initialize the return value */
   int err,status=1;

   /* define local work objects */
   unsigned int i; float p; unsigned char *b; const float MaxSurfPres=20;

   /* define objected needed for charge consumption model  */
   float charge=NaN(),ocv=BatVolts(ADC);

   /* create an initialization object */
   struct Obs o; o.s=NaN(); o.p=NaN(); o.t=NaN();
   
   /* set the state variable */
   StateSet(PROFILE);
      
   /* save the start time of the profile phase */
   vitals.TimeStartProfile = time(NULL);

   /* exercise the charge consumption model */
   if ((err=ChargeModelCompute(&charge)>0))
   {
      /* make a logentry */
      LogEntry(FuncName,"Charge consumption model: OCV:%0.1fV wake-cycles:%lu "
               "standby:%0.1fdays Q:%0.1fkC\n",ocv,cmodel.WakeCycles,
               (float)cmodel.StandbySec/SecPerDay,cmodel.Coulombs/1000);
   }
   else {LogEntry(FuncName,"Charge consumption model failed. [err=%d]\n",err);}
   
   /* record the accumulated charge consumption */
   vitals.CoulombsProfile=(err>0) ? charge : NaN();

   /* initialize the observation array */
   for (vitals.ObsIndex=0, i=0; i<pTableSize; i++) {obs[i]=o;}

   /* get the current pressure */
   if (GetP(&p)<=0) p=mission.PressureProfile;

   /* locate the pressure in the sample-bin table */
   for (pTableIndex=0; pTableIndex<(pTableSize-1) && pTable[pTableIndex]>p; pTableIndex++) {}

   /* make a log entry */
   if (debuglevel>=2 || (debugbits&PROFILE_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"PrfId:%03d  Pressure:%0.1fdbar  pTable[%d]:%0.0fdbar\n",
               PrfIdGet(),p,pTableIndex,pTable[pTableIndex]);
   }
   
   /* initialize the ascent control structure */
   for (b=(unsigned char *)(&AscentControl),i=0; i<sizeof(AscentControl); i++) b[i]=0;

   /* validate the surface pressure to use for ascent control and surface detect */
   if (vitals.SurfacePressure>MaxSurfPres) AscentControl.SurfacePressure = MaxSurfPres;
   else if (vitals.SurfacePressure<(-MaxSurfPres)) AscentControl.SurfacePressure = -MaxSurfPres;
   else AscentControl.SurfacePressure=vitals.SurfacePressure;

   /* initialize the reference pressure and the time stamps */
   AscentControl.RefPressure=p;
   AscentControl.RefTime=itimer();
   AscentControl.TimeStampObs=AscentControl.RefTime;
   
   /* guard against shallow-water traps */
   if (p<=MaxSurfPres+4)
   {
      unsigned short V=0xfff,A=0xfff;

      if (debuglevel>=2 || (debugbits&PROFILE_H))
      {
         /* make the logentry */
         LogEntry(FuncName,"Shallow water trap detected at %0.2f decibars.\n",p);
      }

      /* set the shallow-water-trap status bit */
      vitals.status |= ShallowWaterTrap;

      /* set the piston position to full extension */
      AscentControl.PistonPosition=mission.PistonFullExtension;
      
      /* nudge the piston toward the target position */
      PistonMoveAbsWTO(AscentControl.PistonPosition,&V,&A,LONG_MAX);

      /* record power consumption during the initial extension period */
      if (V!=0xfff && A!=0xfff) 
      {
         vitals.BuoyancyPumpVolts = V;
         vitals.BuoyancyPumpAmps = A;
      }
   }
   
   /* compute the target piston position for the initial piston extension */
   else AscentControl.PistonPosition = PistonPositionAdc() + mission.PistonInitialBuoyancyNudge;

   /* make sure that the target piston position does not exceed full extension */
   if (AscentControl.PistonPosition>mission.PistonFullExtension)
   {
      AscentControl.PistonPosition=mission.PistonFullExtension;
   }

   /* record the initial piston extension for use with power measurements */
   AscentControl.InitialExtension=AscentControl.PistonPosition;

   /* initialize the CP-mode indicator */
   vitals.Sbe41cpStatus &= ~Sbe41cpCpActive;
   return status;
}

/*------------------------------------------------------------------------*/
/* function to terminate the profile phase of the profile cycle           */
/*------------------------------------------------------------------------*/
int ProfileTerminate(void)
{
   /* define the logging signature */
   cc *FuncName="ProfileTerminate()";
   
   /* initialize return value */
   int i,status=1; FILE *fp; const int prfid=PrfIdGet();

   /* record the time when the profile was terminated */
   vitals.TimeStopProfile=time(NULL);
   
   /* validate the profile pathname */
   if (fnameok(prf_path)<=0) 
   {
      /* create the file to contain the profile data */
      snprintf(prf_path,sizeof(prf_path),"%05u.%03d.msg",mission.FloatId,prfid);
   }
   
   /* open the profile data file */
   if ((fp=fopen(prf_path,"a")))
   {
      /* write the date and time to the profile file */
      time_t t=time(NULL); fprintf(fp,"$ Profile %04u.%03d terminated: %s",
                                   mission.FloatId,prfid,ctime(&t));

      /* write a header */
      fprintf(fp,"$ Discrete samples: %d\n",vitals.ObsIndex+1);

      /* write a column header */         
      fprintf(fp,"$ %7s %8s %8s\n","p","t","s");

      /* write the park-level data */
      if (isNaN(vitals.ParkObs.p)) fprintf(fp,"  %7s","nan");
      else fprintf(fp,"  %7.2f",vitals.ParkObs.p);

      /* write the park-level temperature data */
      if (isNaN(vitals.ParkObs.t)) fprintf(fp," %8s","nan");
      else fprintf(fp," %8.4f",vitals.ParkObs.t);

      /* write the park-level salinity data */
      if (isNaN(vitals.ParkObs.s)) fprintf(fp," %8s","nan");
      else fprintf(fp," %8.4f",vitals.ParkObs.s);
         
      /* write the observation to the data file */
      fprintf(fp," (Park Sample)\n");

      /* loop through each discrete observation */
      for (i=0; i<vitals.ObsIndex; i++)
      {
         if (isNaN(obs[i].p)) fprintf(fp,"  %7s","nan");
         else fprintf(fp,"  %7.2f",obs[i].p);

         if (isNaN(obs[i].t)) fprintf(fp," %8s","nan");
         else fprintf(fp," %8.4f",obs[i].t);

         if (isNaN(obs[i].s)) fprintf(fp," %8s","nan");
         else fprintf(fp," %8.4f",obs[i].s);
         
         /* write the observation to the data file */
         fprintf(fp,"\n");
      }
      
      /* check if CP mode was activated during the profile */
      if (vitals.Sbe41cpStatus&Sbe41cpCpActive)
      {
         /* upload the continuouse profile to the file */
         Sbe41cpUploadCP(fp);
         
         /* reset the SBE41 status byte */
         vitals.Sbe41cpStatus &= ~Sbe41cpCpActive;
      }
      else if (debuglevel>=2 || (debugbits&PROFILE_H))
      {
         LogEntry(FuncName,"Sbe41 CP mode not activated during profile; "
                  "bypassing upload from Sbe41cp to Apf11.\n");
      }
      
      /* close the data file */
      fclose(fp);
   }

   /* log the failure */
   else {LogEntry(FuncName,"Attempt to open \"%s\" failed. "
                  "[errno=%d]\n",prf_path,errno);}

   // turn off the ema device and demodulate the data
   if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_DESCENT || mission.EMASample == EMA_SAMPLE_ASCENT) {

      // done with ema collection so turn off device
      if(mission.EMASample == EMA_SAMPLE_BOTH || mission.EMASample == EMA_SAMPLE_ASCENT) {
         unsigned char embuffer[128];
         int emaresponse = EMASleep(embuffer, 128);
         LogEntry(FuncName, "** Turned off ema, response (%d): %s\n", emaresponse, embuffer);
      }

      // we need to figure out what phases of the dive to demodulate
      char phases[] = {'d', 'p'};

      // if only profile then eliminate the descent
      if(mission.EMASample == EMA_SAMPLE_ASCENT) {
         phases[0] = '\0';

      // and vice versa
      } else if(mission.EMASample == EMA_SAMPLE_DESCENT) {
         phases[1] = '\0';
      }

      // read raw efr reccords into demodulated efp records
      FILE* efrfile = NULL;
      FILE* efpfile = NULL;

      for(int i = 0; i < 2; i += 1) {
         char phase = phases[i];
         
         // skip this phase if not defined
         if(phase == '\0') {
            continue;
         }

         // construct the raw filename
         int profileid = PrfIdGet();
         char empath[32];
         snprintf(empath, sizeof(empath), "%05u.%03d%c.efr", mission.FloatId, profileid, phase);

         // open the raw file for binary reading
         if((efrfile = fopen(empath, "rb"))) {
            
            // contruct the processed filename and open for ascii writing
            snprintf(empath, sizeof(empath), "%05u.%03d%c.efp", mission.FloatId, profileid, phase);
            if((efpfile = fopen(empath, "w"))) {

               // time how long the demodulation takes
               time_t secs;
               unsigned char tics;
               float start;
               float end;

               RtcGet(&secs, &tics);
               start = secs + ((float)tics / 256.0);

               // do the domodulation
               int count = EMADemodulateDataFromFile(efrfile, efpfile, EMA_SAMPLE_WINDOW_SIZE, EMA_SAMPLE_SLIDE_SIZE);

               RtcGet(&secs, &tics);
               end = secs + ((float)tics / 256.0);

               LogEntry(FuncName, "** Wrote %d demodulated records to '%s' in %.1f secs\n", count, empath, end - start);

               // be sure to close both files
               fclose(efrfile);
               fclose(efpfile);

            } else {
               LogEntry(FuncName, "** Problem opening demod file for writing\n");
            }

         } else {
            LogEntry(FuncName, "** Problem opening raw file for reading\n");
         }
      }

      // now that we're done demodulating we need to move the raw file to the archive
      // note this is a bit confusing; it's saying look in the root directory and move
      // anything matching the given pattern to the archive (FatArchive) directory
      // note this call gets logged
      fmove("*.efr", FatRoot);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to apply surface detection criteria                           */
/*------------------------------------------------------------------------*/
/**
   This function applies surface detection criteria in order to detect the
   even-oriented sequence point between the profile and telemetry phases of
   the mission cycle.

      \begin{verbatim}
      input:
         p....The current pressure (decibars) of the float.

      output:
         This function returns a positive value if the surface detection
         criteria have been satisfied; otherwise zero is returned.
      \end{verbatim}
*/
static int SurfaceDetect(float p)
{
   /* define the logging signature */
   cc *FuncName = "SurfaceDetect()";

   int err,i,status=0;
   
   /* check nonpedantic surface detection criteria */
   if (p<=4 || p<=(AscentControl.SurfacePressure+4))
   {
      /* a pause is necessary to ensure that the SBE41cp will wake-up */
      Wait(1000);
      
      /* check pedantic surface detection criteria */
      if ((err=GetP(&p))==Sbe41cpOk && (p<=4 || p<=(AscentControl.SurfacePressure+4)))
      {
         /* record the piston position when surface detected */
         vitals.SurfacePistonPosition=PistonPositionAdc();
      
         /* log the surface detection */
         if (debuglevel>=2 || (debugbits&PROFILE_H))
         {
            /* make the logentry */
            LogEntry(FuncName,"SurfacePressure:%0.1fdbars Pressure:%0.1fdbars "
                     "PistonPosition:%d\n",AscentControl.SurfacePressure,p,
                     vitals.SurfacePistonPosition);
         }

         /* check if the CTD is in CP mode */
         if (vitals.Sbe41cpStatus&Sbe41cpCpActive)
         {
            /* deactivate the SBE41CP's CP mode */
            for (i=0; i<3 && (vitals.Sbe41cpStatus&Sbe41cpCpActive); i++)
            {
               if (Sbe41cpStopCP()>0) {break;}

               /* make the logentry */
               else {LogEntry(FuncName,"Deactivation CP mode failed.\n");}
            }
         }
      
         /* if the pressure is unreliable or greater than 5dbar then move to full extension */
         if (p>5 || vitals.status&Sbe41cpPUnreliable)
         {
            AscentControl.PistonPosition=mission.PistonFullExtension;
         }
      
         /* otherwise, add some buoyancy */
         else AscentControl.PistonPosition = vitals.SurfacePistonPosition +
                 mission.PistonInitialBuoyancyNudge;

         /* move the piston to the computed position */
         PistonMoveAbs(AscentControl.PistonPosition);
      
         /* indicate that the surface was detected */
         status=1;
      }

      /* check for notification of surface-detection failure */
      else if (debuglevel>=2 || (debugbits&PROFILE_H))
      {
         /* make the logentry */
         LogEntry(FuncName,"Pedantic surface detection criteria failed: "
                  "[err:%d] [p:%0.2f].\n",err,p);
       }
   }
      
   /* check if the piston is fully extended before the surface is detected */
   else if (PistonPositionAdc()>=mission.PistonFullExtension)
   {
      if ((debuglevel>=2 || (debugbits&PROFILE_H)) && !(vitals.status&PistonFullExt))
      {
         /* make the logentry */
         LogEntry(FuncName,"Piston fully extended before surface detected.\n");
      }
      
      /* assert a status bit that indicates the piston is at full extension */
      vitals.status|=PistonFullExt;
   }
   
   return status;
}

#endif /* PROFILE_C */
