#ifndef LBT9523_H
#define LBT9523_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define lbt9523ChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>
#include <time.h>

/* prototypes for functions with external linkage */
int IrGeoLoc(const struct SerialPort *port, float *x, float *y,
             float *z, float *lon, float *lat, unsigned long int *t);
int IrModemAttention(const struct SerialPort *port);
int IrModemConfigure(const struct SerialPort *port);
int IrModemFwRev(const struct SerialPort *port, char *FwRev, size_t size);
int IrModemIccid(const struct SerialPort *port,char *iccid,size_t size);
int IrModemImei(const struct SerialPort *port, char *imei, size_t size);
int IrModemModel(const struct SerialPort *port, char *model, size_t size);
int IrModemRegister(const struct SerialPort *port);
int IrSignalStrength(const struct SerialPort *port);
int IrSkySearch(const struct SerialPort *port);
const char *IrUnixEpoch(unsigned long int IrSysTime, time_t *epoch);

#endif /* LBT9523_H */
#ifdef LBT9523_C
#undef LBT9523_C

#include <assert.h>
#include <chat.h>
#include <earth.h>
#include <logger.h>
#include <modem.h>
#include <nan.h>
#include <regex.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* declare functions with external linkage */
time_t itimer(void);
int    ModemEnable(long int BaudRate);
int    ModemDisable(void);
int    snprintf(char *str, size_t size, const char *format, ...);
int    Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* function to query the LBT9523 for geolocation data                     */
/*------------------------------------------------------------------------*/
/**
   This function queries the LBT9523 for geolocation data.  Iridium
   uses an Earth-Centered Earth-Fixed (ECEF) Cartesian reference frame
   where its coordinates <x,y,z> are expressed in kilometers.
   Iridium's coordinates <x>, <y>, <z> form a geolocation grid code
   within the ECEF coordinate system, using dimensions, x, y, and z,
   to specify location. The coordinate system is aligned such that the
   z-axis is a ligned with the north and south poles, leaving the
   x-axis and y-axis to lie in the plane containing the equator.  The
   axes are aligned such that at 0 degrees latitude and 0 degrees
   longitude, both y and z are zero and x is positive (x = +6376km,
   representing the nominal earth radius in kilometers).  Each
   dimension of the geolocation grid code is displayed in decimal form
   using units of kilometres.  Each dimension of the geolocation grid
   code has a minimum value of –6376km, a maximum value of +6376km,
   and a resolution of 4km.

   \begin{verbatim}
   input:

      port....The serial port object used to communicate with the LBT.

   output:
      x.....The <x> coordinate (kilometers) of the ECEF frame.
      y.....The <y> coordinate (kilometers) of the ECEF frame.
      z.....The <z> coordinate (kilometers) of the ECEF frame.
      lon...The longitude of the Iridium fix.
      lat...The geodetic latitude of the Iridium fix.
      t.....The Iridium system time is measured in 90msec tics
            relative to the Iridium reference epoch.  The Iridium
            Epoch rolls-over approximately every 12 years.  The
            current Iridium reference epoch is May 11, 2014, 14:23:55.

      On success, this function returns a positive value.  Zero
      indicates that an Iridium fix is not available.  A negative
      value indicates that an exception was encountered.
   \end{verbatim}
*/
int IrGeoLoc(const struct SerialPort *port, float *x, float *y,
             float *z, float *lon, float *lat, unsigned long int *t)
{
   /* define the logging signature */
   cc *FuncName = "IrGeoLoc()";

   /* initialize the return value */
   int status=-1;
   
   /* validate the function arguments */
   if (!port || !x || !y || !z || !lat || !lon || !t)
   {
      LogEntry(FuncName,"NULL function argument.\n");
   }

   /* get the LBTs attention */
   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }

   else
   {
      /* set the timeout period (sec) for chat sessions */
      const time_t timeout = 15;
      
      /* reinitialize the return value */
      status=0;

      /* initialize function arguments */
      (*x)=NaN(); (*y)=NaN(); (*z)=NaN(); (*lon)=NaN(); (*lat)=NaN();

      /* check if the modem is an iridium LBT */
      if (chat(port,"AT I4","IRIDIUM",timeout,"\r")>0)
      {
         #define NSUB (4)
         #define KM  "[ \t]*(-?[0-9]+)"
         #define HEX "[ \t]*([A-Fa-f0-9]+)"
         
         /* construct the regex pattern string for iridium ECEF geolocation data */ 
         const char *pattern = "-MSGEO:" KM "," KM "," KM "," HEX;

         /* define stack-based objects for the retry loop */
         const int NRetry=3; int n,err;

         /* define stack-based objects for the regex parser */
         char buf[64]; int errcode; regex_t regex; regmatch_t regs[NSUB+1];

         /* compile the option pattern */
         assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(NSUB==regex.re_nsub);

         /* register the modem to refresh the iridium geolocation data */
         for (n=0; n<NRetry; n++) {if (IrModemRegister(port)>0) {break;}}

         /* implement a retry loop to query the LBT for geolocation data */
         for (status=-2,n=0; n<NRetry && status<0; n++)
         {
            /* flush IO buffers */
            pflushio(port);
            
            /* send the command to query LBT for geolocation data */
            pputs(port,"AT-MSGEO",2,"\r\n");

            /* loop to receive and analyze LBT response */
            while (pgets(port,buf,sizeof(buf),timeout,"\r\n")>0)
            {
               /* check if no gelocation data are available */
               if (strstr(buf,"-MSGEO: no geolocation data available"))
               {
                  /* make a logentry */
                  LogEntry(FuncName,"Iridium geolocation data not available.\n");

                  /* indicate failure */
                  status=0; break;
               }

               /* check if the current line matches the regex */
               else if (!(errcode=regexec(&regex,buf,regex.re_nsub+1,regs,0)))
               {
                  /* log the LBT response that contains geolocation data */
                  if (debuglevel>=3 || (debugbits&LBT9523_H)) {LogEntry(FuncName,"%s\n",buf);}

                  /* extract iridium geolocation data */
                  buf[regs[1].rm_eo]=0; (*x)=atof(buf+regs[1].rm_so);
                  buf[regs[2].rm_eo]=0; (*y)=atof(buf+regs[2].rm_so);
                  buf[regs[3].rm_eo]=0; (*z)=atof(buf+regs[3].rm_so);
                  buf[regs[4].rm_eo]=0; (*t)=strtoul(buf+regs[4].rm_so,NULL,16);

                  /* compute latitude and longitude from iridium geolocation data */
                  if ((err=Xyz2Geo(*x,*y,*z,lat,lon,NULL))>0)
                  {
                     /* make a logentry of geographical coordinates */
                     LogEntry(FuncName,"<x,y,z>=<%0.0fkm,%0.0fkm,%0.0fkm> lat=%0.3f "
                              "lon=%0.3f IrSysTime=0x%8lx\n",
                              *x,*y,*z,*lat,*lon,*t);
                     
                     /* indicate success */
                     status=1; break;
                  }

                  /* make a logentry of the failure of Xyz2Geo() */
                  else {LogEntry(FuncName,"Failed: <x,y,z>=<%0.0fkm,%0.0fkm,%0.0fkm> "
                                 "t=0x%8lx [err=%d]\n",*x,*y,*z,*t,err);}
               }

               /* add LBT response to engineering logs */
               else if (debuglevel>=4 || (debugbits&LBT9523_H)) {LogEntry(FuncName,"%s\n",buf);}
            }
         }
               
         /* clean up the regex pattern buffer */
         regfree(&regex);
      }

      /* make a logentry that the modem is not an LBT */
      else {LogEntry(FuncName,"Iridium geolocation bypassed.\n");}
   }

   return status;
   #undef NSUB
   #undef KM
   #undef HEX
}

/*------------------------------------------------------------------------*/
/* function to power-up the modem and get its attention                   */
/*------------------------------------------------------------------------*/
/**
   This function powers-up the iridium modem and ensures that it will
   respond to AT commands.  It was created because experience shows that
   under some circumstances the LBT must be power-cycled before it will
   respond to commands.  On success (ie., the modem responds to AT
   commands), then this function returns a positive value.  On failure (ie.,
   the modem fails to respond to AT commands), then this function returns
   zero.  A negative return value indicates an invalid serial port.
*/
int IrModemAttention(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "IrModemAttention()";

   /* initialize the return value */
   int i,j,status = -1;

   /* validate the function argument */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* fault-tolerance loop to get the LBT9523's attention */
   else for (status=0,i=1; !status && i<=4; i++)
   {
      /* allow for autobaud detection */ 
      for (j=0; j<3; j++) {port->putb('\r'); Wait(200);} port->ioflush();

      for (j=0; j<3; j++)
      {
         /* get the LBT9523's attention */
         if (chat(port,"AT","OK",2,"\r")>0) {status=1; break;}
      }

      /* cycle power if modem does not respond */
      if (!status && i<4) {ModemDisable(); sleep(i*15); ModemEnable(19200);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the LBT9523                                      */
/*------------------------------------------------------------------------*/
int IrModemConfigure(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "IrModemConfigure()";

   /* initialize the return value */
   int status=-1;

   /* validate the function argument */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }

   else
   {
      /* set the timeout period (sec) for chat sessions */
      const time_t timeout = 3;
      
      /* reinitialize the return value */
      status=1;

      /* initialize the modem configuration */
      if (modem_initialize(port)>0)
      {
         /* check if the modem is an iridium LBT */
         if (chat(port,"AT I4","IRIDIUM",timeout,"\r")>0)
         {
            /* set the baud rate to be 19200 (fixed) */
            if (chat(port,"AT +IPR=6,0","OK",timeout,"\r")>0)
            {
               /* log the message */
               if (debuglevel>=2 || (debugbits&LBT9523_H))
               {
                  LogEntry(FuncName,"Modem configured for fixed (19200) baud rate.\n");
               }
            }
            
            /* log the message */
            else {LogEntry(FuncName,"Attempt to configure modem for "
                           "fixed baud rate failed.\n"); status=0;}
         }

         /* store the configuration in the modem's NVRAM */
         if (chat(port,"AT &W0 &Y0","OK",timeout,"\r")>0)
         {
            /* log the message */
            if (debuglevel>=2 || (debugbits&LBT9523_H))
            {
               LogEntry(FuncName,"Configuration saved to modem's NVRAM.\n");
            }
         }

         /* log the message */
         else {LogEntry(FuncName,"Attempt to save configuration to "
                        "modem's NVRAM failed.\n"); status=0;}
      }

      /* log the message */
      else {LogEntry(FuncName,"Attempt to initialize "
                     "modem failed.\n"); status=0;}
   }
   
   if (status>0)
   {
      /* log the message */
      if (debuglevel>=2 || (debugbits&LBT9523_H))
      {
         LogEntry(FuncName,"Modem configuration successful.\n");
      }
   }
   
   /* log the message */
   else {LogEntry(FuncName,"Modem configuration failed.\n");}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the LBT for its firmware revision                    */
/*------------------------------------------------------------------------*/
/**
   This function queries the LBT for its firmware.

      \begin{verbatim}
      input:

         port....A structure that contains pointers to machine dependent
                 primitive IO functions.  See the comment section of the
                 SerialPort structure for details.  The function checks to
                 be sure this pointer is not NULL.
         size....The size of the 'FwRev' buffer used to store the firmware
                 revision.  This buffer must be at least 16 bytes long.

      output:

         FeRev...The firmware revision of the LBT is stored in this
                 buffer.

         This function returns a positive value on success and a zero on
         failure.  A negative negative value indicates an invalid argument.
     \end{verbatim}
*/
int IrModemFwRev(const struct SerialPort *port, char *FwRev, size_t size)
{
   /* define the logging signature */
   cc *FuncName = "IrModemFwRev()";

   /* initialize return value */
   int status=-1;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the buffer */
   else if (!FwRev || size<16)
   {
      LogEntry(FuncName,"FwRev buffer NULL or too small.\n");
   }

   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }
 
   else
   {
      char *p,buf[64];

      /* define the key that introduces the firmware revision */
      const char *key="Call Processor Version: ";

      /* compute the length of the key */
      const int len=strlen(key);

      /* reinitialize return value */
      status=0;
      
      /* flush the IO port buffers */
      usleep(50000U); assert(port->ioflush && port->ioflush()>0);

      /* transmit the command to the LBT */
      if (chat(port,"AT+CGMR","AT+CGMR",5,"\r")<=0)
      {
         LogEntry(FuncName,"LBT not responding.\n");
      }

      /* read each line of output */
      else while (status<=0 && pgets(port,buf,sizeof(buf),5,"\r\n")>0)
      {
         /* scan the current line for the key */
         if (strlen(buf)>0 && (p=strstr(buf,key)))
         {
            /* copy the FwRev number to the output buffer */
            strncpy(FwRev,buf+len,size); FwRev[size-1]=0; status=1; break;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the LBT for the ICCID number                         */
/*------------------------------------------------------------------------*/
/**
   This function queries the LBT for its ICCID number (ie., SIM card serial
   number).

      \begin{verbatim}
      input:

         port....A structure that contains pointers to machine dependent
                 primitive IO functions.  See the comment section of the
                 SerialPort structure for details.  The function checks to
                 be sure this pointer is not NULL.

         size....The size of the 'iccid' buffers use to store the ICCID
                 number.  This buffer must be at least 20 bytes long.

      output:

         iccid....The 19-digit ICCID number of the sim card is stored in
                  this buffer.  Also known as the SIM card serial number.

         This function returns a positive value on success or zero if an
         invalid ICCID string was read from the SIM card.  A negative
         value indicates that an exception was encountered.
     \end{verbatim}
*/
int IrModemIccid(const struct SerialPort *port,char *iccid,size_t size)
{
   /* define the logging signature */
   cc *FuncName = "IrModemIccid()";

   /* initialize return value */
   int status=-1;

   if (iccid) *iccid=0;
   
   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the buffer */
   else if (!iccid || size<20)
   {
      LogEntry(FuncName,"ICCID buffer NULL or too small.\n");
   }

   /* induce the modem into a communicative state */
   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }

   else
   {
      char buf[32];

      /* reinitialize return value */
      status=-1;
      
      /* flush the IO port buffers */
      Wait(5000); assert(port->ioflush && port->ioflush()>0);

      /* transmit the command to the LBT */
      if (chat(port,"AT+CICCID","AT+CICCID",5,"\r")<=0)
      {
         LogEntry(FuncName,"LBT not responding.\n");
      }

      /* read each line of output */
      else while (pgets(port,buf,sizeof(buf),5,"\r\n")>0)
      {
         /* scan the current line for a 19 digit number */
         if (strlen(buf)>0 && strspn(buf,"0123456789")==19)
         {
            /* copy the ICCID number to the output buffer */
            strncpy(iccid,buf,size); iccid[size-1]=0; status=1; break;
         }

         /* check for NAK response from LBT */
         else if (strstr(buf,"Failed to read ICCID"))
         {
            /* report the NAK */
            strncpy(iccid,"Read failure. No SIM card?",size);
            iccid[size-1]=0; status=0; break;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the LBT for the IMEI number                          */
/*------------------------------------------------------------------------*/
/**
   This function queries the LBT for its IMEI number.

      \begin{verbatim}
      input:

         port....A structure that contains pointers to machine dependent
                 primitive IO functions.  See the comment section of the
                 SerialPort structure for details.  The function checks to
                 be sure this pointer is not NULL.
         size....The size of the 'imei' buffer used to store the IMEI
                 number.  This buffer must be at least 16 bytes long.

      output:

         imei....The 15-digit IMEI number of the LBT is stored in this
                 buffer.

         This function returns a positive value on success and a zero on
         failure.  A negative negative value indicates an invalid argument.
     \end{verbatim}
*/
int IrModemImei(const struct SerialPort *port, char *imei, size_t size)
{
   /* define the logging signature */
   cc *FuncName = "IrModemImei()";

   /* initialize return value */
   int status=-1;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the buffer */
   else if (!imei || size<16)
   {
      LogEntry(FuncName,"IMEI buffer NULL or too small.\n");
   }
   
   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }

   else
   {
      char buf[32];

      /* reinitialize return value */
      status=0;
      
      /* flush the IO port buffers */
      usleep(50000U); assert(port->ioflush && port->ioflush()>0);

      /* transmit the command to the LBT */
      if (chat(port,"AT+CGSN","AT+CGSN",5,"\r")<=0)
      {
         LogEntry(FuncName,"LBT not responding.\n");
      }

      /* read each line of output */
      else while (status<=0 && pgets(port,buf,sizeof(buf),5,"\r\n")>0)
      {
         /* scan the current line for a 15 digit number */
         if (strlen(buf)>0 && strspn(buf,"0123456789")==15)
         {
            /* copy the IMEI number to the output buffer */
            strncpy(imei,buf,size); imei[size-1]=0; status=1; break;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the LBT for the IMEI number                          */
/*------------------------------------------------------------------------*/
/**
   This function queries the LBT for its IMEI number.

      \begin{verbatim}
      input:

         port....A structure that contains pointers to machine dependent
                 primitive IO functions.  See the comment section of the
                 SerialPort structure for details.  The function checks to
                 be sure this pointer is not NULL.
         size....The size of the 'model' buffer used to store the LBT model
                 identifier.  This buffer must be at least 16 bytes long.

      output:

         model....The LBT model identifier.

         This function returns a positive value on success and a zero on
         failure.  A negative negative value indicates an invalid argument.
     \end{verbatim}
*/
int IrModemModel(const struct SerialPort *port, char *model, size_t size)
{
   /* define the logging signature */
   cc *FuncName = "IrModemModel()";

   /* initialize return value */
   int status=-1;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   /* validate the buffer */
   else if (!model || size<16)
   {
      LogEntry(FuncName,"Model buffer NULL or too small.\n");
   }
  
   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }
 
   else
   {
      char buf[32];

      /* reinitialize return value */
      status=0;
      
      /* flush the IO port buffers */
      usleep(50000U); assert(port->ioflush && port->ioflush()>0);

      /* transmit the command to the LBT */
      if (chat(port,"AT+CGMM","AT+CGMM",5,"\r")<=0)
      {
         LogEntry(FuncName,"LBT not responding.\n");
      }

      /* read each line of output */
      else while (status<=0 && pgets(port,buf,sizeof(buf),5,"\r\n")>0)
      {
         /* scan the current line for the model number */
         if (strlen(buf)>0 && strcspn(buf," \t\r\n")>0)
         {
            /* copy the model to the output buffer */
            strncpy(model,buf,size); model[size-1]=0; status=1; break;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to register the modem with the iridium system                 */
/*------------------------------------------------------------------------*/
/**
   This function attempts to register the LBT with the iridium system.  On
   success, this function returns a positive value.  Zero is returned if the
   registration attempt failed.  A negative return value indicates an
   invalid serial port.
*/
int IrModemRegister(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "IrModemRegister()";

   /* initialize the return value */
   int status=-1;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}

   else if ((status=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n");
   }
   
   /* check if the modem is an iridium modem */
   else if (chat(port,"AT I4","IRIDIUM",10,"\r")>0)
   {
      /* initialize parameters of the timeout mechanism */
      const time_t TimeOut=120, To=time(NULL);

      /* reinitialize the return value */
      status=0;

      do
      {
         /* flush the serial port buffers */
         if (port->ioflush) port->ioflush();

         /* request the LBT9523's registration status */
         if (pputs(port,"AT+CREG?",2,"\r")>0)
         {
            char *p,buf[32];

            /* read the next line from the serial port */
            while (pgets(port,buf,sizeof(buf)-1,2,"\r\n")>0)
            {
               if (debuglevel>=3 || (debugbits&LBT9523_H)) 
               {
                  LogEntry(FuncName,"Received: %s\n",buf);
               }

               /* search for the second field of the expected response string */
               if ((p=strstr(buf,"+CREG:")) && (p=strchr(p,',')))
               {
                  /* get the value of the second field */
                  int s = atoi(++p);

                  /* check for registration */
                  if (s==1 || s==5) {status=1; break;}
               }
            }

            /* pause before retry */
            sleep(5);
         }
      }

      /* check termination criteria */
      while (!status && difftime(time(NULL),To)<TimeOut);
   }

   /* make a logentry that the modem is not an LBT */
   else if (debuglevel>=3 || (debugbits&LBT9523_H)) 
   {
      LogEntry(FuncName,"Modem is not an Iridium LBT.\n"); 
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the iridium modem for signal strength                */
/*------------------------------------------------------------------------*/
/**
   This function queries the Iridium LBT for signal strength.  A time-out
   feature (2 minutes) is implemented to prevent this function waiting
   indefinitely for the LBT to respond.
   
      \begin{verbatim}
      input:

         port.........A structure that contains pointers to machine
                      dependent primitive IO functions.  See the comment
                      section of the SerialPort structure for details.  The
                      function checks to be sure this pointer is not NULL.
         
      output:

         This function returns the average of up to three responses to the
         +CSQ command.  A two second delay follows each response before the
         next +CSQ command is sent to the LBT.  On success, this function
         returns a non-negative number representing the average number of
         decibars (0-50) that would be visible on the LCD screen of an Iridium
         phone.  A negative return value indicates an exception was
         encountered. 
      \end{verbatim}
*/
int IrSignalStrength(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "IrSignalStrength()";

   /* initialize the return value */
   int err=0,status=0;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n"); status=-1;}
   
   else if ((err=IrModemAttention(port))<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Modem not responding.\n"); status=err;
   }

   /* check if the modem is an iridium modem */
   else if (chat(port,"AT I4","IRIDIUM",10,"\r")>0)
   {
      /* initialize parameters of the timeout mechanism */
      const time_t TimeOut=120, To=time(NULL);

      /* initialize averaging parameters */
      int n=0; status=0;

      /* pause before starting */
      sleep(1);
           
      do
      {
         /* pause before (re)try */
         sleep(1);

         /* flush the serial port buffers */
         if (port->ioflush) port->ioflush();

         /* request the LBT9523's registration status */
         if (pputs(port,"AT+CSQ",2,"\r")>0)
         {
            char *p,buf[32];

            /* read the next line from the serial port */
            while (pgets(port,buf,sizeof(buf)-1,5,"\n")>0)
            {
               if (debuglevel>=3 || (debugbits&LBT9523_H)) 
               {
                  LogEntry(FuncName,"Received: %s\n",buf);
               }

               /* search for the expected response string */
               if ((p=strstr(buf,"+CSQ:")))
               {
                  /* get the value of the argument */
                  status+=atoi(p+5); n++;
               }
            }
         }
      }

      /* check termination criteria */
      while (n<3 && difftime(time(NULL),To)<TimeOut);

      /* compute the average signal strength */
      if (n) status=(status*10)/n;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to search the sky for the iridium constellation               */
/*------------------------------------------------------------------------*/
/**
   This function implements a method for ensuring that the sky is visible.
   The LBT is powered up and an attempt is made to register with the iridium
   system.  The power is cycled up, down, and then back up again in order to
   work around a bug in the Sebring and Daytona LBTs.  After being powered
   down for a long time (days), most units will not respond to AT commands
   on the first power-up.  However, the modem will respond if the LBT is
   powered down for 2 seconds and then powered up a second time.
   
      \begin{verbatim}
      input:

         port.........A structure that contains pointers to machine
                      dependent primitive IO functions.  See the comment
                      section of the SerialPort structure for details.  The
                      function checks to be sure this pointer is not NULL.
         
      output:

         This function returns a positive value if the sky is visible (ie.,
         the LBT is able to register).  Zero is returned if the registration
         attempt failed.  A negative return value indicates an invalid
         serial port.
      \end{verbatim}
*/
int IrSkySearch(const struct SerialPort *port)
{
   /* define the logging signature */
   cc *FuncName = "IrSkySearch()";

   /* initialize the return value */
   int status=-1;

   /* validate the port */
   if (!port) {LogEntry(FuncName,"NULL serial port.\n");}
   
   else
   {
      /* enable the modem */
      ModemEnable(19200);

      /* use modem registration as proof of sky visibility */
      status=IrModemRegister(port); 
       
      /* deactivate the modem serial port */
      ModemDisable();
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to convert iridium system time to unix epoch                  */
/*------------------------------------------------------------------------*/
/**
   This function converts the iridium system time to the Unix
   epoch. The Iridium system time is measured in 90msec tics relative
   to the Iridium reference epoch.  The Iridium system time rolls-over
   approximately every 12 years.  The current Iridium reference epoch
   is May 11, 2014, 14:23:55.

   \begin{verbatim}
   input:
      IrSysTime...The iridium system time expressed as the number of
                  90msec tics since the Iridium reference epoch.  

   output:
      epoch......The Unix epoch that corresponds to the Iridium system
                 time (truncated to one second resolution).

      This function returns a string that represents the calendar
      date/time that corresponds to the Iridium system time.
   \end{verbatim}
*/
const char *IrUnixEpoch(unsigned long int IrSysTime, time_t *epoch)
{
   /* reference for iridium epoch is May 11, 2014 14:23:55UTC */
   struct tm IrEpochRef = {55,23,14,11,4,114,0,0,0};

   /* compute the unix epoch */
   const time_t t = mktime(&IrEpochRef) + (IrSysTime*0.09);

   /* compute the calendar date & time */
   static char buf[32]; strftime(buf,sizeof(buf),"%m/%d/%Y %T",gmtime(&t));

   /* store the unix epoch in the function argument */
   if (epoch) {(*epoch)=t;}
   
   return buf;
}

#endif /* LBT9523_C */
