#ifndef PARK_H
#define PARK_H (0x0020U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define parkChangeLog "$RCSfile$  $Revision$  $Date$"

/* function prototypes */
int Park(void);
int ParkInit(void);
int ParkTerminate(void);

#endif /* PARK_H */
#ifdef PARK_C
#undef PARK_C

#include <apf11ad.h>
#include <control.h>
#include <crc16bit.h>
#include <ctdio.h>
#include <ds2740.h>
#include <eeprom.h>
#include <engine.h>
#include <logger.h>
#include <nan.h>
#include <profile.h>
#include <sbe41cp.h>
#include <Stm32f103Rtc.h>
#include <unistd.h>

/*------------------------------------------------------------------------*/
/* execute tasks during the park phase of the mission cycle               */
/*------------------------------------------------------------------------*/
/**
   This function executes regularly scheduled tasks during the park phase
   of the mission cycle.  
*/
int Park(void)
{
   /* define the logging signature */
   cc *FuncName = "Park()";

   /* define local objects */
   int errcode,status=1; float p;

   if ((errcode=GetP(&p))>0)
   {
      float P,T; FILE *fp;

      /* define the deadband for the target park level */
      const float MaxErr=10;
      const float MaxP=mission.PressurePark+MaxErr;
      const float MinP=mission.PressurePark-MaxErr;

      /* collect the park-level temperature data */
      if ((errcode=GetPt(&P,&T))>0)
      {
         /* check logging criteria */
         if (debuglevel>=3 || (debugbits&PARK_H))
         {
            /* write the PT sample to the log file */
            LogEntry(FuncName,"PT: %0.2fdbars %0.4fC\n",P,T);
         }

         /* open the profile data file */
         if ((fp=fopen(prf_path,"a")))
         {
            /* initialize timing variables */
            time_t t=time(NULL),it=itimer();
            
            /* write the date and time */
            char buf[32]; strftime(buf,sizeof(buf),"%b %d %Y %H:%M:%S",gmtime(&t));

            /* write the PT sample to the profile file */
            fprintf(fp,"ParkPt:    %s %11ld %7ld %7.2f %7.4f\n",buf,t,it,P,T);
            
            /* close the data file */
            fclose(fp);
         }
         else {LogEntry(FuncName,"Open failed for: %s\n",prf_path);}
      }
      else {LogEntry(FuncName,"Low-power PT sample failed [err=%d].\n",errcode);}

      /* check if the pressure is out-of-band on the low side */
      if (p<MinP)
      {
         /* maintain a record of out-of-band reports */
         if (vitals.ParkPOutOfBand<=0) --vitals.ParkPOutOfBand;
         else vitals.ParkPOutOfBand=-1;

         /* check active ballasting criteria */
         if (vitals.ParkPOutOfBand<=-3)
         {
            if (debuglevel>=2 || (debugbits&PARK_H))
            {
               /* make the logentry */
               LogEntry(FuncName,"ParkPOutOfBand[%d, %0.1f dbars]: retract piston.\n",
                        vitals.ParkPOutOfBand,p);
            }
            
            /* retract the piston one count to drive the float deeper */
            if (PistonMoveRel(-1)>0) vitals.ActiveBallastAdjustments++;

            /* reset the out-of-band counter */
            vitals.ParkPOutOfBand=0;
         }
      }

      /* check if the pressure is out-of-band on the high side */
      else if (p>MaxP)
      {
         /* maintain a record of out-of-band reports */
         if (vitals.ParkPOutOfBand>=0) ++vitals.ParkPOutOfBand;
         else vitals.ParkPOutOfBand=1;

         /* check active ballasting criteria */
         if (vitals.ParkPOutOfBand>=3)
         {
            if (debuglevel>=2 || (debugbits&PARK_H))
            {
               /* make the logentry */
               LogEntry(FuncName,"ParkPOutOfBand[%d, %0.1f dbars]: extend piston.\n",
                        vitals.ParkPOutOfBand,p);
            }
            
            /* extend the piston one count to drive the float shallower */
            if (PistonMoveRel(1)>0) vitals.ActiveBallastAdjustments++;

            /* reset the out-of-band counter */
            vitals.ParkPOutOfBand=0;
         }
      }

      /* pressure is in-band; reset the out-of-band counter*/
      else vitals.ParkPOutOfBand=0;

      if (debuglevel>=3 || (debugbits&PARK_H))
      {
         /* make the logentry */
         LogEntry(FuncName,"ParkPOutOfBand[%d, %0.1f dbars]\n",
                  vitals.ParkPOutOfBand,p);
      }
   }

   /* make the logentry */
   else {LogEntry(FuncName,"Active ballast attempt aborted; "
                  "pressure query failed. [err=%d]\n",errcode);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the park phase of the profile cycle             */
/*------------------------------------------------------------------------*/
/**
   This function initializes the park phase of the mission cycle.     
*/
int ParkInit(void)
{
   /* define the logging signature */
   cc *FuncName = "ParkInit()";

   /* initialize the return value */
   int err,status=1; 

   /* define objected needed for charge consumption model  */
   float charge=NaN(),ocv=BatVolts(ADC);

   /* set the state variable */
   StateSet(PARK);
   
   /* save the start time of the park phase */
   vitals.TimeStartPark = time(NULL);

   /* exercise the charge consumption model */
   if ((err=ChargeModelCompute(&charge)>0))
   {
      /* make a logentry */
      LogEntry(FuncName,"Charge consumption model: OCV:%0.1fV wake-cycles:%lu "
               "standby:%0.1fdays Q:%0.1fkC\n",ocv,cmodel.WakeCycles,
               (float)cmodel.StandbySec/SecPerDay,cmodel.Coulombs/1000);
   }
   else {LogEntry(FuncName,"Charge consumption model failed. [err=%d]\n",err);}
   
   /* record the accumulated charge consumption */
   vitals.CoulombsPark=(err>0) ? charge : NaN();
   
   /* initialize the active ballasting record */
   vitals.ParkPOutOfBand=0;
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to terminate the park phase of the profile cycle              */
/*------------------------------------------------------------------------*/
/**
   This function terminates the park phase of the mission cycle.     
*/
int ParkTerminate(void)
{
   /* define the logging signature */
   cc *FuncName = "ParkTerminate()";

   /* define local work objects */
   int err,status=1; unsigned short V,A; struct Obs o;

   /* define the timeout for measuring the CTD power consumption */
   const time_t TimeOut = 60;

   /* initialize the observation */
   o.s=NaN(); o.t=NaN(); o.p=NaN();

   /* copy the park piston position to the mission configuration */
   mission.PistonParkPosition = PistonPositionAdc();
      
   /* recompute the signature of the mission configuration */
   mission.crc = Crc16Bit((unsigned char *)(&mission), sizeof(mission)-sizeof(mission.crc));

   /* measure the power consumption by the SBE41 */
   CtdPower(&V,&A,TimeOut); vitals.Sbe41cpVolts=V; vitals.Sbe41cpAmps=A;

   /* measure the internal vacuum at the park depth */
   vitals.Vacuum = BarometerAdc();

   /* pause to allow DS2740 to stabilize, then measure the battery voltage and current */
   sleep(10); vitals.QuiescentVolts=BatVoltsAdc(); vitals.QuiescentAmps=BatAmpsAdc();

   /* measure the humidity */
   vitals.HumidityAdcPark=HumidityAdc();

   /* make a log entry */
   if (debuglevel>=2 || (debugbits&PARK_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"Piston Position:%d Vacuum:%u HumidityAdc:%hu(%0.1f%%) "
               "Vq:%u Aq:%u Vsbe:%u Asbe:%u\n",mission.PistonParkPosition,
               vitals.Vacuum,vitals.HumidityAdcPark,Humidity(vitals.HumidityAdcPark),
               vitals.QuiescentVolts,vitals.QuiescentAmps,vitals.Sbe41cpVolts,
               vitals.Sbe41cpAmps);
   }
   
   /* necessary pause for CTD wakeup */
   else sleep(1);

   /* measure PTS at the park depth */
   if ((err=GetObs(&o))!=Sbe41cpOk) {LogEntry(FuncName,"GetObs()=%d\n",err);}
   
   /* check criteria for logging the sample */
   else if (debuglevel>=2 || (debugbits&PARK_H))
   {
      /* make the logentry */
      LogEntry(FuncName,"PTS: %0.1fdbars %0.4fC %0.4fPSU\n",o.p,o.t,o.s);
   }
   
   /* store the observation in the float vitals */
   vitals.ParkObs=o;

   return status;
}

#endif /* PARK_C */
