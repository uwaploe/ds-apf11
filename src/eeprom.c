#ifndef EEPROM_H
#define EEPROM_H (0x01f0U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define eepromChangeLog "$RCSfile$  $Revision$  $Date$"

#include <control.h>

/* function prototypes */
int                       EepromMap(void);
struct MissionParameters *MissionParametersRead(struct MissionParameters *mission);
int                       MissionParametersWrite(struct MissionParameters *mission);
int                       PrfIdGet(void);
int                       PrfIdSet(unsigned short int pid);
int                       PrfIdIncrement(void);
enum State                StateGet(void);
int                       StateSet(enum State state);

#endif /* EEPROM_H */
#ifdef EEPROM_C
#undef EEPROM_C

#include <crc16bit.h>
#include <logger.h>
#include <at25eeprom.h>

#define PrfId                  (0x0000U) 
#define MissionAddrPrimary     (PrfId+sizeof(unsigned short int))
#define MissionAddrMirror      (MissionAddrPrimary+sizeof(struct MissionParameters))
#define StateAddrPrimary       (MissionAddrMirror+sizeof(struct MissionParameters))
#define StateAddrMirror        (StateAddrPrimary+sizeof(unsigned short int))
#define EepromMapEnd           (StateAddrMirror+sizeof(unsigned short int)-1)
#define EepromMapSize          (EepromMapEnd-PrfId+1)
#define EepromSize             (32*KB)
#define KB                     (1L<<10)

/* define a state variable in persistent memory to mirror eeprom */
persistent static enum State state_mirror;

/* define a profile-id variable in persistent memory to mirror eeprom */
persistent static unsigned short int pid_mirror;

/* declaration of external data objects */
extern const struct MissionParameters DefaultMission;

/* prototypes for functions with static linkage */
static enum State StateMirror(void);

/*------------------------------------------------------------------------*/
/* function to write the EEPROM map to the engineering logs               */
/*------------------------------------------------------------------------*/
/**
   This function writes the EEPROM map to the engineering logs.  The
   map consists of the starting address where each element is stored
   in EEPROM plus the length (in bytes) of each element.
*/
int EepromMap(void)
{
   /* define the logging signature */
   cc *FuncName = "EepromMap()";

   /* initialize the return value */
   int status=At25Ok;

   LogEntry(FuncName,"PrfId               0x%04x , %u bytes\n",PrfId,sizeof(unsigned short int));
   LogEntry(FuncName,"MissionAddrPrimary  0x%04x , %u bytes\n",MissionAddrPrimary,sizeof(struct MissionParameters));
   LogEntry(FuncName,"MissionAddrMirror   0x%04x , %u bytes\n",MissionAddrMirror,sizeof(struct MissionParameters));
   LogEntry(FuncName,"StateAddrPrimary    0x%04x , %u bytes\n",StateAddrPrimary,sizeof(unsigned short int));
   LogEntry(FuncName,"StateAddrMirror     0x%04x , %u bytes\n",StateAddrMirror,sizeof(unsigned short int));
   LogEntry(FuncName,"[EepromMapEnd,Size] 0x%04x , %u bytes\n",EepromMapEnd,EepromMapSize);
   LogEntry(FuncName,"[EepromSize]        0x%04x , %u bytes\n",EepromSize,EepromSize);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the mission configuration from EEPROM                 */
/*------------------------------------------------------------------------*/
/**
   This function reads the mission configuration from EEPROM and validates
   it by computing the mission signature and comparing to the signature
   stored in EEPROM.  If the validation fails, a mirror copy of the mission
   configuration is read.  If the mirror copy is also invalid then this
   function returns NULL otherwise this function returns a pointer to the
   mission configuration.
*/
struct MissionParameters *MissionParametersRead(struct MissionParameters *mission)
{
   /* define the logging signature */
   cc *FuncName = "MissionParametersRead()";

   /* define a static MissionParameters object */
   static struct MissionParameters m;

   /* define some local work objects */
   unsigned int crc=0x5555U; int err;

   /* make sure that the pointer to the MissionParameters is valid */
   if (!mission) mission=&m;

   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"AT25 eeprom failed to enable. [err=%d]\n",err);

      /* ensure that the crc comparison fails */
      crc= ~(mission->crc);
   }

   else
   {
      /* read the mission parameters from EEPROM */
      if ((err=At25Read((unsigned char *)mission, sizeof(*mission), MissionAddrPrimary))<=0)
      {
         LogEntry(FuncName,"Attempt to read (primary) mission parameters "
                  "from eeprom failed. [err=%d]\n",err);
      }

      /* compute the mission signature */
      else {crc = Crc16Bit((unsigned char *)mission, sizeof(*mission)-sizeof(mission->crc));}

      /* validate the primary mission parameters */
      if (crc!=mission->crc || err<=0)
      {
         /* read the mission parameters from EEPROM */
         if ((err=At25Read((unsigned char *)mission, sizeof(*mission), MissionAddrMirror))<=0)
         {
            LogEntry(FuncName,"Attempt to read (mirror) mission parameters "
                     "from eeprom failed. [err=%d]\n",err);
         }

         /* compute the mission signature */
         else {crc = Crc16Bit((unsigned char *)mission, sizeof(*mission)-sizeof(mission->crc));}
      }
   }

   /* validate the mission parameters */
   return (crc==mission->crc) ? mission : NULL;
}

/*------------------------------------------------------------------------*/
/* function to write the mission configuration to EEPROM                  */
/*------------------------------------------------------------------------*/
/**
   This function writes two copies of the mission configuration to
   EEPROM; first to a primary location and then to a mirror location.

   \begin{verbatim}
   input:
      mission....The MissionParameters object that contains the
                 mission parameters.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int MissionParametersWrite(struct MissionParameters *mission)
{
   /* define the logging signature */
   cc *FuncName = "MissionParametersWrite()";

   /* initialize the return value */
   int err,status=At25Ok;

   /* validate the function parameter */
   if (!mission) {LogEntry(FuncName,"NULL function argument.\n"); status=At25Null;}
   
   /* enable the AT25 eeprom on the Apf11 */
   else if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }

   else
   {
      /* compute the mission signature */
      mission->crc = Crc16Bit((unsigned char *)mission, sizeof(*mission)-sizeof(mission->crc));

      /* write the mission configuration to the primary location */
      if ((err=At25Write((unsigned char *)mission, sizeof(*mission), MissionAddrPrimary))<=0)
      {
         /* make a logentry of the AT25 write failure */
         LogEntry(FuncName,"Attempt to write (primary) mission "
                  "parameters failed. [err=%d]\n",err); status=At25Fail;
      }

      /* write the mission configuration to the mirror location */
      if ((err=At25Write((unsigned char *)mission, sizeof(*mission), MissionAddrMirror))<=0)
      {
         /* make a logentry of the AT25 write failure */
         LogEntry(FuncName,"Attempt to write (mirror) mission "
                  "parameters failed. [err=%d]\n",err); status=At25Fail;
      }

      /* disable the eeprom */
      At25Disable();
    }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the profile id from EEPROM                            */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns the profile id from EEPROM.
*/
int PrfIdGet(void)
{
   /* define the logging signature */
   cc *FuncName="PrfIdGet()";

   /* initialize the return value */
   int err,pid=pid_mirror;

   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"Warning: AT25 eeprom failed to enable. [err=%d]\n",err);
   }

   else
   {
       /* read the profile id from EEPROM */
      pid = EEReadWord(PrfId);
  
      /* make sure the profile id is nonnegative */
      if (pid<0) pid=0;
   }
   
   return pid;
}

/*------------------------------------------------------------------------*/
/* function to set the profile id                                         */
/*------------------------------------------------------------------------*/
/**
   This function sets the profile counter to a specified value and stores it
   in EEPROM.  A positive value is returned on success; zero is returned
   otherwise. 
*/
int PrfIdSet(unsigned short int pid)
{
   /* define the logging signature */
   cc *FuncName="PrfIdSet()";

   /* initialize return value */
   int err,status=At25Ok;

   /* write the profile-id to persistent memory */
   pid_mirror=pid;
   
   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"Warning: AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }
   
   /* set the profile id and disable the AT25 eeprom */
   else {EEWriteWord(pid,PrfId); At25Disable();}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to increment the profile id                                   */
/*------------------------------------------------------------------------*/
/**
   This function increments the profile counter stored in EEPROM.  A
   positive value is returned on success; zero is returned otherwise.
*/
int PrfIdIncrement(void)
{
   /* define the logging signature */
   cc *FuncName="PrfIdSIncrement()";

   /* initialize return value */
   int err,status=At25Ok;

   /* increment the profile-id in persistent memory */
   pid_mirror++;
   
   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"Warning: AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }

   else
   {
      /* read the profile id from EEPROM */
      int pid = EEReadWord(PrfId);

      /* increment the profile counter */
      pid++; pid_mirror=pid;

      /* write the profile counter back to EEPROM */
      EEWriteWord(pid,PrfId);
     
      /* disable the AT25 eeprom */
      At25Disable();
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the mission state from EEPROM                         */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns the mission state variable from EEPROM.
   The primary and mirror copy are read and validation criteria are
   applied.  If the validation criteria are satisfied then the mission state
   is returned.  If the validate criteria are violated then an UNDEFINED
   state is returned.
*/
enum State StateGet(void)
{
   /* define the logging signature */
   cc *FuncName = "StateGet()";

   /* initialize the state variable */
   enum State state=StateMirror();

   /* define local work objects */
   int err;

   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())>0)
   {
      /* read the state variable from eeprom */
      enum State primary = EEReadWord(StateAddrPrimary);
      
      /* read the state mirror from eeprom */
      enum State mirror = EEReadWord(StateAddrMirror);
      
      /* check validation criteria for the state variable */
      if (primary>UNDEFINED && primary<EOS && primary==mirror) {state=primary;}
      
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to validate state variable failed.\n");}
      
      /* disable the AT25 eeprom */
      At25Disable();
   }
   
   /* check if the attempt to enable the AT25 failed */
   else {LogEntry(FuncName,"Warning: AT25 eeprom failed to enable. [err=%d]\n",err);}

   /* warn if the mission state is undefined */
   if (state==UNDEFINED) {LogEntry(FuncName,"Warning: Mission state is UNDEFINED.\n");}
   
   return state;
}

/*------------------------------------------------------------------------*/
/* return the mission state that is recorded in persistent memory         */
/*------------------------------------------------------------------------*/
static enum State StateMirror(void) 
{
   return ((state_mirror>UNDEFINED && state_mirror<EOS) ? state_mirror : UNDEFINED);
}

/*------------------------------------------------------------------------*/
/* function to write the mission state to EEPROM                          */
/*------------------------------------------------------------------------*/
/**
   This function writes the mission state variable to EEPROM.  The state is
   then reread and compared as a validation step.  If successful, this
   function returns a positive value.  A return value of zero indicates that
   the read/verification failed.  A negative return value indicates
   that an exception was encountered.
*/
int StateSet(enum State state)
{
   /* define the logging signature */
   cc *FuncName = "StateSet()";

   /* initialize the return value */
   int err,status=At25Ok;

   /* record the state variable in persistent memory */
   if (state>UNDEFINED && state<EOS) {state_mirror=state;}
   
   /* validate the state variable */
   if (!(state>UNDEFINED && state<EOS)) 
   {
      /* make a logentry that the function argument is invalid */
      LogEntry(FuncName,"Invalid mission phase: %d\n",state); status=At25Null;
   }
      
   /* enable the AT25 eeprom on the Apf11 */
   else if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"Panic: AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }

   else
   {
      /* define some local work objects */
      enum State a,b;
      
      /* write the state variable to the primary location */
      EEWriteWord(state,StateAddrPrimary);

      /* write the state variable to the mirror location */
      EEWriteWord(state,StateAddrMirror);

      /* read the state variable back from the primary location */
      if ((a=EEReadWord(StateAddrPrimary))!=state || (b=EEReadWord(StateAddrMirror))!=state)
      {
         /* make the logentry */
         LogEntry(FuncName,"Verification of mission phase failed.\n"); status=At25Fail;
      }

      /* disable the AT25 eeprom */
      At25Disable();
   }

   return status;
}

#endif /* EEPROM_C */
