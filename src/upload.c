#ifndef UPLOAD_H
#define UPLOAD_H (0x0100U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define uploadChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* function prototypes */
int PrepareUpload(unsigned int NRetries);
int UpLoad(const struct SerialPort *modem, unsigned NRetries);
int UpLoadFile(const struct SerialPort *modem, const char *localpath);

#endif /* UPLOAD_H */
#ifdef UPLOAD_C
#undef UPLOAD_C

#include <cd.h>
#include <chat.h>
#include <clogin.h>
#include <config.h>
#include <fatio.h>
#include <logger.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stream.h>
#include <string.h>
#include <unistd.h>
#include <zmodem.h>
#include <zlib.h>

#if defined (__arm__)
   #include <apf11.h>
   #include <apf11rf.h>
#endif /* __arm__ */

/* define the command to download via zmodem */
static const char *rzcmd = "rz";

/* define the time-out period for CD detection */
#define CdTimeOut (3.0)

/* external declaration of profile path */
extern persistent char prf_path[32];

/* declarations for functions with static linkage */
static const int FatFsNext(void);
static const uint8_t RamFsNext(uint8_t fd);

static char fname[FILENAME_MAX + 1];
static char zippedName[FILENAME_MAX + 1];
static unsigned char inbuffer[KB];
static unsigned char outbuffer[KB];

/*------------------------------------------------------------------------*/
/* function to find the next transmit-able file in the FatFs volume       */
/*------------------------------------------------------------------------*/
static const int FatFsNext(void)
{
    const char* cand = NULL;
    cc* FuncName = "FatFsNext()";
    int status = 0;

    if (fat_ismount())
    {
        cand = fglob(NULL, NULL);
    }
    else if (fat_mount())
    {
        cand = fglob("*.*", NULL);
    }
    else
    {
        LogEntry(FuncName, "The FatFs filesystem must be mounted.\n");
        cand = NULL;
    }

    while (cand != NULL)
    {
        if (strcmp(cand, config_path) == 0)
        {
            // Don't upload the mission configuration
            LogEntry(FuncName, "Not uploading the mission config...\n");
        }
        else if (strcmp(cand, log_path) == 0)
        {
            // Don't upload the active logfile.
            LogEntry(FuncName, "Not uploading the log file...\n");
        }
        else if (strcmp(cand, arcdir) == 0)
        {
            // Don't upload from the archive directory
            LogEntry(FuncName, "Not uploading from the archive directory...\n");
        }
        else
        {
            (void)strncpy(fname, cand, FILENAME_MAX + 1);
            LogEntry(FuncName, "Found %s to upload next\n", fname);
            status = 1;
            break;
        }

        cand = fglob(NULL, NULL);
    }

    return status;
}

/*------------------------------------------------------------------------*/
/* function to find the next transmit-able file in the RAM file system    */
/*------------------------------------------------------------------------*/
static const uint8_t RamFsNext(uint8_t fd)
{
    /* function name for log entries */
    cc *FuncName = "RamFsNext()";

    uint8_t next = fd + 1;
    while (next < NFIO)
    {
        memset(fname, 0, FILENAME_MAX + 1);
        if (fioName(next, fname, FILENAME_MAX + 1) == 1)
        {
            if (strcmp(fname, config_path) == 0)
            {
                // Don't upload the mission config.
                LogEntry(FuncName, "Not uploading the mission config...\n");
            }
            else if (strcmp(fname, log_path) == 0)
            {
                // Don't upload the active logfile.
                LogEntry(FuncName, "Not uploading the log file...\n");
            }
            else if (streamok(next + NSTD) > 0)
            {
                // Don't upload an open file.
                LogEntry(FuncName, "Not uploading an open file: %s\n", fname);
            }
            else if (fioLen(next) == 0)
            {
                // Don't upload a zero-length file.
                LogEntry(FuncName, "Removing this zero length file: %s\n", fname);
                fioWipe(&fioblk_[next]);
            }
            else
            {
                LogEntry(FuncName, "Found %s to upload next\n", fname);
                return next;
            }
        }
        else
        {
            LogEntry(FuncName, "Skipping upload of an invalid name: %s\n", fname);
        }

        next++;
    }

    return next;
}

int PrepareUpload(unsigned int NRetries)
{
    cc* FuncName = "PrepareUpload()";

    int status = 0;
    int iseof = 0;
    bool zipped = false;
    int zlibcode = Z_OK;
    size_t fnameLen, writeSize;
    uint8_t curfd = 0U;
    unsigned int consecFails = 0U;
    FILE* infile = NULL;
    FILE* outfile = NULL;
    z_stream stream;

    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;

    // Close all non-stdio buffered FILE streams
    fclose_streams();
    while (consecFails < NRetries)
    {
        // Reset the filename pointer
        memset(fname, 0, FILENAME_MAX + 1);

        // Find the next file
        if (curfd < NFIO)
        {
            curfd = RamFsNext(curfd);
        }

        if (curfd >= NFIO)
        {
            if (FatFsNext() == 0)
            {
                status = 1;
                fat_umount();
                break;
            }
        }

        // Check to make sure we even want to zip this file
        fnameLen = strlen(fname);
        if (fnameLen > FILENAME_MAX - 3)
        {
            fnameLen = FILENAME_MAX - 3;
        }

        if (fnameLen >= 3 && fname[fnameLen - 3] == '.' && fname[fnameLen - 2] == 'g' && fname[fnameLen - 1] == 'z')
        {
            LogEntry(FuncName, "Skipping %s\n", fname);
            continue;
        }

        memset(zippedName, 0, FILENAME_MAX + 1);
        snprintf(zippedName, FILENAME_MAX + 1, "%s.gz", fname);
        infile = fopen(fname, "r");
        if (infile == NULL)
        {
            LogEntry(FuncName, "Problem opening %s for reading.\n", fname);
            consecFails++;
            continue;
        }

        outfile = fopen(zippedName, "wb");
        if (outfile == NULL)
        {
            LogEntry(FuncName, "Problem opening %s for writing.\n", zippedName);
            fclose(infile);
            consecFails++;
            continue;
        }

        LogEntry(FuncName, "Zipping %s...\n", fname);

        // This creates the gzip format
        // Add 16 to windowBits to write a simple gzip header and trailer around the
        // compressed data instead of a zlib wrapper.
        // The APF board reported having low heap space so reduced the options to
        // conserve memory.
        // The memory usage of windowBits and memLevel is described in this equation:
        // memory (bytes) = (1 << (windowBits + 2)) + (1 << (memLevel + 9))
        // a windowsBits = 3, memLevel = 3 yields exactly 8192 bytes of memory
        zlibcode = deflateInit2(
            &stream,
            Z_DEFAULT_COMPRESSION,
            Z_DEFLATED,
            10 | 16,
            3,
            Z_DEFAULT_STRATEGY);

        if (zlibcode != Z_OK)
        {
            LogEntry(FuncName, "Problem initializing the deflate object, zlibcode: %d\n", zlibcode);
            fclose(infile);
            fclose(outfile);
            consecFails++;
            continue;
        }

        zipped = true;
        do
        {
            memset(inbuffer, 0, KB);
            stream.avail_in = fread(inbuffer, 1, KB, infile);
            stream.next_in = inbuffer;

            iseof = feof(infile) ? Z_FINISH : Z_NO_FLUSH;
            do
            {
                memset(outbuffer, 0, KB);
                stream.avail_out = KB;
                stream.next_out = outbuffer;
                zlibcode = deflate(&stream, iseof);
                if (zlibcode == Z_STREAM_ERROR)
                {
                    LogEntry(FuncName, "Deflate error during zipping of %s\n", fname);
                    iseof = Z_FINISH;
                    zipped = false;
                    break;
                }

                writeSize = KB - stream.avail_out;
                if (fwrite(outbuffer, 1, writeSize, outfile) != writeSize || ferror(outfile))
                {
                    LogEntry(FuncName, "Write error during zipping of %s\n", fname);
                    iseof = Z_FINISH;
                    zipped = false;
                    break;
                }

            } while (stream.avail_out == 0);
        } while (iseof != Z_FINISH);

        deflateEnd(&stream);
        fclose(infile);
        fclose(outfile);

        if (!zipped)
        {
            LogEntry(FuncName, "Failed to zip %s\n", fname);
            unlink(zippedName);
            consecFails++;
            continue;
        }
        else
        {
            consecFails = 0U;
            unlink(fname);
        }

        if (!strcmp(fname, prf_path))
        {
            prf_path[0] = 0;
        }
    }

    return status;
}

/*------------------------------------------------------------------------*/
/* function to upload the contents of the file system                     */
/*------------------------------------------------------------------------*/
/**
   This function is designed to upload the contents of the file system
   (except the mission configuration file) to the remote host.
*/
int UpLoad(const struct SerialPort *modem, unsigned int NRetries)
{
    /* function name for log entries */
    cc *FuncName = "UpLoad()";

    /* initialize the function's return value */
    int status = -1;

    unsigned int fail = 0U;
    int upload = 0;
    uint8_t curfd = 0;

    /* close all non-stdio buffered FILE streams */
    fclose_streams();

    /* validate the port */
    if (!modem)
    {
        LogEntry(FuncName,"NULL serial port.\n");
    }
    else
    {
        while (fail < NRetries)
        {
            memset(fname, 0, FILENAME_MAX + 1);

            if (curfd < NFIO)
            {
                curfd = RamFsNext(curfd);
            }

            if (curfd >= NFIO)
            {
                if (FatFsNext() == 0)
                {
                    status = 1;
                    fat_umount();
                    break;
                }
            }

            if (CLogin(modem) > 0)
            {
                if (UpLoadFile(modem, fname) > 0)
                {
                    fclose_streams();
                    fail = 0;
                    upload++;

                    if (fioFindRamFs(fname) > 0)
                    {
                        unlink(fname);
                    }
                    else if (!fmove(fname, FatRoot))
                    {
                        unlink(fname);
                    }

                    if (!strcmp(fname, prf_path))
                    {
                        prf_path[0]=0;
                    }
                }
                else
                {
                    fail++;
                }
            }
            else
            {
                fail++;
            }
        }

        if (fail >= NRetries)
        {
            status = 0;
        }
    }

    if (debuglevel >= 2 || (debugbits & UPLOAD_H))
    {
        /* make the logentry */
        LogEntry(FuncName, "Files successfully uploaded: %d\n", upload);
    }

    /* log the upload status */
    if (status == 0)
    {
        LogEntry(FuncName,"Aborting upload: Retry limit [%u] "
                            "exceeded. Upload incomplete.\n", NRetries);
    }
    else if (debuglevel >= 2 || (debugbits & UPLOAD_H))
    {
        LogEntry(FuncName,"Upload complete.\n");
    }

    return status;
}

/*------------------------------------------------------------------------*/
/* function to upload a file from the Iridium float to the remote host    */
/*------------------------------------------------------------------------*/
/**
   This function uploads a data file from the Iridium float to the remote
   host.
     
      \begin{verbatim}
      input:
         modem.......A structure that contains pointers to machine dependent
                     primitive IO functions.  See the comment section of the
                     SerialPort structure for details.  The function checks
                     to be sure this pointer is not NULL.

         localpath...The filename on the CompactFlash to be transferred to
                     the remote host.
 
         hostpath....The filename on the remote host where the file will be
                     stored. 
                     
      output:
         This function returns a positive value if successful, zero if it
         fails, and a negative value if exceptions were detected in the
         function's arguments.
      \end{verbatim}
*/
int UpLoadFile(const struct SerialPort *modem, const char *localpath)
{
    cc *FuncName = "UpLoadFile()";
    const time_t timeout = 30;
    static char cmd[64];
    static char response[64];
    ZModem* zmodem;

    if (modem == NULL || localpath == NULL)
    {
        LogEntry(FuncName, "NULL serial port or local file path\n");
        return -1;
    }

    if (Cd(modem, CdTimeOut) <= 0)
    {
        LogEntry(FuncName,"No carrier detected.\n");
        return 0;
    }

    zmodem = ZModemInit(modem, NULL, localpath);
    zmodem = ZModemTimeOut(zmodem, mission.ZModemTimeOut);
    zmodem->fp = fopen(localpath, "r");
    if (zmodem->fp == NULL)
    {
        LogEntry(
            FuncName,
            "Unable to open \"%s\" for reading. [errno=%d]\n",
            localpath,
            errno);
        return 0;
    }

    if (modem->iflush)
    {
        modem->iflush();
    }

    snprintf(cmd, sizeof(cmd) - 1, "%s\n", rzcmd);
    snprintf(response, sizeof(response) - 1, "%s\r\n", rzcmd);
    if (chat(modem, cmd, response, timeout, NULL) <= 0)
    {
        LogEntry(
            FuncName,
            "Failed attempt to execute \"%s\" on the host.\n",
            cmd);
        if (zmodem->fp != NULL)
        {
            fclose(zmodem->fp);
            zmodem->fp = NULL;
        }

        return 0;
    }

    if (modem->iflush)
    {
        modem->iflush();
    }

    if (zTx(zmodem) <= 0)
    {
        LogEntry(
            FuncName,
            "Failed to send \"%s\" to the host. ZModem failure\n", localpath);
        if (zmodem->fp != NULL)
        {
            fclose(zmodem->fp);
            zmodem->fp = NULL;
        }

        ZModemCleanUp(zmodem);
        return 0;
    }

    if (zmodem->fp != NULL)
    {
        fclose(zmodem->fp);
        zmodem->fp = NULL;
    }

    if (debuglevel >= 2 || (debugbits & UPLOAD_H))
    {
        LogEntry(
            FuncName,
            "Upload successful: [bytes:%ld, sec:%0.1f, bps:%0.0f]\n",
            zmodem->nbytes,
            zmodem->etime,
            (zmodem->etime > 0) ? (zmodem->nbytes / zmodem->etime) : 0);
    }

    return 1;
}

#endif /* UPLOAD_C */
