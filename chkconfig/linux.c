#ifndef LINUX_H
#define LINUX_H

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define linuxChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>
#include <serial.h>
#include <stdio.h>

typedef struct
{
   unsigned long int SleepSec, WakeCycles;
   float Coulombs;
} ChargeModel;

/* function prototypes */
int               Apf11StackCheck(void);
int               ChargeModelInit(void);
double            IntervalTimerDiff(unsigned long int tics1, unsigned long int tics0);
unsigned long int IntervalTimerTics(void);
time_t            itimer(void);
int               LinuxLogInit(void);
FILE             *LogStream(void);
unsigned long int msTimer(unsigned long int msTimeRef);
int               streamok(int handle);
int               Wait(unsigned long int millisec);

extern time_t RefTime;

extern ChargeModel cmodel;

#endif /* LINUX_H */
#ifdef LINUX_C
#undef LINUX_C

#include <auxtime.h>
#include <limits.h>
#include <logger.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

time_t RefTime=0L;

ChargeModel cmodel;

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int Apf11StackCheck(void) {return 1;}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int ChargeModelInit(void)
{
   /* initialize the charge budget model */
   memset((void *)(&cmodel),0,sizeof(cmodel));

   return 1;
}

/*------------------------------------------------------------------------*/
/* function to compute the time difference between two RTC tics           */
/*------------------------------------------------------------------------*/
/**
   This function represents a safe way to compute the difference
   between two RTC tics.  The result is measured in seconds and is
   positive if tic1 is greater than tic0.  The resolution is
   256 tics per second.

      \begin{verbatim}
      input:
         tic1 ... The final time (tics).
         tic0 ... The initial time (tics).
         
      output:
         This function returns the time difference between times tic1
         and tic0.  The result is measured in seconds and is positive
         if tic1 is greater than tic0.
      \end{verbatm}
*/
double IntervalTimerDiff(unsigned long int tics1, unsigned long int tics0)
{
   /* compute the difference in times with special attention to signedness */
   double dt = (tics0<=tics1) ? (double)(tics1-tics0) : -(double)(tics0-tics1);

   /* convert from RTC tics to seconds */
   dt /= 256;
   
   return dt;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
unsigned long int IntervalTimerTics(void)
{
   /* initialize the return value */
   unsigned long int tics = (unsigned long int)(-1);

   /* initialize the reference time */
   struct timeval T,Tref={RefTime,0}; time_t sec;

   /* get the time of day */
   if (!gettimeofday(&T,NULL) && (sec=(T.tv_sec-Tref.tv_sec))>=0)
   {
      /* compute the number of RTC tics */
      tics = sec*256 + T.tv_usec/3906;
   }

   return tics;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int LinuxLogInit(void)
{
   debugbits=2; loghandle=-1; MaxLogSize=LONG_MAX;
   RefTime=time(NULL); LogPredicate(1);

   return 1;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
FILE *LogStream(void)
{
   static FILE *fp=NULL;

   if (!fp) {fp=fdopen(LogHandle(),"a");}

   return fp;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
time_t itimer(void) {return (time_t)((RefTime)?difftime(time(NULL),RefTime):-1);}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
unsigned long int msTimer(unsigned long int msTimeRef)
{
   #define mSecPerTic (3.906);
   
   /* compute the number of milliseconds since the reference time */
   unsigned long int msTime=IntervalTimerTics()*mSecPerTic;

   /* compute the time interval (milliseconds) since the reference time */
   if (msTime>=msTimeRef) {msTime -= msTimeRef;}

   /* add the elapsed time prior to wrap-around of 32-bit word */
   else {msTime += ((ULONG_MAX-msTimeRef)+1);}
   
   return msTime;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int streamok(int handle) {return (handle>=0) ? 1 : 0;}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int Wait(unsigned long int millisec) {usleep(millisec*1000); return 1;}

#endif /* LINUX_C */
