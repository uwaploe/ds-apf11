/****************************************/
/*     member of utils library          */
/****************************************/
#include <defs.p>

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * $Log$
 *========================================================================*/
/*   function to extract a substring from source string                   */
/*========================================================================*/
/*
   This function extracts n characters from the source string starting
   at the index(th) position into a global character buffer "scrbuf".  If
   either index<=0 or n=0 then the function returns the null character
   If (n<0) then the validity check which ensures that the value of
   index is less than the string length is suppressed...this saves time
   since calculation of the string length is not required but opens up
   the possibility that garbage is returned if the user is not careful.
   This function terminates extraction and returns to the calling function
   if the null character is encountered in the  source string.

      Proper usage: character_pointer = extract(source,index,n,dest).
*/
char *extract(const char *source,int index,int n)
{
   #define INFINITE (32767)
   int i,len;

   /* setting n<0 suppresses time-consuming calculation of string length */
   if (n<0) {len=INFINITE; n *= -1;}
   else len = (index<=1) ? INFINITE : strlen(source);

   if (!source || !(*source) || index>len || index<1 || !n)
   {
      *scrbuf = 0;
      return(scrbuf);
   }

   for (i=0; i<n && i<MAXSTRLEN; i++)
   {
      *(scrbuf+i) = *(source+index+i-1);
      if (!(*(scrbuf+i))) break;
   }
   *(scrbuf+i)=0;

   return(scrbuf);

   #undef INFINITE
}
