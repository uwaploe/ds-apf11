#ifndef FF_H
#define FF_H

/* define a shell of a structure used in FatFs */
typedef struct {} FILINFO;

/* switch to enable/disable the FatFs file system */
unsigned char FatFsDisable;

#endif /* FF_H */
#ifdef FF_C

/* switch to enable/disable the FatFs file system */
unsigned char FatFsDisable = 1;

#endif /* FF_C */
