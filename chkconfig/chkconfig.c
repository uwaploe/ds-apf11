#include <defs.p>
#include <config.h>
#include <ctype.h>
#include <logger.h>
#include <stdio.h>
#include <string.h>

/* enumerate the FatFs volumes */
typedef enum {FatRoot, FatArchive} FatFsVolume;

int    MissionParametersWrite(struct MissionParameters *mission);
void   PowerOff(time_t AlarmSec);
int    RecoveryInit(void);
int    fioClean(void);
int    fioCreate(void);
int    fcloseall(void);
int    fformat(void);
int    fmove(const char *glob, FatFsVolume source);
time_t ialarm(void);

int    MissionParametersWrite(struct MissionParameters *mission) {return 1;}
void   PowerOff(time_t AlarmSec) {LogEntry("PowerOff()","Alarm set for %ld sec.\n",AlarmSec);}
int    RecoveryInit(void) {LogEntry("RecoveryInit()","Activating recovery mode.\n"); return 1;}
int    fioClean(void) {return 1;}
int    fioCreate(void) {return 1;}
int    fcloseall(void) {return 1;}
int    fformat(void) {return 1;}
int    fmove(const char *glob, FatFsVolume source) {return 1;}
time_t ialarm(void) {return 0;}

// string strip (to trim leading and trailing whitespace)
// somehow this isn't a standard function
char* strstrip(char* s)
{
    size_t size;
    char* end;

    size = strlen(s);
    if (!size) {
        return s;
    }

    end = s + size - 1;
    while (end >= s && isspace(*end)) {
        end--;
    }

    *(end + 1) = '\0';
    while (*s && isspace(*s)) {
        s++;
    }

    return s;
}

int main(int argc, char **argv)
{
   enum CMD {if_, cfg_, NCMD};
   struct MetaCmd cmd[NCMD]=
   {
      {"if=", "  Pathname for configuration modifications.", 0,0},
      {"cfg=", " Pathname for the float's present configuration.", 0,0}
   };
   struct MissionParameters config = DefaultMission;
   
   /* check for usage query */
   if (argc < 2)
   { 
      printf("usage: %s %s\n", argv[0], make_usage(cmd, NCMD));
      exit(0);
   }

   // start using log entries from here on
   debugbits = 2;

   // we want to check the new config unless something goes wrong
   int checknew = 1;

   // create a temporary file so the stderr can be redirected there
   char filename[] = "chkconfigXXXXXX";
   int fd, stderrcopy;

   // Create a temp file safely and redirect stderr to write to it.
   // Keep a copy of stderr around so that we can reset it later.
   fd = mkstemp(filename);
   stderrcopy = dup(fileno(stderr));
   dup2(fd, fileno(stderr));

   /* link the metacommands to the command line arguments */
   link_meta_cmds(cmd, NCMD, argc, argv);

   if (!cmd[cfg_].arg)
   {
      LogEntry("chkconfig()", "Unspecified current configuration; assuming default configuration.\n");
   }
   else
   { 
      if (configure(&config, cmd[cfg_].arg, 0) <= 0)
      {
         /* log an entry that the configuration file is invalid */
         LogEntry("chkconfig()", "The float's present configuration is invalid.\n");
         checknew = 0;
      }
      else
      {
         LogEntry("chkconfig()", "The float's current configuration is accepted.\n");
      }
   }
   
   // only check the new file if the existing file succeeded
   if(checknew) {
      LogEntry("chkconfig()", "\n");

      /* make sure that an input file was specified */
      if (!cmd[if_].arg)
      {
         LogEntry("chkconfig()", "No new input configuration file specified.\n");
      }

      /* check configuration file */
      else
      {
         LogEntry("chkconfig()", "Validating the float's new configuration.\n");
         
         if (configure(&config, cmd[if_].arg, 0) <= 0)
         {
            LogEntry("chkconfig()", "Configuration file invalid.\n");
         }
         else
         {
            LogEntry("chkconfig()", "Configuration file OK.\n");
         }
      }
   }

   // now that the writing is done we can read back the file and process it
   // note it doesn't appear the file needs to be closed first in order to open
   int bufferlen = 255;
   char buffer[bufferlen];
   FILE* file = fopen(filename, "r");

   // need a few pointers to do the string searches
   char* paren1 = NULL;
   char* paren2 = NULL;
   char* trimmed = NULL;

   // read the file line by line
   // each line looks something like this, where the date is followed by the function
   // but we don't need either of those in our command line tool
   //   (MMM DD YYYY HH:MM:SS,      NN sec) function()          Comment
   while(fgets(buffer, bufferlen, file)) {

      // find the first paren that closes the date
      paren1 = strchr(buffer, ')');

      // if didn't find anything use the whole buffer
      if(paren1 == NULL) {
         trimmed = buffer;

      // otherwise look for the second paren
      } else {
         // note we advance ahead one past the found )
         paren1 += 1;
         paren2 = strchr(paren1, ')');

         // if a second wasn't found use the first paren
         if(paren2 == NULL) {
            trimmed = paren1;
         
         // otherwise advance the second paren by one
         } else {
            trimmed = (paren2 + 1);
         }
      }

      // trim whitespace from the trimmed string
      trimmed = strstrip(trimmed);

      // color the constraints and errors in red
      if(strstr(trimmed, "Constraint violated") != NULL || strstr(trimmed, "Syntax error") != NULL) {
         printf("\033[1;31m");
         printf("%s\n", trimmed);
         printf("\033[0m");

      // and the sanity checks in yellow
      } else if(strstr(trimmed, "check violated") != NULL) {
         printf("\033[1;33m");
         printf("%s\n", trimmed);
         printf("\033[0m");

      } else if(checknew && strstr(trimmed, "Configuration file OK") != NULL) {
         printf("\033[1;32m");
         printf("%s\n", trimmed);
         printf("\033[0m");

      // assume another ( indicates a param so indent it
      } else if(strchr(trimmed, '(') != NULL) {
         printf("  %s\n", trimmed);
      
      // otherwise just print the string
      } else {
         printf("%s\n", trimmed);
      }
   }

   // be sure the close and delete the file
   fclose(file);
   close(fd);
   remove(filename);

   // Reset stderr
   dup2(stderrcopy, fileno(stderr));
   close(stderrcopy);

   puts("");
   return 0;
}
