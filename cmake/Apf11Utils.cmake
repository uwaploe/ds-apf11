include_guard()

#
# Generate a date-based firmware revision code
#
function(utils_get_fwrev rev)
  if(EXISTS "${PROJECT_SOURCE_DIR}/bin/FwRev")
    execute_process(COMMAND ${PROJECT_SOURCE_DIR}/bin/FwRev -o
      OUTPUT_VARIABLE FWREV
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_VARIABLE REV_ERROR
      RESULT_VARIABLE REV_RESULT)
  else()
    if(NOT "${CMAKE_HOST_SYSTEM_NAME}" STREQUAL "Linux")
      set(ENV{DATE} "gdate")
    endif()
    execute_process(COMMAND ${PROJECT_SOURCE_DIR}/scripts/fwrev.sh -o
      OUTPUT_VARIABLE FWREV
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_VARIABLE REV_ERROR
      RESULT_VARIABLE REV_RESULT)
  endif()

  if(NOT REV_RESULT EQUAL "0")
    message(FATAL_ERROR "Cannot generate revision code: ${REV_ERROR}")
  endif()
  set(${rev} ${FWREV} PARENT_SCOPE)
endfunction()

#
# Define the preprocessor macro to enable the C section of a
# combined header and C source file.
#
function(util_enable_c_section srcs)
  foreach(F ${srcs})
    get_filename_component(BASE "${F}" NAME)
    string(TOUPPER "${BASE}" SYM)
    string(REPLACE "." "_" SYM ${SYM})
    set(defs "${ARGN}")
    list(APPEND defs "${SYM}")
    set_source_files_properties(${F} PROPERTIES COMPILE_DEFINITIONS "${defs}")
  endforeach()
endfunction()

#
# Generate header file links in the build directory. Returns the
# list of links in the variable headers.
#
function(util_gen_headers headers srcs)
  set(HDRS "${${headers}}")
  foreach(F ${srcs})
    string(REPLACE ".c" ".h" HDR ${F})
    execute_process(
      COMMAND ln -s -f  ${CMAKE_CURRENT_SOURCE_DIR}/${F} ${HDR}
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      ERROR_QUIET
      RESULT_VARIABLE LN_RESULT)
    if(NOT LN_RESULT EQUAL "0")
      message(FATAL_ERROR "Linking ${HDR} failed")
    else()
      list(APPEND HDRS "${HDR}")
    endif()
  endforeach()
  set(${headers} "${HDRS}" PARENT_SCOPE)
endfunction()
