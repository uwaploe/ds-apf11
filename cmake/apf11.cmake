set(CMAKE_SYSTEM_NAME               Generic)
set(CMAKE_SYSTEM_PROCESSOR          arm)

# Without that flag CMake is not able to pass test compilation check
set(CMAKE_TRY_COMPILE_TARGET_TYPE   STATIC_LIBRARY)

set(CMAKE_AR                        ${TOOLCHAIN_PATH}arm-none-eabi-ar)
set(CMAKE_ASM_COMPILER              ${TOOLCHAIN_PATH}arm-none-eabi-gcc)
set(CMAKE_C_COMPILER                ${TOOLCHAIN_PATH}arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER              ${TOOLCHAIN_PATH}arm-none-eabi-g++)
set(CMAKE_LINKER                    ${TOOLCHAIN_PATH}arm-none-eabi-ld)
set(CMAKE_OBJCOPY                   ${TOOLCHAIN_PATH}arm-none-eabi-objcopy CACHE INTERNAL "")
set(CMAKE_RANLIB                    ${TOOLCHAIN_PATH}arm-none-eabi-ranlib CACHE INTERNAL "")
set(CMAKE_SIZE                      ${TOOLCHAIN_PATH}arm-none-eabi-size CACHE INTERNAL "")
set(CMAKE_STRIP                     ${TOOLCHAIN_PATH}arm-none-eabi-strip CACHE INTERNAL "")
set(CMAKE_GCOV                      ${TOOLCHAIN_PATH}arm-none-eabi-gcov CACHE INTERNAL "")

set(APP_C_FLAGS                     "-D__APF11__" CACHE INTERNAL "")
set(CMAKE_C_FLAGS                   "${APP_C_FLAGS} -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -fmessage-length=0 -ffunction-sections -fsigned-char" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS                 "${APP_CXX_FLAGS} ${CMAKE_C_FLAGS} -fno-exceptions" CACHE INTERNAL "")

set(CMAKE_C_FLAGS_DEBUG             "-Os -g -ggdb" CACHE INTERNAL "")
set(CMAKE_C_FLAGS_RELEASE           "-Os -DNDEBUG" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_DEBUG           "${CMAKE_C_FLAGS_DEBUG}" CACHE INTERNAL "")
set(CMAKE_CXX_FLAGS_RELEASE         "${CMAKE_C_FLAGS_RELEASE}" CACHE INTERNAL "")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
