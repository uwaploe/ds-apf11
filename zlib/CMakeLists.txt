
cmake_minimum_required(VERSION 3.13)

set(F "zmodem.c")
string(REPLACE ".c" ".h" HDR ${F})
execute_process(
  COMMAND ln -s -f  ${CMAKE_CURRENT_SOURCE_DIR}/${F} ${HDR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  ERROR_QUIET
  RESULT_VARIABLE LN_RESULT)
if(NOT LN_RESULT EQUAL "0")
  message(FATAL_ERROR "Linking ${HDR} failed")
else()
  list(APPEND HEADER_FILES "${HDR}")
endif()

# libzmodem is distributed as a binary due to restrictions on
# the source code.
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/libzmodem.a
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
add_library(zmodem STATIC IMPORTED)
set_target_properties(zmodem
  PROPERTIES
  IMPORTED_LOCATION ${CMAKE_CURRENT_BINARY_DIR}
  OUTPUT_NAME ${CMAKE_CURRENT_BINARY_DIR}/libzmodem.a
  )
set(ZMODEM_LIBRARY "${CMAKE_CURRENT_BINARY_DIR}/libzmodem.a" CACHE INTERNAL "")  

