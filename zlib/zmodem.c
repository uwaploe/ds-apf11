#ifndef ZMODEM_H
#define ZMODEM_H (0x8000U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id: zmodem.c,v 1.1.1.1 2018/10/27 18:14:12 swift Exp $
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log: zmodem.c,v $
 * Revision 1.1.1.1  2018/10/27 18:14:12  swift
 * ZModem library
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define ZModemChangeLog "$RCSfile: zmodem.c,v $ $Revision: 1.1.1.1 $ $Date: 2018/10/27 18:14:12 $"

#include <serial.h>
#include <stdio.h>
#include <string.h>
 
/*------------------------------------------------------------------------*/
/* structure used to manage ZMODEM exchanges                              */
/*------------------------------------------------------------------------*/
/**
   This structure contains the objects and parameters needed by Chuck
   Forseberg's Industrial ZModem implementation for purposes of
   sending or receiving a file over a SerialPort connection.

      \begin{verbatim}
      com........A SerialPort used for ZModem transfers.

      fp.........A FILE from which data are read when transmitting
                 data to a remote host.

      blklen.....The current block-length used for during the ZModem
                 session.  It always starts at 1024 bytes but the
                 ZModem protocol will sometimes reduce the block size
                 in response to transmission conditions.

      blocks.....Maintains a count of the number of blocks read from
                 or written to the local FILE.  When the Zmodem
                 transfer is complete then this will contain the total
                 number of blocks read from or written to the local
                 FILE during the ZModem transfer session.  In the case
                 of crash recovery then this count will include only
                 the incomplete or missing blocks of data; each
                 complete block consists of 1024 bytes.

      Eofseen....A boolean object whose state changes from zero to
                 nonzero when EOF is detected for the local source
                 FILE.

      etime......Records the elapsed time (seconds) during the ZModem
                 transfer.

      Lzconv.....A boolean switch that disables crash-recovery if zero
                 or enables crash-recovery if set to ZCRESUM.  During
                 crash recovery then any portion of the file that
                 already exists on the receiver's host will not be
                 retransmitted.  Instead, the remaining portion of the
                 file will be transmitted and then appended to the
                 previously existing portion.  No attempt is made to
                 verify that the initial existing portion on the
                 receiver's host matches that on the sender's host.
                 Hence, this facility should be not be used if the
                 contents of the file can change between transmission
                 attempts.

      nbytes.....Maintains a count of the number of bytes read from or
                 written to the local FILE.  When the Zmodem transfer
                 is complete then this will contain the total number
                 of bytes read from or written to the local FILE
                 during the ZModem transfer session.  In the case of
                 crash recovery then this count will include only the
                 incomplete or missing blocks of data; each complete
                 block consists of 1024 bytes.

      TimeOut....The ZModem time-out period (seconds) used when
                 reading from or writing to the ZModem SerialPort.

      fname......The file name from which data are read, if ZModem is
                 being used to send data to a remote host.  If ZModem
                 is used to receive data then this is the filename
                 into which the data will be written.
      \end{verbatim}
*/
typedef struct
{
   const struct SerialPort *com;
   FILE    *fp;
   int      blklen;
   int      blocks;
   int      Eofseen;      
   float    etime;      
   char     Lzconv;
   long int nbytes;
   int      TimeOut;
   char     fname[64];
} ZModem;

/* prototypes for functions with external linkage */
ZModem  *ZModemInit(const struct SerialPort *com, FILE *fp, const char *fname);
int      ZModemAbort(void);
int      ZModemCleanUp(ZModem *zmodem);
ZModem  *ZModemCrashRecovery(ZModem *zmodem);
long int ZModemRxHdr(const char *header);
ZModem  *ZModemTimeOut(ZModem *zmodem, int TimeOut);
long int zRx(ZModem *zmodem);
long int zTx(ZModem *zmodem);

/* declaration of ZModem structure of transfer parameters */
extern ZModem zmodem;

/* define ZModem constants */
#define ZCRESUM  (3)  
#define IZOK     (0)
#define IZERROR (-1)
#define TIMEOUT (-2)
#define CTSERR  (-3)
#define BPSBAD  (-4)

#endif /* ZMODEM_H*/
#ifdef ZMODEM_C
#undef ZMODEM_C

#include <cd.h>
#include <auxtime.h>
#include <errno.h>
#include <logger.h>
#include <sys/time.h>
#include <unistd.h>

/* create ZModem container for transfer parameters */
ZModem zmodem;

/* declare external functions that are used locally */
void     canit(void);
long int crprocheader(const char *header);
int      rz(void);
void     saybibi(void);
void     startz(void);
int      TxBreak(const struct SerialPort *com,int millisec);
int      Wait(unsigned long int millisec);
int      wcs(const char *name);

/* declare unpublished functions with external linkage */
long int checkline(void);
void     closeit(int status);
int      purgeline(void);
int      readline(int timeout);
int      sendline(int byte);
void     zclose(int status);
long int zseek(long int pos);

/* external declarations of objects used ZModem library */
extern char secbuf[1025];
extern int  Rxtimeout;  

/* define the time-out period for CD detection */
#define CdTimeOut (3.0)

/*------------------------------------------------------------------------*/
/* function to close the ZModem structure's FILE object                   */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function closes the ZModem
   structure's FILE object; it may be safely called multiple times.

   \begin{verbatim}
   input:
      status....The status of the ZModem transfer at the time the FILE
                object is closed.
   \end{verbatim}
*/
void closeit(int status) {zclose(status);}

/*------------------------------------------------------------------------*/
/* determine if ZModem's SerialPort receive buffer contains data          */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function returns the number of
   bytes in input buffer of the ZModem structure's SerialPort.

   \begin{verbatim}
   output:
      This function returns the number of bytes in the receive buffer
      of the ZModem structure's SerialPort.
   \end{verbatim}
*/
long int checkline(void)
{
   /* define the logging signature */
   static cc FuncName[] = "checkline()";

   /* initialize return value */
   long int n=IZERROR;

   /* validate com port */
   if (zmodem.com)
   {
      /* validate the iflush() function and  flush the input buffer */
      if (zmodem.com->ibytes) {n=zmodem.com->ibytes();}
      else {LogEntry(FuncName,"Com port's ibytes() function not implemented.\n");}
   }
   else {LogEntry(FuncName,"Invalid COM port.");}

   /* return the number of bytes available in the com port's input buffer */
   return ((n>0) ? n : 0);
}

/*------------------------------------------------------------------------*/
/* process a ZModem Rx header                                             */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function processes a ZModem Rx
   header that contains the name of the file to receive and (optional)
   metadata that reveals the size and date of the file.  The header
   starts with the file name which is NULL-terminated.  The header
   continues with a metadata string that lists the (decimal) number of
   bytes in the file followed by white space followed by an octal
   number that representst the modification time of the file.  The
   metadata string is created by the following C-snippet:

      struct stat filestat; fstat(fileno(fp), &filestat);
      sprintf(header, "%lu %lo", filestat.st_size, filestat.st_mtime);

   This function should read the header, open the file, position the
   file pointer to the end of the file, and return the number of bytes
   excluding the last partial 1024-byte block.  If (optional) crash
   recovery is to be supported and if the sender requests crash
   recovery (ie., Lzconv==ZCRESUM) then the file should be opened in
   append mode and the length of the file that consists of complete
   1024-byte blocks.  If crash recovery is not enabled then the file
   should be opened in write mode and zero returned.

   \begin{verbatim}
   input:
      header.....The ZModem Rx header that contains the file name and
                 metadata as described above.

   output:
      This function returns the location of the file pointer which is
      zero for a new file or else the size of the existing partial
      file excluding the last incomplete 1024-byte block, if crash
      recovery is requested.  A return value of -1 will the skip the
      file transfer.
   \end{verbatim}
*/
#pragma weak crprocheader
long int crprocheader(const char *header)
{
   /* define the logging signature */
   static cc FuncName[] = "crprocheader()";

   /* log the error */
   LogEntry(FuncName,"Panic: Fatal error; weak function requires overload.\n");
   
   return -1;
}

/*------------------------------------------------------------------------*/
/* flush the ZModem structure SerialPort's receive buffer                 */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function flushes the receive
   buffer of the ZModem structure's SerialPort.

   \begin{verbatim}
   output:
      This function returns a zero if the receive buffer of the ZModem
      structure's SerialPort was flushed.
   \end{verbatim}
*/
int purgeline(void)
{
   /* define the logging signature */
   static cc FuncName[] = "purgeline()";

   /* initialize return value */
   int status = IZERROR;

   /* validate com port */
   if (zmodem.com)
   {
      /* validate the iflush() function */
      if (zmodem.com->iflush)
      {
         /* flush the input buffer */
         if (zmodem.com->iflush()>0) {status=IZOK;}
         else {LogEntry(FuncName,"Attempt to flush COM input buffer failed.\n");}
      }
      else {LogEntry(FuncName,"Com port's iflush() function not implemented.\n");}
   }
   else {LogEntry(FuncName,"Invalid COM port.");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* write contents of buffer to the ZModem FILE object                     */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function writes the contents
   of a buffer to the ZModem FILE object.

   \begin{verbatim}
   input:
      buf....The buffer to be written to the ZModem FILE object.
      n......The number of bytes to write.
   \end{verbatim}
*/
void putsec(const char *buf, int nbytes)
{
   /* define the logging signature */
   static cc FuncName[] = "putsec()";
   
   /* validate the function arguments and output FILE pointer */
   if (nbytes>0 && buf && zmodem.fp)
   {
      /* declare local work object */
      int n;
      
      /* write the next block form the file */
      if ((n=fwrite(buf,1,nbytes,zmodem.fp))>0)
      {
         /* check for short-write error */
         if (n!=nbytes) {LogEntry(FuncName,"Short write: %d "
                                  "of %d bytes.\n",n,nbytes);}

         /* increment the number of blocks read from the file */
         zmodem.blocks++;

         /* accumulate the number of bytes read from the file */
         zmodem.nbytes+=n;

         /* test for maximum verbosity of engineering log entries */
         if (debuglevel>=5)
         {
            /* add metadata and the block to the engineering logs */
            LogBuf(FuncName,secbuf,n,"Write block:%02d bytes:%04d NBytes:%05ld ",
                   zmodem.blocks,n,zmodem.nbytes);
         }

         /* test for enhanced verbosity of engineering log entries */
         else if (debuglevel==4 && (debugbits&ZMODEM_H))
         {
            /* check criteria for including the whole block */
            if (zmodem.blocks<=3 || !(zmodem.blocks%5) || n<zmodem.blklen)
            {
               /* add metadata and the block to the engineering logs */
               LogBuf(FuncName,secbuf,n,"Write block:%02d bytes:%04d NBytes:%05ld ",
                      zmodem.blocks,n,zmodem.nbytes);
            }

            /* add metadata to the engineering logs */
            else {LogEntry(FuncName,"Write block:%02d bytes:%04d NBytes:%05ld\n",
                           zmodem.blocks,n,zmodem.nbytes);}
         }

         /* test for moderate verbosity of engineering log entries */
         else if (debuglevel==4 || (debuglevel==3 && (debugbits&ZMODEM_H)))
         {
            /* add metadata to the engineering logs */
            LogEntry(FuncName,"Write block:%02d bytes:%04d NBytes:%05ld\n",
                     zmodem.blocks,n,zmodem.nbytes);
         }

         /* test for minimum verbosity of engineering log entries */
         else if (debuglevel==2 && (debugbits&ZMODEM_H))
         {
            /* check criteria for including the metadata */
            if (zmodem.blocks<=3 || !(zmodem.blocks%5) || n<zmodem.blklen)
            {
               /* add metadata to the engineering logs */
               LogEntry(FuncName,"Write block:%02d bytes:%04d NBytes:%05ld\n",
                        zmodem.blocks,n,zmodem.nbytes);
            }
         }
      }

      /* log the error */
      else if (!n) {LogEntry(FuncName,"Failed write of %d bytes.\n",nbytes);}
   }

   /* log the error */
   else if (!buf || !zmodem.fp) {LogEntry(FuncName,"Invalid: Buf:0x%lx "
                                          "ZModem.fp:0x%lx\n",buf,zmodem.fp);}
}

/*------------------------------------------------------------------------*/
/* read one byte from the receive buffer of ZModem structure's SerialPort */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function reads one byte from
   the receive buffer of ZModem structure's SerialPort, if one is
   available. If no bytes are available then this function returns
   after a user-specified time-out has expired.  If the carrier-detect
   signal is dropped low then the Zmodem transfer is terminated and
   the source FILE is closed.

   Note: This ZModem implementation relies on RTS/CTS hardware
         handshaking in order to avoid overflow of limited buffer
         space in the Iridium LBT; the LBT should be configured to
         enable RTS/CTS hardware handshaking.  However, the receive
         buffer for the Apf11 (or Linux) is sufficiently large that
         hardware handshaking need not be enabled by the UART.
         Instead, this function manually asserts the RTS signal of the
         ZModem structure's SerialPort so that the CTS signal of the
         LBT is always asserted.

   \begin{verbatim}
   output:
      This function returns the next byte available in the receive
      buffer of the ZModem structure's SerialPort.  If no data are
      available then TIMEOUT is returned.  If an exception is
      encountered then IZERROR is returned.
   \end{verbatim}
*/
int readline(int timeout)
{
   /* define the logging signature */
   static cc FuncName[] = "readline()";

   /* initialize return value */
   int status=IZERROR;
   
   /* validate com port */
   if (zmodem.com)
   {
      /* define local objects to implement timeout */
      struct timeval T,To; unsigned char byte;
      
      /* read a byte from the SerialPort object */
      if (zmodem.com->getb(&byte)>0) {status=byte;}

      /* return immediately if timeout period has expired */
      else if (Rxtimeout<=0) {status=TIMEOUT;}
      
      /* read the reference time */
      else if (!gettimeofday(&To,NULL)) 
      {
         /* define the minimum allowable transmission rate */
         #define BpsMin (25)

         /* compute the elapsed time since start of ZModem session */
         const time_t dT=difftime(To.tv_sec,zmodem.etime);

         /* compute parameters for iteration protection of timeout */
         const int pace=100; long int n,N=(zmodem.TimeOut<<10)/pace; 
         
         /* check criteria for very poor telemetry */
         if (zmodem.etime>=0 && dT>3*zmodem.TimeOut && zmodem.nbytes<BpsMin*dT) {status=BPSBAD;}
         
         /* implement loop with iteration protection of the timeout feature */
         else for (status=TIMEOUT, n=0; n<N; n++)
         {
            /* make sure the receiver's CTS signal is asserted */
            if (zmodem.com->rts) {zmodem.com->rts(1);}

            /* read a byte from the SerialPort object */
            if (zmodem.com->getb(&byte)>0) {status=byte; break;}
            
            /* implement the timeout feature */
            else if (!gettimeofday(&T,NULL) && difftime_tv(&T,&To)>zmodem.TimeOut) {break;}

            /* verify that the SerialPort's CD signal is asserted */
            else if (Cd(zmodem.com,CdTimeOut)<=0) {status=IZERROR; break;}

            /* wait for some data to become available */
            else {Wait(pace);}
         }

         /* check for recoverable timeout error */
         if (status==TIMEOUT && timeout<Rxtimeout) {status=TIMEOUT;}
         
         /* trigger a nonrecoverable error */
         else if (status<0)
         {
            switch (status)
            {
               /* note the loss of carrier in the engineering logs */
               case IZERROR: {LogEntry(FuncName,"No carrier, "); break;}

               /* note the loss of carrier in the engineering logs */
               case TIMEOUT: {LogEntry(FuncName,"Time-out expired, "); break;}

               /* note the poor telemetry performance */
               case BPSBAD: {LogEntry(FuncName,"Poor telemetry (%d bytes per "
                                      "%ld sec), ",zmodem.nbytes,dT); break;} 
 
               /* not the uncaught error */
               default: {LogEntry(FuncName,"Uncaught error, ");}
            }
            
            /* shut down the ZModem transfer and close the file */
            LogAdd("aborting ZMODEM transfer.\n"); ZModemAbort(); zclose(status); 

            /* indicate timeout in ZModem structure; return nonrecoverable error */
            Rxtimeout=0; status=TIMEOUT;
         }
      }
      else {LogEntry(FuncName,"Panic: gettimeofday() failed.\n");}
   }
   else {LogEntry(FuncName,"Invalid COM port.");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* transmit a BREAK signal on the ZModem structure's SerialPort           */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  A BREAK signal (duration: 250msec)
   is transmitted on the ZModem structure's SerialPort.

   output:
      On success, this function returns a positive value, else zero is
      returned.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int sendbrk(void)
{
   /* define the logging signature */
   static cc FuncName[] = "sendbrk()";

   /* initialize return value */
   int status = IZERROR;

   /* validate com port */
   if (zmodem.com) {status = (TxBreak(zmodem.com,250)>0) ? 1 : 0;}
   else {LogEntry(FuncName,"Invalid COM port.");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* transmit a byte to the ZModem structure's SerialPort                   */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  One byte is transmitted via the
   ZModem structure's SerialPort to the remote host.  The byte is
   transmitted only if both the CD and CTS signals are asserted.  If
   the CD signal drops low then the ZModem session is immediately
   aborted.  If CTS is low then this function will pause until CTS is
   signal asserted again or else a time-out occurs.  If the time-out
   happens then the ZModem session is aborted.

   Note: This ZModem implementation relies on RTS/CTS hardware
         handshaking in order to avoid overflow of limited buffer
         space in the Iridium LBT; the LBT should be configured to
         enable RTS/CTS hardware handshaking.  However, hardware
         handshaking should not be enabled by the UART of the ZModem
         structure's SerialPort.  Instead, this function checks to see
         if the Zmodem SerialPort's CTS signal is asserted before
         transmitting its payload.

   \begin{verbatim}
   input:
      byte.....The byte to be transmitted to the remote host via the
               ZModem structure's SerialPort.

   output:
      This function returns a positive value, if successful.  If the
      transmission was attempted but failed then zero is returned.  A
      negative return value indicates that an exception was returned.
   \end{verbatim}
*/
int sendline(int byte)
{
   /* define the logging signature */
   static cc FuncName[] = "sendline()";

   /* initialize return value */
   int status=IZERROR;

   /* validate com port */
   if (zmodem.com)
   {
      /* define local objects to implement timeout */
      struct timeval T,To;
      
      /* return immediately if timeout period has expired */
      if (Rxtimeout<=0) {status=TIMEOUT;}

      /* read the reference time */
      else if (!gettimeofday(&To,NULL)) 
      {
         /* define the minimum allowable transmission rate */
         #define BpsMin (25)

         /* compute the elapsed time since start of ZModem session */
         const time_t dT=difftime(To.tv_sec,zmodem.etime);

         /* compute parameters for iteration protection of timeout */
         const int pace=100; long int n,N=(zmodem.TimeOut<<10)/pace; 

         /* check criteria for very poor telemetry */
         if (zmodem.etime>=0 && dT>3*zmodem.TimeOut && zmodem.nbytes<BpsMin*dT) {status=BPSBAD;}
                  
         /* implement loop with iteration protection of the timeout feature */
         else for (status=TIMEOUT, n=0; n<N; n++)
         {
            /* implement the timeout feature */
            if (!gettimeofday(&T,NULL) && difftime_tv(&T,&To)>zmodem.TimeOut) {break;}
            
            /* verify that the SerialPort's CD signal is asserted */
            else if (!Cd(zmodem.com,CdTimeOut)) {status=IZERROR; break;}

            /* check existence and state of hardware handshaking  */
            else if (zmodem.com->cts && !zmodem.com->cts()) {status=CTSERR; Wait(pace);}
            
            /* transmit the byte */
            else {status = ((zmodem.com->putb(byte&0xff)>0) ? 1 : 0); break;}
         }

         /* trigger a nonrecoverable error */
         if (status<0) 
         {
            /* create a logentry of the error */
            switch (status)
            {
               /* note the loss of carrier in the engineering logs */
               case IZERROR: {LogEntry(FuncName,"No carrier, "); break;}

               /* note the loss of carrier in the engineering logs */
               case TIMEOUT: {LogEntry(FuncName,"Time-out expired, "); break;}

               /* note the loss of carrier in the engineering logs */
               case CTSERR: {LogEntry(FuncName,"Blocking CTS signal, "); break;}

               /* note the poor telemetry performance */
               case BPSBAD: {LogEntry(FuncName,"Poor telemetry (%d bytes per "
                                      "%ld sec), ",zmodem.nbytes,dT); break;} 
                  
               /* not the uncaught error */
               default: {LogEntry(FuncName,"Uncaught error(%d), ",status);}
            }
            
            /* shut down the ZModem transfer and close the file */
            LogAdd("aborting ZMODEM transfer.\n"); 

            /* indicate timeout in ZModem structure; return nonrecoverable error */
            Rxtimeout=0; status=TIMEOUT;
         }
      }
      else {LogEntry(FuncName,"Panic: gettimeofday() failed.\n");}

      /* put anyway, if exception encountered */
      if (status<0) {zmodem.com->putb(byte&0xff);}
   }
   else {LogEntry(FuncName,"Invalid COM port.");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to close the ZModem structure's FILE object                   */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function closes the ZModem
   structure's FILE object; it may be safely called multiple times.

   \begin{verbatim}
   input:
      status....The status of the ZModem transfer at the time the FILE
                object is closed.
   \end{verbatim}
*/
void zclose(int status)
{
   /* define the logging signature */
   static cc FuncName[] = "zclose()";

   /* validate the FILE pointer */
   if (zmodem.fp)
   {
      /* close the source file */
      fclose(zmodem.fp); zmodem.fp=NULL;

      /* check criteria for making a logentry */
      if (debuglevel>=3 || (debugbits&ZMODEM_H))
      {
         LogEntry(FuncName,"Closing %s. [status=%d]\n",zmodem.fname,status);
      }
   }
   
   /* check criteria for making a logentry */
   else if (debuglevel>=3 || (debugbits&ZMODEM_H))
   {
      LogEntry(FuncName,"NULL FILE pointer to file: %s\n",
               (zmodem.fname[0]) ? zmodem.fname : "<NULL>");
   }
}

/*------------------------------------------------------------------------*/
/* read data from the ZModem structure's FILE object into a buffer        */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function reads a block of data
   from the ZModem structure's FILE object into a buffer that is used
   to transmit the data to the remote host. A count of the number of
   bytes read from the FILE is maintained in the ZModem structure.
   The data are mostly read in blocks of 1024 bytes but there are
   circumstances under which the ZModem protocol will decimate the
   FILE into smaller chunks to achieve transmission.  When the
   end-of-file is reached, this function asserts a boolean variable in
   the ZModem structure.

   \begin{verbatim}
   output:
      This function returns the number of bytes read from the ZModem
      structure's FILE object into the buffer; zero is permitted.  A
      negative value indicates and error or exception was
      encountered. 
   \end{verbatim}
*/
int zfilbuf(void)
{
   /* define the logging signature */
   static cc FuncName[] = "zfilbuf()";

   /* initialize return value */
   size_t n=IZERROR;

   /* validate the source FILE */
   if (!zmodem.fp)
   {
      LogEntry(FuncName,"NULL FILE pointer to file: %s\n",
               (zmodem.fname[0]) ? zmodem.fname : "<NULL>");
   }

   /* validate the requested block length */
   else if (zmodem.blklen>sizeof(secbuf))
   {
      LogEntry(FuncName,"Requested block length (%d bytes) exceeds "
               "storage (%d bytes)",zmodem.blklen,sizeof(secbuf));
   }

   /* bypass the block-read if the ZModem session has timed out */
   else if (Rxtimeout<=0) {zmodem.Eofseen=1; n=TIMEOUT;}
   
   else
   {
      /* read the next block form the file */
      if ((n=fread(secbuf,1,zmodem.blklen,zmodem.fp))>0)
      {
         /* increment the number of blocks read from the file */
         zmodem.blocks++;

         /* accumulate the number of bytes read from the file */
         zmodem.nbytes+=n;

         /* test for maximum verbosity of engineering log entries */
         if (debuglevel>=5)
         {
            /* add metadata and the block to the engineering logs */
            LogBuf(FuncName,secbuf,n,"Read block:%02d bytes:%04d NBytes:%05ld ",
                   zmodem.blocks,n,zmodem.nbytes);
         }

         /* test for enhanced verbosity of engineering log entries */
         else if (debuglevel==4 && (debugbits&ZMODEM_H))
         {
            /* check criteria for including the whole block */
            if (zmodem.blocks<=3 || !(zmodem.blocks%5) || n<zmodem.blklen)
            {
               /* add metadata and the block to the engineering logs */
               LogBuf(FuncName,secbuf,n,"Read block:%02d bytes:%04d NBytes:%05ld ",
                      zmodem.blocks,n,zmodem.nbytes);
            }

            /* add metadata to the engineering logs */
            else {LogEntry(FuncName,"Read block:%02d bytes:%04d NBytes:%05ld\n",
                           zmodem.blocks,n,zmodem.nbytes);}
         }

         /* test for moderate verbosity of engineering log entries */
         else if (debuglevel==4 || (debuglevel==3 && (debugbits&ZMODEM_H)))
         {
            /* add metadata to the engineering logs */
            LogEntry(FuncName,"Read block:%02d bytes:%04d NBytes:%05ld\n",
                     zmodem.blocks,n,zmodem.nbytes);
         }

         /* test for minimum verbosity of engineering log entries */
         else if (debuglevel==2 && (debugbits&ZMODEM_H))
         {
            /* check criteria for including the metadata */
            if (zmodem.blocks<=3 || !(zmodem.blocks%5) || n<zmodem.blklen)
            {
               /* add metadata to the engineering logs */
               LogEntry(FuncName,"Read block:%02d bytes:%04d NBytes:%05ld\n",
                        zmodem.blocks,n,zmodem.nbytes);
            }
         }
      }
         
      /* check for EOF */
      if (feof(zmodem.fp))
      {
         /* record that EOF was detected */
         zmodem.Eofseen=1;

         /* check criteria for making a logentry about EOF */
         if (debuglevel>=3 || (debugbits&ZMODEM_H))
         {
            /* make a logentry about EOF detection */
            LogEntry(FuncName,"EOF detected. (NBytes:%05ld)\n",zmodem.nbytes);
         }
      }

      /* clear EOF or errors */
      if (ferror(zmodem.fp)) {clearerr(zmodem.fp);}
   }
   
   return n;
}

/*------------------------------------------------------------------------*/
/* abort the ZModem transfer                                              */
/*------------------------------------------------------------------------*/
int ZModemAbort(void) {canit(); return 1;}

/*------------------------------------------------------------------------*/
/* function to terminate a failed ZModem session                          */
/*------------------------------------------------------------------------*/
/**
   This function terminates a failed ZModem session and seeks the
   command prompt of the remote host.  If successful, a new ZModem
   session may be initiated immediately.  Upon failure, this function
   clears the modem's DTR signal to force a hang-up in preparation for
   initiation of a new communications session or telemetry cycle.

   \begin{verbatim}
   input:
      zmodem...The ZModem structure that controls the ZModem
               transfer.  Refer to the comment section near the top of
               this file for the definition of the ZModem structure
               and how its member elements are used.

   output:
      This function returns a positive value, on success, or zero on
      failure.  A negative return value indicates an error or
      exception was encountered.
   \end{verbatim}
*/
int ZModemCleanUp(ZModem *zmodem)
{
   /* function name for log entries */
   static cc FuncName[] = "ZModemCleanUp()";

   /* initialize return value */
   int status=IZERROR;

   /* validate com port */
   if (zmodem->com)
   {
      /* define local objects to implement timeout */
      struct timeval T,To; int i; char buf[80];
 
      /* compute parameters for iteration protection of timeout */
      const int pace=5; long int n,N=(zmodem->TimeOut*1.10)/pace; 

      /* transmit the ZModem termination string (CAN's) to the host */
      for (i=0; i<10; i++) {zmodem->com->putb(0x18);}

      /* induce the command prompt */
      zmodem->com->putb('\r'); zmodem->com->putb('\n');
      
      /* read the reference time and implement the timeout loop*/
      if (!gettimeofday(&To,NULL)) for (status=TIMEOUT, n=0; status<=0 && n<N; n++)
      {
         /* verify that the SerialPort's CD signal is asserted */
         if (zmodem->com->cd && zmodem->com->cd()<=0) {status=0; break;}

         /* implement the timeout feature */
         else if (!gettimeofday(&T,NULL) && difftime_tv(&T,&To)>zmodem->TimeOut) {break;}

         /* read strings coming from host */
         else for (i=0; pgets(zmodem->com,buf,sizeof(buf),5,"\r")>0 && i<10; i++)
         {
            /* search for the command prompt of the remote host */
            if (strstr(buf,"cmd>")) {status=1;}
         }
      }
      else {LogEntry(FuncName,"Panic: gettimeofday() failed.\n");}

      /* if the command prompt was not generated then disconnect */
      if (status<=0 && zmodem->com->dtr) zmodem->com->dtr(0);
   }
   else {LogEntry(FuncName,"Invalid COM port.");}

   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to enable crash recovery                            */
/*------------------------------------------------------------------------*/
/**
   This function enables crash recovery by setting a boolean variable
   (Lzconv) in the ZModem structure.

   \begin{verbatim}
   input:
      zmodem...The ZModem structure that controls the ZModem
               transfer.  Refer to the comment section near the top of
               this file for the definition of the ZModem structure
               and how its member elements are used.

   output:
      This function returns a pointer to the same ZModem structure.  
   \end{verbatim}
*/
ZModem *ZModemCrashRecovery(ZModem *zmod)
{
   /* validate the ZModem object */
   if (!zmod) {zmod=(&zmodem);}

   /* configure for crash recovery */
   zmod->Lzconv=ZCRESUM;

   /* return a pointer to the ZModem object */
   return zmod;
}

/*------------------------------------------------------------------------*/
/* function to initialize the ZModem structure                            */
/*------------------------------------------------------------------------*/
/**
   This function initializes the ZModem structure with the SerialPort,
   FILE, and filename to be used in the ZModem file transfer.

   \begin{verbatim}
   input:
      com.....The SerialPort to be used for the ZModem session.  The
              structure element remains unassigned (ie., NULL) if this
              parameter is NULL.
      fp......The FILE object used for reading/writing the ZModem
              session data.  The structure element remains unassigned
              (ie., NULL) if this parameter is NULL.
      fname...The name of the file to/from which data are read/written
              during the ZModem session.  The structure element
              remains unassigned (ie., NULL) if this parameter is
              NULL.

   output:
      This function returns a pointer to the same ZModem structure
      that controls the ZModem transfer.  Refer to the comment section
      near the top of this file for the definition of the ZModem
      structure and how its member elements are used.
   \end{verbatim}
*/
ZModem *ZModemInit(const struct SerialPort *com, FILE *fp, const char *fname)
{
   /* initialize the ZModem object */      
   memset((void *)(&zmodem),0,sizeof(ZModem));
   
   /* initialize the SerialPort object */
   if (com) {zmodem.com=com;}

   /* initialize the FILE object */
   if (fp) {zmodem.fp=fp;}
      
   /* initialize the filename to transfer */
   if (fname && fname[0]) strcpy(zmodem.fname,fname);
      
   /* initialize the ZModem block length */
   zmodem.blklen=1024U; 

   /* initialize ZModem time-out for SerialPort read/write operations */
   zmodem.TimeOut=60;
   
   /* return a pointer to the ZModem object */
   return &zmodem;
}

/*------------------------------------------------------------------------*/
/* predicate function to set the ZModem timeout period (seconds)          */
/*------------------------------------------------------------------------*/
/**
   This function sets the ZModem timeout period that is used during
   read/write operations to/from the Zmodem SerialPeriod.  This period
   must be in the range of [60,300] seconds.

   \begin{verbatim}
   input:
      zmod.......A pointer to the ZModem structure that controls the
                 transfer.
      TimeOut....The ZModem timeout period (seconds) used during
                 read/write operations to/from the ZModem SerialPort.

   output:
      This function returns a pointer to the same ZModem structure.  
   \end{verbatim}
*/
ZModem *ZModemTimeOut(ZModem *zmod, int TimeOut)
{
   /* validate the ZModem object */
   if (!zmod) {zmod=(&zmodem);}
   
   /* configure the ZModem timeout */
   if (TimeOut<60) {zmod->TimeOut=60;}
   else if (TimeOut>300) {zmod->TimeOut=300;}
   else {zmod->TimeOut=TimeOut;}

   return zmod;
}

/*------------------------------------------------------------------------*/
/* seek a specified position in the ZModem structure's FILE object        */
/*------------------------------------------------------------------------*/
/**
   This function represents an external function required by Chuck
   Forseberg's Industrial ZModem.  This function seeks a
   user-specified position in the ZModem structure's FILE object.

   \begin{verbatim}
   input:
      pos...The file position to seek.

   output:
      This function returns the file position after the file pointer
      has been set to the requested location.  A negative value
      indicates that an error or exception was encountered.
   \end{verbatim}
*/
long int zseek(long int pos)
{
   /* define the logging signature */
   static cc FuncName[] = "zseek()";
   
   /* initialize return value */
   int where = IZERROR;

   /* validate the source FILE */
   if (zmodem.fp) {where=fseek(zmodem.fp,pos,SEEK_SET); clearerr(zmodem.fp);}

   /* log the failure */
   else {LogEntry(FuncName,"NULL FILE pointer to file: %s\n",
                  (zmodem.fname[0]) ? zmodem.fname : "<NULL>");}
   
   return where;
}

/*------------------------------------------------------------------------*/
/* receive a file using the ZModem protocol                               */
/*------------------------------------------------------------------------*/
/**
   This function receives a file from a remote host using the ZModem
   protocol.

   \begin{verbatim}
   input:
      zmodem...The ZModem structure that controls the ZModem
               transfer.  Refer to the comment section near the top of
               this file for the definition of the ZModem structure
               and how its member elements are used.

   output:
      This function returns a positive value, on success, or zero on
      failure.  A negative return value indicates an error or
      exception was encountered.
   \end{verbatim}
*/
long int zRx(ZModem *zmodem)
{
   /* define the logging signature */
   static cc FuncName[] = "zRx()";

   /* initialize the return value */
   long int status=-1; int err;

   /* validate the ZModem object */
   if (zmodem && zmodem->com)
   {
      /* set the ZModem timeout in the range [30,600] seconds */
      if (zmodem->TimeOut<30) {Rxtimeout=300;}
      else if (zmodem->TimeOut>600) {Rxtimeout=6000;}
      else {Rxtimeout=(zmodem->TimeOut*10);}
      
      /* assert the receiver's CTS signal */
      if (zmodem->com->rts) {zmodem->com->rts(1);}

      /* flush the IO buffers */
      if (zmodem->com->ioflush) {zmodem->com->ioflush();}

      /* verify that the SerialPort's carrier is detected */
      if (Cd(zmodem->com,CdTimeOut)>0)
      {
         /* define local objects to implement timeout */
         struct timeval T,To;

         /* get the time at the start of the ZModem session */
         if (!gettimeofday(&To,NULL)) {zmodem->etime=To.tv_sec;}
         else {To.tv_sec=-1;}

         /* receive the file via ZModem */
         if (!rz()) {status=zmodem->nbytes; err=1;} else {err=0;}

         /* get the time at the end of the ZModem session and compute elapsed time */
         if (!gettimeofday(&T,NULL) && To.tv_sec>=0) {zmodem->etime=difftime_tv(&T,&To);}
         else {zmodem->etime=0;}
         
         /* check criteria for making a logentry */
         if (debuglevel>=3 || (debugbits&ZMODEM_H))
         {
            /* log the status of the ZModem transfer */
            LogEntry(FuncName,"Transfer %s.\n",(err>0)?"successful":"failed");
         }
      }
      else {LogEntry(FuncName,"No carrier, aborting ZMODEM transfer.\n");}
   }
   else {LogEntry(FuncName,"Invalid ZModem object.\n");}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* send a file using the ZModem protocol                                  */
/*------------------------------------------------------------------------*/
/**
   This function transmits a file to a remote host using the ZModem
   protocol.

   \begin{verbatim}
   input:
      zmodem...The ZModem structure that controls the ZModem
               transfer.  Refer to the comment section near the top of
               this file for the definition of the ZModem structure
               and how its member elements are used.

   output:
      This function returns a positive value, on success, or zero on
      failure.  A negative return value indicates an error or
      exception was encountered.
   \end{verbatim}
*/
long int zTx(ZModem *zmodem)
{
   /* define the logging signature */
   static cc FuncName[] = "zTx()";

   /* initialize the return value */
   long int status=0;

   /* validate the ZModem object */
   if (zmodem && zmodem->com && zmodem->fp && zmodem->fname[0])
   {
      /* set the ZModem timeout in the range [30,600] seconds */
      if (zmodem->TimeOut<30) {Rxtimeout=300;}
      else if (zmodem->TimeOut>600) {Rxtimeout=6000;}
      else {Rxtimeout=(zmodem->TimeOut*10);}
      
      /* assert the receiver's CTS signal */
      if (zmodem->com->rts) {zmodem->com->rts(1);}

      /* flush the IO buffers */
      if (zmodem->com->ioflush) {zmodem->com->ioflush();}
      
      /* verify that the SerialPort's carrier is detected */
      if (Cd(zmodem->com,CdTimeOut)>0)
      {
         /* define local objects to implement timeout */
         struct timeval T,To;

         /* get the time at the start of the ZModem session */
         if (!gettimeofday(&To,NULL)) {zmodem->etime=To.tv_sec;}
         else {To.tv_sec=-1;}
      
         /* initialize the ZModem transfer */
         startz();

         /* transmit the file via ZModem */
         status=wcs(zmodem->fname);

         /* terminate the ZModem transfer */
         saybibi(); 

         /* get the time at the end of the ZModem session and compute elapsed time */
         if (!gettimeofday(&T,NULL) && To.tv_sec>=0) {zmodem->etime=difftime_tv(&T,&To);}
         else {zmodem->etime=0;}
         
         /* check criteria for making a logentry */
         if (debuglevel>=3 || (debugbits&ZMODEM_H))
         {
            /* log the status of the ZModem transfer */
            LogEntry(FuncName,"Transfer %s.\n",
                     (status>0)?"successful":"failed");
         }
      }
      else {LogEntry(FuncName,"No carrier, aborting ZMODEM transfer.\n");}
   }
   else {LogEntry(FuncName,"Invalid ZModem object.\n");}
   
   return status;
}

#endif /* ZMODEM_C */
