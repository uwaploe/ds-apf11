# Design for Shoreside Communications with EM-APEX Floats

## Overview

The EM-APEX vertical profiling float communicates with shore-side servers via the Iridium Satellite Network to both download new dive parameters and upload acquired scientific data. This document details proposed changes to specific components of the EM-APEX firmware and the shore-side server software to improve the reliability, security, and accounting of communications with the EM-APEX floats.

## Current System Architecture

As of today, our lab has only one server, Proetida, that can interact with the EM-APEX floats. Proetida is a Ubuntu Linux desktop computer running in Zoli's office. Floats may call into Proetida directly through the Iridium network or they can connect to it via Mike Kenney's Waves server.

### Float Setup

Every float must be given a username and password on Proetida. There are specific user settings that must be configured for each new float account. Additionally, certain permissions must be set on every float account to make sure the correct login message is sent back to the float when it first connects to the server. The username and password are set in each float's mission configuration and must also be stored in our lab's Sharepoint.

### Server Processes

- `rudicsd`: A server program that is spun up every time an incoming connection is detected on the server for a specific IP address and port number.
- USI: Unmanned Systems Interface. This program runs on the server and knows how to interpret the folder structure of every float. It is responsible for updating mission configurations for all registered floats, plotting their GPS positions on a map, and organizing data files that the floats upload to the server.
- Flowpilot: For the purposes of this document, FlowPilot can be understood as a data processing program that ingests float positions, vitals, and data, and recommends new positions and configuration parameters for each float based on historical data, location, and other floats.

![USI Interface](USI_interface.png "USI Interface")<br />
*USI Mission Configuration Interface to input mission parameters*

### Connection Lifecycle for Telemetry

1. An EM-APEX float surfaces and enters the Telemetry phase of a dive.
2. The EM-APEX attempts to configure and find the Iridium Satellite Network with its on-board modem and antenna.
3. Upon establishing a connection with the Iridium Satellite Network, the float dials in the connection string to a shoreside computer.
4. A connection is established to the shoreside computer and the Iridium Satellite Network acts as a proxy for forwarding messages between the shoreside computer and the float.
5. The float provides its username and password in an attempt to log-in to the shoreside computer.
	- If the remote host is Mike Kenney's Waves server, the float will provide its IMEI number. Waves will then verify that the IMEI number is registered and then forward the connection to the appropriate server within APL.
6. The float runs a shell script that will terminate any zombie processes running on the server associated with its username.
	- It's possible due to spotty connections, ZModem errors, and/or float firmware faults that the processes running on the server associated with a float will stay alive between connections. This will interfere with subsequent float connections, so the float must attempt to terminate any processes running that were started by its username before doing anything else.
7. The float attempts to provide a GPS fix for its current location.
8. The float attempts to download the `mission.cfg` file found in its home directory on the server.
9. The float attempts to upload any data files it has acquired from its latest dive.
10. The float disconnects from the server and hangs up the modem.

![alt text](Connection-Lifecycle.jpg "Float Connections")<br />
*Network Diagram for EM-APEX Floats*

### Connection Lifecycle for Recovery

1. An EM-APEX float surfaces...
	- The float could surface in Recovery mode because it encountered some fault or condition in its firmware that triggered this phase.
	- The float could surface in Telemetry mode and then download a mission configuration that instructs the float to enter Recovery mode.
2. The float transmits its GPS fix at a configured cadence. For example, a float will transmit its location every 10 minutes.
3. The float exits Recovery mode by either being physically recovered or by downloading a new mission configuration that instructs it to enter a different mode.

![alt text](rudicsd_layout.jpg "Rudicsd Layout")<br />
*rudicsd interaction with the EM-APEX floats*

## Current Problems

There are some significant problems with the current system for communicating with the EM-APEX floats.

### Communications Accounting

As currently configured, it is impossible to verify some fundamental questions:

- Was the float able to download the mission configuration on its latest connection?
- What files did the float upload on its last connection? In what order?
- If a connection is flaky, did any of the data files partially upload?
- How many attempts to download and upload succeeded and failed on the last connection?

While `rudicsd` provides a system log file, the information contained in the log is sparse at best.
Sometimes we can observe that a float did indeed download the latest mission configuration by observing its behavior (i.e. the next time it calls in, its uploaded log file, etc). This all assumes that logging-in even works, which sometimes may fail due to transmission issues or if the server provides a login message that is different than the one the float expects. This might happen if the server updates its operating system version, if the server is down, or if the password for the float user is changed.

### Security Issues

Creating float usernames and passwords and storing them in plain-text on every float we deploy provides a simple way to gain access to our servers. An attacker can simply eavesdrop on network activity associated with a shoreside computer, a float, or the Iridium Satellite Network. Furthermore, since our servers are configured to accept connections from external addresses, once someone knows a username/password to a server, they can simply log-in to one of our shoreside computers using a float account. To test this out, I was able to sign into Proetida from a local cafe using its external IP address and the dedicated RUDICs server port.

The main security issue here is the ease-of-access to the full capabilities of our servers.

### Lack of Redundancy

As recently observed on the LeConte cruise in Alaska, if Proetida goes down for any reason, communicating with any EM-APEX float ceases to work. Put bluntly, this is unacceptable as it is inevitable that our servers will experience downtime. Any future system we design must have replicated state such that a float can log in to any one of our servers and obtain the latest mission configuration.

### Terminating Float Connections

During transmission, if the connection between a float and a server drops for any reason, it is likely that some of the server processes will continue to run. The issue is that on the next connection, these processes will interfere with file transmission. As a result, future file transfers will hang. To counteract this, upon login each float executes a script to terminate all previous processes associated with past connections.

### CPU Usage

Proetida's Java Server for USI consistently uses at least one core. The `rudicsd` process regularly uses at least another core while waiting for incoming connections. FlowPilot scripts, when running in the past, have consumed 1-2 cores and in the worst case have actually frozen the whole machine. MATLAB scripts that run on a periodic basis consistently use at least 6 cores. Proetida's compute resources are limited and if the CPU cores are overused, it will freeze-up the computer. In the worst case scenario, we'd have to restart the server. This is what happened during the LeConte cruise and the result was our inability to communicate with any floats in the field.

## Design Goals

- Ease of Use: Adding new floats should be simple without the need for background knowledge on Linux server administration.
- Security: Float communications, even in the presence of a passive observer, should not be able to give user access to the remote host.
- Redundancy: In the presence of at most one server failure, floats should always be able to establish a connection to a remote host that contains up-to-date information on each float.
- Connection Accounting: It should be obvious to outside users when a float establishes a connection, downloads a file, uploads a file, and disconnects.
- Backwards-Compatibility: Any floats deployed and that will continue to operate should continue to be supported by our servers.

### Ease of Use

Float communications can rely on the IMEI number and the float serial number. To simplify things, a float user would simply need to register the IMEI number with the corresponding float serial number with our shoreside server. Adding a new float serial number and IMEI number can be done with a script. Editing an existing IMEI number to a new float serial number can also be done with a script. The server will handle asking the float for its IMEI number and serial number when connections are initiated.

### Security

To prevent the ability for a passive observer to gain user access to our shoreside servers, float operations should be restricted to the bare necessities. This is restricted to the following:

- Reading the float IMEI and float serial numbers.
- Uploading the GPS position.
- Uploading a file.
- Downloading a mission configuration.

This would prevent observers from being able to gain access to information that would allow them to execute commands on our servers. In the future, we could encrypt transmissions between floats and shoreside computers, but given that all of the information that would be transferred is public anyway, I don't think it's worth the additional complexity.

### Redundancy

Lichida is a server rack that has more resources than Proetida in terms of compute and storage. Lichida can become the primary computer that our floats will interface with while Proetida will become the back-up server. The servers will exchange information to correctly replicate registered floats, mission configurations, and log files. This way, our system can tolerate at most one server failure and still be able to communicate with floats in the field.

### Connection Accounting

Each server will maintain a registry of floats that maps IMEI numbers to float serial numbers. Additionally, every float will have a corresponding log file that will track the following events:

- Connection established to the server, with a timestamp.
- Connection dropped from the server, with a reason and timestamp.
- Upload of a file or data, with a status and timestamp.
- Download of the mission configuration, with a status and timestamp.

### Backwards-Compatibility

The new server daemon will use the same prompts as the Teledyne style prompts when first interacting with a float. If the "password" that is received by the server is not an IMEI number, the server will assume that the float is trying to sign into the server as a user. It will then hand off the connection to the Ubuntu login prompt. Otherwise, the server daemon will handle the connection and restrict the abilities to only upload, download, and messaging between the server and the float.

![Server Layout](ServerLayout.jpg "Server Layout")<br />
*The server layout of Lichida and Proetida with WAVES*

### CPU Usage

The server daemons will be event driven and be able to relinquish the CPU when not actively handling a float connection. There is a bug with the current server daemon that can be fixed to relinquish the CPU while waiting for incoming connections. Long-term, we'll need to figure out how to optimize other programs like USI and FlowPilot to keep the servers healthy.

## Proposed Changes

This proposed design will only change the `rudicsd` program and the firmware of the EM-APEX float. USI, FlowPilot, and the various scripts running on Proetida will continue to run on the server without any changes. The hardware used will also not change on the EM-APEX.

### Server Configuration

**Layout**: Each server will have a directory called `float_telemetry`. The server will keep a float registry that maps the IMEI number of a float its float serial number. Each float will have its own directory within `float_telemetry` in the form of `fxxxxx`, where `xxxxx` is the float serial number. Each float directory will contain the most-current mission configuration, gps log file, and any scientific data that is uploaded to the server. Additionally, each float will have a log file that contains four different types of operations:

**Primary-Backup**: Lichida, a 48-core, 64TB server rack in Benjamin Hall, will become the primary server that the EM-APEX floats call into. Proetida will serve as a back-up server that will be serviced by up to 4 analog phone lines and Mike Kenney's Waves server. We can shard the 4 phone lines across the different floats to reduce the likelihood of simultaneous call-ins on the same phone number if the initial connection attempt to Lichida fails.

**Shared State**: Proetida and Lichida will need to coordinate to make sure that the float registry is replicated between the two servers. Additionally, the servers will have to make sure that for each float the mission configuration and gps log file are replicated. Everything else can be allowed to diverge.

### RUDICs Server Daemon
The RUDICs Server Daemon will be configured to always run if the server is online. It will listen on a dedicated port on the server that is registered with Iridium. It will be registered with the Linux OS to always respawn if it is detected to be not running.

#### Assumptions
- Floats are only able to communicate with one remote host at a time.
- Float serial numbers and IMEI numbers are unique.
- Communication channels between Proetida and Lichida are stable. I.e. If the servers fail to communicate, it is safe to assume that one of the servers is down.

#### Software Specification
**Server State**:

```
Map<IMEI, Float SN> float_registry
List<Float SN> unreplicated_float_configurations
```

**Events**:

- *Program Start*: The server daemon will read the float registry into memory. It will then query the other server for its float registry.
	- If the other server's registry is more up-to-date, then the server will update its registry.
	- If both servers agree, then nothing else happens.
	- Otherwise, the server will respond with its float registry for the other server to update itself with.
	- This is repeated for each float's mission configuration and gps log file.
- *Float registry is Updated*: The server attempts to replicate the float registry to the other server.
- *Float Connection Established*: The server logs the connection and its timestamp in the float's log file.
- *Float Connection Dropped*: The server logs the connection drop, its reason, and its timestamp in the float's log file.
- *Float Downloads the Mission Configuration*: The server logs the mission configuration download, its timestamp, and if it was successful.
- *Float Uploads its GPS position*: The server notifies the other server of the GPS fix, if possible. Whether or not the GPS replication succeeds, the server will then update the float's gps log file. Finally, the server will update its float log file.
- *Float Uploads Scientific Data*: The server accepts the data and logs the status, timestamp, and operation in the float's log file.
- *Float Enters Recovery Mode*: If the float notifies the server with a new mission configuration that is newer than the server's latest mission configuration, the server will accept the new configuration. The server will first attempt to replicate the new configuration to the other server. Whether or not the replication succeeds, the server will then update the float's mission configuration. Finally, the server will update its float log file.
- *External User Updates Float Mission Configuration*: After the new mission configuration is written, the server daemon will attempt to replicate the float mission configuration to the other server. The server then logs the operation in the float log.
	- If the mission configuration is unable to be replicated to the other server, the float serial number is added to a list.
- *Heartbeat from the other server*: The server will receive mission configurations from floats that the other server was unable to replicate and the other servers float registry. It will update its internal state accordingly and respond with its own float registry, if necessary, and any mission configurations from the list that was initially sent that need to be updated on the sender.

### EM-APEX Float Firmware
The float firmware changes are relatively straightforward.

**1. Server Authentication**

The float just needs to provide its IMEI number when connecting to the remote host. A simple data packet that is 32-bit CRC'd is a good choice for a simple number to ensure that the sender can check the veracity of the number upon receipt. Mike Kenney's Waves server already does this by prompting a float for its IMEI number before redirecting the connection to the desired endpoint.

**2. File Transfer Protocol**

I propose using eKermit with sliding windows, as it is a good choice for potentially unreliable connections, whereas X/Y/ZModem is optimized for stable connections.

For context, here is the state machine for the Telemetry Phase:

![Telemetry State Machine](telemetry_sm.jpg "Telemetry Phase State Machine")

## Deliverables

- Fix CPU bug in current RUDICs Server Daemon
- Implement the new RUDICs Server Daemon
	- This software will have its own Bitbucket repository
- Spin up the server daemon on Proetida and Lichida and perform live testing
- Integrate eKermit into the float firmware as the file transfer protocol of choice
- Change how the new firmware will interact with the servers
- Register new floats with Mike Kenney's Waves server to point to Proetida as the back-up server
- Perform self tests with new floats using the new server configuration.
- Set up 4 analog phone lines for Proetida and test that they work.
- Swap the IP addresses of Proetida and Lichida so that Proetida is the back-up server and Lichida is the primary.

## Future Concerns

- Storage space: Proetida has already had scenarios of running out of disk space and currently has a good 5TB of scientific data stored in back-up media drives. Lichida is going to configured with a set of 4TB drives, which will provide plenty of additional storage space. Longer term, however, we should discuss data lifecycle properties and figure out which data is worth archiving for an indeterminate amount of time. Running out of storage space would make the server unusable and while this is not a pressing problem, it is something that should be given care and thought in the future.
- CPU usage: Proetida currently experiences high levels of CPU usage that can slow down the server to the point of unresponsiveness. After doing a little digging, FlowPilot and MatLab are the biggest culprits with respect to CPU usage. We should discuss how/why these applications are using so much of the processors and what can be done to optimize these programs besides running them on a more capable server. Minimizing `cron` jobs would also help the CPU load.
- Primary-Backup Drawbacks: The Primary-Backup model for state replication has known flaws and this design relies on an assumption of connection reliability if both servers are alive. In reality, a dead server and a bad connection are indistinguishable, and if we truly want persistent, replicated state, we need at least 3 nodes that are physically separate.
