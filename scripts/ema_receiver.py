#!/usr/bin/python3

from dataclasses import dataclass
import optparse
import os
import serial
import select
import struct
import time

# crctab calculated by Mark G. Mendel, Network Systems Corporation
CRCTAB = [
    0x0000,  0x1021,  0x2042,  0x3063,  0x4084,  0x50a5,  0x60c6,  0x70e7,
    0x8108,  0x9129,  0xa14a,  0xb16b,  0xc18c,  0xd1ad,  0xe1ce,  0xf1ef,
    0x1231,  0x0210,  0x3273,  0x2252,  0x52b5,  0x4294,  0x72f7,  0x62d6,
    0x9339,  0x8318,  0xb37b,  0xa35a,  0xd3bd,  0xc39c,  0xf3ff,  0xe3de,
    0x2462,  0x3443,  0x0420,  0x1401,  0x64e6,  0x74c7,  0x44a4,  0x5485,
    0xa56a,  0xb54b,  0x8528,  0x9509,  0xe5ee,  0xf5cf,  0xc5ac,  0xd58d,
    0x3653,  0x2672,  0x1611,  0x0630,  0x76d7,  0x66f6,  0x5695,  0x46b4,
    0xb75b,  0xa77a,  0x9719,  0x8738,  0xf7df,  0xe7fe,  0xd79d,  0xc7bc,
    0x48c4,  0x58e5,  0x6886,  0x78a7,  0x0840,  0x1861,  0x2802,  0x3823,
    0xc9cc,  0xd9ed,  0xe98e,  0xf9af,  0x8948,  0x9969,  0xa90a,  0xb92b,
    0x5af5,  0x4ad4,  0x7ab7,  0x6a96,  0x1a71,  0x0a50,  0x3a33,  0x2a12,
    0xdbfd,  0xcbdc,  0xfbbf,  0xeb9e,  0x9b79,  0x8b58,  0xbb3b,  0xab1a,
    0x6ca6,  0x7c87,  0x4ce4,  0x5cc5,  0x2c22,  0x3c03,  0x0c60,  0x1c41,
    0xedae,  0xfd8f,  0xcdec,  0xddcd,  0xad2a,  0xbd0b,  0x8d68,  0x9d49,
    0x7e97,  0x6eb6,  0x5ed5,  0x4ef4,  0x3e13,  0x2e32,  0x1e51,  0x0e70,
    0xff9f,  0xefbe,  0xdfdd,  0xcffc,  0xbf1b,  0xaf3a,  0x9f59,  0x8f78,
    0x9188,  0x81a9,  0xb1ca,  0xa1eb,  0xd10c,  0xc12d,  0xf14e,  0xe16f,
    0x1080,  0x00a1,  0x30c2,  0x20e3,  0x5004,  0x4025,  0x7046,  0x6067,
    0x83b9,  0x9398,  0xa3fb,  0xb3da,  0xc33d,  0xd31c,  0xe37f,  0xf35e,
    0x02b1,  0x1290,  0x22f3,  0x32d2,  0x4235,  0x5214,  0x6277,  0x7256,
    0xb5ea,  0xa5cb,  0x95a8,  0x8589,  0xf56e,  0xe54f,  0xd52c,  0xc50d,
    0x34e2,  0x24c3,  0x14a0,  0x0481,  0x7466,  0x6447,  0x5424,  0x4405,
    0xa7db,  0xb7fa,  0x8799,  0x97b8,  0xe75f,  0xf77e,  0xc71d,  0xd73c,
    0x26d3,  0x36f2,  0x0691,  0x16b0,  0x6657,  0x7676,  0x4615,  0x5634,
    0xd94c,  0xc96d,  0xf90e,  0xe92f,  0x99c8,  0x89e9,  0xb98a,  0xa9ab,
    0x5844,  0x4865,  0x7806,  0x6827,  0x18c0,  0x08e1,  0x3882,  0x28a3,
    0xcb7d,  0xdb5c,  0xeb3f,  0xfb1e,  0x8bf9,  0x9bd8,  0xabbb,  0xbb9a,
    0x4a75,  0x5a54,  0x6a37,  0x7a16,  0x0af1,  0x1ad0,  0x2ab3,  0x3a92,
    0xfd2e,  0xed0f,  0xdd6c,  0xcd4d,  0xbdaa,  0xad8b,  0x9de8,  0x8dc9,
    0x7c26,  0x6c07,  0x5c64,  0x4c45,  0x3ca2,  0x2c83,  0x1ce0,  0x0cc1,
    0xef1f,  0xff3e,  0xcf5d,  0xdf7c,  0xaf9b,  0xbfba,  0x8fd9,  0x9ff8,
    0x6e17,  0x7e36,  0x4e55,  0x5e74,  0x2e93,  0x3eb2,  0x0ed1,  0x1ef0
]

@dataclass
class Frame:
    overflow: int
    seqno:    int
    zr_nsum:  int
    zr_sum:   int
    zr_pk2pk: int
    bt_nsum:  int
    bt_sum:   int
    bt_pk2pk: int
    hz_nsum:  int
    hz_sum:   int
    hz_pk2pk: int
    hy_nsum:  int
    hy_sum:   int
    hy_pk2pk: int
    hx_nsum:  int
    hx_sum:   int
    hx_pk2pk: int
    az_nsum:  int
    az_sum:   int
    az_pk2pk: int
    ay_nsum:  int
    ay_sum:   int
    ay_pk2pk: int
    ax_nsum:  int
    ax_sum:   int
    ax_pk2pk: int
    e1_nsum:  int
    e1_sum:   int
    e1_pk2pk: int
    e2_nsum:  int
    e2_sum:   int
    e2_pk2pk: int

@dataclass
class Compass:
    nsum:      int
    hzavg_bef: float
    hyavg_bef: float
    hxavg_bef: float
    hzavg_dur: float
    hyavg_dur: float
    hxavg_dur: float
    hzavg_aft: float
    hyavg_aft: float
    hxavg_aft: float
    hzmax_bef: int
    hymax_bef: int
    hxmax_bef: int
    hzmax_dur: int
    hymax_dur: int
    hxmax_dur: int
    hzmax_aft: int
    hymax_aft: int
    hxmax_aft: int
    hzmin_bef: int
    hymin_bef: int
    hxmin_bef: int
    hzmin_dur: int
    hymin_dur: int
    hxmin_dur: int
    hzmin_aft: int
    hymin_aft: int
    hxmin_aft: int

def parse_options():
    parser = optparse.OptionParser(usage='%prog [Options]',version='%prog 1.0')
    parser.add_option('--tty', dest='tty', default=None, help= 'serial /dev/ttyXXX [%default]')
    parser.add_option('--baud', dest='baud', type=int, default=38400, help='tty baud [%default]')

    (options, args) = parser.parse_args()
    print('Options: ', options)
    print('Args: ', args)
    if options.tty is None or len(options.tty) == 0:
        print('Missing --tty. The serial device is required')
        parser.print_help()
        exit(1)

    lockfile = '/var/lock/LCK..{0}'.format(os.path.basename(options.tty))
    if os.path.exists(lockfile):
        print('Lockfile: ', lockfile, ' exists preventing usage of ', options.tty)
        exit(1)

    try:
        # Set to non-blocking on the device
        conn = serial.Serial(options.tty, options.baud, timeout=0)
    except:
        print('Cannot open device: ', options.tty, ' with baud: ', options.baud)
        exit(1)

    print(f'Opened a serial connection on {options.tty} with baud rate {options.baud}')
    return conn

def get_crc_16(buffer):
    crc = 0
    for bit in buffer:
        crc = CRCTAB[((crc >> 8) & 0xFF) ^ bit] ^ (crc << 8)

    return crc

def decode_frame(data):
    frame = Frame(
        (data[1] << 8) & 0xff00 | data[2] & 0xff,
        (data[3] << 8) & 0xff00 | data[4] & 0xff,
        data[5] & 0xff,
        (data[6] << 8) & 0xff00 | data[7] & 0xff,
        (data[8] << 8) & 0xff00 | data[9] & 0xff,
        data[10] & 0xff,
        (data[11] << 8) & 0xff00 | data[12] & 0xff,
        (data[13] << 8) & 0xff00 | data[14] & 0xff,
        data[15] & 0xff,
        (data[16] << 8) & 0xff00 | data[17] & 0xff,
        (data[18] << 8) & 0xff00 | data[19] & 0xff,
        data[20] & 0xff,
        (data[21] << 8) & 0xff00 | data[22] & 0xff,
        (data[23] << 8) & 0xff00 | data[24] & 0xff,
        data[25] & 0xff,
        (data[26] << 8) & 0xff00 | data[27] & 0xff,
        (data[28] << 8) & 0xff00 | data[29] & 0xff,
        data[30] & 0xff,
        (data[31] << 8) & 0xff00 | data[32] & 0xff,
        (data[33] << 8) & 0xff00 | data[34] & 0xff,
        data[35] & 0xff,
        (data[36] << 8) & 0xff00 | data[37] & 0xff,
        (data[38] << 8) & 0xff00 | data[39] & 0xff,
        data[40] & 0xff,
        (data[41] << 8) & 0xff00 | data[42] & 0xff,
        (data[43] << 8) & 0xff00 | data[44] & 0xff,
        data[45] & 0xff,
        (data[46] << 24) & 0xff000000 | (data[47] << 16) & 0xff0000 | (data[48] << 8) & 0xff00 | data[49] & 0xff,
        (data[50] << 16) & 0xff0000 | (data[51] << 8) & 0xff00 | data[52] & 0xff,
        data[53] & 0xff,
        (data[54] << 24) & 0xff000000 | (data[55] << 16) & 0xff0000 | (data[56] << 8) & 0xff00 | data[57] & 0xff,
        (data[58] << 16) & 0xff0000 | (data[59] << 8) & 0xff00 | data[60] & 0xff
    )

    crc = get_crc_16(data[0:61]) & 0xffff
    data_crc = (data[61] << 8) & 0xff00 | data[62] & 0xff
    if crc != data_crc:
        print(f'Calculated CRC {crc} does not match data crc {data_crc}')
        print(f'Received: {data}')
    else:
        print('Data Frame:')
        print(frame)
        print(f'Remaining frames: {data[0] & 0xff}')

def decode_magnotometer(data):
    hexstring = data.decode('utf-8')
    data = bytes.fromhex(hexstring)
    nsum = data[0] & 0xff
    compass = Compass(
        nsum,
        struct.unpack('e', bytes([data[1], data[2]]))[0] / nsum,
        struct.unpack('e', bytes([data[3], data[4]]))[0] / nsum,
        struct.unpack('e', bytes([data[5], data[6]]))[0] / nsum,
        struct.unpack('e', bytes([data[7], data[8]]))[0] / nsum,
        struct.unpack('e', bytes([data[9], data[10]]))[0] / nsum,
        struct.unpack('e', bytes([data[11], data[12]]))[0] / nsum,
        struct.unpack('e', bytes([data[13], data[14]]))[0] / nsum,
        struct.unpack('e', bytes([data[15], data[16]]))[0] / nsum,
        struct.unpack('e', bytes([data[17], data[18]]))[0] / nsum,
        (data[19] << 8) & 0xff00 | data[20] & 0xff,
        (data[21] << 8) & 0xff00 | data[22] & 0xff,
        (data[23] << 8) & 0xff00 | data[24] & 0xff,
        (data[25] << 8) & 0xff00 | data[26] & 0xff,
        (data[27] << 8) & 0xff00 | data[28] & 0xff,
        (data[29] << 8) & 0xff00 | data[30] & 0xff,
        (data[31] << 8) & 0xff00 | data[32] & 0xff,
        (data[33] << 8) & 0xff00 | data[34] & 0xff,
        (data[35] << 8) & 0xff00 | data[36] & 0xff,
        (data[37] << 8) & 0xff00 | data[38] & 0xff,
        (data[39] << 8) & 0xff00 | data[40] & 0xff,
        (data[41] << 8) & 0xff00 | data[42] & 0xff,
        (data[43] << 8) & 0xff00 | data[44] & 0xff,
        (data[45] << 8) & 0xff00 | data[46] & 0xff,
        (data[47] << 8) & 0xff00 | data[48] & 0xff,
        (data[49] << 8) & 0xff00 | data[50] & 0xff,
        (data[51] << 8) & 0xff00 | data[52] & 0xff,
        (data[53] << 8) & 0xff00 | data[54] & 0xff
    )

    crc = get_crc_16(data[0:55]) & 0xffff
    data_crc = (data[55] << 8) & 0xff00 | data[56] & 0xff
    if crc != data_crc:
        print(f'Calculated CRC {crc} does not match data crc {data_crc}')
        print(f'Received: {hexstring}')
    else:
        print('Compass Reading:')
        print(compass)

def print_ser_lines(conn):
  print('  DTR={0}'.format(conn.dtr))
  print('  DSR={0}'.format(conn.dsr))
  print('   CD={0}'.format(conn.cd) )
  print('  RTS={0}'.format(conn.rts))
  print('  CTS={0}'.format(conn.cts))
  print()

def wake_ema(conn):
    print('Waking EMA')
    conn.rts = False
    time.sleep(0.001)
    conn.rts = True
    time.sleep(0.001)
    conn.rts = False
    time.sleep(0.002)

def send_message(conn, msg):
    wake_ema(conn)
    conn.write(msg)
    print(f'Sent {msg}')
    print_ser_lines(conn)

def get_next_command():
    command = None
    while command != 'F' and command != 'M' and command != 'S':
        command = input('Next command (F/M/S)? ')

    return command.encode('utf-8')

if __name__ == '__main__':
    ema_buffer = []
    conn = parse_options()

    try:
        wake_ema(conn)
        command = get_next_command()
        send_message(conn, command)
        while True:
            readers, _, _ = select.select([conn], [], [], 1.0)
            for reader in readers:
                if conn is reader:
                    msg = conn.read(9999)
                    if len(msg) != 0:
                        ema_buffer.extend(msg)

            if not (conn in readers):
                if len(ema_buffer) != 0:
                    data = bytes(ema_buffer)
                    print(f'Received {len(data)}')
                    if len(data) == 117 and chr(data[0]) == 'M':
                        decode_magnotometer(data[1:115])
                    elif chr(data[0]) == 'F' and (len(data) == 64 or len(data) == 9):
                        decode_frame(data[1:])
                    else:
                        print(f'Received: {data}')

                    ema_buffer.clear()
                    command = get_next_command()
                    send_message(conn, command)

    except Exception as e:
        print('Closing the connection')
        conn.close()
        raise e
