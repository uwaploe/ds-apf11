#!/usr/bin/env bash

#
# Launch gdbfrontend if present
#

GDB_VER="14.2"

if [ $# -lt 1 ]; then
    echo "$0 error: Must supply a path to the APF11 elf file"
    exit 1
fi

if [ -f "/usr/share/gdb-frontend" ]; then
    GDBFE="/usr/share/gdb-frontend"
else
    # Assume the gdb-frontend is in the path
    GDBFE="$(which gdbfrontend)"
fi

if [ -f "/usr/share/gdb-${GDB_VER}/gdb-build/gdb/gdb" ]; then
    GDBPATH="/usr/share/gdb-${GDB_VER}/gdb-build/gdb/gdb"
    DATA_DIR="/usr/share/gdb-${GDB_VER}/gdb-build/gdb/data-directory/"
else
    # Assume gdb is in the path, use `which` to get the exact path
    GDBPATH="$(which gdb)"
    DATA_DIR=""
fi

if [[ -z $GDBFE ]]; then
    echo "Did not find gdbfrontend path"
    exit 1
fi

if [[ -z $GDBPATH ]]; then
    echo "Did not find GDB path"
    exit 1
fi

# Create an initialization file if it does not exist
if [ ! -f .gdbinit ]; then
    # Connect to the open chip gdb server and then load the ELF file.
    echo "target extended-remote :3333" >> .gdbinit
    echo "file $1" >> .gdbinit
    echo "load" >> .gdbinit
fi

# Launch gdbfrontend
if [[ -z $DATA_DIR ]]; then
    $GDBFE -g $GDBPATH -G "-x ./.gdbinit"
else
    $GDBFE -g $GDBPATH -G "-x ./.gdbinit --data-directory=$DATA_DIR"
fi


