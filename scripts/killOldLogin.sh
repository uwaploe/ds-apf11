#!/bin/sh
for i in `w| grep ^$USER |grep -v "/bin/sh killOldLogin" |grep -v "s w" | awk '{print $2,$5}' |cut -d' ' -f1`
do
	DSTAMP=`date +"%Y%m%dT%H%M%S"`
        echo "$DSTAMP Only 1 session autorized ! killing old sessions of User :" $USER $i
        fuser -k /dev/$i
done
