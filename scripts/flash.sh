#!/usr/bin/env bash

# Flash a previously compiled binary to the float using st-flash.

# filename prefix of the binary
BINPREFIX=Apf11Sbe41cp

# locate the binary regardless of the script invocation
DIR="$(dirname $0)/../build/src"

# make sure there's only a single binary file in the build directory
# note the file might have suffixes depending on build config
# note the xargs is just to trim the wc result
COUNT=`find $DIR -name "$BINPREFIX*.bin" | wc -l | xargs`

if [ $COUNT == "0" ]; then
	echo "No binary (.bin) file to flash"
	
elif [ $COUNT != "1" ]; then
	echo "There is more than one binary (.bin) file"
	
else
	if [ -f "/usr/local/bin/st-flash" ]; then
		FLASH="/usr/local/bin/st-flash"
		
	elif [ -f "/opt/homebrew/bin/st-flash" ]; then
		FLASH="/opt/homebrew/bin/st-flash"
	
	else
		# Assume the st-link tools is in the path
		FLASH="st-flash"
	fi
		
	BINFILE="$DIR/$BINPREFIX*.bin"
	$FLASH write $BINFILE 0x8000000
fi
