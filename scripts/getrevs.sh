#!/usr/bin/env bash
#
# Output revision information that can be stored with a build
#

outfile="$1"
[[ -n $outfile ]] && {
    exec 1>"$outfile"
}

commit=$(git log -1 --pretty=format:%H%n)
tstamp=$(date +%FT%T)
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || echo v0)

cat<<EOF
Date: $tstamp
Commit: $commit
Version: $vers
EOF

if [[ -n $FWREV ]]; then
    echo "FwRev: $FWREV"
fi

exit 0

