#!/usr/bin/env bash
#
# Prepare the build directory and run the configuration phase
#

set -e
type cmake >&2

if [ $# -lt 1 ]; then
    echo "$0 error: Must supply path to arm-none-eabi-gcc"
    exit 1
fi

if [[ ":$PATH:" == *":$1:"* ]]; then
    echo "$1 is already in PATH: $PATH"
else
    export PATH="$1:$PATH"
    echo "Path is: $PATH"
fi

buildtype=
case "$2" in
    sim|SIM|Sim)
        buildtype="Sim"
        echo "Configuring build for SIMULATION mode"
        ;;
esac

mkdir -p build/prebuilt/stm32
curl -sS -L -X GET "https://bitbucket.org/uwaploe/ds-stm32/downloads/ds-stm32_latest.tar.gz" |\
    tar -x -z -f - -C build/prebuilt/stm32
cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=cmake/apf11.cmake -DCMAKE_BUILD_TYPE=${buildtype}

echo "Run 'cmake --build build' to build firmware"

# Configure a second build tree for chkconfig using the native compiler
if type -p cc >&2; then
    cmake -S . -B build.chk
    echo "Run 'cmake --build build.chk' to build chkconfig"
else
    echo "Skipping chkconfig build, native C compiler not found"
fi

