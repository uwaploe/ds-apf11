#!/usr/bin/python3

from dataclasses import dataclass
import optparse
import os
import serial
import select
import struct
import time

# crctab calculated by Mark G. Mendel, Network Systems Corporation
CRCTAB = [
    0x0000,  0x1021,  0x2042,  0x3063,  0x4084,  0x50a5,  0x60c6,  0x70e7,
    0x8108,  0x9129,  0xa14a,  0xb16b,  0xc18c,  0xd1ad,  0xe1ce,  0xf1ef,
    0x1231,  0x0210,  0x3273,  0x2252,  0x52b5,  0x4294,  0x72f7,  0x62d6,
    0x9339,  0x8318,  0xb37b,  0xa35a,  0xd3bd,  0xc39c,  0xf3ff,  0xe3de,
    0x2462,  0x3443,  0x0420,  0x1401,  0x64e6,  0x74c7,  0x44a4,  0x5485,
    0xa56a,  0xb54b,  0x8528,  0x9509,  0xe5ee,  0xf5cf,  0xc5ac,  0xd58d,
    0x3653,  0x2672,  0x1611,  0x0630,  0x76d7,  0x66f6,  0x5695,  0x46b4,
    0xb75b,  0xa77a,  0x9719,  0x8738,  0xf7df,  0xe7fe,  0xd79d,  0xc7bc,
    0x48c4,  0x58e5,  0x6886,  0x78a7,  0x0840,  0x1861,  0x2802,  0x3823,
    0xc9cc,  0xd9ed,  0xe98e,  0xf9af,  0x8948,  0x9969,  0xa90a,  0xb92b,
    0x5af5,  0x4ad4,  0x7ab7,  0x6a96,  0x1a71,  0x0a50,  0x3a33,  0x2a12,
    0xdbfd,  0xcbdc,  0xfbbf,  0xeb9e,  0x9b79,  0x8b58,  0xbb3b,  0xab1a,
    0x6ca6,  0x7c87,  0x4ce4,  0x5cc5,  0x2c22,  0x3c03,  0x0c60,  0x1c41,
    0xedae,  0xfd8f,  0xcdec,  0xddcd,  0xad2a,  0xbd0b,  0x8d68,  0x9d49,
    0x7e97,  0x6eb6,  0x5ed5,  0x4ef4,  0x3e13,  0x2e32,  0x1e51,  0x0e70,
    0xff9f,  0xefbe,  0xdfdd,  0xcffc,  0xbf1b,  0xaf3a,  0x9f59,  0x8f78,
    0x9188,  0x81a9,  0xb1ca,  0xa1eb,  0xd10c,  0xc12d,  0xf14e,  0xe16f,
    0x1080,  0x00a1,  0x30c2,  0x20e3,  0x5004,  0x4025,  0x7046,  0x6067,
    0x83b9,  0x9398,  0xa3fb,  0xb3da,  0xc33d,  0xd31c,  0xe37f,  0xf35e,
    0x02b1,  0x1290,  0x22f3,  0x32d2,  0x4235,  0x5214,  0x6277,  0x7256,
    0xb5ea,  0xa5cb,  0x95a8,  0x8589,  0xf56e,  0xe54f,  0xd52c,  0xc50d,
    0x34e2,  0x24c3,  0x14a0,  0x0481,  0x7466,  0x6447,  0x5424,  0x4405,
    0xa7db,  0xb7fa,  0x8799,  0x97b8,  0xe75f,  0xf77e,  0xc71d,  0xd73c,
    0x26d3,  0x36f2,  0x0691,  0x16b0,  0x6657,  0x7676,  0x4615,  0x5634,
    0xd94c,  0xc96d,  0xf90e,  0xe92f,  0x99c8,  0x89e9,  0xb98a,  0xa9ab,
    0x5844,  0x4865,  0x7806,  0x6827,  0x18c0,  0x08e1,  0x3882,  0x28a3,
    0xcb7d,  0xdb5c,  0xeb3f,  0xfb1e,  0x8bf9,  0x9bd8,  0xabbb,  0xbb9a,
    0x4a75,  0x5a54,  0x6a37,  0x7a16,  0x0af1,  0x1ad0,  0x2ab3,  0x3a92,
    0xfd2e,  0xed0f,  0xdd6c,  0xcd4d,  0xbdaa,  0xad8b,  0x9de8,  0x8dc9,
    0x7c26,  0x6c07,  0x5c64,  0x4c45,  0x3ca2,  0x2c83,  0x1ce0,  0x0cc1,
    0xef1f,  0xff3e,  0xcf5d,  0xdf7c,  0xaf9b,  0xbfba,  0x8fd9,  0x9ff8,
    0x6e17,  0x7e36,  0x4e55,  0x5e74,  0x2e93,  0x3eb2,  0x0ed1,  0x1ef0
]

@dataclass
class Frame:
    overflow: int
    seqno:    int
    zr_nsum:  int
    zr_sum:   int
    zr_pk2pk: int
    bt_nsum:  int
    bt_sum:   int
    bt_pk2pk: int
    hz_nsum:  int
    hz_sum:   int
    hz_pk2pk: int
    hy_nsum:  int
    hy_sum:   int
    hy_pk2pk: int
    hx_nsum:  int
    hx_sum:   int
    hx_pk2pk: int
    az_nsum:  int
    az_sum:   int
    az_pk2pk: int
    ay_nsum:  int
    ay_sum:   int
    ay_pk2pk: int
    ax_nsum:  int
    ax_sum:   int
    ax_pk2pk: int
    e1_nsum:  int
    e1_sum:   int
    e1_pk2pk: int
    e2_nsum:  int
    e2_sum:   int
    e2_pk2pk: int

@dataclass
class Compass:
    nsum:      int
    hzavg_bef: float
    hyavg_bef: float
    hxavg_bef: float
    hzavg_dur: float
    hyavg_dur: float
    hxavg_dur: float
    hzavg_aft: float
    hyavg_aft: float
    hxavg_aft: float
    hzmax_bef: int
    hymax_bef: int
    hxmax_bef: int
    hzmax_dur: int
    hymax_dur: int
    hxmax_dur: int
    hzmax_aft: int
    hymax_aft: int
    hxmax_aft: int
    hzmin_bef: int
    hymin_bef: int
    hxmin_bef: int
    hzmin_dur: int
    hymin_dur: int
    hxmin_dur: int
    hzmin_aft: int
    hymin_aft: int
    hxmin_aft: int

FRAME = Frame(0, 0, 11, 0, 0, 11, 1023, 1023, 11, 0, 0, 11, 0, 0, 11, 0, 0, 11, 0, 0, 11, 0, 0, 11, 0, 0, 21, 35135416, 8113, 21, 352146192, 7175)

COMPASS = Compass(5, 0.0, 0.0, 0.0, 204.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 1023, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

def parse_options():
    parser = optparse.OptionParser(usage='%prog [Options]',version='%prog 1.0')
    parser.add_option('--tty', dest='tty', default=None, help= 'serial /dev/ttyXXX [%default]')
    parser.add_option('--baud', dest='baud', type=int, default=38400, help='tty baud [%default]')

    (options, args) = parser.parse_args()
    print('Options: ', options)
    print('Args: ', args)
    if options.tty is None or len(options.tty) == 0:
        print('Missing --tty. The serial device is required')
        parser.print_help()
        exit(1)

    lockfile = '/var/lock/LCK..{0}'.format(os.path.basename(options.tty))
    if os.path.exists(lockfile):
        print('Lockfile: ', lockfile, ' exists preventing usage of ', options.tty)
        exit(1)

    try:
        # Set to non-blocking on the device
        conn = serial.Serial(options.tty, options.baud, timeout=0)
    except:
        print('Cannot open device: ', options.tty, ' with baud: ', options.baud)
        exit(1)

    print(f'Opened a serial connection on {options.tty} with baud rate {options.baud}')
    return conn

def get_float_byte_by_index(num, index):
    # 2-byte float
    return struct.pack('e', num)[index] & 0xFF

def get_byte_by_index(num, index):
    return (num >> (8 * index)) & 0xFF

def get_crc_16(buffer):
    crc = 0
    for bit in buffer:
        crc = CRCTAB[((crc >> 8) & 0xFF) ^ bit] ^ (crc << 8)

    return crc

def generate_data_frame():
    global FRAME
    FRAME.seqno += 1
    buffer = bytearray(64)

    # Metadata
    buffer[0] = 70                                     # Uppercase F
    buffer[1] = get_byte_by_index(5, 0)                # Number of frames left
    buffer[2] = get_byte_by_index(FRAME.overflow, 1)
    buffer[3] = get_byte_by_index(FRAME.overflow, 0)
    buffer[4] = get_byte_by_index(FRAME.seqno, 1)
    buffer[5] = get_byte_by_index(FRAME.seqno, 0)

    # Zeroes
    buffer[6] = get_byte_by_index(FRAME.zr_nsum, 0)    # zr_nsum, should be zero
    buffer[7] = get_byte_by_index(FRAME.zr_sum, 1)
    buffer[8] = get_byte_by_index(FRAME.zr_sum, 0)
    buffer[9] = get_byte_by_index(FRAME.zr_pk2pk, 1)
    buffer[10] = get_byte_by_index(FRAME.zr_pk2pk, 0)

    # Battery
    buffer[11] = get_byte_by_index(FRAME.bt_nsum, 0)
    buffer[12] = get_byte_by_index(FRAME.bt_sum, 1)
    buffer[13] = get_byte_by_index(FRAME.bt_sum, 0)
    buffer[14] = get_byte_by_index(FRAME.bt_pk2pk, 1)
    buffer[15] = get_byte_by_index(FRAME.bt_pk2pk, 0)

    # Vertical Magnetic Field (hz)
    buffer[16] = get_byte_by_index(FRAME.hz_nsum, 0)
    buffer[17] = get_byte_by_index(FRAME.hz_sum, 1)
    buffer[18] = get_byte_by_index(FRAME.hz_sum, 0)
    buffer[19] = get_byte_by_index(FRAME.hz_pk2pk, 1)
    buffer[20] = get_byte_by_index(FRAME.hz_pk2pk, 0)

    # Horizontal Magnetic Field (hy)
    buffer[21] = get_byte_by_index(FRAME.hy_nsum, 0)
    buffer[22] = get_byte_by_index(FRAME.hy_sum, 1)
    buffer[23] = get_byte_by_index(FRAME.hy_sum, 0)
    buffer[24] = get_byte_by_index(FRAME.hy_pk2pk, 1)
    buffer[25] = get_byte_by_index(FRAME.hy_pk2pk, 0)

    # Horizontal Magnetic Field (hx)
    buffer[26] = get_byte_by_index(FRAME.hx_nsum, 0)
    buffer[27] = get_byte_by_index(FRAME.hx_sum, 1)
    buffer[28] = get_byte_by_index(FRAME.hx_sum, 0)
    buffer[29] = get_byte_by_index(FRAME.hx_pk2pk, 1)
    buffer[30] = get_byte_by_index(FRAME.hx_pk2pk, 0)

    # Vertical Acceleration (az)
    buffer[31] = get_byte_by_index(FRAME.az_nsum, 0)
    buffer[32] = get_byte_by_index(FRAME.az_sum, 1)
    buffer[33] = get_byte_by_index(FRAME.az_sum, 0)
    buffer[34] = get_byte_by_index(FRAME.az_pk2pk, 1)
    buffer[35] = get_byte_by_index(FRAME.az_pk2pk, 0)

    # Horizontal Acceleration (ay)
    buffer[36] = get_byte_by_index(FRAME.ay_nsum, 0)
    buffer[37] = get_byte_by_index(FRAME.ay_sum, 1)
    buffer[38] = get_byte_by_index(FRAME.ay_sum, 0)
    buffer[39] = get_byte_by_index(FRAME.ay_pk2pk, 1)
    buffer[40] = get_byte_by_index(FRAME.ay_pk2pk, 0)

    # Horizontal Acceleration (ax)
    buffer[41] = get_byte_by_index(FRAME.ax_nsum, 0)
    buffer[42] = get_byte_by_index(FRAME.ax_sum, 1)
    buffer[43] = get_byte_by_index(FRAME.ax_sum, 0)
    buffer[44] = get_byte_by_index(FRAME.ax_pk2pk, 1)
    buffer[45] = get_byte_by_index(FRAME.ax_pk2pk, 0)

    # Channel 1 Electric Field
    buffer[46] = get_byte_by_index(FRAME.e1_nsum, 0)
    buffer[47] = get_byte_by_index(FRAME.e1_sum, 3)
    buffer[48] = get_byte_by_index(FRAME.e1_sum, 2)
    buffer[49] = get_byte_by_index(FRAME.e1_sum, 1)
    buffer[50] = get_byte_by_index(FRAME.e1_sum, 0)
    buffer[51] = get_byte_by_index(FRAME.e1_pk2pk, 2)
    buffer[52] = get_byte_by_index(FRAME.e1_pk2pk, 1)
    buffer[53] = get_byte_by_index(FRAME.e1_pk2pk, 0)

    # Channel 2 Electric Field
    buffer[54] = get_byte_by_index(FRAME.e2_nsum, 0)
    buffer[55] = get_byte_by_index(FRAME.e2_sum, 3)
    buffer[56] = get_byte_by_index(FRAME.e2_sum, 2)
    buffer[57] = get_byte_by_index(FRAME.e2_sum, 1)
    buffer[58] = get_byte_by_index(FRAME.e2_sum, 0)
    buffer[59] = get_byte_by_index(FRAME.e2_pk2pk, 2)
    buffer[60] = get_byte_by_index(FRAME.e2_pk2pk, 1)
    buffer[61] = get_byte_by_index(FRAME.e2_pk2pk, 0)

    # CRC
    crc = get_crc_16(buffer[1:62])
    buffer[62] = get_byte_by_index(crc, 1)
    buffer[63] = get_byte_by_index(crc, 0)
    return bytes(buffer)

def generate_magnotometer_reading():
    buffer = bytearray(117)
    compass_bytes = bytearray(57)

    # Metadata
    buffer[0] = 77                          # Uppercase M
    buffer[115] = 13                        # Carriage Return
    buffer[116] = 10                        # Line Feed

    # NSum
    compass_bytes[0] = get_byte_by_index(COMPASS.nsum, 0)

    # Before Averages
    compass_bytes[1] = get_float_byte_by_index(COMPASS.hzavg_bef * COMPASS.nsum, 1)
    compass_bytes[2] = get_float_byte_by_index(COMPASS.hzavg_bef * COMPASS.nsum, 0)
    compass_bytes[3] = get_float_byte_by_index(COMPASS.hyavg_bef * COMPASS.nsum, 1)
    compass_bytes[4] = get_float_byte_by_index(COMPASS.hyavg_bef * COMPASS.nsum, 0)
    compass_bytes[5] = get_float_byte_by_index(COMPASS.hxavg_bef * COMPASS.nsum, 1)
    compass_bytes[6] = get_float_byte_by_index(COMPASS.hxavg_bef * COMPASS.nsum, 0)

    # During Average
    compass_bytes[7] = get_float_byte_by_index(COMPASS.hzavg_dur * COMPASS.nsum, 1)
    compass_bytes[8] = get_float_byte_by_index(COMPASS.hzavg_dur * COMPASS.nsum, 0)
    compass_bytes[9] = get_float_byte_by_index(COMPASS.hyavg_dur * COMPASS.nsum, 1)
    compass_bytes[10] = get_float_byte_by_index(COMPASS.hyavg_dur * COMPASS.nsum, 0)
    compass_bytes[11] = get_float_byte_by_index(COMPASS.hxavg_dur * COMPASS.nsum, 1)
    compass_bytes[12] = get_float_byte_by_index(COMPASS.hxavg_dur * COMPASS.nsum, 0)

    # After Average
    compass_bytes[13] = get_float_byte_by_index(COMPASS.hzavg_aft * COMPASS.nsum, 1)
    compass_bytes[14] = get_float_byte_by_index(COMPASS.hzavg_aft * COMPASS.nsum, 0)
    compass_bytes[15] = get_float_byte_by_index(COMPASS.hyavg_aft * COMPASS.nsum, 1)
    compass_bytes[16] = get_float_byte_by_index(COMPASS.hyavg_aft * COMPASS.nsum, 0)
    compass_bytes[17] = get_float_byte_by_index(COMPASS.hxavg_aft * COMPASS.nsum, 1)
    compass_bytes[18] = get_float_byte_by_index(COMPASS.hxavg_aft * COMPASS.nsum, 0)

    # Before Maximums
    compass_bytes[19] = get_byte_by_index(COMPASS.hzmax_bef, 1)
    compass_bytes[20] = get_byte_by_index(COMPASS.hzmax_bef, 0)
    compass_bytes[21] = get_byte_by_index(COMPASS.hymax_bef, 1)
    compass_bytes[22] = get_byte_by_index(COMPASS.hymax_bef, 0)
    compass_bytes[23] = get_byte_by_index(COMPASS.hxmax_bef, 1)
    compass_bytes[24] = get_byte_by_index(COMPASS.hxmax_bef, 0)

    # During Maximums
    compass_bytes[25] = get_byte_by_index(COMPASS.hzmax_dur, 1)
    compass_bytes[26] = get_byte_by_index(COMPASS.hzmax_dur, 0)
    compass_bytes[27] = get_byte_by_index(COMPASS.hymax_dur, 1)
    compass_bytes[28] = get_byte_by_index(COMPASS.hymax_dur, 0)
    compass_bytes[29] = get_byte_by_index(COMPASS.hxmax_dur, 1)
    compass_bytes[30] = get_byte_by_index(COMPASS.hxmax_dur, 0)

    # After Maximums
    compass_bytes[31] = get_byte_by_index(COMPASS.hzmax_aft, 1)
    compass_bytes[32] = get_byte_by_index(COMPASS.hzmax_aft, 0)
    compass_bytes[33] = get_byte_by_index(COMPASS.hymax_aft, 1)
    compass_bytes[34] = get_byte_by_index(COMPASS.hymax_aft, 0)
    compass_bytes[35] = get_byte_by_index(COMPASS.hxmax_aft, 1)
    compass_bytes[36] = get_byte_by_index(COMPASS.hxmax_aft, 0)

    # Before Minimums
    compass_bytes[37] = get_byte_by_index(COMPASS.hzmin_bef, 1)
    compass_bytes[38] = get_byte_by_index(COMPASS.hzmin_bef, 0)
    compass_bytes[39] = get_byte_by_index(COMPASS.hymin_bef, 1)
    compass_bytes[40] = get_byte_by_index(COMPASS.hymin_bef, 0)
    compass_bytes[41] = get_byte_by_index(COMPASS.hxmin_bef, 1)
    compass_bytes[42] = get_byte_by_index(COMPASS.hxmin_bef, 0)

    # During Minimums
    compass_bytes[43] = get_byte_by_index(COMPASS.hzmin_dur, 1)
    compass_bytes[44] = get_byte_by_index(COMPASS.hzmin_dur, 0)
    compass_bytes[45] = get_byte_by_index(COMPASS.hymin_dur, 1)
    compass_bytes[46] = get_byte_by_index(COMPASS.hymin_dur, 0)
    compass_bytes[47] = get_byte_by_index(COMPASS.hxmin_dur, 1)
    compass_bytes[48] = get_byte_by_index(COMPASS.hxmin_dur, 0)

    # After Minimums
    compass_bytes[49] = get_byte_by_index(COMPASS.hzmin_aft, 1)
    compass_bytes[50] = get_byte_by_index(COMPASS.hzmin_aft, 0)
    compass_bytes[51] = get_byte_by_index(COMPASS.hymin_aft, 1)
    compass_bytes[52] = get_byte_by_index(COMPASS.hymin_aft, 0)
    compass_bytes[53] = get_byte_by_index(COMPASS.hxmin_aft, 1)
    compass_bytes[54] = get_byte_by_index(COMPASS.hxmin_aft, 0)

    # CRC
    crc = get_crc_16(compass_bytes[0:55])
    compass_bytes[55] = get_byte_by_index(crc, 1)
    compass_bytes[56] = get_byte_by_index(crc, 0)

    # Convert it to hex bytes
    for i in range(len(compass_bytes)):
        hexbyte = '{:02x}'.format(compass_bytes[i])
        buffer[i * 2 + 1] = ord(hexbyte[0])
        buffer[i * 2 + 2] = ord(hexbyte[1])

    return bytes(buffer)

def write_response(conn, response):
    if response:
        time.sleep(0.1)
        written = conn.write(response)
        print(f'Wrote {written} bytes back: {response}')

# Return True if we should enter sleep mode, False otherwise
def handle_message(conn, msg):
    print(f'Received: {msg}')
    match msg:
        case b'F':
            print('Generating a data frame...')
            write_response(conn, generate_data_frame())
        case b'M':
            print('Generating a magnotometer reading')
            write_response(conn, generate_magnotometer_reading())
        case b'S':
            print('Entering sleep mode')
            write_response(conn, b'S\r\nSTOP1\r\nxxxxx\r\n')
            return True
        case _:
            print('Unexpected message, waiting for next message...')

    return False

# Keep handling messages until timeout after about 8 minutes or until sleep mode
def handle_messages(conn):
    timeouts = 0
    while True:
        readers, _, _ = select.select([conn], [], [], 1.0)
        if not (conn in readers):
            if timeouts == 450:
                print('Timed out waiting for a message, entering sleep mode...')
                return
            else:
                timeouts += 1
        else:
            msg = conn.read(1)
            timeouts = 0
            if handle_message(conn, msg):
                return

if __name__ == '__main__':
    conn = parse_options()
    try:
        while True:
            # Wait for a wake-up signal
            while not conn.cts:
                pass

            # Send the wake-up message
            print('Got the wake-up message')
            write_response(conn, b'EMA Python Script Version 1.0\r\n')
            handle_messages(conn)

    except Exception as e:
        print('Closing the connection...')
        conn.close()
        raise e
