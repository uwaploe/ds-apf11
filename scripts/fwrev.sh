#!/usr/bin/env bash
#
# BASH version of the FwRev support program
#
# Requires GNU date

: ${DATE=date}

# Check for GNU date
t=$($DATE +%s)
$DATE -d@${t} 2> /dev/null 1>&2 || {
    echo "GNU date required" 1>&2
    exit 1
}

# If REFDATE is set, use that as the reference date, otherwise use the
# current time
tnow=$($DATE +%s)
[[ -n $REFDATE ]] && {
    tnow=$($DATE -d "$REFDATE" +%s)
}

# Read the (non zero padded) month and day into an array
f=($($DATE -d@${tnow} +'%-m %-d'))
let dt=0
case "$1" in
    -e)
        # Force even day of the month
        if (((f[1] & 1) == 1)); then
            if ((f[0] == 2 && f[1] >= 29)); then
                let dt="(((28 - f[1])*86400))"
            elif ((f[1] >= 31)); then
                let dt="(((30 - f[1])*86400))"
            else
                dt=86400
            fi
        fi
        ;;
    -o)
        # Force odd day of the month
        if (((f[1] & 1) == 0)); then
            if ((f[0] == 2 && f[1] >= 28)); then
                let dt="(((27 - f[1])*86400))"
            elif ((f[1] >= 30)); then
                let dt="(((29 - f[1])*86400))"
            else
                dt=86400
            fi
        fi
        ;;
    -?|-h)
        echo "Usage: $(basename $0) [-e|-o]" 1>&2
        exit 0
        ;;
esac

let tnow="((tnow + dt))"
$DATE -d@${tnow} +'%m%d%y'
