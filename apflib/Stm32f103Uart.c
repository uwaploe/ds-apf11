#ifndef STM32F103UART_H
#define STM32F103UART_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 UART model
 * -------------------------------------------------
 *
 * The SwiftWare UART model is non-blocking for both received and
 * transmitted data but is interrupt-driven only for received data.
 * The DMA peripheral of the Stm32 is not used because, though more
 * efficient with respect to core MCU cycles, it is somewhat more
 * complicated and consumes slightly more power.  Even at the slowest
 * clock speed (8MHz), the Apf11 core remains considerably
 * underutilized with respect to processor cycles.  So both power
 * considerations and the KISS-principle disfavor the use of the DMA
 * peripheral for the Stm32 UARTs.
 *
 * For received data, an interrupt handler manages the transfer of
 * each byte from the Stm32's USART.DR register to the UART's FIFO
 * buffer.  The interrupt handler also detects and maintains a record
 * of over-run, noise, framing, and parity errors.  If too many errors
 * are detected, the handler disables error interrupts as a
 * fault-tolerance measure in order allow recovery and to avoid
 * infinite loops or interrupt-binding.
 *
 * For transmitted data, the SwiftWare UART model is not
 * interrupt-driven so that the precise timing of transmitted
 * characters can be known by higher level software that uses this
 * low-level API.  Instead of allowing an interrupt-handler to manage
 * the transmission of each byte, this API implements a function to
 * transmit a single character by loading the byte in the Tx register
 * and then monitoring the USART.SR register to detect assertion of
 * the transmit-complete (TC) flag.  After each byte is transmitted
 * then the function returns so that the next byte can be
 * transmitted.  A time-out feature prevents blocking.
 *
 *    Important Note: The SwiftWare UART model was designed as a
 *       drop-in replacment for ST's HAL UART model because the HAL
 *       model was terribly unreliable, complicated, full of nasty
 *       bugs, and resulted in frequent (implicit) infinite loops.
 *       The SwiftWare model is very much simpler, more reliable, and
 *       has no explicit or (as far as I know) implicit infinite
 *       loops.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103UartChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* prototypes for functions with external linkage */
long int Stm32UartConfig(long int baud);
int      Stm32UartErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                         unsigned char *ovrun, unsigned char *noise);

/* declare the console serial port */
extern struct SerialPort uart1;

/* define the return states of the Stm32 UART API */
extern const char UartNoConfig;           /* uart not configured or enabled */
extern const char UartError;              /* uart error encountered */
extern const char UartInvalidBaud;        /* invalid baud rate */
extern const char UartNull;               /* NULL function argument */
extern const char UartFail;               /* general failure */
extern const char UartOk;                 /* general success */

#endif /* STM32F103UART_H */
#ifdef STM32F103UART_C
#undef STM32F103UART_C

#include <apf11.h>
#include <fifo.h>
#include <logger.h>
#include <mcu.h>
#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>
#include <string.h>

/* define the return states of the Stm32 UART API */
const char UartNoConfig          =  -4; /* uart not configured or enabled */
const char UartError             =  -3; /* uart error encountered */
const char UartInvalidBaud       =  -2; /* invalid baud rate */
const char UartNull              =  -1; /* NULL function argument */
const char UartFail              =   0; /* general failure */
const char UartOk                =   1; /* general success */

/* declare external management functions for static UART_HandleTypeDef object */
UART_HandleTypeDef *HAL_UART1_HandleGet(void);

/* declare functions with external linkage */
int Stm32UartIrqHandler(void);

/* declare functions with static linkage */
static int na(void); 
static int na_(int state);
static int Uart1Getb(unsigned char *byte);
static int Uart1IFlush(void);
static int Uart1IBytes(void);
static int Uart1IOFlush(void);
static int Uart1OBytes(void);
static int Uart1OFlush(void);
static int Uart1Putb(unsigned char c);

/* define the UART_HandleTypeDef object for UART1 */      
static UART_HandleTypeDef huart1;

/* define a fifo buffer for UART1 */
persistent static unsigned char Uart1FifoBuf[32768U];

/* define a Fifo object for the console terminal */
struct Fifo Uart1Fifo = {Uart1FifoBuf, sizeof(Uart1FifoBuf), 0, 0, 0, 0};

/* define a serial port for the Stm32's USART1 interface */
struct SerialPort uart1 = {Uart1Getb, Uart1Putb, Uart1IFlush, Uart1IOFlush, Uart1OFlush,
                           Uart1IBytes, Uart1OBytes, na, na_, na, na_, na, Stm32UartConfig};

/*------------------------------------------------------------------------*/
/* anonymous structure to contain status information for USART1 I/O       */
/*------------------------------------------------------------------------*/
static struct 
{
   /* counts of various error conditions */
   unsigned char nbreak,parity,frame,noise,ovrun;
      
} Uart1Status = {0,0,0,0,0};

/*------------------------------------------------------------------------*/
/* function to retrieve the static UART_HandleTypeDef pointer              */
/*------------------------------------------------------------------------*/
UART_HandleTypeDef *HAL_UART1_HandleGet(void) {return (&huart1);}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na(void)
{
   return UartNull;
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na_(int state)
{
   return UartNull;
}

/*------------------------------------------------------------------------*/
/* SerialPort member function to configure the USART1 serial interface    */
/*------------------------------------------------------------------------*/
/**
   This function is the member of the SerialPort abstraction object
   that is designed to manage the configuration of the Stm32's USART1
   serial port.  The Stm32 is configured so that USART1 is strictly a
   3-wire interface (Tx,Rx,Gnd); the Stm32 GPIO pins that would be
   necessary to implement RTS/CTS hardware handshaking have been
   configured for other uses.

      \begin{verbatim}
      input:

         baud....This parameter controls whether this function enables
                 or disables the USART1 serial port or else queries
                 for its current state/baud-rate:

                   baud>0: If the specified mode is positive then
                      USART1 is enabled and configured to use the
                      specified baud-rate.  For example, if mode=9600
                      then the serial port is configured for 9600
                      baud.  The supported baud rates are: 300, 1200,
                      2400, 4800, 9600, 19200, 38400.

                   mode=0: If the specified mode is zero then the
                      USART1 serial port is disabled.

                   mode<0: If the specified mode is negative then this
                      function queries USART1 to determine if it is
                      enabled or disabled; if enabled then the
                      baud-rate is returned.

         output:

            If the mode is positive and USART1 was successfully
            enabled/configured then this function returns a positive
            value.  If the mode is zero and USART1 is successfully
            disabled then this function returns a positive value.  If
            the mode was negative the this function returns the
            current baud rate if USART1 is enabled else zero if USART1
            is disabled.  A negative return value indicates that an
            exception was encountered.
      \end{verbatim}
*/
long int Stm32UartConfig(long int baud)
{
   /* define the logging signature */
   cc *FuncName="Stm32UartConfig()";
   
   /* initialize the return value */
   int status=UartOk;

   /* configure and enable USART1 */
   if (baud>0)
   {
      /* configure Stm32 UART1 Tx pin (A.9) */
      Stm32GpioConfig(GPIOA, UART1_TXD_Pin, AOutPP);

      /* configure Stm32 UART1 Rx pin (A.10) */
      Stm32GpioConfig(GPIOA, UART1_RXD_Pin, InFlt);

      /* initialize the UART handle object */
      memset((void *)(&huart1),0,sizeof(UART_HandleTypeDef));

      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART1_IRQn);

      /* initialize the Stm32 peripheral address */
      huart1.Instance = USART1;

      /* disable USART1 and its clock */
      __HAL_UART_DISABLE(&huart1); __HAL_RCC_USART1_CLK_DISABLE(); 

      /* configure for bidirectional communications */
      huart1.Init.Mode = UART_MODE_TX_RX;

      /* disable hardware flow control */
      huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;

      /* Stm32f103 has only one available mode for oversampling */
      huart1.Init.OverSampling = UART_OVERSAMPLING_16;
      
      /* initialize the UART for 8-bits, 1 stop bit, no parity */
      huart1.Init.WordLength = UART_WORDLENGTH_8B;
      huart1.Init.StopBits = UART_STOPBITS_1;
      huart1.Init.Parity = UART_PARITY_NONE;
      
      /* select the baud rate */
      switch (baud)
      {
         /* enumerate the supported baud rates */
         case   300LU: case  1200LU: case  2400LU: case   4800LU: case 9600LU:
         case 19200LU: case 38400LU: case 57600LU: case 115200LU:
         {
            /* initialize the baud rate */
            huart1.Init.BaudRate = baud; break;
         }

         /* catch unsupported baud rates */
         default:
         {
            /* log the error */
            LogEntry(FuncName,"UART1: Unsupported baud rate: %lu\n",baud);

            /* reset the return value */
            status=UartInvalidBaud;
         }
      }

      /* configure and enable USART1 for the specified baud rate */
      if (status>0)
      {
         /* enable the UART clock */
         __HAL_RCC_USART1_CLK_ENABLE();

         /* set temporary busy state */
         huart1.State = HAL_UART_STATE_BUSY;

         /* initialize USART1.CR1 to the reset state given in the Stm32f103 datasheet */
         huart1.Instance->CR1=0x00000000LU;

         /* configure USART1.CR1 for the word-length, parity, and tx/rx mode */
         SET_BIT(huart1.Instance->CR1, (uint32_t)(huart1.Init.WordLength | huart1.Init.Parity | huart1.Init.Mode));

         /* initialize USART1.CR2 to the reset state given in the Stm32f103 datasheet */
         huart1.Instance->CR2=0x00000000LU;

         /* configure USART1.CR2 with number of stop bits  */
         SET_BIT(huart1.Instance->CR2, huart1.Init.StopBits);

         /* initialize USART1.CR3 to the reset state given in the Stm32f103 datasheet */
         huart1.Instance->CR3=0x00000000LU;
 
         /* configure USART1.BRR with the baud rate */
         huart1.Instance->BRR = UART_BRR_SAMPLING16(HAL_RCC_GetPCLK2Freq(), huart1.Init.BaudRate);

         /* initialize USART1.GTPR to the reset state given in the Stm32f103 datasheet */
         huart1.Instance->GTPR=0x00000000LU;
   
         /* Enable the peripheral */
         __HAL_UART_ENABLE(&huart1);

         /* clear the status register by reading USART1.SR followed by USART1.DR */
         {volatile unsigned long int sr=huart1.Instance->SR; UNUSED(sr);}
         {volatile unsigned long int dr=huart1.Instance->DR; UNUSED(dr);}

         /* flush the USART1 fifo */
         flush(&Uart1Fifo);
      
         /* enable receiver interrupts on USART1 */
         SET_BIT(huart1.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);
      
         /* enable error interrupts on USART1 */
         SET_BIT(huart1.Instance->CR3, USART_CR3_EIE);

         /* initialize the peripheral interrupt */
         HAL_NVIC_SetPriority(USART1_IRQn, 1, 0); HAL_NVIC_EnableIRQ(USART1_IRQn);
      
         /* Initialize the UART error code and state */
         huart1.ErrorCode = HAL_UART_ERROR_NONE; huart1.State= HAL_UART_STATE_READY;
      }

      /* invalid configuration, deconfigure and disable USART1 */
      else Stm32UartConfig(0);
   }
   
   /* query for the state of the USART1 serial interface */
   else if (baud<0) {status=(huart1.Instance==USART1) ? huart1.Init.BaudRate : UartFail;}
   
   /* deconfigure and disable USART1 */
   else
   {
      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART1_IRQn);

      /* initialize the Stm32 peripheral address */
      huart1.Instance = USART1;

      /* disable USART1 */
      __HAL_UART_DISABLE(&huart1);

      /* initialize USART1.CR1 to the reset state given in the Stm32f103 datasheet */
      huart1.Instance->CR1=0x00000000LU;

      /* initialize USART1.CR2 to the reset state given in the Stm32f103 datasheet */
      huart1.Instance->CR2=0x00000000LU;

      /* initialize USART1.CR3 to the reset state given in the Stm32f103 datasheet */
      huart1.Instance->CR3=0x00000000LU;

      /* initialize USART1.GTPR to the reset state given in the Stm32f103 datasheet */
      huart1.Instance->GTPR=0x00000000LU;
      
      /* disable the USART1 clock */
      __HAL_RCC_USART1_CLK_DISABLE(); 

      /* flush the USART1 fifo */
      flush(&Uart1Fifo);

      /* initialize the UART handle object */
      memset((void *)(&huart1),0,sizeof(UART_HandleTypeDef));
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the Uart1 interrupt         */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Uart1
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int Stm32UartErrors(unsigned char *nbreak,unsigned char *parity,unsigned char *frame,
                    unsigned char *ovrun,unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      Uart1Status.nbreak=0; Uart1Status.parity=0; Uart1Status.frame=0;
      Uart1Status.ovrun=0;  Uart1Status.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=Uart1Status.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=Uart1Status.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=Uart1Status.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=Uart1Status.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=Uart1Status.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* interrupt handler for input on the USART1 serial interface             */
/*------------------------------------------------------------------------*/
/*
   This is the interrupt handler for the USART1 serial interface.  The
   SwiftWare UART model is interrupt-driven and non-blocking with
   respect to received data.  For transmitted data, the SwiftWare UART
   model is non-blocking but not interrupt-driven as described in the
   comment section near the top of this module.  This function also
   manages error interrupts and maintains a record of UART errors.

   This function returns a positive value on success or else zero on
   failure.  A negative return value indicates that an exception was
   encountered. 
*/
int Stm32UartIrqHandler(void)
{
   /* define the logging signature */
   cc *FuncName="Stm32UartIrqHandler()";

   /* initialize the return value */
   int status=UartOk;

   /* validate the configuration for USART1 */
   if (huart1.Instance!=USART1) {LogEntry(FuncName,"Invalid UART1 configuration.\n"); status=UartNoConfig;}

   else
   {
      /* define maximum number of errors before error-interrupts are disabled */
      const unsigned char ErrMax=10;

      /* read the status register of USART1 */
      volatile unsigned long int SR = huart1.Instance->SR;

      /* read the data register of USART1 */
      volatile unsigned long int DR = huart1.Instance->DR;

      /* read control register 1 of USART1 */
      volatile unsigned long int CR1 = huart1.Instance->CR1; 

      /* read control register 2 of USART1 */
      volatile unsigned long int CR2 = huart1.Instance->CR2; 

      /* read control register 3 of USART1 */
      volatile unsigned long int CR3 = huart1.Instance->CR3; 

      /* temporarily disable receiver interrupts on USART1 */
      CLEAR_BIT(huart1.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

      /* temporarily disable transmitter interrupts on USART1 */
      CLEAR_BIT(huart1.Instance->CR1, USART_CR1_TXEIE | USART_CR1_TCIE);

      /* temporarily disable error interrupts on USART1 */
      CLEAR_BIT(huart1.Instance->CR3, USART_CR3_EIE);

      /* avoid compiler warnings */
      UNUSED(CR2);
   
      /* test for parity error */
      if (SR&USART_SR_PE) 
      {
         /* check if parity-error interrupt is enabled */
         if (CR1&USART_CR1_PEIE) huart1.ErrorCode |= HAL_UART_ERROR_PE;

         /* increment the parity-error counter */
         if ((Uart1Status.parity++) > ErrMax) CLEAR_BIT(CR1,USART_CR1_PEIE);
      }

      /* test for framing error */
      if (SR&USART_SR_FE)
      {
         /* check if framing-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart1.ErrorCode |= HAL_UART_ERROR_FE;
      
         /* increment the framing-error counter */
         if ((Uart1Status.frame++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for noise error */
      if (SR&USART_SR_NE)
      {
         /* check if noise-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart1.ErrorCode |= HAL_UART_ERROR_NE;

         /* increment the noise-error counter */
         if ((Uart1Status.noise++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for over-run error */
      if (SR&USART_SR_ORE)
      {
         /* check if over-runt error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart1.ErrorCode |= HAL_UART_ERROR_ORE;

         /* increment the over-run error counter */
         if ((Uart1Status.ovrun++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for byte received */
      if (SR&USART_SR_RXNE)
      {
         /* add the byte to the fifo */
         push(&Uart1Fifo,(unsigned char)(DR&0xff));
      }

      /* test if Tx-empty bit is asserted */
      if (SR&USART_SR_TXE)
      {
         /* SwiftWare USART1 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TXEIE) CLEAR_BIT(CR1,USART_CR1_TXEIE);
      }

      /* test if Tx-complete bit is asserted */
      if (SR&USART_SR_TC)
      {
         /* SwiftWare USART1 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TCIE) CLEAR_BIT(CR1,USART_CR1_TCIE);
      }

      /* re-enable interrups on USART1 */
      huart1.Instance->CR1=CR1; huart1.Instance->CR3=CR3;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the Uart1 FIFO queue                       */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the Uart1 serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the Uart1 FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int Uart1Getb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "Uart1Getb()";

   /* initialize the return value */
   int status=UartFail;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=UartNull;}

   /* validate the configuration for USART1 */
   else if (huart1.Instance!=USART1) {LogEntry(FuncName,"Invalid UART1 configuration.\n"); status=UartNoConfig;}
   
   else
   {
      /* disable receive interrupts on UART1 to prevent the fifo from changing state */
      CLEAR_BIT(huart1.Instance->CR1, USART_CR1_RXNEIE);

      /* pop the next byte out of the fifo queue */
      status=pop(&Uart1Fifo,byte);
          
      /* enable receive interrupts on UART1 */
      SET_BIT(huart1.Instance->CR1, USART_CR1_RXNEIE);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue of USART1  */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes in the Rx FIFO queue of USART1.
*/
static int Uart1IBytes(void)
{
   return Uart1Fifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the USART1's Rx FIFO queue                           */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the USART1 serial port.
   This function returns a positive value on success and zero on
   failure.  A negative return value indicates an exception was
   encountered.
*/
static int Uart1IFlush(void)
{
   /* define the logging signature */
   cc *FuncName = "Uart1IFlush()";

   /* initialize the return value */
   int status=UartOk;
            
   /* validate the configuration for USART1 */
   if (huart1.Instance!=USART1) {LogEntry(FuncName,"Invalid UART1 configuration.\n"); status=UartNoConfig;}

   else
   {
      /* disable receive interrupts on UART1 to prevent the fifo from changing state */
      CLEAR_BIT(huart1.Instance->CR1, USART_CR1_RXNEIE);

      /* flush the Com1 serial port's fifo buffer */
      status=flush(&Uart1Fifo); 
                
      /* enable receive interrupts on UART1 */
      SET_BIT(huart1.Instance->CR1, USART_CR1_RXNEIE);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to flush the USART1's Tx register and Rx FIFO queue           */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Tx register and the Rx FIFO queue of the
   USART1 serial port.  This function returns a positive value on
   success and zero on failure.  A negative return value indicates an
   exception was encountered.
*/
static int Uart1IOFlush(void)
{
   /* initialize the return value */
   int err,status=UartOk;
            
   /* flush the Tx register */
   if ((err=Uart1OFlush())<=0) {status=err;}

   /* flush the Rx fifo */
   if ((err=Uart1IFlush())<=0) {status=err;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue of USART1  */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes in the Tx register of USART1.
*/
static int Uart1OBytes(void) {return 0;}

/*------------------------------------------------------------------------*/
/* function to flush the USART1's Tx transmit register                    */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Tx register of the USART1 serial port.
   This function returns a positive value on success and zero on
   failure.  A negative return value indicates an exception was
   encountered.
*/
static int Uart1OFlush(void)
{
   /* define the logging signature */
   cc *FuncName = "Uart1OFlush()";

   /* initialize the return value */
   int status=UartFail;

   /* validate the configuration for USART1 */
   if (huart1.Instance!=USART1) {LogEntry(FuncName,"Invalid UART1 configuration.\n"); status=UartNoConfig;}

   else
   {
      /* define the timeout period */
      const unsigned long int Timeout=50;

      /* initialize the reference time for the time-out feature */
      unsigned long int To=HAL_GetTick();

      /* construct time-out loop */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart1.Instance->SR;

         /* check if the transmit register is empty */
         if (SR&USART_SR_TXE) {status=UartOk; break;}
      }

      /* continue the time-out loop */
      for (status=UartFail; (HAL_GetTick()-To)<Timeout;)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart1.Instance->SR;

         /* check if the transmit-complete flag is set */
         if (SR&USART_SR_TC) {status=UartOk; break;}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a byte to the USART1 serial port                     */
/*------------------------------------------------------------------------*/
/**
   This function writes a byte to the Stm32's USART1 serial port.
   This function returns the number of bytes written to the serial
   port.  A negative return value indicates an exception was
   encountered.
*/ 
static int Uart1Putb(unsigned char byte)
{
   /* define the logging signature */
   cc *FuncName="Uart1Putb()";

   /* initialize the return value */
   int status = UartFail;

   /* validate the configuration for USART1 */
   if (!huart1.Instance) {LogEntry(FuncName,"UART1 not configured or enabled.\n"); status=UartNoConfig;}

   else
   {
      /* define the timeout period */
      const unsigned long int Timeout=50;
   
      /* initialize the reference time for the time-out feature */
      unsigned long int To=HAL_GetTick();

      /* construct time-out loop */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart1.Instance->SR;
         
         /* check if the transmit register is empty */
         if (SR&USART_SR_TXE) {huart1.Instance->DR=byte; break;}
      }
      
      /* continue the time-out loop */
      for (status=UartFail; (HAL_GetTick()-To)<Timeout;)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart1.Instance->SR;

         /* check if the transmit-complete flag is set */
         if (SR&USART_SR_TC) {status=UartOk; break;}
      }
   }
   
   return status;
}

#endif /* STM32F103UART_C */

