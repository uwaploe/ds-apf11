#ifndef PRAM_H
#define PRAM_H (0x01f0U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define pramChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>

/* prototypes of functions with external linkage */
int PRamCrcCheck(unsigned short int *CrcRef, int N);
int PRamCrcRefRead(unsigned short int *CrcRef, int N);
int PRamCrcRefWrite(unsigned short int *CrcRef, int N);

/* define partitioning parameters */
#define PRamCrcRefVec (0x6000U)
#define PRamBlockSize (0x0400U)
#define PRamBlockCnt  (0x0800U)

/* external referenc for CRC reference vector */
extern persistent unsigned short int PRamCrcRef[PRamBlockCnt];

#endif /* PRAM_H */
#ifdef PRAM_C
#undef PRAM_C

#include <apf11.h>
#include <at25eeprom.h>
#include <crc16bit.h>
#include <logger.h>

/* create peristent storage for persistent SRAM reference vector */
persistent unsigned short int PRamCrcRef[PRamBlockCnt];

/*------------------------------------------------------------------------*/
/* function to compare persistent RAM signatures against their references */
/*------------------------------------------------------------------------*/
/**
   This function computes the CRC signature of each block of
   persistent RAM against a vector of reference CRCs.

   \begin{verbatim}
   input:
      CrcRef...A vector of reference CRCs for each block of the
               persistent RAM.
      N........The number of blocks to be read from EEPROM.
   \end{verbatim}
*/
int PRamCrcCheck(unsigned short int *CrcRef, int N)
{
   /* define the logging signature */
   cc *FuncName = "PRamCrcCheck()";

   /* initialize the return value */
   int n,status=At25Ok; unsigned long int addr;

   /* write the test pattern block by block */
   for (addr=Apf11ExtRamLo,n=0; addr<Apf11ExtRamHi; n++,addr+=PRamBlockSize)
   {
      /* compute a 16-bit CRC of the block */
      unsigned int crc = Crc16Bit((const unsigned char *)addr,PRamBlockSize);

      /* guard against buffer over-run */
      if (n>=N) {LogEntry(FuncName,"Persistent RAM block out of range [addr,n]: "
                          "0x%08lx %d\n",addr,n); status=At25Fail;}

      /* compare the CRC against the reference */
      else if (crc!=CrcRef[n])
      {
         LogEntry(FuncName,"CRC failure in block %04d at address "
                  "0x%08lx: 0x%04x != 0x%04x (expected).\n",n,
                  addr,crc,CrcRef[n]); status=At25Fail;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the persistent reference CRC vector from EEPROM       */
/*------------------------------------------------------------------------*/
/**
   This function reads the reference CRC vector from EEPROM.  Each
   element of the vector represents the CRC of a block of persistent
   RAM.  Changes in any element of the block can be detected by
   computing the CRC of the block and comparing to the corresponding
   element of the reference vector.

   \begin{verbatim}
   output:
      CrcRef...A vector of reference CRCs for each block of the
               persistent RAM.
      N........The number of blocks to be read from EEPROM.
   \end{verbatim}
*/
int PRamCrcRefRead(unsigned short int *CrcRef, int N)
{
   /* define the logging signature */
   cc *FuncName = "PRamCrcRefRead()";

   /* initialize the return value */
   int err,status=At25Ok;
   
   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }

   /* read the mission parameters from EEPROM */
   else if ((err=At25Read((unsigned char *)CrcRef, N*sizeof(unsigned short int), PRamCrcRefVec))<=0)
   {
      /* make a logentry of the AT25 write failure */
      LogEntry(FuncName,"Attempt to read persistent RAM CRC reference "
               "vector failed. [err=%d]\n",err); status=At25Fail;
   }
   
   return status;
 }

/*------------------------------------------------------------------------*/
/* function to write the persistent reference CRC vector from EEPROM      */
/*------------------------------------------------------------------------*/
/**
   This function write the reference CRC vector from EEPROM.  Each
   element of the vector represents the CRC of a block of persistent
   RAM.  Changes in any element of the block can be detected by
   computing the CRC of the block and comparing to the corresponding
   element of the reference vector.

   \begin{verbatim}
   input:
      CrcRef...A vector of reference CRCs for each block of the
               persistent RAM.
      N........The number of blocks to be read from EEPROM.
   \end{verbatim}
*/
int PRamCrcRefWrite(unsigned short int *CrcRef, int N)
{
   /* define the logging signature */
   cc *FuncName = "PRamCrcRefWrite()";

   /* initialize the return value */
   int n,err,status=At25Ok; unsigned long int addr;

   /* write the test pattern block by block */
   for(addr=Apf11ExtRamLo,n=0; addr<=Apf11ExtRamHi; addr+=PRamBlockSize,n++)
   {
      /* compute a 16-bit CRC of the block */
      unsigned int crc = Crc16Bit((const unsigned char *)addr,PRamBlockSize);

      /* assign the CRC to the block-signature vector */
      if (n<N && n<PRamBlockSize) {CrcRef[n]=crc;}

      else {LogEntry(FuncName,"Persistent RAM block out of bounds "
                     "[addr,n]: 0x%08lx %d\n",addr,n); status=At25Fail;}
   }

   /* enable the AT25 eeprom on the Apf11 */
   if ((err=At25Enable())<=0)
   {
      /* make a logentry of the AT25 eeprom failure */
      LogEntry(FuncName,"AT25 eeprom failed to enable. [err=%d]\n",err);

      /* initialize the return value to indicate the exception */
      status=At25Config;
   }

   else
   {
      /* write the persistent RAM reference CRC vector */
      if ((err=At25Write((unsigned char *)CrcRef, N*sizeof(unsigned short int), PRamCrcRefVec))<=0)
      {
         /* make a logentry of the AT25 write failure */
         LogEntry(FuncName,"Attempt to write CRC reference vector "
                  "failed. [err=%d]\n",err); status=At25Fail;
      }
      
      /* disable the eeprom */
      At25Disable();
   }
   
   return status;
}

#endif /* PRAM_C */
