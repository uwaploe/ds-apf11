#ifndef MAX7301_H
#define MAX7301_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Description of the SwiftWare API for the MAX7301 GPIO expander
   --------------------------------------------------------------

   A large fraction of the Apf11's functionality depends on reliable
   operation of the MAX7301 GPIO exander.  For this reason, most of
   the configurability/flexibility of the Max7301 is purposely
   hardcoded out of existence by defining a static constant
   initialization of the configuration registers; no API
   configurability is implemented except for the small number of user
   defined GPIO signals on the Apf11's GPIO header J9.  The low-level
   API functions are all prefixed by 'SpGpio' which serve as the
   foundation for higher level functions that relate explicitly to the
   Apf11 moreso than the Max7301.  In general, higher level code that
   uses this API will seldom make use of the low-level 'SpGpio'
   functions.

   For Max7301 ports that are configured for input, attempts to write
   to those ports will result in an error; they are read-only ports.
   However, ports that are configured for output are subject to both
   reading and writing so that the port can be cleared (ie., set low),
   asserted (ie., set high), or queried.

   By design, the Max7301 is intended to be configured via the
   SpGpioInit() function as part of the boot-up process every time
   that the Apf11's STM32 processor exits standby mode.  However,
   SpGpioInit() leaves the Max7301 in a very low-power nonfunctional
   state where all ports are forced to be inputs.  This has the effect
   of disabling a large fraction of the Apf11 functionality while
   making it possible to activate this functionality without having to
   consider low-level initialization of the Max7301.  If SpGpioInit()
   is executed during boot-up then all that is required is to call
   SpGpioWake() in order to activate the functionality of the Apf11
   that is associated with the Max7301.  But even this step can be
   ignored because this API ensures that SpGpioWake() is called prior
   to executing any other supported service.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Max7301ChangeLog "$RCSfile$  $Revision$  $Date$"

/* enumerate the spare GPIO modes */
typedef enum {SpOutPPL = 0x01U, SpInFlt = 0x02U,
              SpOutPPH = 0x01U, SpInPU  = 0x03U} SpGpioPortConfig;

/* enumerate the states of the port */
typedef enum {UNDEF=-1, CLEAR=0, ASSERT=1} SpPortState;

/* enumerate the modes of HDrive operation */
typedef enum {Retract=-1, Brake=0, Extend=1} HDriveMode;

/* declare functions with external linkage */
int AirPumpPowerOff(void);
int AirPumpPowerOn(void);
int AirValveClose(void);
int AirValveOpen(void);
int AnalogMeasureDisable(void);
int AnalogMeasureEnable(void);
int BuzzerOff(void);
int BuzzerOn(void);
int BuzzerSos(void);
int CtdPowerCycle(unsigned char sec);
int CtdPowerOff(void);
int CtdPowerOn(void);
int HDrive(int dir);
int HydraulicPumpMonitorOff(void);
int HydraulicPumpMonitorOn(void);
int J9PowerOff(void);
int J9PowerOn(void);
int LedDisable(void);
int LedEnable(void);
int MagSwitchHigh(void);
int MagSwitchReset(void);
int MagSwitchToggled(void);
int SpDuartPowerEnable(void);
int SpDuartPowerDisable(void);
int SpDuartReset(unsigned int millisec);
int SpGpioConfig(unsigned short int port, SpGpioPortConfig config);
int SpGpioEnable(void);
int SpGpioDisable(void);
int SpGpioInit(void);
int SpGpioSleep(void);
int SpGpioStateGet(unsigned short int port, SpPortState *state);
int SpGpioStateSet(unsigned short int port, SpPortState state);
int SpGpioWake(void);
int WatchDogEvent(void);
int WatchDogLatchReset(void);
int WatchDogPet(void);

/* define the return states of the Stm32 spare sensor API */
extern const char SpareInvalid;         /* invalid selection */
extern const char SparePowerFail;       /* power failure */
extern const char SpareConfig;          /* MAX7301 configuration fault */
extern const char SpareSpiConfig;       /* SPI configuration fault */
extern const char SpareNull;            /* NULL function argument */
extern const char SpareFail;            /* general failure */
extern const char SpareOk;              /* general success */

#endif /* MAX7301_H */
#ifdef MAX7301_C

#include <apf11.h>
#include <logger.h>
#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Spi.h>

/* define the pins of the spare GPIO peripheral */
#define MAGSWITCH_LATCH_STATE   (0x0400U)
#define WATCHDOG_LATCH_STATE    (0x0500U)
#define MAGSWITCH_LATCH_RESET   (0x0600U)
#define WATCHDOG_LATCH_RESET    (0x0700U)
#define SPARE_DUART_POWER_EN    (0x0800U)
#define VBATT_GPIO_EN           (0x0900U)
#define WATCHDOG_PET_PIN        (0x0a00U)
#define USER_J9_14              (0x0b00U)
#define SPARE_DUART_RESET       (0x0c00U)
#define USER_J9_15              (0x0d00U)
#define USER_J9_16              (0x0e00U)
#define USER_J9_17              (0x0f00U)
#define PUMP_PWR_EN             (0x1000U)
#define AIR_PUMP_PWR_EN         (0x1100U)
#define ANALOG_3P3VA_EN         (0x1200U)
#define RA4                     (0x1300U)
#define RA5                     (0x1400U)
#define RA6                     (0x1500U)
#define RA7                     (0x1600U)
#define AP_RA1                  (0x1700U)
#define AP_RA2                  (0x1800U)
#define AP_RA3                  (0x1900U)
#define AP_RA4                  (0x1a00U)
#define BUZZER                  (0x1b00U)
#define MAGSWITCH_STATE         (0x1c00U)
#define CTD_PWR_DISABLE         (0x1d00U)
#define LED_ENABLE              (0x1e00U)
#define TP2                     (0x1f00U)

/* define the return states of the Stm32 GPIO API */
const char SpareInvalid           = -5; /* invalid selection */
const char SparePowerFail         = -4; /* power failure */
const char SpareConfig            = -3; /* MAX7301 configuration fault */
const char SpareSpiConfig         = -2; /* SPI configuration fault */
const char SpareNull              = -1; /* NULL function argument */
const char SpareFail              =  0; /* general failure */
const char SpareOk                =  1; /* general success */

/* define configuration parameters */
#define BaudRate (0x0000U)
#define WordMode (0x0800U)
#define Cr2      (0x0000U)
#define NoCrc    (0x0000U)

/* define MAX7301 command masks */
#define NOOP                    (0x0000U)
#define WRITE                   (0x0000U)
#define READ                    (0x8000U)
#define PIN                     (0x2000U)
#define HDRIVE                  (0x5300U)
#define WAKE                    (0x0001U)

/* define MAX7301 registers */
#define CONFIG                  (0x0400U)
#define CONFIG_P04_P07          (0x0900U)
#define CONFIG_P08_P11          (0x0a00U)
#define CONFIG_P12_P15          (0x0b00U)
#define CONFIG_P16_P19          (0x0c00U)
#define CONFIG_P20_P23          (0x0d00U)
#define CONFIG_P24_P27          (0x0e00U)
#define CONFIG_P28_P31          (0x0f00U)

/* enumerate the modes of HDrive operation */
typedef enum {BRAKE = 0x00U, EXTEND = 0x0c, RETRACT = 0x03,
              LATCH = 0x00U, OPEN   = 0xa0, CLOSE   = 0x50} SpHDriveState;

/* declarations for functions with nonexposed external linkage */
int SpGpioCsPulse(unsigned char msec);

/* declarations for functions with static linkage */
static int SpGpioHDriveSet(SpHDriveState mode);

/*========================================================================*/
/* Notes about initialization/configuration of the MAX7301                */
/*========================================================================*/
/**
   The Max7301Config array of unsigned char is used to configure the
   MAX7301 GPIO expander.  Tables 1 & 2 (below) were taken from the
   data sheet of the MAX7301.

   Table 1. Port Configuration Map
   +--------------------------+------+-----------------------------------+
   |        Register          | Hex  |         Register Data             |
   |                          | Code | D{7,6} | D{5,4} | D{3,2} | D{1,0} |
   +--------------------------+------+--------+--------+--------+--------+
   | Port config for P07->P04 | 0x09 |   P07  |   P06  |   P05  |   P04  |
   | Port config for P11->P08 | 0x0a |   P11  |   P10  |   P09  |   P08  |
   | Port config for P15->P12 | 0x0b |   P15  |   P14  |   P13  |   P12  |
   | Port config for P19->P16 | 0x0c |   P19  |   P18  |   P17  |   P16  |
   | Port config for P23->P20 | 0x0d |   P23  |   P22  |   P21  |   P20  |
   | Port config for P27->P24 | 0x0e |   P27  |   P26  |   P25  |   P24  |
   | Port config for P31->P28 | 0x0f |   P31  |   P30  |   P29  |   P28  |
   +--------------------------+------+-----------------------------------+

   Table 2. Port Configuration Matrix
   +--------+-------------+-----+--------------------+-----------+--------+
   | Mode   | Function    | Reg | Pin Behavior       | Address   | Config |
   +--------+-------------+-----+--------------------+-----------+----+---+
   |             DO NOT USE THIS SETTING             | 0x09-0x0f |  0 | 0 |
   +--------+-------------+-----+--------------------+-----------+----+---+
   | Output | GPIO Output | 0x0 | Active-low output  | 0x09-0x0f |  0 | 1 |
   |        |             | 0x1 | Active-high output | 0x09-0x0f |  0 | 1 |
   +--------+-------------+-----+--------------------+-----------+----+---+
   | Input  | no pull-up  | --- | Schmitt trigger    | 0x09-0x0f |  0 | 1 |
   | Input  | pull-up     | --- | Schmitt trigger    | 0x09-0x0f |  1 | 1 |
   +--------+-------------+-----+--------------------+-----------+----+---+

   The IO configuration of each of the twenty-eight ports is defined below.
*/
   #define P04Config (SpInFlt)  /* Mag switch latch state.                */
   #define P05Config (SpInFlt)  /* Watch dog latch state.                 */
   #define P06Config (SpOutPPL) /* Mag switch latch reset.                */
   #define P07Config (SpOutPPL) /* Watch dog latch reset.                 */
   #define P08Config (SpOutPPL) /* Spare DUART power enable.              */
   #define P09Config (SpOutPPL) /* Enable power for spare GPIO (J9).      */
   #define P10Config (SpOutPPL) /* Watch dog restart (pet pin).           */
   #define P11Config (SpOutPPL) /* User: Spare GPIO signal on J9:14.      */
   #define P12Config (SpOutPPH) /* Spare DUART reset.                     */
   #define P13Config (SpOutPPL) /* User: Spare GPIO signal on J9:15.      */
   #define P14Config (SpOutPPL) /* User: Spare GPIO signal on J9:16.      */
   #define P15Config (SpOutPPL) /* User: Spare GPIO signal on J9:17.      */
   #define P16Config (SpOutPPL) /* Enable power for hydraulic H-drive.    */
   #define P17Config (SpOutPPL) /* Enable power for air pump.             */
   #define P18Config (SpOutPPL) /* Enable 3V power for A/D samples.       */
   #define P19Config (SpOutPPL) /* Hydraulic pump's H-drive (RA4).        */
   #define P20Config (SpOutPPL) /* Hydraulic pump's H-drive (RA5).        */
   #define P21Config (SpOutPPL) /* Hydraulic pump's H-drive (RA6).        */
   #define P22Config (SpOutPPL) /* Hydraulic pump's H-drive (RA7).        */
   #define P23Config (SpOutPPL) /* Pneumatic solenoid H-drive (AP_RA1).   */
   #define P24Config (SpOutPPL) /* Pneumatic solenoid H-drive (AP_RA2).   */
   #define P25Config (SpOutPPL) /* Pneumatic solenoid H-drive (AP_RA3).   */
   #define P26Config (SpOutPPL) /* Pneumatic solenoid H-drive (AP_RA4).   */
   #define P27Config (SpOutPPL) /* Activate the buzzer.                   */
   #define P28Config (SpInFlt)  /* Real-time state of mag switch.         */
   #define P29Config (SpOutPPL) /* Enable/disable power for the CTD.      */
   #define P30Config (SpOutPPL) /* Enable/disable power for the LEDs.     */
   #define P31Config (SpOutPPL) /* Test point (TP2).                      */
/*
   The twenty-eight configurations above are integrated into the array
   of seven unsigned char's below.
*/
static const unsigned char Max7301Config[] =
{
   (P07Config<<6) | (P06Config<<4) | (P05Config<<2) | P04Config,
   (P11Config<<6) | (P10Config<<4) | (P09Config<<2) | P08Config,
   (P15Config<<6) | (P14Config<<4) | (P13Config<<2) | P12Config,
   (P19Config<<6) | (P18Config<<4) | (P17Config<<2) | P16Config,
   (P23Config<<6) | (P22Config<<4) | (P21Config<<2) | P20Config,
   (P27Config<<6) | (P26Config<<4) | (P25Config<<2) | P24Config,
   (P31Config<<6) | (P30Config<<4) | (P29Config<<2) | P28Config   
};

/*------------------------------------------------------------------------*/
/* function to disable power to the Apf11's air pump                      */
/*------------------------------------------------------------------------*/
/**
   This function disables power to the air pump. 

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int AirPumpPowerOff(void)
{
   /* define the logging signature */
   cc *FuncName="AirPumpPowerOff()";

   /* initialize return value */
   int err,status=SpareOk;

   /* disable power to the Apf11's air pump */
   if ((err=SpGpioStateSet(AIR_PUMP_PWR_EN,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disable power to the Apf11's "
               "air pump failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable power to the Apf11's air pump                       */
/*------------------------------------------------------------------------*/
/**
   This function enables power to the air pump. 

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int AirPumpPowerOn(void)
{
   /* define the logging signature */
   cc *FuncName="AirPumpPowerOn()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable power to the Apf11's air pump */
   if ((err=SpGpioStateSet(AIR_PUMP_PWR_EN,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable power to the Apf11's "
               "air pump failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to close the air valve                                        */
/*------------------------------------------------------------------------*/
/**
   This function applies a 320mA current source to pins 2 and 3 of Apf11
   connector J8.  The current will source from pin 3 and sink to pin 2.  The
   air valve should be connected to J8 so that this function will have the
   effect of opening the solenoid air valve.
*/
int AirValveClose(void)
{
   /* initialize the return value */
   int err,status=SpareOk;

   /* apply the pneumatic solenoid H-drive to close the valve */
   if ((err=SpGpioHDriveSet(CLOSE))<=0) {status=err;}
   
   /* apply a 60 millisecond current pulse to the air-valve solenoid */
   Wait(60);
   
   /* deactivate the pneumatic solenoid H-drive */
   if ((err=SpGpioHDriveSet(LATCH))<=0) {status=err;}

   LogEntry("AirValveClose()", "\n");

   return status;     
}

/*------------------------------------------------------------------------*/
/* function to open the air valve                                         */
/*------------------------------------------------------------------------*/
/**
   This function applies a 320mA current source to pins 2 and 3 of Apf11
   connector J8.  The current will source from pin 2 and sink to pin 3.  The
   air valve should be connected to J8 so that this function will have the
   effect of opening the solenoid air valve.
*/
int AirValveOpen(void)
{
   /* initialize the return value */
   int err,status=SpareOk;

   /* apply the pneumatic solenoid H-drive to open the valve */
   if ((err=SpGpioHDriveSet(OPEN))<=0) {status=err;}
   
   /* apply a 60 millisecond current pulse to the air-valve solenoid */
   Wait(60);
   
   /* deactivate the pneumatic solenoid H-drive */
   if ((err=SpGpioHDriveSet(LATCH))<=0) {status=err;}

   LogEntry("AirValveOpen()", "\n");

   return status;     
}

/*------------------------------------------------------------------------*/
/* function to disable power to the Apf11's analog measurement system     */
/*------------------------------------------------------------------------*/
/**
   This function disables power to Apf11's analog measurement system.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int AnalogMeasureDisable(void)
{
   /* define the logging signature */
   cc *FuncName="AnalogMeasureDisable()";

   /* initialize return value */
   int err,status=SpareOk;

   /* disable power to the Apf11's analog system */
   if ((err=SpGpioStateSet(ANALOG_3P3VA_EN,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disable power to the Apf11's analog "
               "measurement system failed. [err=%d]\n",err); status=err;
   }

   /* power stabilization pause */
   else {Wait(10);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable power to the Apf11's analog measurement system      */
/*------------------------------------------------------------------------*/
/**
   This function enables power to Apf11's analog measurement system.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int AnalogMeasureEnable(void)
{
   /* define the logging signature */
   cc *FuncName="AnalogMeasureEnable()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable power to the Apf11's analog system */
   if ((err=SpGpioStateSet(ANALOG_3P3VA_EN,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable power to the Apf11's analog "
               "measurement system failed. [err=%d]\n",err); status=err;
   }

   /* power stabilization pause */
   else {Wait(10);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to turn-on the buzzer                                         */
/*------------------------------------------------------------------------*/
int BuzzerOff(void) {return SpGpioStateSet(BUZZER,CLEAR);}

/*------------------------------------------------------------------------*/
/* function to turn-off the buzzer                                        */
/*------------------------------------------------------------------------*/
int BuzzerOn(void) {return SpGpioStateSet(BUZZER,ASSERT);}

/*------------------------------------------------------------------------*/
/* sound an acoustic SOS                                                  */
/*------------------------------------------------------------------------*/
int BuzzerSos(void)
{
   unsigned char n,dit=100,dah=250;

   /* emit sequence of three short pulses */
   for (n=0; n<3; n++) {BuzzerOn(); Wait(dit); BuzzerOff(); Wait(dit);}

   /* emit sequence of three long pulses */
   for (n=0; n<3; n++) {BuzzerOn(); Wait(dah); BuzzerOff(); Wait(dit);}

   /* emit sequence of three short pulses */
   for (n=0; n<3; n++) {BuzzerOn(); Wait(dit); BuzzerOff(); Wait(dit);}

   /* pause after SOS */
   Wait(500);

   return 1;
}

/*------------------------------------------------------------------------*/
/* function to power cycle the CTD                                        */
/*------------------------------------------------------------------------*/
/**
   This function power-cycles the CTD by disabling power for a
   user-specified number of seconds and then enabling power.  

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int CtdPowerCycle(unsigned char sec)
{
   /* initialize return value */
   int err[2],status=SpareOk;

   /* disable power to the CTD */
   err[0]=CtdPowerOff();

   /* pause for a specified period */
   sleep(sec);
   
   /* enable power to the CTD */
   err[1]=CtdPowerOn();

   /* evaluate success or failure of the power-cycle */
   if (err[0]<=0 || err[1]<=0) {status=SpareFail;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable power to the CTD                                   */
/*------------------------------------------------------------------------*/
/**
   This function disables power to the Apf11's CTD header CN1.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int CtdPowerOff(void)
{
   /* define the logging signature */
   cc *FuncName="CtdPowerOff()";

   /* initialize return value */
   int err,status=SpareOk;

   /* disable power to the Apf11's CTD header CN1 */
   if ((err=SpGpioStateSet(CTD_PWR_DISABLE,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disable power to the Apf11's CTD "
               "header CN1 failed. [err=%d]\n",err); status=err;
   }

   /* power stabilization pause */
   else {Wait(10);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable power to the CTD                                   */
/*------------------------------------------------------------------------*/
/**
   This function disables power to the Apf11's CTD header CN1.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int CtdPowerOn(void)
{
   /* define the logging signature */
   cc *FuncName="CtdPowerOn()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable power to the Apf11's CTD header CN1 */
   if ((err=SpGpioStateSet(CTD_PWR_DISABLE,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable power to the Apf11's CTD "
               "header CN1 failed. [err=%d]\n",err); status=err;
   }

   /* power stabilization pause */
   else {Wait(10);}

   return status;
}

/*------------------------------------------------------------------------*/
/* control the state of the H-drive for the Apf11 hydraulic pump          */
/*------------------------------------------------------------------------*/
/*
   This function places the Apf11 H-drive in one of three states.
   Actually, the H-drive has four possible states but, for DC motor
   control, two of these states are (almost) degenerate.  The effect
   of the H-drive is to connect either ground or the battery directly
   to pins {2,5} and {3,6} of connector J17 of the Apf11.

   \begin{verbatim}
   input:

      dir ... The DC motor is controlled by the sign of this function
              argument.  Positive values cause the DC motor to extend
              the piston and negative values make the motor retract
              the piston.  Zero connects both ends of the motor
              winding to ground which has the effect of braking the DC
              motor so that it strongly resists turning in either
              direction.  The relationship between input and the
              output at connector H11 is summarized in the following
              truth table.
      
                               dir  J17:{3,6} J17:{2,5}
                               ------------------------
                                0        Grnd      Grnd
                                +        VBat      Grnd
                                -        Grnd      VBat
                               n/a       VBat      VBat

              The Apf11 has the ability to simultaneously connect the
              battery to pins {2,5} and {3,6} of connector J17.  This
              has the same braking effect on a DC motor as connecting
              both pins to ground but has the serious downside that
              potential leaks will consume battery energy.
              
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int HDrive(int dir)
{
   /* initialize the return value */
   int status=SpareOk;
   
   /* set the H-drive to extend the piston */
   if (dir>0) {status=SpGpioHDriveSet(EXTEND);}
   
   /* set the H-drive to retract the piston */
   else if (dir<0) {status=SpGpioHDriveSet(RETRACT);}

   /* set the H-drive to brake the piston */
   else {status=SpGpioHDriveSet(BRAKE);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable the buoyancy pump's monitor circuit                */
/*------------------------------------------------------------------------*/
/**
   This function disables the buoyancy pump's monitor circuit.  The
   monitor circuit facilitates the measurement of the Apex's piston
   position and the current consumed by the hydraulic pump.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int HydraulicPumpMonitorOff(void)
{
   /* define the logging signature */
   cc *FuncName="HydraulicPumpMonitorOff()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable hydraulic pump's monitor circuit */
   if ((err=SpGpioStateSet(PUMP_PWR_EN,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disable hydraulic pump's monitor "
               "circuit failed. [err=%d]\n",err); status=err;
   }

   /* power stabilization pause */
   else {Wait(10);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the buoyancy pump's monitor circuit                 */
/*------------------------------------------------------------------------*/
/**
   This function enables the buoyancy pump's monitor circuit.  The
   monitor circuit facilitates the measurement of the Apex's piston
   position and the current consumed by the hydraulic pump.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int HydraulicPumpMonitorOn(void)
{
   /* define the logging signature */
   cc *FuncName="HydraulicPumpMonitorOn()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable hydraulic pump's monitor circuit */
   if ((err=SpGpioStateSet(PUMP_PWR_EN,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable hydraulic pump's monitor "
               "circuit failed. [err=%d]\n",err); status=err;
   }
   
   /* power stabilization pause */
   else {Wait(10);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the Apf11's diagnostic LEDs                         */
/*------------------------------------------------------------------------*/
/**
   This function enables the Apf11's diagnostic LEDs.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int LedDisable(void)
{
   /* define the logging signature */
   cc *FuncName="LedDisable()";

   /* initialize return value */
   int err,status=SpareOk;

   /* disable the Apf11's diagnostic LEDs */
   if ((err=SpGpioStateSet(LED_ENABLE,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disaable the Apf11's diagnostic "
               "LEDs failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the Apf11's diagnostic LEDs                         */
/*------------------------------------------------------------------------*/
/**
   This function enables the Apf11's diagnostic LEDs.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int LedEnable(void)
{
   /* define the logging signature */
   cc *FuncName="LedEnable()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable the Apf11's diagnostic LEDs */
   if ((err=SpGpioStateSet(LED_ENABLE,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable the Apf11's diagnostic "
               "LEDs failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* disable switched battery power to the Apf11's spare GPIO header J9     */
/*------------------------------------------------------------------------*/
/**
   This function disables switched battery power to the Apf11's spare
   GPIO header J9. 

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int J9PowerOff(void)
{
   /* define the logging signature */
   cc *FuncName="J9PowerOff()";

   /* initialize return value */
   int err,status=SpareOk;

   /* disable switched power to the Apf11's spare GPIO header J9 */
   if ((err=SpGpioStateSet(VBATT_GPIO_EN,CLEAR))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable switched power to Apf11 spare "
               "GPIO header J9 failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* enable switched battery power to the Apf11's spare GPIO header J9      */
/*------------------------------------------------------------------------*/
/**
   This function enables switched battery power to the Apf11's spare
   GPIO header J9. 

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int J9PowerOn(void)
{
   /* define the logging signature */
   cc *FuncName="J9PowerOn()";

   /* initialize return value */
   int err,status=SpareOk;

   /* enable switched power to the Apf11's spare GPIO header J9 */
   if ((err=SpGpioStateSet(VBATT_GPIO_EN,ASSERT))<=0)
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to disable switched power to Apf11 spare "
               "GPIO header J9 failed. [err=%d]\n",err); status=err;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the mag-switch is actively being held high    */
/*------------------------------------------------------------------------*/
/**
   This function tests to see if the magnetic reset switch is actively
   being held high with a magnet.  The Apf11 has an RC circuit
   composed of R301=22, R253=10K, and C100=1.0uF so that the RC time
   constants are 22usec for mag-switch activation and 10msec for
   deactivation.  The magnetic switch has to be held closed for only
   22usec or so before the Apf11 will recognise the transistion.  A
   positive value is returned if the magswitch is actively held high
   by a magnet or zero if not.
*/
int MagSwitchHigh(void)
{
   /* define a local work object */
   SpPortState state;

   /* return the state of spare GPIO Apf11:U4:P4 */
   SpGpioStateGet(MAGSWITCH_STATE,&state);

   return ((state>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* reset the flip-flop that records the magnetic switch toggle            */
/*------------------------------------------------------------------------*/
/**
   This function resets the flip-flop that records toggles of the magnetic
   switch.

   Note: For unknown reasons, the Apf11 design reduced the RC
      timescales (relative to the Apf9) from (1.0s,22ms) for switch
      activation and deactivation down to (22us,10ms).  WRC was unable
      to find any reason or rationale for this change.  During testing
      of a simpler initial version of this function, I was able to
      generate debouncing effects/defects at will.  This prompted me
      to add a debouncing filter to wait for a 100ms window of time
      when the magswitch remains low before resetting the magswitch
      latch circuit.  This measure is only partially effective.  There
      is a simple hardware fix that requires replacing R301,C100 with
      270K,2.2uF components, respectively. After this hardware fix has
      been implemented and tested then the debouncing filter can be
      removed from this function. -dds 12/23/2017

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int MagSwitchReset(void)
{
   /* define the logging signature */
   cc *FuncName="MagSwitchReset()";

   /* define a local work object */
   int err,status=SpareOk;

   /* define the number of ticks per second */
   #define sec (1000UL)

   /* define some local work objects to implement the debouncing algorithm */
   unsigned long int Tref=HAL_GetTick(),To=Tref,T=Tref;

   /* loop to find a 100msec window where the magswitch stays low */
   for (err=SpareFail; (T-Tref)<sec;)
   {
      /* restart the test window if the magswitch is high */
      if (MagSwitchHigh()) {To=HAL_GetTick();}

      /* refresh the counter */
      T=HAL_GetTick();

      /* exit the loop if magswitch inactive & low */
      if ((T-To)>0.1*sec) {err=SpareOk; break;}
   }

   /* make a logentry that warns of active transitions of the magswitch  */ 
   if (err<=0) {LogEntry(FuncName,"Warning: Magnetic switch debouncing "
                         "algorithm ineffective.\n");}
   
   /* assert the signal that resets the magswitch latch circuit (Apf11:U78) */
   if ((err=SpGpioStateSet(MAGSWITCH_LATCH_RESET,ASSERT))<=0) {status=err;}

   /* pause before clearing the magswitch reset signal */
   Wait(5);

   /* clear the signal that resets the magswitch latch circuit (Apf11:U78) */
   if ((err=SpGpioStateSet(MAGSWITCH_LATCH_RESET,CLEAR))<=0) {status=err;}

   /* note the failure in the engineering logs */
   if (status<=0) {LogEntry(FuncName,"Magnetic switch reset sequence "
                            "failed. [status=%d]\n",status);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the magnetic reset switch has been toggled    */
/*------------------------------------------------------------------------*/
/**
   This function tests to see if the magnetic reset switch has been
   toggled by the user.  The Apf11 has an RC circuit composed of
   R301=22 and C12=1uF so that the RC time constant is 22us.  The
   magnetic switch has to be held closed for only 22us or so before
   the Apf11 (via the magswitch latch circuit) will recognise the
   transistion.

   \begin{verbatim}
   output:
      A positive value is returned if the magswitch was toggled and
      zero is return if not.
   \end{verbatim}
*/
int MagSwitchToggled(void)
{
   /* define a local work object */
   SpPortState state;

   /* return the state of U4:P28 */
   SpGpioStateGet(MAGSWITCH_LATCH_STATE,&state);

   return ((state>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to enable power to the spare DUART                            */
/*------------------------------------------------------------------------*/
/**
   This function enables power to the spare DUART and initiates a
   reset immediately after power-up.  If the DUART is already powered
   then this function immediately returns without taking any action.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpDuartPowerEnable(void)
{
   /* define the logging signature */
   cc *FuncName="SpDuartPowerEnable()";
  
   /* initialize the return value */
   int status=SpareOk;

   /* define local work objects */
   int err; SpPortState state;

   /* check if power to the spare DUART is already enabled */
   if ((err=SpGpioStateGet(SPARE_DUART_POWER_EN,&state))<=0 || state!=ASSERT)
   {
      /* enable power to the spare DUART */
      if ((status=SpGpioStateSet(SPARE_DUART_POWER_EN,ASSERT))<=0)
      {
         /* log the failure */
         LogEntry(FuncName,"Attempt to enable spare DUART power failed. "
                  "[err=%d, status=%d]\n",err,status);
      }

      /* reset the spare DUART */
      else if ((err=SpDuartReset(5))<=0)
      {
         /* log the reset failure */
         LogEntry(FuncName,"Attempt to reset the spare DUART failed. "
                  "[err=%d]\n",err); status=SpareFail;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable power to the spare DUART                           */
/*------------------------------------------------------------------------*/
/**
   This function disables power to the spare DUART.  The spare GPIO
   pin that controls the DUART's reset signal is also cleared to avoid
   powering a dead DUART.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpDuartPowerDisable(void)
{
   /* define the logging signature */
   cc *FuncName="SpDuartPowerDisable()";
  
   /* initialize the return value */
   int err,status=SpareOk;

   /* clear the GPIO's pin to reset the spare DUART */
   if ((err=SpGpioStateSet(SPARE_DUART_RESET,CLEAR))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Spare DUART's reset deactivation "
               "failed. [err=%d]\n",err); status=SpareFail;
   }
   
   /* disable power to the spare DUART */
   if ((err=SpGpioStateSet(SPARE_DUART_POWER_EN,CLEAR))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to disable spare DUART power failed. "
               "[err=%d]\n",err); status=SpareFail;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to apply a square-wave reset signal to the spare DUART        */
/*------------------------------------------------------------------------*/
/**
   This function applies a square-wave reset signal of a specified
   duration to the reset pin of the spare DUART.  The reset pin is
   cleared which induces the DUART into a reset state.  The reset
   state is maintained for a specified period before the reset pin is
   asserted again to induce the DUART out of the reset state.
   Following that, another pause of the same duration is applied
   before the function returns.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpDuartReset(unsigned int millisec)
{
   /* define the logging signature */
   cc *FuncName="SpDuartReset()";
  
   /* initialize the return value */
   int err1,err2,status=SpareOk;

   /* clear the spare DUART's reset pin to induce the DUART into a reset state */
   if ((err1=SpGpioStateSet(SPARE_DUART_RESET,CLEAR))<=0) {status=SpareFail;}

   /* pause for the specified duration */
   if (millisec) {Wait(millisec);}

   /* assert the spare DUART's reset pin to induce the DUART out of a reset state */
   if ((err2=SpGpioStateSet(SPARE_DUART_RESET,ASSERT))<=0) {status=SpareFail;}

   /* pause for the specified duration */
   if (millisec) {Wait(millisec);}

   /* log the failure to generate a square wave of the specified duration */
   if (status<=0) {LogEntry(FuncName,"Square wave of duration %umsec failed. "
                            "[err1=%d, err2=%d]\n",millisec,err1,err2);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure a USER port                                      */
/*------------------------------------------------------------------------*/
/**
   This function allows the user to configure any of the USER ports.
   This function enforces that only USER ports are allowed to be
   reconfigured and ensures that the configuration selection itself is
   valid.

   \begin{verbatim}
   input:

      port....One of the four port identifiers:
                 {USER_J9_14, USER_J9_15, USER_J9_16, USER_J9_17}
              that are subject to user control and whose electrical
              interface is on the Apf11's J9 header, J9:{14,15,16,17}.

     config...One of the four valid port configuration opcodes:
              SpOutPPL, SpOutPPH, SpInFlt, SpInPU.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioConfig(unsigned short int port, SpGpioPortConfig config)
{
   /* define the logging signature */
   cc *FuncName="SpGpioConfig()";
  
   /* initialize the return value */
   int err,status=SpareOk;
   
   /* ensure the port is configurable */
   if (port!=USER_J9_14 && port!=USER_J9_15 && port!=USER_J9_16 && port!=USER_J9_17)
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Nonconfigurable port selected: 0x%04x\n",(port>>8)); status=SpareNull;
   }

   /* validate the configuration */
   else if (config!=SpOutPPL && config!=SpOutPPH && config!=SpInFlt && config!=SpInPU)
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port configuration: 0x%02x\n",config); status=SpareInvalid;
   }
            
   /* induce the Max7301 out of sleep/suspend mode */
   else if ((err=SpGpioEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"MAX7301 failed to enable. [err=%d]\n",err); status=err;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int quad,new;

      /* compute the port quad */
      volatile unsigned short int QUAD = CONFIG_P04_P07 + (((port>>8)/4-1)*0x0100U);
      
      /* transmit the command to read the port configuration from the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ | QUAD); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      quad=SPI2->DR; err=SPI2->SR;
        
      /* generate the SCK signal required to read the configuration from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); quad=SPI2->DR; err=SPI2->SR;

      /* clear the bits for the port to be configured */
      new = (quad&0xff) & (~(0x3U<<(2*((port>>8)%4))));

      /* compute the port's new configuration */
      new |= (config<<(2*((port>>8)%4)));

      /* check if the new/old configurations differ */
      if (new!=(quad&0xff))
      {
         /* transmit the new port configuration command to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (WRITE | QUAD | new); Stm32SpiBusy(SPI2,timeout);

         /* transmit the port configuration command to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (READ  | QUAD);       Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); quad=SPI2->DR; err=SPI2->SR;

         /* confirm the new configuration */
         if (new!=(quad&0xff))
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Configuration failure for port 0x%02x: 0x%02x != 0x%02x "
                     "(expected).\n",(port>>8),(quad&0xff),new); status=SpareConfig;
         }
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to pulse the MAX7301 chip-select signal                       */
/*------------------------------------------------------------------------*/
/**
   This function pulses the MAX7301's chip-select signal by first
   asserting and then clearing the signal.  After each of the assert
   and clear operations, a user-specified pause (0-255 milliseconds)
   is inserted; the pattern is assert, pause, clear, pause.  The end
   result is a square wave whose period is twice the user-specified
   pause.

   \begin{verbatim}
   input:
      msec....The number of milliseconds to pause after EACH of the
              assert and clear operations.  The total period of the
              square-wave pulse is twice this value.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioCsPulse(unsigned char msec) {return Spi2CsPulse(Spi2DevSpGpio,msec);}

/*------------------------------------------------------------------------*/
/* function to disable the spare GPIO enable                              */
/*------------------------------------------------------------------------*/
/**
   This function disables the spare GPIO interface by powering-down
   the interface, deselecting the Max7301 device, and reinitializing
   the Stm32's SPI2 bus.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioDisable(void)
{
   /* define the logging signature */
   cc *FuncName="SpGpioDisable()";
   
   /* initialize the return value */
   int err,status=SpareOk;

   /* power-up the spare sensor interface */
   if ((err=Apf11SpSensorPowerDown())<=0)
   {
      /* make a logentry of the power-down failure */
      LogEntry(FuncName,"Power-down of spare sensors failed. [err=%d]\n",err);
   }

   /* reinitialize the SPI2 interface */
   Stm32Spi2Init();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the spare GPIO enable                               */
/*------------------------------------------------------------------------*/
/**
   This function enables the spare GPIO interface by powering-up the
   interface, configuring the Stm32's SPI2 bus, and selecting the
   spare GPIO.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioEnable(void)
{
   /* define the logging signature */
   cc *FuncName="SpGpioEnable()";
  
   /* initialize the return value */
   int err,status=SpareOk;

   /* power-up the spare sensor interface */
   if ((err=Apf11SpSensorPowerUp())<=0)
   {
      /* make a logentry of the power-up failure */
      LogEntry(FuncName,"Power-up of spare sensors failed. "
               "[err=%d]\n",err); status=SparePowerFail;
   }
   
   /* configure the Stm32 for communication with the MAX7301 GPIO expander */
   else if ((err=Stm32SpiConfig(SPI2,(WordMode|BaudRate),Cr2,NoCrc))<=0)
   {
      /* make a logentry of the configuration failure */
      LogEntry(FuncName,"Attempt to configure SPI2 failed. "
               "[err=%d]\n",err); status=SpareSpiConfig;
   }

   /* select the MAX7301 GPIO expander for communication */
   else if ((err=Spi2Select(Spi2DevSpGpio))<=0)
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Attempt to select the spare GPIO failed. "
               "[err=%d]\n",err); status=SpareFail;
   }

   /* induce the MAX7301 out of suspend/sleep mode */
   else if ((err=SpGpioWake())<=0)
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"Attempt to wake MAX7301 GPIO failed. "
               "[err=%d]\n",err); status=SpareFail;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the H-drives to a specified mode                 */
/*------------------------------------------------------------------------*/
/**
   This function exploits the Max7301's ability to simultaneously and
   atomically control the state of any eight contiguous ports.  The
   H-drives for the pneumatic solenoid and the hydraulic pump are
   controlled by ports Max7301:P{26-23,22-19}, respectively.  The
   tables below show the state of the port-octet that are associated
   with the three modes of each H-drive.

      \begin{verbatim}
                   Pneumatic Solenoid       
      Mode     P26 P25 P24 P23 P22 P21 P20 P19  Hex 
      ----------------------------------------------
      Latch     0   0   0   0   0   0   0   0   0x00
      Open      1   0   1   0   0   0   0   0   0xa0
      Close     0   1   0   1   0   0   0   0   0x50

                   Hydraulic Pump         
      Mode     P26 P25 P24 P23 P22 P21 P20 P19  Hex 
      ----------------------------------------------
      Brake     0   0   0   0   0   0   0   0   0x00
      Extend    0   0   0   0   1   1   0   0   0x0c
      Retract   0   0   0   0   0   0   1   1   0x03
      \end{verbatim}

   Expressed this way, it is easy to see that the port-octet could
   conceivably code for 256 different configurations of the H-drive
   inputs.  However, only six of these configurations are valid and
   useful.  In fact, the invalid states can induce pathological
   malfunction and/or energy consumption.  This function accepts only
   the six modes above and rejects all others.

   \begin{verbatim}
   input:
      mode...The H-drive that is selected for operation as well as its
             mode of operation are determined by exactly one of the
             six modes shown in the two tables above.  These are
             enumerated to be: {LATCH,OPEN,CLOSE} for the pneumatic
             solenoid or {BRAKE,EXTEND,RETRACT} for the hydraulic
             pump.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
static int SpGpioHDriveSet(SpHDriveState mode)
{
   /* define the logging signature */
   cc *FuncName="SpGpioHDriveSet()";

   /* initialize return value */
   int err,status=SpareOk;

   /* validate the function argument */
   if (!(mode==BRAKE || mode==EXTEND || mode==RETRACT) &&
       !(mode==LATCH || mode==OPEN   || mode==CLOSE)     )
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid mode selected: %u\n",mode); status=SpareNull;
   }
   
   /* induce the Max7301 out of sleep/suspend mode */
   else if ((err=SpGpioEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"MAX7301 failed to enable. [err=%d]\n",err); status=err;
   }
   
   else
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* transmit the command to enable the specified mode of the hydraulic and solenoid HDrives */
      SpGpioCsPulse(0); SPI2->DR = (WRITE | HDRIVE | mode); Stm32SpiBusy(SPI2,timeout);
 
      /* transmit the command to read the mode from the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ  | HDRIVE);        Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the mode from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* verify the current mode matches the specification */
      if ((config&0xffU)!=mode) {LogEntry(FuncName,"HDrive mode mismatch: 0x%02x != 0x%02x (expected).\n",
                                          (config&0xff),mode); status=SpareFail;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the MAX7301 spare GPIO interface                */
/*------------------------------------------------------------------------*/
/**
   This function initializes spare GPIO interface by powering-up the
   Max7301, configuring the Stm32 SPI2 bus, and using the SPI2 bus to
   configure the Max7301.  After initialization is complete, the
   Max7301 is induced into sleep/suspend mode to reduce energy
   consumption.  While in sleep mode, all of its ports revert to input
   (which can be read) and the pull-up current sources are disabled;
   the device can still communicate via the SPI2 interface.  However,
   data in the port and control registers are retained so that port
   configuration and output levels are restored when the Max7301 exits
   sleep mode.  The Apf11 is designed such that when the Max7301 is
   powered-down or in sleep mode then the peripherals attached to the
   spare GPIO are in a well-defined low-power inactive state.

   This function is intended to be called only once during the boot-up
   phase of the Apf11; subsequent execution of this function is
   acceptable but unnecessary.  When the spare GPIO is to be used, it
   is necessary only to call SpGpioEnable() to exit sleep mode.

   \begin{verbatim}
    output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioInit(void)
{
   /* define the logging signature */
   cc *FuncName="SpGpioInit()";
  
   /* initialize the return value */
   int err,status=SpareOk;

   /* power-up the spare sensor interface */
   if ((err=Apf11SpSensorPowerUp())<=0)
   {
      /* make a logentry of the power-up failure */
      LogEntry(FuncName,"Power-up of spare sensors failed. "
               "[err=%d]\n",err); status=SparePowerFail;
   }
   
   /* configure the Stm32 for communication with the MAX7301 GPIO expander */
   else if ((err=Stm32SpiConfig(SPI2,(WordMode|BaudRate),Cr2,NoCrc))<=0)
   {
      /* make a logentry of the configuration failure */
      LogEntry(FuncName,"Attempt to configure SPI2 failed. "
               "[err=%d]\n",err); status=SpareSpiConfig;
   }

   /* select the MAX7301 GPIO expander for communication */
   else if ((err=Spi2Select(Spi2DevSpGpio))<=0)
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Attempt to select the spare GPIO failed. "
               "[err=%d]\n",err); status=SpareFail;
   }

   else
   {
      /* define local work objects */
      int n,ports; volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* loop over the seven sets of quad-ports to write the configuration */
      for (n=0, ports=CONFIG_P04_P07; ports<=CONFIG_P28_P31; ports+=0x0100U, n++)
      {
         /* transmit the port configuration command to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (WRITE | ports | Max7301Config[n]); Stm32SpiBusy(SPI2,timeout);
      }

      /* loop over the seven sets of quad-ports to read/confirm the configuration */
      for (status=SpareOk, n=0, ports=CONFIG_P04_P07; ports<=CONFIG_P28_P31; ports+=0x0100U, n++)
      {
         /* transmit the port configuration command to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (READ | ports); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* compare the port configuration */
         if ((config&0xff)!=Max7301Config[n])
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Configuration failure for ports at 0x%04x: 0x%02x != 0x%02x "
                     "(expected).\n",ports,config,Max7301Config[n]); status=SpareConfig;
         }
      }

      /* make a logentry that the MAX7301 configuration failed */
      if (status<=0) {LogEntry(FuncName,"MAX7301 configuration failed. [status=%d].\n",status);}

      /* check logging criteria */
      else if (debuglevel>=4 || (debugbits&MAX7301_H))
      {
         /* make a logentry that the MAX7301 configuration was successful */
         LogEntry(FuncName,"MAX7301 configuration successful.\n");
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to induce the Max7301 GPIO expander into sleep mode           */
/*------------------------------------------------------------------------*/
/**
   This function induces the Max7301 into sleep/shutdown mode.  The device
   remains powered and configured.   However, all ports are forced to
   inputs (which can be read), and the pull-up current sources are
   disabled.  Data in the port and configuration registers are
   retained so that port configuration and output levels are restored
   when the device is induced out of shutdown mode.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioSleep(void)
{
   /* define the logging signature */
   cc *FuncName="SpGpioSleep()";
  
   /* initialize the return value */
   int err,status=SpareOk; 

   /* check if the spare sensor interface is powered up */
   if ((err=Apf11SpSensorPowerQuery())<=0)
   {
      /* make a logentry about the power failure */
      LogEntry(FuncName,"Aborting: Spare sensor interface is "
               "unpowered. [err=%d]\n",err); status=SparePowerFail;
   }

   else 
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
      
      /* transmit the port configuration command to the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* check if the wake bit is enabled */                  
      if (config&WAKE)
      {
         /* clear the wake bit */
         config = (~WAKE) & (config&0xff);

         /* transmit the command to write the port configuration to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (WRITE | CONFIG | config); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify that the wake bit is cleared */
         if (config&WAKE) {LogEntry(FuncName,"MAX7301 failed to enter sleep/suspend "
                                    "mode. [config=%d]\n",config); status=SpareConfig;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the state of a port on the max7301                    */
/*------------------------------------------------------------------------*/
/**
   This function reads the state of a user specified port on the
   MAX7301 GPIO expander (Apf11:U4).  Any of the twenty-eight ports
   can be queried.

   \begin{verbatim}
   input:
      port....One of the twenty-eight port identifiers in the range
              [0x0400,0x1f00] as defined near the top of this file.
              This function detects and rejects invalid port
              specifiers.

   output:

      state...The current state of the specified port.  If successful,
              this function will return CLEAR for logical zero or else
              ASSERT for logical one.  The value UNDEF is returned
              if an exception is encountered.

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioStateGet(unsigned short int port, SpPortState *state)
{
   /* define the logging signature */
   cc *FuncName="SpGpioStateGet()";
  
   /* initialize the return value */
   int err,status=SpareOk;

   /* initialize return value */
   if (state) {(*state)=UNDEF;}
   
   /* validate the function argument */
   if ((port&0xff) || ((port>>8)<4) || ((port>>8)>31))
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port selected: %u\n",port); status=SpareNull;
   }
   
   /* induce the Max7301 out of sleep/suspend mode */
   else if ((err=SpGpioEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"MAX7301 failed to enable. [err=%d]\n",err); status=err;
   }
   
   else
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
 
      /* transmit the command to read the port configuration from the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ  | PIN | port); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* record the state of the port */
      (*state) = config&0x1;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to set the state of one of the max7301 output ports           */
/*------------------------------------------------------------------------*/
/**
   This function sets the state of one of the Max7301 output ports to
   a user specified value of CLEAR or ASSERT as defined in the
   PinState enumeration.  Only ports that are configured to be outputs
   are eligible for control.  An exception is generated if an attempt
   is made to control the output of a port that is configured as an
   input.  

   \begin{verbatim}
   input:
      port....One of the twenty-eight port identifiers in the range
              [0x0400,0x1f00] as defined near the top of this file.
              This function detects and rejects invalid port
              specifiers.

      state...The specified state for the specified port.  If
              positive, then this function will set the output to a
              logical one.  Otherwise, this function will set the
              output to a logical zero.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioStateSet(unsigned short int port, SpPortState state)
{
   /* define the logging signature */
   cc *FuncName="SpGpioStateSet()";
  
   /* initialize the return value */
   int err,status=SpareOk;

   /* validate the function argument */
   if ((port&0xff) || ((port>>8)<4) || ((port>>8)>31))
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port selected: %u\n",port); status=SpareNull;
   }
   
   /* induce the Max7301 out of sleep/suspend mode */
   else if ((err=SpGpioEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"MAX7301 failed to enable. [err=%d]\n",err); status=err;
   }
   
   else
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* compute the port quad */
      unsigned short int QUAD = CONFIG_P04_P07 + (((port>>8)/4-1)*0x0100U);
      
      /* transmit the command to read the port configuration from the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ | QUAD); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
        
      /* generate the SCK signal required to read the configuration from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* isolate the configuration of the port */
      config = (config>>(2*((port>>8)%4)))&0x3U;

      /* check if the port is configured for output */
      if (config==SpOutPPL || config==SpOutPPH) 
      {
         /* validate the state */
         state = (state>0) ? ASSERT : CLEAR;
         
         /* transmit the command to assert the port */
         SpGpioCsPulse(0); SPI2->DR = (WRITE | PIN | port | state); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (READ  | PIN | port);         Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify the current state of the port matches the specification */
         if ((config&0xffU)!=state) {LogEntry(FuncName,"Port(%u) state mismatch: 0x%02x != 0x%02x (expected).\n",
                                              (port>>8),(config&0xff),state); status=SpareFail;}
      }

      /* check if the port is configured for input */
      else if (config==SpInFlt || config==SpInPU) {LogEntry(FuncName,"Exception: Port 0x%02x configured "
                                                            "for input.\n",(port>>8)); status=SpareInvalid;}

      /* make a logentry about the invalid port configuration */
      else {LogEntry(FuncName,"Port 0x%02x has invalid configuration: 0x%02x\n",(port>>8),config);}
   }
   
   return status;
}


/*------------------------------------------------------------------------*/
/* function to induce the Max7301 GPIO expander out of sleep mode         */
/*------------------------------------------------------------------------*/
/**
   This function induces the Max7301 out of sleep/shutdown mode.  The
   data in the port and configuration registers are retained while in
   sleep mode so that port configuration and output levels are
   restored when the device is induced out of shutdown mode.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int SpGpioWake(void)
{
   /* define the logging signature */
   cc *FuncName="SpGpioWake()";
  
   /* initialize the return value */
   int err,status=SpareOk; 

   /* check if the spare sensor interface is powered up */
   if ((err=Apf11SpSensorPowerQuery())<=0)
   {
      /* make a logentry about the power failure */
      LogEntry(FuncName,"Aborting: Spare sensor interface is "
               "unpowered. [err=%d]\n",err); status=SparePowerFail;
   }

   else 
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
      
      /* transmit the port configuration command to the max7301 */
      SpGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* check if the wake bit is enabled */                  
      if (!(config&WAKE))
      {
         /* clear the wake bit */
         config = WAKE | (config&0xff);

         /* transmit the command to write the port configuration to the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (WRITE | CONFIG | config); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         SpGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         SpGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify that the wake bit is cleared */
         if (!(config&WAKE)) {LogEntry(FuncName,"MAX7301 failed to exit sleep/suspend "
                                       "mode. [config=%d]\n",config); status=SpareConfig;}
         
         /* transmit the command to assert TP2 (port 31) */
         else {SpGpioCsPulse(0); SPI2->DR = (WRITE | PIN | TP2 | 0x01); Stm32SpiBusy(SPI2,timeout);}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the watchdog circuit has been activated       */
/*------------------------------------------------------------------------*/
/**
   This function tests to see if the watchdog circuit has been
   activated to reset the Apf11.

   \begin{verbatim}
   output:
      A positive value is returned if a watchdog event was recorded
      and zero is return if not.
   \end{verbatim}
*/
int WatchDogEvent(void)
{
   /* define a local work object */
   SpPortState state;

   /* return the state of spare GPIO Apf11:U4:P5 */
   SpGpioStateGet(WATCHDOG_LATCH_STATE,&state);
   
   return ((state>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* reset the flip-flop that records an activation of the watch-dog        */
/*------------------------------------------------------------------------*/
/**
   This function resets the flip-flop that records a watchdog event on
   the Apf11.  

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int WatchDogLatchReset(void)
{
   /* define the logging signature */
   cc *FuncName="WatchDogLatchReset()";
   
   /* initialize the return value */
   int err,status=SpareOk;
     
   /* assert the signal that resets the watchdog latch circuit (Apf11:U82) */
   if ((err=SpGpioStateSet(WATCHDOG_LATCH_RESET,ASSERT))<=0) {status=err;}

   /* pause before clearing the magswitch reset signal */
   Wait(5);

   /* clear the signal that resets the watchdog latch circuit (Apf11:U82) */
   if ((err=SpGpioStateSet(WATCHDOG_LATCH_RESET,CLEAR))<=0) {status=err;}

   /* note the failure in the engineering logs */
   if (status<=0) {LogEntry(FuncName,"Watchdog latch reset sequence "
                            "failed. [status=%d]\n",status);}
  
   return status;
}

/*------------------------------------------------------------------------*/
/* function to pet the Apf11's TP5010 watchdog circuit                    */
/*------------------------------------------------------------------------*/
/**
   This function resets the Apf11's watchdog internal timer which
   inhibits generation of a reset signal.  Referred to as "petting the
   watchdog", periodic execution of this function signals that the
   Apf11 is not caught in an infinite loop or other nonoperating
   state.  The Apf11's TP5010 watchdog circuit is tuned by R384 to
   produce a 7200s period.  The watchdog's internal timer must be
   reset sometime during each period in order to prevent the TP5010
   from initiating a reset of the Apf11.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int WatchDogPet(void)
{
   /* define the logging signature */
   cc *FuncName="WatchDogPet()";
   
   /* initialize the return value */
   int err,status=SpareOk;
    
   /* reset the TPL5010's (Apf11:U84) internal counter */
   if ((err=SpGpioStateSet(WATCHDOG_PET_PIN,ASSERT))<=0) {status=err;}

   /* pause before clearing the magswitch reset signal */
   Wait(5);

   /* clear the TPL5010's (Apf11:U84) internal counter */
   if ((err=SpGpioStateSet(WATCHDOG_PET_PIN,CLEAR))<=0) {status=err;}

   /* note the failure in the engineering logs */
   if (status<=0) {LogEntry(FuncName,"Watchdog pet sequence "
                            "failed. [status=%d]\n",status);}
   
   return status;
}

#endif /* MAX7301_C */
