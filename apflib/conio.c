#ifndef CONIO_H
#define CONIO_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 UART model
 * -------------------------------------------------
 *
 * The SwiftWare UART model is non-blocking for both received and
 * transmitted data but is interrupt-driven only for received data.
 * The DMA peripheral of the Stm32 is not used because, though more
 * efficient with respect to core MCU cycles, it is somewhat more
 * complicated and consumes slightly more power.  Even at the slowest
 * clock speed (8MHz), the Apf11 core remains considerably
 * underutilized with respect to processor cycles.  So both power
 * considerations and the KISS-principle disfavor the use of the DMA
 * peripheral for the Stm32 UARTs.
 *
 * For received data, an interrupt handler manages the transfer of
 * each byte from the Stm32's USART.DR register to the UART's FIFO
 * buffer.  The interrupt handler also detects and maintains a record
 * of over-run, noise, framing, and parity errors.  If too many errors
 * are detected, the handler disables error interrupts as a
 * fault-tolerance measure in order allow recovery and to avoid
 * infinite loops or interrupt-binding.
 *
 * For transmitted data, the SwiftWare UART model is not
 * interrupt-driven so that the precise timing of transmitted
 * characters can be known by higher level software that uses this
 * low-level API.  Instead of allowing an interrupt-handler to manage
 * the transmission of each byte, this API implements a function to
 * transmit a single character by loading the byte in the Tx register
 * and then monitoring the USART.SR register to detect assertion of
 * the transmit-complete (TC) flag.  After each byte is transmitted
 * then the function returns so that the next byte can be
 * transmitted.  A time-out feature prevents blocking.
 *
 *    Important Note: The SwiftWare UART model was designed as a
 *       drop-in replacment for ST's HAL UART model because the HAL
 *       model was terribly unreliable, complicated, full of nasty
 *       bugs, and resulted in frequent (implicit) infinite loops.
 *       The SwiftWare model is very much simpler, more reliable, and
 *       has no explicit or (as far as I know) implicit infinite
 *       loops.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define conioChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* prototypes for functions with external linkage */
int  ConioActive(void);
int  ConioDisable(void);
int  ConioEnable(void);
int  ConioPwrDisable(void);
int  ConioPwrEnable(void);
int  ConioPwrQuery(void);
int  getb(void); 
int  getb20ma(unsigned char *byte); 
int  kbdhit(void);
int  putb(unsigned char byte);
int  putb20ma(unsigned char byte);
void putch(char byte);

/* prototypes for functions related to the CTD serial port */
long int ConioConfig(long int baud);
int      ConioErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                     unsigned char *ovrun, unsigned char *noise);

/* declare the console serial port */
extern struct SerialPort conio;

/* define the return states of the Stm32 UART API */
extern const char ConioNoConfig;           /* uart not configured or enabled */
extern const char ConioError;              /* uart error encountered */
extern const char ConioInvalidBaud;        /* invalid baud rate */
extern const char ConioNull;               /* NULL function argument */
extern const char ConioFail;               /* general failure */
extern const char ConioOk;                 /* general success */

#endif /* CONIO_H */
#ifdef CONIO_C

#include <apf11.h>
#include <fifo.h>
#include <logger.h>
#include <mcu.h>
#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>
#include <string.h>

/* define the return states of the Stm32 UART API */
const char ConioNoConfig          =  -4; /* uart not configured or enabled */
const char ConioError             =  -3; /* uart error encountered */
const char ConioInvalidBaud       =  -2; /* invalid baud rate */
const char ConioNull              =  -1; /* NULL function argument */
const char ConioFail              =   0; /* general failure */
const char ConioOk                =   1; /* general success */

/* declare external management functions for static UART_HandleTypeDef object */
UART_HandleTypeDef *HAL_UART3_HandleGet(void);

/* declare functions with external linkage */
int ConioIrqHandler(void);

/* external declaration of primitive function used by printf() */
int __io_putchar(int ch);

/* function prototypes for functions with static linkage */
static int iflush20ma(void); 
static int na(void); 
static int na_(int state);

/* define the UART_HandleTypeDef object for UART1 */      
static UART_HandleTypeDef huart3;

/* define a fifo buffer for the console terminal */
persistent static unsigned char ConsoleFifoBuf[256];

/* define a Fifo object for the console terminal */
struct Fifo ConsoleFifo ={ConsoleFifoBuf, sizeof(ConsoleFifoBuf), 0, 0, 0, 0};

/* define a serial port for the 20ma interface */
struct SerialPort conio = {getb20ma, putb, iflush20ma, iflush20ma, na, na, na, na, na_, na, na_, na, ConioConfig};

/* define some ascii characters special meaning */
#define LF   0x0a /* line feed */
#define CR   0x0d /* carriage return */
#define BKSP 0x08 /* backspace */
#define DEL  0x7f /* DEL */

/*------------------------------------------------------------------------*/
/* anonymous structure to contain status information for console I/O      */
/*------------------------------------------------------------------------*/
static struct 
{
   /* define a flag that is used for buffered input */
   unsigned char ConsoleFifoLineReady;

   /* counts of various error conditions */
   unsigned char nbreak,parity,frame,noise,ovrun;
      
} ConioStatus = {0,0,0,0,0,0};

/*------------------------------------------------------------------------*/
/* function to determine if a computer is attached to the APFx controller */
/*------------------------------------------------------------------------*/
/**
   This function determines of the 20mA console serial current loop is
   active - that is, if the 20mA current loop is connected.
*/
int ConioActive(void)
{
   int i,n;

   /* loop to check the state of the CL RX pin */
   for (n=0,i=0; i<10; i++)
   {
      /* check if the CL RX pin is high */
      if (Stm32GpioIsAssert(GPIOB,UART3_RXD_Pin)>0) n++;

      /* pause a millisecond before rechecking */
      Wait(1);
   }
   
   return (n>3) ? n : 0;
}

/*------------------------------------------------------------------------*/
/* SerialPort member function to configure the USART3 serial interface    */
/*------------------------------------------------------------------------*/
/**
   This function is the member of the SerialPort abstraction object
   that is designed to manage the configuration of the Stm32's USART3
   serial port.  The Stm32 is configured so that USART3 is strictly a
   3-wire interface (Tx,Rx,Gnd); the Stm32 GPIO pins that would be
   necessary to implement RTS/CTS hardware handshaking have been
   configured for other uses.

      \begin{verbatim}
      input:

         baud....This parameter controls whether this function enables
                 or disables the USART3 serial port or else queries
                 for its current state/baud-rate:

                   baud>0: If the specified mode is positive then
                      USART3 is enabled and configured to use the
                      specified baud-rate.  For example, if mode=9600
                      then the serial port is configured for 9600
                      baud.  The supported baud rates are: 300, 1200,
                      2400, 4800, 9600, 19200.

                   mode=0: If the specified mode is zero then the
                      USART3 serial port is disabled.

                   mode<0: If the specified mode is negative then this
                      function queries USART3 to determine if it is
                      enabled or disabled; if enabled then the
                      baud-rate is returned.

         output:

            If the mode is positive and USART3 was successfully
            enabled/configured then this function returns a positive
            value.  If the mode is zero and USART3 is successfully
            disabled then this function returns a positive value.  If
            the mode was negative the this function returns the
            current baud rate if USART3 is enabled else zero if USART3
            is disabled.  A negative return value indicates that an
            exception was encountered.
      \end{verbatim}
*/
long int ConioConfig(long int baud)
{
   /* define the logging signature */
   cc *FuncName="ConioConfig()";
   
   /* initialize the return value */
   int status=ConioOk;

   /* configure and enable USART3 */
   if (baud>0)
   {
      /* configure Stm32 UART3 Tx pin (B.10) */
      Stm32GpioConfig(GPIOB,UART3_TXD_Pin,AOutPP);

      /* configure Stm32 UART3 Rx pin (B.11) */
      Stm32GpioConfig(GPIOB,UART3_RXD_Pin,InFlt);

      /* initialize the UART handle object */
      memset((void *)(&huart3),0,sizeof(UART_HandleTypeDef));

      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART3_IRQn);

      /* initialize the Stm32 peripheral address */
      huart3.Instance = USART3;

      /* disable USART3 and its clock */
      __HAL_UART_DISABLE(&huart3); __HAL_RCC_USART3_CLK_DISABLE(); 

      /* configure for bidirectional communications */
      huart3.Init.Mode = UART_MODE_TX_RX;

      /* disable hardware flow control */
      huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;

      /* Stm32f103 has only one available mode for oversampling */
      huart3.Init.OverSampling = UART_OVERSAMPLING_16;
      
      /* initialize the UART for 8-bits, 1 stop bit, no parity */
      huart3.Init.WordLength = UART_WORDLENGTH_8B;
      huart3.Init.StopBits = UART_STOPBITS_1;
      huart3.Init.Parity = UART_PARITY_NONE;
      
      /* select the baud rate */
      switch (baud)
      {
         /* enumerate the supported baud rates */
         case 300U: case 1200U: case 2400U: case 4800U: case 9600U: case 19200U:
         {
            /* initialize the baud rate */
            huart3.Init.BaudRate = baud; break;
         }

         /* catch unsupported baud rates */
         default:
         {
            /* log the error */
            LogEntry(FuncName,"UART3: Unsupported baud rate: %lu\n",baud);

            /* reset the return value */
            status=ConioInvalidBaud;
         }
      }

      /* configure and enable USART3 for the specified baud rate */
      if (status>0)
      {
         /* enable the UART clock */
         __HAL_RCC_USART3_CLK_ENABLE();

         /* set temporary busy state */
         huart3.State = HAL_UART_STATE_BUSY;

         /* initialize USART3.CR1 to the reset state given in the Stm32f103 datasheet */
         huart3.Instance->CR1=0x00000000LU;

         /* configure USART3.CR1 for the word-length, parity, and tx/rx mode */
         SET_BIT(huart3.Instance->CR1, (uint32_t)(huart3.Init.WordLength | huart3.Init.Parity | huart3.Init.Mode));

         /* initialize USART3.CR2 to the reset state given in the Stm32f103 datasheet */
         huart3.Instance->CR2=0x00000000LU;

         /* configure USART3.CR2 with number of stop bits  */
         SET_BIT(huart3.Instance->CR2, huart3.Init.StopBits);

         /* initialize USART3.CR3 to the reset state given in the Stm32f103 datasheet */
         huart3.Instance->CR3=0x00000000LU;
 
         /* configure USART3.BRR with the baud rate */
         huart3.Instance->BRR = UART_BRR_SAMPLING16(HAL_RCC_GetPCLK1Freq(), huart3.Init.BaudRate);

         /* initialize USART3.GTPR to the reset state given in the Stm32f103 datasheet */
         huart3.Instance->GTPR=0x00000000LU;
   
         /* Enable the peripheral */
         __HAL_UART_ENABLE(&huart3);

         /* clear the status register by reading USART3.SR followed by USART3.DR */
         {volatile unsigned long int sr=huart3.Instance->SR; UNUSED(sr);}
         {volatile unsigned long int dr=huart3.Instance->DR; UNUSED(dr);}

         /* flush the USART3 fifo */
         flush(&ConsoleFifo);
      
         /* enable receiver interrupts on USART3 */
         SET_BIT(huart3.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);
      
         /* enable error interrupts on USART3 */
         SET_BIT(huart3.Instance->CR3, USART_CR3_EIE);

         /* initialize the peripheral interrupt */
         HAL_NVIC_SetPriority(USART3_IRQn, 1, 0); HAL_NVIC_EnableIRQ(USART3_IRQn);
      
         /* Initialize the UART error code and state */
         huart3.ErrorCode = HAL_UART_ERROR_NONE; huart3.State= HAL_UART_STATE_READY;
      }

      /* invalid configuration, deconfigure and disable USART3 */
      else ConioConfig(0);
   }
   
   /* query for the state of the USART3 serial interface */
   else if (baud<0) {status=(huart3.Instance==USART3) ? huart3.Init.BaudRate : ConioFail;}
   
   /* deconfigure and disable USART3 */
   else
   {
      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART3_IRQn);

      /* initialize the Stm32 peripheral address */
      huart3.Instance = USART3;

      /* disable USART3 */
      __HAL_UART_DISABLE(&huart3);

      /* initialize USART3.CR1 to the reset state given in the Stm32f103 datasheet */
      huart3.Instance->CR1=0x00000000LU;

      /* initialize USART3.CR2 to the reset state given in the Stm32f103 datasheet */
      huart3.Instance->CR2=0x00000000LU;

      /* initialize USART3.CR3 to the reset state given in the Stm32f103 datasheet */
      huart3.Instance->CR3=0x00000000LU;

      /* initialize USART3.GTPR to the reset state given in the Stm32f103 datasheet */
      huart3.Instance->GTPR=0x00000000LU;
      
      /* disable the USART3 clock */
      __HAL_RCC_USART3_CLK_DISABLE(); 

      /* flush the USART3 fifo */
      flush(&ConsoleFifo);

      /* initialize the UART handle object */
      memset((void *)(&huart3),0,sizeof(UART_HandleTypeDef));
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable the CL interface                                   */
/*------------------------------------------------------------------------*/
int ConioDisable(void)
{
   /* initialize the return value */
   int status = ConioOk;

   /* validate the configuration for USART3 */
   if (!huart3.Instance) {status=ConioNoConfig;}

   else
   {
      /* disable the Console serial port */
      ConioConfig(0);
        
      /* disable CL_ACTIVE_EN to paralyze the WAKE signal low while in run mode */
      ConioPwrDisable();
    }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the CL interface                                    */
/*------------------------------------------------------------------------*/
int ConioEnable(void)
{
   /* initialize the return value */
   int status = ConioOk;

   /* configure the Console serial port */
   ConioConfig(9600);
   
   /* enable console output if a CL interface is connected */
   if (ConioActive()>0) {Apf11_3p3vEnable();} 
   
   /* enable CL_ACTIVE_EN to paralyze the WAKE signal low while in run mode */
   ConioPwrEnable();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the Uart1 interrupt         */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Uart1
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int ConioErrors(unsigned char *nbreak,unsigned char *parity,unsigned char *frame,
                unsigned char *ovrun,unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      ConioStatus.nbreak=0; ConioStatus.parity=0; ConioStatus.frame=0;
      ConioStatus.ovrun=0;  ConioStatus.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=ConioStatus.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=ConioStatus.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=ConioStatus.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=ConioStatus.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=ConioStatus.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* interrupt handler for input on the USART3 serial interface             */
/*------------------------------------------------------------------------*/
/*
   This is the interrupt handler for the USART3 serial interface.  The
   SwiftWare UART model is interrupt-driven and non-blocking with
   respect to received data.  For transmitted data, the SwiftWare UART
   model is non-blocking but not interrupt-driven as described in the
   comment section near the top of this module.  This function also
   manages error interrupts and maintains a record of UART errors.

   This function returns a positive value on success or else zero on
   failure.  A negative return value indicates that an exception was
   encountered. 
*/
int ConioIrqHandler(void)
{
   /* define the logging signature */
   cc *FuncName="ConioIrqHandler()";

   /* initialize the return value */
   int status=ConioOk;

   /* validate the configuration for USART3 */
   if (huart3.Instance!=USART3) {LogEntry(FuncName,"Invalid UART3 configuration.\n"); status=ConioNoConfig;}

   else
   {
      /* define maximum number of errors before error-interrupts are disabled */
      const unsigned char ErrMax=10;

      /* read the status register of USART3 */
      volatile unsigned long int SR = huart3.Instance->SR;

      /* read the data register of USART3 */
      volatile unsigned long int DR = huart3.Instance->DR;

      /* read control register 1 of USART3 */
      volatile unsigned long int CR1 = huart3.Instance->CR1; 

      /* read control register 2 of USART3 */
      volatile unsigned long int CR2 = huart3.Instance->CR2; 

      /* read control register 3 of USART3 */
      volatile unsigned long int CR3 = huart3.Instance->CR3; 

      /* temporarily disable receiver interrupts on USART3 */
      CLEAR_BIT(huart3.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

      /* temporarily disable transmitter interrupts on USART3 */
      CLEAR_BIT(huart3.Instance->CR1, USART_CR1_TXEIE | USART_CR1_TCIE);

      /* temporarily disable error interrupts on USART3 */
      CLEAR_BIT(huart3.Instance->CR3, USART_CR3_EIE);

      /* avoid compiler warnings */
      UNUSED(CR2);
   
      /* test for parity error */
      if (SR&USART_SR_PE) 
      {
         /* check if parity-error interrupt is enabled */
         if (CR1&USART_CR1_PEIE) huart3.ErrorCode |= HAL_UART_ERROR_PE;

         /* increment the parity-error counter */
         if ((ConioStatus.parity++) > ErrMax) CLEAR_BIT(CR1,USART_CR1_PEIE);
      }

      /* test for framing error */
      if (SR&USART_SR_FE)
      {
         /* check if framing-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart3.ErrorCode |= HAL_UART_ERROR_FE;
      
         /* increment the framing-error counter */
         if ((ConioStatus.frame++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for noise error */
      if (SR&USART_SR_NE)
      {
         /* check if noise-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart3.ErrorCode |= HAL_UART_ERROR_NE;

         /* increment the noise-error counter */
         if ((ConioStatus.noise++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for over-run error */
      if (SR&USART_SR_ORE)
      {
         /* check if over-runt error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart3.ErrorCode |= HAL_UART_ERROR_ORE;

         /* increment the over-run error counter */
         if ((ConioStatus.ovrun++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for byte received */
      if (SR&USART_SR_RXNE)
      {
         /* read the byte from the serial port */
         unsigned char byte = DR&0xff;

         /* buffered IO model is used - ignore the byte if a line exists already */
         if (!ConioStatus.ConsoleFifoLineReady)
         {
            switch(byte)
            {
               /* filter out the LFs from CR/LF pairs */
               case LF: {break;}

               /* backspace deletes the last byte fifo */
               case BKSP: {del(&ConsoleFifo); break;}
            
               /* DEL deletes the last byte from the fifo */            
               case DEL:  {del(&ConsoleFifo); break;}
            
               /* push the byte into the fifo */
               default:
               {
                  /* add the byte to the fifo */
                  push(&ConsoleFifo,byte);

                  /* assert the flag that implements the buffered IO model */
                  if (byte==CR) ConioStatus.ConsoleFifoLineReady=1;
            
                  break;
               } 
            } 
         }
      }
      
      /* test if Tx-empty bit is asserted */
      if (SR&USART_SR_TXE)
      {
         /* SwiftWare USART3 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TXEIE) CLEAR_BIT(CR1,USART_CR1_TXEIE);
      }

      /* test if Tx-complete bit is asserted */
      if (SR&USART_SR_TC)
      {
         /* SwiftWare USART3 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TCIE) CLEAR_BIT(CR1,USART_CR1_TCIE);
      }

      /* re-enable interrups on USART3 */
      huart3.Instance->CR1=CR1; huart3.Instance->CR3=CR3;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* disable the Apf11's current-loop power pin                             */
/*------------------------------------------------------------------------*/
/**
   This function disables the Apf11's current-loop power-enable pin of
   the Stm32.

      \begin{verbatim}
      output:
         This function returns a positive value if the CL_PWR_EN
         signal is successfully disabled.  Zero is returned on
         failure.  A negative value indicates an exception was
         encountered.
      \end{verbatim}
*/
int ConioPwrDisable(void)
{
   /* clear the CL_PWR_EN pin of the Stm32 */
   int status=Stm32GpioClear(GPIOD, CL_PWR_EN_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* enable the Apf11's main switching regulator (3P3V)                     */
/*------------------------------------------------------------------------*/
/**
   This function enables the Apf11's current-loop power-enable pin of
   the Stm32.

      \begin{verbatim}
      output:
         This function returns a positive value if the CL_PWR_EN
         signal is successfully enabled.  Zero is returned on
         failure.  A negative value indicates an exception was
         encountered.
      \end{verbatim}
*/
int ConioPwrEnable(void)
{
   /* assert the CL_PWR_EN pin of the Stm32 */
   int status=Stm32GpioAssert(GPIOD, CL_PWR_EN_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* query the state of the Apf11's main switching regulator (3P3V)         */
/*------------------------------------------------------------------------*/
/**
   This function queries the state of the Apf11's current-loop power-enable pin of
   the Stm32.

      \begin{verbatim}
      output:
         This function returns a positive value if the CL_PWR_EN
         signal is enabled or zero if the CL_PWR_EN signal is
         disabled.  A negative value indicates an exception was
         encountered.
      \end{verbatim}
*/
int ConioPwrQuery(void)
{
   /* read the state of the CL_PWR_EN pin of the Stm32 */
   int status=Stm32GpioIsAssert(GPIOD, CL_PWR_EN_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the Console FIFO queue                    */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the Console serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the Console FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int getb20ma(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "getb20ma()";

   /* initialize the return value */
   int status=ConioFail;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=ConioNull;}

   /* validate the configuration for USART3 */
   else if (huart3.Instance!=USART3) {LogEntry(FuncName,"Invalid UART3 configuration.\n"); status=ConioNoConfig;}
   
   /* line buffered - don't read any bytes until a full line is available */
   else if (ConioStatus.ConsoleFifoLineReady)
   {
      /* disable receive interrupts on UART3 to prevent the fifo from changing state */
      CLEAR_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);

      /* pop the next byte out of the fifo queue */
      status=pop(&ConsoleFifo,byte);
 
      /* enable receive interrupts on UART3 */
      SET_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);
         
      /* check buffer termination criteria */
      if (status<=0 || (*byte)==CR) {ConioStatus.ConsoleFifoLineReady=0;}

      /* transmit a LF if the incoming byte is a CR */
      if ((*byte)==CR) putb20ma(LF); 

      /* return a CR if the FIFO buffer is empty */
      if (status<=0) (*byte)=CR;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* degenerate function to satisfy programming contract with stdio library */
/*------------------------------------------------------------------------*/
/**
   This function is used to satisfy the programming contract with the stdio
   library for reading input from stdin.  However, this function always
   returns EOF. The standard C library relies on a blocking IO model that is
   incompatible with safeguards built into APF9 firmware.  Use the
   SerialPort interface instead.
*/
int getch(void)
{
   return EOF;
}

/*------------------------------------------------------------------------*/
/* function to flush the USART3's Rx FIFO queue                           */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the USART3 serial port.
   This function returns a positive value on success and zero on
   failure.  A negative return value indicates an exception was
   encountered.
*/
static int iflush20ma(void)
{
   /* define the logging signature */
   cc *FuncName = "iflush20ma()";

   /* initialize the return value */
   int status=ConioOk;
            
   /* validate the configuration for USART3 */
   if (huart3.Instance!=USART3) {LogEntry(FuncName,"Invalid UART3 configuration.\n"); status=ConioNoConfig;}

   else
   {
      /* disable receive interrupts on UART3 to prevent the fifo from changing state */
      CLEAR_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);

      /* flush the Com1 serial port's fifo buffer */
      status=flush(&ConsoleFifo); 
                
      /* enable receive interrupts on UART3 */
      SET_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* primitive used by printf() to write single character to 20ma interface */
/*------------------------------------------------------------------------*/
/**
   This function is used by the printf() family of functions to write output
   to the console serial port.  Aside from CR/LF translation, it's only
   action is to pass the byte to the serial port's low-level function:
   putb20ma().  
*/
int __io_putchar(int ch)
{
    /* translate LF to CR, LF */
   if(ch == LF) putb20ma(CR);

   return (putb20ma((unsigned char)(ch&0xff))>0) ? 1 : EOF;
}

/*------------------------------------------------------------------------*/
/* function to detect if the keyboard was hit                             */
/*------------------------------------------------------------------------*/
/**
   This function uses the console FIFO buffer to determine if keyboard input
   is available.

      \begin{verbatim}
         output:
            If input is available, this function pops and returns the next
            byte from the FIFO queue.  Otherwise the status of pop() is
            returned.
      \end{verbatim}
*/
int kbdhit(void)
{
   unsigned char byte=0;
   int status;

   /* disable receive interrupts on UART3 to prevent the fifo from changing state */
   CLEAR_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);

   /* pop the next byte out of the fifo queue */
   status=pop(&ConsoleFifo,&byte);

   /* check buffer termination criteria */
   if (status<=0 || byte==CR) ConioStatus.ConsoleFifoLineReady=0;
     
   /* enable receive interrupts on UART3 */
   SET_BIT(huart3.Instance->CR1, USART_CR1_RXNEIE);
 
   /* transmit a LF if the incoming byte is a CR */
   if (status>0 && byte==CR) putb20ma(LF);

   /* protect against interrupts being turned off too much */
   Wait(5);

   /* return byte if successful, otherwise return the status */
   return (status>0) ? byte : status;
}

/*------------------------------------------------------------------------*/
/* function to write a single byte to the serial port                     */
/*------------------------------------------------------------------------*/
/**
   This function writes the character c, cast to an unsigned char, to the
   console serial port and returns a positive value on success or EOF on
   error.
*/
int putb(unsigned char byte)
{
   /* translate LF to CR, LF */
   if(byte == LF) putb20ma(CR);

   return (putb20ma((unsigned char)byte)>0) ? 1 : EOF;
}
 
/*------------------------------------------------------------------------*/
/* function to write a single character to the serial port                */
/*------------------------------------------------------------------------*/
/**
   This function writes the character c, to the console serial port.
*/
void putch(char byte)        
{
   /* translate LF to CR, LF */
   if(byte == LF) putb20ma(CR);

   /* write the byte to the serial port */
   putb20ma(byte); 
}

/*------------------------------------------------------------------------*/
/* function to write a single byte to the 20ma serial interface           */
/*------------------------------------------------------------------------*/
/**
   This function writes a single byte to the 20ma current loop interface.

      \begin{verbatim}
      input:
         byte ... The byte to be written to the 20ma serial interface.

      output:
         This function always returns a positive number.
      \end{verbatim}
*/
int putb20ma(unsigned char byte)
{
   /* initialize the return value */
   int status = ConioFail;

   /* validate the configuration for USART3 */
   if (!huart3.Instance) {status=ConioNoConfig;}

   /* transmit the byte only if the 20ma serial loop is active */
   else if (Stm32GpioIsAssert(GPIOB,UART3_RXD_Pin)>0)
   {
      /* define the timeout period */
      const unsigned long int Timeout=50;
   
      /* initialize the reference time for the time-out feature */
      unsigned long int To=HAL_GetTick();

      /* read control register 1 of USART3 */
      volatile unsigned long int CR1 = huart3.Instance->CR1; 

      /* read control register 3 of USART3 */
      volatile unsigned long int CR3 = huart3.Instance->CR3; 

      /* temporarily disable receiver interrupts on USART3 */
      CLEAR_BIT(huart3.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

      /* temporarily disable error interrupts on USART3 */
      CLEAR_BIT(huart3.Instance->CR3, USART_CR3_EIE);

      /* construct time-out loop */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart3.Instance->SR;
         
         /* check if the transmit register is empty */
         if (SR&USART_SR_TXE) {huart3.Instance->DR=byte; break;}
      }
      
      /* loop to wait until the transfer-complete flag is set */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart3.Instance->SR;

         /* check if the transmit-complete flag is set */
         if (SR&USART_SR_TC) {break;}
      }
      
      /* loop to catch and discard the current-loop echo */
      for (status=ConioFail; (HAL_GetTick()-To)<Timeout;)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart3.Instance->SR;

         /* check if the receive-buffer-not-empty flag is set */
         if (SR&USART_SR_RXNE && byte==(huart3.Instance->DR&0xff)) {status=ConioOk; break;}
      }

      /* clear the status register by reading USART3.SR followed by USART3.DR */
      {volatile unsigned long int sr=huart3.Instance->SR; UNUSED(sr);}
      {volatile unsigned long int dr=huart3.Instance->DR; UNUSED(dr);}

      /* re-enable interrups on USART3 */
      huart3.Instance->CR1=CR1; huart3.Instance->CR3=CR3;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to retrieve the static UART_HandleTypeDef pointer              */
/*------------------------------------------------------------------------*/
UART_HandleTypeDef *HAL_UART3_HandleGet(void) {return (&huart3);}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na(void)
{
   return ConioNull;
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na_(int state)
{
   return ConioNull;
}

#endif /* CONIO_C */

