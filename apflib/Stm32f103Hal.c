#ifndef STM32F103HAL_H
#define STM32F103HAL_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/**
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103HalChangeLog "$RCSfile$  $Revision$  $Date$"

#endif /* STM32F103HAL_H */
#ifdef STM32F103HAL_C
#undef STM32F103HAL_C

#include <logger.h>
#include <stm32f1xx_hal.h>

/* prototyptes for functions with external linkage */
__weak void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init);
__weak void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
__weak void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init)
{
   /* define the logging signature */
   cc *FuncName = "HAL_GPIO_Init()";

   LogEntry(FuncName,"HAL GPIO model is deprecated; "
            "use Swiftware GPIO model.\n");
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
__weak void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
   /* define the logging signature */
   cc *FuncName = "HAL_GPIO_WritePin()";

   LogEntry(FuncName,"HAL GPIO model is deprecated; "
            "use Swiftware GPIO model.\n");
}

/*------------------------------------------------------------------------*/
/* function to manage error call-backs for all UARTS                      */
/*------------------------------------------------------------------------*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
   /* define the logging signature */
   cc *FuncName = "HAL_UART_ErrorCallback()";

   /* catch debugging port errors */
   if (huart->Instance==USART1)
   {
      LogEntry(FuncName,"HAL UART model is buggy & deprecated; "
               "use Swiftware UART model.\n");
   }
   
   /* catch CTD port errors */
   else if (huart->Instance==USART2)
   {
      LogEntry(FuncName,"HAL UART model is buggy & deprecated; "
               "use Swiftware UART model.\n");
   }

   /* catch CL interface errors */
   else if (huart->Instance==USART3)
   {
      LogEntry(FuncName,"UART3 error: 0x%08lx\n",huart->ErrorCode);
   }

   /* catch invalid UARTs */
   else {LogEntry(FuncName,"Invalid UART: 0x%08lx\n",huart->Instance);}
}

/*------------------------------------------------------------------------*/
/* function to manage interrupt call-backs for all UARTS                  */
/*------------------------------------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
   /* define the logging signature */
   cc *FuncName = "HAL_UART_RxCpltCallback()";

   /* catch the debugging port interrupt */
   if (huart->Instance==USART1)
   {
      LogEntry(FuncName,"HAL UART model is buggy & deprecated; "
               "use Swiftware UART model.\n");
   }

   /* catch the SBE interrupt */
   else if (huart->Instance==USART2)
   {
      LogEntry(FuncName,"HAL UART model is buggy & deprecated; "
               "use Swiftware UART model.\n");
   }

   /* catch the CL interrupt */
   else if (huart->Instance==USART3)
   {
      LogEntry(FuncName,"HAL UART model is buggy & deprecated; "
               "use Swiftware UART model.\n");
   }

   /* catch invalid UARTs */
   else {LogEntry(FuncName,"Invalid UART: 0x08lx\n",huart->Instance);}
}

#endif /* STM32F103HAL_C */
