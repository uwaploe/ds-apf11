#ifndef TSM32F103TIMER_H
#define STM32F103TIMER_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103TimerChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 timer registers                                   */
/*------------------------------------------------------------------------*/
/**
   volatile unsigned long int CR1;   // TIM control register 1,                    
   volatile unsigned long int CR2;   // TIM control register 2,                    
   volatile unsigned long int SMCR;  // TIM slave Mode Control register,           
   volatile unsigned long int DIER;  // TIM DMA/interrupt enable register,         
   volatile unsigned long int SR;    // TIM status register,                       
   volatile unsigned long int EGR;   // TIM event generation register,             
   volatile unsigned long int CCMR1; // TIM capture/compare mode register 1,      
   volatile unsigned long int CCMR2; // TIM capture/compare mode register 2,      
   volatile unsigned long int CCER;  // TIM capture/compare enable register,       
   volatile unsigned long int CNT;   // TIM counter register,                      
   volatile unsigned long int PSC;   // TIM prescaler register,                    
   volatile unsigned long int ARR;   // TIM auto-reload register,                  
   volatile unsigned long int RCR;   // TIM repetition counter register,          
   volatile unsigned long int CCR1;  // TIM capture/compare register 1,            
   volatile unsigned long int CCR2;  // TIM capture/compare register 2,            
   volatile unsigned long int CCR3;  // TIM capture/compare register 3,            
   volatile unsigned long int CCR4;  // TIM capture/compare register 4,            
   volatile unsigned long int BDTR;  // TIM break and dead-time register,          
   volatile unsigned long int DCR;   // TIM DMA control register,                  
   volatile unsigned long int DMAR;  // TIM DMA address for full transfer register,
   volatile unsigned long int OR;    // TIM option register,                       
*/
typedef TIM_TypeDef Stm32Timer;

/* declare functions with external linkage */
int Stm32TimerInit(Stm32Timer *timer);
int uTimerConfig(void);
int uWait(short unsigned int usec);

/* define the return states of the Stm32 TIMER API */
extern const char TimerInvalid;   /* invalid argument */
extern const char TimerNull;      /* NULL function argument */
extern const char TimerFail;      /* general failure */
extern const char TimerOk;        /* general success */

#endif /* STM32F103TIMER_H */
#ifdef STM32F103TIMER_C
#undef STM32F103TIMER_C

#include <limits.h>
#include <logger.h>
#include <Stm32f103Rcc.h>
#include <stm32f1xx_hal.h>
#include <stm32f103xg.h>

/* define the return states of the Stm32 timer API */
const char TimerInvalid     = -2; /* invalid argument */
const char TimerNull        = -1; /* NULL function argument */
const char TimerFail        =  0; /* general failure */
const char TimerOk          =  1; /* general success */

/* record the APB1 frequency used to drive Timer {2-6,12-14} */
const static unsigned long int Apb1Hertz=8000000UL;

/*------------------------------------------------------------------------*/
/* configure TIM5 for use as a delay counter                              */
/*------------------------------------------------------------------------*/
/**
   This function configures TIM5 for use as a delay counter. This
   function returns a positive value on success or else zero value. 
*/
int uTimerConfig(void)
{
   /* initialize the return value */
   int status=TimerOk;

   /* enumerate configuration bitmasks */
   enum {CEN=0x1<<0, URS=0x1<<2, ARPE=0x1<<7};

   /* initialize the Stm32's TIM5 timer */
   if (Stm32TimerInit(TIM5)>0)
   {
      /* enable the TIM5 clock */
      __HAL_RCC_TIM5_CLK_ENABLE();

      /* configure TIM5 for counting */
      TIM5->CR1=(CEN | URS | ARPE); TIM5->ARR=0xfff0U;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* pause for a specified number of microseconds                           */
/*------------------------------------------------------------------------*/
/**
   This function pauses for a specified number of microseconds.  This
   function returns the number of APB1 cycles executed during the pause.
*/
int uWait(short unsigned int usec)
{
   /* initialize the return value */
   int n=TimerFail;

   /* compute number of APB1 cycles in the specified number of microseconds */
   long int winks=usec*Apb1Hertz/1000000UL-25;

   /* condition the number of APB1 cycles to be in the range [1,0xff00] */
   if (winks<1) {winks=1;} else if (winks>0xff00U) {winks=0xff00U;}

   /* delay until the number of cycles has been executed */
   for (TIM5->CNT=0, n=0; TIM5->CNT<winks && n<winks; n++) {}

   /* record the number of cycles executed durig the delay */
   n=TIM5->CNT; 
   
   return n;
}

/*------------------------------------------------------------------------*/
/* function to initialize an Stm32 timer                                  */
/*------------------------------------------------------------------------*/
/**
   This function initializes an Stm32 timer so that all of its
   registers contain their reset values.

   \begin{verbatim}
   input:
      timer....A pointer to one of the timers of the Stm32f103.

   output:
      This function returns a positive value if successful or else
      zero if the initialization failed from some reason.  A negative
      return value indicates an exception was encountered.
   \end{verbatim}
*/
int Stm32TimerInit(Stm32Timer *timer)
{
   /* initialize the return value */
   int n,status=TimerOk;

   /* compute the number of registers in the Stm32Timer structure */
   const int N=sizeof(Stm32Timer)/sizeof(volatile unsigned long int);

   /* initialize a pointer to the timer registers */
   volatile unsigned long int *reg = &(timer->CR1);

   /* initialize the register to zero */
   for (n=0; n<N; n++) {reg[n]=0;}
   
   return status;
}

#endif /* STM32F103TIMER_C */
