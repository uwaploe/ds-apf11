#ifndef STM32F103PWR_H
#define STM32F103PWR_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103PwrChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>

/* declare functions with external linkage */
unsigned long int Stm32AlarmTicsAtWake(void);
void              Stm32AlarmTicsAtWakeSet(unsigned long int AlarmTics);
void              Stm32ClearStandbyFlag(void);
void              Stm32ClearWakeFlag(void);
int               Stm32ResetFromStandby(void);
int               Stm32ResumeFromStandby(void);
unsigned long int Stm32RtcTicsAtWake(void);
void              Stm32RtcTicsAtWakeSet(unsigned long int RtcTics);
void              Stm32Standby(void);
int               Stm32WakeFromStandby(void);

#endif /* STM32F103PWR_H */
#ifdef STM32F103PWR_C
#undef STM32F103PWR_C

#include <limits.h>
#include <Stm32f103Rtc.h>
#include <stm32f1xx_hal.h>

/* define objects to contain the RTC and alarm times for wake cycle after standby */
static unsigned long int WakeRtcTics=ULONG_MAX, WakeAlarmTics=ULONG_MAX;

/*------------------------------------------------------------------------*/
/* accessor function for alarm-time at a wake cycle                       */
/*------------------------------------------------------------------------*/
unsigned long int Stm32AlarmTicsAtWake(void) {return WakeAlarmTics;}

/*------------------------------------------------------------------------*/
/* mutator function to record alarm-time at a wake cycle                  */
/*------------------------------------------------------------------------*/
void Stm32AlarmTicsAtWakeSet(unsigned long int AlarmTics) {WakeAlarmTics=AlarmTics;}

/*------------------------------------------------------------------------*/
/* clear the standby flag in the PWR.CSR register of the Stm32            */
/*------------------------------------------------------------------------*/
/**
   This function clears the standby flag in the PWR.CSR register of
   the Stm32.  An asserted standby flag indicates that the STM32 had
   been in standby mode prior to resuming run mode.
*/
void Stm32ClearStandbyFlag(void)
{
   /* check and handle if the system was resumed from standby mode */ 
   if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET) {__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);}
}

/*------------------------------------------------------------------------*/
/* clear the wake-up flag in the PWR.CSR register of the Stm32            */
/*------------------------------------------------------------------------*/
/**
   This function clears the wake-up flag in the PWR.CSR register of
   the Stm32.  An asserted wake flag indicates that a wake signal was
   received by the Stm32 while not in run mode.  A wake signal is not
   registered and has no effect while the Stm32 is in run mode.
*/
void Stm32ClearWakeFlag(void)
{
   /* clear the wake flag if asserted */ 
   if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU) != RESET) {__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);}
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if reset caused exit from standby mode */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function that determines if a reset signal
   caused the Stm32 to exit standby mode and enter run mode.  Stm32
   hardware causes the standby flag (ie., PWR.CSR:SBF) to be asserted
   whenever standby mode transitions to run mode.  The transition can
   be caused by either a wake signal or a reset signal.  If a wake
   signal caused the transition then the PWR.CSR:WUF is asserted while
   if a reset signal causes the Stm32's PWR.CSR register to be
   initialized to zeros (except that the PWR.CSR:SBF flag is
   asserted). So to determine if a reset caused the transition from
   standby mode to run mode then this predicate function tests both
   PWR.CSR:SBF and PWR.CSR:WUF.

   Note: Applying power to an unpowered Stm32 induces the Stm32 into
   run mode.  However, neither the PWR.CSR:SBF nor the PWR.CSR:WUF
   flags are asserted under such conditions.

      \begin{verbatim}
      output:
         This function returns a positive value if a reset signal
         caused the Stm32 to transition from standby mode to run mode.
         Zero is returned if the Stm32 was not in standby mode prior
         to run mode or if the wake signal caused the transition.  A
         negative return value indicates an exception was encountered.
      \end{veratim}
*/
int Stm32ResetFromStandby(void)
{
   int status=1;
   
   /* check if the standby bit is asserted in the PWR.CSR register */
   if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) == RESET) {status=0;}

   /* check if the wake-up bit is asserted in the PWR.CSR register */ 
   else if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU) != RESET) {status=0;}

   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if standby mode preceeded run mode     */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function that determines if the Stm32 was in
   standby mode prior to resuming run mode.  Stm32 hardware causes the
   standby flag (ie., PWR.CSR:SBF) to be asserted whenever standby
   mode transitions to run mode.  This function tests if that flag is
   asserted.

      \begin{verbatim}
      output:
         This function returns a positive value if standby mode
         transitioned to run mode (as indicated by PWR.CSR:SBF).  Zero
         is returned if PWR.CSR:SBF is not asserted.  A negative
         return value indicates an exception was encountered.
      \end{veratim}
*/
int Stm32ResumeFromStandby(void)
{
   /* check if the standby-bit is asserted in the PWR.CSR register */
   int status = (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET) ? 1 : 0;

   return status;
}

/*------------------------------------------------------------------------*/
/* accessor function for RTC time at a wake cycle                         */
/*------------------------------------------------------------------------*/
unsigned long int Stm32RtcTicsAtWake(void) {return WakeRtcTics;}

/*------------------------------------------------------------------------*/
/* mutator function to record RTC time at a wake cycle                    */
/*------------------------------------------------------------------------*/
void Stm32RtcTicsAtWakeSet(unsigned long int RtcTics) {WakeRtcTics = RtcTics;}

/*------------------------------------------------------------------------*/
/* induce the Stm32 into standby mode (ie., lowest possible power)        */
/*------------------------------------------------------------------------*/
/**
   This function induces the apf11 into standby mode which is the
   state of minimum power consumption for the STM32F103 processor.
   One of the STM3210E evaluation examples provided the following
   advice:

      The Following Wakeup sequence is highly recommended prior to
      each Standby mode entry mainly when using more than one wakeup
      source this is to not miss any wakeup event.
         + Disable all used wakeup sources,
         + Clear all related wakeup flags, 
         + Re-enable all used wakeup sources,
         + Enter the Standby mode.

      \begin{verbatim}
      output:
         On success, this function returns a positive integer; zero
         indicates failure.  A negative value is returned if an
         exception is encountered.
      \end{verbatim}
*/
void Stm32Standby(void)
{
   /* save the current RTC alarm in the RTC backup registers */
   Stm32AlarmSave();
   
   /* check and handle if the system was resumed from standby mode */ 
   if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) != RESET) {__HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);}
 
   /* disable all used wakeup sources: PWR_WAKEUP_PIN1 */
   HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);

   /* clear all related wakeup flags*/
   __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
  
   /* enable WakeUp Pin PWR_WAKEUP_PIN1 connected to PA.00 */
   HAL_PWR_EnableWakeUpPin(PWR_WAKEUP_PIN1);

   /* enter the standby mode (execution never returns from this function call) */
   HAL_PWR_EnterSTANDBYMode();
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if wake signal caused exit from standby*/
/*------------------------------------------------------------------------*/
/**
   This is a predicate function that determines if a wake signal
   caused the Stm32 to exit standby mode and enter run mode.  Stm32
   hardware causes the standby flag (ie., PWR.CSR:SBF) to be asserted
   whenever standby mode transitions to run mode.  The transition can
   be caused by either a wake signal or a reset signal.  If a wake
   signal caused the transition then the PWR.CSR:WUF is asserted while
   if a reset signal causes the Stm32's PWR.CSR register to be
   initialized to zeros (except that the PWR.CSR:SBF flag is
   asserted). So to determine if a reset caused the transition from
   standby mode to run mode then this predicate function tests both
   PWR.CSR:SBF and PWR.CSR:WUF.

   Note: Applying power to an unpowered Stm32 induces the Stm32 into
   run mode.  However, neither the PWR.CSR:SBF nor the PWR.CSR:WUF
   flags are asserted under such conditions.

      \begin{verbatim}
      output:
         This function returns a positive value if a wake signal
         caused the Stm32 to transition from standby mode to run mode.
         Zero is returned if the Stm32 was not in standby mode prior
         to run mode or if the wake signal did not cause the
         transition.  A negative return value indicates an exception
         was encountered.
      \end{veratim}
*/
int Stm32WakeFromStandby(void)
{
   int status=1;
   
   /* check if the standby bit is asserted in the PWR.CSR register */
   if (__HAL_PWR_GET_FLAG(PWR_FLAG_SB) == RESET) {status=0;}

   /* check if the wake-up bit is asserted in the PWR.CSR register */ 
   else if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU) == RESET) {status=0;}

   return status;
}

#endif /* STM32F103PWR_C */
