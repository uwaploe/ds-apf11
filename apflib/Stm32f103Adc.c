#ifndef STM32F103ADC_H
#define STM32F103ADC_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103AdcChangeLog "$RCSfile$  $Revision$   $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 analog-to-digital (ADC) registers                 */
/*------------------------------------------------------------------------*/
/**
   The Stm32Adc is defined by the following 32-bit registers in the Stm32:
      volatile unsigned long int SR;    // status register                   
      volatile unsigned long int CR1;   // control register 1                
      volatile unsigned long int CR2;   // control register 2                
      volatile unsigned long int SMPR1; // sample time register 1            
      volatile unsigned long int SMPR2; // sample time register 2            
      volatile unsigned long int JOFR1; // data offset for injected channels 
      volatile unsigned long int JOFR2; // data offset for injected channels 
      volatile unsigned long int JOFR3; // data offset for injected channels 
      volatile unsigned long int JOFR4; // data offset for injected channels 
      volatile unsigned long int HTR;   // watchdog high threshold           
      volatile unsigned long int LTR;   // watchdog low threshold            
      volatile unsigned long int SQR1;  // sequence register 1               
      volatile unsigned long int SQR2;  // sequence register 2               
      volatile unsigned long int SQR3;  // sequence register 3               
      volatile unsigned long int JSQR;  // injected sequence register        
      volatile unsigned long int JDR1;  // injected data register            
      volatile unsigned long int JDR2;  // injected data register            
      volatile unsigned long int JDR3;  // injected data register            
      volatile unsigned long int JDR4;  // injected data register            
      volatile unsigned long int DR;    // regular data register 
*/
typedef ADC_TypeDef Stm32Adc;

/* declare functions with external linkage */
int Stm32AdcDisable(Stm32Adc *adc);
int Stm32AdcEnable(Stm32Adc *adc);
int Stm32AdcSingleConversion(Stm32Adc *adc, unsigned char channel, unsigned int *value);

/* define the return states of the Stm32 ADC API */
extern const char AdcCalFail;   /* calibration failure */
extern const char AdcInvalid;   /* invalid argument */
extern const char AdcNull;      /* NULL function argument */
extern const char AdcFail;      /* general failure */
extern const char AdcOk;        /* general success */

#endif /* STM32F103ADC_H */
#ifdef STM32F103ADC_C
#undef STM32F103ADC_C

#include <limits.h>
#include <logger.h>
#include <stm32f1xx_hal.h>

/* define the return states of the Stm32 ADC API */
const char AdcCalFail     = -3; /* calibration failure */
const char AdcInvalid     = -2; /* invalid argument */
const char AdcNull        = -1; /* NULL function argument */
const char AdcFail        =  0; /* general failure */
const char AdcOk          =  1; /* general success */

/* declare external functions for local use */
int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* union for schizo-model the Stm32's ADC configuration registers         */
/*------------------------------------------------------------------------*/
typedef union
{
   /* define a structure to model the Stm32's ADC configuration registers */
   struct
   {
      unsigned long int SR;      /* status register                   */                 
      unsigned long int CR[2];   /* control registers                 */
      unsigned long int SMPR[2]; /* sample time registers             */
      unsigned long int JOFR[4]; /* data offset for injected channels */
      unsigned long int HTR;     /* watchdog high threshold           */
      unsigned long int LTR;     /* watchdog low threshold            */
      unsigned long int SQR[3];  /* sequence registers                */
      unsigned long int JSQR;    /* injected sequence register        */
   };

   /* overlay an array of 32-bit integers */
   unsigned long int regs[15];
} AdcCfgRegs;

/*------------------------------------------------------------------------*/
/* initializers for the Stm32f103's ADCs                                  */
/*------------------------------------------------------------------------*/
static const AdcCfgRegs AdcInit[3] =
{
   /*     ADC1:SR       ADC1:CR1      ADC1:CR2      ADC1:SMPR1     ADC1:SMPR2 */
   {{0x00000000UL, {0x00000000UL, 0x00000000UL}, {0x00b7ffffUL,  0x3fffffffUL},
   /*  ADC1:JOFR1     ADC1:JOFR2    ADC1:JOFR3      ADC1:JOFR4       ADC1:HTR */
    {0x00000000UL,  0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000fffUL, 
   /*    ADC1:LTR      ADC1:SQR1     ADC1:SQR2       ADC1:SQR3      ADC1:JSQR */
     0x00000000UL, {0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000000UL}},
   
   /*     ADC2:SR       ADC2:CR1      ADC2:CR2      ADC2:SMPR1     ADC2:SMPR2 */
   {{0x00000000UL, {0x00000000UL, 0x00000000UL}, {0x0003ffffUL,  0x3fffffffUL},
   /*  ADC2:JOFR1     ADC2:JOFR2    ADC2:JOFR3      ADC2:JOFR4       ADC2:HTR */
    {0x00000000UL,  0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000fffUL, 
   /*    ADC2:LTR      ADC2:SQR1     ADC2:SQR2       ADC2:SQR3      ADC2:JSQR */
     0x00000000UL, {0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000000UL}},
   
   /*     ADC3:SR       ADC3:CR1      ADC3:CR2      ADC3:SMPR1     ADC3:SMPR2 */
   {{0x00000000UL, {0x00000000UL, 0x00000000UL}, {0x0003ffffUL,  0x3fffffffUL},
   /*  ADC3:JOFR1     ADC3:JOFR2    ADC3:JOFR3      ADC3:JOFR4       ADC3:HTR */
    {0x00000000UL,  0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000fffUL, 
   /*    ADC3:LTR      ADC3:SQR1     ADC3:SQR2       ADC3:SQR3      ADC3:JSQR */
     0x00000000UL, {0x00000000UL, 0x00000000UL,   0x00000000UL}, 0x00000000UL}},
};

/* declare functions with static linkage */
static int Stm32AdcCalibrate(Stm32Adc *adc);

/* declare external functions that are used locally */
unsigned long int HAL_GetTick(void);

/*------------------------------------------------------------------------*/
/* function to calibrate an ADC                                           */
/*------------------------------------------------------------------------*/
/**
   This function calibrates an ADC of the Stm32.

   \begin{verbatim}
   input:
      adc....A pointer to one of the three ADCs of the Stm32f103.

   output:
      This function returns a positive value if successful or else
      zero if the initialization failed from some reason.  A negative
      return value indicates an exception was encountered.
   \end{verbatim}
*/
static int Stm32AdcCalibrate(Stm32Adc *adc)
{
   /* define the logging signature */
   cc *FuncName="Stm32AdcCalibrate()";
  
   /* initialize the return value */
   int status=AdcOk;

   /* define a one-time switch for calibration */
   static unsigned char CalDone[4]={0,0,0,0};

   /* define some iteration parameters */
   unsigned char n; unsigned long int To;

   /* select the ADC index */
   if (adc==ADC1) {n=1;} else if (adc==ADC2) {n=2;} else if (adc==ADC3) {n=3;} else {n=0;}

   /* validate the ADC */
   if (!n) {LogEntry(FuncName,"Invalid ADC: 0x%08x\n",adc); status=AdcInvalid;}

   /* test the one-time switch to learn if calibration of current ADC is current */
   else if (!CalDone[n])
   {
      /* define iteration parameters */
      enum {CAL=0x04, RSTCAL=0x08};

      /* reset the calibration registers */
      for ((adc->CR2|=RSTCAL), To=HAL_GetTick(); ((adc->CR2)&RSTCAL) && ((HAL_GetTick()-To)<2);) {}
      
      /* initiate a calibration of the ADC */ 
      for ((adc->CR2|=CAL), To=HAL_GetTick(); ((adc->CR2)&CAL) && ((HAL_GetTick()-To)<2);) {}
      
      /* test if calibration terminated */
      if ((adc->CR2)&CAL) {LogEntry(FuncName,"Calibration failed for ADC%d: "
                                    "0x%08x\n",n,adc); status=AdcCalFail;}

      else
      {
         /* assert the one-time switch for calibration of current ADC */
         CalDone[n]=1; 

         /* test the criteria for logentry */
         if (debuglevel>=4 || (debugbits&STM32F103ADC_H))
         {
            /* log the successful calibration */
            LogEntry(FuncName,"ADC%d successfully calibrated. [0x%08lx]\n",n,adc); 
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize one of the Stm32's ADC registers                */
/*------------------------------------------------------------------------*/
/**
   This function initializes one of the three Stm32f103's ADCs by
   loading its registers with initial values and disabling its clock.

   \begin{verbatim}
   input:
      adc....A pointer to one of the three ADCs of the Stm32f103.

   output:
      This function returns a positive value if successful or else
      zero if the initialization failed from some reason.  A
      negative return value indicates an exception was
      encountered. 
   \end{verbatim}
*/
int Stm32AdcDisable(Stm32Adc *adc)
{
   /* define the logging signature */
   cc *FuncName="Stm32AdcDisable()";
  
   /* initialize the return value */
   int n,status=AdcOk;

   /* define a pointer to reference ADC registers as an array */
   volatile unsigned long int *regs;

   /* compute the number of registers in the AdcCfgRegs object */
   const int N=sizeof(AdcCfgRegs)/sizeof(unsigned long int);

   /* select Stm32:ADC1 */
   if (adc==ADC1)
   {
      /* disable the ADC clock */
      __HAL_RCC_ADC1_CLK_DISABLE();

      /* load the ADC's register initializations */
      for (regs=&(ADC1->SR), n=0; n<N; n++) {regs[n]=AdcInit[0].regs[n];}
   }
      
   /* select Stm32:ADC2 */
   else if (adc==ADC2)
   {
      /* disable the ADC clock */
      __HAL_RCC_ADC2_CLK_DISABLE();
         
      /* load the ADC's register initializations */
      for (regs=&(ADC2->SR), n=0; n<N; n++) {regs[n]=AdcInit[1].regs[n];}
   }
      
   /* select Stm32:ADC3 */
   else if (adc==ADC3)
   {
      /* disable the ADC clock */
      __HAL_RCC_ADC3_CLK_DISABLE();
         
      /* load the ADC's register initializations */
      for (regs=&(ADC3->SR), n=0; n<N; n++) {regs[n]=AdcInit[2].regs[n];}
   }

   /* catch invalid pointers to Stm32 ADCs */
   else {LogEntry(FuncName,"Invalid Stm32 ADC: 0x%08x\n",adc); status=AdcInvalid;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize one of the Stm32's ADC registers                */
/*------------------------------------------------------------------------*/
/**
   This function initializes one of the three Stm32f103's ADCs by
   loading its registers with initial values and enabling its clock.

   \begin{verbatim}
   input:
      adc....A pointer to one of the three ADCs of the Stm32f103.

   output:
      This function returns a positive value if successful or else
      zero if the initialization failed from some reason.  A
      negative return value indicates an exception was
      encountered. 
   \end{verbatim}
*/
int Stm32AdcEnable(Stm32Adc *adc)
{
   /* define the logging signature */
   cc *FuncName="Stm32AdcEnable()";
  
   /* initialize the return value */
   int n,status=AdcOk;

   /* define a pointer to reference ADC registers as an array */
   volatile unsigned long int *regs;

   /* compute the number of registers in the AdcCfgRegs object */
   const int N=sizeof(AdcCfgRegs)/sizeof(unsigned long int);

   /* select Stm32:ADC1 */
   if (adc==ADC1)
   {
      /* enable the ADC clock */
      __HAL_RCC_ADC1_CLK_ENABLE();

      /* load the ADC's register initializations */
      for (regs=&(ADC1->SR), n=0; n<N; n++) {regs[n]=AdcInit[0].regs[n];}
   }
      
   /* select Stm32:ADC2 */
   else if (adc==ADC2)
   {
      /* enable the ADC clock */
      __HAL_RCC_ADC2_CLK_ENABLE();
         
      /* load the ADC's register initializations */
      for (regs=&(ADC2->SR), n=0; n<N; n++) {regs[n]=AdcInit[1].regs[n];}
   }
      
   /* select Stm32:ADC3 */
   else if (adc==ADC3)
   {
      /* enable the ADC clock */
      __HAL_RCC_ADC3_CLK_ENABLE();
         
      /* load the ADC's register initializations */
      for (regs=&(ADC3->SR), n=0; n<N; n++) {regs[n]=AdcInit[2].regs[n];}
   }

   /* catch invalid pointers to Stm32 ADCs */
   else {LogEntry(FuncName,"Invalid Stm32 ADC: 0x%08x\n",adc); status=AdcInvalid;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute one ADC conversion                                 */
/*------------------------------------------------------------------------*/
/**
   This function executes one ADC conversion of a specified ADC channel.  

   \begin{verbatim}
   input:
      adc.......A pointer to one of the three ADCs of the Stm32f103.
      channel...A specified channel for the specified ADC.

   output:
      value.....The measured AD conversion in the range: [0,4095].
                The sentinel value 0xffe is reserved to mean that the
                conversion was out-of-range on the high side.  The
                sentinel value 0xfff is reserved for missing data or
                NaN-like references.

      This function returns a positive value if successful or else
      zero if the initialization failed from some reason.  A negative
      return value indicates an exception was encountered.
   \end{verbatim}
*/
int Stm32AdcSingleConversion(Stm32Adc *adc, unsigned char channel, unsigned int *value)
{
   /* define the logging signature */
   cc *FuncName="Stm32AdcSingleConversion()";
  
   /* initialize the return value */
   int status=AdcNull;

   /* define some iteration parameters */
   int err;  unsigned long int To;

   /* initialize the function argument */
   if (value) {(*value)=0xfff;}

   /* validate the function argument */
   if (!value)  {LogEntry(FuncName,"NULL function argument.\n");}
   
   /* validate the channel */
   else if (channel>15) {LogEntry(FuncName,"Invalid ADC channel: %d\n",channel);}
   
   /* enable the ADC */
   else if ((err=Stm32AdcEnable(adc))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to enable ADC(0x%08x) "
               "failed. [err=%d]",adc,err); status=AdcFail;
   }

   else
   {
      /* create local work objects */
      unsigned short int n,dr,ad12; const unsigned short int N=5;
      
      /* define iteration parameters */
      enum {ADON=0x01, EOC=0x02, CLK239=0x07};
      
      /* configure a single conversion on the specified channel and enable the ADC */
      adc->CR2=ADON; adc->SQR1=0; adc->SQR3=channel;

      /* calibrate the ADC */
      if ((status=Stm32AdcCalibrate(adc))>0)
      {
         /* execute an averaging loop */
         for (ad12=0, n=0; n<N; n++)
         {
            /* initiate the AD conversion */
            for (((adc->CR2)|=ADON), To=HAL_GetTick(); !((adc->SR)&EOC) && ((HAL_GetTick()-To)<2);) {}

            /* test for successful conversion and read the ADC data register */
            if ((adc->SR)&EOC) {dr=adc->DR; ad12+=dr; if (dr>=0xffe) {(*value)=0xffe; break;}}
            
            /* log the conversion error */
            else {LogEntry(FuncName,"Conversion failed for channel(%d) of "
                           "ADC: 0x%08x\n",channel,adc); status=AdcFail; break;}

            /* pause between samples */
            if (n<(N-1)) {Wait(5);}
         }

         /* compute the average */
         if (status>0 && (*value)!=0xffe) {(*value)=(ad12/N);}
      }
   }

   /* disable the ADC */
   Stm32AdcDisable(adc);
   
   return status;
}

#endif /* STM32F103ADC_C */
