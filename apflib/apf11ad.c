#ifndef APF11AD_H
#define APF11AD_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define apf11adChangeLog "$RCSfile$  $Revision$  $Date$"

#include <limits.h>
#include <time.h> // for time_t use

/* declare functions with external linkage */
float          AirBladderPressure(unsigned short ad12);
unsigned short AirBladderPressureAdc(void);
float          AirPumpAmps(unsigned short ad12);
unsigned short AirPumpAmpsAdc(void);
float          Barometer(unsigned short ad12);
unsigned short BarometerAdc(void);
float          BatAmps(unsigned short ad16);
float          BatCharge(unsigned short ad16);
float          BatVolts(unsigned short ad12);
unsigned short BatVoltsAdc(void);
float          Com1Amps(unsigned short ad12);
unsigned short Com1AmpsAdc(void);
float          Com2Amps(unsigned short ad12);
unsigned short Com2AmpsAdc(void);
float          CtdAmps(unsigned short ad12);
unsigned short CtdAmpsAdc(void);
float          Humidity(unsigned short ad12);
unsigned short HumidityAdc(void);
float          HydraulicPumpAmps(unsigned short ad12);
unsigned short HydraulicPumpAmpsAdc(void);
float          LeakDetector(unsigned short ad12);
unsigned short LeakAdc(void);
float          LeakVolts(unsigned short ad12);
unsigned short PistonPositionAdc(void);
float          PistonVolume(unsigned short ad12);
float          RfAmps(unsigned short ad12);
unsigned short RfAmpsAdc(void);

/* define sentinel value to induce ADC meaurements in *Ad8() functions */
enum {ADC=0x7ffe};

#endif /* APF11AD_H */
#ifdef APF11AD_C
#undef APF11AD_C

#include <apf11.h>
#include <ds2740.h>
#include <logger.h>
#include <max7301.h>
#include <nan.h>
#include <Stm32f103Adc.h>
#include <Stm32f103Gpio.h>

#define NAN 0xfff
#define OOR 0xffe

/* define the reference voltage used by the Apf11 */
static const float Vref=2.5;

/* enumerate the ADC channels assigned to Apf11 measurements */
enum {RfAmp=8, ExpAmp=9, AirPumpAmp=10, PistonPos=11, PumpAmp=12, CtdAmp=13,
      Com1Amp=14, Com2Amp=15, BatVolt=4, Leak=5, Humid=6, Abp=7, Vac=8};

/* declare external functions used locally */
int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* function to measure the air bladder pressure                           */
/*------------------------------------------------------------------------*/
/**
   This function measures the air bladder pressure using a small
   pressure sensor integrated into the Apf11.  The computation is
   based on a 12-bit ADC measurement made by
   AirBladderPressureAmpsAd12().  The conversion from 12-bit ADC
   counts to engineering units (ie., PSIG) is based on the circuit on
   page 10 of the Apf11 schematic (Drawing 304770).

   Iridium telemetry for Apex floats is particularly dependent on
   inflating the pneumatic bladder while at the surface.  The inflated
   bladder provides an additional positive buoyancy of 1050mL which
   pushes the antenna up out of the water surface and induces the
   float to be a stiff follower of the surface wave field.  The
   pneumatic pressure in the inflated air bladder should be in the
   range [17.6,18.2] PSIA.

   Theory of operation:

      The air bladder pressure sensor on the Apf11 is a Honeywell
      SDX30A4 whose output voltage is ratiometric to its supply
      voltage.  The data sheet indicates that for a supply voltage
      (Vs=12V) then the full-scale output is 90mV.  Since the Apf11's
      supply voltage to the air bladder pressure sensor is Vs=3.33V
      then the full-scale ouput is 25.0mV [=90mV*(3.33V)/(12V)].

      Since the air bladder pressure sensor is a 30PSIA (absolute)
      pressure sensor then the measured absolute pressure is:
      P = (dPdv)*v where dPdv=1.200PSI/mV. 

      The output of the air bladder pressure sensor is attached to the
      input of an Analog Devices AMP04 instrument amplifier which is
      programmed for a gain of 100.  Hence, as the air bladder
      pressure sensor swings through the range [0,30 PSIA] then the
      air bladder pressure sensor output sweeps through the range
      [0,24.75 mV] and the output of the amplifier sweeps through the
      range [0,2.475V].  The gauge pressure is computed from the
      absolute pressure by subtracting atmospheric pressure.

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to PSIG.  If this
              is the sentinel value ADC then an ADC measurement is
              initiated and converted to PSIG.

   output:
      This function returns the air bladder (gauge) pressure in units
      of PSI.  If the 12-bit ADC count the sentinel value 0xfff then
      this function returns NaN to indicate that the value is missing
      or otherwise invalid.
   \end{verbatim}
*/
float AirBladderPressure(unsigned short ad12)
{
    /* initialize the return value */
    float abp = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = AirBladderPressureAdc();
    }

    /* check sentinel value for missing/invalid data */
    if (ad12 == NAN)
    {
        abp = NaN();
    }
    /* check sentinel value for data that are out-of-range */
    else if (ad12 >= OOR)
    {
        abp = Inf(1);
    }
    else
    {
        /* define pressure-related model parameters */
        const float Pmax = 30.0;
        const float dPdv = (Pmax / 0.025);

        /* define voltage-related model parameters */
        const float Gain=100;
        const float V = Vref * ad12 / 4096;
        const float v = V / Gain;
  
        /* compute the gauge pressure according to the Theory of Operation above */
        abp = (v * dPdv);
    }

    return abp;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of the output of air bladder pressure sensor                */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the voltage
   produced by the air bladder pressure sensor and its associated
   instrument amplifier.  The measurement will be in the closed
   interval [0,4095].  However, two sentinel values are reserved to
   indicate special conditions.  The sentinel value 0xfff indicates
   that the 12-bit ADC is missing or not-a-number.  The sentinel value
   0xffe indicates that the measurement is out-of-range on the high
   side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short AirBladderPressureAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOF, ADC3_IN7_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();
   
    /* measure the current consumed by the air bladder pressure sensor */
    if (Stm32AdcSingleConversion(ADC3, Abp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the air pump                  */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   air pump.  The computation is based on a 12-bit ADC measurement
   made by AirPumpAmpsAd12().  The conversion from 12-bit ADC counts
   to engineering units (ie., Amps) is given on page 7 of the Apf11
   schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to Amps.  If this
              is the sentinel value ADC then an ADC measurement is
              initiated and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float AirPumpAmps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = AirPumpAmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps=NaN();
    }

    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps=Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p7 */
        const float R[2] = {499, 10e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 0.25;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = Vout / (Rsense * Gain);
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of electric current consumed by air pump                    */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the air pump.  The measurement will be in the
   closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short AirPumpAmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC123_IN10_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();
   
    /* measure the current consumed by the air pump */
    if (Stm32AdcSingleConversion(ADC1, AirPumpAmp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* function to measure the barometric pressure inside an instrument       */
/*------------------------------------------------------------------------*/
/**
   This function measures the barometric pressure using a small
   pressure sensor integrated into the Apf11.  The computation is
   based on a 12-bit ADC measurement made by BarometermpsAd12().  The
   conversion from 12-bit ADC counts to engineering units (ie., in-Hg)
   is based on the circuit on page 10 of the Apf11 schematic (Drawing
   304770).

   Apex floats are partially evacuated to an absolute pressure of
   12.2PSIA in order to seat and preload the face seals on the upper
   and lower endcaps of the float.  This relatively low internal
   pressure also causes the pneumatic bladder to deflate when the air
   solenoid valve opens. Finally, the pressure differential (-2.5PSI)
   relative to atmospheric pressure induces a force of 83 pounds
   across the endcaps which serves to hold the floats together.  The
   float firmware should be configured to fail the self-test (ie., the
   OK vacuum threshold) if the internal pressure exceeds 13.6PSIA.

   Theory of operation:

      The barometric pressure sensor on the Apf11 is a Honeywell
      SDX15A4 whose output voltage is ratiometric to its supply
      voltage.  The data sheet indicates that for a supply voltage
      (Vs=12V) then the full-scale output is 90mV.  Since the Apf11's
      supply voltage to the barometer is Vs=3.33V then the full-scale
      ouput is 25.0mV [=90mV*(3.33V)/(12V)].

      Since the barometer is a 15PSIA (absolute) pressure sensor then
      the measured absolute pressure is: P = (dPdv)*v where
      dPdv=0.606PSI/mV. The measured vacuum (V) is V = (Patm - P).

      The output of the barometer is attached to the input of an
      Analog Devices AMP04 instrument amplifier which is programmed
      for a gain of 100.  Hence, as the barometer swings through the
      range [0,15 PSIA] then the barometer output sweeps through the
      range [0,24.75 mV] and the output of the amplifier sweeps
      through the range [0,2.475V].  

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to PSI. If this
              is the sentinel value ADC then an ADC measurement is
              initiated and converted to PSI.

   output:
      This function returns the vacuum (PSI) relative to atmospheric
      pressure. If the 12-bit ADC count the sentinel value 0xfff then
      this function returns NaN to indicate that the value is missing
      or otherwise invalid.
   \end{verbatim}
*/
float Barometer(unsigned short ad12)
{
    /* initialize the return value */
    float barometer = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = AirBladderPressureAdc();
    }

    /* check sentinel value for missing/invalid data */
    if (ad12 == NAN)
    {
        barometer=NaN();
    }

    /* check sentinel value for data that are out-of-range */
    else if (ad12 >= OOR)
    {
        barometer = Inf(1);
    }
    else
    {
        /* define pressure-related model parameters */
        const float Pmax = 15.0;
        const float dPdv = (Pmax / 0.025);

        /* define voltage-related model parameters */
        const float Gain = 100;
        const float V = Vref * ad12 / 4096;
        const float v = V / Gain;
   
        /* compute the barometric pressure according to the Theory of Operation above */
        barometer = (v * dPdv);
   }
   
   return barometer;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of the output of the Apf11's barometer                      */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the voltage
   produced by the barometer and its associated instrument amplifier.
   The measurement will be in the closed interval [0,4095].  However,
   two sentinel values are reserved to indicate special conditions.
   The sentinel value 0xfff indicates that the 12-bit ADC is missing
   or not-a-number.  The sentinel value 0xffe indicates that the
   measurement is out-of-range on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short BarometerAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOF, ADC3_IN8_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();
   
    /* measure the current consumed by the barometer */
    if (Stm32AdcSingleConversion(ADC3, Vac, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the Apf11                     */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current consumed by the Apf11.
   The DS2740 Coulomb counter is used to measure the electric current
   via a 16-bit ADC.  Refer to the comment section of ds2740.c for
   details of how the current measurement is made.

   \begin{verbatim}
   input:
      ad16....This  is the  16-bit ADC  output of  the DS2740  Coulomb
              counter.  Five sentinel values are implemented:
                 0x0000: Indicates missing or invalid data.
                 0x7fff: Indicates missing or invalid data.
                 0x8000: Indicates underflow of the 16-bit ADC.
                 0x7ffe: Indicates overflow of the 16-bit ADC.
                    ADC: Induces an ADC measurement which is then
                         converted to Amps.

   output:
      This function returns the electric current (Amps) consumed by
      the Apf11; the range of current measurements are [0,2.56A].
      Three sentinel values are implemented:

         NaN: This sentinel value represents missing data, invalid
              data, or some other pathological exception.  For
              example, if the presence-signal is not received after a
              reset of the Dallas One-Wire (DOW) interface then this
              sentinel value is returned.

        +Inf: This sentinel value indicates an under-flow of the
              e-current measurement.  An under-flow indicates that the
              e-current was negative and out-of-range;
              (e-current <= -2.56A).

        -Inf: This sentinel value indicates an over-flow of the
              e-current measurement.  An over-flow indicates that the
              e-current was positive and out-of-range;
              (e-current >= +2.56A).  This condition should never
              happen with the Apf11 because e-current flows from the
              positive battery terminal to the negative battery
              terminal is measured as negative.
   \end{verbatim}
*/
float BatAmps(unsigned short ad16)
{
   /* initialize return value */
   float amps=NaN();

   /* define the current per LSb of the 16-bit ADC */
   const float AmpsPerCnt=78.125e-6;

   /* check for sentinel value to induce a measurement */
   if (ad16==ADC) {ad16=BatAmpsAdc();}
      
   /* check sentinel value for missing data */
   if (ad16==0x0000 || ad16==0x7fff) {amps=NaN();}

   /* check sentinel value for under-flow of 16-bit ADC */
   else if (ad16==0x8000) {amps=Inf(1);}
   
   /* check sentinel value for over-flow of 16-bit ADC */
   else if (ad16==0x7ffe) {amps=Inf(-1);}

   /* compute the electric current in Amps */
   else {amps=(-ad16*AmpsPerCnt);}
   
   return amps;
}

/*------------------------------------------------------------------------*/
/* compute the electric charge consumed by the Apf11                      */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric charge consumed by the Apf11.
   The DS2740 Coulomb counter is used to measure the electric charge
   via a 16-bit ADC.  Refer to the comment section of ds2740.c for
   details of how the current measurement is made.

   \begin{verbatim}
   input:

      ad16....This  is the  16-bit ADC  output of  the DS2740  Coulomb
              counter.  Five sentinel values are implemented:
                 0x0000: Indicates missing or invalid data.
                 0x7fff: Indicates missing or invalid data.
                 0x8000: Indicates underflow of the 16-bit ADC.
                 0x7ffe: Indicates overflow of the 16-bit ADC.
                    ADC: Induces an ADC measurement which is then
                         converted to Amps.

   output:
      This function returns the electric charge (Coulombs) consumed by
      the Apf11; the range of charge measurements are [0,36.864kC].
      Three sentinel values are implemented:

         NaN: This sentinel value represents missing data, invalid
              data, or some other pathological exception.  For
              example, if the presence-signal is not received after a
              reset of the Dallas One-Wire (DOW) interface then this
              sentinel value is returned.

        -Inf: This sentinel value indicates an under-flow of the
              Couloumb measurement.  An under-flow indicates that the
              charge accumulation was negative and out-of-range;
              (charge <= -36.864kC).

        +Inf: This sentinel value indicates an over-flow of the
              Coulomb measurement.  An over-flow indicates that the
              charge accumulation was positive and out-of-range;
              (charge >= +36.864kC).  This condition should never
              happen with the Apf11 because charge is always consumed
              from the battery.
   \end{verbatim}
*/
float BatCharge(unsigned short ad16)
{
    /* initialize return value */
    float charge = NaN();

    /* define the charge per LSb of the 16-bit ADC */
    const float ChargePerCnt = 1.125;

    /* check for sentinel value to induce a measurement */
    if (ad16 == ADC)
    {
        ad16 = BatChargeAdc();
    }

    /* check sentinel value for missing data */
    if (ad16 == 0x0000 || ad16 == 0x7fff)
    {
        charge = NaN();
    }
    /* check sentinel value for under-flow of 16-bit ADC */
    else if (ad16 == 0x8000)
    {
        charge = Inf(-1);
    }
    /* check sentinel value for over-flow of 16-bit ADC */
    else if (ad16 == 0x7ffe)
    {
        charge = Inf(1);
    }
    /* compute the electric charge in Coulombs */
    else
    {
        charge = (-ad16 * ChargePerCnt);
    }

    return charge;
}

/*------------------------------------------------------------------------*/
/* compute the Apf11's battery voltage                                    */
/*------------------------------------------------------------------------*/
/**
   This function computes the Apf11's battery voltage (Volts).  The
   computation is based on a 12-bit ADC measurement made by
   BatVoltsAd12().  The conversion from 12-bit ADC counts to
   engineering units (ie., Amps) is given on page 11 of the Apf11
   schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted. If this is the
              sentinel value ADC then an ADC measurement is initiated
              and converted to Volts.

   output:
      This function computes the battery voltage that is associated
      with a 12-bit ADC.  If the 12-bit ADC count the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the battery voltage is out-of-range on the high side.
   \end{verbatim}
*/
float BatVolts(unsigned short ad12)
{
    /* compute the current */
    float volts = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = BatVoltsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        volts = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        volts = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p11 */
        const float R[2] = {62e3, 10e3};
        const float Gain = R[1] / (R[0] + R[1]);
        const float Vout = Vref * ad12 / 4096;
   
        /* compute the battery voltage that corresponds to the 12-bit ADC */
        volts = (Vout / Gain);
    }

    return volts;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of battery voltage                                          */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the battery
   voltage.  The measurement will be in the closed interval [0,4095].
   However, two sentinel values are reserved to indicate special
   conditions.  The sentinel value 0xfff indicates that the 12-bit ADC
   is missing or not-a-number.  The sentinel value 0xffe indicates
   that the measurement is out-of-range on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short BatVoltsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOF, ADC3_IN4_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();

    /* measure the current consumed by the expansion interface */
    if (Stm32AdcSingleConversion(ADC3, BatVolt, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();

    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the com1                      */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   com1 serial port.  The computation is based on a 12-bit ADC
   measurement made by Com1AmpsAd12().  The conversion from 12-bit ADC
   counts to engineering units (ie., Amps) is given on page 16 of the
   Apf11 schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted. If this is the
              sentinel value ADC then an ADC measurement is initiated
              and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float Com1Amps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = Com1AmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p16 */
        const float R[2] = {499, 10e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 0.13;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = (Vout / (Rsense * Gain));
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of electric current consumed by com1                        */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the com1 serial port.  The measurement will be
   in the closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short Com1AmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC12_IN14_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();

    /* measure the current consumed by the com1 */
    if (Stm32AdcSingleConversion(ADC1, Com1Amp, &ad12) <= 0)
    {
        ad12 = NAN;
    }
    /* disable power to the analog measurement system */
    AnalogMeasureDisable();

    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the com2                      */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   com2 serial port.  The computation is based on a 12-bit ADC
   measurement made by Com2AmpsAd12().  The conversion from 12-bit
   ADC counts to engineering units (ie., Amps) is given on page 7 of
   the Apf11 schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted. If this is the
              sentinel value ADC then an ADC measurement is initiated
              and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float Com2Amps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();
   
    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = Com2AmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p17 */
        const float R[2] = {499, 10e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 0.13;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = (Vout / (Rsense * Gain));
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of electric current consumed by com2                        */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the com2.  The measurement will be in the
   closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short Com2AmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC12_IN15_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();

    /* measure the current consumed by the com2 */
    if (Stm32AdcSingleConversion(ADC1, Com2Amp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the ctd                       */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   ctd.  The computation is based on a 12-bit ADC measurement made by
   CtdAmpsAd12().  The conversion from 12-bit ADC counts to
   engineering units (ie., Amps) is given on page 9 of the Apf11
   schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted. If this is the
              sentinel value ADC then an ADC measurement is initiated
              and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float CtdAmps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = CtdAmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p9 */
        const float R[2] = {499, 10e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 2.49;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = (Vout / (Rsense * Gain));
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of electric current consumed by ctd                         */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the ctd.  The measurement will be in the closed
   interval [0,4095].  However, two sentinel values are reserved to
   indicate special conditions.  The sentinel value 0xfff indicates
   that the 12-bit ADC is missing or not-a-number.  The sentinel value
   0xffe indicates that the measurement is out-of-range on the high
   side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short CtdAmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC123_IN13_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();
   
    /* measure the current consumed by the ctd */
    if (Stm32AdcSingleConversion(ADC1, CtdAmp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();

    return ad12;
}

/*------------------------------------------------------------------------*/
/* function to measure the relative humidity of the air around the Apf11  */
/*------------------------------------------------------------------------*/
/**
   This function measures the relative humidity of the air around the
   Apf11.  The computation is based on a 12-bit ADC measurement made
   by HumidityAmpsAd12().  The conversion from 12-bit ADC counts to
   engineering units (ie., %RH) is given on page 10 of the Apf11
   schematic (Drawing 304770).

   Theory of operation:

      The humidity sensor on the Apf11 is a Honeywell HIH-5031 whose
      output voltage is ratiometric to its supply voltage.  The data
      sheet indicates that for a supply voltage (Vs=3.3V) then the
      full-scale output is approximately 2.5V.

      The relative humidity is given by the equation RH =
      ((V/Vs)-Offset)/Slope where the Offset is typically 0.1515 and
      the slope is typically 0.00636 at 25C.  There is a minor
      temperature dependence which is not accounted for in this
      function. 

      The output of the humidity sensor is connected directly to the
      ADC.  Hence, as the humidity swings through the range [0,95.3%RH]
      then the humidity output sweeps through the range [0.5V,2.5V]

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted.

   output:
      This function returns the relative humidity (%RH) of the air
      around the Apf11.  If the 12-bit ADC count the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.
   \end{verbatim}
*/
float Humidity(unsigned short ad12)
{
    /* initialize the return value */
    float humidity = NaN();
   
    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = HumidityAdc();
    }

    /* check sentinel value for missing/invalid data */
    if (ad12 == NAN)
    {
        humidity = NaN();
    }
    /* check sentinel value for data that are out-of-range */
    else if (ad12 >= OOR)
    {
        humidity = Inf(1);
    }
    else
    {
        /* define voltage-related model parameters */
        const float Slope = 0.00636;
        const float Vs = 3.3;
        const float V = Vref * ad12 / 4096;
        const float Offset = 0.1515;
   
        /* compute the barometric pressure according to the Theory of Operation above */
        humidity = (((V / Vs) - Offset) / Slope);
    }

    return humidity;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of the output of the humidity sensor                        */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the humidity
   sensor.  The measurement will be in the closed interval [0,4095].
   However, two sentinel values are reserved to indicate special
   conditions.  The sentinel value 0xfff indicates that the 12-bit ADC
   is missing or not-a-number.  The sentinel value 0xffe indicates
   that the measurement is out-of-range on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short HumidityAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOF, ADC3_IN6_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();

    /* measure the current consumed by the humidity */
    if (Stm32AdcSingleConversion(ADC3, Humid, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the hydraulic pump            */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   hydraulic pump.  The computation is based on a 12-bit ADC
   measurement made by HydraulicPumpAmpsAd12().  The conversion from
   12-bit ADC counts to engineering units (ie., Amps) is given on page
   6 of the Apf11 schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to Amps.  If this
              is the sentinel value ADC then an ADC measurement is
              initiated and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float HydraulicPumpAmps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = HydraulicPumpAmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p6 */
        const float R[2] = {499, 20e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 0.03;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = (Vout / (Rsense * Gain));
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of electric current consumed by hydraulic pump              */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the hydraulic pump.  The measurement will be in
   the closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short HydraulicPumpAmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC123_IN12_Pin, InAnlg);

    /* activate the hydraulic pump's monitor circuit  */
    HydraulicPumpMonitorOn();

    /* measure the current consumed by the hydraulic pump */
    if (Stm32AdcSingleConversion(ADC1, PumpAmp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* deactivate the hydraulic pump's monitor circuit  */
    HydraulicPumpMonitorOff();
    return ad12;
}

/*------------------------------------------------------------------------*/
/* function to measure the Apf11's leak-detection index                   */
/*------------------------------------------------------------------------*/
/**
   This function measures the Apf11's leak-detection index which
   ranges from [0,1] where zero is a strong non-leak indicator and one
   is a strong leak indicator.  The computation is based on a 12-bit
   ADC measurement made by LeakAd12().  The conversion from 12-bit ADC
   counts to engineering units (ie., volts) is based on the circuit on
   page 10 of the Apf11 schematic (Drawing 304770).

   Theory of operation:

      The leak detector circuit consists of a resistor network
      involving three resistors.  Resistors R0=100K & R1=232K are
      connected in series as a voltage divider, driven by Vs=3.3V, and
      the leak-sense voltage between the resistors R0,R1 is measured
      by the Stm32 ADC.  The sample-point between R0,R1 is also
      connected to a 2-pin header (J6:2).  Pin J6:1 is connected to a
      R2=22 Ohm resistor; the other end of R2 is connected to ground.
      An leak-detection PCB is mounted in the lower endcap of the
      float where any leakage-water might collect.  This PCB is
      connected by two wires to connector J6 on the Apf11.

      The notion behind the leak-detection PCB is that if the float
      remains dry then pins J6:{1,2} should remain in an open-circuit
      condition.  However, if flooding happens then the water should
      induce the PCB to short pins J6:{1,2}.  Shorting J6:{1,2} causes
      R2 to be connected in parallel with R1 which has the effect of
      drastically altering the voltage divider network.  The voltage
      measured between R0,R1 will drop from 2.35V [=Vs*R1/(R0+R1)]
      down to near zero [=Vs*R2/(R0+R2)].  Hence, if a leak occurs
      then the leak-detection voltage will drop from 2.35V down to
      near zero.

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted.

   output:
      This function returns the the leak-detection index which ranges
      from [0,1] where zero is a strong non-leak indicator and one is
      a strong leak indicator. If the 12-bit ADC count the sentinel
      value 0xfff then this function returns NaN to indicate that the
      value is missing or otherwise invalid.
   \end{verbatim}
*/
float LeakDetector(unsigned short ad12)
{
    /* compute the current */
    float leak = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = LeakAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        leak = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        leak = Inf(1);
    }
    else
    {
        /* define leak-detector model parameters */
        const float R[] = {100e3, 232e3, 22};
        const float Vs = 3.3;

        /* compute the extreme voltages of the leak-detector circuit */
        const float V[] = {Vs * R[1] / (R[0] + R[1]), Vs * R[2] / (R[0] + R[2])};

        /* compute and condition the leak-detection voltage */
        float v = Vref * ad12 / 4096;
        if (v > V[0])
        {
            v = V[0];
        }
        else if (v < V[1])
        {
            v = V[1];
        }

        /* compute the leak detection index */
        leak = (1 - (v - V[1]) / (V[0] - V[1]));
    }

    return leak;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC of the Apf11's leak detector                                */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the voltage
   produced by the Apf11's leak-detection circuit.  The measurement
   will be in the closed interval [0,4095].  However, two sentinel
   values are reserved to indicate special conditions.  The sentinel
   value 0xfff indicates that the 12-bit ADC is missing or
   not-a-number.  The sentinel value 0xffe indicates that the
   measurement is out-of-range on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short LeakAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOF, ADC3_IN5_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable(); Wait(100);

    /* measure the leak-detection circuit */
    if (Stm32AdcSingleConversion(ADC3, Leak, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();

    return ad12;
}

/*------------------------------------------------------------------------*/
/* function to measure the Apf11's leak-detector voltage                  */
/*------------------------------------------------------------------------*/
/**
   This function measures the voltage of Apf11's leak-detection
   circuit.  The computation is based on a 12-bit ADC measurement made
   by LeakAd12().  The conversion from 12-bit ADC counts to
   engineering units (ie., volts) is based on the circuit on page 10
   of the Apf11 schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted.

   output:
      This function returns the voltage of the leak-detection
      circuit. If the 12-bit ADC count the sentinel value 0xfff then
      this function returns NaN to indicate that the value is missing
      or otherwise invalid.
   \end{verbatim}
*/
float LeakVolts(unsigned short ad12)
{
   /* define voltage-related model parameters */
   const float V = Vref * ad12 / 4096;
   
   /* compute the leak detection voltage */
   float leak = (ad12 == NAN) ? NaN() : V;
   return leak;
}

/*------------------------------------------------------------------------*/
/* function to measure the 12-bit piston position of the hydraulic pump   */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the buoyancy
   engine's high pressure pump piston.  The measurement will be in the
   closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short PistonPositionAdc(void)
{
    /* initialize the return value & define local work object */
    unsigned int ad12 = NAN;

    /* define stack-based objects for use with computing the mean */
    int m, n, N = 5;
    unsigned int sum;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOC, ADC123_IN11_Pin, InAnlg);

    /* activate the hydraulic pump's monitor circuit  */
    HydraulicPumpMonitorOn();

    /* loop to compute multiple samples */
    for (sum = 0, m = 0, n = N; n > 0; n--)
    {
        /* execute a single 12-bit ADC conversion */
        // TODO rmarver: Figure out how to sim this ADC measurement based on the
        // position, or feed a fake value into the MAX7301.
        if (Stm32AdcSingleConversion(ADC1, PistonPos, &ad12) <= 0)
        {
            ad12 = NAN;
            break;
        }
        /* check for sentinel values */
        else if (ad12 == OOR || ad12 == NAN)
        {
            break;
        }
        /* accumulate the sum of the AD samples */
        else
        {
            sum += ad12;
            m++;
            if (n > 1)
            {
                Wait(5);
            }
        }
    }

    /* deactivate the hydraulic pump's monitor circuit  */
    HydraulicPumpMonitorOff();

    /* check for sentinels and compute the mean */
    if (ad12 != OOR && ad12 != NAN)
    {
        ad12 = (m > 0) ? (sum / m) : NAN;
    }

    return ad12;
}

/*------------------------------------------------------------------------*/
/* function to measure the volume displaced by the piston                 */
/*------------------------------------------------------------------------*/
/**
   This function measures the 12-bit ADC piston extension and computes
   from that measurement the volume (milliliters) displaced by the
   piston as it is extended.

   Theory of operation:

      The internal diameter of the cylinder is 1.250(+0.004,-0.000)
      inches and the total gross stroke-length of the piston is
      L=31.8cm.  Given that the radius of the piston is R=1.5875cm the
      total gross displacement of the buoyancy engine is
         V = Pi*L*R^2 = 251.8ml

      The Apf11 measures the 8-bit piston position in the same way
      that the Apf9 does; the piston is fully retracted at nine counts
      and fully extended at 227 counts. When the piston is fully
      retracted (ie., piston extension is nine 8-bit counts or 144
      12-bit counts), the trailing edge of the linear potentiometer's
      slide is 31.5mm from the upper end of the linear potentiometer.
      When the piston is fully extended (ie., the piston extension is
      227 8-bit counts or 3633 12-bit counts), the trailing edge of
      the slide is 349.5mm from the upper end of the linear
      potentiometer.

      The following linear expression computes the displaced volume as
      a function of 12-bit count (c):

             dV
         V = -- * c - Vo
             dc

      where dV/dc=0.07069ml/count and Vo=5.0ml.
      
   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to milliliters of
              pump displacement.  If this is the sentinel value ADC
              then an ADC measurement is initiated and converted to
              milliliters .

   output:
      This function returns the volume (milliliters) displaced by the
      piston.  If the 12-bit ADC count is the sentinel value 0xfff
      then this function returns NaN to indicate that the value is
      missing or otherwise invalid.
   \end{verbatim}
*/
float PistonVolume(unsigned short ad12)
{
    /* define a linear transformation between ADC and volume displacement */
    const float dVdc = 0.07069, Vo = 5.0;

    /* initialize the return value */
    float V = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = PistonPositionAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        V = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        V = Inf(1);
    }
    /* compute the volume displaced by the hydraulic pump */
    else
    {
        V = (dVdc * ad12 - Vo);
    }

    return V;
}

/*------------------------------------------------------------------------*/
/* compute the electric current consumed by the RF interface              */
/*------------------------------------------------------------------------*/
/**
   This function computes the electric current (Amps) consumed by the
   RF interface.  The computation is based on a 12-bit ADC measurement
   made by RfAmpsAd12().  The conversion from 12-bit ADC counts to
   engineering units (ie., Amps) is given on page 4 of the Apf11
   schematic (Drawing 304770).

   \begin{verbatim}
   input:
      ad12....The 12-bit measurement to be converted to Amps.  If this
              is the sentinel value ADC then an ADC measurement is
              initiated and converted to Amps.

   output:
      This function computes the electric current that is associated
      with a 12-bit ADC.  If the 12-bit ADC count is the sentinel value
      0xfff then this function returns NaN to indicate that the value
      is missing or otherwise invalid.  If the 12-bit ADC count is the
      sentinel value 0xffe then this function returns +Inf to indicate
      that the electric current measurement is out-of-range on the
      high side.
   \end{verbatim}
*/
float RfAmps(unsigned short ad12)
{
    /* compute the current */
    float amps = NaN();

    /* check for sentinel value to induce a measurement */
    if (ad12 == ADC)
    {
        ad12 = RfAmpsAdc();
    }

    /* check the sentinel that indicates missing/invalid data */
    if (ad12 == NAN)
    {
        amps = NaN();
    }
    /* check the sentinel that indicates out-of-range data */
    else if (ad12 == OOR)
    {
        amps = Inf(1);
    }
    else
    {
        /* define model parameters given on Drawing 304770, p4 */
        const float R[2] = {499, 10e3};
        const float Gain = R[1] / R[0];
        const float Rsense = 0.04;
        const float Vout = Vref * ad12 / 4096;

        /* compute the electric current that corresponds to the 12-bit ADC */
        amps = (Vout / (Rsense * Gain));
    }

    return amps;
}

/*------------------------------------------------------------------------*/
/* 12-bit ADC measurement of electric current consumed by RF interface    */
/*------------------------------------------------------------------------*/
/**
   This function measures a 12-bit ADC conversion of the electric
   current consumed by the RF interface.  The measurement will be in
   the closed interval [0,4095].  However, two sentinel values are
   reserved to indicate special conditions.  The sentinel value 0xfff
   indicates that the 12-bit ADC is missing or not-a-number.  The
   sentinel value 0xffe indicates that the measurement is out-of-range
   on the high side.

   \begin{verbatim}
   output:
      This function returns an unsigned integer in the closed interval
      [0,4095] that represents the 12-bit ADC measurement.  If the
      12-bit ADC count the sentinel value 0xfff then this function
      returns NaN to indicate that the value is missing or otherwise
      invalid.  If the 12-bit ADC count is the sentinel value 0xffe
      then an out-of-range error condition is indicated.
   \end{verbatim}
*/
unsigned short RfAmpsAdc(void)
{
    /* initialize the return value */
    unsigned int ad12 = NAN;

    /* configure the ADC input pin on the Stm32 */
    Stm32GpioConfig(GPIOB, ADC12_IN8_Pin, InAnlg);

    /* enable power to the analog measurement system */
    AnalogMeasureEnable();

    /* measure the current consumed by the RF interface */
    if (Stm32AdcSingleConversion(ADC1, RfAmp, &ad12) <= 0)
    {
        ad12 = NAN;
    }

    /* disable power to the analog measurement system */
    AnalogMeasureDisable();
    return ad12;
}

#endif /* APF11AD_C */
