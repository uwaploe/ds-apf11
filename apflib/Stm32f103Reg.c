#ifndef STM32F103REG_H
#define STM32F103REG_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103RegChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stdio.h>
#include <stm32f103xg.h>

/* declare functions with external linkage */
int AdcRegMap(ADC_TypeDef *adc,FILE *dest);
int AfioRegMap(FILE *dest);
int CtdRegMap(FILE *dest);
int ExtiRegMap(FILE *dest);
int FsmcRegMap(FILE *dest);
int GpioRegMap(GPIO_TypeDef *GPIOx,FILE *dest);
int PwrRegMap(FILE *dest);
int RccRegMap(FILE *dest);
int RtcRegMap(FILE *dest);
int SdioRegMap(FILE *dest);
int Spi1RegMap(FILE *dest);
int Spi2RegMap(FILE *dest);
int Usart1RegMap(FILE *dest);

/* define the return states of the Stm32 registers API */
extern const char RegNull; /* NULL function argument */
extern const char RegFail; /* general failure */
extern const char RegOk;   /* general success */

#endif /* STM32F103REG_H */
#ifdef STM32F103REG_C
#undef STM32F103REG_C

#include <assert.h>
#include <string.h>

/* define the return states of the Stm32 RTC API */
const char RegNull =  -1; /* NULL function argument */
const char RegFail =   0; /* general failure */
const char RegOk   =   1; /* general success */

/* prototypes for functions with static linkage */
static const char *uint32bits(unsigned long int reg);

/*------------------------------------------------------------------------*/
/* create a string of the state of each bit in a 4-byte word              */
/*------------------------------------------------------------------------*/
/**
   This function creates a 32-byte string that shows the state of each
   bit in a 4-byte long-word.  

      \begin{verbatim}
      input:
         reg....32-bit long word containing the value of a register.

      output:
         This function returns a pointer to string that shows the
         state of each bit in a 4-byte long-word.  The bits are in
         little-endian order within the string.
      \end{verbatim}
*/
static const char *uint32bits(unsigned long int reg)
{
   /* initialize the return value */
   int n,m; static char bits[8*sizeof(reg)+8];

   /* validate the size of unsigned long integers */
   assert(sizeof(reg)==4);

   /* initialize the bits string to zeros */
   memset((void *)bits,0,sizeof(bits));

   /* scan each bit of the 4-byte word */
   for (m=38,n=0; n<8*sizeof(reg); n++,m--)
   {
      /* add a space between nibbles */
      if (n && !(n%4)) {bits[m--]=' ';}

      /* record the state of each bit */
      bits[m] = (reg&(1U<<n)) ? '1' : '0';
   }

   return bits;
}

/*------------------------------------------------------------------------*/
/* create register map showing state of each bit in the ADC registers     */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the registers of a specified Stm32f103 ADC.

      \begin{verbatim}
      input:

         adc.....A pointer to a specified ADC of the Stm32f103.

         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int AdcRegMap(ADC_TypeDef *adc,FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* report the ADC registers */
      fprintf(dest,"[ADC base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)adc,3,2,1,0);
      fprintf(dest,"Offset AdcReg %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"---------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] SR      = 0x%08lx : %s\n", adc->SR,    uint32bits(adc->SR));
      fprintf(dest,"[0x04] CR1     = 0x%08lx : %s\n", adc->CR1,   uint32bits(adc->CR1));
      fprintf(dest,"[0x08] CR2     = 0x%08lx : %s\n", adc->CR2,   uint32bits(adc->CR2));
      fprintf(dest,"[0x0c] SMPR[1] = 0x%08lx : %s\n", adc->SMPR1, uint32bits(adc->SMPR1));
      fprintf(dest,"[0x10] SMPR[2] = 0x%08lx : %s\n", adc->SMPR2, uint32bits(adc->SMPR2));
      fprintf(dest,"[0x14] JOFR[1] = 0x%08lx : %s\n", adc->JOFR1, uint32bits(adc->JOFR1));
      fprintf(dest,"[0x18] JOFR[2] = 0x%08lx : %s\n", adc->JOFR2, uint32bits(adc->JOFR2));
      fprintf(dest,"[0x1c] JOFR[3] = 0x%08lx : %s\n", adc->JOFR3, uint32bits(adc->JOFR3));      
      fprintf(dest,"[0x20] JOFR[4] = 0x%08lx : %s\n", adc->JOFR4, uint32bits(adc->JOFR4));      
      fprintf(dest,"[0x24] HTR     = 0x%08lx : %s\n", adc->HTR,   uint32bits(adc->HTR));
      fprintf(dest,"[0x28] LTR     = 0x%08lx : %s\n", adc->LTR,   uint32bits(adc->LTR));
      fprintf(dest,"[0x2c] SQR[1]  = 0x%08lx : %s\n", adc->SQR1,  uint32bits(adc->SQR1));
      fprintf(dest,"[0x30] SQR[2]  = 0x%08lx : %s\n", adc->SQR2,  uint32bits(adc->SQR2));
      fprintf(dest,"[0x34] SQR[3]  = 0x%08lx : %s\n", adc->SQR3,  uint32bits(adc->SQR3));      
      fprintf(dest,"[0x38] JSQR    = 0x%08lx : %s\n", adc->JSQR,  uint32bits(adc->JSQR));
      fprintf(dest,"[0x04] JDR[1]  = 0x%s : %s\n",   "????????",  "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x04] JDR[2]  = 0x%s : %s\n",   "????????",  "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x04] JDR[3]  = 0x%s : %s\n",   "????????",  "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x04] JDR[4]  = 0x%s : %s\n",   "????????",  "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x04] DR      = 0x%s : %s\n",   "????????",  "???? ???? ???? ???? ???? ???? ???? ????");
   }   

   /* indicate that the function argument is NULL */
   else {status=RegNull;}

   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing state of each bit in the AFIO registers    */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the AFIO registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int AfioRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile AFIO registers */
      AFIO_TypeDef AfioReg=(*AFIO);

      /* report the AFIO registers */
      fprintf(dest,"[AFIO base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)AFIO,3,2,1,0);
      fprintf(dest,"Offset AfioReg %14s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"-----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] EVCR      = 0x%08lx : %s\n", AfioReg.EVCR,      uint32bits(AfioReg.EVCR));
      fprintf(dest,"[0x04] MAPR      = 0x%08lx : %s\n", AfioReg.MAPR,      uint32bits(AfioReg.MAPR));
      fprintf(dest,"[0x08] EXTICR[0] = 0x%08lx : %s\n", AfioReg.EXTICR[0], uint32bits(AfioReg.EXTICR[0]));
      fprintf(dest,"[0x0c] EXTICR[1] = 0x%08lx : %s\n", AfioReg.EXTICR[1], uint32bits(AfioReg.EXTICR[1]));
      fprintf(dest,"[0x10] EXTICR[2] = 0x%08lx : %s\n", AfioReg.EXTICR[2], uint32bits(AfioReg.EXTICR[2]));
      fprintf(dest,"[0x14] EXTICR[3] = 0x%08lx : %s\n", AfioReg.EXTICR[3], uint32bits(AfioReg.EXTICR[3]));      
      fprintf(dest,"[0x1c] MAPR2     = 0x%08lx : %s\n", AfioReg.MAPR2,     uint32bits(AfioReg.MAPR2));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in USART2 registers  */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the USART2 registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int CtdRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile USART2 registers */
      USART_TypeDef Usart2Reg=(*USART2);

      /* report the USART2 registers */
      fprintf(dest,"[USART2 base: 0x%08lx] %7d %11d %12d %11d\n",(uint32_t)USART2,3,2,1,0);
      fprintf(dest,"Offset %5sReg %11s   1098 7654 3210 9876 5432 1098 7654 3210\n","Usart2","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] SR       = 0x%08lx : %s\n", Usart2Reg.SR,       uint32bits(Usart2Reg.SR));
      fprintf(dest,"[0x04] DR       = 0x%s : %s\n",    "????????",         "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x08] BRR      = 0x%08lx : %s\n", Usart2Reg.BRR,      uint32bits(Usart2Reg.BRR));
      fprintf(dest,"[0x0c] CR1      = 0x%08lx : %s\n", Usart2Reg.CR1,      uint32bits(Usart2Reg.CR1));
      fprintf(dest,"[0x10] CR2      = 0x%08lx : %s\n", Usart2Reg.CR2,      uint32bits(Usart2Reg.CR2));
      fprintf(dest,"[0x14] CR3      = 0x%08lx : %s\n", Usart2Reg.CR3,      uint32bits(Usart2Reg.CR3));
      fprintf(dest,"[0x18] GTPR     = 0x%08lx : %s\n", Usart2Reg.GTPR,     uint32bits(Usart2Reg.GTPR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing state of each bit in the EXTI registers    */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the EXTI registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int ExtiRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile EXTI registers */
      EXTI_TypeDef ExtiReg=(*EXTI);

      /* report the EXTI registers */
      fprintf(dest,"[EXTI base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)EXTI,3,2,1,0);
      fprintf(dest,"Offset ExtiReg %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] IMR      = 0x%08lx : %s\n", ExtiReg.IMR,      uint32bits(ExtiReg.IMR));
      fprintf(dest,"[0x04] EMR      = 0x%08lx : %s\n", ExtiReg.EMR,      uint32bits(ExtiReg.EMR));
      fprintf(dest,"[0x08] RTSR     = 0x%08lx : %s\n", ExtiReg.RTSR,     uint32bits(ExtiReg.RTSR));
      fprintf(dest,"[0x0c] FTSR     = 0x%08lx : %s\n", ExtiReg.FTSR,     uint32bits(ExtiReg.FTSR));
      fprintf(dest,"[0x10] SWIER    = 0x%08lx : %s\n", ExtiReg.SWIER,    uint32bits(ExtiReg.SWIER));
      fprintf(dest,"[0x14] PR       = 0x%08lx : %s\n", ExtiReg.PR,       uint32bits(ExtiReg.PR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
int FsmcRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;
 
   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile FSMCx registers */
      FSMC_Bank1_TypeDef FsmcReg=(*FSMC_Bank1);
      
      /* report the FSMC registers */
      fprintf(dest,"[FSMC base: 0x%08lx] %9d %11d %12d %11d\n",(unsigned long int)FSMC_Bank1,3,2,1,0);
      fprintf(dest,"Offset %5sReg %12s   1098 7654 3210 9876 5432 1098 7654 3210\n","Fsmc1","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] BCR1     = 0x%08lx : %s\n", FsmcReg.BTCR[0],   uint32bits(FsmcReg.BTCR[0]));
      fprintf(dest,"[0x04] BTR1     = 0x%08lx : %s\n", FsmcReg.BTCR[1],   uint32bits(FsmcReg.BTCR[1]));
      fprintf(dest,"[0x08] BCR2     = 0x%08lx : %s\n", FsmcReg.BTCR[2],   uint32bits(FsmcReg.BTCR[2]));
      fprintf(dest,"[0x0c] BTR2     = 0x%08lx : %s\n", FsmcReg.BTCR[3],   uint32bits(FsmcReg.BTCR[3]));
      fprintf(dest,"[0x10] BCR3     = 0x%08lx : %s\n", FsmcReg.BTCR[4],   uint32bits(FsmcReg.BTCR[4]));
      fprintf(dest,"[0x14] BTR3     = 0x%08lx : %s\n", FsmcReg.BTCR[5],   uint32bits(FsmcReg.BTCR[5]));
      fprintf(dest,"[0x18] BCR4     = 0x%08lx : %s\n", FsmcReg.BTCR[6],   uint32bits(FsmcReg.BTCR[6]));
      fprintf(dest,"[0x1c] BTR4     = 0x%08lx : %s\n", FsmcReg.BTCR[7],   uint32bits(FsmcReg.BTCR[7]));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in USART2 registers  */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the USART2 registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int GpioRegMap(GPIO_TypeDef *GPIOx,FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile GPIOx registers */
      GPIO_TypeDef GpioReg=(*GPIOx);

      /* report the GPIO registers */
      fprintf(dest,"[GPIO base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)GPIOx,3,2,1,0);
      fprintf(dest,"Offset %5sReg %12s   1098 7654 3210 9876 5432 1098 7654 3210\n","GPIOx","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CRL      = 0x%08lx : %s\n", GpioReg.CRL,      uint32bits(GpioReg.CRL));
      fprintf(dest,"[0x04] CRH      = 0x%08lx : %s\n", GpioReg.CRH,      uint32bits(GpioReg.CRH));
      fprintf(dest,"[0x08] IDR      = 0x%08lx : %s\n", GpioReg.IDR,      uint32bits(GpioReg.IDR));
      fprintf(dest,"[0x0c] ODR      = 0x%08lx : %s\n", GpioReg.ODR,      uint32bits(GpioReg.ODR));
      fprintf(dest,"[0x10] BSRR     = 0x%08lx : %s\n", GpioReg.BSRR,     uint32bits(GpioReg.BSRR));
      fprintf(dest,"[0x14] BRR      = 0x%08lx : %s\n", GpioReg.BRR,      uint32bits(GpioReg.BRR));
      fprintf(dest,"[0x18] LCKR     = 0x%08lx : %s\n", GpioReg.LCKR,     uint32bits(GpioReg.LCKR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in the PWR registers */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the PWR registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int PwrRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile PWR registers */
      PWR_TypeDef PwrReg=(*PWR);

      /* report the PWR registers */
      fprintf(dest,"[PWR base: 0x%08lx] %10d %11d %12d %11d\n",(uint32_t)PWR,3,2,1,0);
      fprintf(dest,"Offset PwrReg  %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CR       = 0x%08lx : %s\n", PwrReg.CR,       uint32bits(PwrReg.CR));
      fprintf(dest,"[0x04] CSR      = 0x%08lx : %s\n", PwrReg.CSR,      uint32bits(PwrReg.CSR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in the RCC registers */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the RCC registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int RccRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile RCC registers */
      RCC_TypeDef RccReg=(*RCC);

      /* report the RCC registers */
      fprintf(dest,"[RCC base: 0x%08lx] %10d %11d %12d %11d\n",(uint32_t)RCC,3,2,1,0);
      fprintf(dest,"Offset RccReg  %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CR       = 0x%08lx : %s\n", RccReg.CR,       uint32bits(RccReg.CR));
      fprintf(dest,"[0x04] CFGR     = 0x%08lx : %s\n", RccReg.CFGR,     uint32bits(RccReg.CFGR));
      fprintf(dest,"[0x08] CIR      = 0x%08lx : %s\n", RccReg.CIR,      uint32bits(RccReg.CIR));
      fprintf(dest,"[0x0c] APB2RSTR = 0x%08lx : %s\n", RccReg.APB2RSTR, uint32bits(RccReg.APB2RSTR));
      fprintf(dest,"[0x10] APB1RSTR = 0x%08lx : %s\n", RccReg.APB1RSTR, uint32bits(RccReg.APB1RSTR));
      fprintf(dest,"[0x14] AHBENR   = 0x%08lx : %s\n", RccReg.AHBENR,   uint32bits(RccReg.AHBENR));
      fprintf(dest,"[0x18] APB2ENR  = 0x%08lx : %s\n", RccReg.APB2ENR,  uint32bits(RccReg.APB2ENR));
      fprintf(dest,"[0x1c] APB1ENR  = 0x%08lx : %s\n", RccReg.APB1ENR,  uint32bits(RccReg.APB1ENR));
      fprintf(dest,"[0x20] BDCR     = 0x%08lx : %s\n", RccReg.BDCR,     uint32bits(RccReg.BDCR));
      fprintf(dest,"[0x24] CSR      = 0x%08lx : %s\n", RccReg.CSR,      uint32bits(RccReg.CSR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in the RCC registers */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the RCC registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered.
      \end{verbatim}
*/
int RtcRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile RTC registers */
      RTC_TypeDef RtcReg=(*RTC);

      /* report the RTC registers */
      fprintf(dest,"[RTC base: 0x%08lx] %7d %11d %12d %11d\n",(uint32_t)RTC,3,2,1,0);
      fprintf(dest,"Offset RtcReg %11s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"-------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CRH   = 0x%08lx : %s\n", RtcReg.CRH,  uint32bits(RtcReg.CRH));
      fprintf(dest,"[0x04] CRL   = 0x%08lx : %s\n", RtcReg.CRL,  uint32bits(RtcReg.CRL));
      fprintf(dest,"[0x08] PRLH  = 0x%08lx : %s\n", RtcReg.PRLH, uint32bits(RtcReg.PRLH));
      fprintf(dest,"[0x0c] PRLL  = 0x%08lx : %s\n", RtcReg.PRLL, uint32bits(RtcReg.PRLL));
      fprintf(dest,"[0x10] DIVH  = 0x%08lx : %s\n", RtcReg.DIVH, uint32bits(RtcReg.DIVH));
      fprintf(dest,"[0x14] DIVL  = 0x%08lx : %s\n", RtcReg.DIVL, uint32bits(RtcReg.DIVL));
      fprintf(dest,"[0x18] CNTH  = 0x%08lx : %s\n", RtcReg.CNTH, uint32bits(RtcReg.CNTH));
      fprintf(dest,"[0x1c] CNTL  = 0x%08lx : %s\n", RtcReg.CNTL, uint32bits(RtcReg.CNTL));
      fprintf(dest,"[0x20] ALRH  = 0x%08lx : %s\n", RtcReg.ALRH, uint32bits(RtcReg.ALRH));
      fprintf(dest,"[0x24] ALRL  = 0x%08lx : %s\n", RtcReg.ALRL, uint32bits(RtcReg.ALRL));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in SDIO registers    */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   of the SDIO registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered.
      \end{verbatim}
*/
int SdioRegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile SDIO registers */
      SDIO_TypeDef sdio=(*SDIO);
      
      /* report the SDIO registers */
      fprintf(dest,"[SDIO base: 0x%08lx] %8d %11d %12d %11d\n",(uint32_t)SDIO,3,2,1,0);
      fprintf(dest,"Offset SdReg %14s   1098 7654 3210 9876 5432 1098 7654 3210\n","Hexadecimal");
      fprintf(dest,"---------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] POWER   = 0x%08lx : %s\n", sdio.POWER,   uint32bits(sdio.POWER));
      fprintf(dest,"[0x04] CLKCR   = 0x%08lx : %s\n", sdio.CLKCR,   uint32bits(sdio.CLKCR));
      fprintf(dest,"[0x08] ARG     = 0x%08lx : %s\n", sdio.ARG,     uint32bits(sdio.ARG));
      fprintf(dest,"[0x0c] CMD     = 0x%08lx : %s\n", sdio.CMD,     uint32bits(sdio.CMD));
      fprintf(dest,"[0x10] RESPCMD = 0x%08lx : %s\n", sdio.RESPCMD, uint32bits(sdio.RESPCMD));
      fprintf(dest,"[0x14] RESP1   = 0x%08lx : %s\n", sdio.RESP1,   uint32bits(sdio.RESP1));
      fprintf(dest,"[0x18] RESP2   = 0x%08lx : %s\n", sdio.RESP2,   uint32bits(sdio.RESP2));
      fprintf(dest,"[0x1c] RESP3   = 0x%08lx : %s\n", sdio.RESP3,   uint32bits(sdio.RESP3));
      fprintf(dest,"[0x20] RESP4   = 0x%08lx : %s\n", sdio.RESP4,   uint32bits(sdio.RESP4));
      fprintf(dest,"[0x24] DTIMER  = 0x%08lx : %s\n", sdio.DTIMER,  uint32bits(sdio.DTIMER));
      fprintf(dest,"[0x28] DLEN    = 0x%08lx : %s\n", sdio.DLEN,    uint32bits(sdio.DLEN));
      fprintf(dest,"[0x2c] DCTRL   = 0x%08lx : %s\n", sdio.DCTRL,   uint32bits(sdio.DCTRL));
      fprintf(dest,"[0x30] DCOUNT  = 0x%08lx : %s\n", sdio.DCOUNT,  uint32bits(sdio.DCOUNT));
      fprintf(dest,"[0x34] STA     = 0x%08lx : %s\n", sdio.STA,     uint32bits(sdio.STA));
      fprintf(dest,"[0x38] ICR     = 0x%08lx : %s\n", sdio.ICR,     uint32bits(sdio.ICR));
      fprintf(dest,"[0x3c] MASK    = 0x%08lx : %s\n", sdio.MASK,    uint32bits(sdio.MASK));
      fprintf(dest,"[0x48] FIFOCNT = 0x%08lx : %s\n", sdio.FIFOCNT, uint32bits(sdio.FIFOCNT));
      fprintf(dest,"[0x80] FIFO    = 0x%08lx : %s\n", sdio.FIFO,    uint32bits(sdio.FIFO));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in SPI1 registers    */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   of the SPI1 registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int Spi1RegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile SPI1 registers */
      SPI_TypeDef Spi1Reg=(*SPI1);

      /* report the SPI1 registers */
      fprintf(dest,"[SPI1 base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)SPI1,3,2,1,0);
      fprintf(dest,"Offset %4sReg %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Spi1","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CR1      = 0x%08lx : %s\n", Spi1Reg.CR1,     uint32bits(Spi1Reg.CR1));
      fprintf(dest,"[0x04] CR2      = 0x%08lx : %s\n", Spi1Reg.CR2,     uint32bits(Spi1Reg.CR2));
      fprintf(dest,"[0x08] SR       = 0x%08lx : %s\n", Spi1Reg.SR,      uint32bits(Spi1Reg.SR));
      fprintf(dest,"[0x0c] DR       = 0x%s : %s\n",    "????????",      "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x10] CRCPR    = 0x%08lx : %s\n", Spi1Reg.CRCPR,   uint32bits(Spi1Reg.CRCPR));
      fprintf(dest,"[0x14] RXCRCR   = 0x%08lx : %s\n", Spi1Reg.RXCRCR,  uint32bits(Spi1Reg.RXCRCR));
      fprintf(dest,"[0x18] TXCRCR   = 0x%08lx : %s\n", Spi1Reg.TXCRCR,  uint32bits(Spi1Reg.TXCRCR));
      fprintf(dest,"[0x1c] I2SCFGR  = 0x%08lx : %s\n", Spi1Reg.I2SCFGR, uint32bits(Spi1Reg.I2SCFGR));
      fprintf(dest,"[0x20] I2SPR    = 0x%08lx : %s\n", Spi1Reg.I2SPR,   uint32bits(Spi1Reg.I2SPR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}

   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in SPI2 registers    */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   of the SPI2 registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int Spi2RegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile SPI2 registers */
      SPI_TypeDef Spi2Reg=(*SPI2);

      /* report the SPI2 registers */
      fprintf(dest,"[SPI2 base: 0x%08lx] %9d %11d %12d %11d\n",(uint32_t)SPI2,3,2,1,0);
      fprintf(dest,"Offset %4sReg %13s   1098 7654 3210 9876 5432 1098 7654 3210\n","Spi2","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] CR1      = 0x%08lx : %s\n", Spi2Reg.CR1,     uint32bits(Spi2Reg.CR1));
      fprintf(dest,"[0x04] CR2      = 0x%08lx : %s\n", Spi2Reg.CR2,     uint32bits(Spi2Reg.CR2));
      fprintf(dest,"[0x08] SR       = 0x%08lx : %s\n", Spi2Reg.SR,      uint32bits(Spi2Reg.SR));
      fprintf(dest,"[0x0c] DR       = 0x%s : %s\n",    "????????",      "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x10] CRCPR    = 0x%08lx : %s\n", Spi2Reg.CRCPR,   uint32bits(Spi2Reg.CRCPR));
      fprintf(dest,"[0x14] RXCRCR   = 0x%08lx : %s\n", Spi2Reg.RXCRCR,  uint32bits(Spi2Reg.RXCRCR));
      fprintf(dest,"[0x18] TXCRCR   = 0x%08lx : %s\n", Spi2Reg.TXCRCR,  uint32bits(Spi2Reg.TXCRCR));
      fprintf(dest,"[0x1c] I2SCFGR  = 0x%08lx : %s\n", Spi2Reg.I2SCFGR, uint32bits(Spi2Reg.I2SCFGR));
      fprintf(dest,"[0x20] I2SPR    = 0x%08lx : %s\n", Spi2Reg.I2SPR,   uint32bits(Spi2Reg.I2SPR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}

   return status;
}

/*------------------------------------------------------------------------*/
/* create register map showing the state of each bit in USART1 registers  */
/*------------------------------------------------------------------------*/
/**
   This function creates a register map showing the state of each bit
   in the USART1 registers of the Stm32f103xg processor.

      \begin{verbatim}
      input:
         dest....A FILE into which the report will be written.

      output:

         This function returns a positive integer if successful or
         zero if it failed.  A negative value is returned if an
         exception was encountered
      \end{verbatim}
*/
int Usart1RegMap(FILE *dest)
{
   /* initialize the return value */
   int status=RegOk;

   /* validate the function argument */
   if (dest)
   {
      /* take a snapshot of the volatile USART1 registers */
      USART_TypeDef Usart1Reg=(*USART1);

      /* report the USART1 registers */
      fprintf(dest,"[USART1 base: 0x%08lx] %7d %11d %12d %11d\n",(uint32_t)USART1,3,2,1,0);
      fprintf(dest,"Offset %5sReg %11s   1098 7654 3210 9876 5432 1098 7654 3210\n","Usart1","Hexadecimal");
      fprintf(dest,"----------------------------------------------------------------------\n");
      fprintf(dest,"[0x00] SR       = 0x%08lx : %s\n", Usart1Reg.SR,       uint32bits(Usart1Reg.SR));
      fprintf(dest,"[0x04] DR       = 0x%s : %s\n",    "????????",         "???? ???? ???? ???? ???? ???? ???? ????");
      fprintf(dest,"[0x08] BRR      = 0x%08lx : %s\n", Usart1Reg.BRR,      uint32bits(Usart1Reg.BRR));
      fprintf(dest,"[0x0c] CR1      = 0x%08lx : %s\n", Usart1Reg.CR1,      uint32bits(Usart1Reg.CR1));
      fprintf(dest,"[0x10] CR2      = 0x%08lx : %s\n", Usart1Reg.CR2,      uint32bits(Usart1Reg.CR2));
      fprintf(dest,"[0x14] CR3      = 0x%08lx : %s\n", Usart1Reg.CR3,      uint32bits(Usart1Reg.CR3));
      fprintf(dest,"[0x18] GTPR     = 0x%08lx : %s\n", Usart1Reg.GTPR,     uint32bits(Usart1Reg.GTPR));
   }

   /* indicate that the function argument is NULL */
   else {status=RegNull;}
   
   return status;
}

#endif /* STM32F103REG_C */
