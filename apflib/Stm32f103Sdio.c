#ifndef STM32F103SDIO_H
#define STM32F103SDIO_H (0x2000) 

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 SDIO model
 * -------------------------------------------------
 *
 * The SwiftWare API to the STM32 SDIO peripheral is based on the SD
 * Specifications Part 1 Physical Layer Simplified Specification
 * Version 6.00 (PLSSv6) dated April 10, 2017.  The PLSSv6 is
 * invaluable as a reference and having a copy immediately available
 * is essential to understand how this API works.  However, the PLSSv6
 * can not be used as a tutorial for learning SDIO communications.  I
 * have not found a good tutorial for learning about SDIO.  I have
 * been forced to rely on a patchwork of snippets from the web
 * together with copious experimentation in order to build a
 * conceptual model of SDIO communications.
 *
 * This API implements a simplified model whereby all read, write, and
 * erase functions operate on a fixed block-size of 512 bytes.  This
 * quantum of memory allows for addressing up to 2^32 blocks which
 * represents 2TB (ie., 2^41 bytes) of storage.  For this reason, only
 * high-capacity SD (HCSD) cards are supported by this API because,
 * according to the PLSSv6, all HCSD cards address memory operations
 * in fixed units of 512-byte blocks.  Hence, HDSC cards can address
 * up to 2TB of storage.
 *
 * MultiMedia cards (MMC) tend to be older technology with less
 * storage capacity; they require a more complicated API and so can
 * not be used with this API. MMCs will be detected and rejected
 * during the initialization procedure.
 *
 * According to the PLSSv6, the initialization phase must operate at
 * clock speeds less than 400kHz; after the SD card has been assigned
 * an RCA then the clock speed can be increased up to 3/8 the speed of
 * HCLK/2.  The SdioInit.CLKCR object configures the clock divisor to
 * be 198 (0xc3) so that SDIO_CK = HCLK/(198+2) = 8MHz/200 = 40kHz
 * during the card identification phase.  After the card
 * identification phase is complete, the clock divisor is reduced to 2
 * so that SDIO_CK = HCLK/(2+2) = 8MHz/4 = 2MHz.
 *
 *    2.11.6 No underrun detection with wrong data transmission
 *    Description
 *    In case there is an ongoing data transfer from the SDIO host to
 *    the SD card and the hardware flow control is disabled (bit 14 of
 *    the SDIO_CLKCR is not set), if an underrun condition occurs, the
 *    controller may transmit a corrupted data block (with wrong data
 *    word) without detecting the underrun condition when the clock
 *    frequencies have the following relationship:
 *
 *       [3 x period(PCLK2) + 3 x period(SDIOCLK)] >=
 *                                 (32 / (BusWidth)) x period(SDIO_CK)
 *
 *    Workaround
 *    Avoid the above-mentioned clock frequency relationship, by:
 *    + Incrementing the APB frequency
 *    + or decreasing the transfer bandwidth
 *    + or reducing SDIO_CK frequency
 *
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103SdioChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <stdint.h>

/*------------------------------------------------------------------------*/
/* structure to contain SD card data required for SDIO operations         */
/*------------------------------------------------------------------------*/
typedef struct
{
   uint8_t  Cid[16]; /* Card Identification register contents */
   uint8_t  Csd[16]; /* Card Specific Data register contents  */
   uint16_t Rca;     /* relative card address                 */
} SdCardInfo;

/* enumerate the states of the Card Programming State Machine (CPSM) */
typedef enum {SdInvalid=-1,SdIdle,SdRdy,SdIdent,SdStdby,SdTran,
              SdSnd,SdRcv,SdPrg,SdDis,SdNSTATE} SdCardState;

/* type definition of an SDIO transfer block */
typedef union {uint8_t b[512]; uint32_t w[128];} SdBlk;

/* prototypes for Apf11 functions with external linkage */
int Stm32f103SdBlockRead(const SdCardInfo *sdcard, uint32_t addr, SdBlk *blk);
int Stm32f103SdBlockReadM(const SdCardInfo *sdcard, uint32_t addr, uint32_t blkcnt, SdBlk *blk);
int Stm32f103SdBlockWrite(const SdCardInfo *sdcard, uint32_t addr, SdBlk *blk);
int Stm32f103SdBlockWriteM(const SdCardInfo *sdcard, uint32_t addr, uint32_t blkcnt, SdBlk *blk);
int Stm32f103SdCardDetect(void);
int Stm32f103SdCardIsBusy(const SdCardInfo *sdcard);
int Stm32f103SdCardState(const SdCardInfo *sdcard);
int Stm32f103SdCardStatus(const SdCardInfo *sdcard, uint32_t *cstatus);
int Stm32f103SdCid(SdCardInfo *sdcard);
int Stm32f103SdClockEnable(void);
int Stm32f103SdClockDisable(void);
int Stm32f103SdErase(const SdCardInfo *sdcard, uint32_t StartBlk, uint32_t EndBlk);
int Stm32f103SdIsPowered(SdCardInfo *sdcard);
int Stm32f103SdPowerDown(SdCardInfo *sdcard);
int Stm32f103SdPowerUp(SdCardInfo *sdcard);
int Stm32f103SdRegModify(volatile uint32_t *addr, uint32_t BitsToClear, uint32_t BitsToAssert);
int Stm32f103SdRegRead(volatile uint32_t *addr, uint32_t *value);
int Stm32f103SdRegWrite(volatile uint32_t *addr, uint32_t value);
int Stm32f103SdStateStandby(const SdCardInfo *sdcard);
int Stm32f103SdStateTransfer(const SdCardInfo *sdcard);
int Stm32f103SdStopTransfer(const SdCardInfo *sdcard);

/* prototypes for non-Apf11 functions with external linkage */
int     SdCardInfoInit(const SdCardInfo *sdcard);
int     SdCid(const SdCardInfo *sdcard, uint8_t *manid, uint16_t *oemid,
              char *name, uint8_t *rev, uint32_t *serno, uint16_t *year,
              uint8_t *month, uint8_t *crc);
uint8_t SdCrc7(const uint8_t message[], uint16_t length);
uint8_t SdCrc7t(const uint8_t message[], uint16_t length);
int     SdCsd(const SdCardInfo *sdcard, int32_t *csize, int8_t *version,
              int16_t *ccc, int8_t *eblklen, int8_t *rblklen,
              int8_t *wblklen, int8_t *wprot, uint8_t *crc);

/* bitmasks for errors in the SD status register */
#define SdStatusAddrOutOfRange      (0x80000000LU)
#define SdStatusAddrMisaligned      (0x40000000LU)
#define SdStatusBlockLenErr         (0x20000000LU)
#define SdStatusEraseSeqErr         (0x10000000LU)
#define SdStatusBadEraseParam       (0x08000000LU)
#define SdStatusWriteProtViolation  (0x04000000LU)
#define SdStatusCardLocked          (0x02000000LU)
#define SdStatusLockUnlockFailed    (0x01000000LU)
#define SdStatusComCrcFailed        (0x00800000LU)
#define SdStatusIllegalCmd          (0x00400000LU)
#define SdStatusCardEccFailed       (0x00200000LU)
#define SdStatusCcError             (0x00100000LU)
#define SdStatusGeneralUnknownError (0x00080000LU)
#define SdStatusStreamReadUnderrun  (0x00040000LU)
#define SdStatusStreamWriteOverrun  (0x00020000LU)
#define SdStatusCidCsdOverwrite     (0x00010000LU)
#define SdStatusWpEraseSkip         (0x00008000LU)
#define SdStatusCardEccDisabled     (0x00004000LU)
#define SdStatusEraseReset          (0x00002000LU)
#define SdStatusReady               (0x00000100LU)
#define SdStatusAkeSeqError         (0x00000008LU)

/* define the return states of the Stm32 SDIO API */
extern const char SdioOcrErr;  /* OCR error              */
extern const char SdioCrcFail; /* CRC failure            */
extern const char SdioBusy;    /* indicate busy timout   */
extern const char SdioTimeOut; /* indicate timeout       */
extern const char SdioInvalid; /* invalid parameter      */
extern const char SdioNull;    /* NULL function argument */
extern const char SdioFail;    /* general failure        */
extern const char SdioOk;      /* general success        */

#endif /* STM32F103SDIO_H */
#ifdef STM32F103SDIO_C
#undef STM32F103SDIO_C

#include <apf11.h>
#include <assert.h>
#include <limits.h>
#include <logger.h>
#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Reg.h>
#include <string.h>
#include <time.h>

/* define the return states of the Stm32 SDIO API */
const char SdioOcrErr  =  -6; /* OCR error              */
const char SdioCrcFail =  -5; /* CRC failure            */
const char SdioBusy    =  -4; /* indicate timeout       */
const char SdioTimeOut =  -3; /* indicate timeout       */
const char SdioInvalid =  -2; /* invalid parameter      */
const char SdioNull    =  -1; /* NULL function argument */
const char SdioFail    =   0; /* general failure        */
const char SdioOk      =   1; /* general success        */

/* initialize a constant pointer to the SDIO interface */
static SDIO_TypeDef *const sdio=SDIO;

/* use inline assembly NOPs to create a time delay of nine processor cycles */
#define Delay() {asm volatile("NOP"); asm volatile("NOP"); asm volatile("NOP");\
                 asm volatile("NOP"); asm volatile("NOP"); asm volatile("NOP");\
                 asm volatile("NOP"); asm volatile("NOP"); asm volatile("NOP");}

/* declare external functions for local use */
int Wait(unsigned long int millisec);

/* declare functions with static linkage */
static int Stm32f103SdCsd(SdCardInfo *sdcard);
static int Stm32f103SdInit(void);
static int Stm32f103SdRca(SdCardInfo *sdcard);
static int Stm32f103SdReset(void);
static int Stm32f103SdR1(uint8_t cmd,uint32_t *cstatus);
static int Stm32f103SdR1b(const SdCardInfo *sdcard, uint8_t cmd,
                          uint32_t msTimeOut, uint32_t *cstatus);
static int Stm32f103SdR2(void);
static int Stm32f103SdR3(void);
static int Stm32f103SdR6(uint8_t cmd);
static int Stm32f103SdSendCmd(uint32_t cmd, uint32_t arg);
static int Stm32f103SdV2(void);

/* bitmasks for Stm32f103 SDIO clock control register */
#define SdClockEnable    (0x00000100LU)
#define SdHwFlowControl  (0x00004000LU)
#define SdClockHighSpeed (0x00000002LU)

/* bitmasks for Stm32f103 SDIO STA register */
#define SdCCrcFail    (0x00000001LU)
#define SdDCrcFail    (0x00000002LU)
#define SdCTimeOut    (0x00000004LU)
#define SdDTimeOut    (0x00000008LU)
#define SdTxUnderR    (0x00000010LU)
#define SdRxOverR     (0x00000020LU)
#define SdCmdREnd     (0x00000040LU)
#define SdCmdSent     (0x00000080LU)
#define SdDataEnd     (0x00000100LU)
#define SdStBitErr    (0x00000200LU)
#define SdDBckEnd     (0x00000400LU)
#define SdCmdAck      (0x00000800LU)
#define SdTxAct       (0x00001000LU)
#define SdRxAct       (0x00002000LU)
#define SdTxFifoHe    (0x00004000LU)
#define SdRxFifoHf    (0x00008000LU)
#define SdTxFifoF     (0x00010000LU)
#define SdRxFifoF     (0x00020000LU)
#define SdTxFifoE     (0x00040000LU)
#define SdRxFifoE     (0x00080000LU)
#define SdTxDAvl      (0x00100000LU)
#define SdRxDAvl      (0x00200000LU)
#define SdSdioIt      (0x00400000LU)
#define SdCeAtaEnd    (0x00800000LU)
#define SdStaticFlags (0x000005ffLU)

/* bitmasks for Stm32f103 SDIO CMD register */
#define Cmd0               (0)            /* resets the SD card                  */
#define Cmd2               (2)            /* requests CID of the SD card         */
#define Cmd3               (3)            /* requests RCA of the SD card         */
#define Cmd7               (7)            /* selects an SD card by RCA           */
#define Cmd8               (8)            /* resets the SD card                  */
#define Cmd9               (9)            /* requests CSD of the SD card         */
#define Cmd12              (12)           /* command to stop data transmission   */
#define Cmd13              (13)           /* requests SD card's status register  */
#define Cmd16              (16)           /* sets the block length for transfers */
#define Cmd17              (17)           /* command to read a single block      */
#define Cmd18              (18)           /* command to read multiple blocks     */
#define Cmd24              (24)           /* command to write a single block     */
#define Cmd25              (25)           /* command to write multiple blocks    */
#define Cmd32              (32)           /* set the start-block for erasures    */
#define Cmd33              (33)           /* set the end-block for erasures      */
#define Cmd38              (38)           /* erase command                       */
#define ACmd41             (41)           /* application command                 */
#define Cmd55              (55)           /* prelude to application command      */
#define Cmd_WaitResp       (0x00000040LU) /* response expected                   */
#define Cmd_WaitResp_Long  (0x000000c0LU) /* long response                       */
#define Cmd_WaitInt        (0x00000100LU) /* CPSM waits for interrupt request    */
#define Cmd_WaitPend       (0x00000200LU) /* CPSM waits for end of data transfer */
#define Cmd_CpsmEn         (0x00000400LU) /* Enable the CPSM                     */
#define Cmd_Suspend        (0x00000800LU) /* SD I/O suspend command              */
#define Cmd_EnCmdComp      (0x00001000LU) /* Enable command completion           */
#define Cmd_nIen           (0x00002000LU) /* Disable Interrupt in CE-ATA         */
#define AtaCmd             (0x00004000LU) /* CPSM transfers CMD61                */

/*------------------------------------------------------------------------*/
/* SDIO cluster of read/write registers                                   */
/*------------------------------------------------------------------------*/
/**
   There are a total of sixty-three registers in the SDIO cluster but
   only nine are writeable; these are enumerated in the SDioCfgRegs
   structure.  There are 49 readable registers in the SDIO cluster;
   the remaining 14 are reserved and neither readable nor writeable.
*/
typedef struct
{
   uint32_t POWER;
   uint32_t CLKCR;
   uint32_t ARG;
   uint32_t CMD;
   uint32_t DTIMER;
   uint32_t DLEN;
   uint32_t DCTRL;
   uint32_t ICR;
   uint32_t MASK;
} SdioCfgRegs;

/*------------------------------------------------------------------------*/
/* 32-bit templates for writing/modifying to SDIO registers               */
/*------------------------------------------------------------------------*/
/**
   There are a total of sixty-three registers in the SDIO cluster but
   only nine are writeable; these are enumerated in the SDioCfgRegs
   structure above.  The template for a read/write (RW) SDIO register
   specifies which bits of the register are permitted to be modified
   from their reset value of zero.  Asserted bits in the members of
   the SdioTemplate object represent the writeable bits of each RW
   register; zero bits represent bits that are reserved and are not
   allowed to change.
*/
static const SdioCfgRegs SdioTemplate = {0x00000003UL, 0x00007fffUL, 0xffffffffUL,
                                         0x00007fffUL, 0xffffffffUL, 0x01ffffffUL,
                                         0x00000fffUL, 0x00c007ffUL, 0x00ffffffUL};

/*------------------------------------------------------------------------*/
/* 32-bit initializations for RW SDIO registers                           */
/*------------------------------------------------------------------------*/
static const SdioCfgRegs SdioInit = {0x00000003UL, 0x000000c3LU, 0x00000000LU,
                                     0x00000000LU, 0x00000000LU, 0x00000000LU,
                                     0x00000000LU, 0x00000000LU, 0x00000000LU};

/*------------------------------------------------------------------------*/
/* function to initialize the SdCardInfo structure                        */
/*------------------------------------------------------------------------*/
/**
   This function initializes the SdCardInfo structure to be all zeros.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int SdCardInfoInit(const SdCardInfo *sdcard)
{
   /* initialize the SdCardInfo structure with zeros */
   memset((void *)sdcard,0,sizeof(SdCardInfo));
   
   return SdioOk;
}

/*------------------------------------------------------------------------*/
/* function to extract metadata from the SD card's CID register           */
/*------------------------------------------------------------------------*/
/**
   This function extracts eight elements of metadata from the SD
   card's Card Identification (CID) register. 

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CID register contents.

   output:
      manid...An 8-bit manufacturer's identifier which is unique to
              each manufacturer.
      oemid...A 16-bit OEM application identifier.
      name....A 5-byte ascii string that identifies the product name.
      rev.....An 8-bit encoded product revision of the form N.M where
              the N,M are stored in the most,least significant nibble,
              respectively.
      serno...A 32-bit serial number that is unique to each SD card.
      year....The year that the SD card was manufactured.
      month...The month that the SD card was manufactured.
      crc.....A 7-bit CRC of the most significant 15-bytes of CID. 

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int SdCid(const SdCardInfo *sdcard, uint8_t *manid, uint16_t *oemid,
          char *name, uint8_t *rev, uint32_t *serno, uint16_t *year,
          uint8_t *month, uint8_t *crc)
{
   /* define the logging signature */
   cc *FuncName="SdCid()";

   /* initialize the return value */
   int n,status=SdioOk;

   /* initialize all non-NULL arguments */
   if (manid) {(*manid)=0;}
   if (oemid) {(*oemid)=0;}
   if (name)  {(*name)=0;}
   if (rev)   {(*rev)=0;}
   if (serno) {(*serno)=0;}
   if (year)  {(*year)=0;}
   if (month) {(*month)=0;}
   if (crc)   {(*crc)=0;}
      
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* validate the CID by confirming the CRC */
   else if (SdCrc7(sdcard->Cid+1,sizeof(sdcard->Cid)-1)!=(sdcard->Cid[0]>>1))
   {
      /* log the CRC failure */
      LogEntry(FuncName,"CRC check failed for the CID register.\n");

      /* indicate failure */
      status=SdioCrcFail;
   }

   else
   {
      /* extract the manufacturer's id from the CID */
      if (manid) {(*manid) = sdcard->Cid[15];}

      /* extract the OEM application id from the CID */
      if (oemid) {(*oemid) = (sdcard->Cid[14]<<8)|sdcard->Cid[13];}

      /* extract the product name from the CID */
      if (name)  {for (n=0; n<5; n++) {name[n]=sdcard->Cid[12-n];} name[5]=0;}

      /* extract the product revision from the CID */
      if (rev)   {(*rev) = sdcard->Cid[7];}

      /* extract the product serial number from the CID */
      if (serno) {for ((*serno)=0, n=3; n>=0; n--) {(*serno)|=(sdcard->Cid[3+n]<<(8*n));}}

      /* extract the date code from the CID */
      if (month) {(*month) = (sdcard->Cid[1]&0x0f);}

      /* extract the date code from the CID */
      if (year)  {(*year) = (((sdcard->Cid[1]&0xf0)>>4) | ((sdcard->Cid[2]&0x0f)<<4)) + 2000;}

      /* extract the crc from the CID */
      if (crc)   {(*crc) = ((sdcard->Cid[0])>>1);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to compute the 7-bit CRC used by the SD peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function computes 7-bit CRC used by the SD peripheral.  

   \begin{verbatim}
   input:
      message...The array of bytes for which the CRC should be
                computed.
      length....The number of bytes in the message array.

   output:
      This function returns the 7-bit CRC associated with the message
      string that is the function argument.
   \end{verbatim
*/
uint8_t SdCrc7(const uint8_t *message, uint16_t length)
{
   /* generator polynomial for the SD 7-bit crc */
   const uint8_t CrcPoly = 0x89;

   /* declare local loop varialbes and crc */
   int16_t n,j; uint8_t crc=0;

   /* apply crc algorithm to each byte in the message */
   for (crc=0, n=(length-1); n>=0; n--)
   { 
      crc = (crc<<1)^message[n];    if (crc&0x80) {crc^=CrcPoly;}
      for (j=1; j<8; j++) {crc<<=1; if (crc&0x80) {crc^=CrcPoly;}}
   }
   
   return crc;
}

/*------------------------------------------------------------------------*/
/* function to compute the 7-bit CRC used by the SD peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function computes 7-bit CRC used by the SD peripheral.  

   \begin{verbatim}
   input:
      message...The array of bytes for which the CRC should be
                computed.
      length....The number of bytes in the message array.

   output:
      This function returns the 7-bit CRC associated with the message
      string that is the function argument.
   \end{verbatim
*/
uint8_t SdCrc7t(const uint8_t message[], uint16_t length)
{
   /* CRC polynomial for the SD peripheral */
   const uint8_t CrcPoly = 0x89;

   /* declare loop and CRC variables */
   uint16_t i,j; uint8_t CrcTable[256],crc=0;

   /* initialize for a table-driven CRC algorithm */
   for (i=0; i<256; i++)
   {
      CrcTable[i] = (i & 0x80) ? (i ^ CrcPoly) : i;

      for (j=1; j<8; j++) 
      {
        CrcTable[i] <<= 1;
        if (CrcTable[i] & 0x80) {CrcTable[i] ^= CrcPoly;}
      }
   }

   /* process each byte of the message */
   for (i=0; i<length; i++) {crc = CrcTable[(crc << 1) ^ message[i]];}
   
   return crc;
}

/*------------------------------------------------------------------------*/
/* function to extract metadata from the SD card's CSD register           */
/*------------------------------------------------------------------------*/
/**
   This function extracts seven elements of metadata from the SD
   card's Card Specific Data (CSD) register. 

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CSD register contents.

   output:
      csize.....This is the size of the SD card expressed as the number
                of 512-byte blocks.
      version...This is the CSD version number which will always be
                0x01 for high capacity SD cards.
      ccc.......The bitmask that encodes the card command classes.
      eblklen...This is the block length for erasures.  This will
                always be 1 for high capacity SD cards indicating that
                erasures will be in units of 512-byte blocks.
      rblklen...This is the block length for reading data from from
                the SD cards.  For high capacity cards, data will be
                read from the card in blocks of 512 bytes.
      wblklen...This is the block length for writing data from from
                the SD cards.  For high capacity cards, data will be
                written to the card in blocks of 512 bytes.
      wprot.....The two least significant bits indicate the state of
                write protection for the SD cards.  If both bits are
                zero then the SD card is writeable.  If the 0x2 bit is
                asserted then the SD card is permanently write
                protected.  If the 0x1 bit is asserted then the SD
                card is temporarily write protected; clearing this bit
                renders the SD card writeable again.
      crc.......A 7-bit CRC of the most significant 15-bytes of CID. 

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int SdCsd(const SdCardInfo *sdcard, int32_t *csize, int8_t *version,
          int16_t *ccc, int8_t *eblklen, int8_t *rblklen,
          int8_t *wblklen, int8_t *wprot, uint8_t *crc)
{
   /* define the logging signature */
   cc *FuncName="SdCsd()";

   /* initialize the return value */
   int status=SdioOk;

   /* initialize all non-NULL arguments */
   if (csize)   {(*csize)=-1;}
   if (version) {(*version)=-1;}
   if (ccc)     {(*ccc)=0;}
   if (eblklen) {(*eblklen)=-1;}
   if (rblklen) {(*rblklen)=-1;}
   if (wblklen) {(*wblklen)=-1;}
   if (wprot)   {(*wprot)=-1;}
   if (crc)     {(*crc)=0;}
         
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* validate the CSD by confirming the CRC */
   else if (SdCrc7(sdcard->Csd+1,sizeof(sdcard->Csd)-1)!=(sdcard->Csd[0]>>1))
   {
      /* log the CRC failure */
      LogEntry(FuncName,"CRC check failed for the CSD register.\n");

      /* indicate failure */
      status=SdioCrcFail;
   }

   /* validate that the SD card's CSD register is version 2 */
   else if (((sdcard->Csd[15])&0xc0)!=0x40)
   {
      /* log the version failure */
      LogEntry(FuncName,"SD card is not high capacity; "
               "CSD not V2: 0x%02x.\n",((sdcard->Csd[15])>>6));

      /* indicate failure */
      status=SdioInvalid;
   }
   
   else
   {
      /* check for extraction of SD card size */
      if (csize)
      {
         /* extract SD card size from the CSD register contents */         
         (*csize) = ((sdcard->Csd[8]&0x3f)<<16) | ((sdcard->Csd[7])<<8) | sdcard->Csd[6];
            
         /* compute the number of 512-byte blocks */
         (*csize) *= 1024L;
      }

      /* extract the version of the CSD structure */
      if (version) {(*version) = (sdcard->Csd[15]>>6);}

      /* extract the card command classes from the CSD */
      if (ccc) {(*ccc) = ((sdcard->Csd[11])<<4) | (((sdcard->Csd[10])&0xf0)>>4);}
      
      /* extract the read block length */
      if (rblklen) {(*rblklen) = (sdcard->Csd[10]&0x0f);}

      /* extract the erase block enable flag */
      if (eblklen) {(*eblklen) = (sdcard->Csd[5]&0x20)>>5;}

      /* extract the write block length */
      if (wblklen) {(*wblklen) = ((sdcard->Csd[2]&0xc0)>>6) | ((sdcard->Csd[3]&0x03)<<2);}

      /* extract the write protection flags */
      if (wprot) {(*wprot) = ((sdcard->Csd[1]&0x03)>>2);}    
            
      /* extract the crc from the CSD */
      if (crc) {(*crc) = ((sdcard->Csd[0])>>1);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read a single 512-byte block of data from the SD card      */
/*------------------------------------------------------------------------*/
/**
   This function reads a single 512-byte block from a user-specified
   address of a high capacity SD (HCSD) card.

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.
      addr.....The block-address in the range [0,csize] from which the
               512-byte block will be read.

   output:
      blk......The 512-byte block on the HCSD into which the data will
               be written.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdBlockRead(const SdCardInfo *sdcard, uint32_t addr, SdBlk *blk)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdBlockRead()";
   
   /* initialize the return value */
   int status=SdioFail;
   
   /* define local objects needed for write operations */
   int32_t csize; int16_t n,err,ccc; int8_t wprot;
   uint32_t cstatus; SdCardState state=SdInvalid;

   /* define local objects needed for timeout features */
   const uint32_t msTimeOut=1000; uint32_t msTimeRef;
   
   /* verify the function argument */
   if (!sdcard || !blk)
   {
      /* log the error */
      LogEntry(FuncName,"NULL function parameter.\n"); status=SdioNull;
   }

   /* decode the card specific data (CSD) */
   else if ((err=SdCsd(sdcard,&csize,NULL,&ccc,NULL,NULL,NULL,&wprot,NULL)<=0))
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to decode CSD failed. [err=%d]\n",err); 
   }

   /* congruency check for start and end blocks of the read operation */
   else if (addr<0 || addr>csize)
   {
      /* log the congruency failure */
      LogEntry(FuncName,"Congruency failure: 0<=addr(%lu)<=SdSize(%lu)\n",
               addr,csize); status=SdioInvalid;
   }

   /* initialize by inducing the CPSM into standby state */
   else if ((err=Stm32f103SdStateStandby(sdcard))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to induce standby state. [err=%d]\n",err);
   }
   
   /* induce the SD card into transfer state */
   else if ((err=Stm32f103SdStateTransfer(sdcard))>0) 
   {
      /* DCTRL register bitmask for 512-byte blocks and transfer enable */
      #define DCtrlMode (0x00000093LU)

      /* initialize the data control register */
      sdio->DCTRL=0;
                     
      /* initialize the block-length and timeout of the SD transfer */
      sdio->DTIMER=0x07f00000LU; sdio->DLEN=sizeof(SdBlk);

      /* verify that the FIFO starts empty */
      if (sdio->FIFOCNT) {LogEntry(FuncName,"Orphaned Rx FIFO with %d "
                                   "bytes.\n",(4*sdio->FIFOCNT));}
                  
      /* initialize the DCTRL register for 512-byte blocks and enable transfer mode */
      else if ((err=Stm32f103SdRegModify( &(sdio->DCTRL), SdioTemplate.DCTRL, DCtrlMode ))>0)
      {
         /* execute CMD17 to read a single block from the specified address */
         if ((err=Stm32f103SdSendCmd(Cmd17|Cmd_WaitResp|Cmd_CpsmEn,addr))>0) 
         {
            /* validate the response to the read-single-block command */
            if ((err=Stm32f103SdR1(Cmd17,&cstatus))>0) 
            {
               /* bitmask for termination of transfer from the Rx fifo */
               #define SdErrors (SdDCrcFail | SdDTimeOut | SdRxOverR | SdStBitErr)
                  
               /* compute the number of 32-bit words */
               const uint8_t N = (sizeof(SdBlk)/sizeof(uint32_t));
            
               /* loop to transfer the block from the Rx fifo */
               for (msTimeRef=msTimer(0), n=0; n<N && msTimer(msTimeRef)<msTimeOut;)
               {
                  /* check for errors prior to reading from the Rx fifo */
                  if ((cstatus=sdio->STA)&SdErrors) {err=SdioFail; break;}
                  
                  /* transfer the 512-byte block from the Rx fifo in units of 32-bit words */
                  else if (cstatus&SdRxDAvl) {blk->w[n++] = sdio->FIFO;}
               }

               /* loop to wait for transfer of data from the SD card */
               if (err>0) for (status=SdioTimeOut; msTimer(msTimeRef)<msTimeOut;)
               {
                  /* check for active transfer of data from the SD card */
                  if (sdio->STA&SdRxAct || sdio->STA&SdRxDAvl) {continue;}
                     
                  /* check if the block was successfully transferred from the Rx fifo */
                  if ((sdio->STA&SdDataEnd) && (sdio->STA&SdDBckEnd))
                  {
                     /* query the CPSM state */
                     state=Stm32f103SdCardState(sdcard);
                     
                     /* exit the loop if the CPSM is in standby or transfer state */
                     if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check for nonrecoverable errors */
               else {LogEntry(FuncName,"Block transfer from Rx fifo of SD periperal "
                              "failed. [sdio->STA=0x%08lx]\n",cstatus);}

               /* log the failed attempt for the SD read operation */
               if (status<=0) {LogEntry(FuncName,"Read failed from block address 0x%08lu. "
                                        "[status=%d,state=%d,msTimer=%lumsec]\n",addr,
                                        status,state,msTimer(msTimeRef));}

               /* check criteria for logging the successful write operation */
               else if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
               {
                  /* log the successful read operation */
                  LogEntry(FuncName,"Read successful from block address 0x%08lu. "
                           "[status=%d,state=%d,msTimer=%lumsec]\n",addr,status,
                           state,msTimer(msTimeRef));
               }
            }
         
            /* log the failure of the block-read command */
            else {LogEntry(FuncName,"Failed to read block to address 0x%08lx. "
                           "[err=%d,cstatus=0x%08lx]\n",addr,err,cstatus);}
         }
      
         /* log the failure */
         else {LogEntry(FuncName,"Attempt to send Cmd17 failed. [err=%d]\n",err);}
      }
      
      /* log the failure */
      else {LogEntry(FuncName,"DCTRL register modification failed. [err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to enter transfer state failed. [err=%d]\n",err);}
         
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* undefine macros used locally within function */
   #undef DCtrlMode
   #undef SdErrors

   return status;
}

/*------------------------------------------------------------------------*/
/* function to read multiple 512-byte blocks of data from the SD card     */
/*------------------------------------------------------------------------*/
/**
   This function reads multiple 512-byte contiguous blocks starting at
   a user-specified address of a high capacity SD (HCSD) card.

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.
      addr.....The block-address in the range [0,csize] from which the
               512-byte blocks will be read.
      blkcnt...The number of blocks to be read.

   output:
      blk......The 512-byte blocks of data to read to the HCSD.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdBlockReadM(const SdCardInfo *sdcard, uint32_t addr, uint32_t blkcnt, SdBlk *blk)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdBlockReadM()";
   
   /* initialize the return value */
   int status=SdioFail;
   
   /* define local objects needed for write operations */
   int32_t csize; int16_t m,n,err,ccc; int8_t wprot;
   uint32_t cstatus; SdCardState state=SdInvalid;

   /* define local objects needed for timeout features */
   const uint32_t msTimeOut=10000; uint32_t msTimeRef,Tref;
   
   /* verify the function argument */
   if (!sdcard || !blk)
   {
      /* log the error */
      LogEntry(FuncName,"NULL function parameter.\n"); status=SdioNull;
   }

   /* decode the card specific data (CSD) */
   else if ((err=SdCsd(sdcard,&csize,NULL,&ccc,NULL,NULL,NULL,&wprot,NULL)<=0))
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to decode CSD failed. [err=%d]\n",err); 
   }

   /* validate the number of blocks */
   else if (blkcnt<=0) {LogEntry(FuncName,"Illegal block count: "
                                 "%d\n",blkcnt); status=SdioNull;}
   
   /* congruency check for start and end blocks of the write operation */
   else if (addr<0 || (addr+blkcnt)>csize)
   {
      /* log the congruency failure */
      LogEntry(FuncName,"Congruency failure: (addr+blkcnt)(%lu)<=SdSize(%lu)\n",
               (addr+blkcnt),csize); status=SdioInvalid;
   }

   /* initialize by inducing the CPSM into standby state */
   else if ((err=Stm32f103SdStateStandby(sdcard))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to induce standby state. [err=%d]\n",err);
   }
   
   /* induce the SD card into transfer state */
   else if ((err=Stm32f103SdStateTransfer(sdcard))>0) 
   {
      /* DCTRL register bitmask for 512-byte blocks and transfer enable */
      #define DCtrlMode (0x00000093LU)

      /* initialize the data control register */
      sdio->DCTRL=0;
                     
      /* initialize the block-length and timeout of the SD transfer */
      sdio->DTIMER=0x07f00000LU; sdio->DLEN=(blkcnt*sizeof(SdBlk));

      /* verify that the FIFO starts empty */
      if (sdio->FIFOCNT) {LogEntry(FuncName,"Orphaned Rx FIFO with %d "
                                   "bytes.\n",(4*sdio->FIFOCNT));}
                  
      /* initialize the DCTRL register for 512-byte blocks and enable transfer mode */
      else if ((err=Stm32f103SdRegModify( &(sdio->DCTRL), SdioTemplate.DCTRL, DCtrlMode ))>0)
      {
         /* execute CMD17 to read multiple blocks starting at the specified address */
         if ((err=Stm32f103SdSendCmd(Cmd18|Cmd_WaitResp|Cmd_CpsmEn,addr))>0) 
         {
            /* validate the response to the read-single-block command */
            if ((err=Stm32f103SdR1(Cmd18,&cstatus))>0) 
            {
               /* bitmask for termination of transfer from the Rx fifo */
               #define SdErrors (SdDCrcFail | SdDTimeOut | SdRxOverR | SdStBitErr)
                  
               /* compute the number of 32-bit words */
               const uint8_t N = (sizeof(SdBlk)/sizeof(uint32_t));
            
               /* loop over each SdBlk */
               for (msTimeRef=msTimer(0), m=0; m<blkcnt && err>0; m++)
               {
                  /* loop to transfer the current block to the Tx fifo */
                  for (Tref=msTimer(0), n=0; n<N;)
                  {
                     /* implement timeout feature */
                     if (msTimer(Tref)>msTimeOut) {status=err=SdioTimeOut; break;}

                     /* check for errors prior to reading from the Rx fifo */
                     else if ((cstatus=sdio->STA)&SdErrors) {err=SdioFail; break;}
                  
                     /* transfer the 512-byte block from the Rx fifo in units of 32-bit words */
                     else if (cstatus&SdRxDAvl) {blk[m].w[n++] = sdio->FIFO;}
                  }
               }
               
               /* loop to wait for transfer of data from the SD card */
               if (err>0) for (status=SdioTimeOut; msTimer(Tref)<msTimeOut;)
               {
                  /* check for active transfer of data from the SD card */
                  if (sdio->STA&SdRxAct || sdio->STA&SdRxDAvl) {continue;}
                     
                  /* check if the block was successfully transferred from the Rx fifo */
                  if ((sdio->STA&SdDataEnd) && (sdio->STA&SdDBckEnd))
                  {
                     /* signal the end of blocks to be written to the SD card */
                     Stm32f103SdStopTransfer(sdcard);
                     
                     /* query the CPSM state */
                     state=Stm32f103SdCardState(sdcard);
                     
                     /* exit the loop if the CPSM is in standby or transfer state */
                     if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check for nonrecoverable errors */
               else {LogEntry(FuncName,"Block transfer from Rx fifo of SD periperal "
                              "failed. [sdio->STA=0x%08lx]\n",cstatus);}

               /* log the failed attempt for the SD read operation */
               if (status<=0)
               {
                  /* log the failed attempt for the SD write operation */
                  LogEntry(FuncName,"Read failed for %d blocks starting at address "
                           "0x%08lu. [status=%d,state=%d,msTimer=%ldmsec]\n",blkcnt,
                           addr,status,state,msTimer(msTimeRef));

                  /* loop to induce the CPSM back into standby or transfer state */
                  for (Tref=msTimer(0); msTimer(Tref)<msTimeOut;)
                  {
                     /* pause if SD card is in receive or programming states */
                     if ((state=Stm32f103SdCardState(sdcard))==SdRcv || state==SdPrg)
                     {
                        /* terminate the write operation */
                        Stm32f103SdStopTransfer(sdcard); Wait(25);
                     }
                        
                     /* exit the loop if the CPSM is in standby or transfer state */
                     else if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check criteria for logging the successful write operation */
               else if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
               {
                  /* log the successful read operation */
                  LogEntry(FuncName,"Read successful for %d blocks starting at address "
                           "0x%08lu. [status=%d,state=%d,msTimer=%lumsec]\n",blkcnt,addr,
                           status,state,msTimer(msTimeRef));
               }
            }
         
            /* log the failure of the block-read command */
            else {LogEntry(FuncName,"Failed to read %d blocks starting at address 0x%08lx. "
                           "[err=%d,cstatus=0x%08lx]\n",blkcnt,addr,err,cstatus);}
         }
      
         /* log the failure */
         else {LogEntry(FuncName,"Attempt to send Cmd17 failed. [err=%d]\n",err);}
      }
      
      /* log the failure */
      else {LogEntry(FuncName,"DCTRL register modification failed. [err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to enter transfer state failed. [err=%d]\n",err);}
         
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* undefine macros used locally within function */
   #undef DCtrlMode
   #undef SdErrors

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a single 512-byte block of data to the SD card       */
/*------------------------------------------------------------------------*/
/**
   This function writes a single 512-byte block to a user-specified
   address of a high capacity SD (HCSD) card.  

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.
      addr.....The block-address in the range [0,csize] into which the
               512-byte block will be written.
      blk......The 512-byte block of data to write to the HCSD.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdBlockWrite(const SdCardInfo *sdcard, uint32_t addr, SdBlk *blk)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdBlockWrite()";
   
   /* initialize the return value */
   int status=SdioFail;
   
   /* define local objects needed for write operations */
   int32_t csize; int16_t n,err,ccc; int8_t wprot;
   uint32_t cstatus; SdCardState state=SdInvalid;

   /* define local objects needed for timeout features */
   const uint32_t msTimeOut=1000; uint32_t msTimeRef;
   
   /* verify the function argument */
   if (!sdcard || !blk)
   {
      /* log the error */
      LogEntry(FuncName,"NULL function parameter.\n"); status=SdioNull;
   }

   /* decode the card specific data (CSD) */
   else if ((err=SdCsd(sdcard,&csize,NULL,&ccc,NULL,NULL,NULL,&wprot,NULL)<=0))
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to decode CSD failed. [err=%d]\n",err); 
   }

   /* congruency check for start and end blocks of the write operation */
   else if (addr<0 || addr>csize)
   {
      /* log the congruency failure */
      LogEntry(FuncName,"Congruency failure: 0<=addr(%lu)<=SdSize(%lu)\n",
               addr,csize); status=SdioInvalid;
   }

   /* initialize by inducing the CPSM into standby state */
   else if ((err=Stm32f103SdStateStandby(sdcard))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to induce standby state. [err=%d]\n",err);
   }
   
   /* induce the SD card into transfer state */
   else if ((err=Stm32f103SdStateTransfer(sdcard))>0) 
   {
      /* initialize the data control register */
      sdio->DCTRL=0;
      
      /* verify that the FIFO starts empty */
      if (sdio->FIFOCNT) {LogEntry(FuncName,"Orphaned Tx FIFO with %d "
                                   "bytes.\n",(4*sdio->FIFOCNT));}

      /* execute CMD24 to write a single block at the specified address */
      else if ((err=Stm32f103SdSendCmd(Cmd24|Cmd_WaitResp|Cmd_CpsmEn,addr))>0) 
      {
         /* validate the response to the write-single-block command */
         if ((err=Stm32f103SdR1(Cmd24,&cstatus))>0) 
         {
            /* DCTRL register bitmask for 512-byte blocks and transfer enable */
            #define DCtrlMode (0x00000091LU)
            
            /* bitmask for termination of transfer to the Tx fifo */
            #define SdErrors (SdDCrcFail | SdDTimeOut | SdTxUnderR | SdStBitErr)
               
            /* initialize the block-length and timeout of the SD transfer */
            sdio->DTIMER=0x07f00000LU; sdio->DLEN=sizeof(SdBlk);
                  
            /* initialize the DCTRL register for 512-byte blocks and enable transfer mode */
            if ((err=Stm32f103SdRegModify( &(sdio->DCTRL), SdioTemplate.DCTRL, DCtrlMode))>0)
            {
               /* compute the number of 32-bit words */
               const uint8_t N = (sizeof(SdBlk)/sizeof(uint32_t));
               
               /* loop to transfer the block to the Tx fifo */
               for (msTimeRef=msTimer(0), n=0; n<N && msTimer(msTimeRef)<msTimeOut;)
               {
                  /* check for errors prior to writing to the Tx fifo */
                  if ((cstatus=sdio->STA)&SdErrors) {err=SdioFail; break;}
                  
                  /* transfer the 512-byte block to the Tx fifo in units of 32-bit words */
                  else if (sdio->STA&SdTxFifoHe) {sdio->FIFO = blk->w[n++];}
               }

               /* loop to wait for transfer of data to the SD card */
               if (err>0) for (status=SdioTimeOut; msTimer(msTimeRef)<msTimeOut;)
               {
                  /* check for active transfer of data to the SD card */
                  if (sdio->STA&SdTxAct || sdio->STA&SdTxDAvl) {continue;}
                     
                  /* check if the block was successfully transferred to the Tx fifo */
                  else if ((sdio->STA&SdDataEnd) && (sdio->STA&SdDBckEnd))
                  {
                     /* pause if SD card is in receive or programming states */
                     if ((state=Stm32f103SdCardState(sdcard))==SdRcv || state==SdPrg) {Wait(25);}
                        
                     /* exit the loop if the CPSM is in standby or transfer state */
                     else if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check for nonrecoverable errors */
               else {LogEntry(FuncName,"Block transfer to Tx fifo of SD periperal "
                              "failed. [sdio->STA=0x%08lx]\n",cstatus);}

               /* log the failed attempt for the SD write operation */
               if (status<=0)
               {
                  /* log the failed attempt for the SD write operation */
                  LogEntry(FuncName,"Write failed to block address 0x%08lu. "
                           "[status=%d,state=%d,msTimer=%lmsec]\n",addr,
                           status,state,msTimer(msTimeRef));

                  /* terminate the write operation */
                  Stm32f103SdStopTransfer(sdcard);

                  /* loop to induce the CPSM back into standby or transfer state */
                  for (msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
                  {
                     /* pause if SD card is in receive or programming states */
                     if ((state=Stm32f103SdCardState(sdcard))==SdRcv || state==SdPrg) {Wait(25);}
                        
                     /* exit the loop if the CPSM is in standby or transfer state */
                     else if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check criteria for logging the successful write operation */
               else if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
               {
                  /* log the successful write operation */
                  LogEntry(FuncName,"Write successful to block address 0x%08lu. "
                           "[status=%d,state=%d,msTimer=%lumsec]\n",addr,status,
                           state,msTimer(msTimeRef));
               }
            }
               
            /* log the failure */
            else {LogEntry(FuncName,"DCTRL register modification failed. [err=%d]\n",err);}
         }
         
         /* log the failure of the block-write command */
         else {LogEntry(FuncName,"Failed to write block to address 0x%08lx. "
                        "[err=%d,cstatus=0x%08lx]\n",addr,err,cstatus);}
      }
      
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd24 failed. [err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to enter transfer state failed. [err=%d]\n",err);}
         
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* undefine macros used locally within function */
   #undef DCtrlMode
   #undef SdErrors
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a single 512-byte block of data to the SD card       */
/*------------------------------------------------------------------------*/
/**
   This function writes a single 512-byte block to a user-specified
   address of a high capacity SD (HCSD) card.  

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.
      addr.....The block-address in the range [0,csize] into which the
               512-byte block will be written.
      blkcnt...The number of blocks to be written.
      blk......The 512-byte block of data to write to the HCSD.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdBlockWriteM(const SdCardInfo *sdcard, uint32_t addr, uint32_t blkcnt, SdBlk *blk)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdBlockWriteM()";
   
   /* initialize the return value */
   int status=SdioFail;
   
   /* define local objects needed for write operations */
   int32_t csize; int16_t m,n,err,ccc; int8_t wprot;
   uint32_t cstatus; SdCardState state=SdInvalid;

   /* define local objects needed for timeout features */
   const uint32_t msTimeOut=1000; uint32_t msTimeRef,Tref;
   
   /* verify the function argument */
   if (!sdcard || !blk)
   {
      /* log the error */
      LogEntry(FuncName,"NULL function parameter.\n"); status=SdioNull;
   }

   /* decode the card specific data (CSD) */
   else if ((err=SdCsd(sdcard,&csize,NULL,&ccc,NULL,NULL,NULL,&wprot,NULL)<=0))
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to decode CSD failed. [err=%d]\n",err); 
   }

   /* validate the number of blocks */
   else if (blkcnt<=0) {LogEntry(FuncName,"Illegal block count: "
                                 "%d\n",blkcnt); status=SdioNull;}

   /* congruency check for start and end blocks of the write operation */
   else if (addr<0 || (addr+blkcnt)>csize)
   {
      /* log the congruency failure */
      LogEntry(FuncName,"Congruency failure: (addr+blkcnt)(%lu)<=SdSize(%lu)\n",
               (addr+blkcnt),csize); status=SdioInvalid;
   }

   /* initialize by inducing the CPSM into standby state */
   else if ((err=Stm32f103SdStateStandby(sdcard))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to induce standby state. [err=%d]\n",err);
   }
   
   /* induce the SD card into transfer state */
   else if ((err=Stm32f103SdStateTransfer(sdcard))>0) 
   {
      /* initialize the data control register */
      sdio->DCTRL=0;
      
      /* verify that the FIFO starts empty */
      if (sdio->FIFOCNT) {LogEntry(FuncName,"Orphaned Tx FIFO with %d "
                                   "bytes.\n",(4*sdio->FIFOCNT));}

      /* execute CMD25 to write a single block at the specified address */
      else if ((err=Stm32f103SdSendCmd(Cmd25|Cmd_WaitResp|Cmd_CpsmEn,addr))>0) 
      {
         /* validate the response to the write-single-block command */
         if ((err=Stm32f103SdR1(Cmd25,&cstatus))>0) 
         {
            /* DCTRL register bitmask for 512-byte blocks and transfer enable */
            #define DCtrlMode (0x00000091LU)
            
            /* bitmask for termination of transfer to the Tx fifo */
            #define SdErrors (SdDCrcFail | SdDTimeOut | SdTxUnderR | SdStBitErr)
               
            /* initialize the block-length and timeout of the SD transfer */
            sdio->DTIMER=0x07f00000LU; sdio->DLEN=(blkcnt*sizeof(SdBlk));
                  
            /* initialize the DCTRL register for 512-byte blocks and enable transfer mode */
            if ((err=Stm32f103SdRegModify( &(sdio->DCTRL), SdioTemplate.DCTRL, DCtrlMode))>0)
            {
               /* compute the number of 32-bit words */
               const uint8_t N = (sizeof(SdBlk)/sizeof(uint32_t));

               /* loop over each SdBlk */
               for (msTimeRef=msTimer(0), m=0; m<blkcnt && err>0; m++)
               {
                  /* loop to transfer the current block to the Tx fifo */
                  for (Tref=msTimer(0), n=0; n<N;)
                  {
                     /* implement timeout feature */
                     if (msTimer(Tref)>msTimeOut) {status=err=SdioTimeOut; break;}

                     /* check for errors prior to writing to the Tx fifo */
                     else if ((cstatus=sdio->STA)&SdErrors) {err=SdioFail; break;}
                  
                     /* transfer the 512-byte block to the Tx fifo in units of 32-bit words */
                     else if (sdio->STA&SdTxFifoHe) {sdio->FIFO = blk[m].w[n++];}
                  }
               }
                  
               /* loop to wait for transfer of data to the SD card */
               if (err>0) for (status=SdioTimeOut; msTimer(Tref)<msTimeOut;)
               {
                  /* check for active transfer of data to the SD card */
                  if (sdio->STA&SdTxAct || sdio->STA&SdTxDAvl) {continue;}
                     
                  /* check if the block was successfully transferred to the Tx fifo */
                  else if ((sdio->STA&SdDataEnd) && (sdio->STA&SdDBckEnd))
                  {
                     /* signal the end of blocks to be written to the SD card */
                     Stm32f103SdStopTransfer(sdcard);

                     /* pause if SD card is in receive or programming states */
                     if ((state=Stm32f103SdCardState(sdcard))==SdRcv || state==SdPrg) {Wait(25);}
                        
                     /* exit the loop if the CPSM is in standby or transfer state */
                     else if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check for nonrecoverable errors */
               else {LogEntry(FuncName,"Data transfer to Tx fifo of SD periperal "
                              "failed. [err=%d,sdio->STA=0x%08lx]\n",err,cstatus);}
               
               /* check if the multi-block write operation was successful */ 
               if (status<=0) 
               {
                  /* log the failed attempt for the SD write operation */
                  LogEntry(FuncName,"Write failed for %d blocks starting at address "
                           "0x%08lu. [status=%d,state=%d,msTimer=%lumsec]\n",blkcnt,
                           addr,status,state,msTimer(msTimeRef));

                  /* loop to induce the CPSM back into standby or transfer state */
                  for (Tref=msTimer(0); msTimer(Tref)<msTimeOut;)
                  {
                     /* terminate the write operation */
                     Stm32f103SdStopTransfer(sdcard);
                     
                     /* pause if SD card is in receive or programming states */
                     if ((state=Stm32f103SdCardState(sdcard))==SdRcv || state==SdPrg) {Wait(25);}
                        
                     /* exit the loop if the CPSM is in standby or transfer state */
                     else if (state==SdStdby || state==SdTran) {status=SdioOk; break;}
                  }
               }

               /* check criteria for logging the successful write operation */
               else if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
               {
                  /* log the successful write operation */
                  LogEntry(FuncName,"Write successful for %d blocks starting at address "
                           "0x%08lu. [status=%d,state=%d,msTimer=%lumsec]\n",blkcnt,addr,
                           status,state,msTimer(msTimeRef));
               }
            }
               
            /* log the failure */
            else {LogEntry(FuncName,"DCTRL register modification failed. [err=%d]\n",err);}
         }
         
         /* log the failure of the block-write command */
         else {LogEntry(FuncName,"Failed to write %d blocks starting at address 0x%08lx. "
                        "[err=%d,cstatus=0x%08lx]\n",blkcnt,addr,err,cstatus);}
      }
      
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd24 failed. [err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to enter transfer state failed. [err=%d]\n",err);}
         
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* undefine macros used locally within function */
   #undef DCtrlMode
   #undef SdErrors
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the SD card is detected                       */
/*------------------------------------------------------------------------*/
/**
   This function determines if the SD card is detected.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdCardDetect(void)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdCardDetect()";
   
   /* initialize the return value */
   int err,status=SdioOk;

   /* check if external power to the SDIO peripheral is enabled */
   if ((err=Stm32GpioIsAssert(GPIOB,SD_PWR_EN_Pin))<=0)
   {
      /* indicate exception encountered */
      status=SdioNull;

      /* check criteria for logentry */
      if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
      {
         /* make a logentry of the failure */
         LogEntry(FuncName,"SD card is not powered. [err=%d]\n",err);
      }
   }

   /* SD_DETECT_Pin is clear when SD card is inserted */
   else if (!(err=Stm32GpioIsClear(GPIOG,SD_DETECT_Pin)))
   {
      /* indicate exception encountered */
      status=SdioFail;

      /* check criteria for logentry */
      if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
      {
         /* make a logentry of the failure */
         LogEntry(FuncName,"SD card is not detected. [err=%d]\n",err);
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the SD card is busy                           */
/*------------------------------------------------------------------------*/
/**
   This function uses the SD card's status register to deterine if the
   card is ready to receive commands.  Some operations (eg., write,
   erase) can cause the SD card to remain busy for relatively long
   periods of time after the command itself completes.  The PLSSv6
   specification for response R1b requires the SD host to check for a
   busy condition on the D0 line.

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

   output:
      This function returns a positive value if the SD card is busy or
      else zero if the card is ready to receive commands.  A negative
      return value indicates that an exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdCardIsBusy(const SdCardInfo *sdcard)
{
   /* initialize the return value */
   int status=SdioTimeOut;

   /* create object to contain the SD card's status register */
   uint32_t cstatus;

   /* query the SD card's status register */
   if (Stm32f103SdCardStatus(sdcard,&cstatus)>=0)
   {
      /* determine if the SD card is busy */
      status = ((cstatus&SdStatusReady) ? 0 : 1);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* query the SD card for the current state of  the CPSM                   */
/*------------------------------------------------------------------------*/
/**
   This function queries the SD card for the current state of the
   Command Path State Machine (CPSM).

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

   output:
      On success, this function returns the current state of the CPSM
      as defined by SdCardState enumeration in the header section of
      this C source module.  This also matches the CURRENT_STATE as
      described in PLSSv6, Section 4.10, Table 4-42.  On failure, this
      function returns a negative value to indicate an exception has
      occurred. 
\end{verbatim}
*/
int Stm32f103SdCardState(const SdCardInfo *sdcard)
{
   /* initialize the return value */
   SdCardState state=SdInvalid;

   /* create object to contain the SD cards status register */
   uint32_t cstatus;

   /* query the SD card's status register */
   if (Stm32f103SdCardStatus(sdcard,&cstatus)>=0)
   {
      /* set the return value to the state of the CPSM */
      state=((cstatus>>9)&0xf);

      /* validate the state */
      if (state<SdIdle || state>=SdNSTATE) state=SdInvalid;
   }
   
   return state;
}

/*------------------------------------------------------------------------*/
/* query the SD card for the current status of the CPSM                   */
/*------------------------------------------------------------------------*/
/**
   This function queries the SD card for the current status of the
   Command Path State Machine (CPSM).

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

   output:
      cstatus...This is the contents of the SD card's status
                register. Refer to PLSSv6, Section 4.10, Table 4-42
                for a description of the contents of the card status
                register.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdCardStatus(const SdCardInfo *sdcard,uint32_t *cstatus)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdCardStatus()";
   
   /* initialize the return value */
   int err,status=SdioFail;

   /* verify the function argument */
   if (!sdcard || !cstatus)
   {
      /* log the error */
      LogEntry(FuncName,"NULL function argument.\n"); status=SdioNull;
   }

   /* request the SD card's status register */
   else if ((err=Stm32f103SdSendCmd(Cmd13|Cmd_WaitResp|Cmd_CpsmEn,(sdcard->Rca<<16)))>0) 
   {
      /* validate the response to the SD status request */ 
      if ((err=Stm32f103SdR1(Cmd13,cstatus))>0) {status=SdioOk;}

      /* log the failure */
      else {LogEntry(FuncName,"Attempt to request SD status "
                     "register failed. [err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to send Cmd13 failed. [err=%d]\n",err);}
 
   return status;
}

/*------------------------------------------------------------------------*/
/* function to erase a user specified segment of SD memory blocks         */
/*------------------------------------------------------------------------*/
/**
   This function erases a user-specified sector of memory blocks from
   an SD card.

   \begin{verbatim}
   input:
      sdcard.....This is the SdCardInfo structure used to contain the SD
                 card's CSD register contents.
      StartBlk...The first block in the sector to be erased.
      EndBlk.....The last block in the sector to be erased.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdErase(const SdCardInfo *sdcard, uint32_t StartBlk, uint32_t EndBlk)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdErase()";
   
   /* initialize the return value */
   int err,status=SdioFail;

   /* define objects to contain CSD data needed for erasures */
   int32_t csize; int16_t ccc; int8_t wprot; uint32_t cstatus;
   
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* decode the card specific data (CSD) */
   else if ((err=SdCsd(sdcard,&csize,NULL,&ccc,NULL,NULL,NULL,&wprot,NULL)<=0))
   {
      /* log the error */
      LogEntry(FuncName,"Attempt to decode CSD failed. [err=%d]\n",err); 
   }

   /* test for erase function in the CCC field (bit 5) */ 
   else if (!(ccc&0x020) || wprot)
   {
      /* log the error */
      LogEntry(FuncName,"SD card not erasable or write-protected. "
               "[ccc:0x%03x wprot:0x%x]\n",ccc,wprot); status=SdioInvalid;
   }

   /* congruency check for start and end blocks of the erase operation */
   else if (StartBlk>EndBlk || EndBlk>csize)
   {
      /* log the congruency failure */
      LogEntry(FuncName,"Congruency failure: StartBlk(%lu)<=EndBlk(%lu)"
               "<=SdSize(%lu)\n",StartBlk,EndBlk,csize); status=SdioInvalid;
   }

   /* initialize by inducing the CPSM into standby state */
   else if ((err=Stm32f103SdStateStandby(sdcard))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to induce standby state. [err=%d]\n",err);
   }
   
   /* induce the SD card into transfer state */
   else if ((err=Stm32f103SdStateTransfer(sdcard))>0) 
   {
      /* execute CMD32 to set the start-block of the erase command */
      if ((err=Stm32f103SdSendCmd(Cmd32|Cmd_WaitResp|Cmd_CpsmEn,StartBlk))>0) 
      {
         /* validate the response to the erase start-block */
         if ((err=Stm32f103SdR1(Cmd32,&cstatus))>0) 
         {
            /* execute CMD33 to set the end-block of the erase command */
            if ((err=Stm32f103SdSendCmd(Cmd33|Cmd_WaitResp|Cmd_CpsmEn,EndBlk))>0) 
            {
               /* validate the response to the erase end-block */
               if ((err=Stm32f103SdR1(Cmd33,&cstatus))>0) 
               {
                  /* execute CMD38 to erase the start/end group of blocks */
                  if ((err=Stm32f103SdSendCmd(Cmd38|Cmd_WaitResp|Cmd_CpsmEn,0))>0) 
                  {
                     /* compute the timeout per Section 4.6.2.3 of PLSSv6 */
                     uint32_t msTimeOut = 5000 + (EndBlk-StartBlk)*250;
                     
                     /* validate the response to the erase command */
                     if ((err=Stm32f103SdR1b(sdcard,Cmd38,msTimeOut,&cstatus))>0) 
                     {
                        /* induce the SD card into standby state */
                        Stm32f103SdStateStandby(sdcard);
                        
                        /* indicate success */
                        status=SdioOk;

                        /* check criteria for logentry */
                        if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
                        {
                           /* make a logentry of the failure */
                           LogEntry(FuncName,"SD card blocks [%lu:%lu] erased. [err=%d,"
                                    "cstatus=0x%08lx]\n",StartBlk,EndBlk,err,cstatus);
                        }
                     }
               
                     /* log the failure of the erase command */
                     else {LogEntry(FuncName,"Erase command failed. [err=%d,"
                                    "cstatus=0x%08lx]\n",err,cstatus);}
                  }
           
                  /* log the failure of the erase start-block */
                  else {LogEntry(FuncName,"Attempt to send Cmd38 failed. "
                                 "[err=%d]\n",err);}
               }
               
               /* log the failure of the erase start-block */
               else {LogEntry(FuncName,"Erase end-block command failed. [err=%d,"
                              "cstatus=0x%08lx]\n",err,cstatus);}
            }
            
            /* log the failure of the erase start-block */
            else {LogEntry(FuncName,"Attempt to send Cmd33 failed. "
                              "[err=%d]\n",err);}
         }
         
         /* log the failure of the erase start-block */
         else {LogEntry(FuncName,"Erase start-block command failed. [err=%d,"
                        "cstatus=0x%08lx]\n",err,cstatus);}
      }

      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd32 failed. "
                        "[err=%d]\n",err);}
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"Attempt to enter transfer state failed. "
                  "[err=%d]\n",err);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SD card for its Card Identification register     */
/*------------------------------------------------------------------------*/
/**
   This function queries the SD card for the contents of its Card
   Identification (CID) register.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CID register contents.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdCid(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdCid()";

   /* initialize the return value */
   int n,err,status=SdioFail;

   /* check if the SD card & peripheral are enabled */
   if (sdio->POWER!=SdioTemplate.POWER || Stm32f103SdCardDetect()<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"SD card missing or peripheral "
               "disabled."); status=SdioInvalid;
   }
   
   /* verify the function argument */
   else if (sdcard)
   {
      /* initialzie the CID array */
      memset((void *)(&sdcard->Cid),0,sizeof(sdcard->Cid));
   
      /* execute CMD2 to request the CID of the SD card */
      if ((err=Stm32f103SdSendCmd(Cmd2|Cmd_WaitResp_Long|Cmd_CpsmEn,0))>0) 
      {
         /* validate the response to the application prelude */
         if ((err=Stm32f103SdR2())>0) 
         {
            /* copy each 32-byte word into the CID array */
            for (n=3; n>=0; n--) sdcard->Cid[12+n] = ((sdio->RESP1)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Cid[ 8+n] = ((sdio->RESP2)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Cid[ 4+n] = ((sdio->RESP3)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Cid[   n] = ((sdio->RESP4)>>(8*n));

            /* check criteria for making a logentry */
            if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
            {
               /* create a pointer to the CID */
               const uint8_t *cid = sdcard->Cid;

               /* create a header for the CID logentry */
               LogEntry(FuncName,"CID: %02X-%02X%02X-%c%c%c%c%c-%02X-"
                        "%02X%02X%02X%02X-%02X%02X-%02X\n",cid[15],
                        cid[14],cid[13],cid[12],cid[11],cid[10],cid[9],
                        cid[8],cid[7],cid[6],cid[5],cid[4],cid[3],
                        cid[2],cid[1],cid[0]);
            }

            /* indicate success */
            status=SdioOk;
         }

         /* log the failure */
         else {LogEntry(FuncName,"Response failure for CMD2. [err=%d]\n",err);}
      }

      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send CMD2 failed. [err=%d]\n",err);}
   }

   /* log the failure */
   else {LogEntry(FuncName,"NULL function argument.\n"); status=SdioNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the SDIO clock generator                            */
/*------------------------------------------------------------------------*/
/**
   This function enables the SDIO clock generator by asserting the
   CLKEN bit in the CLKCR register of the SDIO peripheral.  If
   successful, this function returns a positive value.  If the
   specified alterations are valid but the modifications failed then
   zero is returned.  If the specified alterations or register are
   invalid then a negative value is returned.
*/
int Stm32f103SdClockEnable(void)
{
   /* assert the CLKEN bit of the CLKCR register */
   return Stm32f103SdRegModify( &(sdio->CLKCR), 0, SdClockEnable);
}

/*------------------------------------------------------------------------*/
/* function to disable the SDIO clock generator                           */
/*------------------------------------------------------------------------*/
/**
   This function disables the SDIO clock generator by clearing the
   CLKEN bit in the CLKCR register of the SDIO peripheral.  If
   successful, this function returns a positive value.  If the
   specified alterations are valid but the modifications failed then
   zero is returned.  If the specified alterations or register are
   invalid then a negative value is returned.
*/
int Stm32f103SdClockDisable(void)
{
   /* clear the CLKEN bit of the CLKCR register */
   return Stm32f103SdRegModify( &(sdio->CLKCR), SdClockEnable, 0);
}

/*------------------------------------------------------------------------*/
/* function to query the SD card for its Card Specific Data register      */
/*------------------------------------------------------------------------*/
/**
   This function queries the SD card for the contents of its Card
   Specific Data (CSD) register.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CSD register contents.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdCsd(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdCsd()";

   /* initialize the return value */
   int n,err,status=SdioFail;

   /* check if the SD card & peripheral are enabled */
   if (sdio->POWER!=SdioTemplate.POWER || Stm32f103SdCardDetect()<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"SD card missing or peripheral "
               "disabled."); status=SdioInvalid;
   }
   
   /* verify the function argument */
   else if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* check for invalid Relative Card Address (RCA) */
   else if (sdcard->Rca==0 || sdcard->Rca==USHRT_MAX)
   {
      /* log the failure */
      LogEntry(FuncName,"Invalid RCA(0x%04x) for SD card.\n",
               sdcard->Rca); status=SdioInvalid;
   }

   else
   {
      /* initialzie the CSD array */
      memset((void *)(&sdcard->Csd),0,sizeof(sdcard->Csd));
   
      /* execute CMD9 to request the CSD of the SD card */
      if ((err=Stm32f103SdSendCmd(Cmd9|Cmd_WaitResp_Long|Cmd_CpsmEn,(sdcard->Rca<<16)))>0) 
      {
         /* validate the response to the application prelude */
         if ((err=Stm32f103SdR2())>0) 
         {
            /* copy each 32-byte word into the CSD array */
            for (n=3; n>=0; n--) sdcard->Csd[12+n] = ((sdio->RESP1)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Csd[ 8+n] = ((sdio->RESP2)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Csd[ 4+n] = ((sdio->RESP3)>>(8*n));
            for (n=3; n>=0; n--) sdcard->Csd[   n] = ((sdio->RESP4)>>(8*n));

            /* check criteria for making a logentry */
            if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
            {
               /* create a header for the CSD logentry */
               LogEntry(FuncName,"CSD:");

               /* add the CSD to the logentry */
               for (n=(sizeof(sdcard->Csd)-1); n>=0; n--) LogAdd(" %02X",sdcard->Csd[n]);

               /* terminate the logentry*/
               LogAdd("\n");
            }

            /* indicate success */
            status=SdioOk;
         }

         /* log the failure */
         else {LogEntry(FuncName,"Response failure for CMD9. [err=%d]\n",err);}
      }

      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send CMD9 failed. [err=%d]\n",err);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the SDIO peripheral and card is powered up    */
/*------------------------------------------------------------------------*/
/**
   This function determines if the SDIO peripheral and card are powered
   up.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdIsPowered(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdIsPowered()";

   /* initialize the return value */
   int err,status=SdioInvalid;

   /* define objects used locally */
   uint32_t value;
 
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* determine if the SD card is powered up */
   else if ((err=Stm32f103SdRegRead(&(sdio->POWER),&value))<=0)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to read SDIO POWER register. "
               "[err=%d,value=0x%08lx]\n",err,value);
   }

   /* read the state of the SDIO power-enable pin */
   else if ((err=Stm32GpioIsAssert(GPIOB,SD_PWR_EN_Pin))<0)
   {
      /* log the error */
      LogEntry(FuncName,"Undetermined SDIO power state. [err=%d]\n",err);
   }

   /* determine the state SDIO peripheral and card are powered up */
   else if (err>0 && value>0) {status=SdioOk;}

   /* SDIO peripheral and card are powered down */
   else {status=SdioFail;}
         
   return status;
}

/*------------------------------------------------------------------------*/
/* initialize the SD card prior to storage/retrieval operations           */
/*------------------------------------------------------------------------*/
/**
   This function initializes the SD card by specifying the voltage to
   be in the range 3.2V-3.3V and confirming that the SD card is a high
   capacity V2.0+ card rather than the older MMC card.  The voltage
   range is specified by asserting one (and only one) bit of the
   ACMD41 argument within the range of bits [15:23] according to the
   table below (ref: SD PLLv6, Section 5.1).

   \begin{verbatim}
      OcrBit  VoltRange(V)     BitMask
      --------------------------------
          15     2.7 - 2.8  0x00008000
          16     2.8 - 2.9  0x00010000
          17     2.9 - 3.0  0x00020000
          18     3.0 - 3.1  0x00040000
          19     3.1 - 3.2  0x00080000
          20     3.2 - 3.3  0x00100000
          21     3.3 - 3.4  0x00200000
          22     3.4 - 3.5  0x00400000
          23     3.5 - 3.6  0x00800000

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdInit(void)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdInit()";

   /* initialize the return value */
   int status=SdioFail;

   /* create local variables */
   int m,n,err; uint32_t cstatus;
   
   /* bitmask to determine that initialization is complete */
   #define SdInitComplete (0x80000000LU)

   /* bitmask to check for errors in the SD status register */
   #define SdStatusErrorBits (0xFDF9E008LU)
   
   /* bitmask for high capacity SD card */
   #define Sdhc (0x40000000LU)

   /* bitmask for voltage range of Apf11 SD interface */
   #define Volts (0x00100000LU)
   
   /* combine voltage window specification with high capacity */
   #define ACmd41Arg (SdInitComplete | Sdhc | Volts)

   /* define parameters for a timeout mechanism */
   const time_t To=time(NULL), TimeOut=3; 

   /* retry loop to allow completion of SD card initialization */
   for (status=SdioTimeOut, m=0, n=0xffffLU; n>0 && m<3; n--)
   {
      /* implement a timeout feature */
      if (difftime(time(NULL),To)>TimeOut) {status=SdioTimeOut; break;}
      
      /* send Cmd55 prelude to the application command */
      if ((err=Stm32f103SdSendCmd(Cmd55|Cmd_WaitResp|Cmd_CpsmEn,0))>0)
      {
         /* validate the response to the application prelude */
         if ((err=Stm32f103SdR1(Cmd55,&cstatus))>0 && !(cstatus&SdStatusErrorBits)) 
         {
            /* send the application command */
            if ((err=Stm32f103SdSendCmd(ACmd41|Cmd_WaitResp|Cmd_CpsmEn,ACmd41Arg))>0)
            {
               /* validate the response to the application prelude */
               if ((err=Stm32f103SdR3())>0) 
               {
                  /* check if the initialization is complete */
                  if (sdio->RESP1 & SdInitComplete)
                  {
                     /* confirm that the SD card is high capacity */
                     if (sdio->RESP1 & Sdhc) {status=SdioOk;}

                     /* log the error */
                     else {LogEntry(FuncName,"SD card is not high capacity.\n");}

                     /* exit the initialization loop */
                     break;
                  }

                  /* indicate that initialization is not complete */
                  else {status=SdioInvalid;}
               }
               
               /* log the failure */
               else {LogEntry(FuncName,"Voltage negotiation failed. "
                              "[err=%d]\n",err); status=SdioFail;}
            }

            /* log the failure */
            else {LogEntry(FuncName,"Attempt to send ACmd41 failed. "
                           "[err=%d]\n",err); status=SdioFail;}
         }
         
         /* log the failure of the application prelude */
         else {LogEntry(FuncName,"Application prelude failed. [err=%d,"
                        "cstatus=0x%08lu]\n",err,cstatus); status=SdioFail;}

         /* increment the failure-counter and pause before retry */
         m++; Wait(250);
      }

      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd55 failed. "
                     "[err=%d]\n",err); status=SdioFail; Wait(100);}
   }

   /* undefine macros used locally within function */
   #undef SdInitComplete
   #undef SdStatusErrorBits
   #undef Sdhc
   #undef Volts
   #undef ACmd41Arg
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to power-down & de-initialize the SD card                     */
/*------------------------------------------------------------------------*/
/**
   This function to power-down and de-initialize the SD card.

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CSD register contents.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdPowerDown(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdPowerDown()";
   
   /* initialize the return value */
   int err,status=SdioOk;

   /* reinitialize the SdCardInfo structure */
   if ((err=SdCardInfoInit(sdcard))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"SdCardInfo initialization failed. [err=%d]\nm",err);

      /* indicate failure */
      status=SdioFail;
   }
   
   /* clear the CLKEN bit of the CLKCR register */
   if ((err=Stm32f103SdClockDisable())<=0) 
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to disable SD clock failed. [err=%d]\n",err);

      /* indicate failure */
      status=SdioFail;
   }
    
   /* disable power to the SDIO card */
   if ((err=Stm32f103SdRegWrite( &(sdio->POWER), 0))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to disable SD card power failed. [err=%d]\n",err);

      /* indicate failure */
      status=SdioFail;
   }

   /* disable external power to the SDIO peripheral */
   if (Stm32GpioClear(GPIOB,SD_PWR_EN_Pin)<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to disable power to the SD "
               "peripheral failed. [err=%d]\n",err);

      /* indicate failure */
      status=SdioFail;
   }

   /* deconfigure Stm32 SD IO/CLK pins (including optional 4-pin interface) */
   Stm32GpioConfig(GPIOC, (SDIO_D0_Pin | SDIO_D1_Pin | SDIO_D2_Pin |
                           SDIO_D3_Pin | SDIO_CLK_Pin), InAnlg);

   /* deconfigure Stm32 SD CMD pin */
   Stm32GpioConfig(GPIOD, SDIO_CMD_Pin, InAnlg);

   /* disable the SD clock */
   __HAL_RCC_SDIO_CLK_DISABLE();

   /* pause after successful power-down */
   if (status==SdioOk) {Wait(100);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to power-up & initialize the SD card                          */
/*------------------------------------------------------------------------*/
/**
   This function to power-up and initialize the SD card.  This
   initialization sequence requires a high-capacity SD (HCSD) card in
   order to successfully complete; MMC cards will fail the
   initialization.

   According to the PLSSv6, the initialization phase must operate at
   clock speeds less than 400kHz; after the SD card has been assigned
   an RCA then the clock speed can be increased up to 3/32 the speed
   of HCLK.  The SdioInit.CLKCR object configures the clock divisor to
   be 198 (0xc3) so that SDIO_CK = HCLK/(198+2) = 8MHz/200 = 40kHz
   during the card identification phase.  After the card
   identification phase is complete, the clock divisor is reduced to 2
   so that SDIO_CK = HCLK/(2+2) = 8MHz/4 = 2MHz.

   Note: The initialization sequence implemented in this function
      incorporates some recommendations found at the following URL:
         https://blog.frankvh.com/2011/09/04/stm32f2xx-sdio-sd-card-interface
      In particular, the GPIO pins used by the SDIO peripheral are
      initally configured as push-pull outputs that are initialized to
      a high state.  After powering-up the peripheral and card then
      the GPIO pins are reconfigured to their alternate function for
      use by the SDIO peripheral.  According to the author, this
      renders initialization to be more reliable.

   \begin{verbatim}
   input:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's CSD register contents.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdPowerUp(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdPowerUp()";
   
   /* initialize the return value */
   int err,status=SdioFail;

   /* bitmask to enable HW flow control and set clock speed at 1MHz */
   #define SdioClkCfg (SdClockHighSpeed | SdHwFlowControl)

   /* enable the SD clock */
   __HAL_RCC_SDIO_CLK_ENABLE();

   /* configure Stm32 SD IO/CLK pins (single D0-pin interface) */
   Stm32GpioConfig(GPIOC, (SDIO_D0_Pin | SDIO_CLK_Pin), OutPPH);

   /* configure unused Stm32 SD IO/CLK pins (D{1,2,3} of 4-pin interface) */
   Stm32GpioConfig(GPIOC, (SDIO_D1_Pin | SDIO_D2_Pin | SDIO_D3_Pin ), InPU);

   /* configure Stm32 SD CMD pin */
   Stm32GpioConfig(GPIOD, SDIO_CMD_Pin, OutPPH);

   /* configure Stm32 SD detection pin */
   Stm32GpioConfig(GPIOG, SD_DETECT_Pin, InFlt);

   /* reconfigure the SDIO interface with reset values */
   if (Stm32f103SdRegWrite(&(sdio->POWER ),0)<=0 || Stm32f103SdRegWrite(&(sdio->CLKCR ),0)<=0 ||
       Stm32f103SdRegWrite(&(sdio->ARG   ),0)<=0 || Stm32f103SdRegWrite(&(sdio->CMD   ),0)<=0 ||
       Stm32f103SdRegWrite(&(sdio->DTIMER),0)<=0 || Stm32f103SdRegWrite(&(sdio->DLEN  ),0)<=0 ||
       Stm32f103SdRegWrite(&(sdio->DCTRL ),0)<=0 || Stm32f103SdRegWrite(&(sdio->ICR   ),0)<=0 ||
       Stm32f103SdRegWrite(&(sdio->MASK  ),0)<=0)
   {
      /* add a logentry of the failure */
      LogEntry(FuncName,"SDIO reset configuration failed.\n");
   }

   /* initialize and configure the clock control register */
   if (Stm32f103SdRegModify( &(sdio->CLKCR), SdioTemplate.CLKCR, SdioInit.CLKCR)<=0)
   {
      /* add a logentry of the failure */
      LogEntry(FuncName,"SDIO clock control configuration failed.\n");
   }

   /* enable external power to the SDIO peripheral */
   if (Stm32GpioAssert(GPIOB,SD_PWR_EN_Pin)<=0)
   {
      /* add a logentry of the failure */
      LogEntry(FuncName,"Attempt to enable power to the SDIO peripheral.\n");
   }
   
   /* enable power to the SDIO card */
   if (Stm32f103SdRegWrite( &(sdio->POWER), SdioInit.POWER)<=0)
   {
      /* add a logentry of the failure */
      LogEntry(FuncName,"SDIO power control configuration failed.\n");
   }

   /* pause 1ms for power-up of SD card and then enable the SDIO clock */
   Wait(1); Stm32f103SdClockEnable();

   /* reconfigure Stm32 SD IO/CLK pins (single D0-pin interface) */
   Stm32GpioConfig(GPIOC, (SDIO_D0_Pin | SDIO_CLK_Pin), AOutPP);

   /* reconfigure Stm32 SD CMD pin */
   Stm32GpioConfig(GPIOD, SDIO_CMD_Pin, AOutPP);
   
   /* SD_DETECT_Pin is clear when SD card is inserted */
   if (!(err=Stm32GpioIsClear(GPIOG,SD_DETECT_Pin)))
   {
      /* log the absence of an SD card */
      LogEntry(FuncName,"No SD card detected.\n");
   }

   /* check for exception */
   else if (err<0) {LogEntry(FuncName,"SD detection exception. [err=%d]\n",err);}
   
   /* reset the SD card */
   else if ((err=Stm32f103SdReset())<=0) {LogEntry(FuncName,"Attempt to reset SD "
                                                   "card failed. [err=%d]\n",err);} 

   /* confirm SD card, exclude MMC card */
   else if ((err=Stm32f103SdV2())<=0) {LogEntry(FuncName,"Non-SD card detected. "
                                                "[err=%d]\n",err);}

   /* initialize the SD card */
   else if ((err=Stm32f103SdInit())<=0) {LogEntry(FuncName,"SD card initialization "
                                                        "failed. [err=%d]\n",err);}

   /* query the SD card for contents of its Card Identification (CID) register */
   else if ((err=Stm32f103SdCid(sdcard))<=0) {LogEntry(FuncName,"Query for SD CID register "
                                                       "failed. [err=%d]\n",err);}

   /* query the SD card for its relative card address */
   else if ((err=Stm32f103SdRca(sdcard))<=0) {LogEntry(FuncName,"Query for SD RCA "
                                                       "failed. [err=%d]\n",err);}

   /* query the SD card for contents of its Card Specific Data (CSD) register */
   else if ((err=Stm32f103SdCsd(sdcard))<=0) {LogEntry(FuncName,"Query for SD CSD register "
                                                       "failed. [err=%d]\n",err);}

   /* enable hardware flow control and high speed transfer */
   else if ((err=Stm32f103SdRegModify( &(sdio->CLKCR), SdioInit.CLKCR, SdioClkCfg))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Failed to enable HW flow control and "
               "high speed operation. [err=%d]\n",err);
   }
   
   /* indicate success */
   else {status=SdioOk;}
   
   return status; 
}

/*------------------------------------------------------------------------*/
/* function to query the SD card for its relative card address (RCA)      */
/*------------------------------------------------------------------------*/
/**
   This function queries the SD card for its relative card address (RCA).

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain the SD
               card's relative card address (RCA).

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdRca(SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdRca()";

   /* initialize the return value */
   int n,err,status=SdioFail;

   /* check if the SD card & peripheral are enabled */
   if (sdio->POWER!=SdioTemplate.POWER || Stm32f103SdCardDetect()<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"SD card missing or peripheral "
               "disabled."); status=SdioInvalid;
   }
   
   /* verify the function argument */
   else if (sdcard)
   {
      /* initialize the Relative Card Address (RCA) */
      sdcard->Rca=USHRT_MAX;
      
      /* retry loop to establish the Relative Card Address (RCA) */
      for (status=SdioInvalid, n=0; n<3 && status!=SdioOk; n++)
      {
         /* execute CMD3 to request the RCA of the SD card */
         if ((err=Stm32f103SdSendCmd(Cmd3|Cmd_WaitResp|Cmd_CpsmEn,0))>0) 
         {
            /* validate the response to the application prelude */
            if ((err=Stm32f103SdR6(Cmd3))>0) 
            {
               /* extract the Relative Card Address (RCA) */
               sdcard->Rca=((sdio->RESP1)>>16);

               /* validate the RCA */
               if (sdcard->Rca>0 && sdcard->Rca<USHRT_MAX)
               {
                  /* indicate success */
                  status=SdioOk;
                  
                  /* check criteria for making a logentry */
                  if (debuglevel>3 || (debugbits&STM32F103SDIO_H))
                  {
                     /* create a header for the CID logentry */
                     LogEntry(FuncName,"RCA: 0x%02x\n",sdcard->Rca);
                  }
               }
            }
            
            /* log the failure */
            else {LogEntry(FuncName,"Response failure for CMD3. [err=%d]\n",err);}
         }

         /* log the failure */
         else {LogEntry(FuncName,"Attempt to send CMD3 failed. [err=%d]\n",err);}
      }
               
      /* validate the RCA */
      if (sdcard->Rca<=0 || sdcard->Rca==USHRT_MAX)
      {
         /* log the invalid RCA */
         LogEntry(FuncName,"Invalid RCA: 0x%02x\n",sdcard->Rca);
         
         /* reset the RCA to the sentinel and indicate failure */
         sdcard->Rca=USHRT_MAX; status=SdioInvalid;
      }
   }
   
   /* log the failure */
   else {LogEntry(FuncName,"NULL function argument.\n"); status=SdioNull;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to modify user-pecified bits of an SDIO register              */
/*------------------------------------------------------------------------*/
/**
   The SDIO cluster consists of both read/write (RW) and read-only (R)
   registers.  Many of the RW registers allow writing to only some of
   the bits of the registers, the remaining bits are required to
   remain at their reset states.

   This function modifies user-specified bits of an SDIO RW register
   while protecting read-only registers or nonwriteable bits against
   write attempts.  If a register is selected that is out-of-range or
   nonwriteable then no modifications are made and an exception is
   returned.  If a valid register is specified but nonwriteable bits
   are selected for alteration then no modifications are made and an
   exception is returned.

   \begin{verbatim}
   input:
       addr...........The address of a writeable register within the
                      SDIO cluster.  
       BitsToClear....This bitmask indicates specific bits to clear
                      from the SDIO register; bits that are asserted
                      in this mask will be cleared in the register.
       BitsToAssert...This bitmask indicates specific bits to assert
                      from the SDIO register; bits that are asserted
                      in this mask will be asserted in the register.

   output:
      If successful, this function returns a positive value.  If the
      specified alterations are valid but the modifications failed
      then zero is returned.  If the specified alterations or register
      are invalid then a negative value is returned.
   \end{verbatim
*/
int Stm32f103SdRegModify(volatile uint32_t *addr, uint32_t BitsToClear, uint32_t BitsToAssert)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdRegModify()";

   /* initialize the return value */
   int status=SdioNull;

   /* 32-bit object for template of valid register content */
   uint32_t template=0;
   
   /* validate uint32_t as 32-bit words */
   assert(sizeof(uint32_t)==4);

   /* validate the register address to modify */
   if (addr)
   {
      /* re-initialize the return value */
      status=SdioInvalid;
      
      /* define the template for the SDIO register */
      if      (addr==(&(sdio->POWER ))) {template=SdioTemplate.POWER; }
      else if (addr==(&(sdio->CLKCR ))) {template=SdioTemplate.CLKCR; }
      else if (addr==(&(sdio->ARG   ))) {template=SdioTemplate.ARG;   }
      else if (addr==(&(sdio->CMD   ))) {template=SdioTemplate.CMD;   }
      else if (addr==(&(sdio->DTIMER))) {template=SdioTemplate.DTIMER;}
      else if (addr==(&(sdio->DLEN  ))) {template=SdioTemplate.DLEN;  }
      else if (addr==(&(sdio->DCTRL ))) {template=SdioTemplate.DCTRL; }
      else if (addr==(&(sdio->ICR   ))) {template=SdioTemplate.ICR;   }
      else if (addr==(&(sdio->MASK  ))) {template=SdioTemplate.MASK;  }
      else {LogEntry(FuncName,"Invalid SDIO register: 0x%08ul\n",addr);}

      /* check if a valid template was defined above */
      if (template)
      {
         /* test for invalid bit modifications */
         if (!((~template)&(BitsToClear|BitsToAssert)))
         {
            /* read the contents of the address */
            uint32_t reg = (*addr);

            /* first, clear specified bits then assert specified bits */
            reg &= (~BitsToClear); reg |= BitsToAssert;
            
            /* assign the modified register contents back to the register */
            (*addr) = (template & reg); Delay();
         
            /* test if the register contains the expected value */
            status = (((*addr) == (template & reg)) ? SdioOk : SdioFail);
         }

         /* log the attempt to modify invalid register bits */
         else {LogEntry(FuncName,"Invalid modification of SDIO register 0x%08lx: clear(0x%08lx) "
                        "assert(0x%08lx)\n",addr,BitsToClear,BitsToAssert);}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the contents of a specified SDIO register             */
/*------------------------------------------------------------------------*/
/**
   This function reads the contents of a specified SDIO register. 

   \begin{verbatim}
   input:
       addr...The address of a writeable register within the SDIO
              cluster.

   output:
      value...This is a 32-bit object in which the register's
              contents is written.

      If successful, this function returns a positive value.  If an
      exception encountered then a negative value is returned.
   \end{verbatim
*/
int Stm32f103SdRegRead(volatile uint32_t *addr, uint32_t *value)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdRead()";

   /* initialize the return value */
   int status=SdioInvalid;

   /* initialize a pointer to the SDIO registers */
   uint32_t *sdio=(uint32_t *)SDIO;
   
   /* validate uint32_t as 32-bit words */
   assert(sizeof(uint32_t)==4);

   /* initialize the return value */
   if (value) {(*value)=0xffffffffUL;}
   
   /* check for NULL function arguments */
   if (!addr || !value) {status=SdioNull;}

   /* validate the register address */
   else if (addr>=sdio && addr<=(sdio+0xfc)) {(*value)=(*addr); status=SdioOk;}

   /* make a logentry of the invalid register address */
   else {LogEntry(FuncName,"Invalid SDIO register: 0x%08lx\n",addr);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write user-pecified value to an SDIO register              */
/*------------------------------------------------------------------------*/
/**
   The SDIO cluster consists of both read/write (RW) and read-only (R)
   registers.  Many of the RW registers allow writing to only some of
   the bits of the registers, the remaining bits are required to
   remain at their reset states.

   This function writes user-specified bits of an SDIO RW register
   while protecting read-only registers or nonwriteable bits against
   write attempts.  If a register is selected that is out-of-range or
   nonwriteable then no modifications are made and an exception is
   returned.  If a valid register is specified but nonwriteable bits
   are selected for alteration then no modifications are made and an
   exception is returned.

   \begin{verbatim}
   input:
       addr....The address of a writeable register within the
               SDIO cluster.
       value...This bitmask indicates specific bits to assert from the
               SDIO register; bits that are asserted in this mask will
               be asserted in the register.

   output:
      If successful, this function returns a positive value.  If the
      specified alterations are valid but the modifications failed
      then zero is returned.  If the specified alterations or register
      are invalid then a negative value is returned.
   \end{verbatim
*/
int Stm32f103SdRegWrite(volatile uint32_t *addr, uint32_t value)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdWrite()";

   /* initialize the return value */
   int status=SdioNull;

   /* 32-bit object for template of valid register content */
   uint32_t template=0;
   
   /* validate uint32_t as 32-bit words */
   assert(sizeof(uint32_t)==4);

   /* validate the register address to modify */
   if (addr)
   {
      /* re-initialize the return value */
      status=SdioInvalid;
      
      /* define the template for the SDIO register */
      if      (addr==(&(sdio->POWER ))) {template=SdioTemplate.POWER; }
      else if (addr==(&(sdio->CLKCR ))) {template=SdioTemplate.CLKCR; }
      else if (addr==(&(sdio->ARG   ))) {template=SdioTemplate.ARG;   }
      else if (addr==(&(sdio->CMD   ))) {template=SdioTemplate.CMD;   }
      else if (addr==(&(sdio->DTIMER))) {template=SdioTemplate.DTIMER;}
      else if (addr==(&(sdio->DLEN  ))) {template=SdioTemplate.DLEN;  }
      else if (addr==(&(sdio->DCTRL ))) {template=SdioTemplate.DCTRL; }
      else if (addr==(&(sdio->ICR   ))) {template=SdioTemplate.ICR;   }
      else if (addr==(&(sdio->MASK  ))) {template=SdioTemplate.MASK;  }
      else {LogEntry(FuncName,"Invalid SDIO register: 0x%08ul\n",addr);}
 
      /* check if a valid template was defined above */
      if (template)
      {
         /* test for invalid bit modifications */
         if (!((~template)&value))
         {
            /* assign the masked register contents back to the register */
            (*addr) = (template & value); Delay();
         
            /* test if the register contains the expected value */
            status = (((*addr) == (template & value)) ? SdioOk : SdioFail);
         }

         /* log the attempt to modify invalid register bits */
         else {LogEntry(FuncName,"Invalid modification of SDIO register 0x%08lx: "
                        "value(0x%08lx)\n",addr,value);}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to reset the SD card                                          */
/*------------------------------------------------------------------------*/
/**
   This function resets the SD card in preparation for initialization.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdReset(void)
{
   /* initialize the return value */
   int n,status=SdioFail;

   /* execute CMD0 to reset the SD card */
   if ((status=Stm32f103SdSendCmd(Cmd0|Cmd_CpsmEn,0))>0) 
   {
      /* expiration loop to wait for completion of CMD0 */
      for (status=SdioTimeOut, n=0x10000UL; n>0; n--) {}

      /* check status register for completion of command */
      if (sdio->STA & SdCmdSent) {status=SdioOk;}
   }
      
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;
   
   return status;
}

/*------------------------------------------------------------------------*/
/* seek response R1 from CPSM of the Stm32f103 SDIO peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function seeks a response R1 from CPSM of the Stm32f103 SDIO
   peripheral.

   \begin{verbatim}
   input:
      cmd........The command whose response is expected.  The response
                 from an SD card includes the command identifier which
                 is explicitly verified to ensure that the response
                 matches the expected command.  Failure of this
                 verification constitutes an exception.

   output:
      cstatus....The contents of the SD card's status register will be
                 copied to this object and passed back to the calling
                 function.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdR1(uint8_t cmd,uint32_t *cstatus)
{
   /* initialize the return value */
   int status=SdioFail;
   
   /* parameter for timeout feature (>1000ms per PLSSv6) */
   #define msTimeOut (1000UL)

   /* define local objects needed for timeout feature */
   uint32_t msTimeRef; 
   
   /* initialize the function argument */
   if (cstatus) {(*cstatus)=0;}
   
   /* expiration loop to wait for completion of command */
   for (status=SdioTimeOut, msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
   {
      /* check status register for completion of command */
      if (sdio->STA & (SdCCrcFail|SdCmdREnd|SdCTimeOut)) {status=SdioOk; break;}
   }
   
   /* verify that the response is to the expected command */
   if (cmd!=(sdio->RESPCMD)) {status=SdioInvalid;}
   
   /* check timeout criteria */
   else if ((sdio->STA & SdCTimeOut) || status==SdioTimeOut) {status=SdioTimeOut;}

   /* check for CRC failure */
   else if (sdio->STA & SdCCrcFail) {status=SdioCrcFail;}

   /* check for normal command termination */
   else if (sdio->STA & SdCmdREnd)
   {
      /* transfer the card status register */
      if (cstatus) {(*cstatus) = sdio->RESP1;}

      /* indicate success */
      status=SdioOk;
   }

   /* catch-all criteria for failure */
   else {status=SdioFail;}

   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;
      
   /* clear local definitions */
   #undef msTimeOut
   
   return status;
}

/*------------------------------------------------------------------------*/
/* seek response R1b from CPSM of the Stm32f103 SDIO peripheral           */
/*------------------------------------------------------------------------*/
/**
   This function seeks a response R1b from CPSM of the Stm32f103 SDIO
   peripheral.

   \begin{verbatim}
   input:
      sdcard......This is the SdCardInfo structure used to contain the SD
                  card's CSD register contents.
      cmd.........The command whose response is expected.  The
                  response from an SD card includes the command
                  identifier which is explicitly verified to ensure
                  that the response matches the expected command.
                  Failure of this verification constitutes an
                  exception.
      msTimeOut...Some commands (eg., erase) might require significant
                  amounts of time to complete.  Section 6 of the
                  PLSSv6 provides estimates of expected timeout
                  periods for various operations. This timeout should
                  be expressed in milliseconds.

   output:
      cstatus....The contents of the SD card's status register will be
                 copied to this object and passed back to the calling
                 function.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdR1b(const SdCardInfo *sdcard, uint8_t cmd,
                          uint32_t msTimeOut, uint32_t *cstatus)
{
   /* initialize the return value */
   int err,status=Stm32f103SdR1(cmd,cstatus);

   /* define the pause between status checks of the SD card */
   #define HeartBeat (100)

   /* define local objects needed for timeout feature */
   uint32_t msTimeRef; 

   /* enforce a minimum timeout period */
   if (msTimeOut<5000) {msTimeOut=5000;}
   
   /* loop to implement the timeout feature */
   for (err=SdioBusy, msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
   {
      /* check if the SD card is ready to receive commands */
      if (!Stm32f103SdCardIsBusy(sdcard)) {err=SdioOk; break;}

      /* pause before the next status check */
      else {Wait(HeartBeat);}
   }

   /* check criteria for a busy-timeout */
   if (status>0 && err==SdioBusy) {status=SdioBusy;}

   /* clear local definitions */
   #undef HeartBeat
   
   return status;
}

/*------------------------------------------------------------------------*/
/* seek response R2 from CPSM of the Stm32f103 SDIO peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function seeks a response R2 from CPSM of the Stm32f103 SDIO
   peripheral.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdR2(void)
{
   /* initialize the return value */
   int status=SdioFail;
      
   /* parameter for timeout feature (>1000ms per PLSSv6) */
   #define msTimeOut (1000UL)

   /* define local objects needed for timeout feature */
   uint32_t msTimeRef; 

   /* expiration loop to wait for completion of command */
   for (status=SdioTimeOut, msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
   {
      /* check status register for completion of command */
      if (sdio->STA & (SdCCrcFail|SdCmdREnd|SdCTimeOut)) {status=SdioOk; break;}
   }

   /* verify that the response is to the expected command */
   if (sdio->RESPCMD != 0x3f) {status=SdioInvalid;}
   
   /* check timeout criteria */
   else if ((sdio->STA & SdCTimeOut) || status==SdioTimeOut) {status=SdioTimeOut;}

   /* check for CRC failure */
   else if (sdio->STA & SdCCrcFail) {status=SdioCrcFail;}

   /* check for normal command termination */
   else if (sdio->STA & SdCmdREnd) {status=SdioOk;}

   /* catch-all criteria for failure */
   else {status=SdioFail;}

   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;
   
   /* clear local definitions */
   #undef msTimeOut

   return status;
}

/*------------------------------------------------------------------------*/
/* seek response R3 from CPSM of the Stm32f103 SDIO peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function seeks response R3 from the Command Path State Machine
   (CPSM) after the contents of the OCR register has been sent by the
   SD card.  The R3 response is the only one that is not CRC
   protected.  Hence, the CRC bit will always be asserted in the
   status register after receipt of the response; assertion of the CRC
   bit is evidence of receipt of the OCR but not evidence of CRC
   failure (ref: Sections 4.9 and 4.9.4 of SD Physical Layer
   Specification).

   Moreover, since R3 receives an OCR response then the RESPCMD
   register will contain 0x3f which indicates that the initiating
   command is unknown (ref: Section 22.9.5 of the Stm32f103 reference
   manual).

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdR3(void)
{
   /* initialize the return value */
   int status=SdioFail;
      
   /* parameter for timeout feature (>1000ms per PLSSv6) */
   #define msTimeOut (1000UL)

   /* define local objects needed for timeout feature */
   uint32_t msTimeRef; 
   
   /* expiration loop to wait for completion of command */
   for (status=SdioTimeOut, msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
   {
      /* check status register for completion of command */
      if (sdio->STA & (SdCCrcFail|SdCmdREnd|SdCTimeOut)) {status=SdioOk; break;}
   }

   /* verify that the response is to the expected command */
   if (sdio->RESPCMD != 0x3f) {status=SdioInvalid;}
 
   /* check timeout criteria */
   else if ((sdio->STA & SdCTimeOut) || status==SdioTimeOut) {status=SdioTimeOut;}

   /* R3 lacks CRC check so CRC failure indicates success */
   else if (sdio->STA & SdCCrcFail) {status=SdioOk;}

   /* check for normal command termination */
   else if (sdio->STA & SdCmdREnd) {status=SdioOk;}

   /* indicate general failure */
   else {status=SdioFail;}
   
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* clear local definitions */
   #undef msTimeOut

   return status;
}

/*------------------------------------------------------------------------*/
/* seek response R6 from CPSM of the Stm32f103 SDIO peripheral            */
/*------------------------------------------------------------------------*/
/**
   This function seeks a response R6 from CPSM of the Stm32f103 SDIO
   peripheral.

   \begin{verbatim}
   input:
      cmd...The command whose response is expected.  The response from
            an SD card includes the command identifier which is
            explicitly verified to ensure that the response matches
            the expected command.  Failure of this verification
            constitutes an exception.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdR6(uint8_t cmd)
{
   /* initialize the return value */
   int status=SdioFail;

   /* bitmask to specify general unknown, illegal command, or crc error */
   #define ErrorBits (0x0000e000LU)
   
   /* parameter for timeout feature (>1000ms per PLSSv6) */
   #define msTimeOut (1000UL)

   /* define local objects needed for timeout feature */
   uint32_t msTimeRef; 

   /* expiration loop to wait for completion of command */
   for (status=SdioTimeOut, msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
   {
      /* check status register for completion of command */
      if (sdio->STA & (SdCCrcFail|SdCmdREnd|SdCTimeOut)) {status=SdioOk; break;}
   }

   /* verify that the response is to the expected command */
   if (cmd!=(sdio->RESPCMD)) {status=SdioInvalid;}
   
   /* check timeout criteria */
   else if ((sdio->STA & SdCTimeOut) || status==SdioTimeOut) {status=SdioTimeOut;}

   /* check for CRC failure */
   else if (sdio->STA & SdCCrcFail) {status=SdioCrcFail;}

   /* check for normal command termination */
   else if (sdio->STA & SdCmdREnd)
   {
      /* check response for general unknown, illegal command, or crc error */
      status = ((sdio->RESP1)&ErrorBits) ? SdioFail : SdioOk;
   }

   /* catch-all criteria for failure */
   else {status=SdioFail;}

   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* clear local definitions */
   #undef ErrorBits
   #undef msTimeOut

   return status;
}

/*------------------------------------------------------------------------*/
/* function to send a command/argument to the SD card                     */
/*------------------------------------------------------------------------*/
/**
   This function sends a command/argument to the SD card via the SDIO
   peripheral interface.  

   \begin{verbatim}
   input:
      cmd...The command to be executed by the SD card.
      arg...The argument to the command.

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdSendCmd(uint32_t cmd, uint32_t arg)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdSendCmd()";
   
   /* initialize the return value */
   int status=SdioOk;

   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags;

   /* load the argument register */
   sdio->ARG=arg;

   /* load the command register */
   if ((status=Stm32f103SdRegModify( &(sdio->CMD), SdioTemplate.CMD, cmd))<=0) 
   {
      /* make a logentry of the failure */
      LogEntry(FuncName,"Attempt to send command (CMD%d, ARG:0x%08lx) "
                        "to SD card failed. [err=%d]\n",cmd,arg,status);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to induce the CPSM into standby mode                          */
/*------------------------------------------------------------------------*/
/**
   This function induces the CPSM into standby mode from any state
   that the CPSM permits.  This function is also successful if the
   CPSM is already in standby state.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdStateStandby(const SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdStateStandby()";
   
   /* initialize the return value */
   int status=SdioFail;

   /* create local work objects */
   int err; uint8_t state; uint32_t msTimeOut=5000;

   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* check if the CPSM is already in the standby state */
   else if ((state=Stm32f103SdCardState(sdcard))==SdStdby) {status=SdioOk;}
   
   /* verify that the CPSM is currently in a legal transition state */
   else if (state==SdTran || state==SdSnd || state==SdPrg)
   {
      /* induce the SD card into standby state */
      if ((err=Stm32f103SdSendCmd(Cmd7|Cmd_WaitResp|Cmd_CpsmEn,0))>0) 
      {
         /* wait for completion of the send command */
         Stm32f103SdR1b(sdcard,Cmd7,msTimeOut,NULL);

         /* request the card state */
         if ((state=Stm32f103SdCardState(sdcard))==SdStdby) {status=SdioOk;}

         /* log the error */
         else {LogEntry(FuncName,"Transition to standby state "
                        "failed. [state=%d]\n",state);}
      }
   
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd7 failed. [err=%d]\n",err);}
   }

   /* log the failure */
   else {LogEntry(FuncName,"Illegal transition from state: %d\n",state);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to induce the CPSM into transfer mode                         */
/*------------------------------------------------------------------------*/
/**
   This function induces the CPSM into transfer mode from standby
   state.  This function is also successful if the CPSM is already in
   transfer state.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdStateTransfer(const SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdStateTransfer()";
   
   /* initialize the return value */
   int status=SdioInvalid;

   /* create local work objects */
   int err; uint32_t cstatus; uint8_t state;

   /* bitmask to check for errors in the SD status register */
   #define SdStatusErrorBits (0x0ff9e000LU)
      
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* check if the CPSM is already in the transfer state */
   else if ((state=Stm32f103SdCardState(sdcard))==SdTran) {status=SdioOk;}
 
   /* verify that the CPSM is currently in standby state */
   else if (state==SdStdby)
   {
      /* induce the SD card into transfer state */
      if ((err=Stm32f103SdSendCmd(Cmd7|Cmd_WaitResp|Cmd_CpsmEn,(sdcard->Rca<<16)))>0) 
      {
         /* validate the response to the request to enter transfer state */ 
         if ((err=Stm32f103SdR1(Cmd7,&cstatus))>0) 
         {
            /* check for errors in the SD status register */
            if (!(cstatus&SdStatusErrorBits)) {status=SdioOk;}

            /* log the status errors */
            else {LogEntry(FuncName,"Errors in SD status register: 0x%08lx\n",cstatus);}
         }

         /* log the failure */
         else {LogEntry(FuncName,"Attempt to entery standby "
                        "state failed. [err=%d]\n",err);}
      }
   
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd7 failed. [err=%d]\n",err);}
   }

   /* log the failure */
   else {LogEntry(FuncName,"Illegal transition from state: %d\n",state);}

   /* clear local definitions */
   #undef SdStatusErrorBits
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the SD card to determine Version 2+ of the spec      */
/*------------------------------------------------------------------------*/
/**
   This function queroes the SD card to determine if it conforms to
   Version 2+ of the SD Physical Layer Simplified Specification.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
static int Stm32f103SdV2(void)
{
   /* initialize the return value */
   int n,status=SdioInvalid;
   
   /* execute CMD8 with argument of 0x100 (2.7-3.6V) and 0xAA (echo pattern) */
   if (Stm32f103SdSendCmd(Cmd8|Cmd_WaitResp|Cmd_CpsmEn,0x1AA)>0) 
   {
      /* timeout loop to wait for completion of CMD8 */
      for (n=0x10000UL; n>0; n--)
      {
         /* check status register for completion of command */
         if (sdio->STA & (SdCCrcFail|SdCmdREnd|SdCTimeOut)) {break;}
      }

      /* check timeout criteria */
      if ((sdio->STA & SdCTimeOut) || n<=0) {status=SdioTimeOut;}

      /* check for CRC failure */
      else if (sdio->STA & SdCCrcFail) {status=SdioCrcFail;}

      /* check for normal command termination */
      else if (sdio->STA & SdCmdREnd) {status=SdioOk;}

      /* catch-all criteria for failure */
      else {status=SdioFail;}
   }
   
   /* clear the status flags in the STA register */
   sdio->ICR = SdStaticFlags; 

   return status;
}

/*------------------------------------------------------------------------*/
/* stop data transfer between SD peripheral and card                      */
/*------------------------------------------------------------------------*/
/**
   This function stops data transfer between the SD peripheral and the
   SD card.

   \begin{verbatim}
   output:
      sdcard...This is the SdCardInfo structure used to contain
               various pieces of data specific to a given SD card.

      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int Stm32f103SdStopTransfer(const SdCardInfo *sdcard)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103SdStopTransfer()";
   
   /* initialize the return value */
   int status=SdioInvalid;

   /* create local work objects */
   int err; uint32_t cstatus; uint8_t state;

   /* set the period for the timeout feature */
   const uint32_t msTimeOut=5000;

   /* bitmask to check for errors in the SD status register */
   #define SdStatusErrorBits (0x0ff9e000LU)
      
   /* verify the function argument */
   if (!sdcard) {LogEntry(FuncName,"NULL SdCardInfo pointer.\n"); status=SdioNull;}

   /* check if the CPSM is in a state where CMD12 works */
   else if ((state=Stm32f103SdCardState(sdcard))==SdTran || state==SdSnd || state==SdRcv)
   {
      /* execute command to stop transmission */
      if ((err=Stm32f103SdSendCmd(Cmd12|Cmd_WaitResp|Cmd_CpsmEn,0))>0) 
      {
         /* validate the response to the request to enter transfer state */ 
         if ((err=Stm32f103SdR1b(sdcard,Cmd12,msTimeOut,&cstatus))>0) 
         {
            /* check for errors in the SD status register */
            if (!(cstatus&SdStatusErrorBits)) {status=SdioOk;}

            /* log the status errors */
            else {LogEntry(FuncName,"Errors in SD status register: 0x%08lx\n",cstatus);}
         }

         /* log the failure */
         else {LogEntry(FuncName,"Attempt to entery standby "
                        "state failed. [err=%d]\n",err);}
      }
   
      /* log the failure */
      else {LogEntry(FuncName,"Attempt to send Cmd12 failed. [err=%d]\n",err);}
   }

   /* log the failure */
   else {LogEntry(FuncName,"Illegal state for CMD12: %d\n",state);}

   /* clear local definitions */
   #undef SdStatusErrorBits
   
   return status;
}

#endif /* STM32F103SDIO_C */
