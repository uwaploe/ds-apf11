#ifndef STM32F103RTC_H
#define STM32F103RTC_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 RTC model
 * ------------------------------------------------
 *
 * STMicro's RTC model stores only the (24-hour) time in the RTC counter
 * registers and then stores the date in a storage register that was
 * updated by the API.  While the MCU is in stop or standby mode, the
 * date is not updated and the RTC counter maintained time.  After
 * exiting standby or stop mode, it was the application's responsibility
 * to update the date and reset the clock in order to conform to that
 * model. The model also used Jan 1, 2000 as the reference date and
 * allowed no provision for representing dates prior to that reference.
 * This model is unsuitable for use with autonomous oceanographic
 * profiling applications and so a completely new model and API were
 * required. 
 * 
 * Important Note: The Stm32 must be configured so that the RTC counter
 * increments 256 times per second.  This deviates from STMicro's RTC
 * model with requires that the RTC register counter increment once per
 * second.  The Apf11 uses an external high-accuracy 32kHz oscillator as
 * the LSE source for the Stm32's RTC.  Configuration of the RTC
 * registers is outlined in Section 2, page 8, of STMicro's Application
 * Note AN2821 (Doc ID 14949 Rev 2, April 2009).  The Stm32 RTC includes
 * a 20-bit RTC prescaler that can be programmed to generate the 256Hz
 * RTC timebase required by this SwiftWare Stm32 RTC model.  AN2821:p8
 * provides the formula for configuring the prescaler:
 *    p = [(Flse/Frtc) - 1] = [(32kHz/256Hz) - 1] = 255
 * where Flse (=32kHz) is the frequency of the external clock-source,
 * Frtc (=256Hz) is the required time-base for the SwiftWare Stm32 RTC
 * model.  Hence, the Stm32's 20-bit prescaler register should be
 * configured with the value (p=255) in order to generate the required
 * 256Hz timebase.
 * 
 * This SwiftWare STM32F103 RTC API represents a complete drop-in
 * replacement for STMicro's HAL RTC API.  The SwiftWare RTC model
 * maintains both a 32-bit signed Unix epoch (referenced to
 * 01/01/1970:00:00:00) and a 32-bit unsigned mission-time configured for
 * 24-bits of range (ie., 194.2 days) and 8-bits of resolution (ie.,
 * 1/256th second).  Hence, this RTC model is capable of reproducing the
 * semantics of the Dallas Semiconductor DS2404 RTC used by the Apf9
 * controller and firmware.
 * 
 * The Stm32 RTC register is used to maintain the mission-time that
 * governs progress through all of the phases of the profile cycle.  The
 * Stm32 RTC includes a single alarm that can be used to generate a reset
 * to bring the Stm32 out of standby or stop mode.  If the alarm is
 * enabled then when the 32-bit contents of the RTC register exactly
 * matches the contents of the 32-bit RTC alarm register then the reset
 * is initiated.  This also matches the semantics of Apf9 behavior and so
 * this Stm32 RTC model is compatible with the Apf9 RTC model.
 * 
 * The DS2404 RTC used by the Apf9 included two independent counter
 * registers; these were used to implement independent clocks to maintain
 * the Unix epoch and the mission-timer.  Unfortunately, the Stm32
 * implements only one RTC counter register which (as mentioned above) is
 * used to maintain the mission-timer.  In order to implement clock to
 * maintain the Unix epoch, two Stm32 RTC backup registers (DR1,DR2) are
 * reserved to maintain a reference-epoch that represents the Unix epoch
 * when the mission-timer was zero.  The relationship between the current
 * Unix epoch (T), the reference epoch (Tref), and the mission-time (Tm)
 * is given by: T = Tref + Tm.
 * 
 * In order to reset the mission-time without changing the current unix
 * epoch then the reference epoch must be computed and to preserve the
 * current unix epoch: Tref = (T - Tm) where (T) is the current unix epoch
 * and (Tm) is the new mission-time.
 * 
 * Similarly, in order to reset the current unix epoch without changing
 * the mission-time then the reference epoch must be computed to preserve
 * the mission-time: Tref = (T - Tm) where (T) is the new unix epoch and
 * (Tm) is the mission-time.
 * 
 * The DS2404 RTC used by the Apf9 also included two alarm counters which
 * were used to implement independent alarms for the mission-time and for
 * the unix epoch.  Unfortunately, the Stm32 RTC implements only a single
 * alarm which is reserved by the SwiftWare RTC model to implement a
 * mission-timer alarm that maintains forward progress through the
 * profile cycle.  Hence, this API does not implement an alarm for the
 * unix epoch.  Though regrettable, in practice, nothing is lost because
 * the unix epoch alarm on the DS2404 was never used by Ap9 firmware.
 * 
 * The 24-bit mission timer and 32-bit unix epoch clock are both fully
 * protected against underflow and overflow while resetting either
 * timer. This API detects and prevents under/over flow conditions; the
 * API returns an error without changing the mission-time or unix epoch.
 * However, no protection is offered against overflow of the
 * mission-timer by the 256Hz external LSE signal.  The mission-timer has
 * a range of 2^24 seconds which is slightly more than 192 days.  On
 * overflow, the mission-timer will wrap-around to zero and continue
 * incrementing at 256Hz; a bit in the Stm32's RTC status register is
 * asserted to indicate that the overflow happened.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103RtcChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stm32f1xx_hal_rtc.h>

/* declare functions with external linkage */
time_t            ialarm(void);
int               IntervalAlarmDisable(void);
int               IntervalAlarmEnable(void);
int               IntervalAlarmInterruptOff(void);
int               IntervalAlarmInterruptOn(void);
int               IntervalAlarmGet(time_t *sec, unsigned char *tics);
int               IntervalAlarmSet(time_t sec, unsigned char tics);
double            IntervalTimerDiff(unsigned long int tics1, unsigned long int tics0);
int               IntervalTimerGet(time_t *sec, unsigned char *tics);
int               IntervalTimerSet(time_t sec, unsigned char tics);
unsigned long int IntervalTimerTics(void);
time_t            itimer(void);
int               RtcGet(time_t *sec, unsigned char *tics);
int               RtcSet(time_t sec);
int               Stm32AlarmSave(void);
int               Stm32AlarmRecall(unsigned long int *AlarmTics);
int               Stm32RtcConfig(void);
time_t            Stm32RefEpoch(void);
time_t            time(time_t *sec);

/* define some constants of nature */
#define SecPerYear  (31536000L)
#define SecPerDay   (86400L)
#define SecPerHour  (3600L)
#define SecPerMin   (60L)
#define WDogTimeOut (105*SecPerMin)

/* define the upper bound for the reference epoch and mission-time */
extern const time_t RefEpochMax;
extern const uint32_t MTimeMax;

/* define the return states of the Stm32 RTC API */
extern const char RtcAlarmNotSet;        /* attempt to enable missing alarm-time */
extern const char RtcAlarmWriteFail;     /* attempt to write reference epoch failed */
extern const char RtcAlarmReadFail;      /* attempt to read reference epoch failed */
extern const char RtcRefEpochWriteFail;  /* attempt to write reference epoch failed */
extern const char RtcRefEpochReadFail;   /* attempt to read reference epoch failed */
extern const char RtcWriteFail;          /* attempt to write reference epoch failed */
extern const char RtcReadFail;           /* attempt to read reference epoch failed */
extern const char RtcUnderflow;          /* underflow condition detected */
extern const char RtcOverflow;           /* overflow condition detected */
extern const char RtcNull;               /* NULL function argument */
extern const char RtcFail;               /* general failure */
extern const char RtcOk;                 /* general success */

#endif /* STM32F103RTC_H */
#ifdef STM32F103RTC_C
#undef STM32F103RTC_C

#include <assert.h>
#include <limits.h>
#include <logger.h>   
#include <stm32f1xx_hal.h>
#include <string.h>

#ifdef HAL_RTC_MODULE_ENABLED

/* define the return states of the Stm32 RTC API */
const char RtcAlarmNotSet       = -10; /* attempt to enable missing alarm-time */ 
const char RtcAlarmWriteFail    =  -9; /* attempt to write reference epoch failed */
const char RtcAlarmReadFail     =  -8; /* attempt to read reference epoch failed */
const char RtcRefEpochWriteFail =  -7; /* attempt to write reference epoch failed */
const char RtcRefEpochReadFail  =  -6; /* attempt to read reference epoch failed */
const char RtcWriteFail         =  -5; /* attempt to write reference epoch failed */
const char RtcReadFail          =  -4; /* attempt to read reference epoch failed */
const char RtcUnderflow         =  -3; /* underflow condition detected */
const char RtcOverflow          =  -2; /* overflow condition detected */
const char RtcNull              =  -1; /* NULL function argument */
const char RtcFail              =   0; /* general failure */
const char RtcOk                =   1; /* general success */

/* define the tic-mask and the number of bits that represent sub-second tics */
#define RtcTicMask (0xff)
#define RtcTicBits (8)

/* define the upper bound for the reference epoch and mission-time */
const time_t RefEpochMax = (LONG_MAX-((1UL)<<(32-RtcTicBits)));
const uint32_t MTimeMax  = ((1UL)<<(32-RtcTicBits))-1;

/* local handle to the external RTC_HandleTypeDef object */      
static RTC_HandleTypeDef hrtc;

/* declare external reference for static RTC_HandleTypeDef object */
RTC_HandleTypeDef *HAL_RTC_HandleGet(void);

/* prototypes for RTC functions with static linkage */
static int Stm32RefEpochRead(time_t *sec);
static int Stm32RefEpochWrite(time_t sec);
static int Stm32RtcAlarmRead(unsigned long int *sec, unsigned char *tics);
static int Stm32RtcAlarmWrite(unsigned long int sec, unsigned char tics);
static int Stm32RtcRead(unsigned long int *sec,unsigned char *tics);
static int Stm32RtcWrite(unsigned long int sec,unsigned char tics);

/* prototypes for functions with unpublished external linkage */
int  Stm32RtcAlarmWriteTics(unsigned long int AlarmTics);

/* prototypes for external functions that are used locally */
void ErrorHandler(const char *FuncName);
void Stm32RtcTicsAtWakeSet(unsigned long int RtcTics);

/* prototypes for STMicro HAL RTC API functions with external linkage */
void              HAL_RTC_AlarmIRQHandler(RTC_HandleTypeDef* hrtc);
HAL_StatusTypeDef HAL_RTC_DeactivateAlarm(RTC_HandleTypeDef *hrtc, uint32_t Alarm);
HAL_StatusTypeDef HAL_RTC_DeInit(RTC_HandleTypeDef *hrtc);
HAL_StatusTypeDef HAL_RTC_Init(RTC_HandleTypeDef *hrtc);
__weak void       HAL_RTC_MspDeInit(RTC_HandleTypeDef* hrtc);
__weak void       HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc);

/* prototypes for STMicro HAL RTC API functions with static linkage */
static HAL_StatusTypeDef RTC_EnterInitMode(RTC_HandleTypeDef* hrtc);
static HAL_StatusTypeDef RTC_ExitInitMode(RTC_HandleTypeDef* hrtc);

/* define the engineering log message for deprecated HAL RTC functions */
static const char *DeprecatedMsg = "Deprecated: Use SwiftWare RTC model.\n";
   
/* prototypes for neutered STMicro HAL RTC API functions */
__weak void       HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc);
HAL_StatusTypeDef HAL_RTC_GetAlarm(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Alarm, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_GetDate(RTC_HandleTypeDef *hrtc, RTC_DateTypeDef *sDate, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_GetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_PollForAlarmAEvent(RTC_HandleTypeDef *hrtc, uint32_t Timeout);
HAL_StatusTypeDef HAL_RTC_SetAlarm_IT(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_SetAlarm(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_SetDate(RTC_HandleTypeDef *hrtc, RTC_DateTypeDef *sDate, uint32_t Format);
HAL_StatusTypeDef HAL_RTC_SetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime, uint32_t Format);
__weak void       HAL_RTCEx_RTCIRQHandler(RTC_HandleTypeDef* hrtc);
__weak void       HAL_RTCEx_TamperIRQHandler(RTC_HandleTypeDef *hrtc);

/* sentinel values used in several RTC functions */
#define RTC_ALARM_RESETVALUE_REGISTER (uint16_t)0xFFFF
#define RTC_ALARM_RESETVALUE          (uint32_t)0xFFFFFFFF

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
/**
   This function should not be modified, when the callback is needed,
   the HAL_RTC_AlarmAEventCallback could be implemented in the user file
*/
__weak void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_AlarmAEventCallback()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* Prevent unused argument(s) compilation warning */
   UNUSED(hrtc);
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
void HAL_RTC_AlarmIRQHandler(RTC_HandleTypeDef* hrtc)
{  
   if(__HAL_RTC_ALARM_GET_IT_SOURCE(hrtc, RTC_IT_ALRA))
   {
      /* Get the status of the Interrupt */
      if(__HAL_RTC_ALARM_GET_FLAG(hrtc, RTC_FLAG_ALRAF) != (uint32_t)RESET)
      {
         /* AlarmA callback */ 
         HAL_RTC_AlarmAEventCallback(hrtc);
      
         /* Clear the Alarm interrupt pending bit */
         __HAL_RTC_ALARM_CLEAR_FLAG(hrtc,RTC_FLAG_ALRAF);
      }
   }
  
   /* Clear the EXTI's line Flag for RTC Alarm */
   __HAL_RTC_ALARM_EXTI_CLEAR_FLAG();
  
   /* Change RTC state */
   hrtc->State = HAL_RTC_STATE_READY; 
}

/*------------------------------------------------------------------------*/
/* STMicro's HAL function to deactivate the RTC alarm                     */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_DeactivateAlarm(RTC_HandleTypeDef *hrtc, uint32_t Alarm)
{
   /* Check the parameters */
   assert_param(IS_RTC_ALARM(Alarm));
  
   /* Check input parameters */
   if(hrtc == NULL) {return HAL_ERROR;}
  
   /* Process Locked */ 
   __HAL_LOCK(hrtc); hrtc->State = HAL_RTC_STATE_BUSY;
  
   /* In case of interrupt mode is used, the interrupt source must disabled */ 
   __HAL_RTC_ALARM_DISABLE_IT(hrtc, RTC_IT_ALRA);
  
   /* set initialization mode */
   if(RTC_EnterInitMode(hrtc) != HAL_OK)
   {
      /* set RTC state and placed an advisory lock on the RTC process */
      hrtc->State = HAL_RTC_STATE_ERROR; __HAL_UNLOCK(hrtc);
    
      return HAL_ERROR;
   } 
   else
   {
      /* Clear flag alarm A */
      __HAL_RTC_ALARM_CLEAR_FLAG(hrtc, RTC_FLAG_ALRAF);
    
      /* Set to default values ALRH & ALRL registers */
      WRITE_REG(hrtc->Instance->ALRH, RTC_ALARM_RESETVALUE_REGISTER);
      WRITE_REG(hrtc->Instance->ALRL, RTC_ALARM_RESETVALUE_REGISTER);

      /* RTC Alarm Interrupt Configuration: Disable EXTI configuration */
      __HAL_RTC_ALARM_EXTI_DISABLE_IT();
    
      /* Wait for synchro */
      if(RTC_ExitInitMode(hrtc) != HAL_OK)
      {       
         /* set RTC state and placed an advisory lock on the RTC process */
         hrtc->State = HAL_RTC_STATE_ERROR; __HAL_UNLOCK(hrtc);
      
         return HAL_ERROR;
      }
   }

   /* set ready-state and remove the advisory lock on the RTC process */
   hrtc->State = HAL_RTC_STATE_READY;  __HAL_UNLOCK(hrtc);  
  
   return HAL_OK; 
}

/*------------------------------------------------------------------------*/
/* function to de-initialize the RTC peripheral                           */
/*------------------------------------------------------------------------*/
/**
   This function de-initializes the RTC peripheral.  It does not reset
   the RTC back-up data registers.

      \begin{verbatim}
      input:
         hrtc....pointer to a RTC_HandleTypeDef structure that
                 contains the configuration information for RTC.

      output:
         If successful, this function returns zero. A non-zero value
         is returned if an error occurred.
      \end{verbatim}
*/
HAL_StatusTypeDef HAL_RTC_DeInit(RTC_HandleTypeDef *hrtc)
{
  /* Check input parameters */
  if(hrtc == NULL) {return HAL_ERROR;}
  
  /* Check the parameters */
  assert_param(IS_RTC_ALL_INSTANCE(hrtc->Instance));

  /* Set RTC state */
  hrtc->State = HAL_RTC_STATE_BUSY; 
  
  /* Set Initialization mode */
  if(RTC_EnterInitMode(hrtc) != HAL_OK)
  {
    /* Set RTC state and remove the advisory lock */
    hrtc->State = HAL_RTC_STATE_ERROR; __HAL_UNLOCK(hrtc);

    return HAL_ERROR;
  }  
  else
  {
    CLEAR_REG(hrtc->Instance->CNTL);
    CLEAR_REG(hrtc->Instance->CNTH);
    WRITE_REG(hrtc->Instance->PRLL, 0x00008000);
    CLEAR_REG(hrtc->Instance->PRLH);

    /* Reset All CRH/CRL bits */
    CLEAR_REG(hrtc->Instance->CRH);
    CLEAR_REG(hrtc->Instance->CRL);
    
    if(RTC_ExitInitMode(hrtc) != HAL_OK)
    {
       /* record the error and remove the advisory lock */
       hrtc->State = HAL_RTC_STATE_ERROR; __HAL_UNLOCK(hrtc);
      
      return HAL_ERROR;
    }
  }

  /* wait for synchro*/
  HAL_RTC_WaitForSynchro(hrtc);

  /* clear RSF flag */
  CLEAR_BIT(hrtc->Instance->CRL, RTC_FLAG_RSF);
    
  /* de-initialize RTC MSP */
  HAL_RTC_MspDeInit(hrtc);

  /* record the state and release the advisory lock */
  hrtc->State = HAL_RTC_STATE_RESET; __HAL_UNLOCK(hrtc);

  return HAL_OK;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_GetAlarm(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Alarm, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_GetAlarm()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sAlarm); UNUSED(Alarm); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_GetDate(RTC_HandleTypeDef *hrtc, RTC_DateTypeDef *sDate, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_GetDate()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sDate); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* function to retrieve the State variale of the RTC_HandleTypeDef object */
/*------------------------------------------------------------------------*/
HAL_RTCStateTypeDef HAL_RTC_GetState(RTC_HandleTypeDef* hrtc) {return hrtc->State;}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_GetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_GetTime()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sTime); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* function to retrieve the static RTC_HandleTypeDef pointer              */
/*------------------------------------------------------------------------*/
RTC_HandleTypeDef *HAL_RTC_HandleGet(void) {return &hrtc;}

/*------------------------------------------------------------------------*/
/* function to initialize the RTC peripheral                              */
/*------------------------------------------------------------------------*/
/**
   This function initializes the RTC peripheral.  The RTC Prescaler
   should be programmed to generate the RTC 1Hz time base.  To read
   the calendar after wakeup from low power modes (Standby or Stop)
   the software must first wait for the RSF bit (Register Synchronized
   Flag) in the RTC_CRL register to be set by hardware.  The
   HAL_RTC_WaitForSynchro() function implements the above software
   sequence (RSF clear and RSF check).

      \begin{verbatim}
      input:
         hrtc....pointer to a RTC_HandleTypeDef structure that
                 contains the configuration information for RTC.

      output:
         If successful, this function returns zero or non-zero, on
         if an error occurred.
      \end{verbatim}
*/
HAL_StatusTypeDef HAL_RTC_Init(RTC_HandleTypeDef *hrtc)
{
  uint32_t prescaler = 0;
  
  /* Check input parameters */
  if(hrtc == NULL) {return HAL_ERROR;}
  
  /* Check the parameters */
  assert_param(IS_RTC_ALL_INSTANCE(hrtc->Instance));
  assert_param(IS_RTC_CALIB_OUTPUT(hrtc->Init.OutPut));
  assert_param(IS_RTC_ASYNCH_PREDIV(hrtc->Init.AsynchPrediv));
    
  if(hrtc->State == HAL_RTC_STATE_RESET)
  {
    /* Allocate lock resource and initialize it */
    hrtc->Lock = HAL_UNLOCKED;
    
    /* Initialize RTC MSP */
    HAL_RTC_MspInit(hrtc);
  }
  
  /* Set RTC state */  
  hrtc->State = HAL_RTC_STATE_BUSY;  
       
  /* Waiting for synchro */
  if(HAL_RTC_WaitForSynchro(hrtc) != HAL_OK)
  {
    /* Set RTC state */
    hrtc->State = HAL_RTC_STATE_ERROR;
    
    return HAL_ERROR;
  } 

  /* Set Initialization mode */
  if(RTC_EnterInitMode(hrtc) != HAL_OK)
  {
    /* Set RTC state */
    hrtc->State = HAL_RTC_STATE_ERROR;
    
    return HAL_ERROR;
  } 
  else
  { 
    /* Clear Flags Bits */
    CLEAR_BIT(hrtc->Instance->CRL, (RTC_FLAG_OW | RTC_FLAG_ALRAF | RTC_FLAG_SEC));
    
    if(hrtc->Init.OutPut != RTC_OUTPUTSOURCE_NONE)
    {
      /* Disable the selected Tamper pin */
      CLEAR_BIT(BKP->CR, BKP_CR_TPE);
    }
    
    /* Set the signal which will be routed to RTC Tamper pin*/
    MODIFY_REG(BKP->RTCCR, (BKP_RTCCR_CCO | BKP_RTCCR_ASOE | BKP_RTCCR_ASOS), hrtc->Init.OutPut);

    if (hrtc->Init.AsynchPrediv != RTC_AUTO_1_SECOND)
    {
      /* RTC Prescaler provided directly by end-user*/
      prescaler = hrtc->Init.AsynchPrediv;
    }
    else
    {
      /* RTC Prescaler will be automatically calculated to get 1 second timebase */
      /* Get the RTCCLK frequency */
      prescaler = HAL_RCCEx_GetPeriphCLKFreq(RCC_PERIPHCLK_RTC);

      /* Check that RTC clock is enabled*/
      if (prescaler == 0)
      {
        /* Should not happen. Frequency is not available*/
        hrtc->State = HAL_RTC_STATE_ERROR;
        return HAL_ERROR;
      }
      else
      {
        /* RTC period = RTCCLK/(RTC_PR + 1) */
        prescaler = prescaler - 1;
      }
    }
    
    /* Configure the RTC_PRLH / RTC_PRLL */
    MODIFY_REG(hrtc->Instance->PRLH, RTC_PRLH_PRL, (prescaler >> 16));
    MODIFY_REG(hrtc->Instance->PRLL, RTC_PRLL_PRL, (prescaler & RTC_PRLL_PRL));
      
    /* Wait for synchro */
    if(RTC_ExitInitMode(hrtc) != HAL_OK)
    {       
      hrtc->State = HAL_RTC_STATE_ERROR;
      
      return HAL_ERROR;
    }

    /* Set RTC state */
    hrtc->State = HAL_RTC_STATE_READY;
    
    return HAL_OK;
  }
}

/*------------------------------------------------------------------------*/
/* function to de-initialize the MCU specific package                     */
/*------------------------------------------------------------------------*/
/*
   This is a weak placeholder for a callback function that implements
   MCU-specific de-initialization.  

      \begin{verbatim}
      input:
         hrtc.....pointer to a RTC_HandleTypeDef structure that
                  contains the configuration information for RTC.
      \end{verbatim}
*/
__weak void HAL_RTC_MspDeInit(RTC_HandleTypeDef* hrtc)
{
   if(hrtc->Instance==RTC)
   {
      __HAL_RCC_RTC_DISABLE();
      HAL_NVIC_DisableIRQ(RTC_IRQn);
  }
}

/*------------------------------------------------------------------------*/
/* function to initialize the MCU specific package                        */
/*------------------------------------------------------------------------*/
/*
   This is a weak placeholder for a callback function that implements
   MCU-specific initialization.  

      \begin{verbatim}
      input:
         hrtc.....pointer to a RTC_HandleTypeDef structure that
                  contains the configuration information for RTC.
      \end{verbatim}
*/
__weak void HAL_RTC_MspInit(RTC_HandleTypeDef* hrtc)
{
   if(hrtc->Instance==RTC)
   {
      HAL_PWR_EnableBkUpAccess();
      __HAL_RCC_BKP_CLK_ENABLE();
      __HAL_RCC_RTC_ENABLE();
      HAL_NVIC_SetPriority(RTC_IRQn, 0, 0);
      HAL_NVIC_EnableIRQ(RTC_IRQn);
   }
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
__weak HAL_StatusTypeDef HAL_RTC_PollForAlarmAEvent(RTC_HandleTypeDef *hrtc, uint32_t Timeout)
{  
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_AlarmAEventCallback()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);
  
   /* Prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(Timeout);

  return HAL_ERROR;  
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetAlarm(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_SetAlarm()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sAlarm); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetAlarm_IT(RTC_HandleTypeDef *hrtc, RTC_AlarmTypeDef *sAlarm, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_SetAlarm_IT()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);
   
   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sAlarm); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetDate(RTC_HandleTypeDef *hrtc, RTC_DateTypeDef *sDate, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_SetDate()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);
   
   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sDate); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_SetTime(RTC_HandleTypeDef *hrtc, RTC_TimeTypeDef *sTime, uint32_t Format)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTC_SetTime()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);
   
   /* prevent unused argument(s) compilation warning */
   UNUSED(hrtc); UNUSED(sTime); UNUSED(Format); 

   return HAL_ERROR;
}

/*------------------------------------------------------------------------*/
/* function to pause for Stm32's RTC synchronization                      */
/*------------------------------------------------------------------------*/
HAL_StatusTypeDef HAL_RTC_WaitForSynchro(RTC_HandleTypeDef* hrtc)
{
  uint32_t tickstart = 0;
  
  /* Check input parameters */
  if(hrtc == NULL) {return HAL_ERROR;}
  
  /* Clear RSF flag */
  CLEAR_BIT(hrtc->Instance->CRL, RTC_FLAG_RSF);

  /* get timer reference */
  tickstart = HAL_GetTick();
  
  /* Wait the registers to be synchronised */
  while((hrtc->Instance->CRL & RTC_FLAG_RSF) == (uint32_t)RESET)
  {
     /* check for expiration of timeout */
     if((HAL_GetTick() - tickstart ) >  RTC_TIMEOUT_VALUE) {return HAL_TIMEOUT;} 
  }
  
  return HAL_OK;
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
__weak void HAL_RTCEx_RTCIRQHandler(RTC_HandleTypeDef* hrtc)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTCEx_RTCIRQHandler()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);
   
   /* call the error handler to reset the processor */
   ErrorHandler(FuncName);

   /* Prevent unused argument(s) compilation warning */
   UNUSED(hrtc);
}

/*------------------------------------------------------------------------*/
/* neutered HAL RTC API function                                          */
/*------------------------------------------------------------------------*/
__weak void HAL_RTCEx_TamperIRQHandler(RTC_HandleTypeDef *hrtc)
{
   /* function name for log entries */
   cc *FuncName = "HAL_RTCEx_TamperIRQHandler()";

   /* log the deprecation message */
   LogEntry(FuncName,DeprecatedMsg);

   /* call the error handler to reset the processor */
   ErrorHandler(FuncName);

   /* Prevent unused argument(s) compilation warning */
   UNUSED(hrtc);
}

/*------------------------------------------------------------------------*/
/* function to read the Stm32's RTC alarm register                        */
/*------------------------------------------------------------------------*/
/**
   This function reads Stm32 RTC alarm register.  On success, the
   value of the alarm register is returned.  Failure is signaled with
   a return value of (time_t)(-1).
*/
time_t ialarm(void)
{
   /* define some local work objects */
   time_t sec; unsigned char tics;

   /* read the interval alarm register */
   if (IntervalAlarmGet(&sec,&tics)<=0) sec=(time_t)(-1);

   return sec;
}

/*------------------------------------------------------------------------*/
/* disable the Stm32's RTC alarm                                          */
/*------------------------------------------------------------------------*/
/**
   This function disables the Stm32's RTC alarm and writes sentinel
   values to the RTC's alarm register.  If successful, this function
   returns a positive value, zero is returned on failure, or a
   negative value indicates an exception was encountered.
*/
int IntervalAlarmDisable(void)
{
   /* initialize return value */
   int status=RtcOk;

   /* disable the RTC alarm */
   if (HAL_RTC_DeactivateAlarm(&hrtc,RTC_ALARM_A) != HAL_OK) {status=RtcFail;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* enable the alarm for the Stm32's RTC                                   */
/*------------------------------------------------------------------------*/
/**
   This function enables the Stm32's RTC alarm but does not set the
   alarm time.  Prior to calling this function, the IntervalAlarmSet()
   function must be called to set the alarm.
*/
int IntervalAlarmEnable(void)
{
   /* function name for log entries */
   cc *FuncName = "IntervalAlarmEnable()";
      
   /* initialize return value */
   int status=RtcNull;

   /* local work object */
   unsigned long int sec;
   
   /* set an advisory lock on the RTC process and indicate a busy-state */
   __HAL_LOCK(&hrtc); hrtc.State = HAL_RTC_STATE_BUSY;

   /* read the Stm32's RTC alarm register */
   if ((status=Stm32RtcAlarmRead(&sec,NULL))>=RtcOk)
   {
      /* clear flag alarm A */
      __HAL_RTC_ALARM_CLEAR_FLAG(&hrtc, RTC_FLAG_ALRAF);
    
      /* configure the alarm interrupt */
      __HAL_RTC_ALARM_ENABLE_IT(&hrtc,RTC_IT_ALRA);
    
      /* RTC alarm interrupt Configuration: EXTI configuration */
      __HAL_RTC_ALARM_EXTI_ENABLE_IT(); __HAL_RTC_ALARM_EXTI_ENABLE_RISING_EDGE();
   }

   /* check for NULL function argument or mission RTC handle */
   else if (status==RtcNull) {LogEntry(FuncName,"Missing mission-time.\n");}

   /* check for unset alarm */
   else if (status==RtcAlarmNotSet) {LogEntry(FuncName,"RTC alarm-time not set.\n");}

   /* check for alarm overflow */
   else if (status==RtcOverflow) {LogEntry(FuncName,"Alarm time (%lu) exceeds maximum (%lu) "
                                           "of RTC model.",sec,MTimeMax);}

   /* catch-all for any other error condition */
   else {LogEntry(FuncName,"Uncaught exception: err=%d\n",status);}
        
   /* indicate ready-state and release the advisory lock */
   hrtc.State = HAL_RTC_STATE_READY; __HAL_UNLOCK(&hrtc); 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* mask the alarm interrupt so that ISR is not executed                   */
/*------------------------------------------------------------------------*/
/**
   This function masks the alarm interrupt so that the ISR is not
   executed if the interrupt occurs.  Note that this only delays
   execution of the ISR; if the interrupt is unmasked at a later time
   then the ISR will be immediately executed.
*/
int IntervalAlarmInterruptOff(void)
{
   /* configure the alarm interrupt */
   __HAL_RTC_ALARM_DISABLE_IT(&hrtc,RTC_IT_ALRA);

   return RtcOk;
}

/*------------------------------------------------------------------------*/
/* unmask the alarm interrupt so that ISR is executed                     */
/*------------------------------------------------------------------------*/
/**
   This function unsks the alarm interrupt so that the ISR is executed
   if the interrupt occurs.  Note that if an interrupt has already
   pending then unmasking the interrupt will induce the ISR to be
   executed immediately upon unmasking the interrupt.
*/
int IntervalAlarmInterruptOn(void)
{
   /* configure the alarm interrupt */
   __HAL_RTC_ALARM_ENABLE_IT(&hrtc,RTC_IT_ALRA);

   return RtcOk;
}

/*------------------------------------------------------------------------*/
/* function to read the interval-timer alarm setting from Stm32's RTC     */
/*------------------------------------------------------------------------*/
/**
   This function reads the time setting from the DS2404's interval-timer
   alarm register.  The interval-timer is a 5-byte binary counter that
   increments 256 times per second.  The least significant byte (lsb) is a
   count of fractional seconds (1/256 resolution) and the most significant
   4-bytes are a count of seconds.  When the interval-timer register equals
   the interval-timer alarm register then the alarm is activated.  If the
   control register is set properly then the alarm flag in the status
   register is asserted and an interrupt is generated.  See p5-8 of the
   DS2404 data sheet (referenced above).

      \begin{verbatim}
      output:

         sec....The number of seconds stored in the upper 4-bytes of the
                interval-timer alarm register.

         tics...The number of tics (1/256 seconds) stored in the lsb of the
                interval-timer alarm register.

         This function returns a positive number if at least the upper
         4-bytes of the interval-timer alarm register were successfully
         read.  A return value of 1 indicates that only the seconds portion
         of the register was read.  A return value of 2 indicates that both
         the seconds and tics were successfully read from the register.
         This function returns zero if it failed to read the seconds portion
         of the register in which case both of the function arguments will
         also be set to zero.  This function returns a negative number if
         either of the function arguments are invalid (NULL).
      \end{verbatim}
*/
int IntervalAlarmGet(time_t *sec,unsigned char *tics)
{
   /* initialize the function's return status */
   int status=RtcNull; unsigned long int mtime;

   /* validate integer size */
   assert(sizeof(time_t)==4);
   
   /* validate the function parameters */
   if (!sec) {status=RtcNull;}

   /* read the seconds from the RTC alarm register */
   else if (Stm32RtcAlarmRead(&mtime,tics)>0) {(*sec)=mtime; status=RtcOk;}

   /* initialize the return value and the function parameters */  
   else {status=RtcAlarmReadFail; (*sec)=0; if (tics) (*tics)=0;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* write the interval-timer alarm setting to the Stm32 RTC alarm register */
/*------------------------------------------------------------------------*/
/**
   This function writes the alarm setting to the Stm32's RTC alarm
   register.  The mission time is a 32-bit unsigned integer that
   increments 256 times per second.  Hence, the range of the
   mission-time is 2^24 seconds (194.2days) and its resolution is
   ~4msec (1/256th sec).

      \begin{verbatim}
      input:
         sec.....The alarm time to be written into the Stm32's RTC
                 alarm register. The valid range is [0,16777216] sec.

         tic.....The number of 1/256 second clock cycles that
                 represent the fraction of a second.

      output:
         This function returns a positive number if interval-timer alarm
         register was successfully written otherwise zero is returned.
      \end{verbatim}
*/
int IntervalAlarmSet(time_t sec,unsigned char tics)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* validate integer size */
   assert(sizeof(time_t)==4);

   /* check for overflow error */
   if (sec<0) {status=RtcUnderflow;}

   /* write the seconds to the interval-timer alarm register */
   else {status=Stm32RtcAlarmWrite(sec,tics);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to compute the time difference between two RTC tics           */
/*------------------------------------------------------------------------*/
/**
   This function represents a safe way to compute the difference
   between two RTC tics.  The result is measured in seconds and is
   positive if tic1 is greater than tic0.  The resolution is
   (1<<RtcTicBits) per second.

      \begin{verbatim}
      input:
         tic1 ... The final time (tics).
         tic0 ... The initial time (tics).
         
      output:
         This function returns the time difference between times tic1
         and tic0.  The result is measured in seconds and is positive
         if tic1 is greater than tic0.
      \end{verbatm}
*/
double IntervalTimerDiff(unsigned long int tics1, unsigned long int tics0)
{
   /* compute the difference in times with special attention to signedness */
   double dt = (tics0<=tics1) ? (double)(tics1-tics0) : -(double)(tics0-tics1);

   /* convert from RTC tics to seconds */
   dt /= (1<<RtcTicBits);
      
   return dt;
}

/*------------------------------------------------------------------------*/
/* function to read the Stm32's RTC register                              */
/*------------------------------------------------------------------------*/
/**
   This function reads the time setting from the Stm32's RTC register.
   The RTC register a 4-byte binary counter that is configured to
   increment 256 times per second.  The least significant byte (lsb)
   is a count of fractional seconds (1/256 resolution) and the most
   significant 3-bytes are a count of seconds.

      \begin{verbatim}
      output:

         sec....The number of seconds stored in the upper 3-bytes of the
                RTC register.

         tics...The number of tics (1/256 seconds) stored in the lsb of the
                RTC register.

         This function returns a positive number if RTC register was
         successfully read.  This function returns zero if it failed
         to read the RTC register in which case both of the function
         arguments will also be set to zero.  This function returns a
         negative number if either of the function arguments are
         invalid (NULL).
      \end{verbatim}
*/
int IntervalTimerGet(time_t *sec,unsigned char *tics)
{
   /* initialize the function's return status */
   int status=RtcNull; unsigned long int mtime;
   
   /* validate integer size */
   assert(sizeof(time_t)==4);

   /* validate the function parameters */
   if (!sec) {status=RtcNull;}

   /* read the seconds from the interval-timer register */
   else if ((status=Stm32RtcRead(&mtime,tics))>0) {(*sec)=mtime;}

   /* initialize the return value and the function parameters */      
   else {(*sec)=0; if (tics) (*tics)=0;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write the interval-timer setting to the DS2402             */
/*------------------------------------------------------------------------*/
/**
   This function writes the mission time to the Stm32's RTC register.
   The mission time is a 32-bit unsigned integer that increments 256
   times per second.  Hence, the range of the mission-time is 2^24
   seconds (194.2days) and its resolution is ~4msec (1/256th sec).

      \begin{verbatim}
      input:
         sec.....The mission-time to be written into the Stm32's RTC
                 register. The valid range is [0,16777216] sec.

         tic.....The number of 1/256 second clock cycles that
                 represent the fraction of a second.

      output:
         This function returns a positive number if interval-timer alarm
         register was successfully written otherwise zero is returned.
     \end{verbatim}
*/
int IntervalTimerSet(time_t sec,unsigned char tics)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* define some local work objects */
   time_t epoch,RefEpoch;
   
   /* validate integer size */
   assert(sizeof(time_t)==4);

   /* get the current unix epoch so that a new reference-epoch can be computed */
   if ((status=RtcGet(&epoch,NULL))<=0) {RefEpoch=0;}

   /* guard against underflow of the reference epoch */
   else if (epoch<(LONG_MIN+sec)) {RefEpoch=LONG_MIN;}
   
   /* compute the reference epoch and guard against overflow */
   else if ((RefEpoch=(epoch-sec))>RefEpochMax) {RefEpoch=RefEpochMax;}

   /* adjust the reference epoch to keep the current epoch fixed */
   if (Stm32RefEpochWrite(RefEpoch)<=0) {Stm32RefEpochWrite(0);}
   
   /* write the seconds to the interval-timer register */
   if ((status=Stm32RtcWrite(sec,tics))>0)
   {
      /* store the RTC tics at wake time */
      Stm32RtcTicsAtWakeSet((sec<<RtcTicBits)|tics);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the mission-time from the Stm32's RTC registers       */
/*------------------------------------------------------------------------*/
/**
   This function reads the Stm32's RTC registers to retrieve the
   32-bit RTC counter.  The mission time is a 32-bit unsigned integer
   that increments 256 times per second.  The resolution is ~4msec
   (1/256th sec).

      \begin{verbatim}
      output:
         On success, this function returns the Stm32 RTC counter.  On
         failure, this function returns (uint32_t)(-1).
      \end{verbatim}
*/
unsigned long int IntervalTimerTics(void)
{
   /* initialize the return value */
   unsigned long int RtcTics=ULONG_MAX;
 
   /* read the Stm32 RTC registers */
   uint16_t high1 = READ_REG(hrtc.Instance->CNTH & RTC_CNTH_RTC_CNT);
   uint16_t low   = READ_REG(hrtc.Instance->CNTL & RTC_CNTL_RTC_CNT);
   uint16_t high2 = READ_REG(hrtc.Instance->CNTH & RTC_CNTH_RTC_CNT);

   /* validate integer size */
   assert(sizeof(unsigned long int)==4);
   
   /* check if the high-word rolled over during read of CNTL,CNTH registers */ 
   if (high1!=high2) {low = READ_REG(hrtc.Instance->CNTL & RTC_CNTL_RTC_CNT);}
   
   /* compute the 32-bit RTC counter */
   RtcTics = ((high2<<16) | low);

   return RtcTics;
}

/*------------------------------------------------------------------------*/
/* function to read the Stm32 RTC                                         */
/*------------------------------------------------------------------------*/
/**
   This function reads Stm32 RTC register.  On success, the value of
   the interval-timer register is returned.  Failure is signaled with
   a return value of (time_t)(-1).
*/
time_t itimer(void)
{
   /* define some local work objects */
   time_t sec; unsigned char tics;

   /* read the interval timer register */
   if (IntervalTimerGet(&sec,&tics)<=0) sec=(time_t)(RtcNull);

   return sec;
}

/*------------------------------------------------------------------------*/
/* function to exit from INIT mode                                        */
/*------------------------------------------------------------------------*/
static HAL_StatusTypeDef RTC_EnterInitMode(RTC_HandleTypeDef* hrtc)
{
   /* initialize return value */
   uint32_t tickstart = 0;
  
   /* wait till RTC is in INIT state and if Time out is reached exit */
   for (tickstart=HAL_GetTick(); (hrtc->Instance->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET;)
   {
      /* check if the time-out period has expired */
      if((HAL_GetTick() - tickstart) >  RTC_TIMEOUT_VALUE) {return HAL_TIMEOUT;} 
   }

   /* disable the write protection for RTC registers */
   __HAL_RTC_WRITEPROTECTION_DISABLE(hrtc);
  
  return HAL_OK;  
}

/*------------------------------------------------------------------------*/
/* function to exit from INIT mode                                        */
/*------------------------------------------------------------------------*/
static HAL_StatusTypeDef RTC_ExitInitMode(RTC_HandleTypeDef* hrtc)
{
   /* initialize return value */
   int status = HAL_OK;

   /* define an object to contain the time-out reference */
   uint32_t tickstart = 0;
  
   /* enable the write protection for RTC registers */
   __HAL_RTC_WRITEPROTECTION_ENABLE(hrtc);
  
   /* wait till RTC is in INIT state or exit when time-out is reached */
   for (tickstart=HAL_GetTick(); (hrtc->Instance->CRL & RTC_CRL_RTOFF) == (uint32_t)RESET;)
   {
      /* check if the time-out period has expired */
      if((HAL_GetTick() - tickstart) >  RTC_TIMEOUT_VALUE) {status=HAL_TIMEOUT; break;} 
   }
   
   return status;  
}

/*------------------------------------------------------------------------*/
/* function to compute the unix epoch using the Stm32 RTC model           */
/*------------------------------------------------------------------------*/
/**
   This function computes the current unix epoch by reading and then
   adding the reference-epoch and the mission-time.  The RTC model is
   based on the equation:
      Epoch = RefEpoch + MissionTime
   where the reference-epoch is stored in the RTC's backup registers
   and the mission-time is maintained by the Stm32's RTC register.

      \begin{verbatim}
      output:
         sec....The current unix epoch which is the number of seconds
                since Jan 1, 1970 at 00:00:00 GMT.

         tics...The number of tics (1/256th seconds) read from the RTC
                register.

         This function returns a positive number if successful,
         otherwise, zero is returned.  This function returns a
         negative number if an exception was encountered.
      \end{verbatim}
*/
int RtcGet(time_t *sec,unsigned char *tics)
{
   /* initialize the function's return status */
   int status=RtcOk; unsigned long int mtime;

   /* validate integer sizes */
   assert(sizeof(time_t)==4 && sizeof(unsigned long int)==4);
   
   /* validate the function parameters */
   if (!sec) {status=RtcNull;}

   /* read the reference epoch from the RTC backup registers */
   else if (Stm32RefEpochRead(sec)<=0) {status=RtcRefEpochReadFail;}

   /* read the mission-time from the Stm32 RTC registers */
   else if (Stm32RtcRead(&mtime,tics)<=0)
   {
      /* initialize the return value and the function parameters */      
      status=RtcReadFail; (*sec)=0; if (tics) {(*tics)=0;}
   }

   /* check for overflow conditions */
   else if ((*sec)>0 && (LONG_MAX-(*sec))<mtime)
   {
      /* initialize the return value and the function parameters */      
      status=RtcOverflow; (*sec)=0; if (tics) {(*tics)=0;}
   }

   /* compute the current unix epoch */
   else {(*sec) += mtime;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write the real-time clock setting to the Stm32             */
/*------------------------------------------------------------------------*/
/**
   This function computes the reference-epoch so that the RTC model
   matches a user-specified epoch and then writes the reference-epoch
   to the Stm32's backup register.  The RTC model represents the
   current unix epoch as the sum of a reference-epoch plus the 
   mission-time which is stored in the Stm32's RTC register:
      Epoch = RefEpoch + MissionTime
   This function rearranges this equation to compute the
   reference-epoch from the mission-time and the specified epoch
   according to:
      RefEpoch = Epoch - MissionTime
   Checks are also made to guard against overflow or underflow of the
   specified epoch.

      \begin{verbatim}
      output:
         sec....The unix epoch to be represented by the RTC model. The
                unix epoch is equal to the number of seconds since Jan
                1, 1970 at 00:00:00 GMT.

         This function returns a positive number if successful,
         otherwise, zero is returned.
      \end{verbatim}
*/
int RtcSet(time_t sec)
{
   /* initialize the function's return status */
   int status=RtcFail;

   /* define some local work objects */
   time_t RefEpoch; unsigned long int mtime; unsigned char tics;

   /* validate integer size */
   assert(sizeof(time_t)==4);

   /* read the mission-time from the Stm32's RTC register */
   if (Stm32RtcRead(&mtime,&tics)<=0) {status=RtcReadFail;}

   /* guard against underflow of the reference-epoch */
   else if (sec<(LONG_MIN+(time_t)mtime)) {status=RtcUnderflow;}

   /* guard against overflow of the reference-epoch */
   else if ((RefEpoch=(sec-(time_t)mtime))>RefEpochMax) {status=RtcOverflow;}
   
   /* write the reference epoch to the RTC backup registers */
   else if (Stm32RefEpochWrite(RefEpoch)<=0) {status=RtcRefEpochWriteFail;}

   /* indicate success */
   else {status=RtcOk;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* store the alarm time in the Stm32's backup domain                      */
/*------------------------------------------------------------------------*/
/**
   This function retrieves the alarm time from the ALARM registers and
   stores them in RTC backup registers.  

      \begin{verbatim}
      output:
         On success, this function returns a positive value.  Zero is
         returned, on failure.  A negative value indicates that an
         exception occurred. 
      \end{verbatim}
*/
int Stm32AlarmSave(void)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* initialize a pointer to the base of the RTC backup registers */
   BKP_TypeDef *RtcRef = (BKP_TypeDef *)BKP_BASE;

   /* read two 16-bit words from the RTC alarm register */
   uint16_t high = READ_REG(hrtc.Instance->ALRH & RTC_CNTH_RTC_CNT);
   uint16_t low  = READ_REG(hrtc.Instance->ALRL & RTC_CNTL_RTC_CNT);

   /* store the low-order word in RTC backup register 3 */
   RtcRef->DR3 = (low & BKP_DR3_D);

   /* store the high-order word in RTC backup register 4 */
   RtcRef->DR4 = (high & BKP_DR4_D);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* retrieve the alarm time from the Stm32's backup registers              */
/*------------------------------------------------------------------------*/
/**
   This function retrieves the alarm-time from the Stm32's backup registers.

      \begin{verbatim}
      output:
         On success, this function returns a positive value.  Zero is
         returned, on failure.  A negative value indicates that an
         exception occurred. 
      \end{verbatim}
*/
int Stm32AlarmRecall(unsigned long int *AlarmTics)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* validate integer size */
   assert(sizeof(unsigned long int)==4);

   /* validate the function argument */
   if (!AlarmTics) {status=RtcNull;}

   else
   {
      /* initialize a pointer to the base of the RTC backup registers */
      BKP_TypeDef *RtcRef = (BKP_TypeDef *)BKP_BASE;

      /* read the low,high order words from RTC backup registers 3,4 */
      uint16_t low=(RtcRef->DR3),high=(RtcRef->DR4);

      /* assemble the 32-bit word */
      (*AlarmTics) = (((high)<<16) | low);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return Stm32's reference epoch from RTC backup register    */
/*------------------------------------------------------------------------*/
/**
  This function returns the reference epoch from the Stm32's backup
  register.  The reference epoch defines the unix epoch when the
  mission-time was zero.  The mission-time is maintained in the
  Stm32's RTC register.  The unix epoch is defined to be the sum of
  the reference epoch plus the mission-time.
*/
time_t Stm32RefEpoch(void)
{
   time_t RefEpoch=(time_t)(-1);

   /* read the reference epoch from the Stm32's backup register */
   if (Stm32RefEpochRead(&RefEpoch)<=0) {RefEpoch=(time_t)(-1);}

   return RefEpoch;
}

/*------------------------------------------------------------------------*/
/* function to read the reference epoch from the Stm32's backup registers */
/*------------------------------------------------------------------------*/
/**
   This function reads the reference unix epoch from the Stm32's backup
   registers.  The reference unix epoch represents the date/time when
   the mission-time was zero.  The current unix epoch (T), the
   reference unix epoch (Tr), and the mission-time (Tm) are related by
   the following equation: T = Tr + Tm.

      \begin{verbatim}
      output:
         sec.....The reference Unix epoch to be read from the Stm32's
                 RTC backup registers. The valid range is
                 [-2147483648,2130706431] sec.

         This function returns a positive integer on success or zero
         on failure.  A negative value indicates an exception was
         encountered. 
      \end{verbatim}
*/
static int Stm32RefEpochRead(time_t *sec)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* validate the function parameters */
   if (!sec) {status=RtcNull;}

   else
   {
      /* initialize a pointer to the base of the RTC backup registers */
      BKP_TypeDef *RtcRef = (BKP_TypeDef *)BKP_BASE;

      /* read the high-order word from RTC backup register 2 */
      uint32_t high = (RtcRef->DR2)&BKP_DR2_D;

      /* read the low-order word from RTC backup register 1 */
      uint32_t low = (RtcRef->DR1)&BKP_DR1_D;

      /* assemble the reference unix epoch */
      (*sec) = ((high<<16)|low);

      /* prevent overflow errors */
      if ((*sec)>RefEpochMax) {(*sec)=RefEpochMax;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write the reference epoch to the Stm32's backup registers  */
/*------------------------------------------------------------------------*/
/**
   This function writes the reference unix epoch to the Stm32's backup
   registers.  The reference unix epoch represents the date/time when
   the mission-time was zero.  The current unix epoch (T), the
   reference unix epoch (Tr), and the mission-time (Tm) are related by
   the following equation: T = Tr + Tm.

      \begin{verbatim}
      input:
         sec.....The reference Unix epoch to be written to the Stm32's
                 RTC backup registers. The valid range is
                 [-2147483648,2130706431] sec.

      output:
         This function returns a positive integer on success or zero
         on failure.  A negative value indicates an exception was
         encountered. 
      \end{verbatim}
*/
static int Stm32RefEpochWrite(time_t sec)
{
   /* initialize the function's return status */
   int status=RtcOk;

   /* validate integer size */
   assert(sizeof(long int)==4 && sizeof(unsigned long int)==4);

   /* validate the function argument */
   if (sec>RefEpochMax) {status=RtcOverflow;}

   else
   {
      /* initialize a pointer to the base of the RTC backup registers */
      BKP_TypeDef *RtcRef = (BKP_TypeDef *)BKP_BASE;

      /* store the low-order word in RTC backup register 1 */
      RtcRef->DR1 = (((unsigned long int)sec) & BKP_DR1_D);

      /* store the high-order word in RTC backup register 2 */
      RtcRef->DR2 = ((((unsigned long int)sec)>>16) & BKP_DR2_D);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the alarm time from the Stm32's RTC alarm register    */
/*------------------------------------------------------------------------*/
/**
   This function reads the 32-bit signed integer alarm from the
   Stm32's RTC alarm register.

   \begin{verbatim}
   output:
      sec....The mission time (seconds) when the alarm will expire.
   \end{verbatim}
*/
int Stm32RtcAlarmRead(unsigned long int *sec, unsigned char *tics)
{
   /* initialize the return value */
   int status=RtcOk;

   /* validate the RTC handle and function arguements */
   if (!sec) {status=RtcNull;}

   else
   {
      /* read two 16-bit words from the RTC alarm register */
      uint16_t high = READ_REG(hrtc.Instance->ALRH & RTC_CNTH_RTC_CNT);
      uint16_t low  = READ_REG(hrtc.Instance->ALRL & RTC_CNTL_RTC_CNT);

      /* compute number of seconds represented in Stm32's RTC alarm register */
      (*sec) = ((((unsigned long int)high)<<(16-RtcTicBits)) | (low>>RtcTicBits));

      /* compute number of clock tics represented in Stm32's RTC alarm register */
      if (tics) {(*tics) = (low&RtcTicMask);}

      /* check if the alarm-time has sentinel values to indicate unset condition */
      if ((high==RTC_ALARM_RESETVALUE_REGISTER && low==RTC_ALARM_RESETVALUE_REGISTER))
      {
         /* re-initialize the error condition and return values */
         status=RtcAlarmNotSet; (*sec)=RTC_ALARM_RESETVALUE; if (tics) {(*tics)=0xff;} 
      }
      else if ((*sec)>MTimeMax) {status=RtcOverflow;}
   }
   
  return status;
}

/*------------------------------------------------------------------------*/
/* function to read the alarm time from the Stm32's RTC alarm register    */
/*------------------------------------------------------------------------*/
/**
   This function writes a 32-bit unsigned integer alarm to the Stm32's
   RTC alarm register.

   \begin{verbatim}
   input:
      sec....The mission time (seconds) when the alarm will expire.
      tics...The marginal mission time (256th second) when the alarm
             will expire.

   ouput:
      This function returns a positive value on success or else zero
      on failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
static int Stm32RtcAlarmWrite(unsigned long int sec, unsigned char tics)
{
   /* initialize the return value */
   int status=RtcOk;

   /* check for overflow error */
   if (sec>=MTimeMax) {status=RtcOverflow;}

   /* compute RTC tics and write to the RTC alarm registers */
   else if ((status=Stm32RtcAlarmWriteTics((sec<<RtcTicBits) | (tics&RtcTicMask)))>0)
   {
      /* save the alarm in the back-up registers */
      Stm32AlarmSave();
   }
   
  return status;
}

/*------------------------------------------------------------------------*/
/* function to read the alarm time from the Stm32's RTC alarm register    */
/*------------------------------------------------------------------------*/
/**
   This function writes a 32-bit unsigned integer alarm to the Stm32's
   RTC alarm register.

   \begin{verbatim}
   input:
      AlarmTics....The mission time (tics) when the alarm will
                   expire.  Each tic represents 1/256th of a second. 

   ouput:
      This function returns a positive value on success or else zero
      on failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Stm32RtcAlarmWriteTics(unsigned long int AlarmTics)
{
   /* initialize the return value */
   int status=RtcOk;

   /* set initialization mode */      
   if(RTC_EnterInitMode(&hrtc) != HAL_OK) {status=RtcWriteFail;} 

   else
   {
      /* Set RTC alarm msb word */
      WRITE_REG(hrtc.Instance->ALRH, (AlarmTics >> 16));

      /* Set RTC alarm lsb word */
      WRITE_REG(hrtc.Instance->ALRL, (AlarmTics & RTC_ALRL_RTC_ALR));
   }
        
   /* wait for synchro */
   if(RTC_ExitInitMode(&hrtc) != HAL_OK) {status=RtcWriteFail;}
   
  return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Stm32 RTC for 256Hz operation.               */
/*------------------------------------------------------------------------*/
/**
   This function configures the Stm32 RTC for 256Hz operation per the
   SwiftWare STM32F103 RTC model (see description at the top of this
   file).

      \begin{verbatim}
      output:
         This function returns a positive integer on success or zero
         on failure.  A negative value indicates an exception was
         encountered. 
      \end{verbatim}
*/
int Stm32RtcConfig(void)
{
   /* initialize return value */
   int status=RtcOk;
      
   /* specify the desired RTC frequency */
   const uint32_t RtcHz = (1<<RtcTicBits);
   
   /* query the peripheral clock speed */
   const uint32_t LseHz = HAL_RCCEx_GetPeriphCLKFreq(RCC_PERIPHCLK_RTC);

   /* initialize the pointer to the RTC regsiter */
   hrtc.Instance = RTC;

   /* initialize the RTC to output on the tamper pin */
   hrtc.Init.OutPut = RTC_OUTPUTSOURCE_SECOND;
      
   /* compute the RTC prescaler: RtcHz = LseHz/(Prl[19:0]+1) */
   hrtc.Init.AsynchPrediv = LseHz/RtcHz - 1;

   /* use reset state to start the RTC */
   hrtc.State = HAL_RTC_STATE_RESET;

   /* initialize the advisory lock */
   hrtc.Lock = HAL_UNLOCKED;

   /* initialize the RTC */
   if (HAL_RTC_Init(&hrtc) != HAL_OK) {status=RtcFail;}
         
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the mission-time from the Stm32's RTC registers       */
/*------------------------------------------------------------------------*/
/**
   This function reads the mission-time from the Stm32's RTC
   registers.  The mission time is a 32-bit unsigned integer that
   increments 256 times per second.  Hence, the range of the
   mission-time is 2^24 seconds (194.2days) and its resolution is
   ~4msec (1/256th sec).

      \begin{verbatim}
      output:
         sec.....The mission-time to be read from the Stm32's RTC
                 register. The valid range is [0,16777216] sec.

         tic.....The number of 1/256th second clock cycles that
                 represent the fraction of a second.

         This function returns a positive integer on success or zero
         on failure.  A negative value indicates an exception was
         encountered. 
      \end{verbatim}
*/
static int Stm32RtcRead(unsigned long int *sec,unsigned char *tics)
{
   /* initialize the return value */
   int status=RtcOk;

   /* validate integer size */
   assert(sizeof(unsigned long int)==4);
 
   /* validate the RTC handle */
   if (!sec) {status=RtcNull;}
   
   else
   {
      /* read the Stm32 RTC registers */
      uint16_t high1 = READ_REG(hrtc.Instance->CNTH & RTC_CNTH_RTC_CNT);
      uint16_t low   = READ_REG(hrtc.Instance->CNTL & RTC_CNTL_RTC_CNT);
      uint16_t high2 = READ_REG(hrtc.Instance->CNTH & RTC_CNTH_RTC_CNT);

      /* check if the high-word rolled over during read of CNTL,CNTH registers */ 
      if (high1!=high2) {low = READ_REG(hrtc.Instance->CNTL & RTC_CNTL_RTC_CNT);}
      
      /* compute the number of seconds represented in the Stm32's RTC register */
      (*sec) = ((((unsigned long int)high2)<<(16-RtcTicBits)) | (low>>RtcTicBits));

      /* compute the number of clock tics represented in the Stm32's RTC register */
      if (tics) {(*tics) = (low&RtcTicMask);}
   }

  return status;
}

/*------------------------------------------------------------------------*/
/* function to write the mission-time to the Stm32's RTC registers        */
/*------------------------------------------------------------------------*/
/**
   This function writes the mission-time into the Stm32's RTC
   registers.  The mission time is a 32-bit unsigned integer that
   increments 256 times per second.  Hence, the range of the
   mission-time is 2^24 seconds (194.2days) and its resolution is
   ~4msec (1/256th sec).

      \begin{verbatim}
      input:
         sec.....The mission-time to be written into the Stm32's RTC
                 register. The valid range is [0,16777216] sec.

         tic.....The number of 1/256 second clock cycles that
                 represent the fraction of a second.

      output:
         This function returns a positive integer on success or zero
         on failure.  A negative value indicates an exception was
         encountered. 
      \end{verbatim}
*/
static int Stm32RtcWrite(unsigned long int sec, unsigned char tics)
{
   /* initialize the return value */
   int status=RtcOk;
  
   /* validate integer size */
   assert(sizeof(unsigned long int)==4);

   /* check for overflow error */
   if (sec>=MTimeMax) {status=RtcOverflow;}
   
   else
   {
      /* set initialization mode */      
      if(RTC_EnterInitMode(&hrtc) != HAL_OK) {status=RtcWriteFail;} 

      else
      {
         /* compute the tic-counter to be written into the RTC register */
         uint32_t TimeTics = ((sec<<RtcTicBits) | (tics&RtcTicMask));
         
         /* set RTC MSB word */
         WRITE_REG(hrtc.Instance->CNTH, (TimeTics >> 16));

         /* set RTC LSB word */
         WRITE_REG(hrtc.Instance->CNTL, (TimeTics & RTC_CNTL_RTC_CNT));
      }
      
      /* wait for synchro */
      if(RTC_ExitInitMode(&hrtc) != HAL_OK) {status=RtcWriteFail;}
   }
   
  return status;
}

/*------------------------------------------------------------------------*/
/* function to implement the ANSI C time() function using the Stm32's RTC */
/*------------------------------------------------------------------------*/
/**
   This function returns the unix epoch as defined by the Stm32's RTC
   API.  The unix epoch is equal to the number of seconds since Jan 1,
   1970 at 00:00:00 GMT.  This function is intended to implement the
   behavior and function of the ANSI C standard library function.  It
   is implemented as a wrapper-function for RtcGet().  Refer to the
   comment section of RtcGet() for details.

      \begin{verbatim} output:
         sec ... If this pointer is non-NULL then it will be initialized
                 with the value in the RTC counter register.  If NULL then
                 this function parameter is safely ignored.  

         On success, this function returns the value stored in the value in
         the RTC counter register.  On error, this function returns the
         special value: (time_t)(-1).
      \end{verbatim}
*/
time_t time(time_t *sec)
{
   /* define some local work objects */
   time_t T=(time_t)(RtcNull);;

   /* read the RTC register */
   if (RtcGet(&T,NULL)<=0) {T=(time_t)(RtcNull);}

   /* initialize the function parameter */
   if (sec) {*sec = T;} 

   return T;
}

#endif /* HAL_RTC_MODULE_ENABLED */
#endif /* STM32F103RTC_C */
