#ifndef STM32F103EXTI_H
#define STM32F103EXTI_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103ExtiChangeLog "$RCSfile$  $Revision$   $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 EXTI registers                                    */
/*------------------------------------------------------------------------*/
/**
   The ExtiReg is defined by the following 32-bit registers in the Stm32:     
      volatile unsigned long int IMR;   // interrupt mask register            
      volatile unsigned long int EMR;   // event mask register                
      volatile unsigned long int RTSR;  // rising trigger selection register  
      volatile unsigned long int FTSR;  // falling trigger selection register 
      volatile unsigned long int SWIER; // software interrupt event register  
      volatile unsigned long int PR;    // pending register                   
*/
typedef EXTI_TypeDef ExtiReg;

/* enumerate the EXTI trigger methods */
typedef enum {Rising,Falling,RisingFalling} ExtiTrigger;

/* declare functions with external linkage */
int  Stm32f103ExtiInit(void);
int  Stm32f103ExtiIrqAssert(unsigned char irq);
int  Stm32f103ExtiIrqClear(unsigned char irq);
int  Stm32f103ExtiIrqDisable(unsigned char irq);
int  Stm32f103ExtiIrqEnable(unsigned char irq, ExtiTrigger trig);
int  Stm32f103ExtiIrqMask(unsigned char irq);
int  Stm32f103ExtiIrqUnmask(unsigned char irq);

/* prototypes for the EXTI interrupt handlers */
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);

/* define the EXTI irq specifiers for each duart on the Apf11 */
extern const char SpareDuartIrq; /* Stm32:PG10 services interrupts for spare sensor port */
extern const char RfDuartIrq;    /* Stm32:PG11 services interrupts for RF port           */
extern const char Exp1DuartIrq;  /* Stm32:PG12 services interrupts for expansion-port 1  */
extern const char Exp2DuartIrq;  /* Stm32:PG12 services interrupts for expansion-port 2  */
extern const char Exp3DuartIrq;  /* Stm32:PG12 services interrupts for expansion-port 3  */
extern const char Exp4DuartIrq;  /* Stm32:PG12 services interrupts for expansion-port 4  */

/* define the return states of the Stm32 EXTI API */
extern const char ExtiInvalid;   /* invalid argument */
extern const char ExtiNotImp;    /* feature not implemented */
extern const char ExtiNull;      /* NULL function argument */
extern const char ExtiFail;      /* general failure */
extern const char ExtiOk;        /* general success */

#endif /* STM32F103EXTI_H */
#ifdef STM32F103EXTI_C
#undef STM32F103EXTI_C

#include <logger.h>
#include <stm32f1xx_hal.h>

/* define the return states of the Stm32 EXTI API */
const char ExtiInvalid     = -3; /* invalid argument */
const char ExtiNotImp      = -2; /* feature not implemented */
const char ExtiNull        = -1; /* NULL function argument */
const char ExtiFail        =  0; /* general failure */
const char ExtiOk          =  1; /* general success */

/* define the EXTI irq specifiers for each duart on the Apf11 */
const char SpareDuartIrq = 10; /* Stm32:PG10 services interrupts for spare sensor port */
const char RfDuartIrq    = 11; /* Stm32:PG11 services interrupts for RF port           */
const char Exp1DuartIrq  = 12; /* Stm32:PG12 services interrupts for expansion-port 1  */
const char Exp2DuartIrq  = 13; /* Stm32:PG13 services interrupts for expansion-port 2  */
const char Exp3DuartIrq  = 14; /* Stm32:PG14 services interrupts for expansion-port 3  */
const char Exp4DuartIrq  = 15; /* Stm32:PG15 services interrupts for expansion-port 4  */

/*------------------------------------------------------------------------*/
/* define the Stm32f103 AFIO registers                                    */
/*------------------------------------------------------------------------*/
/**
   The AfioReg is defined by the following 32-bit registers in the Stm32:
      volatile unsigned long int EVCR;
      volatile unsigned long int MAPR;
      volatile unsigned long int EXTICR[4];
      volatile unsigned long int MAPR2;  
*/
typedef AFIO_TypeDef AfioReg;

/* declare external functions used locally */
int SpareDuartIsr(void);
int RfDuartIsr(void);

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI0 interface            */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI0 interface.
*/
void EXTI0_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI0_IRQHandler()";

   /* clear the pending IRQ */
   Stm32f103ExtiIrqClear(0);

   /* log the error */
   LogEntry(FuncName,"ISR for EXTI0 not implemented.\n");
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI1 interface            */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI1 interface.
*/
void EXTI1_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI1_IRQHandler()";

   /* clear the pending IRQ */
   Stm32f103ExtiIrqClear(1);

   /* log the error */
   LogEntry(FuncName,"ISR for EXTI1 not implemented.\n");
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI2 interface            */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI2 interface.
*/
void EXTI2_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI2_IRQHandler()";

   /* clear the pending IRQ */
   Stm32f103ExtiIrqClear(2);

   /* log the error */
   LogEntry(FuncName,"ISR for EXTI2 not implemented.\n");
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI3 interface            */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI3 interface.
*/
void EXTI3_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI3_IRQHandler()";

   /* clear the pending IRQ */
   Stm32f103ExtiIrqClear(3);

   /* log the error */
   LogEntry(FuncName,"ISR for EXTI3 not implemented.\n");
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI4 interface            */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI4 interface.
*/
void EXTI4_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI4_IRQHandler()";

   /* clear the pending IRQ */
   Stm32f103ExtiIrqClear(4);

   /* log the error */
   LogEntry(FuncName,"ISR for EXTI4 not implemented.\n");
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for Stm32's EXTI[5-9] interfaces       */
/*------------------------------------------------------------------------*/
/**
   This function is an interrupt service routine (ISR) for the Stm32's
   EXTI[5-9] interfaces.
*/
void EXTI9_5_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI9_5_IRQHandler()";

   /* test if the interrupt was triggered by IRQ5 */
   if (EXTI->PR==0x0020U) 
   {
      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(5);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI5 not implemented.\n");
   }

   /* test if the interrupt was triggered by IRQ6 */
   if  (EXTI->PR==0x0040U) 
   {
      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(6);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI6 not implemented.\n");
   }

   /* test if the interrupt was triggered by IRQ7 */
   if  (EXTI->PR==0x0080U) 
   {
      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(7);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI7 not implemented.\n");
   }

   /* test if the interrupt was triggered by IRQ8 */
   if  (EXTI->PR==0x0100U) 
   {
      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(8);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI8 not implemented.\n");
   }
   
   /* test if the interrupt was triggered by IRQ9 */
   if  (EXTI->PR==0x0200U) 
   {
      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(9);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI9 not implemented.\n");
   }
}

/*------------------------------------------------------------------------*/
/* interrupt service routine (ISR) for EXTI[10-15] interfaces             */
/*------------------------------------------------------------------------*/
/**
   This is an interrupt service routine (ISR) for the Stm32's
   EXTI[10-15] interfaces.
*/
void EXTI15_10_IRQHandler(void)
{
   /* define the logging signature */
   cc *FuncName="EXTI15_10_IRQHandler()";

   /* record the configuration of the EXTI interrupt mask register */
   volatile const unsigned long int imr=EXTI->IMR;

   /* test if the interrupt was triggered by the spare sensor duart */
   if (EXTI->PR==(0x1U<<SpareDuartIrq)) 
   {
      /* disable all EXTI interrupts except for the Spare duart */
      EXTI->IMR=(0x1U<<SpareDuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(SpareDuartIrq);

      /* execute the ISR for the spare-sensor duart */
      SpareDuartIsr();
   }

   /* test if the interrupt was triggered by the RF duart */
   if  (EXTI->PR==(0x1U<<RfDuartIrq)) 
   {
      /* disable all EXTI interrupts except for the RF duart */
      EXTI->IMR=(0x1U<<RfDuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(RfDuartIrq);
      
      /* execute the ISR for the RF interface DUART */
      RfDuartIsr();
   }

   /* test if the interrupt was triggered by duart for expansion port 1 */
   if  (EXTI->PR==(0x1U<<Exp1DuartIrq))
   {
      /* disable all EXTI interrupts except for the EXP1 duart */
      EXTI->IMR=(0x1U<<Exp1DuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(Exp1DuartIrq);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI%d not implemented.\n",Exp1DuartIrq);
   }

   /* test if the interrupt was triggered by duart for expansion port 2 */
   if  (EXTI->PR==(0x1U<<Exp2DuartIrq))
   {
      /* disable all EXTI interrupts except for the EXP2 duart */
      EXTI->IMR=(0x1U<<Exp2DuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(Exp2DuartIrq);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI%d not implemented.\n",Exp2DuartIrq);
   }
   
   /* test if the interrupt was triggered by duart for expansion port 3 */
   if  (EXTI->PR==(0x1U<<Exp3DuartIrq))
   {
      /* disable all EXTI interrupts except for the EXP3 duart */
      EXTI->IMR=(0x1U<<Exp3DuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(Exp3DuartIrq);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI%d not implemented.\n",Exp3DuartIrq);
   }
   
   /* test if the interrupt was triggered by duart for expansion port 4 */
   if  (EXTI->PR==(0x1U<<Exp4DuartIrq))
   {
      /* disable all EXTI interrupts except for the EXP4 duart */
      EXTI->IMR=(0x1U<<Exp4DuartIrq);

      /* clear the pending IRQ */
      Stm32f103ExtiIrqClear(Exp4DuartIrq);
      
      /* log the error */
      LogEntry(FuncName,"ISR for EXTI%d not implemented.\n",Exp4DuartIrq);
   }
   
   /* restore the configuration of the EXTI interrupt mask register */
   EXTI->IMR=imr;
}

/*------------------------------------------------------------------------*/
/* function to initialize the EXTI interface of the Stm32f103             */
/*------------------------------------------------------------------------*/
/**
   This function initializes the Stm32's EXTI interface to match the
   state after a reset of the Stm32.

   \begin{verbatim}
   output:
      This function returns a positive value if successful or else
      zero if the reconfiguration failed from some reason.  A negative
      return value indicates an exception was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiInit(void)
{
   /* initialize return value */
   int status=ExtiOk;

   /* disable interrupt generation and confirm */
   EXTI->IMR=0; if (EXTI->IMR) {status=ExtiFail;}
   
   /* disable event generation and confirm */
   EXTI->EMR=0; if (EXTI->EMR) {status=ExtiFail;}

   /* disable rising-trigger selection and confirm */
   EXTI->RTSR=0; if (EXTI->RTSR) {status=ExtiFail;}

   /* disable falling-trigger selection and confirm */
   EXTI->FTSR=0; if (EXTI->FTSR) {status=ExtiFail;}

   /* reset software interrupt and pending registers */
   EXTI->SWIER=0; EXTI->PR=0x000fffffUL; 

   /* confirm that interrupts and pending registers have been reset */
   if (EXTI->SWIER || EXTI->PR) {status=ExtiFail;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to trigger an interrupt on a specified EXTI interface         */
/*------------------------------------------------------------------------*/
/**
   This function triggers an interrupt on a specified EXTI interface.

   \begin{verbatim}
   input:

      irq....The EXTI interface specifier in the range [0,15].

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqAssert(unsigned char irq)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqAssert()";
   
   /* initialize the return value */
   int status=ExtiOk;
         
   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or unimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiInvalid;
   }

   /* trigger an interrupt of the EXTI interface */
   else {((ExtiReg *)EXTI)->SWIER |= (0x1U<<irq);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear an interrupt on a specified EXTI interface           */
/*------------------------------------------------------------------------*/
/**
   This function clears an interrupt on a specified EXTI interface.

   \begin{verbatim}
   input:

      irq....The EXTI interface specifier in the range [0,15].

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqClear(unsigned char irq)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqClear()";
   
   /* initialize the return value */
   int status=ExtiOk;
         
   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or unimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiInvalid;
   }

   /* clear an interrupt of the EXTI interface */
   else {((ExtiReg *)EXTI)->PR |= (0x1U<<irq);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable interrupts for a specified EXTI interface          */
/*------------------------------------------------------------------------*/
/**
   This function disables interrupts for a specified EXTI interface.

   \begin{verbatim}
   input:

      irq....The EXTI interface specifier in the range [0,15].

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqDisable(unsigned char irq)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqDisable()";

   /* initialize return value */
   int status=ExtiOk;

   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or unimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiInvalid;
   }
   
   else
   {
      /* local work object */
      unsigned char IRQ=0xffU;
      
      /* clear interrupts that are active-low */
      EXTI->FTSR &= ~(0x1<<irq);

      /* clear interrupts that are active-high */
      EXTI->RTSR &= ~(0x1<<irq);

      /* clear any pensing interrupts */
      EXTI->PR |= (0x1<<irq);

      /* deactivate interrupts */
      EXTI->IMR &= ~(0x1<<irq);

      /* select the IRQ for the specified EXTI interface */
      switch (irq)
      {
         /* select irq for EXT0 */
         case 0: {IRQ=EXTI0_IRQn; break;}
            
         /* select irq for EXT1 */
         case 1: {IRQ=EXTI1_IRQn; break;}
            
         /* select irq for EXT2 */
         case 2: {IRQ=EXTI2_IRQn; break;}
            
         /* select irq for EXT3 */
         case 3: {IRQ=EXTI3_IRQn; break;}
            
         /* select irq for EXT4 */
         case 4: {IRQ=EXTI4_IRQn; break;}
            
         /* select irq for EXT[5-9] */
         case 5: case 6: case 7: case 8: case 9: {IRQ=EXTI9_5_IRQn; break;}
            
         /* select irq for EXT[10-15] */
         case 10: case 11: case 12: case 13: case 14: case 15:
         {
            /* define a pointer to the AFIO interface */
            AfioReg *afio=AFIO;

            /* clear the EXTI configuration */
            afio->EXTICR[irq>>2] &= ~(0xfU<<(4*(irq&0x3)));

            /* assign the IRQ */
            IRQ=EXTI15_10_IRQn; break;
         }

         /* inhibit selection of invalid EXTI interface specifier */
         default: {LogEntry(FuncName,"Invalid or unimplemented interrupt/event "
                            "specifier: EXTI%d\n",irq); status=ExtiInvalid;}
      }

      /* compute/set the priority and the IRQ in the NVIC controller */
      if (IRQ!=0xffU) {HAL_NVIC_DisableIRQ(IRQ);}
   }
   
   return status;
}


/*------------------------------------------------------------------------*/
/* function to enables interrupts for a specified EXTI interface          */
/*------------------------------------------------------------------------*/
/**
   This function enables interrupts for a specified EXTI interface.

   \begin{verbatim}
   input:

      irq....The EXTI interface specifier in the range [0,15].

   output:
      This function returns a positive value if successful or else
      zero upon failure.  A negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqEnable(unsigned char irq, ExtiTrigger trigger)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqEnable()";

   /* initialize return value */
   int status=ExtiOk;

   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or nonimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiNotImp;
   }

   /* validate the EXTI trigger */
   else if (!(trigger==Rising || trigger==Falling || trigger==RisingFalling))
   {
      /* log the invalid trigger specifier */
      LogEntry(FuncName,"Invalid trigger selection(%d) for "
               "EXTI%d.\n",trigger,irq);status=ExtiInvalid;
   }
   
   else
   {
      /* local work object */
      unsigned char IRQ=0xffU;
      
      /* configure interrupts that are active-low */
      if (trigger==Falling || trigger==RisingFalling) {EXTI->FTSR|=(0x1<<irq);}

      /* configure interrupts that are active-high */
      if (trigger==Rising || trigger==RisingFalling) {EXTI->RTSR|=(0x1<<irq);}

      /* clear any pensing interrupts */
      EXTI->PR |= (0x1<<irq);

      /* activate interrupts */
      EXTI->IMR |= (0x1<<irq);

      /* Enable AFIO Clock */
      __HAL_RCC_AFIO_CLK_ENABLE();

      /* select the IRQ for the specified EXTI interface */
      switch (irq)
      {
         /* select irq for EXT0 */
         case 0: {IRQ=EXTI0_IRQn; break;}
            
         /* select irq for EXT1 */
         case 1: {IRQ=EXTI1_IRQn; break;}
            
         /* select irq for EXT2 */
         case 2: {IRQ=EXTI2_IRQn; break;}
            
         /* select irq for EXT3 */
         case 3: {IRQ=EXTI3_IRQn; break;}
            
         /* select irq for EXT4 */
         case 4: {IRQ=EXTI4_IRQn; break;}
            
         /* select irq for EXT[5-9] */
         case 5: case 6: case 7: case 8: case 9: {IRQ=EXTI9_5_IRQn; break;}
            
         /* select irq for EXT[10-15] */
         case 10: case 11: case 12: case 13: case 14: case 15:
         {
            /* define a pointer to the AFIO interface */
            AfioReg *afio=AFIO;

            /* clear the EXTI configuration in AFIO control registers */
            afio->EXTICR[irq>>2] &= ~(0xfU<<(4*(irq&0x3)));

            /* configure the EXTI for interrupts on Stm32 port G (0x6) */
            afio->EXTICR[irq>>2] |=  (0x6U<<(4*(irq&0x3)));

            /* assign the IRQ */
            IRQ=EXTI15_10_IRQn; break;
         }

         /* inhibit selection of invalid EXTI interface specifier */
         default: {LogEntry(FuncName,"Invalid or unimplemented interrupt/event "
                            "specifier: EXTI%d\n",irq); status=ExtiNotImp;}
      }

      /* compute/set the priority and the IRQ in the NVIC controller */
      if (IRQ!=0xffU) {HAL_NVIC_SetPriority(IRQ, 1, irq&0x7U); HAL_NVIC_EnableIRQ(IRQ);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to mask an EXTI interrupt                                     */
/*------------------------------------------------------------------------*/
/**
   This function masks a specified EXTI interrupt so that interrupts
   will not be generated even if the associated interrupt condition
   occurs.

   \begin{verbatim}
   input:
      irq....The identifier for the EXTI interrupt which must be in
             the rage [0,15].

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqMask(unsigned char irq)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqMask()";

   /* initialize return value */
   int status=ExtiOk;

   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or nonimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiNotImp;
   }

   /* mask the interrupt */
   else {EXTI->IMR &= ~(0x1<<irq);}

   return status;
}


/*------------------------------------------------------------------------*/
/* function to unmask an EXTI interrupt                                   */
/*------------------------------------------------------------------------*/
/**
   This function unmasks a specified EXTI interrupt so that interrupts
   will be generated if the associated interrupt condition occurs.

   \begin{verbatim}
   input:
      irq....The identifier for the EXTI interrupt which must be in
             the rage [0,15].

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Stm32f103ExtiIrqUnmask(unsigned char irq)
{
   /* define the logging signature */
   cc *FuncName="Stm32f103ExtiIrqUnmask()";

   /* initialize return value */
   int status=ExtiOk;

   /* validate the EXTI IRQ specifier */
   if (!(irq>=0 && irq<=15))
   {
      /* log the invalid interrupt specifier */
      LogEntry(FuncName,"Invalid or nonimplemented interrupt/event "
               "specifier: EXTI%d\n",irq); status=ExtiNotImp;
   }

   /* unmask the interrupt */
   else {EXTI->IMR |= (0x1<<irq);}

   return status;
}

#endif /* STM32F103EXTI_C */
