#ifndef FATIO_H
#define FATIO_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define fatioChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>
#include <fatsys.h>

#define BLOCKSIZE (KB/2)
#define FILESIZE  (64*KB-BLOCKSIZE)
#define MAXFILES  (NFIO)
#define BUFSIZE   (FILESIZE)

/* define the structure for file control blocks */
typedef struct
{
   unsigned short int fpos;
   unsigned short int len;
   char fname[FILENAME_MAX+1];
   unsigned char buf[BUFSIZE];
} FioBlk;

/* define file control blocks */
extern persistent FioBlk fioblk_[NFIO];

/* function prototypes */
int fioClean(void);
int fioCreate(void);
int fioFind(const char *path);
int fioFindFatFs(const char *fname, const char *dir);
int fioFindRamFs(const char *fname);
int fioGet(const char *fname,FioBlk *fioblk);
int fioInit(void);
int fioLen(int fdes);
int fioName(int fdes,char *fname,size_t bufsize);
int fioPut(const FioBlk *fioblk);
int fioRename(const char *oldfname,const char *newfname);
int fioUnlink(const char *fname);
int fioWipe(FioBlk *fioblk);

#endif /* FATIO_H */
#ifdef FATIO_C
#undef FATIO_C

#include <diskio.h>
#include <ff.h>
#include <logger.h>
#include <string.h>

/* define file control blocks */
persistent FioBlk fioblk_[NFIO];

/*------------------------------------------------------------------------*/
/* erase all files from the root & archive directories of FatFs volume    */
/*------------------------------------------------------------------------*/
/**
   This function cleans the root and archive directories of the FatFs
   volume by deleting any existing files.  On success, this function
   returns a positive value.  On failure, this function returns zero.
   A negative return value indicates an exceptional condition.
*/
int fioClean(void)
{
   /* define the logging signature */
   cc *FuncName="fioClean()";
   
   /* initialize the RAM file system */
   int status=1; errno=0;

   /* mount the FatFs volume */
   if (fat_mount())
   {
      /* define objects used for FatFs operations */
      DIR dp; FILINFO finfo; FRESULT fstatus;
      
      /* search the directory for any files */
      if (f_findfirst(&dp,&finfo,"","*.*")==FR_OK && finfo.fname[0]) do
      {
         /* delete the file */
         if ((fstatus=f_unlink(finfo.fname))!=FR_OK)
         {
            /* log the deletion error */
            LogEntry(FuncName,"Failed to unlink: \"%s\" [FatErr=%d]\n",
                     finfo.fname,fstatus); status=0;
         }
      }

      /* find the next file in the root directory of the FatFs volume */
      while (f_findnext(&dp,&finfo)==FR_OK && finfo.fname[0]);
       
      /* close the FatFs directory */
      if ((fstatus=f_closedir(&dp))!=FR_OK)
      {
         /* log the error */
         LogEntry(FuncName,"Attempt to close FatFs directory failed. "
                  "[FatErr=%d]\n",fstatus); errno=EIO;
      }

      /* search the directory for any files */
      if (f_findfirst(&dp,&finfo,arcdir,"*.*")==FR_OK && finfo.fname[0]) do
      {
         /* define a buffer to hold the pathname for the file to unlink */
         char path[2*FILENAME_MAX+2];

         /* create a pathname for the file to delete */
         snprintf(path,sizeof(path),"%s/%s",arcdir,finfo.fname);
         
         /* delete the file */
         if ((fstatus=f_unlink(path))!=FR_OK)
         {
            /* log the deletion error */
            LogEntry(FuncName,"Failed to unlink: \"%s\" "
                     "[FatErr=%d]\n",path,fstatus); status=0;
         }
      }
      
      /* find the next file in the archive directory of the FatFs volume */
      while (f_findnext(&dp,&finfo)==FR_OK && finfo.fname[0]);
       
      /* close the FatFs directory */
      if ((fstatus=f_closedir(&dp))!=FR_OK)
      {
         /* log the error */
         LogEntry(FuncName,"Attempt to close FatFs directory failed. "
                  "[FatErr=%d]\n",fstatus); errno=EIO;
      }
   }

   /* indicate failure to mount the FatFs volume */
   else  {status=0; errno=EIO;}
   
   /* unmount the FatFs volume */
   if (!fat_umount()) {errno=EIO;} 

   return status;
}

/*------------------------------------------------------------------------*/
/* function to create the RAM and FatFs file systems                      */
/*------------------------------------------------------------------------*/
/**
   This function creates the RAM and FatFs file systems as well as an
   archive directory in which previously transmitted files will be
   permanently stored.  This function might require several minutes to
   complete depending on the size of the SD card to be formatted.

   On success, a positive value is returned; else zero is returned if
   any of the operations failed.  If an exception is encountered, then
   errno is set accordingly.
*/
int fioCreate(void)
{
   /* define the logging signature */
   cc *FuncName="fioCreate()";
   
   /* initialize the return value */
   int status=0; errno=0;

   /* define objects used for local FatFs operations */
   DRESULT dstatus; FRESULT fstatus; const time_t Tref=time(NULL);

   /* create the RAM file system */
   if (fformat()<=0) {errno=ENOEXEC;}

   /* make sure the FatFs volume starts out unmounted */
   if (!fat_umount()) {errno=EIO;} 

   /* create the FatFs volume (warning: all data lost) */
   else if ((dstatus=disk_ioctl(0,FAT_MKFS,&fstatus))!=RES_OK || fstatus!=FR_OK)
   {
      /* log the error */
      LogEntry(FuncName,"Failed to create FatFs volume. "
               "[FatErr=%d,%d]\n",dstatus,fstatus);
   }
   
   /* mount the FatFs volume */
   else if (fat_mount())
   {
      /* create the FatFs archive directory */
      if ((fstatus=f_mkdir(arcdir))!=FR_OK)
      {
         /* log the error */
         LogEntry(FuncName,"Failed to create directory: \"%s\" [FatErr=%d,dT=%0.0f]\n",
                  arcdir,fstatus,difftime(time(NULL),Tref)); errno=ENOENT;
      }

      /* check criteria for logging the time to create the file system */
      else {LogEntry(FuncName,"FatFs & RAM file systems created in %0.0f "
                     "seconds.\n",difftime(time(NULL),Tref)); status=1;}
   }
   
   /* indicate FatFs mount failure */
   else {errno=EIO;}
   
   /* unmount the FatFs volume */
   if (!fat_umount()) {errno=EIO;} 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to find a file in the RAM file system or FatFs volume         */
/*------------------------------------------------------------------------*/
/**
   This function searches the FatFs and the RAM file system for a file
   with a specified filename.  The specified filename must valid as
   determined by fnameok().  On success, this function returns the
   file descriptor of the FioBlk containing the file or else NFIO, if
   the file is found in the FatFs volume.  On failure, this function
   returns a negative value.
*/
int fioFind(const char *fname)
{
   /* initialize the return value */
   int fdes=EOF;

   /* guard against invalid file names */
   if (fnameok(fname)<=0) {errno=EINVAL;}

   /* first, seek the file name in the RAM file system */
   else if ((fdes=fioFindRamFs(fname))<0)
   {
      /* next, seek the file name in the FatFs volume */
      fdes=fioFindFatFs(fname,NULL);
   }
   
   return fdes;
}

/*------------------------------------------------------------------------*/
/* function to find a file in the FatFs volume                            */
/*------------------------------------------------------------------------*/
/**
   This function searches the FatFs volume for a file with a specified
   filename.  The specified filename must valid as determined by
   fnameok().  On success, this function returns NFIO, if the file is
   found in the FatFs volume.  On failure, this function returns a
   negative value.
*/
int fioFindFatFs(const char *fname, const char *dir)
{
   /* define the logging signature */
   cc *FuncName="fioFindFatFs()";

   /* initialize the return value */
   int fdes=EOF;

   /* guard against an invalid file name */
   if (fnameok(fname)>0)
   {
      /* mount the FatFs volume */
      if (fat_mount())
      {
         /* define objects used for FatFs operations */
         DIR dp; FILINFO finfo; FRESULT fstatus; 

         /* validate the directory to search */
         if (!dir) {dir="";}
         
         /* search the directory for a matching file */
         if (f_findfirst(&dp,&finfo,dir,fname)==FR_OK && finfo.fname[0]) {fdes=NFIO;}

         /* set errno to  indicate file not found */
         else {errno=ENOENT;}
       
         /* close the FatFs directory */
         if ((fstatus=f_closedir(&dp))!=FR_OK)
         {
            /* log the error */
            LogEntry(FuncName,"Attempt to close FatFs directory failed. "
                     "[FatErr=%d]\n",fstatus); errno=EIO;
         }
      }
   
      /* indicate FatFs mount failure */
      else {errno=EIO;}
   
      /* unmount the FatFs volume */
      if (!fat_umount()) {errno=EIO;} 
   }
   
   /* indicate invalid file name */
   else {errno=EINVAL;}
 
   return fdes;
}

/*------------------------------------------------------------------------*/
/* function to find a file in the RAM file system                         */
/*------------------------------------------------------------------------*/
/**
   This function searches the RAM file system for a file with a
   specified filename.  The specified filename must valid as
   determined by fnameok().  On success, this function returns the
   file descriptor of the FioBlk containing the file.  On failure,
   this function returns a negative value.
*/
int fioFindRamFs(const char *fname)
{
   /* initialize the return value */
   int i,fdes=EOF;
   
   /* validate the function argument */
   if (!fname) {errno=EINVAL;}

   /* guard against a null file name */
   else if (!(*fname) || fnameok(fname)<=0) {errno=EINVAL;}

   /* loop through each of FioBlk in the RAM file system */
   else for (i=0; i<NFIO; i++)
   {
      /* compare the filename to the one in the FioBlk object */
      if (!strcmp(fname,fioblk_[i].fname)) {fdes=i; break;}
   }
   
   return fdes;
}
   
/*------------------------------------------------------------------------*/
/* function to get an FioBlk from the FatFs volume                        */
/*------------------------------------------------------------------------*/
/**
   This function reads a file from the FatFs volume.  On success, this
   function returns a non-negative value equal to the number of bytes
   read from the FatFs volume. On failure, a negative value is
   returned if the file was not read.
*/
int fioGet(const char *fname,FioBlk *fioblk)
{
   /* define the logging signature */
   cc *FuncName="fioGet()";

   /* initialize the return value */
   int status=EOF; 

   /* guard against invalid function arguments */
   if (fioblk && fnameok(fname)>0)
   {
      /* mount the FatFs volume */
      if (fat_mount())
      {
         /* define objects used for FatFs operations */
         FIL fp; FRESULT fstatus; unsigned int n;

         /* wipe the FioBlk clean */
         fioWipe(fioblk);
      
         /* open an existing file in read mode */
         if ((fstatus=f_open(&fp,fname,FA_READ|FA_OPEN_EXISTING))==FR_OK)
         {
            /* read the entire file from the FatFs file */
            if ((fstatus=f_read(&fp,(fioblk->buf),BUFSIZE,&n))==FR_OK && n>0)
            {
               /* initialize the file position and length */
               fioblk->fpos=0; fioblk->len=n; status=fioblk->len;
      
               /* write the file name to the FioBlk object */
               strncpy(fioblk->fname,fname,FILENAME_MAX); fioblk->fname[FILENAME_MAX]=0;
            }
         
            /* log the read error */
            else {LogEntry(FuncName,"FatFs read failed for \"%s\" after %u bytes. "
                           "[FatErr=%d]\n",fname,fioblk->len,fstatus); errno=EIO;}
         }
   
         /* log the open failure */
         else {LogEntry(FuncName,"FatFs open failed: %s [FatErr=%d]\n",fname,fstatus);}

         /* close the FatFs file */
         if ((fstatus=f_close(&fp))!=FR_OK) 
         {
            /* log the close failure */
            LogEntry(FuncName,"FatFs close failed: %s [FatErr=%d]\n",fname,fstatus);
         }
      }
    
      /* indicate FatFs mount failure */
      else {errno=EIO;}
  
      /* unmount the FatFs volume */
      if (!fat_umount()) {errno=EIO;} 
   }
   
   /* indicate invalid function arguments */
   else {errno=EINVAL;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to format the RAM file system                                 */
/*------------------------------------------------------------------------*/
/**
   This function initializes the FioBlk objects in the RAM file
   system.  On success, this function returns a positive value.  On
   failure, this function returns zero.
*/
int fioInit(void)
{
   /* loop through each of FioBlk */
   int i; for (i=0; i<NFIO; i++) {memset(&fioblk_[i],0,sizeof(FioBlk));}
   
   return 1;
}

/*------------------------------------------------------------------------*/
/* function to get the length of a file in the RAM file system            */
/*------------------------------------------------------------------------*/
/**
   This function returns the length of a file in the RAM file system.  On
   failure, this function returns a negative value
*/
int fioLen(int fdes)
{
   long len=-1;

   /* validate the fdes and get the file length from the FioBlk object */
   if (fdes>=0 && fdes<NFIO)
   {
      /* validate the FioBlk object */
      if (fnameok(fioblk_[fdes].fname)>0) len=fioblk_[fdes].len;

      /* validate the length */
      if (len<0 || len>=BUFSIZE) len=-1;
   }

   return len;
}

/*------------------------------------------------------------------------*/
/* function to return the name of a file in the RAM file system           */
/*------------------------------------------------------------------------*/
/**
   This function returns the name of a file in the RAM filesystem that is
   associated with a specified fdes.

   \begin{verbatim}
   input:
      fdes.......The file identifer for the file in the RAM
                 filesystem.
      bufsize...The maximum length of the 'fname' buffer including the
                terminating NULL character.  If the filename is longer
                than bufsize-1 then it will be truncated.

   output:
      fname.....The name of the file associated with the specified fdes.
   \end{verbatim}
*/
int fioName(int fdes,char *fname,size_t bufsize)
{
   int status=-1;

   /* validate the file name */
   if (fname)
   {
      /* initialize the filename */
      *fname=0;

      /* validate the remaining function arguments */
      if (fdes>=0 && fdes<NFIO && bufsize>1)
      {
         /* copy the file name from the FioBlk object */
         strncpy(fname,fioblk_[fdes].fname,bufsize-1);

         /* make sure the filename is terminated */
         fname[bufsize-1]=0;

         /* check if the file name is valid */
         status = (fnameok(fname)>0) ? 1 : 0;
      }
   }
         
   return status;
}

/*------------------------------------------------------------------------*/
/* function to put a FioBlk object to the RAM file system                 */
/*------------------------------------------------------------------------*/
/**
   This function writes a file to the FatFs volume.  On success, this
   function returns a non-negative value equal to the number of bytes
   written to the FatFs volume. On failure, a negative value is
   returned if the file was not written.
*/
int fioPut(const FioBlk *fioblk)
{
   /* define the logging signature */
   cc *FuncName="fioPut()";

   /* initialize the return value */
   int status=EOF; 

   /* validate the function argument */
   if (fioblk && fnameok(fioblk->fname)>0 && fioblk->len>0)
   {
      /* mount the FatFs volume */
      if (fat_mount())
      {
         /* define objects used for FatFs operations */
         FIL fp; FRESULT fstatus; unsigned int n;

         /* open an existing file in read mode */
         if ((fstatus=f_open(&fp,fioblk->fname,FA_WRITE|FA_CREATE_ALWAYS))==FR_OK)
         {
            /* write the next block from the FatFs file */
            if ((fstatus=f_write(&fp,(fioblk->buf),(fioblk->len),&n))==FR_OK && n>0)
            {
               /* initialize the return value with the number of bytes written */
               status=n;

               /* detect when the written file is truncated */
               if (n<fioblk->len) {LogEntry(FuncName,"FatFs file \"%s\" truncated "
                                            "from %u bytes down to %u bytes.\n",
                                            fioblk->fname,fioblk->len,n);}
            }

            /* log the read error */
            else {LogEntry(FuncName,"FatFs write failed for \"%s\" after %u bytes. "
                           "[FatErr=%d]\n",fioblk->fname,n,fstatus); errno=EIO;}

            /* close the FatFs file */
            if ((fstatus=f_close(&fp))!=FR_OK) 
            {
               /* log the close failure */
               LogEntry(FuncName,"FatFs close failed: %s [FatErr=%d]\n",fioblk->fname,fstatus);
            }
         }

         /* log the open failure */
         else {LogEntry(FuncName,"FatFs open failed: %s [FatErr=%d]\n",fioblk->fname,fstatus);}
      }
    
      /* indicate FatFs mount failure */
      else {errno=EIO;}
  
      /* unmount the FatFs volume */
      if (!fat_umount()) {errno=EIO;} 
   }
   
   /* indicate invalid function argument */
   else {errno=EINVAL;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to rename a file in the RAM file system or FatFs volume       */
/*------------------------------------------------------------------------*/
/**
  This function renames a file in the RAM file systems or FatFs
  volume.  For the RAM file system, no attempt is made to prevent
  duplicate file names; this must be done prior to calling this
  function.  On success, this function returns a nonnegative file
  descriptor which will be in the range [0,NFIO) if the file was in
  the RAM file system or else NFIO if the file was in the FatFs
  volume. Failure is indicated by a negative return value.  If an
  exception is encountered then errno is set accordingly.
*/
int fioRename(const char *oldfname,const char *newfname)
{
   /* define the logging signature */
   cc *FuncName="fioRename()";

   /* initialize the return value */
   int fdes,status=EOF; 

   /* guard against invalid file names */
   if (fnameok(oldfname)>0 && fnameok(newfname)>0)
   {
      /* loop through each of FioBlk in the RAM file system */      
      for (fdes=0; fdes<NFIO; fdes++)
      {
         /* search for the old file name in ram storage */
         if (!strcmp(oldfname,fioblk_[fdes].fname))
         {
            /* copy the new file name to the FioBlk object */
            strncpy(fioblk_[fdes].fname,newfname,FILENAME_MAX); status=fdes; break;
         }
      }

      /* if file not found in the RAM file system then look in the FatFs volume */
      if (status<0)
      {
         /* mount the FatFs volume */
         if (fat_mount())
         {
            /* define objects used for FatFs operations */
            FRESULT fstatus;

            /* rename the file in the FatFs volume */
            if ((fstatus=f_rename(oldfname,newfname))==FR_OK) {status=NFIO;}

            /* check if the file was not found in the FatFs volume */
            else if (fstatus==FR_NO_FILE) {errno=ENOENT;}
         
            /* log the rename failure */
            else {LogEntry(FuncName,"Failed to rename \"%s\" to \"%s\" in "
                           "the FatFs volume. [FatErr=%d]\n",oldfname,
                           newfname,fstatus); errno=EIO;}
         }
   
         /* indicate FatFs mount failure */
         else {errno=EIO;}
   
         /* unmount the FatFs volume */
         if (!fat_umount()) {errno=EIO;} 
      }
   }

   /* indicate invalid file name */
   else {errno=EINVAL;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to unlink a file in the RAM file system or FatFs volume       */
/*------------------------------------------------------------------------*/
/**
   This function unlinks a file from the RAM file system or FatFs
   volume.  On success, this function returns a nonnegative file
   descriptor which will be in the range [0,NFIO) if the file was in
   the RAM file system or else NFIO if the file was in the FatFs
   volume.  A negative value indicates failure to unlink the file.  If
   an exception is encountered then errno is set accordingly.
*/
int fioUnlink(const char *fname)
{
   /* define the logging signature */
   cc *FuncName="fioUnlink()";

   /* initialize the return value */
   int fdes,status=EOF; 

   /* guard against invalid file name */
   if (fnameok(fname)>0)
   {
      /* loop through each of FioBlk in the RAM file system */
      for (fdes=0; fdes<NFIO; fdes++)
      {
         /* search for the file name in ram storage and remove, if it exists */
         if (!strcmp(fname,fioblk_[fdes].fname)) {fioblk_[fdes].fname[0]=0; status=fdes; break;}
      }

      /* if file not found in the RAM file system then look in the FatFs volume */
      if (status<0) 
      {
         /* mount the FatFs volume */
         if (fat_mount())
         {
            /* define objects used for FatFs operations */
            FRESULT fstatus;

            /* delete the file from the FatFs volume */
            if ((fstatus=f_unlink(fname))==FR_OK) {status=NFIO;}

            /* check if the file was not found in the FatFs volume */
            else if (fstatus==FR_NO_FILE) {errno=ENOENT;}
         
            /* log the deletion failure */
            else {LogEntry(FuncName,"Failed to unlink \"%s\" from FatFs volume. "
                           "[FatErr=%d]\n",fname,fstatus); errno=EIO;}
         }
   
         /* indicate FatFs mount failure */
         else {errno=EIO;}
   
         /* unmount the FatFs volume */
         if (!fat_umount()) {errno=EIO;} 
      }
   }
   
   /* indicate invalid file name */
   else {errno=EINVAL;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to wipe a FioBLk object clean                                 */
/*------------------------------------------------------------------------*/
/**
   This function initializes the elements of a FioBlk object to zeros.  On
   success, this function returns a positive value.  On failure, this
   function returns zero.  A negative return value indicates an exceptional
   condition. 
*/
int fioWipe(FioBlk *fioblk)
{
   /* initialize the return value */
   int status=-1;

   /* initialize the FioBlk object to zeros */
   if (fioblk) {memset(fioblk,0,sizeof(FioBlk)); status=0;}

   return status;
}

#endif /* FATIO_C */
