#ifndef APF11RF_H
#define APF11RF_H (0x4000U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Description of the SwiftWare API for the RF interface serial ports.
   -------------------------------------------------------------------

   The SwiftWare UART model is non-blocking for both received and
   transmitted data but is interrupt-driven only for received data.
   Even at the slowest clock speed (8MHz), the Apf11 core remains
   considerably underutilized with respect to processor cycles.
   Moreover, the Max3109 includes a 128 byte FIFO which further
   relieves processor loads.
  
   For received data, an interrupt handler manages the transfer of
   each byte from the Max3109's Rx register to the UART's FIFO buffer.
   The interrupt handler also detects and maintains a record of
   over-run, noise, framing, and parity errors.  If too many errors
   are detected, the handler stops counting when the number of any
   given type of error exceeds 255 (ie., the maximum capacity of
   unsigned char).
  
   For transmitted data, the SwiftWare UART model is not
   interrupt-driven so that the precise timing of transmitted
   characters can be known by higher level software that uses this
   low-level API.  Instead of allowing an interrupt-handler to manage
   the transmission of each byte, this API implements a function to
   transmit a single character by loading the byte in the Tx register
   of the Max3109 and then monitoring to detect completion of the
   transmission of the byte.  After each byte is transmitted then the
   function returns so that the next byte can be transmitted.  A
   time-out feature prevents blocking.

   The Apf11's RF interface consists of two serial ports (gps, modem)
   on connectors J2 and {J3,J5,J9}, respectively.  A single Max3109
   DUART services both serial ports.  The two serial ports are
   completely independent from each other and could hypothetically be
   used either alone or in combination.  However, because there is
   only one antenna to service both the GPS and LBT then this
   implementation enforces the constraint that only one of the serial
   ports may be active at any given time.  The RF switch automatically
   connects the antenna to whichever serial port is active. The
   implemented baud rates are 1200, 2400, 4800, 9600, 19200, 38400 and
   the implemented modes are N81, E71, O71.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define apf11rfChangeLog "$RCSfile$  $Revision$  $Date$"

#include <max7301.h>
#include <serial.h>

/* enumerate the RF GPIO modes */
typedef enum {RfOutPPL = 0x01U, RfInFlt = 0x02U,
              RfOutPPH = 0x01U, RfInPU  = 0x03U} RfGpioPortConfig;

/* enumerate the states of the port */
typedef SpPortState RfPortState;

/* declare functions with external linkage */
int    GpsDisable(void);
int    GpsEnable(long int BaudRate);
int    GpsErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                 unsigned char *ovrun, unsigned char *noise);
size_t GpsFifoLen(void);
size_t GpsFifoSize(void);
int    GpsIsEnable(void);
int    GpsTransceiverForceOff(RfPortState state);
int    GpsTransceiverForceOn(RfPortState state);
int    GpsTransceiverIsEnable(void);
int    GpsPower(RfPortState state);
int    GpsRelay(RfPortState state);
int    Iridium3v(RfPortState state);
int    Iridium5v(RfPortState state);
int    ModemCd(void);
int    ModemCts(void);
int    ModemDisable(void);
int    ModemDsr(void);
int    ModemDtrAssert(void);
int    ModemDtrClear(void);
int    ModemEnable(long int BaudRate);
int    ModemErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                   unsigned char *ovrun, unsigned char *noise);
size_t ModemFifoLen(void);
size_t ModemFifoSize(void);
int    ModemIsEnable(void);
int    ModemRing(void);
int    ModemRtsAssert(void);
int    ModemRtsClear(void);
int    ModemTransceiverForceOff(RfPortState state);
int    ModemTransceiverForceOn(RfPortState state);
int    ModemTransceiverIsEnable(void);
int    ModemTxBreak(unsigned short int millisec);
int    RfEnable(void);
int    RfDisable(void);
int    RfDuartReset(unsigned int millisec);
int    RfGpioConfig(unsigned short int port, RfGpioPortConfig config);
int    RfGpioInit(void);
int    RfGpioSleep(void);
int    RfGpioStateGet(unsigned short int port, RfPortState *state);
int    RfGpioStateSet(unsigned short int port, RfPortState state);
int    RfGpioWake(void);
int    RfIsEnable(void);
int    RfLed(RfPortState state);
int    RfSwitchedBatteryPower(RfPortState state);
int    Rf5v(RfPortState state);
int    Sbd(RfPortState state);

/* extern references to expansion serial ports */
extern const struct SerialPort gps,modem;

/* define the return states of the Stm32 RF sensor API */
extern const char RfInvalid;         /* invalid selection */
extern const char RfPowerFail;       /* power failure */
extern const char RfConfig;          /* MAX7301 configuration fault */
extern const char RfSpiConfig;       /* SPI configuration fault */
extern const char RfNull;            /* NULL function argument */
extern const char RfFail;            /* general failure */
extern const char RfOk;              /* general success */

#endif /* APF11RF_H */
#ifdef APF11RF_C

#include <apf11.h>
#include <fifo.h>
#include <limits.h>
#include <logger.h>
#include <max3109.h>
#include <Stm32f103Exti.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Spi.h>
#include <string.h>

/* define the return states of the RF GPIO API */
const char RfInvalid           = -5; /* invalid selection */
const char RfPowerFail         = -4; /* power failure */
const char RfConfig            = -3; /* MAX7301 configuration fault */
const char RfSpiConfig         = -2; /* SPI configuration fault */
const char RfNull              = -1; /* NULL function argument */
const char RfFail              =  0; /* general failure */
const char RfOk                =  1; /* general success */

/* prototypes for functions with static linkage */
static long int Gps(long int BaudRate);
static int      GpsGetb(unsigned char *byte);
static int      GpsIBytes(void);
static int      GpsIflush(void);
static int      GpsPutb(unsigned char c);
static long int Modem(long int BaudRate);
static int      ModemDtr(int state);
static int      ModemGetb(unsigned char *byte);
static int      ModemIBytes(void);
static int      ModemIflush(void);
static int      ModemPutb(unsigned char c);
static int      ModemRts(int state);
static int      na(void);
static int      na_(int state);

/* define a fifo buffer for the Modem serial port */
persistent static unsigned char ModemFifoBuf[2048];

/* define a Fifo object for the Modem serial port */
static struct Fifo ModemFifo ={ModemFifoBuf, sizeof(ModemFifoBuf), 0, 0, 0, 0};
 
/* define a serial port for the Modem interface */
const struct SerialPort modem = {ModemGetb, ModemPutb, ModemIflush, ModemIflush, na, ModemIBytes, na,
                                 ModemCd, ModemRts, ModemCts, ModemDtr, ModemDsr, Modem};

/* define a fifo buffer for the Gps serial port */
persistent static unsigned char GpsFifoBuf[4096];
 
/* define a Fifo object for the Gps serial port */
static struct Fifo GpsFifo ={GpsFifoBuf, sizeof(GpsFifoBuf), 0, 0, 0, 0};

/* define a serial port for the Gps interface */
const struct SerialPort gps = {GpsGetb, GpsPutb, GpsIflush, GpsIflush, na,GpsIBytes,
                               na, na, na_, na, na_, na, Gps};

/* define the pins of the RF GPIO peripheral */
#define EN_5V                        (0x0400U) 
#define LED_ENABLE                   (0x0500U)
#define VBATT_GPIO_ENABLE            (0x0600U)
#define GPP7                         (0x0700U)
#define GPP8                         (0x0800U)
#define GPP9                         (0x0900U)
#define GPP10                        (0x0a00U)
#define GPP11                        (0x0b00U)
#define IRID_SIG_FORCE_OFF_N         (0x0c00U)
#define IRID_SIG_FORCE_ON            (0x0d00U)
#define NC14                         (0x0e00U)
#define RF_3P3V_HIGH_SIDE_IRIDIUM    (0x0f00U)
#define RF_3P3V_HIGH_SIDE_IRID_PHONE (0x1000U)
#define IRID_SBD_ON                  (0x1100U)
#define NC18                         (0x1200U)
#define RF_3P3V_HIGH_SIDE_GPS        (0x1300U)
#define GPS_RELAY_ENA                (0x1400U)
#define DUART_RST_N                  (0x1500U)
#define GPS_SIG_FORCEOFF_N           (0x1600U)
#define GPS_SIG_FORCEON              (0x1700U)
#define GPP24                        (0x1800U)
#define GPP25                        (0x1900U)
#define GPP26                        (0x1a00U)
#define GPP27                        (0x1b00U)
#define GPP28                        (0x1c00U)
#define GPP29                        (0x1d00U)
#define GPP30                        (0x1e00U)
#define NC31                         (0x1f00U) 

/* define configuration parameters */
#define SpiBaudRate                  (0x0000U)
#define WordMode                     (0x0800U)
#define Cr2                          (0x0000U)
#define NoCrc                        (0x0000U)

/* define MAX7301 command masks */
#define NOOP                         (0x0000U)
#define WRITE                        (0x0000U)
#define READ                         (0x8000U)
#define PIN                          (0x2000U)
#define HDRIVE                       (0x5300U)
#define WAKE                         (0x0001U)

/* define MAX7301 registers */
#define CONFIG                       (0x0400U)
#define CONFIG_P04_P07               (0x0900U)
#define CONFIG_P08_P11               (0x0a00U)
#define CONFIG_P12_P15               (0x0b00U)
#define CONFIG_P16_P19               (0x0c00U)
#define CONFIG_P20_P23               (0x0d00U)
#define CONFIG_P24_P27               (0x0e00U)
#define CONFIG_P28_P31               (0x0f00U)

/* declarations for functions with nonexposed external linkage */
int Com1Disable(void);
int Com2Disable(void);
unsigned long int HAL_GetTick(void);
int RfDuartIsr(void);
int RfGpioCsPulse(unsigned char msec);
int Wait(unsigned long int millisec);

/* define objects to contain counts of Max3109 line status errors */
UartStatus ModemStatus = {0,0,0,0,0}, GpsStatus = {0,0,0,0,0};

/*========================================================================*/
/* Notes about initialization/configuration of the MAX7301                */
/*========================================================================*/
/**
   The Max7301Config array of unsigned char is used to configure the
   MAX7301 GPIO expander.  Tables 1 & 2 (below) were taken from the
   data sheet of the MAX7301.

   Table 1. Port Configuration Map
   +--------------------------+------+-----------------------------------+
   |        Register          | Hex  |         Register Data             |
   |                          | Code | D{7,6} | D{5,4} | D{3,2} | D{1,0} |
   +--------------------------+------+--------+--------+--------+--------+
   | Port config for P07->P04 | 0x09 |   P07  |   P06  |   P05  |   P04  |
   | Port config for P11->P08 | 0x0a |   P11  |   P10  |   P09  |   P08  |
   | Port config for P15->P12 | 0x0b |   P15  |   P14  |   P13  |   P12  |
   | Port config for P19->P16 | 0x0c |   P19  |   P18  |   P17  |   P16  |
   | Port config for P23->P20 | 0x0d |   P23  |   P22  |   P21  |   P20  |
   | Port config for P27->P24 | 0x0e |   P27  |   P26  |   P25  |   P24  |
   | Port config for P31->P28 | 0x0f |   P31  |   P30  |   P29  |   P28  |
   +--------------------------+------+-----------------------------------+

   Table 2. Port Configuration Matrix
   +--------+-------------+-----+--------------------+-----------+--------+
   | Mode   | Function    | Reg | Pin Behavior       | Address   | Config |
   +--------+-------------+-----+--------------------+-----------+----+---+
   |             DO NOT USE THIS SETTING             | 0x09-0x0f |  0 | 0 |
   +--------+-------------+-----+--------------------+-----------+----+---+
   | Output | GPIO Output | 0x0 | Active-low output  | 0x09-0x0f |  0 | 1 |
   |        |             | 0x1 | Active-high output | 0x09-0x0f |  0 | 1 |
   +--------+-------------+-----+--------------------+-----------+----+---+
   | Input  | no pull-up  | --- | Schmitt trigger    | 0x09-0x0f |  0 | 1 |
   | Input  | pull-up     | --- | Schmitt trigger    | 0x09-0x0f |  1 | 1 |
   +--------+-------------+-----+--------------------+-----------+----+---+

   The IO configuration of each of the twenty-eight ports is defined below.
*/
   #define P04Config (RfOutPPL) /* 5V regulator on RF PCB                    */
   #define P05Config (RfOutPPL) /* LED illumination on RF PCB                */
   #define P06Config (RfOutPPL) /* Switched battery power to RF:J1           */
   #define P07Config (RfInPU)   /* GPIO to RF:J1.9                           */
   #define P08Config (RfInPU)   /* GPIO to RF:J1.11                          */
   #define P09Config (RfInPU)   /* GPIO to RF:J1.13                          */
   #define P10Config (RfInPU)   /* GPIO to RF:J1.15                          */
   #define P11Config (RfInPU)   /* GPIO to RF:J1.17                          */
   #define P12Config (RfOutPPL) /* Iridium Max3245 transceiver \FORCEOFF     */
   #define P13Config (RfOutPPL) /* Iridium Max3245 transceiver FORCEON       */
   #define P14Config (RfInPU)   /* No connection                             */
   #define P15Config (RfOutPPL) /* Switched 5V power to RF:J{3,5,9}          */
   #define P16Config (RfOutPPL) /* Switched 3V supply to Max3245 transceiver */
   #define P17Config (RfOutPPL) /* SBD LBT on/off control                    */
   #define P18Config (RfInPU)   /* No connection                             */
   #define P19Config (RfOutPPL) /* Switched 3V GPS power to RF:J2            */
   #define P20Config (RfOutPPL) /* GPS relay enable/disable                  */
   #define P21Config (RfOutPPH) /* RF Max3109 DUART reset                    */
   #define P22Config (RfOutPPL) /* GPS Max3225 transceiver \FORCEOFF         */
   #define P23Config (RfOutPPL) /* GPS Max3225 transceiver \FORCEON          */
   #define P24Config (RfInPU)   /* GPIO to RF:J1.19                          */
   #define P25Config (RfInPU)   /* GPIO to RF:J1.10                          */
   #define P26Config (RfInPU)   /* GPIO to RF:J1.12                          */
   #define P27Config (RfInPU)   /* GPIO to RF:J1.14                          */
   #define P28Config (RfInPU)   /* GPIO to RF:J1.16                          */
   #define P29Config (RfInPU)   /* GPIO to RF:J1.18                          */
   #define P30Config (RfInPU)   /* GPIO to RF:J1.20                          */
   #define P31Config (RfInPU)   /* No Connection                             */
/*                                                                       
   The twenty-eight configurations above are integrated into the array
   of seven unsigned char's below.
*/
static const unsigned char Max7301Config[] =
{
   (P07Config<<6) | (P06Config<<4) | (P05Config<<2) | P04Config,
   (P11Config<<6) | (P10Config<<4) | (P09Config<<2) | P08Config,
   (P15Config<<6) | (P14Config<<4) | (P13Config<<2) | P12Config,
   (P19Config<<6) | (P18Config<<4) | (P17Config<<2) | P16Config,
   (P23Config<<6) | (P22Config<<4) | (P21Config<<2) | P20Config,
   (P27Config<<6) | (P26Config<<4) | (P25Config<<2) | P24Config,
   (P31Config<<6) | (P30Config<<4) | (P29Config<<2) | P28Config   
};

/*------------------------------------------------------------------------*/
/* function to enable/disable Gps                                         */
/*------------------------------------------------------------------------*/
/**
   This function is used to enable or disable Gps.  To enable the com port
   then set the argument equal to the desired baud rate.  To disable the com
   port then set the argument less than or equal to zero. 

   \begin{verbatim}
   input:

      BaudRate...This parameter controls functionality.

                 - The value LONG_MAX is a sentinel value that
                   requests the SerialPort identifer to be returned.
                 - Zero requests that the com port be disabled.
                 - A positive value requests that the com port be
                   enabled with the specified baud rate.
                 - A negative value determines the state of the
                   serial port.

   output:
      If the argument of this function is non-negative then a
      positive value is returned if successful or else zero upon
      failure.  If the argument is negative then the baud rate is
      returned if the serial port is enabled or else zero is
      returned if the serial port is disabled. In all cases, a
      negative return value indicates an exception.
   \end{verbatim}
*/
static long int Gps(long int BaudRate)
{
   /* initialize the return value */
   int status=1;

   /* check for sentinel value to query com id */
   if (BaudRate==LONG_MAX) {status=3;}

   /* configure the serial port if BaudRate is positive */
   else if (BaudRate>0) {status=GpsEnable(BaudRate);}

   /* disable the serial port if BaudRate is zero */
   else if (!BaudRate) {status=GpsDisable();}

   /* determine serial port state */
   else {status=GpsIsEnable();}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable the GPS interface                                  */
/*------------------------------------------------------------------------*/
/**
   This function disables UART0 of the RF DUART (Max3109).  If both
   the RF interface and the GPS interface are enabled then the RF
   interface is disabled; otherwise, the state of the RF interface is
   not altered. If successful, then this function returns a positive
   value or else zero is returned, on failure. A negative return value
   indicates that an exception was encountered.
*/
int GpsDisable(void)
{
   /* initialize return value */
   int status=1;

   /* if GPS transceiver is not enabled then disable the Max3109 */
   if (RfIsEnable()>0 && GpsTransceiverIsEnable()>0)
   {
      /* disable the IRQ for the RF duart */
      Stm32f103ExtiIrqDisable(RfDuartIrq);
      
      /* disable the RF interface */
      RfDisable();
   }

   /* flush the GPS fifo */
   Wait(50); flush(&GpsFifo);

   /* reinitialize the error counters */
   memset((void *)(&GpsStatus),0,sizeof(GpsStatus));

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the GPS interface                                   */
/*------------------------------------------------------------------------*/
/**
   The function enables UART0 of the RF DUART (Max3109).  The baud
   rate is capped at the upper end by 38400 due to the fact that, at
   higher speeds, the Max3109 interrupts can not be serviced fast
   enough to prevent overflow of the Max3109's Rx FIFOs.

   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int GpsEnable(long int BaudRate)
{
   /* define the logging signature */
   cc *FuncName="GpsEnable()";
  
   /* initialize the return value */
   int err[2],dev,status=RfOk;

   /* avoid conflicts on the SPI bus */
   switch ((dev=Spi1Select(SpiDevQuery)))
   {
      /* ignore meta-devices */
      case SpiDevQuery: case Spi1Dev: {break;}

      /* steal the SPI bus away from COM{1,2} */
      case Spi1DevSpDuart: {Com1Disable(); Com2Disable(); Spi1Select(Spi1Dev); break;}

      /* ignore the RF DUART on SPI bus 1 */
      case Spi1DevRfDuart: {break;}

      /* manage unimplemented SPI devices */
      default: {LogEntry(FuncName,"Device(%d) not implemented on SPI bus 1.\n",dev);}
   }
      
   /* make sure the RF interface starts off in a disabled state */
   if (RfIsEnable()>0) {GpsDisable(); ModemDisable(); Wait(1000);}

   /* power-up and initialize the RF interface */
   if ((err[0]=RfGpioInit())<=0 || (err[1]=RfDuartReset(5))<=0)
   {
      LogEntry(FuncName,"RF interface activation failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfPowerFail;
   }

   /* power-up the GPS interface and switch the antenna relay to the GPS engine */
   else if ((err[0]=GpsPower(ASSERT))<=0 || (err[1]=GpsRelay(ASSERT))<=0)
   {
      LogEntry(FuncName,"GPS interface activation failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfConfig;
   }
   
   /* configure the hardware control signals to the GPS transceiver */
   else if ((err[0]=GpsTransceiverForceOff(ASSERT))<=0 || (err[1]=GpsTransceiverForceOn(ASSERT))<=0)
   {
      LogEntry(FuncName,"GPS transceiver configuration failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfConfig;
   }

   /* switch statement to enforce valid configurations */
   else switch (BaudRate)
   {
      /* enumerate the implemented baud rates */
      case 1200: case 2400:  case 4800:
      case 9600: case 19200: case 38400:
      {
         /* configure the communications paramters */
         if ((err[0]=Max3109Config(Spi1DevRfDuart,UART0,BaudRate,N81))>0 &&
             (err[1]=Max3109RegWrite(Spi1DevRfDuart,UART0,GPIOConfig,0x00))>0)
         {
            /* enable the IRQ for the spare duart */
            Stm32f103ExtiIrqEnable(RfDuartIrq,Falling);

            /* flush the Tx/Rx buffers of UART0 */
            Wait(50); Max3109Flush(Spi1DevRfDuart,UART0);
            
            /* reinitialize the error counters */
            memset((void *)(&GpsStatus),0,sizeof(GpsStatus));
            
            /* flush the FIFO */
            Wait(1000); flush(&GpsFifo); status=RfOk;
         }

         /* log the configuration error */
         else {LogEntry(FuncName,"Attempt to configure GPS serial interface "
                        "failed. [err={%d,%d}]\n",err[0],err[1]);}
         
         break;
      }

      /* catch invalid or nonimplemented baud rate configuration */
      default: {LogEntry(FuncName,"Nonimplemented baud rate: %u\n",BaudRate);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue            */
/*------------------------------------------------------------------------*/
static int GpsIBytes(void)
{
   return GpsFifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the GPS's Rx FIFO queue                              */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the GPS serial port.  This
   function returns a positive value on success and zero on failure.  A
   negative return value indicates an invalid or corrupt Fifo object.
*/
static int GpsIflush(void)
{
   int status=0;

   /* flush any bytes in the Max3109:UART0's FIFOs of the RF interface */
   Max3109Flush(Spi1DevRfDuart,UART0);

   /* flush the GPS serial port's fifo buffer */
   status=flush(&GpsFifo);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* test if GPS transceiver is enabled                                     */
/*------------------------------------------------------------------------*/
/**
   This function determines if the transceiver for UART0 of the RF
   interface is enabled.  If successful, then this function returns a
   positive value; zero is returned, on failure.
*/
int GpsIsEnable(void)
{
   /* initialize the return value */
   int status=0; unsigned long int baud;

   /* test if the UART0 transceiver is enabled */
   if (RfIsEnable()>0 && GpsTransceiverIsEnable()>0)
   {
      /* retrieve the baud rate from UART0 */
      status = (Max3109BaudRateGet(Spi1DevRfDuart,UART0,&baud)>0) ? baud : 0;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the GPS interrupt           */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Gps
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int GpsErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
              unsigned char *ovrun, unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      GpsStatus.nbreak=0; GpsStatus.parity=0; GpsStatus.frame=0;
      GpsStatus.ovrun=0;  GpsStatus.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=GpsStatus.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=GpsStatus.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=GpsStatus.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=GpsStatus.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=GpsStatus.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* function to return number of bytes currently stored in the GPS fifo    */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes currently stored in the GPS
   fifo.  The returned value should be regarded as merely a lower-bound
   estimate since interrupt-driven input from the GPS port can (without
   notice) increase the number of stored bytes.
*/
size_t GpsFifoLen(void)
{
   return GpsFifo.length;
}

/*------------------------------------------------------------------------*/
/* function to return the maximum number of bytes the GPS fifo can store  */
/*------------------------------------------------------------------------*/
/**
   This function returns the maximum number of bytes that the GPS fifo can
   store without overflowing.  
*/
size_t GpsFifoSize(void)
{
   return GpsFifo.size;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the GPS FIFO queue                        */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the GPS serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the GPS FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int GpsGetb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "GpsGetb()";

   /* initialize the return value */
   int status=0;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   /* pop the next byte out of the fifo queue */
   else {status=pop(&GpsFifo,byte);}

   return status;
}

/*------------------------------------------------------------------------*/
/* enable/disable the 3V/5V power supplies to GPS section of RF interface */
/*------------------------------------------------------------------------*/
/**
   This function enables/disables the 3V and 5V power supplies to the GPS
   functionality of the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the 3V & 5V power supplies to
              the GPS functionality of the RF interface.  A zero value
              disables the this same functionality.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int GpsPower(RfPortState state)
{
   return RfGpioStateSet(RF_3P3V_HIGH_SIDE_GPS, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to write one byte to RF interface's Max3109:UART0 serial port */
/*------------------------------------------------------------------------*/
/**
   This function writes one byte to the RF interface's Max3109:UART0 and
   pauses until that byte is transmitted by the serial port.  

   \begin{verbatim}
   input:
      byte...This is the byte to add to the Tx fifo.

   output:
      This function returns a positive value on success or zero on
      failure. 
   \end{verbatim}
*/
static int GpsPutb(unsigned char byte)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a timeout limit */
   const unsigned long int timeout=20;

   /* initialize the reference time for the timeout loop */
   const unsigned long int To=HAL_GetTick();

   /* write the byte to the RF interface's Max3109:UART0 */
   if ((err=Max3109Putb(Spi1DevRfDuart,UART0,byte))<=0) {status=Max3109Fail;}

   /* loop to wait until the byte has been transmitted */
   else while ((HAL_GetTick()-To)<timeout)
   {
      /* wait until the Tx FIFO is empty */
      if (!Max3109OBytes(Spi1DevRfDuart,UART0)) {break;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* enable/disable the 3V/5V power supplies to GPS section of RF interface */
/*------------------------------------------------------------------------*/
/**
   This function enables/disables the 3V and 5V power supplies to the GPS
   functionality of the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the 3V & 5V power supplies to
              the GPS functionality of the RF interface.  A zero value
              disables the this same functionality.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int GpsRelay(RfPortState state)
{
   return RfGpioStateSet(GPS_RELAY_ENA, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to control the Max3225 transceiver's ForceOff signal          */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the Max3225 transceiver's
   ForceOff signal.  The transceiver converts between TTL logic levels
   and RS232 logic levels.  If the ForceOff signal is cleared then
   the Max3225 is induced into shutdown mode which will inhibit
   communications with the Iridium LBT; the ForceOff signal needs to
   be asserted in order for communications to happen between the LBT
   and the Apf11.

   \begin{verbatim}
   input:

      state...A nonzero value asserts the ForceOff signal which
              induces the Max3225 into shutdown mode.  A zero value
              clears the ForceOff signal which allows communication
              between the Apf11 and the LBT.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int GpsTransceiverForceOff(RfPortState state)
{
   return RfGpioStateSet(GPS_SIG_FORCEOFF_N, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to control the Max3225 transceiver's ForceOn signal           */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the Max3225 transceiver's
   ForceOn signal.  The transceiver converts between TTL logic levels
   and RS232 logic levels.  If the ForceOn signal is cleared then
   the Max3225 is induced into automatic shutdown/wakeup mode with the
   objective of reducing energy consumption. If the ForceOn signal is
   asserted then the transceiver will remain awake regardless of
   serial port inactivity.

   \begin{verbatim}
   input:

      state...A zero value clears the ForceOn signal which induces the
              Max3225 into auto shutdown/wakeup mode.  A zero value
              clears the ForceOn signal which forces the transceiver
              to remain awake.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int GpsTransceiverForceOn(RfPortState state)
{
   return RfGpioStateSet(GPS_SIG_FORCEON, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to determine if the GPS transceiver is enabled                */
/*------------------------------------------------------------------------*/
/**
   The function to determine if the GPS transceiver is enabled. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int GpsTransceiverIsEnable(void)
{
   /* initialize return value */
   int status=RfOk; RfPortState state;

   /* read the GPIO register from the Max3109 */
   if (RfGpioStateGet(RF_3P3V_HIGH_SIDE_GPS, &state)<0) {status=RfConfig;}

   /* criteria to determine if transceiver is enabled */
   else {status = ((state>0) ? RfOk : RfFail);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* enable/disable the 3V power supply to Iridium section of RF interface  */
/*------------------------------------------------------------------------*/
/**
   This function enables/disables the 3V power supply to the Iridium
   functionality of the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the 3V power supply to the
              Iridium functionality of the RF interface.  A zero value
              disables the this same functionality.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Iridium3v(RfPortState state)
{
   return RfGpioStateSet(RF_3P3V_HIGH_SIDE_IRID_PHONE, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* enable/disable the 5V power supply to Iridium section of RF interface  */
/*------------------------------------------------------------------------*/
/**
   This function enables/disables the 5V power supply to the Iridium
   functionality of the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the 5V power supply to the
              Iridium functionality of the RF interface.  A zero value
              disables the this same functionality.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Iridium5v(RfPortState state)
{
   return RfGpioStateSet(RF_3P3V_HIGH_SIDE_IRIDIUM, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to enable/disable the modem interface                         */
/*------------------------------------------------------------------------*/
/**
   This function is used to enable or disable the modem interface.  To
   enable the modem serial port then set the argument equal to the
   desired baud rate.  To disable the modem serial port then set the
   argument less than or equal to zero.

   \begin{verbatim}
   input:

      BaudRate...This parameter controls functionality.

                 - The value LONG_MAX is a sentinel value that
                   requests the SerialPort identifer to be returned.
                 - Zero requests that the modem serial port be
                   disabled.
                 - A positive value requests that the modem serial
                   port be enabled with the specified baud rate.
                 - A negative value determines the state of the
                   serial port.

   output:
      If the argument of this function is non-negative then a
      positive value is returned if successful or else zero upon
      failure.  If the argument is negative then the baud rate is
      returned if the serial port is enabled or else zero is
      returned if the serial port is disabled. In all cases, a
      negative return value indicates an exception.
   \end{verbatim}
*/
static long int Modem(long int BaudRate)
{
   /* initialize the return value */
   int status=1;

   /* check for sentinel value to query com id */
   if (BaudRate==LONG_MAX) {status=4;}

   /* configure the serial port if BaudRate is positive */
   else if (BaudRate>0) {status=ModemEnable(BaudRate);}

   /* disable the serial port if BaudRate is zero */
   else if (!BaudRate) {status=ModemDisable();}

   /* determine serial port state */
   else {status=ModemIsEnable();}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* funtion to return the state of the CD line of the modem serial port    */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the carrier-detect (CD) signal
   of UART1.  If CD is asserted, pin 11 of J3 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J3:11 is
   asserted if the max3109 CD is clear.  This function returns one if
   CD is asserted or zero otherwise.
*/
int ModemCd(void)
{
   /* initialize the return */
   int status=0;

   /* define local work object */
   unsigned char value;

   /* read the state of the four GPIO pins associated with UART1 */
   if (Max3109RegRead(Spi1DevRfDuart,UART1,GPIOData,&value)>0)
   {
      /* extract the state of pin GPIO6 (with transceiver inversion) */
      status=(value&0x40) ? 0 : 1;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the state of the CTS line of the modem serial port  */
/*------------------------------------------------------------------------*/
/**
   This function retuns the state of the CTS line of the modem serial
   port.  If CTS is asserted, pin 10 of J3 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J3:10 is
   asserted if the Max3109 CTS signal is clear.  This function returns
   1 if CTS is asserted or zero otherwise.
*/
int ModemCts(void)
{
   /* read the CTS pin of the spare-sensor Max3109:UART1 */
   return ((Max3109CtsIsClear(Spi1DevRfDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* funtion to return the state of the DSR line of the modem serial port   */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the data-set-ready (DSR) signal
   of UART1.  If DSR is asserted, pin 11 of J3 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J3:11 is
   asserted if the max3109 DSR is clear.  This function returns one if
   DSR is asserted or zero otherwise.
*/
int ModemDsr(void)
{
   /* initialize the return */
   int status=0;

   /* define local work object */
   unsigned char value;

   /* read the state of the four GPIO pins associated with UART1 */
   if (Max3109RegRead(Spi1DevRfDuart,UART1,GPIOData,&value)>0)
   {
      /* extract the state of pin GPIO4 (with transceiver inversion) */
      status=(value&0x20) ? 0 : 1;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert/clear the RTS signal of the modem serial port       */
/*------------------------------------------------------------------------*/
/**
   This function is used to assert or clear the RTS signal for the
   modem serial port.  To assert the RTS line then set the argument to
   be positive.  To clear the RTS line then set the argument to be
   zero or negative.  This function returns a positive value on
   success; otherwise zero is returned.
*/
static int ModemDtr(int state)
{
   int status = (state>0) ? ModemDtrAssert() : ModemDtrClear();

   return ((status>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert the DTR line of the modem serial port               */
/*------------------------------------------------------------------------*/
/**
   This function asserts the DTR line of the modem serial port; pin 5
   of J3 will measure +5V.  The transceiver inverts the sense of the
   Max3109 signal and so asserting J3:5 requires clearing the Max3109 
   signal. If successful, a positive value is returned; zero is
   returned on failure.
*/
int ModemDtrAssert(void)
{
   return ((Max3109RegModify(Spi1DevRfDuart,UART1,GPIOData,0x00,0x01)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the DTR line of the modem serial port                */
/*------------------------------------------------------------------------*/
/**
   This function clears the DTR line of the modem serial port; pin 5
   of J3 will measure -5V.  The transceiver inverts the sense of the
   Max3109 signal and so clearing J3:5 requires asserting the Max3109 
   signal. If successful, a positive value is returned; zero is
   returned on failure.
*/
int ModemDtrClear(void)
{
   return ((Max3109RegModify(Spi1DevRfDuart,UART1,GPIOData,0x01,0x00)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to disable the modem serial interface                         */
/*------------------------------------------------------------------------*/
/**
   This function disables UART1 of the RF DUART (Max3109).  If both
   the RF interface and the modem interface are enabled then the RF
   interface is disabled; otherwise, the state of the RF interface is
   not altered. If successful, then this function returns a positive
   value or else zero is returned, on failure. A negative return value
   indicates that an exception was encountered.
*/
int ModemDisable(void)
{
   /* initialize return value */
   int status=1;

   /* if modem transceiver is not enabled then disable the Max3109 */
   if (RfIsEnable()>0 && ModemTransceiverIsEnable()>0)
   {
      /* disable the IRQ for the RF duart */
      Stm32f103ExtiIrqDisable(RfDuartIrq);

      /* disable the transceiver control signals */
      ModemTransceiverForceOn(CLEAR); ModemTransceiverForceOff(CLEAR);

      /* disable the RF interface */
      RfDisable();
   }

   /* flush the modem fifo */
   Wait(50); flush(&ModemFifo);

   /* reinitialize the error counters */
   memset((void *)(&ModemStatus),0,sizeof(ModemStatus));

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the MODEM interface                                 */
/*------------------------------------------------------------------------*/
/**
   The function enables UART1 of the RF DUART (Max3109).  The baud
   rate is capped at the upper end by 38400 due to the fact that, at
   higher speeds, the Max3109 interrupts can not be serviced fast
   enough to prevent overflow of the Max3109's Rx FIFOs.

   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int ModemEnable(long int BaudRate)
{
   /* define the logging signature */
   cc *FuncName="ModemEnable()";
  
   /* initialize the return value */
   int err[4],dev,status=RfOk;

   /* avoid conflicts on the SPI bus */
   switch ((dev=Spi1Select(SpiDevQuery)))
   {
      /* ignore meta-devices */
      case SpiDevQuery: case Spi1Dev: {break;}

      /* steal the SPI bus away from COM{1,2} */
      case Spi1DevSpDuart: {Com1Disable(); Com2Disable(); Spi1Select(Spi1Dev); break;}

      /* ignore the RF DUART on SPI bus 1 */
      case Spi1DevRfDuart: {break;}

      /* manage unimplemented SPI devices */
      default: {LogEntry(FuncName,"Device(%d) not implemented on SPI bus 1.\n",dev);}
   }

   /* make sure the RF interface starts off in a disabled state */
   if (RfIsEnable()>0) {GpsDisable(); ModemDisable(); Wait(1000);}
   
   /* power-up and initialize the RF interface */
   if ((err[0]=RfGpioInit())<=0 || (err[1]=RfDuartReset(5))<=0)
   {
      LogEntry(FuncName,"RF interface activation failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfPowerFail;
   }
   
   /* temporarily disable the modem transceiver */
   else if ((err[0]=ModemTransceiverForceOff(CLEAR))<=0 || (err[1]=ModemTransceiverForceOn(CLEAR))<=0)
   {
      LogEntry(FuncName,"Modem transceiver configuration failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfConfig;
   }

   /* power-up the 3V/5V sections of the modem interface */
   else if ((err[0]=Iridium3v(ASSERT))<=0 || (err[1]=Iridium5v(ASSERT))<=0)
   {
      LogEntry(FuncName,"Modem interface activation failed. "
               "[err={%d,%d}]\n",err[0],err[1]); status=RfConfig;
   }

   /* switch statement to enforce valid configurations */
   else switch (BaudRate)
   {
      /* enumerate the implemented baud rates */
      case 1200: case 2400:  case 4800:
      case 9600: case 19200: case 38400:
      {
         /* configure the communications paramters */
         if ((err[0]=Max3109Config(Spi1DevRfDuart,UART1,BaudRate,N81))>0 &&
             (err[1]=Max3109RegWrite(Spi1DevRfDuart,UART1,GPIOConfig,0x01))>0 &&
             (err[2]=(ModemDtrAssert()>0 && ModemRtsAssert()>0)) &&
             (err[3]=(ModemTransceiverForceOff(ASSERT)>0 && ModemTransceiverForceOn(ASSERT)>0)))
         {
            /* enable the IRQ for the RF duart */
            Stm32f103ExtiIrqEnable(RfDuartIrq,Falling);

            /* flush the Tx/Rx buffers of UART1 */
            Wait(50); Max3109Flush(Spi1DevRfDuart,UART1);
            
            /* reinitialize the error counters */
            memset((void *)(&ModemStatus),0,sizeof(ModemStatus));
            
            /* flush the FIFO */
            Wait(1000); flush(&ModemFifo); status=RfOk;
         }

         /* log the configuration error */
         else {LogEntry(FuncName,"Attempt to configure modem serial interface "
                        "failed. [err={%d,%d,%d,%d}]\n",err[0],err[1],err[2],err[3]);}
         
         break;
      }

      /* catch invalid or nonimplemented baud rate configuration */
      default: {LogEntry(FuncName,"Nonimplemented baud rate: %u\n",BaudRate);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the modem interrupt         */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the modem
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int ModemErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                unsigned char *ovrun, unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      ModemStatus.nbreak=0; ModemStatus.parity=0; ModemStatus.frame=0;
      ModemStatus.ovrun=0;  ModemStatus.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=ModemStatus.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=ModemStatus.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=ModemStatus.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=ModemStatus.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=ModemStatus.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the MODEM FIFO queue                      */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the modem serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the modem FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int ModemGetb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "ModemGetb()";

   /* initialize the return value */
   int status=0;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   /* pop the next byte out of the fifo queue */
   else {status=pop(&ModemFifo,byte);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return number of bytes currently stored in the modem fifo  */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes currently stored in the modem
   fifo.  The returned value should be regarded as merely a lower-bound
   estimate since interrupt-driven input from the modem port can (without
   notice) increase the number of stored bytes.
*/
size_t ModemFifoLen(void)
{
   return ModemFifo.length;
}

/*------------------------------------------------------------------------*/
/* function to return the maximum number of bytes the Modem fifo can store  */
/*------------------------------------------------------------------------*/
/**
   This function returns the maximum number of bytes that the Modem fifo can
   store without overflowing.  
*/
size_t ModemFifoSize(void)
{
   return ModemFifo.size;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue            */
/*------------------------------------------------------------------------*/
static int ModemIBytes(void)
{
   return ModemFifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the MODEM's Rx FIFO queue                            */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the modem serial port.  This
   function returns a positive value on success and zero on failure.  A
   negative return value indicates an invalid or corrupt Fifo object.
*/
static int ModemIflush(void)
{
   int status=0;

   /* flush any bytes in the Max3109:UART1's FIFOs of the RF interface */
   Max3109Flush(Spi1DevRfDuart,UART1);

   /* flush the modem serial port's fifo buffer */
   status=flush(&ModemFifo);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* test if modem transceiver is enabled                                   */
/*------------------------------------------------------------------------*/
/**
   This function determines if the transceiver for UART1 of the RF
   interface is enabled.  If successful, then this function returns a
   positive value; zero is returned, on failure.
*/
int ModemIsEnable(void)
{
   /* initialize the return value */
   int status=0; unsigned long int baud;

   /* test if the UART1 transceiver is enabled */
   if (RfIsEnable()>0 && ModemTransceiverIsEnable()>0)
   {
      /* retrieve the baud rate from UART1 */
      status = (Max3109BaudRateGet(Spi1DevRfDuart,UART1,&baud)>0) ? baud : 0;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write one byte to RF interface's Max3109:UART1 serial port */
/*------------------------------------------------------------------------*/
/**
   This function writes one byte to the RF interface's Max3109:UART1 and
   pauses until that byte is transmitted by the serial port.  

   \begin{verbatim}
   input:
      byte...This is the byte to add to the Tx fifo.

   output:
      This function returns a positive value on success or zero on
      failure. 
   \end{verbatim}
*/
static int ModemPutb(unsigned char byte)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a timeout limit */
   const unsigned long int timeout=20;

   /* initialize the reference time for the timeout loop */
   const unsigned long int To=HAL_GetTick();

   /* write the byte to the RF interface's Max3109:UART1 */
   if ((err=Max3109Putb(Spi1DevRfDuart,UART1,byte))<=0) {status=Max3109Fail;}

   /* loop to wait until the byte has been transmitted */
   else while ((HAL_GetTick()-To)<timeout)
   {
      /* wait until the Tx FIFO is empty */
      if (!Max3109OBytes(Spi1DevRfDuart,UART1)) {break;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* funtion to return the state of the RING line of the modem serial port  */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the ring-indicator (RI) signal
   of UART1.  If RI is asserted, pin 6 of J3 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J3:6 is
   asserted if the max3109 RI is clear.  This function returns one if
   RI is asserted or zero otherwise.
*/
int ModemRing(void)
{
   /* initialize the return */
   int status=0;

   /* define local work object */
   unsigned char value;

   /* read the state of the four GPIO pins associated with UART1 */
   if (Max3109RegRead(Spi1DevRfDuart,UART1,GPIOData,&value)>0)
   {
      /* extract the state of pin GPIO7 (with transceiver inversion) */
      status=(value&0x80) ? 0 : 1;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert/clear the RTS signal of the modem serial port       */
/*------------------------------------------------------------------------*/
/**
   This function is used to assert or clear the RTS signal for the
   modem serial port.  To assert the RTS line then set the argument to
   be positive.  To clear the RTS line then set the argument to be
   zero or negative.  This function returns a positive value on
   success; otherwise zero is returned.
*/
static int ModemRts(int state)
{
   int status = (state>0) ? ModemRtsAssert() : ModemRtsClear();

   return ((status>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert the RTS line of the modem serial port               */
/*------------------------------------------------------------------------*/
/**
   This function asserts the RTS line of the modem serial port; pin 7
   of J3 will measure +5V.  The transceiver inverts the sense of the
   Max3109 signal and so asserting J3:7 requires clearing the Max3109 
   signal. If successful, a positive value is returned; zero is
   returned on failure.
*/
int ModemRtsAssert(void)
{
   /* assert the RTS pin of the RF interface's Max3109:UART1 */
   return ((Max3109RtsClear(Spi1DevRfDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the RTS line of the modem serial port                */
/*------------------------------------------------------------------------*/
/**
   This function clears the RTS line of the modem serial port; pin 7
   of J3 will measure -5V.  The transceiver inverts the sense of the
   Max3109 signal and so clearing J3:7 requires asserting the Max3109
   signal. If successful, a positive value is returned; zero is
   returned on failure.
*/
int ModemRtsClear(void)
{
   /* clear the RTS pin of the RF interface's Max3109:UART0 */
   return ((Max3109RtsAssert(Spi1DevRfDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to control the Max3245 transceiver's ForceOff signal          */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the Max3245 transceiver's
   ForceOff signal.  The transceiver converts between TTL logic levels
   and RS232 logic levels.  If the ForceOff signal is cleared then
   the Max3245 is induced into shutdown mode which will inhibit
   communications with the Iridium LBT; the ForceOff signal needs to
   be asserted in order for communications to happen between the LBT
   and the Apf11.

   \begin{verbatim}
   input:

      state...A nonzero value asserts the ForceOff signal which
              induces the Max3245 into shutdown mode.  A zero value
              clears the ForceOff signal which allows communication
              between the Apf11 and the LBT.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int ModemTransceiverForceOff(RfPortState state)
{
   return RfGpioStateSet(IRID_SIG_FORCE_OFF_N, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to control the Max3245 transceiver's ForceOn signal           */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the Max3245 transceiver's
   ForceOn signal.  The transceiver converts between TTL logic levels
   and RS232 logic levels.  If the ForceOn signal is cleared then
   the Max3245 is induced into automatic shutdown/wakeup mode with the
   objective of reducing energy consumption. If the ForceOn signal is
   asserted then the transceiver will remain awake regardless of
   serial port inactivity.

   \begin{verbatim}
   input:

      state...A zero value clears the ForceOn signal which induces the
              Max3245 into auto shutdown/wakeup mode.  A zero value
              clears the ForceOn signal which forces the transceiver
              to remain awake.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int ModemTransceiverForceOn(RfPortState state)
{
   return RfGpioStateSet(IRID_SIG_FORCE_ON, ((state>0)?ASSERT:CLEAR));
}
 
/*------------------------------------------------------------------------*/
/* function to determine if the modem transceiver is enabled              */
/*------------------------------------------------------------------------*/
/**
   The function to determine if the modem transceiver is enabled. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int ModemTransceiverIsEnable(void)
{
   /* initialize return value */
   int status=RfOk; RfPortState state;

   /* read the GPIO register from the Max7301 for the state of the ForceOn signal */
   if (RfGpioStateGet(IRID_SIG_FORCE_ON, &state)<0) {status=RfConfig;}

   /* check first criteria for an enabled transceiver */
   else if (state>0) {status=RfOk;}

   /* read the GPIO register from the Max7301 */
   else if (RfGpioStateGet(RF_3P3V_HIGH_SIDE_IRID_PHONE, &state)<0) {status=RfConfig;}

   /* check second criteria  */
   else {status = ((state>0) ? RfOk : RfFail);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute a break signal on the Modem's serial port          */
/*------------------------------------------------------------------------*/
/**
   This function executes a Tx-break signal on the Modem's serial port
   for a specified period of time.

   \begin{verbatim}
   input:
      millisec...This specifies the period of time (millisec) that the
                 break signal is maintained.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int ModemTxBreak(unsigned short int millisec)
{
   /* assert the RTS pin of the RF interface's Max3109:UART1 */
   return ((Max3109TxBreak(Spi1DevRfDuart,UART1,millisec)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na(void)
{
   return -1;
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na_(int state)
{
   return -1;
}

/*------------------------------------------------------------------------*/
/* function to disable the RF interface                                   */
/*------------------------------------------------------------------------*/
/**
   This function disables power to the RF interface and reinitializes
   the SPI{1,2} interfaces.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfDisable(void)
{
   /* define the logging signature */
   cc *FuncName="RfDisable()";
  
   /* initialize the return value */
   int err,status=RfOk;

   /* clear the GPIO's pin to reset the RF DUART */
   if ((err=RfGpioStateSet(DUART_RST_N,CLEAR))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Spare DUART's reset deactivation "
               "failed. [err=%d]\n",err); status=RfFail;
   }
   
   /* disable power to the RF interface */
   if ((err=Stm32GpioClear(GPIOB,RF_3P3V_PWR_EN_Pin|RF_VBAT_PWR_EN_Pin))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to disable the RF interface's "
               "power failed. [err=%d]\n",err); status=RfFail;
   }

   /* reinitialize the selected device on each SPI bus */
   Spi1Select(Spi1Dev); Spi2Select(Spi2Dev);
   
   /* reinitialize the SPI{1,2} interfaces */
   Stm32Spi1Init(); Stm32Spi2Init();

   return status;
}

/*------------------------------------------------------------------------*/
/* interrupt handler for the RF interface's serial interface              */
/*------------------------------------------------------------------------*/
/*
   This is the interrupt handler for the RF interface's serial
   interface.  The SwiftWare UART model is interrupt-driven and
   non-blocking with respect to received data.  For transmitted data,
   the SwiftWare UART model is non-blocking but not interrupt-driven.
   This function also manages error interrupts and maintains a record
   of UART errors.

   This function returns a positive value on success or else zero on
   failure.  A negative return value indicates that an exception was
   encountered. 
*/
int RfDuartIsr(void)
{
   /* initialize the return value */
   int status=0;

   /* define local work objects */
   int Xen; unsigned char irq,isr;
   
   /* read the IRQ bits from the Max3109 */
   if (Max3109Irq(Spi1DevRfDuart,&irq)>0)
   {
      /* test for IRQ on UART0 */
      if (irq&0x1)
      {
         /* clear the interrupt status register by reading it */
         Max3109RegRead(Spi1DevRfDuart,UART0,ISR,&isr);
         
         /* test if transceiver is enabled and for serviceable ISR conditions */
         if ((Xen=GpsTransceiverIsEnable())>0 && isr&0x09U)
         {
            /* pass a pointer to the UartStatus object if errors exist */
            UartStatus *comstat = (isr&0x01) ? &GpsStatus : NULL;
            
            /* transfer the contents of the Max3109:UART0 Rx FIFO */
            Max3109BurstRx(Spi1DevRfDuart,UART0,&GpsFifo,comstat);
         }

         /* clear the Rx/Tx FIFOs if the transceiver is not enabled */
         else if (!Xen) {Max3109Flush(Spi1DevRfDuart,UART0);}
      }
      
      /* test for IRQ on UART1 */
      if (irq&0x2)
      {
         /* clear the interrupt status register by reading it */
         Max3109RegRead(Spi1DevRfDuart,UART1,ISR,&isr);
         
         /* test if transceiver is enabled and for serviceable ISR conditions */
         if ((Xen=ModemTransceiverIsEnable())>0 && isr&0x09U)
         {
            /* pass a pointer to the UartStatus object if errors exist */
            UartStatus *comstat = (isr&0x01) ? &ModemStatus : NULL;
            
            /* transfer the contents of the Max3109:UART1 Rx FIFO */
            Max3109BurstRx(Spi1DevRfDuart,UART1,&ModemFifo,comstat);
         }

         /* clear the Rx/Tx FIFOs if the transceiver is not enabled */
         else if (!Xen) {Max3109Flush(Spi1DevRfDuart,UART1);}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to apply a square-wave reset signal to the RF DUART           */
/*------------------------------------------------------------------------*/
/**
   This function applies a square-wave reset signal of a specified
   duration to the reset pin of the RF DUART.  The reset pin is
   cleared which induces the DUART into a reset state.  The reset
   state is maintained for a specified period before the reset pin is
   asserted again to induce the DUART out of the reset state.
   Following that, another pause of the same duration is applied
   before the function returns.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfDuartReset(unsigned int millisec)
{
   /* define the logging signature */
   cc *FuncName="RfDuartReset()";
  
   /* initialize the return value */
   int err[2],status=RfOk;

   /* clear the RF DUART's reset pin to induce the DUART into a reset state */
   if ((err[0]=RfGpioStateSet(DUART_RST_N,CLEAR))<=0) {status=RfFail;}

   /* pause for the specified duration */
   if (millisec) {Wait(millisec);}

   /* assert the RF DUART's reset pin to induce the DUART out of a reset state */
   if ((err[1]=RfGpioStateSet(DUART_RST_N,ASSERT))<=0) {status=RfFail;}

   /* pause for the specified duration */
   if (millisec) {Wait(millisec);}

   /* log the failure to generate a square wave of the specified duration */
   if (status<=0) {LogEntry(FuncName,"Square wave of duration %umsec failed. "
                            "[err={%d,%d}]\n",millisec,err[0],err[1]);}
   
   return status;
}
 
/*------------------------------------------------------------------------*/
/* function to enable power to the RF DUART                               */
/*------------------------------------------------------------------------*/
/**
   This function enables power to the RF interface and initiates a
   reset of the RF DUART immediately after power-up.  If the RF
   interface is already powered then this function immediately returns
   without taking any action.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfEnable(void)
{
   /* define the logging signature */
   cc *FuncName="RfEnable()";
  
   /* initialize the return value */
   int err,status=RfOk;

   /* check if power to the RF board is already enabled */
   if (Stm32GpioIsAssert(GPIOB,RF_3P3V_PWR_EN_Pin|RF_VBAT_PWR_EN_Pin)<=0)
   {
      /* power-up the RF interface and initialize the Max7301 GPIO */
      if ((status=RfGpioInit())<=0)
      {
         /* log the failure */
         LogEntry(FuncName,"Attempt to enable RF interface failed. "
                  "[status=%d]\n",status);
      }

      /* reset the RF DUART */
      else if ((err=RfDuartReset(5))<=0)
      {
         /* log the reset failure */
         LogEntry(FuncName,"Attempt to reset the RF DUART failed. "
                  "[err=%d]\n",err); status=RfFail;
      }
   }

   /* check if SPI2 bus has already been configured for communication with RF GPIO interface */
   if (Spi2Select(SpiDevQuery)!=Spi2DevRfGpio)
   {
      /* configure the Stm32 for communication with the MAX7301 GPIO expander */
      if ((err=Stm32SpiConfig(SPI2,(WordMode|SpiBaudRate),Cr2,NoCrc))<=0)
      {
         /* make a logentry of the configuration failure */
         LogEntry(FuncName,"Attempt to configure the SPI2 bus failed. "
                  "[err=%d]\n",err); status=RfSpiConfig;
      }
      
      /* select the MAX7301 GPIO expander for communication */
      else if ((err=Spi2Select(Spi2DevRfGpio))<=0)
      {
         /* make a logentry of the selection failure */
         LogEntry(FuncName,"Attempt to select the RF Max7301 GPIO interface "
                  "failed. [err=%d]\n",err); status=RfFail;
      }
   }
      
   /* induce the MAX7301 out of suspend/sleep mode */
   if (status>=0 && (err=RfGpioWake())<=0)
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"Attempt to wake the RF MAX7301 GPIO interface "
               "failed. [err=%d]\n",err); status=RfFail;
   }
   
   /* if enabling the RF interface failed then disable power to the RF interface */
   if (status<=0) {Stm32GpioClear(GPIOB, RF_3P3V_PWR_EN_Pin|RF_VBAT_PWR_EN_Pin);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure a USER port                                      */
/*------------------------------------------------------------------------*/
/**
   This function allows the user to configure any of the USER ports.
   This function enforces that only USER ports are allowed to be
   reconfigured and ensures that the configuration selection itself is
   valid.

   \begin{verbatim}
   input:

      port....One of the twelve port identifiers in the ranges:
                 {[GPP7,GPP11], [GPP24,GPP30]}
              that are subject to user control and whose electrical
              interface is on the RF interface's J1 header, J1:[9-20].

     config...One of the four valid port configuration opcodes:
              RfOutPPL, RfOutPPH, RfInFlt, RfInPU.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioConfig(unsigned short int port, RfGpioPortConfig config)
{
   /* define the logging signature */
   cc *FuncName="RfGpioConfig()";
  
   /* initialize the return value */
   int err,status=RfOk;
   
   /* ensure the port is configurable */
   if (!(port>=GPP7 && port<=GPP11) && !(port>=GPP24 && port<=GPP30))
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Nonconfigurable port selected: 0x%04x\n",(port>>8)); status=RfNull;
   }

   /* validate the configuration */
   else if (config!=RfOutPPL && config!=RfOutPPH && config!=RfInFlt && config!=RfInPU)
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port configuration: 0x%02x\n",config); status=RfInvalid;
   }
            
   /* enable the RF interface */
   else if ((err=RfEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"RF interface failed to enable. [err=%d]\n",err); status=err;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int quad,new;

      /* compute the port quad */
      volatile unsigned short int QUAD = CONFIG_P04_P07 + (((port>>8)/4-1)*0x0100U);
      
      /* transmit the command to read the port configuration from the max7301 */
      RfGpioCsPulse(0); SPI2->DR = (READ | QUAD); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      quad=SPI2->DR; err=SPI2->SR;
        
      /* generate the SCK signal required to read the configuration from max7301 */
      RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); quad=SPI2->DR; err=SPI2->SR;

      /* clear the bits for the port to be configured */
      new = (quad&0xff) & (~(0x3U<<(2*((port>>8)%4))));

      /* compute the port's new configuration */
      new |= (config<<(2*((port>>8)%4)));

      /* check if the new/old configurations differ */
      if (new!=(quad&0xff))
      {
         /* transmit the new port configuration command to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (WRITE | QUAD | new); Stm32SpiBusy(SPI2,timeout);

         /* transmit the port configuration command to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (READ  | QUAD);       Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); quad=SPI2->DR; err=SPI2->SR;

         /* confirm the new configuration */
         if (new!=(quad&0xff))
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Configuration failure for port 0x%02x: 0x%02x != 0x%02x "
                     "(expected).\n",(port>>8),(quad&0xff),new); status=RfConfig;
         }
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to pulse the MAX7301 chip-select signal                       */
/*------------------------------------------------------------------------*/
/**
   This function pulses the MAX7301's chip-select signal by first
   asserting and then clearing the signal.  After each of the assert
   and clear operations, a user-specified pause (0-255 milliseconds)
   is inserted; the pattern is assert, pause, clear, pause.  The end
   result is a square wave whose period is twice the user-specified
   pause.

   \begin{verbatim}
   input:
      msec....The number of milliseconds to pause after EACH of the
              assert and clear operations.  The total period of the
              square-wave pulse is twice this value.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioCsPulse(unsigned char msec) {return Spi2CsPulse(Spi2DevRfGpio,msec);}

/*------------------------------------------------------------------------*/
/* function to initialize the MAX7301 RF GPIO interface                   */
/*------------------------------------------------------------------------*/
/**
   This function initializes RF GPIO interface by powering-up the
   Max7301, configuring the Stm32 SPI2 bus, and using the SPI2 bus to
   configure the Max7301.  After initialization is complete, the
   Max7301 is induced into sleep/suspend mode to reduce energy
   consumption.  While in sleep mode, all of its ports revert to input
   (which can be read) and the pull-up current sources are disabled;
   the device can still communicate via the SPI2 interface.  However,
   data in the port and control registers are retained so that port
   configuration and output levels are restored when the Max7301 exits
   sleep mode.  The Apf11 is designed such that when the Max7301 is
   powered-down or in sleep mode then the peripherals attached to the
   RF GPIO are in a well-defined low-power inactive state.

   This function is intended to be called only once during the boot-up
   phase of the Apf11; subsequent execution of this function is
   acceptable but unnecessary.  When the RF GPIO is to be used, it
   is necessary only to call RfGpioEnable() to exit sleep mode.

   \begin{verbatim}
    output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioInit(void)
{
   /* define the logging signature */
   cc *FuncName="RfGpioInit()";
  
   /* initialize the return value */
   int err,status=RfOk;

   /* enable power to the RF board */
   if ((err=Stm32GpioAssert(GPIOB,RF_3P3V_PWR_EN_Pin|RF_VBAT_PWR_EN_Pin))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to power-up RF interface failed. "
               "[err=%d]\n",err); status=RfPowerFail;
   }
   
   /* configure the Stm32 for communication with the MAX7301 GPIO expander */
   else if ((err=Stm32SpiConfig(SPI2,(WordMode|SpiBaudRate),Cr2,NoCrc))<=0)
   {
      /* make a logentry of the configuration failure */
      LogEntry(FuncName,"Attempt to configure SPI2 failed. "
               "[err=%d]\n",err); status=SpareSpiConfig;
   }

   /* select the MAX7301 GPIO expander for communication */
   else if ((err=Spi2Select(Spi2DevRfGpio))<=0)
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Attempt to select the spare GPIO failed. "
               "[err=%d]\n",err); status=SpareFail;
   }

   else
   {
      /* define local work objects */
      int n,ports; volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* loop over the seven sets of quad-ports to write the configuration */
      for (n=0, ports=CONFIG_P04_P07; ports<=CONFIG_P28_P31; ports+=0x0100U, n++)
      {
         /* transmit the port configuration command to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (WRITE | ports | Max7301Config[n]); Stm32SpiBusy(SPI2,timeout);
      }

      /* loop over the seven sets of quad-ports to read/confirm the configuration */
      for (status=RfOk, n=0, ports=CONFIG_P04_P07; ports<=CONFIG_P28_P31; ports+=0x0100U, n++)
      {
         /* transmit the port configuration command to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (READ | ports); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* compare the port configuration */
         if ((config&0xff)!=Max7301Config[n])
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Configuration failure for ports at 0x%04x: 0x%02x != 0x%02x "
                     "(expected).\n",ports,config,Max7301Config[n]); status=RfConfig;
         }
      }

      /* make a logentry that the MAX7301 configuration failed */
      if (status<=0) {LogEntry(FuncName,"MAX7301 configuration failed. [status=%d].\n",status);}

      /* check logging criteria */
      else if (debuglevel>=4 || (debugbits&MAX7301_H))
      {
         /* make a logentry that the MAX7301 configuration was successful */
         LogEntry(FuncName,"MAX7301 configuration successful.\n");
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to induce the Max7301 GPIO expander into sleep mode           */
/*------------------------------------------------------------------------*/
/**
   This function induces the Max7301 into sleep/shutdown mode.  The device
   remains powered and configured.   However, all ports are forced to
   inputs (which can be read), and the pull-up current sources are
   disabled.  Data in the port and configuration registers are
   retained so that port configuration and output levels are restored
   when the device is induced out of shutdown mode.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioSleep(void)
{
   /* define the logging signature */
   cc *FuncName="RfGpioSleep()";
  
   /* initialize the return value */
   int err,status=RfOk; 

   /* check if the RF sensor interface is powered up */
   if ((err=RfEnable())<=0)
   {
      /* make a logentry about the power failure */
      LogEntry(FuncName,"Aborting: RF interface failed to enable. "
               "[err=%d]\n",err); status=RfFail;
   }

   else 
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
      
      /* transmit the port configuration command to the max7301 */
      RfGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* check if the wake bit is enabled */                  
      if (config&WAKE)
      {
         /* clear the wake bit */
         config = (~WAKE) & (config&0xff);

         /* transmit the command to write the port configuration to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (WRITE | CONFIG | config); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify that the wake bit is cleared */
         if (config&WAKE) {LogEntry(FuncName,"MAX7301 failed to enter sleep/suspend "
                                    "mode. [config=%d]\n",config); status=RfConfig;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the state of a port on the max7301                    */
/*------------------------------------------------------------------------*/
/**
   This function reads the state of a user specified port on the
   MAX7301 GPIO expander (Apf11:U4).  Any of the twenty-eight ports
   can be queried.

   \begin{verbatim}
   input:
      port....One of the twenty-eight port identifiers in the range
              [0x0400,0x1f00] as defined near the top of this file.
              This function detects and rejects invalid port
              specifiers.

   output:

      state...The current state of the specified port.  If successful,
              this function will return CLEAR for logical zero or else
              ASSERT for logical one.  The value UNDEF is returned
              if an exception is encountered.

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioStateGet(unsigned short int port, RfPortState *state)
{
   /* define the logging signature */
   cc *FuncName="RfGpioStateGet()";
  
   /* initialize the return value */
   int err,status=RfOk;

   /* initialize return value */
   if (state) {(*state)=UNDEF;}
   
   /* validate the function argument */
   if ((port&0xff) || ((port>>8)<4) || ((port>>8)>31))
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port selected: %u\n",port); status=RfNull;
   }
   
   /* enable the RF interface */
   else if ((err=RfEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"RF interface failed to enable. [err=%d]\n",err); status=err;
   }
   
   else
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
 
      /* transmit the command to read the port configuration from the max7301 */
      RfGpioCsPulse(0); SPI2->DR = (READ  | PIN | port); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* record the state of the port */
      (*state) = config&0x1;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to set the state of one of the max7301 output ports           */
/*------------------------------------------------------------------------*/
/**
   This function sets the state of one of the Max7301 output ports to
   a user specified value of CLEAR or ASSERT as defined in the
   PinState enumeration.  Only ports that are configured to be outputs
   are eligible for control.  An exception is generated if an attempt
   is made to control the output of a port that is configured as an
   input.  

   \begin{verbatim}
   input:
      port....One of the twenty-eight port identifiers in the range
              [0x0400,0x1f00] as defined near the top of this file.
              This function detects and rejects invalid port
              specifiers.

      state...The specified state for the specified port.  If
              positive, then this function will set the output to a
              logical one.  Otherwise, this function will set the
              output to a logical zero.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioStateSet(unsigned short int port, RfPortState state)
{
   /* define the logging signature */
   cc *FuncName="RfGpioStateSet()";
  
   /* initialize the return value */
   int err,status=RfOk;

   /* validate the function argument */
   if ((port&0xff) || ((port>>8)<4) || ((port>>8)>31))
   {
      /* make a logentry of the invalid port selection */
      LogEntry(FuncName,"Invalid port selected: %u\n",port); status=RfNull;
   }
   
   /* enable the RF interface */
   else if ((err=RfEnable())<=0) 
   {
      /* make a logentry of the wake failure */
      LogEntry(FuncName,"RF interface failed to enable. [err=%d]\n",err); status=err;
   }
   
   else
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* compute the port quad */
      unsigned short int QUAD = CONFIG_P04_P07 + (((port>>8)/4-1)*0x0100U);
      
      /* transmit the command to read the port configuration from the max7301 */
      RfGpioCsPulse(0); SPI2->DR = (READ | QUAD); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
        
      /* generate the SCK signal required to read the configuration from max7301 */
      RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* isolate the configuration of the port */
      config = (config>>(2*((port>>8)%4)))&0x3U;

      /* check if the port is configured for output */
      if (config==RfOutPPL || config==RfOutPPH) 
      {
         /* validate the state */
         state = (state>0) ? ASSERT : CLEAR;
         
         /* transmit the command to assert the port */
         RfGpioCsPulse(0); SPI2->DR = (WRITE | PIN | port | state); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (READ  | PIN | port);         Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify the current state of the port matches the specification */
         if ((config&0xffU)!=state) {LogEntry(FuncName,"Port(%u) state mismatch: 0x%02x != 0x%02x (expected).\n",
                                              (port>>8),(config&0xff),state); status=RfFail;}
      }

      /* check if the port is configured for input */
      else if (config==RfInFlt || config==RfInPU) {LogEntry(FuncName,"Exception: Port 0x%02x configured "
                                                            "for input.\n",(port>>8)); status=RfInvalid;}

      /* make a logentry about the invalid port configuration */
      else {LogEntry(FuncName,"Port 0x%02x has invalid configuration: 0x%02x\n",(port>>8),config);}
   }
   
   return status;
}


/*------------------------------------------------------------------------*/
/* function to induce the Max7301 GPIO expander out of sleep mode         */
/*------------------------------------------------------------------------*/
/**
   This function induces the Max7301 out of sleep/shutdown mode.  The
   data in the port and configuration registers are retained while in
   sleep mode so that port configuration and output levels are
   restored when the device is induced out of shutdown mode.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfGpioWake(void)
{
   /* define the logging signature */
   cc *FuncName="RfGpioWake()";
  
   /* initialize the return value */
   int err,status=RfOk; 

   /* check if the RF GPIO interface is currently selected */
   if ((err=Spi2Select(SpiDevQuery))!=Spi2DevRfGpio)
   {
      /* make a logentry about the power failure */
      LogEntry(FuncName,"Aborting: RF GPIO interface is not currently "
               "selected. [err=%d]\n",err); status=RfFail;
   }

   else 
   {
      /* define local work object */
      volatile unsigned short int config;
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;
      
      /* transmit the port configuration command to the max7301 */
      RfGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */ 
      config=SPI2->DR; err=SPI2->SR;
         
      /* generate the SCK signal required to read the configuration from max7301 */
      RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

      /* check if the wake bit is enabled */                  
      if (!(config&WAKE))
      {
         /* clear the wake bit */
         config = WAKE | (config&0xff);

         /* transmit the command to write the port configuration to the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (WRITE | CONFIG | config); Stm32SpiBusy(SPI2,timeout);
 
         /* transmit the command to read the port configuration from the max7301 */
         RfGpioCsPulse(0); SPI2->DR = (READ | CONFIG); Stm32SpiBusy(SPI2,timeout);

         /* clear the Stm32's status flags */ 
         config=SPI2->DR; err=SPI2->SR;
         
         /* generate the SCK signal required to read the configuration from max7301 */
         RfGpioCsPulse(0); SPI2->DR=NOOP; Stm32SpiRxne(SPI2,timeout); config=SPI2->DR; err=SPI2->SR;

         /* verify that the wake bit is cleared */
         if (!(config&WAKE)) {LogEntry(FuncName,"MAX7301 failed to exit sleep/suspend "
                                       "mode. [config=%d]\n",config); status=RfConfig;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if RF interface is enabled                       */
/*------------------------------------------------------------------------*/
/**
   This function determines if the RF interface is enabled. This
   function returns a positive value on success or zero on failure.  A
   negative return value indicates that an exception was encountered.
*/
int RfIsEnable(void)
{
   /* define the logging signature */
   cc *FuncName="RfIsEnable()";
  
   /* initialize the return value */
   int status=RfOk;

   /* check if power to the RF board is already enabled */
   if ((status=Stm32GpioIsAssert(GPIOB,RF_3P3V_PWR_EN_Pin|RF_VBAT_PWR_EN_Pin))<0)
   {
      LogEntry(FuncName,"Unable to determine state of RF "
               "interface. [status=%d]\n",status);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to control the display of LEDs on the RF interface            */
/*------------------------------------------------------------------------*/
/**
   This function controls the display of LEDs on the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the display of LEDs on the RF
      interface.  A zero value disables the LEDs.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfLed(RfPortState state)
{
   return RfGpioStateSet(LED_ENABLE, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* control the state of battery power supply on RF's spare sensor port    */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the switched battery power
   supply to the RF interface's spare sensor port (J1).

   \begin{verbatim}
   input:

      state...A nonzero value enables the switched battery power
              supply on the J1 connector.  A zero value disables
              battery power to the J1 connector.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int RfSwitchedBatteryPower(RfPortState state)
{
   return RfGpioStateSet(VBATT_GPIO_ENABLE, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to control the state of the 5V power supply on RF interface   */
/*------------------------------------------------------------------------*/
/**
   This function controls the state of the 5V power supply on RF
   interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the 5V power supply on the RF
      interface.  A zero value disables the 5V power supply.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Rf5v(RfPortState state)
{
   return RfGpioStateSet(EN_5V, ((state>0)?ASSERT:CLEAR));
}

/*------------------------------------------------------------------------*/
/* function to enable/disable the SBD LBT                                 */
/*------------------------------------------------------------------------*/
/**
   This function enables/disables the SBD LBT but has no effect on any
   CSD LBT that might be connected to the RF interface.

   \begin{verbatim}
   input:

      state...A nonzero value enables the SBD LBT while a zero value
              disables the LBT.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Sbd(RfPortState state)
{
   return RfGpioStateSet(IRID_SBD_ON, ((state>0)?ASSERT:CLEAR));
}

#endif /* APF11RF_C */
