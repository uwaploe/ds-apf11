#ifndef CTDIO_H
#define CTDIO_H (0x4000)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 UART model
 * -------------------------------------------------
 *
 * The SwiftWare UART model is non-blocking for both received and
 * transmitted data but is interrupt-driven only for received data.
 * The DMA peripheral of the Stm32 is not used because, though more
 * efficient with respect to core MCU cycles, it is somewhat more
 * complicated and consumes slightly more power.  Even at the slowest
 * clock speed (8MHz), the Apf11 core remains considerably
 * underutilized with respect to processor cycles.  So both power
 * considerations and the KISS-principle disfavor the use of the DMA
 * peripheral for the Stm32 UARTs.
 *
 * For received data, an interrupt handler manages the transfer of
 * each byte from the Stm32's USART.DR register to the UART's FIFO
 * buffer.  The interrupt handler also detects and maintains a record
 * of over-run, noise, framing, and parity errors.  If too many errors
 * are detected, the handler disables error interrupts as a
 * fault-tolerance measure in order allow recovery and to avoid
 * infinite loops or interrupt-binding.
 *
 * For transmitted data, the SwiftWare UART model is not
 * interrupt-driven so that the precise timing of transmitted
 * characters can be known by higher level software that uses this
 * low-level API.  Instead of allowing an interrupt-handler to manage
 * the transmission of each byte, this API implements a function to
 * transmit a single character by loading the byte in the Tx register
 * and then monitoring the USART.SR register to detect assertion of
 * the transmit-complete (TC) flag.  After each byte is transmitted
 * then the function returns so that the next byte can be
 * transmitted.  A time-out feature prevents blocking.
 *
 * This API configures USART2 for use with the Sbe41cp in such a way
 * as to work-around an undesirable behavior of the Sbe41cp.
 * Unfortunately, a transition from low to high (or vice versa) on the
 * Tx pin will cause a sleeping CTD to collect a spot sample.  If
 * USART2 were to be configured for Rx/Tx communications then whenever
 * the Stm32 was awakened from standby mode then the Tx-pin of USART2
 * would transition from low to high.  Hence, an unintended CTD sample
 * would be generated each time that the Apf11 was awakened.  Each
 * profile cycle of a float is associated with many hundreds of
 * standby/wake cycles and so it is important to avoid such spurious
 * CTD spot samples which would waste considerable energy.
 *
 * In order to inhibit this undesirable behavior, then USART2 is
 * configured for only for Rx-communications.  The Tx pin is
 * configured for GPIO and initializes in a low state to inhibit the
 * unintended sample.  To enter communications mode then the USART2
 * serial port must be reconfigured to support both Rx & Tx
 * communications.
 *
 *    Important Note: The SwiftWare UART model was designed as a
 *       drop-in replacment for ST's HAL UART model because the HAL
 *       model was terribly unreliable, complicated, full of nasty
 *       bugs, and resulted in frequent (implicit) infinite loops.
 *       The SwiftWare model is very much simpler, more reliable, and
 *       has no explicit or (as far as I know) implicit infinite
 *       loops.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define ctdioChangeLog "$RCSfile$  $Revision$   $Date$"

#include <serial.h>

/* prototypes for functions with external linkage */
int CtdActiveIo(time_t timeout);
int CtdAssertModePin(void);
int CtdAssertTxPin(void);
int CtdAssertWakePin(void);
int CtdClearModePin(void);
int CtdClearTxPin(void);
int CtdClearWakePin(void);
int CtdDisableRx(void);
int CtdDisableTx(void);
int CtdEnableRx(void);
int CtdEnableTx(void);
int CtdIsEnableTx(void);
int CtdPower(unsigned short* VoltCnt, unsigned short* AmpCnt, time_t timeout);
int CtdPSample(char *buf, int size);
int CtdPtSample(char *buf, int size);
int CtdPtsSample(char *buf, int size, time_t timeout);

/* prototypes for functions related to the CTD serial port */
long int CtdConfig(long int baud);
int      CtdErrors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
                   unsigned char *ovrun, unsigned char *noise);

/* declare the console serial port */
extern struct SerialPort ctdio;

/* define the return states of the Stm32 UART API */
extern const char CtdNoConfig;           /* uart not configured or enabled */
extern const char CtdError;              /* uart error encountered */
extern const char CtdInvalidBaud;        /* invalid baud rate */
extern const char CtdNull;               /* NULL function argument */
extern const char CtdFail;               /* general failure */
extern const char CtdOk;                 /* general success */

#endif /* CTDIO_H */
#ifdef CTDIO_C

#include <apf11.h>
#include <apf11ad.h>
#include <assert.h>
#include <fifo.h>
#include <logger.h>
#include <mcu.h>
#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>
#include <string.h>

/* define the return states of the Stm32 UART API */
const char CtdNoConfig          =  -4; /* uart not configured or enabled */
const char CtdError             =  -3; /* uart error encountered */
const char CtdInvalidBaud       =  -2; /* invalid baud rate */
const char CtdNull              =  -1; /* NULL function argument */
const char CtdFail              =   0; /* general failure */
const char CtdOk                =   1; /* general success */

/* declare external management functions for static UART_HandleTypeDef object */
UART_HandleTypeDef *HAL_UART2_HandleGet(void);

/* declare functions with external linkage */
int CtdIrqHandler(void);

/* declare functions with static linkage */
static int na(void); 
static int na_(int state);
static int CtdGetb(unsigned char *byte);
static int CtdIFlush(void);
static int CtdIBytes(void);
static int CtdIOFlush(void);
static int CtdOBytes(void);
static int CtdOFlush(void);
static int CtdPutb(unsigned char c);

/* define the UART_HandleTypeDef object for UART1 */      
static UART_HandleTypeDef huart2;

/* define a fifo buffer for UART2 */
persistent static unsigned char CtdFifoBuf[65536U];

/* define a Fifo object for the console terminal */
struct Fifo CtdFifo = {CtdFifoBuf, sizeof(CtdFifoBuf), 0, 0, 0, 0};

/* define a serial port for the Stm32's USART2 interface */
struct SerialPort ctdio = {CtdGetb, CtdPutb, CtdIFlush, CtdIOFlush, CtdOFlush,
                           CtdIBytes, CtdOBytes, na, na_, na, na_, na, CtdConfig};

/*------------------------------------------------------------------------*/
/* anonymous structure to contain status information for USART2 I/O       */
/*------------------------------------------------------------------------*/
static struct 
{
   /* counts of various error conditions */
   unsigned char nbreak,parity,frame,noise,ovrun;
      
} CtdStatus = {0,0,0,0,0};

/*------------------------------------------------------------------------*/
/* function to determine if CTD IO is active                              */
/*------------------------------------------------------------------------*/
/**
   This function determines if the CTD serial port is active.  The serial
   port is defined to be active if the length of the CtdFifo changes within
   a user-specified time-out period.

      \begin{verbatim}
      input:
         timeout ... The CTD serial port is determined to be active if the
                     length of the CtdFifo changes within this length of
                     time.

      output:
         This function returns a positive number if the CTD serial port is
         determined to be active and zero otherwise.
      \end{verbatim}
*/
int CtdActiveIo(time_t timeout)
{
   int status=CtdFail;
   const long int length=CtdFifo.length;
   const long int boc=CtdFifo.BufOverflowCount;
   const time_t To=time(NULL);
      
   while (difftime(time(NULL),To)<timeout) 
   {
      if (CtdFifo.length!=length || CtdFifo.BufOverflowCount!=boc) {status=CtdOk; break;}
   }
   
   return status;
}
 
/*------------------------------------------------------------------------*/
/* function to assert the Apf11's CTD PTS/FP mode-select pin              */
/*------------------------------------------------------------------------*/
/**
   This function asserts the Apf11's CTD PTS/FP mode-select pin. 

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdAssertModePin(void)
{
   /* assert the CTD_PTS_FP pin of the Stm32 */
   int status=Stm32GpioAssert(CTD_PTS_FP_GPIO_Port, CTD_PTS_FP_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert the Apf11's CTD Tx pin                              */
/*------------------------------------------------------------------------*/
/**
   This function asserts the Apf11's CTD Tx pin.  For the Sbe41's
   hardware interface, the Tx pin is configured as a general-purpose
   output pin.  This function reconfigures the Tx pin as a GPIO output
   pin and initializes the pin in a high state.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdAssertTxPin(void)
{
   int status=CtdNull;

   /* disable transmissions on the CTD serial port */
   CLEAR_BIT(huart2.Instance->CR1,USART_CR1_TE);

   /* read the configuration of ports A.0-A.7 */
   volatile unsigned long int CRL=GPIOA->CRL;

   /* configure port A.2 for general purpose push-pull output */
   CRL &= 0xfffff0ff; CRL |= 0x00000100; GPIOA->CRL=CRL;
   
   /* assert the CTD Tx pin (A.2) of the Stm32 */
   status=Stm32GpioAssert(GPIOA, UART2_TXD_Pin);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert the Apf11's CTD power-control pin                   */
/*------------------------------------------------------------------------*/
/**
   This function asserts the Apf11's CTD power-control pin. 

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdAssertWakePin(void)
{
   /* assert the CTD wake-pin of the Stm32 */
   int status=Stm32GpioAssert(CTD_PWR_CTL_GPIO_Port, CTD_PWR_CTL_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear the Apf11's CTD PTS/FP mode-select pin               */
/*------------------------------------------------------------------------*/
/**
   This function clears the Apf11's CTD PTS/FP mode-select pin.  

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdClearModePin(void)
{
   /* clear the CTD_PTS_FP pin of the Stm32 */
   int status=Stm32GpioClear(CTD_PTS_FP_GPIO_Port, CTD_PTS_FP_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear the Apf11's CTD Tx pin                               */
/*------------------------------------------------------------------------*/
/**
   This function clears the Apf11's CTD Tx pin.  For the Sbe41's
   hardware interface, the Tx pin is configured as a general-purpose
   output pin.  This function reconfigures the Tx pin as a GPIO output
   pin and initializes the pin in a low state.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdClearTxPin(void)
{
   /* initialize the return value */
   int status=CtdNull;
 
   /* disable transmissions on the CTD serial port */
   CLEAR_BIT(huart2.Instance->CR1,USART_CR1_TE);
    
   /* read the configuration of ports A.0-A.7 */
   volatile unsigned long int CRL=GPIOA->CRL;

   /* configure port A.2 for general purpose push-pull output */
   CRL &= 0xfffff0ff; CRL |= 0x00000100; GPIOA->CRL=CRL;
 
   /* clear the CTD Tx pin (A.2) of the Stm32 */
   status=Stm32GpioClear(GPIOA, UART2_TXD_Pin);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear the Apf11's CTD power-control pin                    */
/*------------------------------------------------------------------------*/
/**
   This function clears the Apf11's CTD power-control pin.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdClearWakePin(void)
{
   /* clear the CTD wake-pin of the Stm32 */
   int status=Stm32GpioClear(CTD_PWR_CTL_GPIO_Port, CTD_PWR_CTL_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* SerialPort member function to configure the USART2 serial interface    */
/*------------------------------------------------------------------------*/
/**
   This function is the member of the SerialPort abstraction object
   that is designed to manage the configuration of the Stm32's USART2
   serial port.  The Stm32 is configured so that USART2 is strictly a
   3-wire interface (Tx,Rx,Gnd); the Stm32 GPIO pins that would be
   necessary to implement RTS/CTS hardware handshaking have been
   configured for other uses.

   This function configures USART2 for use with the Sbe41cp in such a
   way as to work-around an undesirable behavior of the Sbe41cp.
   Unfortunately, a transition from low to high (or vice versa) on the
   Tx pin will cause a sleeping CTD to collect a spot sample.  If
   USART2 were to be configured for Rx/Tx communications then whenever
   the Stm32 was awakened from standby mode then the Tx-pin of USART2
   would transition from low to high.  Hence, an unintended CTD sample
   would be generated each time that the Apf11 was awakened.  In order
   to inhibit this undesirable behavior, then USART2 is configured for
   only for Rx-communications.  The Tx pin is configured for GPIO and
   initializes in a low state to inhibit the unintended sample.

   In order for the Apf11 to enter communications mode then the USART2
   serial port must be reconfigured to support both Rx & Tx
   communications.

      \begin{verbatim}
      input:

         baud....This parameter controls whether this function enables
                 or disables the USART2 serial port or else queries
                 for its current state/baud-rate:

                   baud>0: If the specified mode is positive then
                      USART2 is enabled and configured to use the
                      specified baud-rate.  For example, if mode=9600
                      then the serial port is configured for 9600
                      baud.  The supported baud rates are: 300, 1200,
                      2400, 4800, 9600, 19200, 38400.

                   mode=0: If the specified mode is zero then the
                      USART2 serial port is disabled.

                   mode<0: If the specified mode is negative then this
                      function queries USART2 to determine if it is
                      enabled or disabled; if enabled then the
                      baud-rate is returned.

         output:

            If the mode is positive and USART2 was successfully
            enabled/configured then this function returns a positive
            value.  If the mode is zero and USART2 is successfully
            disabled then this function returns a positive value.  If
            the mode was negative the this function returns the
            current baud rate if USART2 is enabled else zero if USART2
            is disabled.  A negative return value indicates that an
            exception was encountered.
      \end{verbatim}
*/
long int CtdConfig(long int baud)
{
   /* define the logging signature */
   cc *FuncName="CtdConfig()";
   
   /* initialize the return value */
   int status=CtdOk;

   /* configure and enable USART2 */
   if (baud>0)
   {
      /* configure Stm32 UART2 Tx pin (A.2) */
      Stm32GpioConfig(UART2_TXD_GPIO_Port, UART2_TXD_Pin, OutPPL);

      /* configure Stm32 UART2 Rx pin (A.3) */
      Stm32GpioConfig(UART2_RXD_GPIO_Port, UART2_RXD_Pin, InFlt);

      /* initialize the UART handle object */
      memset((void *)(&huart2),0,sizeof(UART_HandleTypeDef));

      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART2_IRQn);

      /* initialize the Stm32 peripheral address */
      huart2.Instance = USART2;

      /* disable USART2 and its clock */
      __HAL_UART_DISABLE(&huart2); __HAL_RCC_USART2_CLK_DISABLE(); 

      /* configure for Rx-only communications */
      huart2.Init.Mode = UART_MODE_RX;

      /* disable hardware flow control */
      huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;

      /* Stm32f103 has only one available mode for oversampling */
      huart2.Init.OverSampling = UART_OVERSAMPLING_16;
      
      /* initialize the UART for 8-bits, 1 stop bit, no parity */
      huart2.Init.WordLength = UART_WORDLENGTH_8B;
      huart2.Init.StopBits = UART_STOPBITS_1;
      huart2.Init.Parity = UART_PARITY_NONE;
      
      /* select the baud rate */
      switch (baud)
      {
         /* enumerate the supported baud rates */
         case   300LU: case  1200LU: case  2400LU: case   4800LU: case 9600LU:
         case 19200LU: case 38400LU: case 57600LU: case 115200LU:
         {
            /* initialize the baud rate */
            huart2.Init.BaudRate = baud; break;
         }

         /* catch unsupported baud rates */
         default:
         {
            /* log the error */
            LogEntry(FuncName,"UART2: Unsupported baud rate: %lu\n",baud);

            /* reset the return value */
            status=CtdInvalidBaud;
         }
      }

      /* configure and enable USART2 for the specified baud rate */
      if (status>0)
      {
         /* enable the UART clock */
         __HAL_RCC_USART2_CLK_ENABLE();

         /* set temporary busy state */
         huart2.State = HAL_UART_STATE_BUSY;

         /* initialize USART2.CR1 to the reset state given in the Stm32f103 datasheet */
         huart2.Instance->CR1=0x00000000LU;

         /* configure USART2.CR1 for the word-length, parity, and tx/rx mode */
         SET_BIT(huart2.Instance->CR1, (uint32_t)(huart2.Init.WordLength | huart2.Init.Parity | huart2.Init.Mode));

         /* initialize USART2.CR2 to the reset state given in the Stm32f103 datasheet */
         huart2.Instance->CR2=0x00000000LU;

         /* configure USART2.CR2 with number of stop bits  */
         SET_BIT(huart2.Instance->CR2, huart2.Init.StopBits);

         /* initialize USART2.CR3 to the reset state given in the Stm32f103 datasheet */
         huart2.Instance->CR3=0x00000000LU;
 
         /* configure USART2.BRR with the baud rate */
         huart2.Instance->BRR = UART_BRR_SAMPLING16(HAL_RCC_GetPCLK1Freq(), huart2.Init.BaudRate);

         /* initialize USART2.GTPR to the reset state given in the Stm32f103 datasheet */
         huart2.Instance->GTPR=0x00000000LU;
   
         /* Enable the peripheral */
         __HAL_UART_ENABLE(&huart2);

         /* clear the status register by reading USART2.SR followed by USART2.DR */
         {volatile unsigned long int sr=huart2.Instance->SR; UNUSED(sr);}
         {volatile unsigned long int dr=huart2.Instance->DR; UNUSED(dr);}
      
         /* enable receiver interrupts on USART2 */
         SET_BIT(huart2.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);
      
         /* enable error interrupts on USART2 */
         SET_BIT(huart2.Instance->CR3, USART_CR3_EIE);

         /* initialize the peripheral interrupt */
         HAL_NVIC_SetPriority(USART2_IRQn, 1, 0); HAL_NVIC_EnableIRQ(USART2_IRQn);
      
         /* Initialize the UART error code and state */
         huart2.ErrorCode = HAL_UART_ERROR_NONE; huart2.State= HAL_UART_STATE_READY;
         
         /* flush the USART2 fifo */
         Wait(5); flush(&CtdFifo);
      }

      /* invalid configuration, deconfigure and disable USART2 */
      else CtdConfig(0);
   }
   
   /* query for the state of the USART2 serial interface */
   else if (baud<0) {status=(huart2.Instance==USART2) ? huart2.Init.BaudRate : CtdFail;}
   
   /* deconfigure and disable USART2 */
   else
   {
      /* disable the IRQ */
      HAL_NVIC_DisableIRQ(USART2_IRQn);

      /* initialize the Stm32 peripheral address */
      huart2.Instance = USART2;

      /* disable USART2 */
      __HAL_UART_DISABLE(&huart2);

      /* initialize USART2.CR1 to the reset state given in the Stm32f103 datasheet */
      huart2.Instance->CR1=0x00000000LU;

      /* initialize USART2.CR2 to the reset state given in the Stm32f103 datasheet */
      huart2.Instance->CR2=0x00000000LU;

      /* initialize USART2.CR3 to the reset state given in the Stm32f103 datasheet */
      huart2.Instance->CR3=0x00000000LU;

      /* initialize USART2.GTPR to the reset state given in the Stm32f103 datasheet */
      huart2.Instance->GTPR=0x00000000LU;
      
      /* disable the USART2 clock */
      __HAL_RCC_USART2_CLK_DISABLE(); 

      /* flush the USART2 fifo */
      flush(&CtdFifo);

      /* initialize the UART handle object */
      memset((void *)(&huart2),0,sizeof(UART_HandleTypeDef));
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable Rx interrupts the Apf11's CTD serial port          */
/*------------------------------------------------------------------------*/
/**
   This function disables the receive-interrupts for the Apf11's CTD
   (USART2) serial port.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdDisableRx(void)
{
   /* initialize the return value */
   int status=CtdOk;
      
   /* disable receiver interrupts on USART2 */
   CLEAR_BIT(huart2.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Apf11's CTD Tx pin for serial transmission   */
/*------------------------------------------------------------------------*/
/**
   This function configures the USART2 serial port to disable
   Tx-communication.  Rx communications remain enabled.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdDisableTx(void)
{
   /* initialize the return value */
   int status=CtdNull;

   /* disable transmissions on the CTD serial port */
   CLEAR_BIT(huart2.Instance->CR1,USART_CR1_TE);

   /* read the configuration of ports A.0-A.7 */
   volatile unsigned long int CRL=GPIOA->CRL;

   /* configure port A.2 for general purpose push-pull output */
   CRL &= 0xfffff0ff; CRL |= 0x00000100; GPIOA->CRL=CRL;
      
   /* clear the CTD Tx pin (A.2) of the Stm32 */
   status=Stm32GpioClear(GPIOA, UART2_TXD_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable Rx interrupts the Apf11's CTD serial port           */
/*------------------------------------------------------------------------*/
/**
   This function disables the receive-interrupts for the Apf11's CTD
   (USART2) serial port.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdEnableRx(void)
{
   /* initialize the return value */
   int status=CtdOk;
      
   /* enable receiver interrupts on USART2 */
   SET_BIT(huart2.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Apf11's CTD Tx pin for serial transmission   */
/*------------------------------------------------------------------------*/
/**
   This function configures the USART2 serial port to enable
   Tx-communication.  Rx communications remain enabled.

   This function returns a positive value if successful or else zero
   on failure.  A negtive value indicates an exception was encountered.
*/
int CtdEnableTx(void)
{
   /* initialize the return value */
   int status=CtdNull;

   /* disable transmissions on the CTD serial port */
   CLEAR_BIT(huart2.Instance->CR1,USART_CR1_TE);

   /* read the configuration of ports A.0-A.7 */
   volatile unsigned long int CRL=GPIOA->CRL;

   /* configure port A.2 for alternate-function push-pull output */
   CRL &= 0xfffff0ff; CRL |= 0x00000b00; GPIOA->CRL=CRL;

   /* enable transmissions on the CTD serial port */
   SET_BIT(huart2.Instance->CR1,USART_CR1_TE);
     
   return status;
}

/*------------------------------------------------------------------------*/
/* determine if Tx signal is enabled                                      */
/*------------------------------------------------------------------------*/
/**
   This function determines if USART2 is enabled for bidirectional
   communications using the Rx & Tx signals.  If the transmit-enable
   (TE) bit of USART2's CR1 register is asserted then bidirectional
   communications are enabled.

   This function returns a positive value if the Tx signal is enabled
   on USART2. Zero indicates that the Tx signal is disabled.
*/
int CtdIsEnableTx(void)
{
   /* check if the TE bit of CR1 register is asserted */
   int status = (huart2.Instance->CR1&USART_CR1_TE) ? 1 : 0;

   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the Ctd interrupt           */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Ctd
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int CtdErrors(unsigned char *nbreak,unsigned char *parity,unsigned char *frame,
              unsigned char *ovrun,unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      CtdStatus.nbreak=0; CtdStatus.parity=0; CtdStatus.frame=0;
      CtdStatus.ovrun=0;  CtdStatus.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=CtdStatus.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=CtdStatus.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=CtdStatus.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=CtdStatus.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=CtdStatus.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the Ctd FIFO queue                        */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the Ctd serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the Ctd FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int CtdGetb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "CtdGetb()";

   /* initialize the return value */
   int status=CtdFail;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=CtdNull;}

   /* validate the configuration for USART2 */
   else if (huart2.Instance!=USART2) {LogEntry(FuncName,"Invalid UART2 configuration.\n"); status=CtdNoConfig;}
   
   else
   {
      /* disable receive interrupts on UART2 to prevent the fifo from changing state */
      CLEAR_BIT(huart2.Instance->CR1, USART_CR1_RXNEIE);

      /* pop the next byte out of the fifo queue */
      status=pop(&CtdFifo,byte);
          
      /* enable receive interrupts on UART2 */
      SET_BIT(huart2.Instance->CR1, USART_CR1_RXNEIE);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue of USART2  */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes in the Rx FIFO queue of USART2.
*/
static int CtdIBytes(void)
{
   return CtdFifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the USART2's Rx FIFO queue                           */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the USART2 serial port.
   This function returns a positive value on success and zero on
   failure.  A negative return value indicates an exception was
   encountered.
*/
static int CtdIFlush(void)
{
   /* define the logging signature */
   cc *FuncName = "CtdIFlush()";

   /* initialize the return value */
   int status=CtdOk;
            
   /* validate the configuration for USART2 */
   if (huart2.Instance!=USART2) {LogEntry(FuncName,"Invalid UART2 configuration.\n"); status=CtdNoConfig;}

   else
   {
      /* disable receive interrupts on UART2 to prevent the fifo from changing state */
      CLEAR_BIT(huart2.Instance->CR1, USART_CR1_RXNEIE);

      /* flush the Com1 serial port's fifo buffer */
      status=flush(&CtdFifo); 
                
      /* enable receive interrupts on UART2 */
      SET_BIT(huart2.Instance->CR1, USART_CR1_RXNEIE);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to flush the USART2's Tx register and Rx FIFO queue           */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Tx register and the Rx FIFO queue of the
   USART2 serial port.  This function returns a positive value on
   success and zero on failure.  A negative return value indicates an
   exception was encountered.
*/
static int CtdIOFlush(void)
{
   /* initialize the return value */
   int err,status=CtdOk;
            
   /* flush the Tx register */
   if ((err=CtdOFlush())<=0) {status=err;}

   /* flush the Rx fifo */
   if ((err=CtdIFlush())<=0) {status=err;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* interrupt handler for input on the USART2 serial interface             */
/*------------------------------------------------------------------------*/
/*
   This is the interrupt handler for the USART2 serial interface.  The
   SwiftWare UART model is interrupt-driven and non-blocking with
   respect to received data.  For transmitted data, the SwiftWare UART
   model is non-blocking but not interrupt-driven as described in the
   comment section near the top of this module.  This function also
   manages error interrupts and maintains a record of UART errors.

   This function returns a positive value on success or else zero on
   failure.  A negative return value indicates that an exception was
   encountered. 
*/
int CtdIrqHandler(void)
{
   /* define the logging signature */
   cc *FuncName="CtdIrqHandler()";

   /* initialize the return value */
   int status=CtdOk;

   /* validate the configuration for USART2 */
   if (huart2.Instance!=USART2) {LogEntry(FuncName,"Invalid UART2 configuration.\n"); status=CtdNoConfig;}

   else
   {
      /* define maximum number of errors before error-interrupts are disabled */
      const unsigned char ErrMax=10;

      /* read the status register of USART2 */
      volatile unsigned long int SR = huart2.Instance->SR;

      /* read the data register of USART2 */
      volatile unsigned long int DR = huart2.Instance->DR;

      /* read control register 1 of USART2 */
      volatile unsigned long int CR1 = huart2.Instance->CR1; 

      /* read control register 2 of USART2 */
      volatile unsigned long int CR2 = huart2.Instance->CR2; 

      /* read control register 3 of USART2 */
      volatile unsigned long int CR3 = huart2.Instance->CR3; 

      /* temporarily disable receiver interrupts on USART2 */
      CLEAR_BIT(huart2.Instance->CR1, USART_CR1_PEIE | USART_CR1_IDLEIE | USART_CR1_RXNEIE);

      /* temporarily disable transmitter interrupts on USART2 */
      CLEAR_BIT(huart2.Instance->CR1, USART_CR1_TXEIE | USART_CR1_TCIE);

      /* temporarily disable error interrupts on USART2 */
      CLEAR_BIT(huart2.Instance->CR3, USART_CR3_EIE);

      /* avoid compiler warnings */
      UNUSED(CR2);
   
      /* test for parity error */
      if (SR&USART_SR_PE) 
      {
         /* check if parity-error interrupt is enabled */
         if (CR1&USART_CR1_PEIE) huart2.ErrorCode |= HAL_UART_ERROR_PE;

         /* increment the parity-error counter */
         if ((CtdStatus.parity++) > ErrMax) CLEAR_BIT(CR1,USART_CR1_PEIE);
      }

      /* test for framing error */
      if (SR&USART_SR_FE)
      {
         /* check if framing-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart2.ErrorCode |= HAL_UART_ERROR_FE;
      
         /* increment the framing-error counter */
         if ((CtdStatus.frame++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for noise error */
      if (SR&USART_SR_NE)
      {
         /* check if noise-error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart2.ErrorCode |= HAL_UART_ERROR_NE;

         /* increment the noise-error counter */
         if ((CtdStatus.noise++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for over-run error */
      if (SR&USART_SR_ORE)
      {
         /* check if over-runt error interrupt is enabled */
         if (CR3&USART_CR3_EIE) huart2.ErrorCode |= HAL_UART_ERROR_ORE;

         /* increment the over-run error counter */
         if ((CtdStatus.ovrun++) > ErrMax) CLEAR_BIT(CR3,USART_CR3_EIE);
      }

      /* test for byte received */
      if (SR&USART_SR_RXNE)
      {
         /* add the byte to the fifo */
         push(&CtdFifo,(unsigned char)(DR&0xff));
      }

      /* test if Tx-empty bit is asserted */
      if (SR&USART_SR_TXE)
      {
         /* SwiftWare USART2 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TXEIE) CLEAR_BIT(CR1,USART_CR1_TXEIE);
      }

      /* test if Tx-complete bit is asserted */
      if (SR&USART_SR_TC)
      {
         /* SwiftWare USART2 model requires interrupt-driven Tx to be disabled */
         if (CR1&USART_CR1_TCIE) CLEAR_BIT(CR1,USART_CR1_TCIE);
      }

      /* re-enable interrups on USART2 */
      huart2.Instance->CR1=CR1; huart2.Instance->CR3=CR3;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue of USART2  */
/*------------------------------------------------------------------------*/
/**
   This function returns the number of bytes in the Tx register of USART2.
*/
static int CtdOBytes(void) {return 0;}

/*------------------------------------------------------------------------*/
/* function to flush the USART2's Tx transmit register                    */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Tx register of the USART2 serial port.
   This function returns a positive value on success and zero on
   failure.  A negative return value indicates an exception was
   encountered.
*/
static int CtdOFlush(void)
{
   /* define the logging signature */
   cc *FuncName = "CtdOFlush()";

   /* initialize the return value */
   int status=CtdFail;
            
   /* validate the configuration for USART2 */
   if (huart2.Instance!=USART2) {LogEntry(FuncName,"Invalid UART2 configuration.\n"); status=CtdNoConfig;}

   else
   {
      /* define the timeout period */
      const unsigned long int Timeout=50;

      /* initialize the reference time for the time-out feature */
      unsigned long int To=HAL_GetTick();

      /* construct time-out loop */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart2.Instance->SR;

         /* check if the transmit register is empty */
         if (SR&USART_SR_TXE) {status=CtdOk; break;}
      }

      /* continue the time-out loop */
      for (status=CtdFail; (HAL_GetTick()-To)<Timeout;)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart2.Instance->SR;

         /* check if the transmit-complete flag is set */
         if (SR&USART_SR_TC) {status=CtdOk; break;}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* measure power consumption during a full PTS sample                     */
/*------------------------------------------------------------------------*/
/**
   This function measures the power consumption by the CTD during a full PTS
   sample.

      \begin{verbatim}
      output:
         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exception was
         encountered.
      \end{verbatim}
*/
int CtdPower(unsigned short* VoltCnt, unsigned short* AmpCnt, time_t timeout)
{
   /* define the logging signature */
   cc *FuncName="CtdPower()";
   
   /* initialize the return value */
   int status=CtdNull;

   /* local buffer to receive STP sample */
   char buf[32];

   /* validate the function arguments */
   if (!VoltCnt || !AmpCnt)
   {
      /* log the message */
      LogEntry(FuncName,"NULL function argument.\n");
   }

   else
   {
      /* assert that buffer-flushing services exist on ctdio and conio */
      assert(ctdio.iflush);

      /* disable Rx, set Tx low, inhibit CTD output (mode high) */
      CtdDisableRx(); CtdClearTxPin(); CtdAssertModePin();
   
      /* initiate the CTD wake sequence */   
      CtdAssertWakePin(); Wait(50); CtdClearWakePin();

      /* flush Rx buffer and wait until 200ms have elapsed since wake was initiated */
      CtdEnableRx(); Wait(50); ctdio.iflush(); Wait(500); CtdClearModePin();
      
      /* make the volt and current measurements 1 second into the sample-cycle */
      Wait(400); (*VoltCnt)=BatVoltsAdc(); (*AmpCnt)=CtdAmpsAdc();

      /* read the string from the serial port */
      status=pgets(&ctdio,buf,sizeof(buf),timeout,"\r\n");

      /* log the status */
      if (status<=0) {LogEntry(FuncName,"No response from CTD.\n");}
      
      else if (debuglevel>=2 || (debugbits&CTDIO_H))
      {
         /* log the message */
         LogEntry(FuncName,"[%s]\n",buf);
      }
      
      /* check if the power consumption is loggable */
      if (debuglevel>=2 || (debugbits&CTDIO_H))
      {
         /* convert counts to engineering quantities */
         float Amp=CtdAmps(*AmpCnt), Volt=BatVolts(*VoltCnt);
         
         /* log the power consumption */
         LogEntry(FuncName,"CTD Power consumption [%uVCnt %uACnt]: "
                  "%0.3fVolts * %0.3fAmps = %0.2fWatts.\n",
                  *VoltCnt,*AmpCnt,Volt,
                  Amp,Volt*Amp);
      }
   }
   
   return status;   
}

/*------------------------------------------------------------------------*/
/* function to initiate a P-only sample using hardware controls           */
/*------------------------------------------------------------------------*/
/**
   This function initiates a p-only sample by the CTD.  The
   mode-select line is used to inhibit output of the pressure stream
   for an SBE41CP.  The following timing information was taken from
   SeaBird's document: sbe41cp_UW_HardwareReferenceRevDec07.pdf which
   is in the documents directory of the distribution.

      \begin{verbatim}
      Data Request:
         * Mark state greater than 400 millisecond: Enter Command mode. See
           Command Summary for command syntax.

         * Mark state 1 to 100 millisecond:
              * If Pin 3 is in mark state, take PTS sample. PTS sample
                time: 3.25 seconds
              * If Pin 3 is in space state and pin 4 is in space state
                or open circuit, take fast pressure sample.  fast
                pressure sample time: 0.65 seconds

              * If pin 3 is in space state and pin 4 is in mark state,
                take fast PT sample.  fast PT sample time: 1.95
                seconds
      \end{verbatim}

   \begin{verbatim}
   output:
      This function returns a positive value on success and zero on
      failure.  A negative value indicates an exceptional error.
   \end{verbatim}
*/
int CtdPSample(char *buf, int size)
{
   /* initialize the return value */
   int status=CtdNull;
      
   /* initialize the communications timeout period */
   const time_t timeout=3;
   
   /* assert that buffer-flushing services exist on ctdio and conio */
   assert(ctdio.iflush);

   /* disable Rx, request p-only sample (Tx low), inhibit CTD output (mode high) */
   CtdDisableRx(); CtdAssertModePin(); Wait(100); CtdClearTxPin();
   
   /* initiate the CTD wake sequence */   
   CtdAssertWakePin(); Wait(50); CtdClearWakePin();

   /* flush Rx buffer and wait until 200ms have elapsed since wake was initiated */
   CtdEnableRx(); Wait(50); ctdio.iflush(); CtdClearModePin();
   
   /* read the string from the serial port */
   status=pgets(&ctdio,buf,size,timeout,"\r\n");

   /* disable Rx interrupts */
   CtdDisableRx();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initiate a low-power PT sample using hardware controls     */
/*------------------------------------------------------------------------*/
/**
   This function initiates a low-power PT sample by the CTD.  The
   following timing information was taken from SeaBird's document:
   sbe41cp_UW_HardwareReferenceRevDec07.pdf which is in the documents
   directory of the distribution.

      \begin{verbatim}
      Data Request:
         * Mark state greater than 400 millisecond: Enter Command mode. See
           Command Summary for command syntax.

         * Mark state 1 to 100 millisecond:
              * If Pin 3 is in mark state, take PTS sample. PTS sample
                time: 3.25 seconds
              * If Pin 3 is in space state and pin 4 is in space state
                or open circuit, take fast pressure sample.  fast
                pressure sample time: 0.65 seconds

              * If pin 3 is in space state and pin 4 is in mark state,
                take fast PT sample.  fast PT sample time: 1.95
                seconds
      \end{verbatim}


      \begin{verbatim}
      output:
         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exceptional error.
      \end{verbatim}
*/
int CtdPtSample(char *buf, int size)
{
   /* initialize the return value */
   int status=CtdNull;
      
   /* initialize the communications timeout period */
   const time_t timeout=5;
   
   /* assert that buffer-flushing services exist on ctdio and conio */
   assert(ctdio.iflush);

   /* disable Rx, request PT sample (Tx high), inhibit CTD output (mode high) */
   CtdDisableRx(); CtdAssertModePin(); Wait(100); CtdAssertTxPin();
   
   /* initiate the CTD wake sequence */   
   CtdAssertWakePin(); Wait(50); CtdClearWakePin();

   /* flush Rx buffer and wait until 200ms have elapsed since wake was initiated */
   CtdEnableRx(); Wait(50); ctdio.iflush(); CtdClearModePin();
   
   /* read the string from the serial port */
   status=pgets(&ctdio,buf,size,timeout,"\r\n");

   /* clear the Tx pin */
   CtdClearTxPin();
   
   /* disable Rx interrupts */
   CtdDisableRx();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initiate a full PTS sample using hardware controls         */
/*------------------------------------------------------------------------*/
/**
   This function initiates a full PTS sample by the CTD.  The
   following timing information was taken from SeaBird's document:
   sbe41cp_UW_HardwareReferenceRevDec07.pdf which is in the documents
   directory of the distribution.

      \begin{verbatim}
      Data Request:
         * Mark state greater than 400 millisecond: Enter Command mode. See
           Command Summary for command syntax.

         * Mark state 1 to 100 millisecond:
              * If Pin 3 is in mark state, take PTS sample. PTS sample
                time: 3.25 seconds
              * If Pin 3 is in space state and pin 4 is in space state
                or open circuit, take fast pressure sample.  fast
                pressure sample time: 0.65 seconds

              * If pin 3 is in space state and pin 4 is in mark state,
                take fast PT sample.  fast PT sample time: 1.95
                seconds
      \end{verbatim}


      \begin{verbatim}
      output:
         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exceptional error.
      \end{verbatim}
*/
int CtdPtsSample(char *buf, int size, time_t timeout)
{
   /* initialize the return value */
   int status=CtdNull;
   
   /* assert that buffer-flushing services exist on ctdio and conio */
   assert(ctdio.iflush);

   /* disable Rx, set Tx low, inhibit CTD output (mode high) */
   CtdDisableRx(); CtdAssertModePin(); Wait(100); CtdClearTxPin();
   
   /* initiate the CTD wake sequence */   
   CtdAssertWakePin(); Wait(50); CtdClearWakePin();

   /* flush Rx buffer and wait until 200ms have elapsed since wake was initiated */
   CtdEnableRx(); Wait(50); ctdio.iflush(); Wait(300); CtdClearModePin();
   
   /* read the string from the serial port */
   status=pgets(&ctdio,buf,size,timeout,"\r\n");

   /* disable Rx interrupts */
   CtdDisableRx();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a byte to the USART2 serial port                     */
/*------------------------------------------------------------------------*/
/**
   This function writes a byte to the Stm32's USART2 serial port.
   This function returns the number of bytes written to the serial
   port.  A negative return value indicates an exception was
   encountered.
*/ 
static int CtdPutb(unsigned char byte)
{
   /* define the logging signature */
   cc *FuncName="CtdPutb()";

   /* initialize the return value */
   int status = CtdFail;

   /* validate the configuration for USART2 */
   if (!huart2.Instance) {LogEntry(FuncName,"UART2 not configured or enabled.\n"); status=CtdNoConfig;}

   else
   {
      /* define the timeout period */
      const unsigned long int Timeout=50;
   
      /* initialize the reference time for the time-out feature */
      unsigned long int To=HAL_GetTick();

      /* construct time-out loop */
      while ((HAL_GetTick()-To)<Timeout)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart2.Instance->SR;
         
         /* check if the transmit register is empty */
         if (SR&USART_SR_TXE) {huart2.Instance->DR=byte; break;}
      }
      
      /* continue the time-out loop */
      for (status=CtdFail; (HAL_GetTick()-To)<Timeout;)
      {
         /* read the UART's status register */
         volatile unsigned long int SR=huart2.Instance->SR;

         /* check if the transmit-complete flag is set */
         if (SR&USART_SR_TC) {status=CtdOk; break;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to retrieve the static UART_HandleTypeDef pointer             */
/*------------------------------------------------------------------------*/
UART_HandleTypeDef *HAL_UART2_HandleGet(void) {return (&huart2);}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na(void)
{
   return CtdNull;
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na_(int state)
{
   return CtdNull;
}

#endif /* CTDIO_C */
