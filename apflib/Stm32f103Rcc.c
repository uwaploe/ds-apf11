#ifndef STM32F103RCC_H
#define STM32F103RCC_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103RccChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 RCC registers                                     */
/*------------------------------------------------------------------------*/
/**
   volatile unsigned long int CR;       // RCC clock control register  
   volatile unsigned long int CFGR;     // RCC clock configuration register
   volatile unsigned long int CIR;      // RCC clock interrupt register
   volatile unsigned long int APB2RSTR; // RCC APB2 peripheral reset register
   volatile unsigned long int APB1RSTR; // RCC APB1 peripheral reset register
   volatile unsigned long int AHBENR;   // RCC AHB peripheral clock enable register
   volatile unsigned long int APB2ENR;  // RCC APB2 peripheral clock enable register
   volatile unsigned long int APB1ENR;  // RCC APB1 peripheral clock enable register
   volatile unsigned long int BDCR;     // RCC backup control register
   volatile unsigned long int CSR;      // RCC control status register
*/
typedef RCC_TypeDef Stm32Rcc;

/* declare functions with external linkage */
unsigned long int Stm32AhbFreq(void);
unsigned long int Stm32Apb1Freq(void);
unsigned long int Stm32Apb2Freq(void);
unsigned long int Stm32HseFreq(void);
unsigned long int Stm32HsiFreq(void);
unsigned long int Stm32SysClkFreq(void);

/* define the return states of the Stm32 RCC API */
extern const char RccInvalid;   /* invalid argument */
extern const char RccNull;      /* NULL function argument */
extern const char RccFail;      /* general failure */
extern const char RccOk;        /* general success */

#endif /* STM32F103RCC_H */
#ifdef STM32F103RCC_C
#undef STM32F103RCC_C

#include <limits.h>
#include <logger.h>
#include <stm32f103xg.h>

/* define the return states of the Stm32 timer API */
const char RccInvalid     = -2; /* invalid argument */
const char RccNull        = -1; /* NULL function argument */
const char RccFail        =  0; /* general failure */
const char RccOk          =  1; /* general success */

/*------------------------------------------------------------------------*/
/* Determine the frequency of the Advanced High-Speed Bus (AHB)           */
/*------------------------------------------------------------------------*/
/**
   This function determines and returns the frequency (Hertz) of the
   Advanced High-Speed Bus (AHB).
*/
unsigned long int Stm32AhbFreq(void)
{
   /* initialize the return value */
   unsigned long int Hertz=Stm32SysClkFreq();

   /* create bit-masks for relevant bits of the RCC configuration register */
   enum {HPRE=(0xf<<4)};

   /* copy the contents of the RCC configuration register */
   const unsigned long int CFGR=RCC->CFGR;

   /* apply the AHB prescaler */
   switch ((CFGR&HPRE)>>4)
   {
      case 0x8: {Hertz /= 2;   break;}
      case 0x9: {Hertz /= 4;   break;}
      case 0xa: {Hertz /= 8;   break;}
      case 0xb: {Hertz /= 16;  break;}
      case 0xc: {Hertz /= 64;  break;}
      case 0xd: {Hertz /= 128; break;}
      case 0xe: {Hertz /= 256; break;}
      case 0xf: {Hertz /= 512; break;}
   }
   
   return Hertz;
}

/*------------------------------------------------------------------------*/
/* Determine the frequency of the Advanced Peripheral Bus (APB1)          */
/*------------------------------------------------------------------------*/
/**
   This function determines and returns the frequency (Hertz) of the
   Advanced Peripheral Bus (APB1).
*/
unsigned long int Stm32Apb1Freq(void)
{
   /* initialize the return value */
   unsigned long int Hertz=Stm32AhbFreq();

   /* create bit-masks for relevant bits of the RCC configuration register */
   enum {PPRE1=(0x7<<8)};

   /* copy the contents of the RCC configuration register */
   const unsigned long int CFGR=RCC->CFGR;

   /* apply the APB1 prescaler */
   switch ((CFGR&PPRE1)>>8)
   {
      case 0x4: {Hertz /= 2;   break;}
      case 0x5: {Hertz /= 4;   break;}
      case 0x6: {Hertz /= 8;   break;}
      case 0x7: {Hertz /= 16;  break;}
   }
   
   return Hertz;
}

/*------------------------------------------------------------------------*/
/* Determine the frequency of the Advanced Peripheral Bus (APB2)          */
/*------------------------------------------------------------------------*/
/**
   This function determines and returns the frequency (Hertz) of the
   Advanced Peripheral Bus (APB2).
*/
unsigned long int Stm32Apb2Freq(void)
{
   /* initialize the return value */
   unsigned long int Hertz=Stm32AhbFreq();

   /* create bit-masks for relevant bits of the RCC configuration register */
   enum {PPRE2=(0x7<<11)};

   /* copy the contents of the RCC configuration register */
   const unsigned long int CFGR=RCC->CFGR;

   /* apply the APB2 prescaler */
   switch ((CFGR&PPRE2)>>11)
   {
      case 0x4: {Hertz /= 2;   break;}
      case 0x5: {Hertz /= 4;   break;}
      case 0x6: {Hertz /= 8;   break;}
      case 0x7: {Hertz /= 16;  break;}
   }
   
   return Hertz;
}

/*------------------------------------------------------------------------*/
/* return the frequency (Hertz) of High Speed External (HSE) oscillator   */
/*------------------------------------------------------------------------*/
unsigned long int Stm32HseFreq(void) {return (8000000UL);}

/*------------------------------------------------------------------------*/
/* return the frequency (Hertz) of High Speed Internal (HSI) oscillator   */
/*------------------------------------------------------------------------*/
unsigned long int Stm32HsiFreq(void) {return (8000000UL);}

/*------------------------------------------------------------------------*/
/* return the frequency (Hertz) if the core system clock                  */
/*------------------------------------------------------------------------*/
/**
   This function returns the frequency (Hertz) of the core system
   clock which includes the chained effects of the HSE (or HSI) plus
   the PLL.
*/
unsigned long int Stm32SysClkFreq(void)
{
   /* initialize the return value */
   unsigned long int Hertz=0;

   /* create bit-masks for relevant bits of the RCC control & configuration registers */
   enum {HSEON=(0x1<<16), PLLON=(0x1<<24), PLLSRC=(0x1<<16), PLLXTPRE=(0x1<<17), PLLMUL=0xf<<18};

   /* copy the contents of the RCC control & configuration registers */
   const unsigned long int CR=RCC->CR, CFGR=RCC->CFGR;

   /* compute the system clock frequency if the PLL is disabled */
   if (!(CR&PLLON)) {Hertz = ((CR&HSEON) ? Stm32HseFreq() : Stm32HsiFreq());}
   
   else
   {
      /* extract the PLL multiplier from the RCC configuration register */
      unsigned char pllmul=((CFGR&PLLMUL)>>18);

      /* test if HSI is used as input to the PLL */
      if (!(CFGR&PLLSRC)) {Hertz=Stm32HsiFreq()/2;}

      /* test if HSE is halved prior to feeding into the PLL */
      else {Hertz = ((CFGR&PLLXTPRE) ? Stm32HseFreq()/2 :  Stm32HseFreq());}

      /* compute the system clock frequency */
      Hertz *= ((pllmul<16) ? (pllmul+1) : pllmul);
   }
   
   return Hertz;
}

#endif /* STM32F103RCC_C */
