#ifndef STM32F103GPIO_H
#define STM32F103GPIO_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Description of the SwiftWare STM32F103 GPIO model
 * -------------------------------------------------
 *
 * The HAL GPIO model implements four input modes and twenty-four
 * output modes.  So many output modes results in unnecessary
 * complexity of the HAL GPIO API and is ridiculous overkill for any
 * functionality envisioned for the Apf11 controller. I have abandoned
 * the HAL because its attempt to abstract all peripherals across all
 * Stm32 product lines has resulted in an API that is overly
 * complicated and full of nasty bugs and infinite loops (both
 * explicit and implicit).  In practice, I found it impossible to
 * develop reliable firmware using the HAL as a foundation.
 *
 * In contrast to the HAL GPIO model, the SwiftWare GPIO model
 * implements four input modes and only 6 output modes and its
 * implementation is simpler, more functional, has smaller footprint,
 * and almost certainly fewer bugs than HAL.  The SwiftWare GPIO model
 * implements a hard-coded configuration at boot-up that induces the
 * Apf11 into a known, valid, and tested working state.  The
 * configuration is based on TWR's Apf11 schematic 304770, Revision A.
 * After boot-up, the GPIO ports can be reconfigured into any
 * configuration desired, although some configurations will result in
 * a nonfunctional Apf11.
 *
 * GPIO modes can be segregated into three broad categories: input,
 * general-purpose (GP) output, and alternate-function (AF) output.
 * According to the SwiftWare GPIO model, only pins configured for GP
 * output are writable to allow changes to individual pin states (ie.,
 * clear or assert); pins that are configured for input or AF output
 * are not subject to software control.  On the other hand, pins that
 * are configured for digital IO allow pins' states to be read
 * regardless of configuration (pins configured for analog input are
 * an exception).  Pins that are configured for AF output are strictly
 * under the control of the peripheral to which the pin is assigned.
 * Pins that are configured for analog input have their
 * Schmidt-trigger disabled and should be used only as inputs to the
 * analog-to-digital (AD) peripheral, not as digital input pins.
 *
 *   Here is a terse description of each of the ten valid IO modes of
 *   the SwiftWare GPIO model:
 *
 *      InAnlg: This mode is intended for analog input to the
 *         analog-to-digital (AD) peripheral; the schmidt trigger is
 *         disabled.  This mode can not be used for digital input.
 *
 *      InFlt: This mode is for digital input from a driven source; its
 *         input is floating.
 *
 *      InPD: This mode is for digital input and incorporates a weak
 *         pull-down.
 *
 *      InPU: This mode is for digital input and incorporates a weak
 *         pull-up. 
 *
 *      AOutPP: This mode is for digital alternate-function output
 *         with a push-pull.  These speed is configured for 50MHz and
 *         represents the rate at which the signal can slew from high
 *         to low or vice versa.
 *
 *      AOutOD: This mode is for digital alternate-function output
 *         with a open-drain interface.  These speed is configured for
 *         50MHz and represents the rate at which the signal can slew
 *         from high to low or vice versa.
 *
 *      OutPPL: This mode is for digital general-purpose output with a
 *         push-pull interface.  It is initialized in the low state.
 *         These speed is configured for 10MHz and represents the rate
 *         at which the signal can slew from high to low or vice
 *         versa.
 *
 *      OutPPH: This mode is for digital general-purpose output with a
 *         push-pull interface.  It is initialized in the high
 *         state. It is initialized in the high state.  These speed is
 *         configured for 10MHz and represents the rate at which the
 *         signal can slew from high to low or vice versa.
 *
 *      OutODL: This mode is for digital general-purpose output with a
 *         open-drain interface.  It is initialized in the low state.
 *         These speed is configured for 10MHz and represents the rate
 *         at which the signal can slew from high to low or vice
 *         versa.
 *
 *      OutODH: This mode is for digital general-purpose output with a
 *         open-drain interface.  It is initialized in the highThese
 *         speed is configured for 10MHz and represents the rate at
 *         which the signal can slew from high to low or vice versa.
 *         state.
 *
 *   Only the last four modes above allow API control over the output
 *   state (ie., clear or assert) of the respective pin.  The
 *   alternate-function outputs are entirely under the control of
 *   their respective peripherals (eg., UARTS, FSMC, etc.).
 *
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103GpioChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 GPIO registers                                    */
/*------------------------------------------------------------------------*/
/**
   The GpioPort is defined by the following 32-bit registers in the Stm32:
      volatile unsigned long int CRL;    control register, low.
      volatile unsigned long int CRH;    control register, high.
      volatile unsigned long int IDR;    input data register.
      volatile unsigned long int ODR;    output data register.
      volatile unsigned long int BSRR;   set/reset register.
      volatile unsigned long int BRR;    reset register.
      volatile unsigned long int LCKR;   lock register.
*/
typedef GPIO_TypeDef GpioPort;

/* enumerate the GPIO modes */
typedef enum {InAnlg = 0x00U, OutPPL = 0x01U, AOutPP = 0x0bU,
              InFlt  = 0x04U, OutODL = 0x05U, AOutOD = 0x0fU,
              InPD   = 0x08U, OutPPH = 0x11U,
              InPU   = 0x18U, OutODH = 0x17U} GpioMode;

/* prototypes for functions with external linkage */
int Stm32GpioAssert(GpioPort *port, unsigned int PinsToAssert);
int Stm32GpioClear(GpioPort *port, unsigned int PinsToClear);
int Stm32GpioConfig(GpioPort *port, unsigned int pins, GpioMode mode);
int Stm32GpioInit(void);
int Stm32GpioIsAssert(GpioPort *port, unsigned int pins);
int Stm32GpioIsClear(GpioPort *port, unsigned int pins);
int Stm32GpioIsInPin(GpioPort *port, unsigned int pins);
int Stm32GpioIsOutPin(GpioPort *port, unsigned int pins);
int Stm32GpioIsValidPort(GpioPort *port);
int Stm32GpioModify(GpioPort *port, unsigned int PinsToClear, unsigned int PinsToAssert);
int Stm32GpioRead(GpioPort *port, unsigned int *idr);

/* define the return states of the Stm32 GPIO API */
extern const char GpioInvalid;            /* invalid parameter */
extern const char GpioNull;               /* NULL function argument */
extern const char GpioFail;               /* general failure */
extern const char GpioOk;                 /* general success */

/* define masks for pin identifiers */
#ifndef __STM32F1xx_HAL_GPIO_H
#define GPIO_PIN_0  (0x0001U)
#define GPIO_PIN_1  (0x0002U)
#define GPIO_PIN_2  (0x0004U)
#define GPIO_PIN_3  (0x0008U)
#define GPIO_PIN_4  (0x0010U)
#define GPIO_PIN_5  (0x0020U)
#define GPIO_PIN_6  (0x0040U)
#define GPIO_PIN_7  (0x0080U)
#define GPIO_PIN_8  (0x0100U)
#define GPIO_PIN_9  (0x0200U)
#define GPIO_PIN_10 (0x0400U)
#define GPIO_PIN_11 (0x0800U)
#define GPIO_PIN_12 (0x1000U)
#define GPIO_PIN_13 (0x2000U)
#define GPIO_PIN_14 (0x4000U)
#define GPIO_PIN_15 (0x8000U)
#endif /* __STM32F1xx_HAL_GPIO_H */

/* Stm32f103 Port A pin assignments */
#define WAKE_Pin                      GPIO_PIN_0
#define EXP_DUART4_SPI_CS_Pin         GPIO_PIN_1
#define UART2_TXD_Pin                 GPIO_PIN_2
#define UART2_RXD_Pin                 GPIO_PIN_3
#define CTD_PWR_CTL_Pin               GPIO_PIN_4
#define SPI1_SCK_Pin                  GPIO_PIN_5
#define SPI1_MISO_Pin                 GPIO_PIN_6
#define SPI1_MOSI_Pin                 GPIO_PIN_7
#define COULOMB_OWD_Pin               GPIO_PIN_8
#define UART1_TXD_Pin                 GPIO_PIN_9
#define UART1_RXD_Pin                 GPIO_PIN_10
#define USB_DM_Pin                    GPIO_PIN_11
#define USB_DP_Pin                    GPIO_PIN_12
#define TMS_Pin                       GPIO_PIN_13
#define TCK_Pin                       GPIO_PIN_14
#define TDI_Pin                       GPIO_PIN_15

/* Stm32f103 Port B pin assignments */
#define ADC12_IN8_Pin                 GPIO_PIN_0
#define ADC12_IN9_Pin                 GPIO_PIN_1
#define SD_PWR_EN_Pin                 GPIO_PIN_2
#define TDO_Pin                       GPIO_PIN_3
#define TRST_Pin                      GPIO_PIN_4
#define SPARE_GPIO_SPI_CS_Pin         GPIO_PIN_5
#define SPARE_DUART_SPI_CS_Pin        GPIO_PIN_6
#define RF_DUART_SPI_CS_Pin           GPIO_PIN_7
#define RF_GPIO_SPI_CS_Pin            GPIO_PIN_8
#define RF_3P3V_PWR_EN_Pin            GPIO_PIN_9
#define UART3_TXD_Pin                 GPIO_PIN_10
#define UART3_RXD_Pin                 GPIO_PIN_11
#define RF_VBAT_PWR_EN_Pin            GPIO_PIN_12
#define SPI2_SCK_Pin                  GPIO_PIN_13
#define SPI2_MISO_Pin                 GPIO_PIN_14
#define SPI2_MOSI_Pin                 GPIO_PIN_15

/* Stm32f103 Port C pin assignments */
#define ADC123_IN10_Pin               GPIO_PIN_0
#define ADC123_IN11_Pin               GPIO_PIN_1
#define ADC123_IN12_Pin               GPIO_PIN_2
#define ADC123_IN13_Pin               GPIO_PIN_3
#define ADC12_IN14_Pin                GPIO_PIN_4
#define ADC12_IN15_Pin                GPIO_PIN_5
#define PPS_Pin                       GPIO_PIN_6
#define CTD_PTS_FP_Pin                GPIO_PIN_7
#define SDIO_D0_Pin                   GPIO_PIN_8
#define SDIO_D1_Pin                   GPIO_PIN_9
#define SDIO_D2_Pin                   GPIO_PIN_10
#define SDIO_D3_Pin                   GPIO_PIN_11
#define SDIO_CLK_Pin                  GPIO_PIN_12
#define TAMPER_Pin                    GPIO_PIN_13
#define RTC_32KHZ_Pin                 GPIO_PIN_14
#define RTC_NC_Pin                    GPIO_PIN_15

/* Stm32f103 Port D pin assignments */
#define D2_Pin                        GPIO_PIN_0
#define D3_Pin                        GPIO_PIN_1
#define SDIO_CMD_Pin                  GPIO_PIN_2
#define CL_PWR_EN_Pin                 GPIO_PIN_3
#define OE_N_Pin                      GPIO_PIN_4
#define WE_N_Pin                      GPIO_PIN_5
#define EEPROM_CS_Pin                 GPIO_PIN_6
#define CS_N_Pin                      GPIO_PIN_7
#define D13_Pin                       GPIO_PIN_8
#define D14_Pin                       GPIO_PIN_9
#define D15_Pin                       GPIO_PIN_10
#define A16_Pin                       GPIO_PIN_11
#define A17_Pin                       GPIO_PIN_12
#define A18_Pin                       GPIO_PIN_13
#define D0_Pin                        GPIO_PIN_14
#define D1_Pin                        GPIO_PIN_15

/* Stm32f103 Port E pin assignments */
#define BE_LO_N_Pin                   GPIO_PIN_0
#define BE_HI_N_Pin                   GPIO_PIN_1
#define EXP_GPIO_SPI_CS_Pin           GPIO_PIN_2
#define A19_Pin                       GPIO_PIN_3
#define EXP_DUART3_SPI_CS_Pin         GPIO_PIN_4
#define EXP_DUART2_SPI_CS_Pin         GPIO_PIN_5
#define EXP_DUART1_SPI_CS_Pin         GPIO_PIN_6
#define D4_Pin                        GPIO_PIN_7
#define D5_Pin                        GPIO_PIN_8
#define D6_Pin                        GPIO_PIN_9
#define D7_Pin                        GPIO_PIN_10
#define D8_Pin                        GPIO_PIN_11
#define D9_Pin                        GPIO_PIN_12
#define D10_Pin                       GPIO_PIN_13
#define D11_Pin                       GPIO_PIN_14
#define D12_Pin                       GPIO_PIN_15

/* Stm32f103 Port F pin assignments */
#define A0_Pin                        GPIO_PIN_0
#define A1_Pin                        GPIO_PIN_1
#define A2_Pin                        GPIO_PIN_2
#define A3_Pin                        GPIO_PIN_3
#define A4_Pin                        GPIO_PIN_4
#define A5_Pin                        GPIO_PIN_5
#define ADC3_IN4_Pin                  GPIO_PIN_6
#define ADC3_IN5_Pin                  GPIO_PIN_7
#define ADC3_IN6_Pin                  GPIO_PIN_8
#define ADC3_IN7_Pin                  GPIO_PIN_9
#define ADC3_IN8_Pin                  GPIO_PIN_10
#define ACTIVE_ENABLE_3P3V_Pin        GPIO_PIN_11
#define A6_Pin                        GPIO_PIN_12
#define A7_Pin                        GPIO_PIN_13
#define A8_Pin                        GPIO_PIN_14
#define A9_Pin                        GPIO_PIN_15

/* Stm32f103 Port G pin assignments */
#define A10_Pin                       GPIO_PIN_0
#define A11_Pin                       GPIO_PIN_1
#define A12_Pin                       GPIO_PIN_2
#define A13_Pin                       GPIO_PIN_3
#define A14_Pin                       GPIO_PIN_4
#define A15_Pin                       GPIO_PIN_5
#define SPARE_SENSOR_PWR_EN_Pin       GPIO_PIN_6
#define SD_DETECT_Pin                 GPIO_PIN_7
#define EXP_3P3V_PWR_EN_Pin           GPIO_PIN_8
#define EXP_VBAT_PWR_EN_Pin           GPIO_PIN_9
#define SPARE_DUART_IRQ_N_Pin         GPIO_PIN_10
#define RF_DUART_IRQ_N_Pin            GPIO_PIN_11
#define EXP_DUART1_IRQ_N_Pin          GPIO_PIN_12
#define EXP_DUART2_IRQ_N_Pin          GPIO_PIN_13
#define EXP_DUART3_IRQ_N_Pin          GPIO_PIN_14
#define EXP_DUART4_IRQ_N_Pin          GPIO_PIN_15

/* Stm32f103 Port A funcions */
#define WAKE_GPIO_Port                GPIOA
#define EXP_DUART4_SPI_CS_GPIO_Port   GPIOA
#define UART2_TXD_GPIO_Port           GPIOA
#define UART2_RXD_GPIO_Port           GPIOA
#define CTD_PWR_CTL_GPIO_Port         GPIOA
#define SPI1_SCK_GPIO_Port            GPIOA
#define SPI1_MISO_GPIO_Port           GPIOA
#define SPI1_MOSI_GPIO_Port           GPIOA
#define COULOMB_OWD_GPIO_Port         GPIOA
#define UART1_TXD_GPIO_Port           GPIOA
#define UART1_RXD_GPIO_Port           GPIOA
#define USB_DM_GPIO_Port              GPIOA
#define USB_DP_GPIO_Port              GPIOA
#define TMS_GPIO_Port                 GPIOA
#define TCK_GPIO_Port                 GPIOA
#define TDI_GPIO_Port                 GPIOA

/* Stm32f103 Port B funcions */
#define ADC12_IN8_GPIO_Port           GPIOB
#define ADC12_IN9_GPIO_Port           GPIOB
#define SD_PWR_EN_GPIO_Port           GPIOB
#define TDO_GPIO_Port                 GPIOB
#define TRST_GPIO_Port                GPIOB
#define SPARE_GPIO_SPI_CS_GPIO_Port   GPIOB
#define SPARE_DUART_SPI_CS_GPIO_Port  GPIOB
#define RF_DUART_SPI_CS_GPIO_Port     GPIOB
#define RF_GPIO_SPI_CS_GPIO_Port      GPIOB
#define RF_3P3V_PWR_EN_GPIO_Port      GPIOB
#define UART3_TXD_GPIO_Port           GPIOB
#define UART3_RXD_GPIO_Port           GPIOB
#define RF_VBAT_PWR_EN_GPIO_Port      GPIOB
#define SPI2_SCK_GPIO_Port            GPIOB
#define SPI2_MISO_GPIO_Port           GPIOB
#define SPI2_MOSI_GPIO_Port           GPIOB

/* Stm32f103 Port C funcions */
#define ADC123_IN10_GPIO_Port         GPIOC
#define ADC123_IN11_GPIO_Port         GPIOC
#define ADC123_IN12_GPIO_Port         GPIOC
#define ADC123_IN13_GPIO_Port         GPIOC
#define ADC12_IN14_GPIO_Port          GPIOC
#define ADC12_IN15_GPIO_Port          GPIOC
#define PPS_GPIO_Port                 GPIOC
#define CTD_PTS_FP_GPIO_Port          GPIOC
#define SDIO_D0_GPIO_Port             GPIOC
#define SDIO_D1_GPIO_Port             GPIOC
#define SDIO_D2_GPIO_Port             GPIOC
#define SDIO_D3_GPIO_Port             GPIOC
#define SDIO_CLK_GPIO_Port            GPIOC
#define TAMPER_GPIO_Port              GPIOC
#define RTC_32KHZ_GPIO_Port           GPIOC
#define RTC_NC_GPIO_Port              GPIOC

/* Stm32f103 Port D funcions */
#define D2_GPIO_Port                  GPIOD
#define D3_GPIO_Port                  GPIOD
#define SDIO_CMD_GPIO_Port            GPIOD
#define CL_PWR_EN_GPIO_Port           GPIOD
#define OE_N_GPIO_Port                GPIOD
#define WE_N_GPIO_Port                GPIOD
#define EEPROM_CS_GPIO_Port           GPIOD
#define CS_N_GPIO_Port                GPIOD
#define D13_GPIO_Port                 GPIOD
#define D14_GPIO_Port                 GPIOD
#define D15_GPIO_Port                 GPIOD
#define A16_GPIO_Port                 GPIOD
#define A17_GPIO_Port                 GPIOD
#define A18_GPIO_Port                 GPIOD
#define D0_GPIO_Port                  GPIOD
#define D1_GPIO_Port                  GPIOD

/* Stm32f103 Port E funcions */
#define BE_LO_N_GPIO_Port             GPIOE
#define BE_HI_N_GPIO_Port             GPIOE
#define EXP_GPIO_SPI_CS_GPIO_Port     GPIOE
#define A19_GPIO_Port                 GPIOE
#define EXP_DUART3_SPI_CS_GPIO_Port   GPIOE
#define EXP_DUART2_SPI_CS_GPIO_Port   GPIOE
#define EXP_DUART1_SPI_CS_GPIO_Port   GPIOE
#define D4_GPIO_Port                  GPIOE
#define D5_GPIO_Port                  GPIOE
#define D6_GPIO_Port                  GPIOE
#define D7_GPIO_Port                  GPIOE
#define D8_GPIO_Port                  GPIOE
#define D9_GPIO_Port                  GPIOE
#define D10_GPIO_Port                 GPIOE
#define D11_GPIO_Port                 GPIOE
#define D12_GPIO_Port                 GPIOE

/* Stm32f103 Port F funcions */
#define A0_GPIO_Port                  GPIOF
#define A1_GPIO_Port                  GPIOF
#define A2_GPIO_Port                  GPIOF
#define A3_GPIO_Port                  GPIOF
#define A4_GPIO_Port                  GPIOF
#define A5_GPIO_Port                  GPIOF
#define ADC3_IN4_GPIO_Port            GPIOF
#define ADC3_IN5_GPIO_Port            GPIOF
#define ADC3_IN6_GPIO_Port            GPIOF
#define ADC3_IN7_GPIO_Port            GPIOF
#define ADC3_IN8_GPIO_Port            GPIOF
#define ACTIVE_ENABLE_3P3V_GPIO_Port  GPIOF
#define A6_GPIO_Port                  GPIOF
#define A7_GPIO_Port                  GPIOF
#define A8_GPIO_Port                  GPIOF
#define A9_GPIO_Port                  GPIOF

/* Stm32f103 Port G funcions */
#define A10_GPIO_Port                 GPIOG
#define A11_GPIO_Port                 GPIOG
#define A12_GPIO_Port                 GPIOG
#define A13_GPIO_Port                 GPIOG
#define A14_GPIO_Port                 GPIOG
#define A15_GPIO_Port                 GPIOG
#define SPARE_SENSOR_PWR_EN_GPIO_Port GPIOG
#define SD_DETECT_GPIO_Port           GPIOG
#define EXP_3P3V_PWR_EN_GPIO_Port     GPIOG
#define EXP_VBAT_PWR_EN_GPIO_Port     GPIOG
#define SPARE_DUART_IRQ_N_GPIO_Port   GPIOG
#define RF_DUART_IRQ_N_GPIO_Port      GPIOG
#define EXP_DUART1_IRQ_N_GPIO_Port    GPIOG
#define EXP_DUART2_IRQ_N_GPIO_Port    GPIOG
#define EXP_DUART3_IRQ_N_GPIO_Port    GPIOG
#define EXP_DUART4_IRQ_N_GPIO_Port    GPIOG

#endif /* STM32F103GPIO_H */
#ifdef STM32F103GPIO_C
#undef STM32F103GPIO_C

#include <logger.h>
#include <stm32f103xg.h>
#include <stm32f1xx_hal_rcc.h>

/* define the return states of the Stm32 GPIO API */
const char GpioInvalid           =  -2; /* invalid parameter */
const char GpioNull              =  -1; /* NULL function argument */
const char GpioFail              =   0; /* general failure */
const char GpioOk                =   1; /* general success */

/*========================================================================*/
/* union to implement interface for each of 16 pins in an Stm32 GPIO port */
/*========================================================================*/
/**
   The union that defines a GpioInit object overlays two different
   representations of the GPIO peripheral's CRL, CRH, and ODR
   registers.  This representation obviously renders initializing the
   a GPIO port's CRL,CRH,ODR registers to be a simple task.  The
   alternative representation defines a nibble to define the
   configuration for each of the 16 pins in each GPIO port plus one
   bit per pin to specify the GPIO state via the ODR register.
   Implementation of this union requires that sizeof(unsigned int)==2
   and sizeof(unsigned long int)==4.

   The definitions of the configuration bits are given in the tables
   below which were taken from the Stm32f103 Reference Manual
   (Document 13902 Rev 16).
   
                           Table 20. Port bit configuration table
   +--------------------+-----------------+------+------+-------+-------+----------+
   | Configuration mode                   | Cnf1 | Cnf2 | Mode1 | Mode2 |  PxODR   | 
   +--------------------+-----------------+------+------+-------+-------+----------+
   | General purpose    | Push-pull       |   0  |   0  |      01       |  0 or 1  |
   | output             | Open-drain      |   0  |   1  |      10       |  0 or 1  |
   +--------------------+-----------------+------+------+      11       +----------+
   | Alternate function | Push-pull       |   1  |   0  | see Table 21  | not used |
   | output             | Open-drain      |   1  |   1  |               | not used |
   +--------------------+-----------------+------+------+---------------+----------+
   |                    | Analog          |   0  |   0  |               | not used |
   | Input              | Input floating  |   0  |   1  |      00       | not used |
   |                    | Input pull-down |   1  |   0  |               |     0    |
   |                    | Input pull-up   |   1  |   0  |               |     1    |
   +--------------------+-----------------+------+------+---------------+----------+
   | Invalid mode                         |   1  |   1  |      00       |  0 or 1  |
   +--------------------------------------+------+------+---------------+----------+  
    
                               Table 21. Output MODE bits
                       +-----------+----------------------------+
                       | Mode[1:0] | Meaning                    |
                       +-----------+----------------------------+
                       |    00     | Reserved                   |
                       |    01     | Maximum output speed 10MHz |
                       |    10     | Maximum output speed 2MHz  |
                       |    11     | Maximum output speed 50MHz |
                       +-----------+----------------------------+

   The tables above allow for a total of four input modes and
   twenty-four output modes.  As mentioned above, the HAL is overly
   complicated due to the large number of modes and especially its
   attempt to abstract all peripherals across the full product line of
   Stm32 processors.  The SwiftWare GPIO model reduces these
   twenty-eight modes to only ten as described above in the
   description of the SwiftWare GPIO model.
*/
typedef union
{
   /* define a structure to implement a configuration interface for each of 16 pins in an Stm32 GPIO port */
   struct
   {
      /* create a nibble to manipulate the CRL,CRH mode for each of 16 pins in an Stm32 GPIO port */         
      unsigned char pin0:4, pin1:4,  pin2:4,  pin3:4,  pin4:4,  pin5:4,  pin6:4,  pin7:4,
                    pin8:4, pin9:4, pin10:4, pin11:4, pin12:4, pin13:4, pin14:4, pin15:4;

      /* define one bit to manuipulate the ODR bit for each of 16 pins in an Stm32 GPIO port */   
      unsigned char state0:1, state1:1,  state2:1,  state3:1,  state4:1,  state5:1,  state6:1,   state7:1,
                    state8:1, state9:1, state10:1, state11:1, state12:1, state13:1, state14:1, state15f:1;
   };

   /* overlay the 32-bit CRL,CRH words plus 16-bit word over the configuration interface */   
   struct {unsigned long int CRL,CRH; unsigned int ODR;};
} GpioInit;

/* declare functions with static linkage */
static int Stm32GpioPortInit(GpioPort *port, const GpioInit *init);

/*========================================================================*/
/* GPIO configuration for Stm32 port A on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   A  SignalName        BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 WAKE              In,float               Wake  Wake interrupt for reed switch or CL.
   01 EXP_DUART4_SPI_CS Out,push-pull,high     GPIO  SPI1 CS for DUART4 (high=disable).
   02 UART2_TXD         AltOut,push-pull      UART2  TTL serial interface to CTD, Tx pin.
   03 UART2_RXD         In,float              UART2  TTL serial interface to CTD, Rx pin.
   04 CTD_PWR_CTL       Out,push-pull,low      GPIO  TTL serial interface to CTD, wake pin.
   05 SPI1_SCK          AltOut,push-pull       SPI1  SPI clock signal.
   06 SPI1_MISO         In,float               SPI1  SPI master input, slave output.
   07 SPI1_MOSI         AltOut,push-pull       SPI1  SPI master output, slave input. 
   08 COULOMB_OWD       Out,push-pull,low      GPIO  Coulomb counter 1-wire IO.
   09 UART1_TXD         In,float              UART1  TTL serial interface, Tx pin.
   10 UART1_RXD         In,float              UART1  TTL serial interface, Rx pin.
   11 USB_DM            In,float               GPIO  USB interface (unused).
   12 USB_DP            In,float               GPIO  USB interface (unused).
   13 TMS               In,pull-up             JTAG  TMS signal.
   14 TCK               In,pull-down           JTAG  TCK signal.
   15 TDI               In,pull-up             JTAG  TDI signal.

   GPIO registers: A.CRH=0x88844441 A.CRL=0xb4b14b14 A.ODR=0xa00a
*/
static const GpioInit A={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
        InFlt,  OutPPH,  AOutPP,    InPU,  OutPPL,  AOutPP,   InFlt,  AOutPP,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
        InFlt,   InFlt,   InFlt,   InFlt,   InFlt,    InPU,    InPD,    InPU,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x1,     0x0,     0x1,     0x0,     0x0,     0x0,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x1,     0x0,     0x1 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port B on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   B  SignalName           BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 ADC12_IN8/RF_ISENSE  In,analog          ADC1,Ch8  RF PCB current.  
   01 ADC12_IN9/EXP_ISENSE In,analog          ADC1,Ch9  Expansion PCB current.       
   02 SD_PWR_EN            Out,push-pull,low      GPIO  SD interface power enable.
   03 TDO                  In,pull-up             JTAG  JTAG TDO signal.
   04 TRST                 In,pull-up             JTAG  JTAG RTST signal.
   05 SPARE_GPIO_SPI_CS    Out,push-pull,high     SPI2  CS for spare GPIO (high=disable).
   06 SPARE_DUART_SPI_CS   Out,push-pull,high     SPI1  CS for spare DUART (high=disable).
   07 RF_DUART_SPI_CS      Out,push-pull,high     SPI1  CS for RF DUART (high=disable).
   08 RF_GPIO_SPI_CS       Out,push-pull,high     SPI2  CS for RF GPIO (high=disable).
   09 RF_3P3V_PWR_EN       Out,push-pull,low      GPIO  Enable 3V power to RF PCB.
   10 UART3_TXD            AltOut,push-pull      UART3  20mA current loop coms port.
   11 UART3_RXD            In,float              UART3  20mA current loop coms port.
   12 RF_VBAT_PWR_EN       Out,push-pull,low      GPIO  Enable battery power to RF PCB.
   13 SPI2_SCK             AltOut,push-pull       SPI2  SPI clock signal.               
   14 SPI2_MISO            In,float               SPI2  SPI master input, slave output. 
   15 SPI2_MOSI            AltOut,push-pull       SPI2  SPI master output, slave input. 

   GPIO registers: B.CRH=0xb4b14b11 B.CRL=0x11188100 B.ODR=0x01f8
*/
static const GpioInit B={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       InAnlg,  InAnlg,  OutPPL,    InPU,    InPU,  OutPPH,  OutPPH,  OutPPH,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
       OutPPH,  OutPPL,  AOutPP,   InFlt,  OutPPL,  AOutPP,   InFlt,  AOutPP,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x0,     0x0,     0x1,     0x1,     0x1,     0x1,     0x1,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x1,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port C on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   C  SignalName               BootUpConfig  Peripheral  Description
   ---------------------------------------------------------------------------------
   00 ADC123_IN10/AIR_PUMP_ISENSE In,analog   ADC1,Ch10  Air pump current.
   01 ADC123_IN11/PUMP_POS        In,analog   ADC1,Ch11  Buoyancy pump position.
   02 ADC123_IN12/PUMP_CURRENT    In,analog   ADC1,Ch12  Buoyancy pump current.
   03 ADC123_IN13/CTD_ISENSE      In,analog   ADC1,Ch13  CTD current.
   04 ADC12_IN14/SPARE1_ISENSE    In,analog   ADC1,Ch14  Spare sensor #1 current.
   05 ADC12_IN15/SPARE2_ISENSE    In,analog   ADC1,Ch15  Spare sensor #2 current.
   06 PPS                          In,float        GPIO  GPS pulse per second. 
   07 CTD_PTS_FP          Out,push-pull,low        GPIO  TTL serial interface to CTD, mode pin.
   08 SDIO_D0                      In,float      SDCARD  SD card.
   09 SDIO_D1                      In,float      SDCARD  SD card.
   10 SDIO_D2                      In,float      SDCARD  SD card.
   11 SDIO_D3                      In,float      SDCARD  SD card.
   12 SDIO_CLK                     In,float      SDCARD  SD card.
   13 TAMPER                       In,float      TAMPER  SD card.
   14 RTC_32KHZ                    In,float         RTC  Real time clock input.
   15 RTC_NC                       In,float         RTC  N/C.

   GPIO registers: C.CRH=0x44844444 C.CRL=0x14000000 C.ODR=0x2000
*/
static const GpioInit C={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       InAnlg,  InAnlg,  InAnlg,  InAnlg,  InAnlg,  InAnlg,   InFlt,  OutPPL,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
        InFlt,   InFlt,   InFlt,   InFlt,   InFlt,    InPU,   InFlt,   InFlt,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x1,     0x0,     0x0 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port D on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**
   D  SignalName           BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 D2_Pin               AltOut,push-pull       FSMC  FSMC data pin.    
   01 D3_Pin               AltOut,push-pull       FSMC  FSMC data pin.   
   02 SDIO_CMD_Pin         In,float               SDCARD  SD card.
   03 CL_PWR_EN_Pin        Out,push-pull,low      GPIO  Enable 20ma current loop.
   04 OE_N_Pin             Out,push-pull,high     FSMC  FSMC output enable pin.
   05 WE_N_Pin             Out,push-pull,high     FSMC  FSMC write enable pin.
   06 EEPROM_CS_Pin        Out,push-pull,high     SPI2  CS for EEPROM (high=disable).
   07 CS_N_Pin             In,float               FSMC  FSMC chip select.  
   08 D13_Pin              AltOut,push-pull       FSMC  FSMC data pin.   
   09 D14_Pin              AltOut,push-pull       FSMC  FSMC data pin.   
   10 D15_Pin              AltOut,push-pull       FSMC  FSMC data pin.   
   11 A16_Pin              AltOut,push-pull       FSMC  FSMC address pin.   
   12 A17_Pin              AltOut,push-pull       FSMC  FSMC address pin. 
   13 A18_Pin              AltOut,push-pull       FSMC  FSMC address pin. 
   14 D0_Pin               AltOut,push-pull       FSMC  FSMC data pin.
   15 D1_Pin               AltOut,push-pull       FSMC  FSMC data pin.

   Note: In order to prevent corruption of the persistent SRAM, it is
      essential that the chip-select pin (CS_N_Pin) be configured as a
      floating input so that the pull-up resistor (R367) induces the
      inverted input of the CY62157ESL's \CE1 pin to be high.  The
      inverted pull-up causes the chip-select (\CE1) to be low which
      deselects the SRAM to protect if from corruption during
      initialization.  In addition, the \OE and \WE pins are
      configured to be push-pull outputs (state: high) in order to
      induce the SRAM into standby mode.  While the IC is in standby,
      the address and data pins are placed in a high impedance state.

   GPIO registers: D.CRH=0xbbbbbbbb D.CRL=0x411114bb D.ODR=0x00e0
*/
static const GpioInit D={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       AOutPP,  AOutPP,   InFlt,  OutPPL,  OutPPH,  OutPPH,  OutPPH,   InFlt,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
       AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x0,     0x0,     0x0,     0x1,     0x1,     0x1,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port E on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   E  SignalName           BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 BE_LO_N              Out,push-pull,high     FSMC  FSMC byte enable low.
   01 BE_HI_N              Out,push-pull,high     FSMC  FSMC byte enable high.
   02 EXP_GPIO_SPI_CS      Out,push-pull,high     SPI2  CS for expansion GPIO (high=disable).
   03 A19                  AltOut,push-pull       FSMC  FSMC address pin. 
   04 EXP_DUART3_SPI_CS    Out,push-pull,high     SPI1  CS for DUART3 (high=disable). 
   05 EXP_DUART2_SPI_CS    Out,push-pull,high     SPI1  CS for DUART2 (high=disable).
   06 EXP_DUART1_SPI_CS    Out,push-pull,high     SPI1  CS for DUART1 (high=disable).
   07 D4                   AltOut,push-pull       FSMC  FSMC data pin. 
   08 D5                   AltOut,push-pull       FSMC  FSMC data pin. 
   09 D6                   AltOut,push-pull       FSMC  FSMC data pin. 
   10 D7                   AltOut,push-pull       FSMC  FSMC data pin. 
   11 D8                   AltOut,push-pull       FSMC  FSMC data pin. 
   12 D9                   AltOut,push-pull       FSMC  FSMC data pin. 
   13 D10                  AltOut,push-pull       FSMC  FSMC data pin. 
   14 D11                  AltOut,push-pull       FSMC  FSMC data pin. 
   15 D12                  AltOut,push-pull       FSMC  FSMC data pin.

   Note: The \UB and \LB pins (ie., BE_HI_N and BE_LO_N) are
      configured to be push-pull outputs (state: high) in order to
      induce the SRAM into standby mode.  While the IC is in standby,
      the address and data pins are placed in a high impedance state.

   GPIO registers: E.CRH=0xbbbbbbbb E.CRL=0xb1111111 E.ODR=0x0077
*/
static const GpioInit E={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       OutPPH,  OutPPH,  OutPPH,  OutPPH,  OutPPH,  OutPPH,  OutPPH,  AOutPP,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
       AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x1,     0x1,     0x1,     0x0,     0x1,     0x1,     0x1,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port F on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   F  SignalName           BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 A0                   AltOut,push-pull       FSMC  FSMC address pin. 
   01 A1                   AltOut,push-pull       FSMC  FSMC address pin. 
   02 A2                   AltOut,push-pull       FSMC  FSMC address pin. 
   03 A3                   AltOut,push-pull       FSMC  FSMC address pin.  
   04 A4                   AltOut,push-pull       FSMC  FSMC address pin.  
   05 A5                   AltOut,push-pull       FSMC  FSMC address pin.  
   06 ADC3_IN4/VBAT_VOLTAGE       In,analog   ADC3,Ch4  Battery voltage.
   07 ADC3_IN5/LEAK_SENSE         In,analog   ADC3,Ch5  Leak sensor. 
   08 ADC3_IN6/HUM_ISENSE         In,analog   ADC3,Ch6  Humidity sensor.
   09 ADC3_IN7/PS1_SENSE          In,analog   ADC3,Ch7  Air bladder pressure.        
   10 ADC3_IN8/PS2_SENSE          In,analog   ADC3,Ch8  Internal vacuum sensor.
   11 ACTIVE_ENABLE_3P3V   Out,push-pull,low      GPIO  Enable 3.3V switching regulator.
   12 A6                   AltOut,push-pull       FSMC  FSMC address pin.  
   13 A7                   AltOut,push-pull       FSMC  FSMC address pin.  
   14 A8                   AltOut,push-pull       FSMC  FSMC address pin.  
   15 A9                   AltOut,push-pull       FSMC  FSMC address pin.  

   GPIO registers: F.CRH=0xbbbb1000 F.CRL=0x00bbbbbb F.ODR=0x0000
*/
static const GpioInit F={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  InAnlg,  InAnlg,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
       InAnlg,  InAnlg,  InAnlg,  OutPPL,  AOutPP,  AOutPP,  AOutPP,  AOutPP,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0 }};

/*========================================================================*/
/* GPIO configuration for Stm32 port G on Apf11 (Schematic: 304770 RevA)  */
/*========================================================================*/
/**   
   G  SignalName           BootUpConfig     Peripheral  Description
   ---------------------------------------------------------------------------------
   00 A10                  AltOut,push-pull       FSMC  FSMC address pin.   
   01 A11                  AltOut,push-pull       FSMC  FSMC address pin.   
   02 A12                  AltOut,push-pull       FSMC  FSMC address pin.   
   03 A13                  AltOut,push-pull       FSMC  FSMC address pin.   
   04 A14                  AltOut,push-pull       FSMC  FSMC address pin.   
   05 A15                  AltOut,push-pull       FSMC  FSMC address pin.   
   06 SPARE_SENSOR_PWR_EN  Out,push-pull,low      GPIO  Enable power for spare sensors.
   07 SD_DETECT            In,float             SDCARD  SD card detect pin.
   08 EXP_3P3V_PWR_EN      Out,push-pull,low      GPIO  Enable 3V power for expansion port.
   09 EXP_VBAT_PWR_EN      Out,push-pull,low      GPIO  Enable battery power for expansion port. 
   10 SPARE_DUART_IRQ_N    In,pull-up             SPI1  Interrupt input for spare sensor DUART. 
   11 RF_DUART_IRQ_N       In,pull-up             SPI1  Interrupt input for RF sensor DUART.
   12 EXP_DUART1_IRQ_N     In,pull-up             SPI1  Interrupt input for expansion DUART1. 
   13 EXP_DUART2_IRQ_N     In,pull-up             SPI1  Interrupt input for expansion DUART2. 
   14 EXP_DUART3_IRQ_N     In,pull-up             SPI1  Interrupt input for expansion DUART3. 
   15 EXP_DUART4_IRQ_N     In,pull-up             SPI1  Interrupt input for expansion DUART4. 

   GPIO registers: G.CRH=0x88888811 G.CRL=0x41bbbbbb G.ODR=0x0000
*/
static const GpioInit G={{
   /*  CRL.00,  CRL.01,  CRL.02,  CRL.03,  CRL.04,  CRL.05,  CRL.06,  CRL.07 */
       AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  AOutPP,  OutPPL,   InFlt,
   /*  CRH.08,  CRH.09,  CRH.10,  CRH.11,  CRH.12,  CRH.13,  CRH.14,  CRH.15 */
       OutPPL,  OutPPL,    InPU,    InPU,    InPU,    InPU,    InPU,    InPU,
   /*  ODR.00,  ODR.01,  ODR.02,  ODR.03,  ODR.04,  ODR.05,  ODR.06,  ODR.07 */
          0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,     0x0,
   /*  ODR.08,  ODR.09,  ODR.10,  ODR.11,  ODR.12,  ODR.13,  ODR.14,  ODR.15 */
          0x0,     0x0,     0x1,     0x1,     0x1,     0x1,     0x1,     0x1 }};

/*------------------------------------------------------------------------*/
/* function to assert pin(s) of an Stm32 GPIO port                        */
/*------------------------------------------------------------------------*/
/**
   This function asserts any combination of the 16-bits of an Stm32
   GPIO port; any changes are made atomically.  Each bit of a 16-bit
   word is used to determine which pins should be asserted; the least
   significant bit (LSB) is mapped to pin-1 and the most significant
   bit (MSB) is mapped to pin-16.  This function can be used to assert
   arbitrary pins but can not be used to clear them.  If a given bit
   is a '1' then the corresponding pin is to be asserted while a '0'
   causes no change to the corresponding bit.  Note that requests to
   assert a pin can be made only if the pin is configured for general
   purpose output; any other mode will induce an exception.

      \begin{verbatim}
      input:

         port...........The register address of the Stm32's GPIO port
                        (ie., GPIOA, GPIOB, etc.).

         PinsToAssert...A 16-bit word used to determine which pins
                        should be asserted and which should be left as
                        is.  If a given bit is a '1' then the
                        corresponding pin is to be asserted while a
                        '0' causes no change to the corresponding bit.

      output:

         This function returns a positive value if successful or else
         zero if pin(s) failed to be asserted for some reason.  A
         negative return value indicates that an exception was
         encounted; most likely, a pin was specified to be asserted
         when the pin was not configured go general purpose output.
      \end{verbatim}
*/
int Stm32GpioAssert(GpioPort *port, unsigned int PinsToAssert)
{
   return Stm32GpioModify(port,0x0000U,PinsToAssert);
}

/*------------------------------------------------------------------------*/
/* function to clear pin(s) of an Stm32 GPIO port                         */
/*------------------------------------------------------------------------*/
/**
   This function clears any combination of the 16-bits of an Stm32
   GPIO port; any changes are made atomically.  Each bit of a 16-bit
   word is used to determine which pins should be cleared; the least
   significant bit (LSB) is mapped to pin-1 and the most significant
   bit (MSB) is mapped to pin-16.  This function can be used to clear
   arbitrary pins but can not be used to assert them.  If a given bit
   is a '1' then the corresponding pin is to be cleared while a '0'
   causes no change to the corresponding bit.  Note that requests to
   assert a pin can be made only if the pin is configured for general
   purpose output; any other mode will induce an exception.

      \begin{verbatim}
      input:

         port...........The register address of the Stm32's GPIO port
                        (ie., GPIOA, GPIOB, etc.).

         PinsToClear....A 16-bit word used to determine which pins
                        should be cleared and which should be left as
                        is.  If a given bit is a '1' then the
                        corresponding pin is to be cleared while a '0'
                        causes no change to the corresponding bit.

      output:

         This function returns a positive value if successful or else
         zero of pin(s) failed to be cleared for some reason.  A
         negative return value indicates that an exception was
         encounted; most likely, a pin was specified to be cleared
         when the pin was not configured go general purpose output.
      \end{verbatim}
*/
int Stm32GpioClear(GpioPort *port, unsigned int PinsToClear)
{
   return Stm32GpioModify(port,PinsToClear,0x0000U);
}

/*------------------------------------------------------------------------*/
/* function to configure Stm32 GPIO pin(s)                                */
/*------------------------------------------------------------------------*/
/**
   This function configures individual GPIO pin(s) of the Stm32 into
   any of ten modes of operation.  Any combination of the sixteen pins
   can be simultaneously configured with the same mode of operation.
   Although the Stm32 allows selection of any of twenty-eight modes of
   operation, the SwiftWare GPIO model limits the selection to one of
   the following ten modes:

      InAnlg: This mode is intended for analog input to the
         analog-to-digital (AD) peripheral; the schmidt trigger is
         disabled.  This mode can not be used for digital input.
  
      InFlt: This mode is for digital input from a driven source; its
         input is floating.
  
      InPD: This mode is for digital input and incorporates a weak
         pull-down.
  
      InPU: This mode is for digital input and incorporates a weak
         pull-up. 
  
      AOutPP: This mode is for digital alternate-function output
         with a push-pull.  These speed is configured for 50MHz and
         represents the rate at which the signal can slew from high
         to low or vice versa.
  
      AOutOD: This mode is for digital alternate-function output
         with a open-drain interface.  These speed is configured for
         50MHz and represents the rate at which the signal can slew
         from high to low or vice versa.
  
      OutPPL: This mode is for digital general-purpose output with a
         push-pull interface.  It is initialized in the low state.
         These speed is configured for 10MHz and represents the rate
         at which the signal can slew from high to low or vice
         versa.
  
      OutPPH: This mode is for digital general-purpose output with a
         push-pull interface.  It is initialized in the high
         state. It is initialized in the low state.  These speed is
         configured for 10MHz and represents the rate at which the
         signal can slew from high to low or vice versa.
  
      OutODL: This mode is for digital general-purpose output with a
         open-drain interface.  It is initialized in the low state.
         These speed is configured for 10MHz and represents the rate
         at which the signal can slew from high to low or vice
         versa.
  
      OutODH: This mode is for digital general-purpose output with a
         open-drain interface.  It is initialized in the highThese
         speed is configured for 10MHz and represents the rate at
         which the signal can slew from high to low or vice versa.
         state.

   Although this function could be used repeatedly to configure all
   seven of the Stm32's GPIO ports during boot-up, the function
   Stm32GpioInit() was designed explicitly to serve that purpose and
   is there for recommended and preferred.

      \begin{verbatim}
      input:

         port...........The register address of the Stm32's GPIO port
                        (ie., GPIOA, GPIOB, etc.).

         pins...........A 16-bit word that specifies which pin(s)
                        should be reconfigured.  The least significant
                        bit (LSB) is mapped to pin-1 and the most
                        significant bit (MSB) is mapped to pin-16.
                        If a bit is asserted then the corresponding
                        pin will be reconfigured according to the
                        specified mode.  If a bit is cleared then the
                        corresponding pin configuration remains
                        unchanged.

          mode..........The GpioMode that must take the value of one
                        of the ten enumerated values listed above
                        (eg., InFlt, OutPPL, AOutPP, etc.).  Other
                        values that would be allowed by the Stm32 will
                        be rejected by the SwiftWare GPIO model.  All
                        of the pins corresponding to asserted bits in
                        the 'pins' argument will be reconfigured with
                        this GpioMode.

      output:
         This function returns a positive value if successful or else
         zero if the reconfiguration failed from some reason.  A
         negative return value indicates an exception was
         encountered. 
      \end{verbatim}
*/
int Stm32GpioConfig(GpioPort *port, unsigned int pins, GpioMode mode)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioConfig()";
  
   /* initialize the return value */
   int status=GpioOk;

   /* define some local work objects */
   int p; unsigned long int mask,CRL,CRH,ODR;

   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* enable the clock for the GPIO port */
   else if (port==(GpioPort *)GPIOA) {__HAL_RCC_GPIOA_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOB) {__HAL_RCC_GPIOB_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOC) {__HAL_RCC_GPIOC_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOD) {__HAL_RCC_GPIOD_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOE) {__HAL_RCC_GPIOE_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOF) {__HAL_RCC_GPIOF_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOG) {__HAL_RCC_GPIOG_CLK_ENABLE();}

   /* switch statement to enforce implemented GPIO modes */
   if (status>0) switch (mode)
   {
      /* enumerate configuration modes */
      case InAnlg: case OutPPL: case AOutPP:
      case InFlt:  case OutODL: case AOutOD:
      case InPD:   case OutPPH: 
      case InPU:   case OutODH: 
      {
         /* read the current GPIO configuration */
         CRL=port->CRL; CRH=port->CRH; ODR=port->ODR;

         /* loop thru all 16 pins of the port */
         for (p=0; p<16; p++)
         {
            /* determine if the current pin should be reconfigured */
            if (pins&(1UL<<p)) 
            {
               /* compute the mask to clear bits for the current pin */
               mask=(~(0xfUL<<(4*(p%8))));

               /* clear configuration bits for the current pin */
               if (p<8) {CRL&=mask;} else {CRH&=mask;}

               /* compute the mask to set the current pin's configuration */
               mask=mode; mask=mask<<(4*(p%8));

               /* set the current pin's configuration */
               if (p<8) {CRL|=mask;} else {CRH|=mask;}

               /* assert the current pin's ODR bit */
               if (mode==OutPPH || mode==OutODH || mode==InPU) {mask=(0x1UL<<p); ODR|=mask;}

               /* clear current pin's ODR bit */
               else {mask=(~(0x1UL<<p)); ODR&=mask;}
            }
         }

         /* reconfigure the GPIO port */
         port->ODR=ODR; port->CRL=CRL; port->CRH=CRH;
   
         break;
      }

      /* log the unsupported GPIO mode */
      default: {LogEntry(FuncName,"Unsupported GPIO mode: 0x%x\n",mode); status=GpioFail;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the Stm32's GPIO ports to a known working state */
/*------------------------------------------------------------------------*/
/**
   This function is designed to initialize the Stm32's GPIO ports into
   a known, valid, and tested working state at time of boot-up.  The
   CRL,CRH,ODR registers of each of the Stm32's GPIO registers are
   initialized with contents of the GpioInit objects A,B,C,D,E,F,G
   listed above.  These objects are all static, constant, and
   hard-coded for reliability.

      \begin{verbatim}
      output:
         This function returns a positive value if successful or else
         zero if the initializations failed for some reason.  A
         negative return value indicates that an exception was
         encounted.
      \end{verbatim}
*/
int Stm32GpioInit(void)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioInit()";
  
   /* initialize the return value */
   int err,status=GpioOk;

   /* initialize all of the Stm32's GPIO ports */
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOA,&A))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOB,&B))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOC,&C))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOD,&D))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOE,&E))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOF,&F))<=0) {status=err;}
   if ((err=Stm32GpioPortInit((GpioPort *)GPIOG,&G))<=0) {status=err;}

   if (status<=0)
   {
      /* log the error */
      LogEntry(FuncName,"GPIO port initializations failed.\n");
   }
   
   /* check the logging criteria */
   else if (debuglevel>=3 || (debugbits&STM32F103GPIO_H))
   {
      /* make a logentry */
      LogEntry(FuncName,"GPIO port initializations successful.\n");
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if a pin is asserted                   */
/*------------------------------------------------------------------------*/
/**
   This function determines if an arbitrary selection of pins are
   asserted as indicated by the GPIO IDR register.  A 16-bit word is
   used to specify which pins are to be tested.  Each asserted bit
   indicates that the corresponding bit of the specified GPIO port
   should be subject to the test.  Pins that are configured for any IO
   mode except for analog input are valid.

      \begin{verbatim}
      input:
      
         port....The register address of the Stm32's GPIO port (ie.,
                 GPIOA, GPIOB, etc.).

         pins....This 16-bit word specifies which pins to test.  Each
                 asserted bit indicates that the corresponding bit of
                 the specified GPIO port should be subject to the
                 test.  Pins that are configured for any IO mode
                 except for analog input are valid.

      output:
         This function returns a positive value if ALL of the selected
         pins are asserted.  However, if even a single pin is cleared
         then this function will return zero.  A negative return value
         indicates that an exception was encountered.
      \end{verbatim}
*/
int Stm32GpioIsAssert(GpioPort *port, unsigned int pins)
{
   /* initialize the return value */
   int status=GpioOk;
   
   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* test if all of the specified GPIO port's pins are asserted */
   else if (((port->IDR)&pins)^pins) {status=GpioFail;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if a pin is cleared                    */
/*------------------------------------------------------------------------*/
/**
   This function determines if an arbitrary selection of pins are
   cleared as indicated by the GPIO IDR register.  A 16-bit word is
   used to specify which pins are to be tested.  Each asserted bit
   indicates that the corresponding bit of the specified GPIO port
   should be subject to the test.  Pins that are configured for any IO
   mode except for analog input are valid.

      \begin{verbatim}
      input:
      
         port....The register address of the Stm32's GPIO port (ie.,
                 GPIOA, GPIOB, etc.).

         pins....This 16-bit word specifies which pins to test.  Each
                 asserted bit indicates that the corresponding bit of
                 the specified GPIO port should be subject to the
                 test.  Pins that are configured for any IO mode
                 except for analog input are valid.

      output:
         This function returns a positive value if ALL of the selected
         pins are cleared.  However, if even a single pin is asserted
         then this function will return zero.  A negative return value
         indicates that an exception was encountered.
      \end{verbatim}
*/
int Stm32GpioIsClear(GpioPort *port, unsigned int pins)
{
   /* initialize the return value */
   int status=GpioOk;
   
   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* test if all of the specified GPIO port's pins are cleared */
   else if (((~(port->IDR))&pins)^pins) {status=GpioFail;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if an Stm32 GPIO pin is configured for input     */
/*------------------------------------------------------------------------*/
/**
   This function determines if an arbitrary selection Stm32 GPIO pins
   are configured for digitial input.  A 16-bit word is used to
   specify which pins are to be tested.  Each asserted bit indicates
   that the corresponding bit of the specified GPIO port should be
   subject to the test.  Of the twenty-eight valid GPIO modes for the
   Stm32, only three represent digital input (ie., InFlt,InPU,InPD).
   This function tests the configuration of each selected pin to
   determine if it represents digital input.

      \begin{verbatim}
      input:
      
         port....The register address of the Stm32's GPIO port (ie.,
                 GPIOA, GPIOB, etc.).

         pins....This 16-bit word specifies which pins to test.  Each
                 asserted bit indicates that the corresponding bit of
                 the specified GPIO port should be subject to the
                 test.  

      output:
         This function returns a positive value if ALL of the selected
         pins are configured for digital input.  However, if even a
         single pin is not so configured then this function will
         return zero.  A negative return value indicates that an
         exception was encountered.
      \end{verbatim}
*/
int Stm32GpioIsInPin(GpioPort *port, unsigned int pins)
{
   /* initialize the return value */
   int p,status=GpioOk;
     
   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* loop over each of the port's 16 pins */
   else for (p=0; p<16; p++)
   {
      /* check if the current pin should be tested */
      if (pins&(0x1U<<p))
      {
         /* extract the port's configuration for the current pin */
         unsigned char cnf=(( ((p<8)?(port->CRL):(port->CRH)) >> ((p%8)*4))&0xf);

         /* test if the configuration does not represent digital input */
         if (!(cnf==InFlt || cnf==InPU || cnf==InPD)) {status=GpioFail; break;}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if an Stm32 GPIO pin is configured for output    */
/*------------------------------------------------------------------------*/
/**
   This function determines if an arbitrary selection Stm32 GPIO pins
   are configured for general purpose digitial output.  A 16-bit word
   is used to specify which pins are to be tested.  Each asserted bit
   indicates that the corresponding bit of the specified GPIO port
   should be subject to the test.  Of the twenty-eight valid GPIO
   modes for the Stm32, only twelve represent general purpose digital
   output.  This function tests the configuration of each selected pin
   to determine if it represents general purpose digitial output.

      \begin{verbatim}
      input:
      
         port....The register address of the Stm32's GPIO port (ie.,
                 GPIOA, GPIOB, etc.).

         pins....This 16-bit word specifies which pins to test.  Each
                 asserted bit indicates that the corresponding bit of
                 the specified GPIO port should be subject to the
                 test.  

      output:
         This function returns a positive value if ALL of the selected
         pins are configured for general purpose digitial output.
         However, if even a single pin is not so configured then this
         function will return zero.  A negative return value indicates
         that an exception was encountered.
      \end{verbatim}
*/
int Stm32GpioIsOutPin(GpioPort *port, unsigned int pins)
{
   /* initialize the return value */
   int p,status=GpioOk;
     
   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* loop over each of the port's 16 pins */
   else for (p=0; p<16; p++)
   {
      /* check if the current pin should be tested */
      if (pins&(0x1U<<p))
      {
         /* extract the port's configuration for the current pin */
         unsigned char cnf=(( ((p<8)?(port->CRL):(port->CRH)) >> ((p%8)*4))&0xf);

         /* test if the configuration does not represent general purpose output */
         if (!((~cnf)&0x8 && cnf&0x3)) {status=GpioFail; break;}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if a GpioPort pointer is valid                   */
/*------------------------------------------------------------------------*/
/**
   This function determines if a specified GpioPort points to one of
   the seven GPIO ports of the Stm32 (ie., GPIO{A,B,C,D,E,F,G}).

      \begin{verbatim}
      input:

         port....The register address of the Stm32's GPIO port.

      output:
         This function returns a positive value if the GpioPort
         pointer is valid or else zero if it is not.  A negative
         return value indicates that an exception was encounted.
      \end{verbatim}
*/
int Stm32GpioIsValidPort(GpioPort *port)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioIsValidPort()";
  
   /* initialize the return value */
   int status=GpioInvalid;
   
   /* validate the pointer to the GPIO port */
   if (!port) {LogEntry(FuncName,"Missing GPIO port."); status=GpioNull;}

   /* validate the GPIO port address */
   else if (port==(GpioPort *)GPIOA || port==(GpioPort *)GPIOB || port==(GpioPort *)GPIOC ||
            port==(GpioPort *)GPIOD || port==(GpioPort *)GPIOE || port==(GpioPort *)GPIOF ||
            port==(GpioPort *)GPIOG) {status=GpioOk;}

   /* log the error */
   else {LogEntry(FuncName,"Invalid Stm32 GPIO port: 0x08lx\n",(unsigned long int)port);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert or clear pin(s) of an Stm32 GPIO port               */
/*------------------------------------------------------------------------*/
/**
   This function asserts of clears any combination of the 16-bits of
   an Stm32 GPIO port; any changes are made atomically.  Each bit of
   two 16-bit words are used to determine which pins should be
   modified; the least significant bit (LSB) is mapped to pin-1 and
   the most significant bit (MSB) is mapped to pin-16.  This function
   can be used to assert or clear arbitrary pins.  One 16-bit word
   specifies pins that should be cleared while the other 16-bit word
   specifies pins that should be asserted.  If a given bit is a '1'
   then the corresponding pin is to be modified while a '0' causes no
   change to the corresponding bit.  A conflict is created when the
   same bit of each 16-bit word is asserted which would specify that
   the corresponding bit should be both cleared and asserted.  Such
   conflicts are resolved in favor of assertion as specified in the
   Stm32f103 reference manual.  Note that requests to modify a pin can
   be made only if the pin is configured for general purpose output;
   any other mode will induce an exception.

      \begin{verbatim}
      input:

         port...........The register address of the Stm32's GPIO port
                        (ie., GPIOA, GPIOB, etc.).

         PinsToClear....A 16-bit word used to determine which pins
                        should be cleared and which should be left as
                        is.  If a given bit is a '1' then the
                        corresponding pin is to be cleared while a '0'
                        causes no change to the corresponding bit.

         PinsToAssert...A 16-bit word used to determine which pins
                        should be asserted and which should be left as
                        is.  If a given bit is a '1' then the
                        corresponding pin is to be asserted while a '0'
                        causes no change to the corresponding bit.

      output:

         This function returns a positive value if successful or else
         zero of pin(s) failed to be modified for some reason.  A
         negative return value indicates that an exception was
         encounted; most likely, a pin was specified to be modified
         when the pin was not configured go general purpose output.
      \end{verbatim}
*/
int Stm32GpioModify(GpioPort *port, unsigned int PinsToClear, unsigned int PinsToAssert)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioModify()";
  
   /* initialize the return value */
   int status=GpioOk;
   
   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* confirm that pins to be modified are general purpose output pins */
   else if (Stm32GpioIsOutPin(port,PinsToClear|PinsToAssert)<=0)
   {
      /* log the error */
      LogEntry(FuncName,"GPIO port(0x%08lx) not configured for output on pin(s): 0x%04x\n",
               (unsigned long int)port,(PinsToClear|PinsToAssert)); status=GpioFail;
   }
      
   else
   {
      /* define maximum number of retries for confirmation loop */
      const int N=3;
      
      /* define some local work objects */
      int n; unsigned long int IDR,BSRR; unsigned int ClearViolations,AssertViolations;

      /* precondition the arguments for 16-bits */
      PinsToAssert&=0xffffU; PinsToClear&=0xffffU;
      
      /* deconflict by giving preference to assertion (per Stm32f103 data sheet) */
      PinsToClear &= ~(PinsToClear&PinsToAssert);

      /* construct the word to write to the Stm32's BSRR register */
      BSRR=PinsToClear; BSRR=(BSRR<<16); BSRR|=PinsToAssert;
      
      /* modify the GPIO port's pins and initiate the confirmation loop */
      for (status=GpioFail,port->BSRR=BSRR,n=0; n<N; n++)
      {
         /* read the input data register (IDR) */
         IDR=port->IDR;

         /* compute pins that should be clear but are actually asserted */
         ClearViolations = (((~IDR)&PinsToClear)^PinsToClear);
         
         /* compute pins that should be asserted but are actually clear */
         AssertViolations = ((IDR&PinsToAssert)^PinsToAssert);
         
         /* exit the confirmation loop if there are no violations */
         if (!ClearViolations && !AssertViolations) {status=GpioOk; break;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize an Stm32 GPIO port                              */
/*------------------------------------------------------------------------*/
/**
   This function initializes a Stm32 GPIO port into a known, valid,
   and tested working state at time of boot-up.  The CRL,CRH,ODR
   registers of a specified GPIO port are initialized with contents of
   a specified GpioInit object.  These objects are all static,
   constant, and hard-coded for reliability.  This function has static
   linkage and is intended for use by Stm32GpioInit().

      \begin{verbatim}
      input:

         port.....The register address of the Stm32's GPIO port (ie.,
                  GPIOA, GPIOB, etc.).

         init.....The GpioInit object containing the values of CRL,
                  CRH, and ODR to be assigned to the GPIO port.

      output:
         This function returns a positive value if successful or else
         zero if the initializations failed for some reason.  A
         negative return value indicates that an exception was
         encounted.
      \end{verbatim}
*/
static int Stm32GpioPortInit(GpioPort *port, const GpioInit *init)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioPortInit()";
  
   /* initialize the return value */
   int status=GpioOk;

   /* validate the GPIO port */
   if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* validate the pointer to the GpioInit object */
   else if (!init) {LogEntry(FuncName,"Missing GPIO initialization."); status=GpioNull;}

   /* enable the clock for the GPIO port */
   else if (port==(GpioPort *)GPIOA) {__HAL_RCC_GPIOA_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOB) {__HAL_RCC_GPIOB_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOC) {__HAL_RCC_GPIOC_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOD) {__HAL_RCC_GPIOD_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOE) {__HAL_RCC_GPIOE_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOF) {__HAL_RCC_GPIOF_CLK_ENABLE();}
   else if (port==(GpioPort *)GPIOG) {__HAL_RCC_GPIOG_CLK_ENABLE();}

   /* validate the GPIO port selection */
   if (status>0)
   {
      /* initialize port with reset values (see data sheet) prior to modifying ODR */
      port->CRL=0x44444444UL; port->CRH=0x44444444UL;
      
      /* initialize the GPIO port */
      port->ODR=init->ODR; port->CRL=init->CRL; port->CRH=init->CRH;

      /* validate the initialization */
      if (port->CRH!=init->CRH || port->CRL!=init->CRL || port->ODR!=init->ODR)
      {
         /* log the error */
         LogEntry(FuncName,"GPIO(0x%08lx) initialization failed:\n",(unsigned long int)port);
         LogEntry(FuncName,"(CRH,CRL,ODR):(0x%08lx,0x%08lx,0x%04lx) != (0x%08lx,0x%08lx,0x%04lx)\n",
                  port->CRH,port->CRL,port->ODR,init->CRH,init->CRL,init->ODR);
         
         /* reinitialize the return value */
         status=GpioFail;
      }

      /* check the logging criteria */
      else if (debuglevel>=4 || (debugbits&STM32F103GPIO_H))
      {
         /* make a logentry */
         LogEntry(FuncName,"GPIO(0x%08lx) initialization successful.\n",(unsigned long int)port);
      }
   }
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the input data register from an Stm32 GPIO port.      */
/*------------------------------------------------------------------------*/
/**
   This function reads the contents of the input data register (IDR)
   of a specified Stm32 GPIO port.  The IDR contains the state of each
   pin that is configured for digital IO.  Pins that are configured
   for analog IO will always indicate the bit to be cleared.

      \begin{verbatim}
      input:
      
         port....The register address of the Stm32's GPIO port (ie.,
                 GPIOA, GPIOB, etc.).

      output:

         idr.....The 16-bit word into which the GPIO port's IDR
                 register will be written.

         This function returns a positive value if the GpioPort IDR
         register was successfully read or else zero if not.  A
         negative return value indicates that an exception was
         encounted.
      \end{verbatim}
*/
int Stm32GpioRead(GpioPort *port, unsigned int *idr)
{
   /* define the logging signature */
   cc *FuncName="Stm32GpioRead()";

   /* initialize the return value */
   int status=GpioOk;

   /* initialize the return value */
   if (idr) {(*idr)=0;}
   
   /* validate the function argument */
   if  (!idr) {LogEntry(FuncName,"NULL function parameter.\n"); status=GpioNull;}
   
   /* validate the GPIO port */
   else if (Stm32GpioIsValidPort(port)<=0) {status=GpioInvalid;}

   /* read the GPIO port's input data register */
   else {(*idr)=port->IDR;}

   return status;
}

#endif /* STM32F103GPIO_C */
