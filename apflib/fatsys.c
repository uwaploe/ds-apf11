#ifndef FATSYS_H
#define FATSYS_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare file IO system
 * -------------------------------------------
 *
   NewLib is a C library intended for use on embedded systems
   (http://sourceware.org/newlib).  The NewLib C library is used by
   the GNU EABI (Elf Application Binary Interface) toolchain to build
   Apf11 firmware.  NewLib is full-featured, mature, and cleverly
   designed to rely on the embedded systems developer's low-level file
   system implementation.  NewLib's API for low-level file I/O is
   modeled on UNIX-style file descriptors and unbuffered file I/O.
   The API requires that the firmware developer (ie., me) implement
   seventeen primitive functions that include unbuffered file I/O:
   _close(), _fstat(), _lseek(), _link(), _open(), _read(), _unlink(),
   and _write().

   The SwiftWare file system is designed for battery-powered embedded
   systems. I have exploited NewLib's API to design and implement
   three especially important features of the SwiftWare file system
   used by Apf11 firmware:

      1. Facilitate persistent valid file I/O across standby/wake
         cycles of the Apf11.  The Apf11 experiences many hundreds of
         standby/wake cycles during the course of a typical profile
         cycle.  To open/close files at the start/end of each
         wake/standby cycle would impose OCD-like machinations.
         Hence, it is desirable for a file to be opened once during
         the profile cycle, remain operational throughout the entire
         profile cycle so that data could be appended as it becomes
         available, and finally closed in preparation for the next
         profile cycle.

      2. To maximize immunity to file system corruption caused by
         brown-out, black-out, or file I/O glitches associated with
         firmware defects.

      3. To make it possible for the Apf11 to continue to function and
         produce normal hydrographic data if the FatFs file system
         fails or the SD card becomes unreadable.  The intention is
         for the firmware to behave similarly to the early prototype
         floats that used only the RAM file system (ie., before the
         FatFs system had been developed).

   Since the Apf11's persistent RAM is permanently powered, it is
   permissible to leave files open and fully operational across
   standby/wake cycles of the STM32 by exploiting unbuffered file I/O
   that uses UNIX-style file descriptors and the family of file I/O
   functions that include open(), close(), read(), write(), fstat(),
   and lseek().  For example, the engineering log file in Apf11
   firmware is opened at the beginning of each telemetry phase and
   remains open during the entire profile cycle until being closed
   just prior to the next telemetry phase.  This feature was
   implemented by the exclusive use of UNIX-style file I/O for
   creation and management of the engineering logs (see logger.c). 

   The FatFs by Chan is the basis for permanent nonvolatile storage on
   an SD card.  However, during critical operations involving writes,
   renaming, or unlinking operations the FatFs is vulnerable to
   corruption.  This vulnerability persists for the duration of the
   period that any files or directories are open for writing or
   modification.  The SwiftWare file system minimizes these
   vulnerabilities by limiting the amount of time that FatFs
   experiences open files.  FatFs files are open for only milliseconds
   or up to a second or so before being closed again.  The FatFs is
   accessed and modified for only brief periods during calls to
   _open(), _close(), _unlink(), or _link() before the FatFs is closed
   once again.  In this way the time-period of vulnerability is
   minimized.  This strategy is implemented in such a way that is
   transparent to the user.  That is, any give file can be opened and
   operated-on for arbitrarily long periods of time.

   When a file is opened, say with fopen(), the file in FatFs is
   opened, transferred to a small RAM-based file system, and then
   FatFs is closed again.  All subsequent operations are applied to
   the RAM-based file system that represents the entirety of the file.
   When this file is closed, say with fclose(), then the file in FatFs
   is opened, and over-written with the RAM-based version, and then
   FatFs is closed once again.  In this way, the RAM file sysytem acts
   to buffer file IO operations so that the FatFs volume suffers the
   mimimum probability of corruption.

   The RAM-based file system is purely virtual/conceptual in nature
   and so its structure can not suffer irreparable corruption.  Any
   potential corruption is limited to the contents of individual
   files.  While such a corruption would represent the loss of data,
   it would not prevent subsequent correct operation of the file
   system.  This is in contrast to a corrupted FatFs file system;
   subsequent operation may not be possible until the file system
   itself was rebuilt.

   The RAM-based file system uses relatively fast persistent memory
   and so, except for calls to _open(), _close(), _link(), or
   _unlink() then file operations do not impose a speed-penalty.
   
   The NewLib design for buffered file I/O implements FILE streams
   that are built upon the lower-level UNIX-style file I/O.  However,
   NewLib buffered FILE streams completely lack the capacity to remain
   valid across standby/wake cycles.  For this reason, the logging
   facility completely avoids buffered FILE streams except for use of
   the STDERR stream whose management across standby/wake cycles is
   performed by the NewLib libc library.  Despite the nonpersistent
   property of NewLib's buffered file I/O, FILE streams are fully
   functional and behave as expected within any given wake/standby
   cycle.  It is up to the firmware developer to properly fclose()
   open streams prior to entering standby mode, else the associated
   files might be left in an undefined state.  Even in such a case,
   the file system will remain usable and uncorrupted.

   There are two main limitations of this file system.  First, since
   the entire file resides in RAM between calls to _open() and
   _close() then the maximum file size is determined by the size of
   the RAM buffer (eg., 64KB).  Second, the maximum number of files
   that can be simultaneously open is relatively small (eg., 5 files)
   but the standard IO streams (stdin, stdout, stderr) do NOT count
   against this maximum limit.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define fatsysChangeLog "$RCSfile$  $Revision$  $Date$"

#include <ctype.h>
#include <errno.h>
#include <ff.h>
#include <mcu.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/times.h>

/* file management structure */
typedef struct 
{
   signed   char fdes;
   unsigned char mode;
   unsigned char err;
   unsigned int  crc;
} File;

/* stdio resident macros */
#undef  FOPEN_MAX
#define FOPEN_MAX     (5)
#undef  FILENAME_MAX
#define FILENAME_MAX  (63)

/* nonstandard macros */
#define NSTD (3)
#define NFIO (FOPEN_MAX)
#define KB   (1L<<10)
#define MB   (1L<<20)

/* low-level functions used by Newlib libc library */
int   _close(int file);
int   _execve(char *name, char **argv, char **env);
void  _exit (int status);
int   _fork(void);
int   _fstat(int file, struct stat *st);
int   _getpid(void);
int   _isatty(int file);
int   _kill(int pid, int sig);
off_t _lseek(int file, off_t offset, int whence);
int   _link(const char *oldfname, const char *newfname);
int   _open(const char *fname, int flags, ...);
int   _read(int file, char *ptr, int len);
int   _stat(const char *file, struct stat *st);
int   _times(struct tms *buf);
int   _unlink(const char *fname);
int   _wait(int *status);
int   _write(int file, char *ptr, int len);

/* enumerate the FatFs volumes */
typedef enum {FatRoot,FatArchive} FatFsVolume;

/* nonstandard functions to augment the stdio library */
int         fat_ismount(void);
int         fat_mount(void);
int         fat_umount(void);
int         fdes(void);
int         fdump(FILE *fp);
int         fformat(void);
const char *fglob(const char *pattern, const char *dir);
int         fmove(const char *glob, FatFsVolume source);
struct tm  *ftime(const FILINFO *finfo, struct tm *ftime);
int         fnameok(const char *fname);
int         sflush(File *stream);
int         streamok(int handle);

/* external declaration of FatFs archive directory */
extern const char *arcdir;

/* external declaration for switch to enable/disable the FatFs file system */
extern persistent unsigned char FatFsDisable;

#endif /* FATSYS_H */
#ifdef FATSYS_C
#undef FATSYS_C

#include <crc16bit.h>
#include <diskio.h>
#include <fatio.h>
#include <fcntl.h>
#include <logger.h>
#include <Stm32f103Gpio.h>
#include <stream.h>
#include <unistd.h>

#define IOOK    (0x00)
#define IOR     (0x01)
#define IOW     (0x02)
#define IOEOF   (0x40)
#define IOERR   (0x80)

#define STDIN   STDIN_FILENO
#define STDOUT  STDOUT_FILENO
#define STDERR  STDERR_FILENO

/* define the array of File objects */
static persistent File fio[NFIO];

/* define space to emulate Newlib's rename mechanism */
static char unlinkfname[FILENAME_MAX+1];

/* define the pathname to the FatFs archive directory */
const char *arcdir = "archive";

/* switch to enable/disable the FatFs file system */
persistent unsigned char FatFsDisable;

/* prototypes for local functions with external linkage */
void ErrorHandler(const char *FuncName);
int __io_getchar(void) __attribute__((weak));
int __io_putchar(int ch) __attribute__((weak));

/* prototypes for functions with static linkage */
static int          fiobusy(const char *fname);
static void         fioinit(File *fio);
static unsigned int fstreamcrc(File *stream);
static int          fstreamok(File *fp);
static void         fvalid(void);
static void         streamerr_set(File *stream,unsigned char err);
static void         streamerr_clear(File *stream,unsigned char err);

/* prototypes for unpublished functions with external linkage */
int _closeall(void);

/*------------------------------------------------------------------------*/
/* function to close a file                                               */
/*------------------------------------------------------------------------*/
/**
   This function closes a file associated with a specified file
   descriptor.  If the file was being used for output, any buffered
   data is written first, using fflush().

   \begin{verbatim}
   input:
      file...The handle for the file to be closed.

   output:
      Upon successful completion 0 is returned.  Otherwise, EOF is
      returned and the global variable errno is set to indicate the
      error:
         EBADF: The stream associated with the file is invalid.
         EPERM: The stdio streams can not be closed.
   \end{verbatim}
*/
int _close(int file)
{
   /* initialize the function's return value */
   int err=EOF; errno=0;

   /* refuse to close stdio streams */
   if (file==STDIN || file==STDOUT || file==STDERR) errno=EPERM;
   
   else
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];

      /* clear the IOEOF bit */
      streamerr_clear(stream,IOEOF);
      
      /* validate the stream */
      if (fstreamok(stream)<=0) {errno=EBADF;} 
   
      /* determine if the file is writable */
      else if (stream->mode&IOW)
      {
         /* write the file to the FatFs volume */
         if (fioPut(&fioblk_[stream->fdes])>=0 || !fioblk_[stream->fdes].len)
         {
            /* erase the file from the RAM file system */
            fioWipe(&fioblk_[stream->fdes]); err=0;
         }

         /* check for mount failure in fioPut() */
         else if (errno==EIO) {err=0;}
      }

      /* for read-only files, do not erase if file not in FatFs volume */
      else if (fioFindFatFs(fioblk_[stream->fdes].fname,"")<0) {err=0;}

      /* file exists in FatFs; erase from RAM file system */
      else {fioWipe(&fioblk_[stream->fdes]); err=0;}
      
      /* reinitialize the stream */
      fioinit(stream);
   }
   
   return err;  
}

/*------------------------------------------------------------------------*/
/* function to close all open Files                                       */
/*------------------------------------------------------------------------*/
/**
   This function closes all open Files. This function should not be
   called directly because Files that might be associated with open
   FILE streams are also closed which would leave the buffered stream
   in an invalid and potentially truncated state.  Instead, the
   fcloseall() function is recommended because it properly manages the
   task of flushing and closing open buffered FILE streams before
   calling this function to close any remaining (unbuffered) Files.

   This function returns zero on success or else EOF on failure.
*/
int _closeall(void)
{
   /* initialize the function's return value */
   int i,status=0;

   /* loop over all File objects */
   for (i=0; i<NFIO; i++)
   {
      /* initialize invalid or not-open File objects */
      if (fstreamok(&fio[i])<=0) {fioinit(&fio[i]);}

      /* close the File object */
      else if (close(fio[i].fdes+NSTD)) {status=EOF;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _execve(char *name, char **argv, char **env)
{
   /* make a logentry */
   LogEntry("_execve()","Unimplemented.\n"); errno=ENOMEM;

   return EOF;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
void _exit(int status) {do {ErrorHandler("_exit()");} while (1);}

/*------------------------------------------------------------------------*/
/* function to determine if the FatFs volume is mounted                   */
/*------------------------------------------------------------------------*/
/**
   This function determines if the FatFs volume is mounted.  If the
   FatFs volume is mounted then this function returns a positive value
   or else zero if the FatFs volume is not mounted.
*/
int fat_ismount(void)
{
   /* determine if the FatFs volume is mounted */
   int status = (disk_ioctl(0,FAT_ISMOUNT,NULL)==RES_OK) ? 1 : 0;
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to mount the FatFs volume                                     */
/*------------------------------------------------------------------------*/
/**
   This function mounts the FatFs volume for subsequent use.  If
   successful, this function returns a positive value; otherwise zero
   is returned.
*/
int fat_mount(void)
{
   /* initialize the return value */
   int status=0;

   /* check if the FatFs system is enabled or disabled */
   if (!FatFsDisable)
   {
      /* define local objects needed for SD card operation */
      FRESULT f; unsigned char pwren;

      /* record the state of the SD power-enable pin */
      if (!(pwren=Stm32GpioIsAssert(GPIOB,SD_PWR_EN_Pin)))
      {
         /* assert the SD power-enable pin */
         Stm32GpioAssert(GPIOB,SD_PWR_EN_Pin);
      }

      /* detect the presence/absence of the SD card */
      if (Stm32f103SdCardDetect()>0)
      {
         /* mount the FatFs volume */
         status = (disk_ioctl(0,FAT_MOUNT,&f)==RES_OK && f==FR_OK) ? 1 : 0;
      }

      /* check if SD card should be returned to original disabled state */
      else if (!pwren) {Stm32GpioClear(GPIOB,SD_PWR_EN_Pin);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to unmount the FatFs volume                                   */
/*------------------------------------------------------------------------*/
/**
   This function unmounts the FatFs volume.  If successful, this
   function returns a positive value; otherwise zero is returned.
*/
int fat_umount(void)
{
   /* initialize the return value */
   int status=0;

   /* check if the FatFs system is enabled or disabled */
   if (!FatFsDisable)
   {
      /* define local objects needed for SD card operation */
      FRESULT f; unsigned char pwren;

      /* record the state of the SD power-enable pin */
      if (!(pwren=Stm32GpioIsAssert(GPIOB,SD_PWR_EN_Pin)))
      {
         /* assert the SD power-enable pin */
         Stm32GpioAssert(GPIOB,SD_PWR_EN_Pin);
      }

      /* detect the presence/absence of the SD card */
      if (Stm32f103SdCardDetect()>0)
      {
         /* unmount the FatFs volume */
         status = (disk_ioctl(0,FAT_UMOUNT,&f)==RES_OK && f==FR_OK) ? 1 : 0;
      }

      /* check if SD card should be returned to original disabled state */
      else if (!pwren) {Stm32GpioClear(GPIOB,SD_PWR_EN_Pin);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the next available file descriptor                  */
/*------------------------------------------------------------------------*/
/**
   This function returns the next available file descriptor, if one
   exists.  If the maximum number of open files already exists then
   this function returns EOF.
*/
int fdes(void)
{
   /* initialize the return value */
   int i,fdes=EOF;

   /* loop through each of FioBlk in the RAM file system */
   for (i=0; i<NFIO; i++)
   {
      /* file is available if the current FioBlk does not have a valid file name */
      if (fnameok(fioblk_[i].fname)<=0) {fdes=i; break;}
   }
   
   return fdes;
}

/*------------------------------------------------------------------------*/
/* diagnostic function to dump a FioBlk object to stderr                  */
/*------------------------------------------------------------------------*/
/**
   This function dumps the contents of a FILE stream object to stderr.  It
   was intended mostly for diagnostic purposes.  If the function argument is
   NULL then errno is set to ENULLARG and the function will return EOF.
   Otherwise, the return value and the value of errno will be as determined
   by fstreamok().
*/
int fdump(FILE *fp) 
{
   /* initialize the return value */
   int i,status=EOF;

   /* extract the Newlib file descriptor from the stream */
   const int file=fileno(fp);

   /* flush the stream */
   if (fp) {fflush(fp);}

   /* validate the function arguments */
   if (!fp) errno=EINVAL;

   /* check for a standard io stream */
   else if (file==STDIN || file==STDOUT || file==STDERR)
   {
      /* dump just the file descriptor */
      fprintf(stderr,"STDIO: file[%d]\n",file);
   }

   /* validate the Newlib file descriptor */
   else if (file>=NSTD && file<(NSTD+NFIO))
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];
      
      /* reinitialize the function's return value */
      status=fstreamok(stream);

      /* dump the members of the FILE structure */
      fprintf(stderr,"FILE%d: fdes[%d] mode[0x%04x] err[0x%02x] crc[0x%04x] [%s]\n",file,
              stream->fdes,stream->mode,stream->err,stream->crc,((status>0)?"OK":"Invalid"));

      /* validate the stream */
      if (status>0 && stream->fdes>=0 && stream->fdes<NFIO)
      {
         /* create a local pointer to the IO control block */
         FioBlk *fioblk=&fioblk_[stream->fdes];

         /* dump the length, file position, and file name */
         fprintf(stderr,"FioBlk[%d]:fpos[%u] len[%u] fname[%s]\n",
                 stream->fdes,fioblk->fpos,fioblk->len,fioblk->fname);

         /* flush the stderr stream */
         fflush(stderr);

         /* dump the buffer */
         for (i=0; i<fioblk->len; i++)
         {
            if (isprint(fioblk->buf[i])) putchar(fioblk->buf[i]);
            else {printf("[0x%02x]",fioblk->buf[i]);}
         }
         
         /* terminate the dump and flush the stdout stream */
         putchar('\n'); fflush(stdout);
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to format and reinitialize the file system.                   */
/*------------------------------------------------------------------------*/
/**
   This function formats the ram-based file IO system leaving it fully
   initialized and prepared for operation.  All of the File structures
   as well as FioBlk structures are reinitialized.  This function
   returns a positive value, on success, or else zero, on failure.
*/
int fformat(void)
{
   /* initialize the return value */
   int i,status=0;

   /* reinitialize the File objects */
   for (i=0; i<NFIO; i++) fioinit(&fio[i]);
   
   /* reinitialize the FioBlk objects */
   status=fioInit();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to seek file names from FatFs volume that match a pattern     */
/*------------------------------------------------------------------------*/
/**
   This function seeks file names from the FatFs volume that match a
   user-specified globbing pattern.  Only the '?' and '*' characters
   are treated as metacharacters where '?' matches any single
   character and '*' matches any contiguous string of characters of
   length zero or more.  All other characters in the pattern string
   require literal matches in the directory entry of the FatFs
   volume.  Matching is applied to the long file name (LFN) only.

   A glob is initiated by calling this function with a specified
   pattern; the existence of a non-NULL pattern initializes the glob.
   Subsequent calls with a NULL function argument will repeat the same
   glob in order to acquire the next FatFs directory entry that
   matches the pattern.  In this way, continued subsequent calls with
   a NULL argument will cause the glob to walk through all of the
   directory entries in order to find all matching entries.  After the
   last matching entry is found, this function will return NULL until
   the next glob session is initiated.

   Each time that this function is called with a non-NULL argument, a
   new globbing session is initiated and so repeated calls with a
   non-NULL argument will always find the first matching directory
   entry.

   This function never attempts to mount or unmount the FatFs
   volume. The FatFs volume must be mounted prior to calling this
   function, must remain continously mounted during all repeated calls
   to this function, and must be unmounted after the last call to this
   function.

   \begin{verbatim}
   input:

      pattern...This is the globbing pattern used to initialize the
                session. The path is always relative to the root
                directory of the FatFs volume.  A non-NULL pattern
                always initiates a new globbing session.  If this
                argument is NULL then the directory is searched for
                the next entry that matches the current globbing
                session.
      dir.......The FatFs directory within which the globbing pattern
                should be applied. If this argument is NULL then the
                root directory of the FatFs volume is searched.

   output:
      This function returns the next directory entry that matches the
      current globbing session.  If no such match exists then this
      function returns NULL to signal the end of the current session.
   \end{verbatim}
*/
const char *fglob(const char *pattern, const char *dir)
{
   /* define the logging signature */
   cc *FuncName="fglob()";

   /* initialize return value */
   const char *fname=NULL;

   /* define static objects to support repeated calls */
   static DIR dj; static FILINFO fno; static long int lock=0;

   /* define local objects used for FatFs operations */
   FRESULT fstatus;

   /* make sure the FatFs volume has been mounted externally */
   if (!fat_ismount()) {LogEntry(FuncName,"FatFs volume not mounted.\n");}
      
   /* check if a new glob should be initiating */
   else if (pattern)
   {
      /* if the glob is already locked then close the directory */
      if (lock) {f_closedir(&dj); lock=0;}

      /* find the first file that matches the pattern */
      if ((fstatus=f_findfirst(&dj,&fno,((dir)?dir:""),pattern))==FR_OK)
      {
         /* initialize the return value */
         fname = ((fno.fname[0]) ? fno.fname : NULL);

         /* lock the glob with the directory open */
         lock = (fname) ? 1 : 0;
      }

      /* log the globbing failure */
      else {LogEntry(FuncName,"Glob failure for pattern: "
                     "\"%s\" [FatErr=%d]\n",fstatus);}
      
      /* if unlocked then close the directory */
      if (!lock) {f_closedir(&dj);}
   }

   /* check if the glob is locked */
   else if (lock)
   {
      /* find the next file that matches the locked glob */
      if ((fstatus=f_findnext(&dj,&fno))==FR_OK)
      {
         /* initialize the return value */
         fname = (fno.fname[0]) ? fno.fname : NULL;

         /* lock the glob with the directory open */
         lock = (fname) ? 1 : 0;
      }

      /* log the globbing failure */
      else {LogEntry(FuncName,"Glob failure. [FatErr=%d]\n",fstatus); lock=0;}
      
      /* if unlocked then close the directory */
      if (!lock) {f_closedir(&dj);}
   }

   /* log the missing globbing pattern */
   else {LogEntry(FuncName,"Missing glob pattern.\n");}
      
   return fname;
}

/*------------------------------------------------------------------------*/
/* function to determine if a given file is already open                  */
/*------------------------------------------------------------------------*/
/**
   This function determines if a file with a specified name is already
   open.  This function returns a non-negative value (ie., the file
   descriptor) if an open file is found.  Otherwise, this function returns
   the value -1.  If the function argument is NULL then errno is set to
   EINVAL and the function returns the value -1.
*/
static int fiobusy(const char *fname)
{
   /* initialize the return value */
   int i,fdes=EOF;

   /* validate the function argument */
   if (!fname) {errno=EINVAL;}

   /* loop over the FioBlk's */
   else for (i=0; i<NFIO; i++)
   {
      /* disregard if the current File is not in use */
      if (fio[i].fdes==EOF || fio[i].mode==IOOK) continue;

      /* check if filename in the current File is a match */
      if (!strncmp(fname,fioblk_[i].fname,FILENAME_MAX)) fdes=fio[i].fdes;
   }

   return fdes;
}

/*------------------------------------------------------------------------*/
/* function to initialize a File object                                   */
/*------------------------------------------------------------------------*/
/**
   This function initializes a File object to be in a valid but inactive
   state.  If the function argument is NULL then errno is set to EINVAL.
*/
static void fioinit(File *fio)
{
   /* validate the function argument */
   if (!fio) errno=EINVAL;

   /* initialize the File structure elements */
   else {fio->fdes=EOF; fio->mode=IOOK; fio->err=0; fio->crc=fstreamcrc(fio);}
}

/*------------------------------------------------------------------------*/
/* function to move files between the archive and the FatFs volume        */
/*------------------------------------------------------------------------*/
/**
   This function moves files from the FatFs root directory to the
   archive or vice versa.  All files in the source volume that match
   the globbing pattern will be moved.  Any files in the destination
   directory that match the file name will be over-written.  The
   globbing pattern should not include any part of the path except for
   the file name itself.  Two metacharacters (?,*) are supported for
   the glob. The '?'  metacharacter matches any character and '*'
   matches any string of length zero or more.

*/
int fmove(const char *glob, FatFsVolume source)
{
   /* define the logging signature */
   cc *FuncName="fmove()";
   
   /* initialize the return value */
   int n=0;
   
   /* qualify the glob pattern */
   if (!glob || !glob[0] || strlen(glob)>FILENAME_MAX) {n=0;}

   /* validate the source volume */
   else if (source!=FatRoot && source!=FatArchive) {n=0;}
   
   /* close all FILE streams to avoid corruption of the FatFs file system */
   else if (!fclose_streams())
   {
      /* unmount then re-mount the FatFs volume */
      if (fat_umount() && fat_mount())
      {
         /* define static objects to support repeated calls */
         DIR dj; FILINFO finfo; FRESULT fstatus; char buf[2*FILENAME_MAX+2]; cc *to;

         /* initialize the directory to search */
         cc *from=(source==FatArchive) ? arcdir : ""; 

         /* find the first file that matches the pattern */
         if ((fstatus=f_findfirst(&dj,&finfo,from,glob))==FR_OK && finfo.fname[0]) do
         {
            /* create a pathname for the archived file */
            snprintf(buf,sizeof(buf),"%s/%s",arcdir,finfo.fname);
            
            /* assign the source path */
            from = (source==FatArchive) ? buf : finfo.fname;

            /* assign the destination path */
            to = (source==FatArchive) ? finfo.fname : buf;

            /* clear the destination path */
            f_unlink(to);
            
            /* move the file from the source path to the destination path */
            if ((fstatus=f_rename(from,to))!=FR_OK) {break;}

            /* log the moved file */
            else {LogEntry(FuncName,"Moved: %s -> %s\n",from,to); n++;}
         }

         /* find the next file that matches the glob pattern */
         while ((fstatus=f_findnext(&dj,&finfo))==FR_OK && finfo.fname[0]);
      
         /* close the directory */
         f_closedir(&dj);

         /* log a FatFs error */
         if (fstatus!=FR_OK) {LogEntry(FuncName,"Failed: %s -> %s [FatErr=%d]\n",from,to,fstatus);}
      }

      /* unmount the FatFs volume */
      fat_umount();
   }
   
   return n;
}

/*------------------------------------------------------------------------*/
/* function to determine if a specified filename is valid                 */
/*------------------------------------------------------------------------*/
/**
   This function determines if a specified filename is valid.  Filenames are
   case sensitive, must start with a letter, and be composed from the the
   set of characters inside the brackets: [A-Za-z0-9.:;-_].  The length of
   the filename may range from 1 to FILENAME_MAX.

   This function returns a positive value if the filename is valid and zero
   if the filename is invalid.  This function returns a negative value if an
   exception is detected and errno is set accordingly.  EINVAL indicates
   that the function argument was NULL.
*/
int fnameok(const char *fname)
{
   int len,status=EOF;

   /* validate the function argument */
   if (!fname) errno=EINVAL;

   /* valid names must be at least one byte long */
   else if (!fname[0]) status=0;
   
   else
   {
      /* reiniatialize the return value */
      status=0;
      
      /* an acceptable first character might be a letter of the alphabet */
      if (toupper(*fname)>='A' && toupper(*fname)<='Z') status=1;
      
      /* an acceptable first character might be a number in the range [0,9] */
      else if (toupper(*fname)>='0' && toupper(*fname)<='9') status=1;

      /* loop through and validate each character of the file name */
      for (len=0; (*fname) && status; fname++,len++) 
      {
         /* reject any characters not in the set: [A-Za-z0-9./:;-_] */
         if ((*fname)<'-' || (*fname)>'z') status=0;
         else if (strchr("\\[]<>{}^=?`@*",*fname)) status=0;
      }

      /* check to see if the filename is too long */
      if (len>FILENAME_MAX) status=0;
   }
           
   return status;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _fork(void) {LogEntry("_fork()","Unimplemented\n"); errno=EAGAIN; return -1;}

/*------------------------------------------------------------------------*/
/* collect metadata for a file                                            */
/*------------------------------------------------------------------------*/
/**
   These functions return information about a file, in the buffer
   pointed to by stat. This function returns zero, on success, or EOF,
   on failure.
*/
int _fstat(int file, struct stat *st)
{
   /* initialize the return value */
   int status=0; errno=0;
   
   /* reinitialize the stat structure to zeros */
   memset((void *)st,0,sizeof(struct stat));

   /* specify the standard streams as character devices */
   if (file==STDIN || file==STDOUT || file==STDERR) {st->st_mode=S_IFCHR;} 

   /* validate the stream */
   else if (streamok(file))
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];

      /* set the current file size */
      st->st_size=fioblk_[stream->fdes].len;

      /* set the maximum file size */
      st->st_blksize=BUFSIZE;
         
      /* specify FioBlk's to be a character device */
      st->st_mode=S_IFCHR;
   }

   /* indicate failure */
   else {status=EOF; errno=EBADF;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to compute the File object's CRC                              */
/*------------------------------------------------------------------------*/
/**
   This function computes a File object's CRC element.  This CRC is used to
   determine that a stream is in a valid state.  If the function argument is
   NULL then this function returns zero (which is never a valid CRC).
*/
static unsigned int fstreamcrc(File *stream)
{
   /* initialize the return value */
   unsigned int crc = 0;

   /* validate the stream */
   if (stream)
   {
      /* determine the number of bytes in the CRC computation */
      unsigned int n = sizeof(*stream)-sizeof(stream->crc);

      /* compute CRC of the stream object */
      crc = Crc16Bit((unsigned char *)stream,n);

      /* remap 0x0000 to 0x0001 */
      if (!crc) crc=0x0001;
   }
   
   return crc;
}

/*------------------------------------------------------------------------*/
/* function to determine if a File stream is valid                        */
/*------------------------------------------------------------------------*/
/**
   This function determines if a File stream is valid.  A stream is valid if
   its file descriptor is in the semiopen range [0,FOPEN_MAX] and has a
   valid CRC.  This function returns a positive number if the stream is
   valid and zero if the stream is invalid.  If the function argument is
   NULL then this function returns -1 and errno is set to EINVAL.
*/
static int fstreamok(File *stream)
{
   /* initialize the return value */
   int status=1;

   /* validate the function argument */
   if (!stream) {errno=EINVAL; status=-1;}

   /* validate the file descriptor */
   else if (stream->fdes<0 || stream->fdes>=FOPEN_MAX) status=0;

   /* compare the CRC of the stream object */
   else status=(fstreamcrc(stream)==stream->crc && stream->crc)?1:0;
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to extract the FatFs file modification date/time              */
/*------------------------------------------------------------------------*/
/**
   This function extracts the file modification date and time from a
   FatFs FILINFO object and stores the data in a tm object. On
   success, this function returns a pointer to the tm object. NULL is
   returned if either of the function arguments is NULL.
*/
struct tm  *ftime(const FILINFO *finfo, struct tm *ftime)
{
   if (finfo && ftime)
   {
      /*initialize the tm object to zeros */
      memset(ftime,0,sizeof(struct tm));
      
      /* extract the year from the modification date */
      ftime->tm_year = (((finfo->fdate)&0xfe00)>>9) + 80;

      /* extract the month from the modification date */
      ftime->tm_mon = (((finfo->fdate)&0x01e0)>>5) - 1;

      /* extract the day from the modification date */
      ftime->tm_mday = (finfo->fdate)&0x001f;

      /* extract the hour from the modification time */
      ftime->tm_hour = ((finfo->ftime)&0xf800)>>11;

      /* extract the minute from the modification time */
      ftime->tm_min = ((finfo->ftime)&0x07e0)>>5;

      /* extract the seconds from the modification time */
      ftime->tm_sec = ((finfo->ftime)&0x001f)*2;
   }
   
   /* return a pointer to the tm object */
   return ((ftime) ? ftime : NULL);
}

/*------------------------------------------------------------------------*/
/* function to initialize FILE objects                                    */
/*------------------------------------------------------------------------*/
/**
   This function makes sure that all of the FILE objects are initialized and
   valid. 
*/
static void fvalid(void)
{
   /* validate/initialize the file IO control structures */
   int i; for (i=0; i<NFIO; i++) {if (fstreamok(&fio[i])<=0) fioinit(&fio[i]);}
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _getpid(void) {LogEntry("_getpid()","Unimplemented.\n"); return -1;}

/*------------------------------------------------------------------------*/
/* test whether a file refers to a terminal                               */
/*------------------------------------------------------------------------*/
/**
   The _isatty() function tests whether an open file handle refers to
   a terminal. The function returns 1 if the handle refers to a TTY,
   otherwise zero is returned.
*/
int _isatty(int file)
{
   /* specify the standard streams as referring to a TTY device */
   int tty=1; if (file!=STDIN && file!=STDOUT && file!=STDERR) {tty=0; errno=ENOTTY;} 

   return tty;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _kill(int pid, int sig) {LogEntry("_kill()","Unimplemented\n"); return -1;}

/*------------------------------------------------------------------------*/
/* function to rename a file                                              */
/*------------------------------------------------------------------------*/
/**
   This function has the effect of renaming a file.  The NewLib libc
   library accomplished the task of renaming a file by first creating
   a link to a new name and then deleting the link to the old name by
   calling unlink().  This model is incompatible with that used by the
   FatFs file system which has a f_rename() function that accomplishes
   the rename operation directly.  This function also accomplishes the
   rename operation directly.  However, it also has the side effect of
   copying the old file name to a temporary buffer so that a potential
   subsequent call (by rename()) to _unlink() can emulate the
   operation of unlinking the old file name.

   This function returns zero on success or else EOF upon failure.
*/
int _link(const char *oldfname, const char *newfname)
{
   /* initialize the return value */
   int fid=EOF; errno=0;

   /* validate the old and new filenames */
   if (fnameok(oldfname)<=0 || fnameok(newfname)<=0) errno=EINVAL;

   /* make sure that neither filename is currently in use */
   else if (fiobusy(oldfname)>=0 || fiobusy(newfname)>=0) errno=EBUSY;

   /* make sure the new filename isn't already assigned to a file */
   else if (fioFind(newfname)>=0) errno=EACCES;

   /* make sure the old file exists */
   else if (fioFind(oldfname)<0) errno=ENOENT;

   /* rename the file */
   else if ((fid=fioRename(oldfname,newfname))<=0) {errno=ENOEXEC;}

   /* record the oldfilename for potential use by rename */
   else {strncpy(unlinkfname,oldfname,FILENAME_MAX);}

   /* return zero on success or EOF on failure */
   return ((fid>=0)?0:EOF);
}

/*------------------------------------------------------------------------*/
/* reposition the file pointer                                            */
/*------------------------------------------------------------------------*/
/**
   The lseek() function repositions the offset of the open file
   associated with the file descriptor fd to the argument offset
   according to the directive whence as follows:

      SEEK_SET: The offset is set to offset bytes.
      SEEK_CUR: The offset is set to its current location plus offset bytes.
      SEEK_END: The offset is set to the size of the file plus offset bytes.

   Upon successful completion, lseek() returns the resulting offset
   location as measured in bytes from the beginning of the file.  On
   error, the value (off_t) -1 is returned and errno is set to
   indicate the error.
*/
off_t _lseek(int file, off_t offset, int whence)
{
   /* initialize the return value */
   off_t where=-1; errno=0;
 
   /* can't use _lseek() on the stdio streams */
   if (file!=STDIN && file!=STDOUT && file!=STDERR)
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];

      /* validate the stream */
      if (fstreamok(stream)<=0) {errno=EBADF; fioinit(stream);} 

      /* make sure the stream is seekable */
      else if (stream->fdes<0 || stream->fdes>=NFIO) errno=EBADF;
   
      else
      {
         /* set a local pointer to the FioBlk object */
         FioBlk *fioblk=&fioblk_[stream->fdes];

         /* compute the new file position */
         switch (whence)
         {
            /* compute the offset relative to the beginning of the file */
            case SEEK_SET: {where=offset; break;}
            
            /* compute the offset relative to the current file position */
            case SEEK_CUR: {where=offset + fioblk->fpos; break;}
            
            /* compute the offset relative to the end of the file */
            case SEEK_END: {where=offset + fioblk->len; break;}

            /* invalid whence specifier */   
            default:       {errno=EINVAL;}
         }

         /* reposition the file pointer */
         if (where>=0 && where<=fioblk->len)
         {
            /* reposition the file pointer */
            fioblk->fpos=where;
            
            /* clear the IOEOF bits */
            streamerr_clear(stream,IOEOF);
         }
         
         /* attempt to seek beyond either end of the file */
         else {where=-1; errno=ERANGE;}
      }
   }

   /* _lseek() on stdio streams is not allowed */
   else {errno=ESPIPE;}
   
   return where;
}

/*------------------------------------------------------------------------*/
/* function to open a stream and associate it with a filename             */
/*------------------------------------------------------------------------*/
/**
   This function opens a file with a specified name and mode.  A call
   to _open() initializes File and FioBlk structures.  If the file
   already exists in the file system and if opened in read or append
   mode then the existing file is read into the FioBlk's buffer.  

   \begin{verbatim}
   input:

      fname....This is the pathname for the file to be opened.  Valid
               file names are determined by fnameok().

      flags....The argument flags must include one of the following
               access modes: O_RDONLY, O_WRONLY, or O_RDWR.  These
               request opening the file read-only, write-only, or
               read/write, respectively.  In addition, zero or more
               file creation flags and file status flags can be
               bitwise-or'd in flags.  The file creation flags are
               O_CREAT, O_TRUNC, and O_APPEND.

               The full list of file creation flags and file status
               flags is as follows:

                  O_APPEND: The file is opened in append mode.  Before
                            each write(2), the file offset is
                            positioned at the end of the file, as if
                            with lseek(2).

                  O_CREAT:  If the file does not exist, it will be
                            created.

                  O_TRUNC:  If the file already exists and is a regular
                            file and the access mode allows writing
                            (i.e., is O_RDWR or O_WRONLY) it will be
                            truncated to length 0.

   output:
      This function returns the new file descriptor, or -1 if an error
      occurred (in which case, errno is set appropriately).
   \end{verbatim}
*/
int _open(const char *fname, int flags, ...)
{
   /* initialize the return value */
   int fd=EOF; errno=0;

   /* define local work objects */
   File *stream=NULL;
   
   /* validate the file objects */
   fvalid();

   /* validate the function arguments */
   if (!fname) errno=EINVAL;

   /* validate the file name */
   else if (!fnameok(fname)) errno=ENXIO;

   else
   {
      /* make sure a file with the same name is not already open */
      if (fiobusy(fname)>=0) errno=EBUSY;
        
      /* open a file in append mode */
      else if (flags&O_APPEND) 
      {
         /* check if the file exists in the RAM file system */
         if ((fd=fioFind(fname))>=0 && fd<NFIO)
         {
            /* create a local pointer to the file IO block */
            FioBlk *fioblk = &fioblk_[fd]; fioblk->fpos=0;

            /* initialize the return value */
            stream = &fio[fd];

            /* initialize the file IO stream members */
            stream->fdes=fd; stream->err=0;
            stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOW;
            stream->crc=fstreamcrc(stream);
         }
         
         /* check if the file exists in the FatFs volume */
         else if (fd>=NFIO)
         {
            /* obtain a unused file descriptor */
            if ((fd=fdes())>=0)
            {
               /* create a local pointer to the file IO block */
               FioBlk *fioblk = &fioblk_[fd]; fioblk->fpos=0;

               /* read the file from the FatFs volume */
               if (fioGet(fname,fioblk)>0)
               {
                  /* initialize the return value */
                  stream = &fio[fd];

                  /* initialize the file IO stream members */
                  stream->fdes=fd; stream->err=0;
                  stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOW;
                  stream->crc=fstreamcrc(stream);
               }

               /* indicate failure to read the file from FatFs volume */
               else {errno=EIO; fd=EOF;}
            }

            /* set the errno to indicate that the RAM file system is full */
            else {errno=ENOSPC;}
         }

         /* file doesn't exist in FatFs - create a new file */
         else if ((fd=fdes())>=0)
         {
            /* create a local pointer to the file IO block */
            FioBlk *fioblk = &fioblk_[fd];

            /* initialize the file IO block members */
            fioblk->len=0; fioblk->fpos=0; fioblk->buf[0]=0;
            strncpy(fioblk->fname,fname,FILENAME_MAX);

            /* initialize the return value */
            stream = &fio[fd];

            /* initialize the file IO stream members */
            stream->fdes=fd; stream->err=0;
            stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOW;
            stream->crc=fstreamcrc(stream);
         }
 
         /* set the errno to indicate that the RAM file system is full */
         else {errno=ENOSPC; fd=EOF;}
      }
            
      /* open a file in read mode */
      else if (!(flags&(O_CREAT|O_TRUNC|O_APPEND)))
      {
         /* check if the file exists in the RAM file system */
         if ((fd=fioFind(fname))>=0 && fd<NFIO)
         {
            /* create a local pointer to the file IO block */
            FioBlk *fioblk = &fioblk_[fd]; fioblk->fpos=0;

            /* initialize the return value */
            stream = &fio[fd];

            /* initialize the file IO stream members */
            stream->fdes=fd; stream->err=0;
            stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOR;
            stream->crc=fstreamcrc(stream);
         }
         
         /* check if the file exists in the FatFs volume */
         else if (fd>=NFIO)
         {
            /* obtain a unused file descriptor */
            if ((fd=fdes())>=0)
            {
               /* create a local pointer to the file IO block */
               FioBlk *fioblk = &fioblk_[fd]; fioblk->fpos=0;

               /* read the file from the FatFs volume */
               if (fioGet(fname,fioblk)>0)
               {
                  /* initialize the return value */
                  stream = &fio[fd];

                  /* initialize the file IO stream members */
                  stream->fdes=fd; stream->err=0;
                  stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOR;
                  stream->crc=fstreamcrc(stream);
               }

               /* indicate failure to read the file from FatFs volume */
               else {errno=EIO; fd=EOF;}
            }

            /* set the errno to indicate that the RAM file system is full */
            else {errno=ENOSPC;}
         }

         /* indicate that the file doesn't exist */
         else errno=ENOENT;
      }

      /* open a file in write mode */
      else if (flags&O_TRUNC)
      {
         /* check if file exists in RAM file system or get new file descriptor */
         if (((fd=fioFind(fname))>=0 && fd<NFIO) || (fd=fdes())>=0)
         {
            /* create a local pointer to the file IO block */
            FioBlk *fioblk = &fioblk_[fd];
            
            /* initialize the file IO block members */
            fioblk->len=0; fioblk->fpos=0; fioblk->buf[0]=0;
            strncpy(fioblk->fname,fname,FILENAME_MAX);

            /* initialize the return value */
            stream = &fio[fd];

            /* initialize the file IO stream members */
            stream->fdes=fd; stream->err=0;
            stream->mode=(flags&O_RDWR) ? (IOR|IOW) : IOW;
            stream->crc=fstreamcrc(stream);
         }

         /* set the errno to indicate that the FatFs is full */
         else {errno=ENOSPC; fd=EOF;}
      }
      
      /* illegal in unimplemented mode */
      else errno=EINVAL;
   }

   /* adjust the file descriptor to become a file handle */
   if (fd>=0) fd+=NSTD;
   
   return fd;
}

/*------------------------------------------------------------------------*/
/* read a buffer from a file                                              */
/*------------------------------------------------------------------------*/
/**
   The _read() function attempts to read up to count bytes from file
   descriptor fd into the buffer starting at ptr.  On files that
   support seeking, the read operation commences at the current file
   offset, and the file offset is incremented by the number of bytes
   read.  If the current file offset is at or past the end of file, no
   bytes are read, and read() returns zero.  If count is zero, read()
   may detect the errors described below.

      EBADF:  The file handle is not a valid or is not open for reading.
      EIO:    I/O error. 
      EACCES: The file has not been opened with read mode enabled.
      EOF:    End of file.

   \begin{verbatim}
   input:
      file...The handle for the file from which data will be read.

   output:
      ptr....A pointer to the buffer into which the data will be
             stored.
      len....The number of bytes to read from the file.

      On success, the number of bytes read is returned (zero indicates
      end of file), and the file position is advanced by this number.
      It is not an error if this number is smaller than the number of
      bytes requested; this may happen for example because fewer bytes
      are actually available before end-of-file. On error, -1 is
      returned, and errno is set appropriately.
   \end{verbatim}
*/
int _read(int file, char *ptr, int len)
{
   /* initialize the return value */
   int n=-1; errno=0;

   /* read the buffer from the STDIN stream */
   if (file==STDIN) {for (n=0; n<len; n++) {(*ptr)=__io_getchar(); ptr++;}}

   /* validate the file handle */
   else if (file>=NSTD && file<NFIO+NSTD)
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];
 
      /* validate the stream */
      if (fstreamok(stream)<=0) {errno=EIO; fioinit(stream);} 
      
      /* validate the stream */
      else if (stream->fdes<0 || stream->fdes>=NFIO) errno=EBADF;
      
      /* check if the stream is read-enabled */
      else if (!(stream->mode&IOR)) errno=EACCES;

      else
      {
         /* create a local pointer to the FioBlk object */
         FioBlk *fioblk = &fioblk_[stream->fdes];
         
         for (n=0; n<len;)
         {
            /* check for EOF */
            if (fioblk_[stream->fdes].fpos>=fioblk_[stream->fdes].len) 
            {
               /* set the stream's EOF bit */
               streamerr_set(stream,IOEOF); break;
            }
            
            /* extract the next byte from the FioBlk buffer */
            else {(*ptr)=fioblk->buf[fioblk->fpos++]; ptr++; n++;}
         }
      }
   }
   
   /* indicate an invalid file descriptor */
   else  {errno=EIO;}

   return n;
}

/*------------------------------------------------------------------------*/
/* function to write buffered data to the FatFs volume                    */
/*------------------------------------------------------------------------*/
/**
  This function forces a write of all buffered data for the specified File
  stream.  The open status of the stream is unaffected.  If the stream
  argument is NULL then all open output streams are flushed.  Upon
  successful completion 0 is returned.  Otherwise, EOF is returned and the
  global variable errno is set to indicate the error.  EBADF indicates that
  'stream' is not an open/valid stream or is not open for writing.
*/
int sflush(File *stream)
{
   /* initialize the function's return value */
   int i,status=EOF;

   /* clear the IOEOF bit */
   if (stream) stream->err&=(~IOEOF);

   /* check to see if all streams should be flushed */
   if (!stream) 
   {
      /* loop through each FILE object */
      for (status=0,i=0; i<NFIO; i++)
      {
         /* associate the stream to the current FILE object & clear the IOEOF bit */
         stream=&fio[i]; streamerr_clear(stream,IOEOF);

         /* validate the stream and check its writeability */
         if (fstreamok(stream)>=0 && (stream->mode&IOW))
         {
            /* write the file to the FatFs volume */
            if (fioPut(&fioblk_[stream->fdes])<0) {status=EOF; errno=EIO;}
         }
      }
   }

   /* make sure the IO stream is writeable */
   else if (!(stream->mode&IOW)) errno=EBADF;

   /* write the stream buffers to the FatFs volume */
   else if (fioPut(&fioblk_[stream->fdes])>=0) status=0;

   /* indicate general write failure */
   else {errno=EIO;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _stat(const char *file, struct stat *st)
{
   LogEntry("_stat()","Unimplemented\n"); st->st_mode = S_IFCHR;

   return 0;
}

/*------------------------------------------------------------------------*/
/* function to clear a specified error bitmask                            */
/*------------------------------------------------------------------------*/
/**
   This function clears a specified bitmask on the error element of
   the File structure. 
*/
static void streamerr_clear(File *stream, unsigned char err)
{
   /* clear the specified error bits and recompute the CRC */
   stream->err&=(~err); stream->crc=fstreamcrc(stream);
}

/*------------------------------------------------------------------------*/
/* function to assert a specified error bitmask                           */
/*------------------------------------------------------------------------*/
/**
   This function asserts a specified bitmask on the error element of
   the File structure. 
*/
static void streamerr_set(File *stream, unsigned char err)
{
   /* assert the specified error bits and recompute the CRC */
   stream->err|=err; stream->crc=fstreamcrc(stream);
}

/*------------------------------------------------------------------------*/
/* validate a stream associated with a handle                             */
/*------------------------------------------------------------------------*/
/**
   This function validates a File stream associated with a user
   specified handle.  It validates the handle itself and then executes
   fstreamok() to validate the stream associated with the handle.

   This function returns a positive number if the stream is valid and
   zero if the stream is invalid.
*/
int streamok(int handle)
{
   /* initialize the return value */
   int status=0;

   /* validate the handle */
   if (handle>STDERR && handle<(NFIO+NSTD)) 
   {
      /* initialize the stream */
      File *stream=&fio[handle-NSTD];

      /* confirm that the handle matches the file descriptor */
      if (handle!=(stream->fdes+NSTD)) {status=0;}

      /* validate the stream associated with the handle */
      else {status = (fstreamok(stream)>0) ? 1 : 0;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _times(struct tms *buf) {LogEntry("_times()","Unimplemented\n"); return -1;}

/*------------------------------------------------------------------------*/
/* remove a file from the filesystem                                      */
/*------------------------------------------------------------------------*/
/**
   The unlink() function deletes a file with a specified pathname from
   the filesystem.  If that pathname was the last link to a file and no
   processes have the file open, the file is deleted and the space it
   was using is made available for reuse.

   \begin{verbatim}
   input:
      fname...The pathname for the file to be deleted.

   output:
      Upon successful completion 0 is returned.  Otherwise, EOF is
      returned and the global variable errno is set to indicate the
      error.
   \end{verbatim}
*/
int _unlink(const char *fname)
{
   /* initialize the return value */
   int fid=EOF; errno=0;

   /* guard against an invalid file name */
   if (fnameok(fname)<=0) errno=EINVAL;
   
   /* check if the file is open */
   else if (fiobusy(fname)>=0) errno=EBUSY;
      
   /* check if the file is already in the FatFs volume */
   else if (fioFind(fname)>=0) {fid=fioUnlink(fname);}

   /* check for emulation of Newlib's rename() function */
   else if (!strcmp(fname,unlinkfname)) {fid=NFIO;}
   
   /* indicate that no such file exists */
   else {errno=ENOENT;}

   /* initialize subsequent emulation of Newlib's rename() function */
   unlinkfname[0]=0;

   /* return zero on success or EOF on failure */
   return ((fid>=0)?0:EOF);
}

/*------------------------------------------------------------------------*/
/* function do-nothing stub for Newlib libc library                       */
/*------------------------------------------------------------------------*/
int _wait(int *status) {LogEntry("_wait()","Unimplemented\n"); errno=ECHILD; return -1;}

/*------------------------------------------------------------------------*/
/* write a buffer to a file                                               */
/*------------------------------------------------------------------------*/
/**
   The _write() function writes up to count bytes from the buffer
   pointed buf to the file referred to by the file handle. The number
   of bytes written may be less than count if, for example, there is
   insufficient space on the underlying physical medium.  Bytes are
   always appended to the end of the file.

      EBADF:  The file handle is not a valid or is not open for reading.
      ENOSPC: No space left in buffer or device.
      EACCES: The file has not been opened with write mode enabled.

   \begin{verbatim}
   input:
      file...The handle for the file into which the data will be
             written.

   output:
      ptr....A pointer to the buffer of data will be stored.
      len....The number of bytes to written to the file.

      Upon successful completion 0 is returned.  Otherwise, EOF is
      returned and the global variable errno is set to indicate the
      error.
   \end{verbatim}
*/
int _write(int file, char *ptr, int len)
{
   /* initialize the return value */
   int n=-1; errno=0;

   /* check for output to stdio output streams */
   if (file==STDOUT || file==STDERR)
   {
      /* write the buffer to 20mA current loop interface */
      for (n=0; n<len; n++) {__io_putchar(*ptr++);}
   }

   /* validate the file handle */
   else if (file>=NSTD && file<NFIO+NSTD)
   {
      /* initialize the stream */
      File *stream=&fio[file-NSTD];

      /* validate the stream */
      if (fstreamok(stream)<=0) {errno=EIO; fioinit(stream);} 

      /* validate the file descriptor */
      else if (stream->fdes<0 || stream->fdes>=NFIO) errno=EBADF;
   
      /* check if the stream is write-enabled */
      else if (stream->mode&IOW)
      {
         /* create a local pointer to the file control block */
         FioBlk *fioblk = &fioblk_[stream->fdes];

         for (n=0; n<len;)
         {
            /* guard against buffer overflow */
            if (fioblk->len<BUFSIZE)
            {
               /* write the character to the file buffer */
               fioblk->buf[fioblk->len++]=(unsigned char)(*ptr++);

               /* increment the number of bytes written to the stream */
               n++;
            }
         
            /* indicate that the file is full */
            else {errno=ENOSPC;  stream->err|=IOEOF; break;}
         }
      }
      
      /* indicate that the stream is not write-enabled */
      else {errno=EACCES;}
   }

   /* indicate an invalid file descriptor */
   else  {errno=EBADF;}

   /* return the number of bytes written to the stream */
   return n;
}

#endif /* FATSYS_C */
