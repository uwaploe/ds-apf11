#ifndef STM32F103FSMC_H
#define STM32F103FSMC_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the SwiftWare STM32F103 FSMC model
 * -------------------------------------------------
 *
 * This Stm32's Flexible Static Memory Controller (FSMC) is used to
 * access the Apf11's external SRAM.
 *
 * Note: At the time of the writing, I am using version 1.4.0 of the
 *       STM32Cube HAL firmare and version 4.19.0 of the STM32CubeMX
 *       configuration utility.  With this combination, there is a bug
 *       that causes a failure to generate MX_FSMC_Init() which should
 *       be responsible for configuring the Stm32's FSMC peripheral.
 *       That deficiency motivated the creation of this function.
 *       Moreover, I have found that the HAL implementation is overly
 *       complicated, unreliable, very buggy, and contains nasty
 *       implicit infinite loops.  I have deprecated its use.
 *
 * The Apf11 contains two blocks of SRAM.  The Stm32f103 includes 96kb
 * of internal SRAM that is volatile with respect to Stm32 resets and
 * standy mode.  That is, the 96kb of internal SRAM is powered-down
 * while the Stm32 is in standby mode and then reinitialized after a
 * wake or reset cycle.  However, the external SRAM is nonvolatile in
 * the sense that it remains permanently powered-up whenever power is
 * connected to the Apf11.  This fact is exploited to implement
 * variables and storage capacity that is persistent across resets and
 * power cylcles of the Apf11 controller.
 * 
 * The HiTech compiler that was used to build firmware for the Apf9
 * controller implemented a nonstandard nonportable 'persistent'
 * keyword that facilitated the retention of nonvolatile variables
 * across resets and power cycles of the Apf9.  The GCC ARM
 * cross-compiler used to build Apf11 firmware does not explicitly
 * implement such a keyword.  However, GCC does implement an even more
 * general '__attribute()' feature that is exploited to create
 * persistent storage with same properties as the Apf9.
 *
 * A new 'persistent' keyword can be invented using the C macro
 *
 *    #define persistent __attribute((section(".persistent")))
 *
 * which instructs the linker to locate the declared variable in the
 * ".persistent" section of memory.  The linker script can be used to
 * specify that the .persistent section should be located in the
 * external SRAM:
 *
 *    MEMORY
 *    {
 *       RAM (xrw)       : ORIGIN = 0x20000000, LENGTH = 96K
 *       PERSISTENT(xrw) : ORIGIN = 0x60000000, LENGTH = 1024K
 *       FLASH (rx)      : ORIGIN = 0x8000000,  LENGTH = 1024K
 *     }
 *
 * The compiler and linker can be instructed not to initialize the
 * .persistent segment during boot-up:
 *
 *    .persistent (NOLOAD) : ALIGN(4)
 *    {
 *       *(.persistent, .persistent*)
 *    } >PERSISTENT
 *
 * The combination of these three elements allows the firmware writer
 * to declare a persistent variable that will automatically be located
 * in the nonvolatile external SRAM and that will not be initialized
 * during power, wake, or reset cycles.  For example, the declaration:
 *
 *    persistent struct MissionParameters mission;
 *
 * creates a MissionParameters object external SRAM that is not
 * initialized during code start-up prior to execution of main().
 * This means that the 'mission' object will reside in nonvolatile
 * external SRAM and retain its contents across power, wake, and reset
 * cycles.
 *
 * IMPORTANT NOTE: By purpose and design, objects that are created in
 *    the .persistent section are never automatically initialized
 *    by the compiler.  By corollary, it is the firmware-writer's
 *    responsibility to ensure that persistent objects are explicitly
 *    and manually initialized prior to use; otherwise, the contents
 *    of such objects will be undefined and the firmware is likely to
 *    misbehave.
 *
 *    Moreover, declarations that include an explicit initialization
 *    such as: "persistent integer foo=42;" will not have the
 *    implied compile-time initialization applied.  However, the
 *    construct: "persistent integer foo; foo=42;" will have the
 *    intended effect.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103FsmcChangeLog "$RCSfile$  $Revision$   $Date$"

/* prototypes for functions with external linkage */
int Stm32FsmcConfig(void);
int Stm32FsmcDisable(void);
int Stm32FsmcEnable(void);

#endif /* STM32F103FSMC_H */
#ifdef STM32F103FSMC_C
#undef STM32F103FSMC_C

#include <stm32f1xx_hal.h>
#include <Stm32f103Gpio.h>

/* create a pointer to the FSMC BTCR registers */
static FSMC_Bank1_TypeDef *fsmc = FSMC_Bank1;

#define Bank (0)          /* use FSMC Bank-1             */
#define CReg (2*Bank + 0) /* offset for control register */
#define TReg (2*Bank + 1) /* offset for timing register  */

/* bit-masks used to configure the FSMC control register */
#define MemEn       (0x00000001LU) /* enable memory bank 1         */
#define MuxEn       (0x00000000LU) /* disable muxed addr/data      */
#define MemType     (0x00000000LU) /* specify SRAM                 */
#define BusWidth    (0x00000010LU) /* specify 16-bit data bus      */
#define NorFlashEn  (0x00000000LU) /* disable NOR flash            */
#define BurstEn     (0x00000000LU) /* disable BURST mode           */
#define WaitPol     (0x00000000LU) /* NWAIT not used               */
#define WrapMode    (0x00000000LU) /* disable wrap mode            */
#define WaitCfg     (0x00000000LU) /* NWAIT not used               */
#define WriteEn     (0x00001000LU) /* enable writing to SRAM       */
#define WaitEn      (0x00000000LU) /* NWAIT not used               */
#define ExtMode     (0x00000000LU) /* disable extended-mode timing */
#define AsyncWait   (0x00000000LU) /* NWAIT not used               */
#define CBurstRW    (0x00000000LU) /* disable BURST mode           */

/* assemble the bit-mask for the FSMC control register (sans MemEn & WriteEn) */
#define FsmcCReg ( CBurstRW |  AsyncWait |  ExtMode | WriteEn | \
                     WaitEn |    WaitCfg | WrapMode | WaitPol | \
                    BurstEn | NorFlashEn | BusWidth | MemType | \
                      MuxEn | MemEn                           )

/* define the reserved bits of the FSMC control register */
#define FsmcCRegReserved (0xfff00080LU)

/* bit-masks used to configure the FSMC timing register */
#define AddressSetup  (0x00000000LU) /* address set-up period      */
#define AddressHold   (0x00000010LU) /* address hold duration      */
#define DataSetUp     (0x00000100LU) /* data set-up duration       */
#define BusTurn       (0x00010000LU) /* pause for bus turn-around  */
#define ClockDiv      (0x00100000LU) /* define perod of FSMC clock */
#define DataLatency   (0x00000000LU) /* Not used for SRAM          */
#define AccessMode    (0x00000000LU) /* use access mode A          */

/* assemble the bit-mask for the FSMC timing register */
#define FsmcTReg ( AccessMode | DataLatency | ClockDiv | BusTurn | \
                    DataSetUp | AddressHold | AddressSetup       )

/* define the reserved bits of the FSMC timing register */
#define FsmcTRegReserved (0xc0000000LU) 

/*------------------------------------------------------------------------*/
/* configure the Stm32's FSMC to use the Apf11's external SRAM            */
/*------------------------------------------------------------------------*/
/**
   This function configures Stm32's Flexible Static Memory Controller
   (FSMC) to access the Apf11's persistent external SRAM.  The
   configuration contained herein applies specifically to the Cypress
   CY62167EV30 which provides a storage capacity of 1024kb.

   \begin{verbatim}
   output:
      On success, this function returns a positive value.  Zero
      indicates failure and a negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32FsmcConfig(void)
{
   /* initialize the function's return value */
   int status=1;
   
   /* disable the FSMC chip select to protect against corruption */  
   Stm32GpioConfig(GPIOD, CS_N_Pin, InFlt);

   /* configure the GPIO pins of Stm32:D to be SRAM data lines */
   Stm32GpioConfig(GPIOD, GPIO_PIN_0  | GPIO_PIN_1  | GPIO_PIN_8 | GPIO_PIN_9 |
                          GPIO_PIN_10 | GPIO_PIN_14 | GPIO_PIN_15, AOutPP);
   
   /* configure the GPIO pins of Stm32:E to be SRAM data lines */
   Stm32GpioConfig(GPIOE, GPIO_PIN_7  | GPIO_PIN_8  | GPIO_PIN_9  | GPIO_PIN_10 |
                          GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 |
                          GPIO_PIN_15, AOutPP);
   
   /* configure the GPIO pins of Stm32:F to be SRAM address lines */
   Stm32GpioConfig(GPIOF, GPIO_PIN_0  | GPIO_PIN_1 | GPIO_PIN_2  | GPIO_PIN_3  | 
                          GPIO_PIN_4  | GPIO_PIN_5 | GPIO_PIN_12 | GPIO_PIN_13 | 
                          GPIO_PIN_14 | GPIO_PIN_15, AOutPP);
   
   /* configure the GPIO pins of Stm32:D to be SRAM address lines */
   Stm32GpioConfig(GPIOD, GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13, AOutPP);
      
   /* configure the GPIO pins of Stm32:E to be SRAM address lines */
   Stm32GpioConfig(GPIOE, GPIO_PIN_3, AOutPP);

   /* configure the GPIO pins of Stm32:G to be SRAM address lines */
   Stm32GpioConfig(GPIOG, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | 
                          GPIO_PIN_4 | GPIO_PIN_5, AOutPP);
   
   /* configure the GPIO pins of Stm32:D to be NOE,NWE lines */  
   Stm32GpioConfig(GPIOD, GPIO_PIN_4 | GPIO_PIN_5, AOutPP);
   
   /* configure the GPIO pins of Stm32:E to be NBL0,NBL1 lines */  
   Stm32GpioConfig(GPIOE, GPIO_PIN_0 | GPIO_PIN_1, AOutPP);
   
   /* enable FSMC clock */
   __HAL_RCC_FSMC_CLK_ENABLE();

   /* clear all non-reserved bits of the FSMC control register */
   fsmc->BTCR[CReg] &= FsmcCRegReserved;

   /* apply the configuration to the FSMC control register */
   fsmc->BTCR[CReg] |= ((~FsmcCRegReserved) & FsmcCReg);

   /* clear all non-reserved bits of the FSMC timing register */
   fsmc->BTCR[TReg] &= FsmcTRegReserved;

   /* apply the configuration to the FSMC timing register */
   fsmc->BTCR[TReg] |= ((~FsmcTRegReserved) & FsmcTReg);

   /* disconnect NADV line (unused by SRAM controller) */
   __HAL_AFIO_FSMCNADV_DISCONNECTED();

   /* enable the FSMC chip select */  
   Stm32GpioConfig(GPIOD, CS_N_Pin, AOutPP);

   return status;
}

/*------------------------------------------------------------------------*/
/* disable Apf11's persistent SRAM to protect against memory corruption   */
/*------------------------------------------------------------------------*/
/**
   This function deselects the Apf11's CY62167EV30 persistent SRAM to
   protect against corruption during initialization and
   de-initialization phases.

   \begin{verbatim}
   output:
      On success, this function returns a positive value.  Zero
      indicates failure and a negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32FsmcDisable(void) 
{
   /* initialize the function's return value */
   int status=1;

   /* deselect the Apf11's persistent SRAM */  
   Stm32GpioConfig(GPIOD, CS_N_Pin, InFlt);
 
   return status;
}

/*------------------------------------------------------------------------*/
/* enable Apf11's persistent SRAM                                         */
/*------------------------------------------------------------------------*/
/**
   This function selects the Apf11's CY62167EV30 persistent SRAM.
   This function should be called only after the SRAM has been configured.

   \begin{verbatim}
   output:
      On success, this function returns a positive value.  Zero
      indicates failure and a negative return value indicates an
      exception was encountered.
   \end{verbatim}
*/
int Stm32FsmcEnable(void) 
{
   /* initialize the function's return value */
   int status=1;

   /* deselect the Apf11's persistent SRAM */  
   Stm32GpioConfig(GPIOD, CS_N_Pin, AOutPP);
 
   return status;
}

#endif /* STM32F103FSMC_C */
