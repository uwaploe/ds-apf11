#ifndef DISKIO_H
#define DISKIO_H (0x2000) 

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define diskioChangeLog "$RCSfile$  $Revision$  $Date$"

#include <ff.h>
#include <Stm32f103Sdio.h>

/* typedefs to improve readability */
typedef unsigned char  BYTE;
typedef unsigned char  DSTATUS;
typedef unsigned long  DWORD;
typedef unsigned short WORD;
typedef unsigned int   UINT;

/* Results of Disk Functions */
typedef enum {
   RES_OK = 0,    /* 0: Successful */
   RES_ERROR,     /* 1: R/W Error */
   RES_WRPRT,     /* 2: Write Protected */
   RES_NOTRDY,    /* 3: Not Ready */
   RES_PARERR     /* 4: Invalid Parameter */
} DRESULT;

/* Prototypes for disk control functions */
DSTATUS disk_initialize(BYTE pdrv);
DRESULT disk_ioctl(BYTE pdrv, BYTE cmd, void* buf);
DRESULT disk_read(BYTE pdrv, BYTE* buf, DWORD sector, UINT count);
DSTATUS disk_status(BYTE pdrv);
DRESULT disk_write(BYTE pdrv, const BYTE* buf, DWORD sector, UINT count);
DWORD   get_fattime(void);

/* Disk Status Bits (DSTATUS) */
#define STA_NOINIT      0x01  /* Drive not initialized */
#define STA_NODISK      0x02  /* No medium in the drive */
#define STA_PROTECT     0x04  /* Write protected */
#define STA_NOTRDY      0x08  /* Drive exists but not ready */

/* Generic command (Used by FatFs) */
#define CTRL_SYNC          0  /* Complete pending write process (needed at FF_FS_READONLY == 0) */
#define GET_SECTOR_COUNT   1  /* Get media size (needed at FF_USE_MKFS == 1)                    */
#define GET_SECTOR_SIZE    2  /* Get sector size (needed at FF_MAX_SS != FF_MIN_SS)             */
#define GET_BLOCK_SIZE     3  /* Get erase block size (needed at FF_USE_MKFS == 1)              */
#define CTRL_TRIM          4  /* Data no longer used (needed at FF_USE_TRIM == 1)               */

/* application specific commands (not used by FatFs) */
#define FAT_ISMOUNT        5  /* determine if the FatFs volume is already mounted               */
#define FAT_MOUNT          6  /* mount the FatFs volume                                         */
#define FAT_UMOUNT         7  /* unmount the FatFs volume                                       */
#define FAT_MKFS           8  /* create a new FatFs volume on the SD card                       */

/* external declaration of a global FATFS object */
extern FATFS fs;

/* external declaration of a global SdCardInfo object */
extern SdCardInfo sdcard;

#endif /* DISKIO_H */
#ifdef DISKIO_C
#undef DISKIO_C

#include <logger.h>
#include <string.h>
#include <time.h>

/* define a SdCardInfo object */
SdCardInfo sdcard;

/* define a FATFS object */
FATFS fs;

/* declarations of external functions used locally */
unsigned long int msTimer(unsigned long int msTimeRef);

/*-----------------------------------------------------------------------*/
/* initialize a FatFs drive                                              */
/*-----------------------------------------------------------------------*/
/**
   This function initializes the SD peripheral and makes it ready for
   general read/write operations.  This function needs to be under
   control of FatFs module. The application program must not call this
   function, or the FAT structure on the volume can be broken. To
   re-initialize the SD peripheral, use f_mount function instead.

   \begin{verbatim}
   input:
      pdrv.....Physical drive number to identify the target device.
               For the SwiftWare SD API on the Apf11 controller, this
               will always be zero.

   output:
      This function returns one of the following four values:
         RES_OK: The function succeeded.
         RES_ERROR: An error occured.
         RES_PARERR: The command code or parameter is invalid.
         RES_NOTRDY: The device has not been initialized. 
   \end{verbatim}
*/
DSTATUS disk_initialize(BYTE pdrv)
{
   /* define the logging signature */
   cc *FuncName="FatFs:disk_initialize()";

   /* initialize the return value */
   DSTATUS status=RES_NOTRDY;

   /* define the number of retries for the disk initializations */
   const int NRetry=3;
   
   /* define objects used locally */
   int n,err,value;

   /* validate the drive identifier */
   if (pdrv) {status=RES_PARERR;}

   /* retry loop for FatFs disk initialization */
   else for (n=0; n<NRetry; n++)
   {
      /* determine if the SD card is powered up */
      if ((value=Stm32f103SdIsPowered(&sdcard))<0)
      {
         /* log the error */
         LogEntry(FuncName,"Undetermined SDIO power. "
                  "[value=0x%08lx]\n",value);
      }

      /* power down the SD card */
      else if (value && (err=Stm32f103SdPowerDown(&sdcard))<=0)
      {
         /* log the error */
         LogEntry(FuncName,"Attempt to power-down the SD card failed. "
                  "[err=%d,value=0x%08lx]\n",err,value);
      }

      /* power up the SD card */
      else if ((err=Stm32f103SdPowerUp(&sdcard))<=0)
      {
         /* log the error */
         LogEntry(FuncName,"Attempt to power-up "
                  "the SD card failed. [err=%d]\n");
      }

      /* SD initialization successful */
      else {status=RES_OK; break;}
   }
   
   /* check criteria for making logentry */
   if (status==RES_OK && (debuglevel>3 || (debugbits&DISKIO_H)))
   {
      /* log the error */
      LogEntry(FuncName,"SD peripheral ready.\n");
   }
   
   return status;
}

/*-----------------------------------------------------------------------*/
/* miscellaneous SD IO control commands                                  */
/*-----------------------------------------------------------------------*/
/**
   The disk_ioctl function is called to control device specific
   features and miscellaneous functions other than general read/write.

   The FatFs module requires the five device independent commands as
   described below.  In addition to these five, optional commands
   could be implemented and used by application code exterior to the
   FatFs module.

   \begin{verbatim}
      CTRL_SYNC: Make sure that the device has finished pending write
         process. If the disk I/O module or storage device has a
         write-back cache, the cached data marked dirty must be
         written back to the media immediately. Since the SwiftWare SD
         API waits for the completion of each read/write operation
         then this function can immediately return RES_OK.

      GET_SECTOR_COUNT: Returns number of available sectors on the
         drive into the DWORD variable pointed by buf. This command
         is used by f_mkfs and f_fdisk function to determine the
         volume/partition size to be created. Required if
         FF_USE_MKFS==1.

      GET_SECTOR_SIZE: Returns sector size of the device into the WORD
         variable pointed by buf. Valid return values for this
         command are 512, 1024, 2048 and 4096. This command is
         required only if FF_MAX_SS>FF_MIN_SS. When
         FF_MAX_SS==FF_MIN_SS, this command is never used and the
         device must work at that sector size.

      GET_BLOCK_SIZE: Returns erase block size of the flash memory
         media in unit of sector into the DWORD variable pointed by
         buf. The allowable value is 1 to 32768 in power of 2. Return
         1 if the erase block size is unknown or non flash memory
         media. This command is used by only f_mkfs function and it
         attempts to align data area on the erase block
         boundary. Required at FF_USE_MKFS==1.

      CTRL_TRIM: Informs the device the data on the block of sectors
         is no longer needed and it can be erased. The sector block is
         specified by a DWORD array {<start sector>, <end sector>}
         pointed by buf. This is an identical command to Trim of ATA
         device. Nothing to do for this command if this funcion is not
         supported or not a flash memory device. FatFs does not check
         the result code and the file function is not affected even if
         the sector block was not erased well. This command is called
         on remove a cluster chain and in the f_mkfs
         function. Required at FF_USE_TRIM==1.

      FAT_ISMOUNT: This command determines if the FatFs volume is
         already mounted. The value RES_OK is returned if the FatFs
         volume is mounted while RES_NOTRDY is returned if the FatFs
         is not mounted.

      FAT_MOUNT: This command powers-up the SDIO periheral and mounts
         the FatFs volume to make it ready for use.  The value RES_OK
         is returned if the mount was successful while RES_ERROR is
         returned if the mount failed.  The return value of f_mount()
         is stored in the buf argument.

      FAT_UMOUNT: This command unmounts the FatFs volume and
         powers-down the SDIO peripheral.  The value RES_OK is
         returned if f_mount() was successful while RES_ERROR is
         returned if on failure.  The return value of f_mount() is
         stored in the buf argument.

      FAT_MKFS: This command creates a new FatFs volume on the SDIO
         card; any data previously stored on the SD card are
         permanently lost.  The return values are RES_OK on success or
         RES_ERROR on failure.  The return value of f_mkfs() is stored
         in the buf argument.

   input:
      pdrv....The drive identifier: [0,255].  For the SwiftWare SD API
              on the Apf11, there is only one SD peripheral and so
              this argument should always be zero.
      cmd.....The command to be executed is identified by the five
              macros described above as well as any additional
              optional commands that could be used by application code
              exterior to the FatFs module.
      buf.....This is a pointer to a buffer whose storage space is
              defined externally to this function.  It is used to pass
              data between this and the calling function.

   output:
      This function returns one of the following four values:
         RES_OK: The function succeeded.
         RES_ERROR: An error occured.
         RES_PARERR: The command code or parameter is invalid.
         RES_NOTRDY: The device has not been initialized. 
   \end{verbatim}
*/
DRESULT disk_ioctl(BYTE pdrv,BYTE cmd,void *buf)
{
   /* define the logging signature */
   cc *FuncName="FatFs:disk_ioctl()";

   /* initialize the return value */
   DSTATUS status=RES_NOTRDY;

   /* define a boolean object to record when FatFs volume is mounted */
   static unsigned char FatMounted=0;

   /* define objects need for timeout feature */
   uint32_t msTimeRef; const uint32_t msTimeOut=3000;

   /* create objects used locally */
   uint32_t cstatus; int32_t csize; int err;
      
   /* validate the drive identifier */
   if (pdrv) {status=RES_PARERR;}

   /* process commands that autoinitialize the SDIO peripheral */
   else if (cmd>=FAT_ISMOUNT) switch (cmd)
   {
      /* determine if the FatFs volume is already mounted */
      case FAT_ISMOUNT:
      {
         /* check if the FatFs volume is already mounted */
         if (FatMounted>0 && fs.fs_type>0 && Stm32f103SdIsPowered(&sdcard)>0) {status=RES_OK;}

         /* exit the switch statement */
         break;
      }

      /* power-up the SDIO peripheral and mount the FatFs volume */      
      case FAT_MOUNT:
      {
         /* initialize the return value in the function argument */
         if (buf) {(*((FRESULT *)buf))=FR_OK;}

         /* check if the FatFs volume is already mounted */
         if (FatMounted>0 && fs.fs_type>0 && Stm32f103SdIsPowered(&sdcard)>0) {status=RES_OK;}
      
         /* use f_mount() to power-up the SDIO peripheral and mount the FatFs volume */
         else if ((status=f_mount(&fs,"",1))!=FR_OK)
         {
            /* log the error */
            LogEntry(FuncName,"Mount failure for FatFs volume. [FatErr=%d]\n",status);
         
            /* store the return value of f_mount() in the function argument */
            if (buf) {(*((FRESULT *)buf))=status;}
         
            /* initialize the FATFS object and record the FatFs volume as unmounted */
            memset((void *)(&fs),0,sizeof(fs)); FatMounted=0; status=RES_ERROR; 
         }
      
         /* indicate success and initialize the return value in the function argument */
         else {FatMounted=1; status=RES_OK;}

         /* exit the switch statement */
         break;
      }

      /* unmount the FatFs volume and power-down the SDIO peripheral */
      case FAT_UMOUNT:
      {
         /* initialize the return value in the function argument */
         if (buf) {(*((FRESULT *)buf))=FR_OK;}
   
         /* unmount the FatFs volume */
         if ((status=f_mount(0,"",0))==FR_OK)  {status=RES_OK;}
         
         else
         {
            /* log the error */
            LogEntry(FuncName,"Failed to unmount FatFs volume. [FatErr=%d]\n",status);
            
            /* initialize the return value in the function argument */
            if (buf) {(*((FRESULT *)buf))=status;} status=RES_ERROR;
         }
      
         /* power down the SDIO peripheral */
         if ((err=Stm32f103SdPowerDown(&sdcard))<=0)
         {
            /* log the error */
            LogEntry(FuncName,"Failed to power-down the SDIO peripheral. [err=%d]\n",err);
         }
               
         /* initialize the FATFS object and record the FatFs volume as unmounted */
         memset((void *)(&fs),0,sizeof(fs)); FatMounted=0;

         /* exit the switch statement */
         break;
      }
      
      /* create a new FatFs volume */
      case FAT_MKFS:
      {
         /* work buffer needed by f_mkfs() */
         BYTE buff[2*FF_MAX_SS];

         /* create a new FatFs volume */
         if ((status=f_mkfs("",FM_FAT32,0,buff,sizeof(buff)))!=FR_OK)
         {
            /* log the error */
            LogEntry(FuncName,"Failed to create new FatFs "
                     "volume. [FatErr=%d]\n",status);
                        
            /* initialize the return value in the function argument */
            if (buf) {(*((FRESULT *)buf))=status;} status=RES_ERROR;
         }

         /* initialize the return value in the function argument */
         else {if (buf) {(*((FRESULT *)buf))=FR_OK;} status=RES_OK;}
      
         /* initialize the FATFS object and record the FatFs volume as unmounted */
         memset((void *)(&fs),0,sizeof(fs)); FatMounted=0;
         
         /* exit the switch statement */
         break;
      }
         
      /* catch unimplemented commands */
      default: {LogEntry(FuncName,"Unimplemented IOCTL command. "
                         "[cmd=%d]\n",cmd); status=RES_ERROR;}
   }
   
   /* determine if the SDIO peripheral is powered-up */
   else if ((err=Stm32f103SdIsPowered(&sdcard))>0)
   {
      /* acquire a ready-status from the SD card */      
      for (status=RES_OK,msTimeRef=msTimer(0); msTimer(msTimeRef)<msTimeOut;)
      {
         /* query the SD card for its status register */
         if (Stm32f103SdCardStatus(&sdcard,&cstatus)<=0) {status=RES_NOTRDY; break;}

         /* determine if the SD card is ready to receive commands */
         else if (cstatus&SdStatusReady) {break;}
      }

      /* select amongst available IOCTL commands */
      if (status==RES_OK) switch (cmd)
      {
         /* indicate that write operations are complete */
         case CTRL_SYNC: {break;}

         /* query the card for its block/sector count */   
         case GET_SECTOR_COUNT:
         {
            /* query the SD card for its size */
            if ((err=SdCsd(&sdcard,&csize,NULL,NULL,NULL,NULL,NULL,NULL,NULL))<=0)
            {
               /* log the failure */
               LogEntry(FuncName,"SD size query failed. [err=%d,csize=%d]\n",err,csize);

               /* initialize the size to zero */
               (*((DWORD *)buf))=0; status=RES_ERROR;
            }

            /* store the count of SD card blocks/sectors */
            else {(*((DWORD *)buf)) = (csize+1);}

            /* exit the switch statement */
            break;
         }

         /* query the sector size */
         case GET_SECTOR_SIZE: {(*((WORD *)buf))=sizeof(SdBlk); break;}

         /* query the sector size */
         case GET_BLOCK_SIZE: {(*((DWORD *)buf))=sizeof(SdBlk); break;}

         /* command to erase contiguous blocks */
         case CTRL_TRIM:
         {
            /* initialize a pointer to the DWORD array */
            DWORD *erase = (DWORD *)buf;
      
            /* erase a contiguous block of storage */
            if ((err=Stm32f103SdErase(&sdcard,erase[0],erase[1]))<=0) {status=RES_ERROR;}
      
            /* exit the switch statement */
            break;
         }
         
         /* catch unimplemented commands */
         default: {LogEntry(FuncName,"Unimplemented IOCTL command. "
                            "[cmd=%d]\n",cmd); status=RES_ERROR;}
      }

      /* log the failure to induce the SD card into a ready state */
      else {LogEntry(FuncName,"SD card not ready.\n"); status=RES_NOTRDY;}
   }

   /* check if the SDIO peripheral is not powered */
   else if (!err) {LogEntry(FuncName,"SDIO peripheral not powered-up.\n");}

   /* failed attempt to determine state of SDIO peripheral */
   else {LogEntry(FuncName,"Undetermined state of SDIO peripheral. "
                  "[err=%d]\n",err); status=RES_ERROR;}
   
   return status;
}

/*-----------------------------------------------------------------------*/
/* read blocks of data from the SD card into a memory buffer             */
/*-----------------------------------------------------------------------*/
/**
   This function reads 512-blocks of data from the SD card into a
   memory buffer.

   \begin{verbatim}
   input:
      pdrv.....Physical drive number to identify the target device.
               For the SwiftWare SD API on the Apf11 controller, this
               will always be zero.
      sector...The address of the first block to read from the SD
               card.  The address is expressed in terms of blocks (not
               bytes).  Sector 0 refers to the first 512-byte
               block of data, sector 1 refers to the second 512-byte
               block, and so on.
      count....The number of sectors to read.

   output:
      buf......Pointer to the memory buffer in which the data will be
               stored. The number of bytes read will be equal to the
               block size (512 bytes) multiplied by 'count' blocks.

      This function returns one of the following four values:
         RES_OK: The function succeeded.
         RES_ERROR: An error occured.
         RES_PARERR: The command code or parameter is invalid.
         RES_NOTRDY: The device has not been initialized. 
   \end{verbatim}
*/
DRESULT disk_read (BYTE pdrv, BYTE *buf, DWORD sector, UINT count)
{
   /* define the logging signature */
   cc *FuncName="FatFs:disk_read()";

   /* initialize the return value */
   DSTATUS status=RES_OK;
   
   /* validate the drive identifier */
   if (pdrv) {status=RES_PARERR;}

   /* read a contiguous chain of blocks from the SD card into the buffer */
   else if (Stm32f103SdBlockReadM(&sdcard,sector,count,(SdBlk *)buf)<=0)
   {
      /* indicate that an error occurred */
      status=RES_ERROR;
   }

   /* check criteria for logging a successful read operation */
   else if ((debuglevel>3 || (debugbits&DISKIO_H)))
   {
      /* log metadata of the read operation */
      LogEntry(FuncName,"Read %u blocks starting at address %lu from "
               "SD card to buffer 0x%08lx.\n",count,sector,buf);
   }

   return status;
}

/*-----------------------------------------------------------------------*/
/* function to determine the current status of a FatFs drive             */
/*-----------------------------------------------------------------------*/
/**
   This function determines the current status of a FatFs drive.  The
   current drive status is returned as a bitmask which indicates the
   presence of various status conditions as described below. FatFs
   refers only to STA_NOINIT and STA_PROTECT; the remaining bitmasks
   are not used by FatFs but could be used by non-FatFs application
   software.

   \begin{verbatim}
   STA_NOINIT: Indicates that the device has not been initialized and
      not ready to work. This flag is set on system reset, media
      removal or failure of disk_initialize function. It is cleared on
      disk_initialize function succeeded. Any media change that occurs
      asynchronously must be captured and reflect it to the status
      flags, or auto-mount function will not work correctly. If the
      system does not support media change detection, application
      program needs to explicitly re-mount the volume with f_mount
      function after each media change.

   STA_PROTECT: Indicates that the medium is write protected. This is
      always cleared at the drives without write protect function. Not
      valid if STA_NODISK is set.

   STA_NODISK: Indicates that no medium in the drive. This is always
      cleared at fixed disk drive. Note that FatFs does not refer this
      flag.

   STA_NOTRDY: Indicates that the FatFs drive exists but is not ready
      due to some sort of error condition.

   input:
      pdrv.....Physical drive number to identify the target device.
               For the SwiftWare SD API on the Apf11 controller, this
               will always be zero.

   output:
      This function returns a bitmask which indicates the various
      conditions described above.  For a FatFs disk that is normal and
      ready then this function will return zero.
   \end{verbatim}
*/
DSTATUS disk_status(BYTE pdrv)
{
   /* define the logging signature */
   cc *FuncName="FatFs:disk_status()";

   /* initialize the return value */
   DSTATUS status=0;

   /* define objects used locally */
   int err; uint32_t cstatus;
                  
   /* allow only the default drive (pdrv=0) */
   if (pdrv)
   {
      /* reinitialize the return value */
      status = (STA_NOINIT | STA_NODISK);

      /* log the error */
      LogEntry(FuncName,"Invalid FatFs volume. "
               "[pdrv=%d,status=0x%02x]\n",pdrv,status);
   }

   /* check if an SD card is holstered in the SD peripheral */
   else if ((err=Stm32f103SdCardDetect())<=0)
   {
      /* reinitialize the return value */
      status = (STA_NOINIT | STA_NODISK);

      /* log the error */
      LogEntry(FuncName,"SD card not detected. "
               "[err=%d,status=0x%02x]\n",err,status);
   }

   /* query the SD card for its status */
   else if ((err=Stm32f103SdCardStatus(&sdcard,&cstatus))>0)
   {
      /* check if the card is locked */
      if (cstatus&SdStatusCardLocked) {status|=STA_PROTECT;}

      /* check for card controller error */
      if (cstatus&SdStatusCcError) {status|=STA_NOTRDY;}

      /* check criteria for logging a warning message */
      if (status&(STA_PROTECT|STA_NOTRDY))
      {
         /* log a warning message */
         LogEntry(FuncName,"SD card detected but not ready. "
                  "[err=%d,cstatus=0x%08lx,status=0x%02x]\n",
                  err,cstatus,status);
      }

      /* check criteria for making logentry */
      else if ((debuglevel>3 || (debugbits&DISKIO_H)))
      {
         LogEntry(FuncName,"SD card detected and ready.\n");
      }
   }
   
   return status;
}

/*-----------------------------------------------------------------------*/
/* write blocks of data from a memory buffer to the SD card              */
/*-----------------------------------------------------------------------*/
/**
   This function writes 512-blocks of data from a memory buffer to the
   SD card.

   \begin{verbatim}
   input:
      pdrv.....Physical drive number to identify the target device.
               For the SwiftWare SD API on the Apf11 controller, this
               will always be zero.
      sector...The address of the first block to write to the SD
               card.  The address is expressed in terms of blocks (not
               bytes).  Sector 0 refers to the first 512-byte
               block of data, sector 1 refers to the second 512-byte
               block, and so on.
      count....The number of sectors to write.
      buf......Pointer to the memory buffer from which the data will
               be read. The number of bytes to write will be equal to
               the block size (512 bytes) multiplied by 'count'
               blocks.

   output:
      This function returns one of the following four values:
         RES_OK: The function succeeded.
         RES_ERROR: An error occured.
         RES_PARERR: The command code or parameter is invalid.
         RES_NOTRDY: The device has not been initialized. 
   \end{verbatim}
*/
DRESULT disk_write (BYTE pdrv, const BYTE *buf, DWORD sector, UINT count)
{
   /* define the logging signature */
   cc *FuncName="FatFs:disk_write()";

   /* initialize the return value */
   DSTATUS status=RES_OK;

   /* validate the drive identifier */
   if (pdrv) {status=RES_PARERR;}

   /* write a contiguous chain of blocks from the buffer to the SD card */
   else if (Stm32f103SdBlockWriteM(&sdcard,sector,count,(SdBlk *)buf)<=0)
   {
      /* indicate that an error occurred */
      status=RES_ERROR;
   }

   /* check criteria for logging a successful write operation */
   else if ((debuglevel>3 || (debugbits&DISKIO_H)))
   {
      /* log metadata of the read operation */
      LogEntry(FuncName,"Write %u blocks starting at address %lu to the "
               "SD card from buffer 0x%08lx.\n",count,sector,buf);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to encode the RTC date/time into a 32-bit word                */
/*------------------------------------------------------------------------*/
/**
   This function encodes the current RTC date/time into a 32-bit word
   according to the FatFs specification as described here:
      http://elm-chan.org/fsw/ff/doc/fattime.html

   \begin{verbatim}
      bit[31:25]: Years since 1980 (0..127, e.g. 37 for 2017)
      bit[24:21]: Month (1..12)
      bit[20:16]: Day of the month (1..31)
      bit[15:11]: Hour (0..23)
      bit[10:5]:  Minute (0..59)
      bit[4:0]:   Second / 2 (0..29, e.g. 25 for 50)

   output:
      This function returns the RTC date/time encoded according to the
      FatFs specification.  If the RTC is invalid then the encoded
      value for 01/01/1980:00:00:00 is returned.
   \end{verbatim}
*/
DWORD get_fattime (void)
{
   /* initialize the fat-time to 01/01/1980:00:00:00 */
   DWORD ftime=0x00210000LU;

   /* get the current time from the RTC */
   time_t Tnow=time(NULL);

   /* compute the broken-down time */
   const struct tm *now = gmtime(&Tnow);

   /* validate the broken-down time elements */
   if ((now->tm_year>=80 && now->tm_year<=207) &&
       (now->tm_mon>=0   && now->tm_mon<=11)   &&
       (now->tm_mday>=1  && now->tm_mday<=31)  &&
       (now->tm_hour>=0 && now->tm_hour<=23)   &&
       (now->tm_min>=0  && now->tm_min<=59)    &&
       (now->tm_sec>=0  && now->tm_sec<=59))
   {
      /* ftime[31:25]: years since 1980 */
      ftime = (now->tm_year-80)<<25;

      /* ftime[24:21]: month [Jan=1, Feb=2, ... Dec=12] */
      ftime |= (now->tm_mon+1)<<21;

      /* ftime[20:16]: day of month [1,31] */
      ftime |= (now->tm_mday)<<16;

      /* ftime[15:11]: hour [0,23] */
      ftime |= (now->tm_hour)<<11;

      /* ftime[10:5]: minutes [0,59] */
      ftime |= (now->tm_min)<<5;

      /* ftime[4:0]: seconds/2 [0,29] */
      ftime |= (now->tm_sec/2);
   }
   
   return ftime;   
}

#endif /* DISKIO_C */
