#ifndef AT25EEPROM_H
#define AT25EEPROM_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Description of the SwiftWare API for the AT25 eeprom
   ----------------------------------------------------

   The Apf11 controller is equipped with an Atmel AT25256 eeprom with
   a capacity of 256KB of nonvolatile storage.  The AT25 is connected
   to the Apf11's SPI2 bus and (according to the AT25256 datasheet)
   can communicate at up to 10MHz (at 2.5V).

   The datasheet for the AT25256 is lacking in detail and contains no
   examples.  This comment section is intended as an adjunct to
   flesh-out some important context and information that the data
   sheet lacks.

   The AT25 contains an 8-bit status register that is both readable
   and writable.  The status register contains 8-bits as described by:

      Bit7 Bit6 Bit5 Bit4 Bit3 Bit2 Bit1 Bit0
      WPEN    X    X    X  BP1  BP0  WEN \RDY   (X: Does not matter.)

   The write-enable (WEN) and ready (\RDY) bits are read-only bits,
   writing to them has no effect.  The write-protect-enable (WPEN) bit
   and BP{0,1} bits are both writable and readable.  Bits{4,5,6} are
   undefined and should be disregarded.  All eight bits are asserted
   during an internal write cycle.

        WPEN: When asserted, the WPEN bit enables hardware
              write-protection.  If cleared, then hardware
              write-protection is disabled.  However, the Apf11 ties
              the write-protect (\WP) pin high and so hardware
              write-protection has been permenantly disabled.  Hence,
              the WPEN bit in the status register has no effect on the
              Apf11.

        WEN:  When the AT25 is powered-up then the write-enable (WEN)
              bit is always zero in order to prevent modification of
              any writable element of the eeprom is inhibited. When
              the WREN opcode is executed then the WEN bit of the
              status register is asserted and modification of the AT25
              is permitted.

        \RDY: This bit is zero when the AT25 is ready to accept
              commands.  This bit is asserted while a write cycle is
              in progress.

     BP{0,1}: The two bits BP{0,1} configure write-protection of
              selected segments of eeprom storage according to the
              following table:

                   Level  BP1  BP0  ProtectedAddresses StatusWord
                 0  None    0    0  None                     0x00
                 1 (1/4)    0    1  0x6000-0x7fff            0x04
                 2 (1/2)    1    0  0x4000-0x7fff            0x08
                 3 (All)    1    1  0x0000-0x7fff            0x0c
           
   The AT25 implements six opcodes which represent the AT25's internal
   API:

      WREN: The ability to write to the AT25 is disabled when powered
            up.  The WREN command asserts the WEN bit of the status
            register. The WRSR & WRITE opcodes must be proceeded by
            WREN since both modify the eeprom.  This command requires
            no argument.

      WRDI: This command clears the WEN bit of the status register and
            inhibits the ability to modify the eeprom.  This command
            requires no argument.

      RDSR: This command reads the contents of the status register.
            Upon completion, any further MOSI transmissions are
            ignored (until CS is pulsed) so that a dummy opcode can be
            used to generate the SCK signal so that the 8-bit status
            register can be transmitted via the MISO pin.

      WRSR: Configure the status register with an 8-bit word.  Only
            the WPEN, BP{0,1} bits are writable.  However, since the
            Apf11 permanently disables hardware write-protection (ie.,
            the \WP pin is permanently forced high) then the WPEN bit
            has no effect.  Hence, configuring the AT25 is limited to
            selection of BP{0,1}.  This opcode must be followed by an
            8-bit argument that configures BP{0,1}.

      READ: This opcode allows the contents of the eeprom to be read.
            This command must be followed by a 15-bit address (ie.,
            MSB followed by LSB). Upon completion, any further
            MOSI transmissions are ignored (until CS is pulsed) so
            that dummy opcodes can be used to generate the SCK signal
            so that the 8-bit data word can be transmitted via the
            MISO pin.

     WRITE: This opcode allows the contents of the eeprom to be
            written.  This command must be followed by a 15-bit
            address (ie., MSB followed by LSB) and then by the 8-bit
            data word.

   Each of these six opcodes must be preceeded by a pulse of the
   chip-select (CS) signal.  The pulse is executed by first
   asserting and then clearing the CS signal.  

   The AT25 eeprom implements a paged storage model based on the
   least-significant 6-bits of the 15-bit storage address.  That is,
   the page is identified by the most-signficant 9-bits of the address
   while the page-address is identified by the least-significant
   6-bits. After each byte is written to or read from the eeprom, the
   6-bit page-address is internally incremented by one; the
   most-significant 9-bits will remain constant.  A single execution
   of the READ or WRITE opcode involves only one page; the
   page-address will wrap around from 0x3f to 0x00 and read/write
   operations will continue within the same page.  Hence, in order to
   write to or read from different pages then multiple executions of
   the READ/WRITE opcodes are required.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define at25eepromChangeLog "$RCSfile$  $Revision$  $Date$"

/* declarations for functions with external linkage */
int                At25Enable(void);
int                At25Disable(void);
int                At25Read(unsigned char *buf, const unsigned short int size, const unsigned short int address);
int                At25StatusRead(unsigned char *reg);
int                At25StatusWrite(unsigned char config);
int                At25Test(void);
int                At25Write(const unsigned char *buf, const unsigned short int size, const unsigned short int address);
unsigned char      EERead(unsigned short int addr);
float              EEReadFloat(unsigned short int addr);
long int           EEReadLong(unsigned short int addr);
unsigned short int EEReadWord(unsigned short int addr);
int                EETest(void);
int                EEWrite(unsigned char b, unsigned short int addr);
int                EEWriteFloat(float f, unsigned short int addr);
int                EEWriteLong(long int l, unsigned short int addr);
int                EEWriteWord(unsigned short int w, unsigned short int addr);

/* define the return states of the Amtel 25 eeprom */
extern const char At25Timeout;    /* Timeout period expired */
extern const char At25Congruency; /* Congruency violation */
extern const char At25Protect;    /* AT25 protection fault */
extern const char At25PageFault;  /* page over-run error */
extern const char At25Busy;       /* timeout error on SPI2 bus */
extern const char At25Config;     /* AT25 configuration fault */
extern const char At25SpiConfig;  /* SPI configuration fault */
extern const char At25Null;       /* NULL function argument */
extern const char At25Fail;       /* general failure */
extern const char At25Ok;         /* general success */

#endif /* AT25EEPROM_H */
#ifdef AT25EEPROM_C

#include <apf11.h>
#include <limits.h>
#include <logger.h>
#include <nan.h>
#include <stdlib.h>
#include <string.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Spi.h>

/* define the return states of the Amtel 25 eeprom */
const char At25Timeout    = -8; /* Timeout period expired */
const char At25Congruency = -7; /* Congruency violation */
const char At25Protect    = -6; /* AT25 protection fault */
const char At25PageFault  = -5; /* page over-run error */
const char At25Busy       = -4; /* timeout error on SPI2 bus */
const char At25Config     = -3; /* AT25 configuration fault */
const char At25SpiConfig  = -2; /* SPI configuration fault */
const char At25Null       = -1; /* NULL function argument */
const char At25Fail       =  0; /* general failure */
const char At25Ok         =  1; /* general success */

/* enumerate the SPI serial opcodes for the AT25 eeprom */
enum {WRSR=0x1,WRITE,READ,WRDI,RDSR,WREN};

/* define configuration parameters */
#define BaudRate (0x00)
#define Cr2      (0x00)
#define NoCrc    (0x00)

/* define page-based bitmasks */
#define PageSize (64)
#define PageAddr (0x003f)
#define PageMask (0x7fc0)
#define AddrMsb  (0x7f00)
#define AddrLsb  (0x00ff)

/* declarations for functions with nonexposed external linkage */
int At25CsPulse(unsigned char msec);

/* declarations for functions with static linkage */
static int At25IsBusy(unsigned char timeout);
static int At25ReadPage(unsigned char *buf, unsigned char size, unsigned short int address);
static int At25WritePage(const unsigned char *buf, unsigned char size, unsigned short int address);

/* declare external functions used locally */
unsigned long int HAL_GetTick(void);

/* define the protection boundaries of the AT25256 */
static const unsigned short int protected[]={0x8000U, 0x6000U, 0x4000U, 0x0000U};

/*------------------------------------------------------------------------*/
/* function to disable the AT25 eeprom and reinitialize the SPI2 bus      */
/*------------------------------------------------------------------------*/
/**
   This function disables the AT25 by first configuring it into a safe
   mode, deselect the eeprom device from the SPI2 bus, and
   reinitialize the SPI2 bus.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int At25Disable(void)
{
   /* define the logging signature */
   cc *FuncName="At25Disable()";
  
   /* initialize the return value */
   int err,status=At25Ok;
      
   /* deselect the AT25 eeprom */
   if ((err=Spi2Select(Spi2Dev))<=0)
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Attempt to deselect the AT25 eeprom failed. "
               "[err=%d]\n",err); status=At25Fail;
   }
   
   /* reinitialize the Stm32 SPI2 configuration */
   Stm32Spi2Init();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure the Stm32 to communicate with the AT25 eeprom    */
/*------------------------------------------------------------------------*/
/**
   This function configures the Stm32 to communicate with the AT25
   eeprom.  The SPI2 registers are configured, the eeprom's CS signal
   is cleared to select the eeprom for communication, and all other
   devices on the SPI2 bus are deselected.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end(verbatim}
*/
int At25Enable(void)
{
   /* define the logging signature */
   cc *FuncName="At25Enable()";
  
   /* initialize the return value */
   int err,status=At25Ok;

   /* configure the Stm32 for communication with the AT25 eeprom */
   if ((err=Stm32SpiConfig(SPI2,BaudRate,Cr2,NoCrc))<=0)
   {
      /* make a logentry of the configuration failure */
      LogEntry(FuncName,"Attempt to configure SPI2 failed. "
               "[err=%d]\n",err); status=At25SpiConfig;
   }

   /* select the AT25 eeprom for communication */
   else if ((err=Spi2Select(Spi2DevEeprom))<=0)
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Attempt to select the AT25 eeprom failed. "
               "[err=%d]\n",err); status=At25Fail;
   }

   /* enable power for the AT25 eeprom */
   else if ((err=Apf11_3p3vEnable())<=0)
   {
      /* make a logentry of the power-up failure */
      LogEntry(FuncName,"Power-up of AT25 eeprom failed. [err=%d]\n",err); status=At25Fail;
   }
   
   /* disable write protection */
   else if ((err=At25StatusWrite(0x0))<=0 && (debuglevel>=3 || debugbits&AT25EEPROM_H))
   {
      /* make a logentry of the selection failure */
      LogEntry(FuncName,"Warning: Attempt to disable write "
               "protection failed. [err=%d]\n",err); 
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the AT25 to determine if write-operation in progress */
/*------------------------------------------------------------------------*/
/**
   This function queries the AT25 to determine if write-operations are
   in-progress or not.  A time-out is implemented to limit the maximum
   period that operations will pause.

   \begin{verbatim}
   output:
      This function returns zero if the AT25 is not busy.  Otherwise,
      a positive value is returned.
   \end{verbatim}
*/
static int At25IsBusy(unsigned char timeout)
{
   /* initialize the return value */
   int err,status=1; 

   /* define some local work objects */
   unsigned long int To; unsigned char reg; 

   /* define the bitmask for the AT25's \RDY bit */
   #define BUSY (0x1)

   /* define the bitmask for the under-run bit of SPI2 */
   #define UNDERRUN (0x8)
   
   /* pulse the chip-select to initialize communcations with the AT25 */
   At25CsPulse(0);

   /* implement timeout loop to determine if AT25 is busy */                
   for (To=HAL_GetTick(); (HAL_GetTick()-To)<timeout;)
   {
      /* transmit opcode to read the status register & clear the Stm32's status flags */
      SPI2->DR=RDSR; Stm32SpiBusy(SPI2,timeout); reg=SPI2->DR; err=SPI2->SR; 

      /* transmit a dummy opcode to generate the SCK signal */
      SPI2->DR=0; Stm32SpiRxne(SPI2,timeout); reg=SPI2->DR; err=SPI2->SR;

      /* test if AT25 is still busy or under-run error on SPI2 */
      if (!(reg&BUSY) && !(err&UNDERRUN)) {status=0; break;}
   }

   #undef BUSY
   #undef UNDERRUN
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to pulse the AT25 eeprom's chip-select signal                 */
/*------------------------------------------------------------------------*/
/**
   This function pulses the AT25 eeprom's chip-select signal by first
   asserting and then clearing the signal.  After each of the assert
   and clear operations, a user-specified pause (0-255 milliseconds)
   is inserted; the pattern is assert, pause, clear, pause.  The end
   result is a square wave whose period is twice the user-specified
   pause.

   \begin{verbatim}
   input:
      msec....The number of milliseconds to pause after EACH of the
              assert and clear operations.  The total period of the
              square-wave pulse is twice this value.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int At25CsPulse(unsigned char msec) {return Spi2CsPulse(Spi2DevEeprom,msec);}

/*------------------------------------------------------------------------*/
/* function to read a buffer of data from the AT25 eeprom                 */
/*------------------------------------------------------------------------*/
/**
   This function reads a buffer of quasi-arbitrary size from the AT25
   eeprom starting at a quasi-arbitrary address.  The only constraints
   imposed on the size and address are that the unprotected segment of
   eeprom starting at the specified address must be large enough to
   accommodate the buffer.

   This function decomposes the buffer into page segments and reads
   them (with verification) to the eeprom.  All read attempts are
   inhibited if protection faults are detected.

   \begin{verbatim}
   input:
      buf.......The buffer of data to be read from the eeprom.

      size......The number of bytes to read starting at the specified
                address; this must not exceed the number of bytes
                remaining in the unprotected segment in the eeprom.

      address...The 15-bit address that identifies the starting point
                for read operations. 

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int At25Read(unsigned char *buf, const unsigned short int size, const unsigned short int address)
{
   /* define the logging signature */
   cc *FuncName="At25Read()";
  
   /* initialize the return value */
   int err,status=At25Ok; 

   /* compute an upper bound for number of pages to write */
   const unsigned short int pmax = size/PageSize + 2;
   
   /* local work object to record the AT25 configuration */
   unsigned char config; unsigned short int p,n,N,AddrLo,AddrHi,PageLo,PageHi;

   /* check if the At25 eeprom is already the active device on the SPI2 bus */
   if (Spi2Select(SpiDevQuery)!=Spi2DevEeprom) {At25Enable();}
   
   /* validate the function argument */
   if (!buf || !size) {LogEntry(FuncName,"NULL function argument: buf:0x%08lx "
                                "size:%d.\n",buf,size); status=At25Null;}
 
   /* read the AT25 configuration */
   else if ((err=At25StatusRead(&config))<=0)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Attempt to read AT25 eeprom status "
               "register failed. [err=%d,config=0x%02x]\n",
               err,config); status=At25Config;
   }

   /* loop to write each page of AT25 eeprom; pmax prevents infinite loop */
   else for (status=At25Ok, AddrHi=(address+size), AddrLo=address, p=0, n=0;
             n<size && p<pmax; AddrLo+=N, n+=N, p++)
   {
      /* compute current page address boundaries */
      PageLo = AddrLo&PageMask; PageHi = PageLo+PageSize;

      /* compute the number of bytes to write in the current page */
      N = (AddrHi>PageHi) ? (PageHi-AddrLo) :  (AddrHi-AddrLo);

      /* write the current page to AT25 eeprom */
      if ((err=At25ReadPage(buf+n, N, AddrLo))<=0)
      {
         /* make a logentry of the error */
         LogEntry(FuncName,"Attempt to read %d bytes starting at AT25 "
                  "address 0x%04x failed.\n",(AddrHi-AddrLo),AddrLo);

         /* initialize the return value and exit the write loop */
         status=At25Fail; break;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read one page to AT25 eeprom                               */
/*------------------------------------------------------------------------*/
/**
   This function reads one page (ie., up to 64 bytes) of data from the
   AT25 eeprom.  The AT25 eeprom implements a paged storage model as
   described in the SwiftWare API near the top of this file.  This
   function allows read operations to begin at any specified
   page-address and continue up to address 0x3f.  However,
   wrap-arounds from page-address 0x3f to 0x00 are detected and
   prohibited; the only way to read all sixty-four bytes of a given
   page is to start at page-address 0x00.

   \begin{verbatim}
   input:
      buf.......The buffer of data to be written into the page
                identified by the most significant 9-bits of the
                address.

      size......The number of bytes to write starting at the specified
                address; this must not exceed the number of bytes
                remaining in the 64-byte page.

      address...The 15-bit address that identifies the starting point
                for write operations. 

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
static int At25ReadPage(unsigned char *buf, unsigned char size, unsigned short int address)
{
   /* define the logging signature */
   cc *FuncName="At25ReadPage()";

   /* define the timeout period for SPI activity */
   const unsigned char timeout=10;
  
   /* initialize the return value */
   int err,status=At25Fail;

   /* local work object to record the AT25 configuration */
   unsigned char config;

   /* validate the function argument */
   if (!buf || !size) {LogEntry(FuncName,"NULL function argument: buf:0x%08lx "
                                "size:%d.\n",buf,size); status=At25Null;}
   
   /* make sure the write does not over-run the page boundary */
   else if (((address&PageAddr)+size)>PageSize)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Page over-run: address:0x%04x size:%d\n",
               address,size); status=At25PageFault;
   }
   
   /* pause until the SPI bus is not busy */
   else if ((err=Stm32SpiBusy(SPI2,timeout*2)))
   {
      /* make a logentry */
      LogEntry(FuncName,"Error or timeout(%dmsec) on SPI2 bus. "
               "[err=%d]\n",timeout,err); status=At25Busy;
   }
 
   /* read the AT25 configuration */
   else if ((err=At25StatusRead(&config))<=0)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Attempt to read AT25 eeprom status "
               "register failed. [err=%d,config=0x%02x]\n",
               err,config); status=At25Config;
   }
            
   else
   {
      /* define local work objects */
      unsigned char n;

      /* define the address MSB and LSB */
      const unsigned char msb=((address&AddrMsb)>>8), lsb=(address&AddrLsb);
      
      /* transmit the opcode to read from the AT25 eeprom */
      At25CsPulse(0); SPI2->DR=READ; Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=msb;  Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=lsb;  Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */                
      buf[0]=SPI2->DR; err=SPI2->SR;
                      
      /* read each byte from the eeprom page and compare to the buffer */                
      for (status=At25Ok, n=0; n<size; n++)
      {
         /* transmit a dummy opcode to generate the SCK signal */
         SPI2->DR=0; Stm32SpiRxne(SPI2,timeout); buf[n]=SPI2->DR; err=SPI2->SR;
      }
   }
   
   return status;
}
   
/*------------------------------------------------------------------------*/
/* function to read to the AT25 eeprom's status register                  */
/*------------------------------------------------------------------------*/
/**
   This function reads the AT25 eeprom's status register.  The format
   for the status register is:

      Bit7 Bit6 Bit5 Bit4 Bit3 Bit2 Bit1 Bit0
      WPEN    X    X    X  BP1  BP0  WEN \RDY   (X: Does not matter.)

   The WPEN bit enables/disables hardware write-protection; this
   facility has been disabled.  The WEN bit is '1' if that the AT25 is
   currently write-enabled.  The \RDY bit is '1' if the AT25 is
   currently NOT ready for programming.  The two bits BP{0,1}
   configure write-protection of selected segments of eeprom according
   to the following table:

        Level  BP1  BP0  ProtectedAddresses At25Status 
      0  None    0    0  None                     0x00
      1 (1/4)    0    1  0x6000-0x7fff            0x04
      2 (1/2)    1    0  0x4000-0x7fff            0x08
      3 (all)    1    1  0x0000-0x7fff            0x0c

   \begin{verbatim}
   output:
      reg...The current contents of the eeprom's status register.

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end(verbatim}
*/
int At25StatusRead(unsigned char *reg)
{
   /* define the logging signature */
   cc *FuncName="At25StatusRead()";
  
   /* initialize the return value */
   int err,status=At25Fail;

   /* define the timeout period for SPI activity */
   const unsigned char timeout=20;

   /* check if the At25 eeprom is already the active device on the SPI2 bus */
   if (Spi2Select(SpiDevQuery)!=Spi2DevEeprom) {At25Enable();}

   /* initialize the return value */
   if (reg) {(*reg)=0x71;}

   /* validate the function argument */
   if (!reg) {LogEntry(FuncName,"NULL function argument.\n"); status=At25Null;}

   /* pause until the SPI bus is not busy */
   else if ((err=Stm32SpiBusy(SPI2,timeout)))
   {
      /* make a logentry */
      LogEntry(FuncName,"Error or timeout(%dmsec) on SPI2 bus. "
               "[err=%d]\n",timeout,err);

      /* initialize the return value to indicate the exception */
      status=At25Busy;
   }
   
   else
   {
      /* clear rxne, error flags */
      (*reg)=SPI2->DR; err=SPI2->SR;

      /* transmit opcode to read the status register */
      At25CsPulse(0); SPI2->DR=RDSR; 

      /* wait for rxne flag and then clear rxne, error flags */
      Stm32SpiRxne(SPI2,timeout/2); (*reg)=SPI2->DR; err=SPI2->SR;

      /* transmit dummy opcode to generate SCK signal to receive response */
      SPI2->DR=0;

      /* wait for rxne flag and then read the AT25's response */
      if (Stm32SpiRxne(SPI2,timeout/2)>0) {(*reg)=SPI2->DR; status=At25Ok;}

      /* initialize the return value to indicate missing data */
      else {(*reg)=0x71;}

      /* initiate SPI transaction with CS-pulse */
      At25CsPulse(0);
   }
   
   return status;
}
   
/*------------------------------------------------------------------------*/
/* function to write to the AT25 eeprom's status register                 */
/*------------------------------------------------------------------------*/
/**
   This function configures the AT25 eeprom's status register to
   select write-protection of user-specified segments of the eeprom.
   The format for the status register is:

      Bit7 Bit6 Bit5 Bit4 Bit3 Bit2 Bit1 Bit0
      WPEN    X    X    X  BP1  BP0  WEN \RDY   (X: Does not matter.)

   For this function, only bits {2,3} are relevant since bits {0,1}
   are read-only and the WPEN bit is disabled.  The data sheet for the
   AT25256 eeprom provides the following table:

        Level  BP1  BP0  ProtectedAddresses At25Status
      0  None    0    0  None                     0x00
      1 (1/4)    0    1  0x6000-0x7fff            0x04
      2 (1/2)    1    0  0x4000-0x7fff            0x08
      3 (all)    1    1  0x0000-0x7fff            0x0c

   \begin{verbatim}
   input:
      config...The specified new value for the eeprom's status
               register.  Useful values are 0x0, 0x4, 0x8, 0xc. 

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end(verbatim}
*/
int At25StatusWrite(unsigned char config)
{
   /* define the logging signature */
   cc *FuncName="At25StatusWrite()";
  
   /* initialize the return value */
   int err,status=At25Fail;

   /* define the timeout period for SPI activity */
   const unsigned char timeout=15;

   /* check if the At25 eeprom is already the active device on the SPI2 bus */
   if (Spi2Select(SpiDevQuery)!=Spi2DevEeprom) {At25Enable();}

   /* pause until the SPI bus is not busy */
   if ((err=Stm32SpiBusy(SPI2,2*timeout)))
   {
      /* make a logentry */
      LogEntry(FuncName,"Error or timeout(%dmsec) on SPI2 bus. "
               "[err=%d]\n",2*timeout,err);

      /* initialize the return value to indicate the exception */
      status=At25Busy;
   }
   
   else
   {
      /* bitmask for the busy-status of the At25's status register */
      #define BUSY   (0x01)

      /* bitmask for the storage protection bits (B{0,1}) of the At25's status register */
      #define CONFIG (0x0c)

      /* define some local work objects for timeout and status confirmation */
      unsigned long int To; unsigned char reg;

      /* allow configuration only of BP0, BP1; disable WPEN */
      config &= CONFIG;

      /* transmit opcode to write-enable the At25 eeprom */
      At25CsPulse(0); SPI2->DR=WREN;   Stm32SpiBusy(SPI2,timeout);

      /* transmit opcode to write the status register with the configuration */
      At25CsPulse(0); SPI2->DR=WRSR;   Stm32SpiTxe(SPI2,timeout);
                      SPI2->DR=config; Stm32SpiBusy(SPI2,timeout);

      /* transmit opcode to write-disable the At25 eeprom */
      At25CsPulse(0); SPI2->DR=WRDI;   Stm32SpiBusy(SPI2,timeout);

      /* implement timeout loop to confirm the new configuration */                
      for (status=At25Timeout, To=HAL_GetTick(); (HAL_GetTick()-To)<3*timeout;)
      {
         /* request the contents of the At25 eeprom's status register */
         if (At25StatusRead(&reg)>0 && !(reg&BUSY))
         {
            /* verify that the At25's configuration matches the request */
            status = (config==(reg&CONFIG)) ? At25Ok : At25Fail;

            /* exit the confirmation loop */
            if (status>0) {break;}
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute a write/read test of the AT25 eeprom               */
/*------------------------------------------------------------------------*/
/**
   This function executes a write/read test of the AT25 eeprom. 

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end(verbatim}
*/
int At25Test(void)
{
   /* define the logging signature */
   cc *FuncName="At25Test()";

   /* initialize the return value */
   int err,status=At25Ok;

   /* define some local work objects */
   unsigned char read[100],write[100]; unsigned int pkt,m,n,dn;

   /* disable write protection of the AT25 eeprom */
   At25StatusWrite(0);
   
   /* initiate a logentry */
   LogEntry(FuncName,"write/read packet:");

   /* loop through each packet of AT25 eeprom storage capacity */
   for (err=1, pkt=0, n=0; n<protected[0] && status>0; n+=dn, pkt++)
   {
      /* compute the size of the packet to create */
      dn = ((n+sizeof(write))>protected[0]) ? (protected[0]-n) : sizeof(write);

      /* generate a packet of random bytes */
      for (m=0; m<dn; m++) {write[m] = random()%256;}

      /* initialize the packet to read */
      memset((void *)read,0,sizeof(read));

      /* write the packet to the AT25 eeprom */
      if (At25Write(write, dn, n)<=0) {LogEntry(FuncName,"Attempt to write packet at address "
                                                "0x%04x failed.\n",n); status=At25Fail;}

      /* read the packet back from the AT25 eeprom */
      else if (At25Read(read, dn, n)<=0) {LogEntry(FuncName,"Attempt to read packet at address "
                                                   "0x%04x failed.\n",n); status=At25Fail;}

      /* verify the write/read by comparing each byte in the two packets */
      for (m=0; m<dn; m++) {if (read[m]!=write[m]) {err=0;}}

      /* note the packet just completed */
      LogAdd(" %03d",pkt);
   }

   /* terminate the logentry */
   if (err>0) {LogAdd("\n");}

   /* make a logentry of the success or failure of the AT25 eeprom test */
   LogEntry(FuncName,"AT25 eeprom write/read test %s.\n",(err>0)?"successful":"failed");
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a buffer of data to the AT25 eeprom                  */
/*------------------------------------------------------------------------*/
/**
   This function writes a buffer of quasi-arbitrary size to the AT25
   eeprom starting at a quasi-arbitrary address.  The only constraints
   imposed on the size and address are that the unprotected segment of
   eeprom starting at the specified address must be large enough to
   accommodate the buffer.

   This function decomposes the buffer into page segments and writes
   them (with verification) to the eeprom.  All write attempts are
   inhibited if protection faults are detected.

   \begin{verbatim}
   input:
      buf.......The buffer of data to be written to the eeprom.

      size......The number of bytes to write starting at the specified
                address; this must not exceed the number of bytes
                remaining in the unprotected segment in the eeprom.

      address...The 15-bit address that identifies the starting point
                for write operations. 

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int At25Write(const unsigned char *buf, const unsigned short int size, const unsigned short int address)
{
   /* define the logging signature */
   cc *FuncName="At25Write()";
  
   /* initialize the return value */
   int err,status=At25Ok; 

   /* compute an upper bound for number of pages to write */
   const unsigned short int pmax = size/PageSize + 2;
   
   /* local work object to record the AT25 configuration */
   unsigned char config; unsigned short int p,n,N,AddrLo,AddrHi,PageLo,PageHi;

   /* check if the At25 eeprom is already the active device on the SPI2 bus */
   if (Spi2Select(SpiDevQuery)!=Spi2DevEeprom) {At25Enable();}

   /* validate the function argument */
   if (!buf || !size) {LogEntry(FuncName,"NULL function argument: buf:0x%08lx "
                                "size:%d.\n",buf,size); status=At25Null;}
 
   /* read the AT25 configuration */
   else if ((err=At25StatusRead(&config))<=0)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Attempt to read AT25 eeprom status "
               "register failed. [err=%d,config=0x%02x]\n",
               err,config); status=At25Config;
   }

   /* check for a protection fault */
   else if ((address+size)>protected[((config&0xc)>>2)])
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Protection fault: address:0x%04x protected:0x%04x\n",
               address,protected[((config&0xc)>>2)]); status=At25Protect;
   }

   /* loop to write each page of AT25 eeprom; pmax prevents infinite loop */
   else for (status=At25Ok, AddrHi=(address+size), AddrLo=address, p=0, n=0;
             n<size && p<pmax; AddrLo+=N, n+=N, p++)
   {
      /* compute current page address boundaries */
      PageLo = AddrLo&PageMask; PageHi = PageLo+PageSize;

      /* compute the number of bytes to write in the current page */
      N = (AddrHi>PageHi) ? (PageHi-AddrLo) :  (AddrHi-AddrLo);

      /* write the current page to AT25 eeprom */
      if ((err=At25WritePage(buf+n, N, AddrLo))<=0)
      {
         /* make a logentry of the error */
         LogEntry(FuncName,"Attempt to write %d bytes starting at AT25 "
                  "address 0x%04x failed.\n",(AddrHi-AddrLo),AddrLo);

         /* initialize the return value and exit the write loop */
         status=At25Fail; break;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write one page to AT25 eeprom                              */
/*------------------------------------------------------------------------*/
/**
   This function writes one page (ie., up to 64 bytes) of data to the
   AT25 eeprom.  The AT25 eeprom implements a paged storage model as
   described in the SwiftWare API near the top of this file.  This
   function allows write operations to begin at any specified
   page-address and continue up to address 0x3f.  However,
   wrap-arounds from page-address 0x3f to 0x00 are detected and
   prohibited; the only way to write all sixty-four bytes of a given
   page is to start at page-address 0x00.  

   After write operations are complete, this function verifies those
   operations by reading/comparing each byte from eeprom.

   \begin{verbatim}
   input:
      buf.......The buffer of data to be written into the page
                identified by the most significant 9-bits of the
                address.

      size......The number of bytes to write starting at the specified
                address; this must not exceed the number of bytes
                remaining in the 64-byte page.

      address...The 15-bit address that identifies the starting point
                for write operations. 

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
static int At25WritePage(const unsigned char *buf, unsigned char size, unsigned short int address)
{
   /* define the logging signature */
   cc *FuncName="At25WritePage()";

   /* define the timeout period for SPI activity */
   const unsigned char timeout=10;
  
   /* initialize the return value */
   int err,status=At25Fail;

   /* local work object to record the AT25 configuration */
   unsigned char config;

   /* validate the function argument */
   if (!buf || !size) {LogEntry(FuncName,"NULL function argument: buf:0x%08lx "
                                "size:%d.\n",buf,size); status=At25Null;}
   
   /* make sure the write does not over-run the page boundary */
   else if (((address&PageAddr)+size)>PageSize)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Page over-run: address:0x%04x size:%d\n",
               address,size); status=At25PageFault;
   }
   
   /* pause until the SPI bus is not busy */
   else if ((err=Stm32SpiBusy(SPI2,timeout*2)))
   {
      /* make a logentry */
      LogEntry(FuncName,"Error or timeout(%dmsec) on SPI2 bus. "
               "[err=%d]\n",timeout,err); status=At25Busy;
   }
 
   /* read the AT25 configuration */
   else if ((err=At25StatusRead(&config))<=0)
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Attempt to read AT25 eeprom status "
               "register failed. [err=%d,config=0x%02x]\n",
               err,config); status=At25Config;
   }

   /* check for a protection fault */
   else if (address>protected[((config&0xc)>>2)])
   {
      /* make a logentry of the error */
      LogEntry(FuncName,"Protection fault: address:0x%04x protected:0x%04x\n",
               address,protected[((config&0xc)>>2)]); status=At25Protect;
   }
            
   else
   {
      /* define local work objects */
      unsigned char n,byte;

      /* define the address MSB and LSB */
      const unsigned char msb=((address&AddrMsb)>>8), lsb=(address&AddrLsb);
      
      /* transmit opcode to write-enable the AT25 eeprom */
      At25CsPulse(0); SPI2->DR=WREN; Stm32SpiBusy(SPI2,timeout);

      /* transmit the opcode to write to the AT25 eeprom */
      At25CsPulse(0); SPI2->DR=WRITE; Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=msb;   Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=lsb;   Stm32SpiBusy(SPI2,timeout);

      /* write each byte of the buffer to eeprom page */                
      for (n=0; n<size; n++) {SPI2->DR=buf[n]; Stm32SpiBusy(SPI2,timeout);}

      /* terminate the page-write operation and wait until AT25 is not busy */
      At25CsPulse(0); At25IsBusy(timeout); 

      /* transmit opcode to write-enable the AT25 eeprom */
      At25CsPulse(0); SPI2->DR=WREN; Stm32SpiBusy(SPI2,timeout);

      /* transmit the opcode to read from the AT25 eeprom */
      At25CsPulse(0); SPI2->DR=READ; Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=msb;  Stm32SpiBusy(SPI2,timeout);
                      SPI2->DR=lsb;  Stm32SpiBusy(SPI2,timeout);

      /* clear the Stm32's status flags */                
      byte=SPI2->DR; err=SPI2->SR;
                      
      /* read each byte from the eeprom page and compare to the buffer */                
      for (status=At25Ok, n=0; n<size; n++)
      {
         /* transmit a dummy opcode to generate the SCK signal */
         SPI2->DR=0; Stm32SpiRxne(SPI2,timeout); byte=SPI2->DR; err=SPI2->SR;

         /* confirm congruency */
         if (byte!=buf[n])
         {
            /* initialize the return value to indicate the congruency violation */
            status=At25Congruency;

            /* check criteria for logentry */
            if (debuglevel>=4 || debugbits&AT25EEPROM_H)
            {
               /* make a logentry */
               LogEntry(FuncName,"Congruency violation at 0x%04x: 0x%02x!=0x%02x "
                        "(expected).\n",address+n,byte,buf[n]); 
            }
         }
      }

      /* transmit opcode to write-disable the At25 eeprom */
      At25CsPulse(0); SPI2->DR=WRDI;   Stm32SpiBusy(SPI2,timeout);

      /* check for congruency violation */
      if (status==At25Congruency)
      {
         /* make a logentry */
         LogEntry(FuncName,"Congruency violation detected on AT25 page "
                  "starting at address: 0x%04x.\n",(address&PageMask));
      }

      /* check for other page-write failure */
      else if (status<At25Ok)
      {
         /* make a logentry */
         LogEntry(FuncName,"Write failure on AT25 page starting at address: "
                  "0x%04x. [status=%d]\n",(address&PageMask),status);
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from a specified address of the EEPROM         */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns a single byte from a specified address of
   the EEPROM.
*/
unsigned char EERead(unsigned short int addr)
{
   unsigned char byte;

   /* read one byte from the AT25 eeprom */
   if (At25Read(&byte,1,addr)<=0) {byte=0xff;}
   
   return byte;
}

/*------------------------------------------------------------------------*/
/* function to read a float from a specified address of the EEPROM        */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns a float from a specified address of the
   EEPROM.
*/
float EEReadFloat(unsigned short int addr)
{
   float f;
   
   /* read one float from the AT25 eeprom */
   if (At25Read((unsigned char *)(&f), sizeof(f), addr)<=0) {f=NaN();}
   
   return f;
}

/*------------------------------------------------------------------------*/
/* function to read a long integer from a specified address of the EEPROM */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns a long integer from a specified address of
   the EEPROM.
*/
long int EEReadLong(unsigned short int addr)
{
   long int l;
   
   /* read one long int from the AT25 eeprom */
   if (At25Read((unsigned char *)(&l), sizeof(l), addr)<=0) {l=LONG_MAX;}

   return l;
}

/*------------------------------------------------------------------------*/
/* function to read a long integer from a specified address of the EEPROM */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns a long integer from a specified address of
   the EEPROM.
*/
unsigned short int EEReadWord(unsigned short int addr)
{
   unsigned short int w;
   
   /* read one unsigned int from the AT25 eeprom */
   if (At25Read((unsigned char *)(&w), sizeof(w), addr)<=0) {w=USHRT_MAX;}

   return w;
}

/*------------------------------------------------------------------------*/
/* function to write a test pattern to EEPROM and verify by rereading     */
/*------------------------------------------------------------------------*/
/**
   This function tests the EEPROM by first writing a test pattern and then
   rereading and verifying the pattern.
*/
int EETest(void) {return At25Test();}

/*------------------------------------------------------------------------*/
/* function to read a byte from a specified address of the EEPROM         */
/*------------------------------------------------------------------------*/
/**
   This function reads and returns a single byte from a specified address of
   the EEPROM.
*/
int EEWrite(unsigned char b, unsigned short int addr)
{
   /* write one byte to the AT25 eeprom */
   return At25Write(&b,1,addr);
}

/*------------------------------------------------------------------------*/
/* function to write a float to a specified address of the EEPROM         */
/*------------------------------------------------------------------------*/
/**
   This function writes a float to a specified address of the EEPROM.
*/   
int EEWriteFloat(float f, unsigned short int addr)
{
   /* write the float to the AT25 eeprom */
   return At25Write((unsigned char *)(&f), sizeof(f), addr);
}

/*------------------------------------------------------------------------*/
/* function to write a long integer to a specified address of the EEPROM  */
/*------------------------------------------------------------------------*/
/**
   This function writes a long integer to a specified address of the EEPROM.
*/   
int EEWriteLong(long int l, unsigned short int addr)
{
   /* write the long int to the AT25 eeprom */
   return At25Write((unsigned char *)(&l), sizeof(l), addr);
}

/*------------------------------------------------------------------------*/
/* function to write a long integer to a specified address of the EEPROM  */
/*------------------------------------------------------------------------*/
/**
   This function writes a long integer to a specified address of the EEPROM.
*/   
int EEWriteWord(unsigned short int w, unsigned short int addr)
{
   /* write the unsigned int to the AT25 eeprom */
   return At25Write((unsigned char *)(&w), sizeof(w), addr);
}
   
#endif /* AT25EEPROM_C */
