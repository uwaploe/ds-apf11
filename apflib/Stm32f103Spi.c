#ifndef STM32F103SPI_H
#define STM32F103SPI_H (0x0040U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
   Description of the SwiftWare STM32F103 SPI model
   ------------------------------------------------

   The SwiftWare STM32F103 SPI model is fundamentally based on the
   fact that there are six serial DUARTs on the Apf11's SPI1 bus and
   four GPIO/EEPROM devices on the Apf11's SPI2 bus.  At most, only
   one device can be selected on each SPI bus at any given time;
   daisy-chained SPI devices are not permitted.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define Stm32f103SpiChangeLog "$RCSfile$  $Revision$  $Date$"

#include <stm32f103xg.h>

/*------------------------------------------------------------------------*/
/* define the Stm32f103 SPI registers                                     */
/*------------------------------------------------------------------------*/
/**
   The SpiBus is defined by the following 32-bit registers in the Stm32:
      volatile unsigned long int CR1;
      volatile unsigned long int CR2;
      volatile unsigned long int SR;
      volatile unsigned long int DR;
      volatile unsigned long int CRCPR;
      volatile unsigned long int RXCRCR;
      volatile unsigned long int TXCRCR;
      volatile unsigned long int I2SCFGR;
      volatile unsigned long int I2SPR;
*/
typedef SPI_TypeDef SpiBus;

/*------------------------------------------------------------------------*/
/* enumerate the devices implemented on busses SPI1 and SPI2              */
/*------------------------------------------------------------------------*/
/**
   This is an enumeration of devices on the Apf11's two SPI busses (SPI{1,2}).

      SpiDevQuery:      This is a pseudo-device that, when passed as the
                        argument of Spi{1,2}Select(), will query for
                        the currently active device.

      Spi1Dev:          This indicates that all devices on SPI1 are deselected.
      Spi1DevSpDuart:   This devices is the DUART for spare sensors.
      Spi1DevRfDuart:   This device is the DUART for the RF port.
      Spi1DevExpDuart1: This device is DUART1 of the expansion port.
      Spi1DevExpDuart2: This device is DUART2 of the expansion port.
      Spi1DevExpDuart3: This device is DUART3 of the expansion port.
      Spi1DevExpDuart4: This device is DUART4 of the expansion port.

      Spi2Dev:          This indicates that all devices on SPI2 are deselected.
      Spi2DevEeprom:    This device is the EEPROM.
      Spi2DevSpGpio:    This device is the GPIO for the spare sensors.
      Spi2DevRfGpio:    This device is the GPIO for the RF port.
      Spi2DevExpGpio:   This device is the GPIO for the expansion port.

      SpiDevNone:       This marks the upper end of the enumeration.
      SpiDevInvalid:    This indicates that the SPI device is somehow invalid.
*/
typedef enum {SpiDevQuery=0, Spi1Dev=2, Spi1DevSpDuart, Spi1DevRfDuart, Spi1DevExpDuart1,
              Spi1DevExpDuart2, Spi1DevExpDuart3, Spi1DevExpDuart4,
              Spi2Dev, Spi2DevEeprom, Spi2DevSpGpio, Spi2DevRfGpio, Spi2DevExpGpio,
              SpiDevNone, SpiDevInvalid=-2} SpiDevice;

/* declarations for functions with external linkage */
int Spi1CsAssert(SpiDevice dev);
int Spi1CsClear(SpiDevice dev);
int Spi1CsPulse(SpiDevice dev, unsigned char millisec);
int Spi1Select(SpiDevice dev);
int Spi2CsPulse(SpiDevice dev, unsigned char millisec);
int Spi2Select(SpiDevice dev);
int Stm32Spi1Init(void);
int Stm32Spi2Init(void);
int Stm32SpiBusy(SpiBus *bus, unsigned char millisec);
int Stm32SpiConfig(SpiBus *bus, unsigned int cr1, unsigned int cr2, unsigned int crcpoly);
int Stm32SpiDisable(SpiBus *bus);
int Stm32SpiEnable(SpiBus *bus);
int Stm32SpiIsValidBus(SpiBus *bus);
int Stm32SpiRxne(SpiBus *bus, unsigned char millisec);
int Stm32SpiTxe(SpiBus *bus, unsigned char millisec);

/* define the return states of the Stm32 SPI API */
extern const char SpiInvalid; /* invalid parameter */
extern const char SpiBusy;    /* SPI bus busy */
extern const char SpiNull;    /* NULL function argument */
extern const char SpiFail;    /* general failure */
extern const char SpiOk;      /* general success */

#endif /* STM32F103SPI_H */
#ifdef STM32F103SPI_C
#undef STM32F103SPI_C

#include <apf11.h>
#include <logger.h>
#include <stm32f1xx.h>
#include <stm32f1xx_hal_rcc.h>
#include <Stm32f103Gpio.h>

/* define the return states of the Stm32 SPI API */
const char SpiInvalid = -3; /* invalid parameter */
const char SpiBusy    = -2; /* SPI bus busy */
const char SpiNull    = -1; /* NULL function argument */
const char SpiFail    =  0; /* general failure */
const char SpiOk      =  1; /* general success */

/* define bitmasks for CR2 */
#define SPI_CR2_RESRV (0x0018U)

/* declare external functions used locally */
unsigned long int HAL_GetTick(void);

/* declarations for functions with nonexposed external linkage */
int Stm32SpiInit(void);

/*------------------------------------------------------------------------*/
/* function to assert the chip-select signal on a device on the SPI1 bus  */
/*------------------------------------------------------------------------*/
/**
   This function asserts the chip-select (CS) signal on a device
   attached to the SPI1 bus.  

   \begin{verbatim}
   input:
      dev.........The ID of one of the devices attached to the SPI1
                  bus.  The valid SpiDevice IDs are enumerated above
                  in the header section of this module.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Spi1CsAssert(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Spi1CsAssert()";
   
   /* initialize the return value */
   int status=SpiOk;

   /* define local work objects */
   GpioPort *port=NULL; unsigned int pin=0;

   /* select the port and pin for the specified device on SPI1 bus */
   switch (dev)
   {
      case Spi1DevSpDuart:   {port=SPARE_DUART_SPI_CS_GPIO_Port; pin=SPARE_DUART_SPI_CS_Pin; break;}
      case Spi1DevRfDuart:   {port=RF_DUART_SPI_CS_GPIO_Port;    pin=RF_DUART_SPI_CS_Pin;    break;}
      case Spi1DevExpDuart1: {port=EXP_DUART1_SPI_CS_GPIO_Port;  pin=EXP_DUART1_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart2: {port=EXP_DUART2_SPI_CS_GPIO_Port;  pin=EXP_DUART2_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart3: {port=EXP_DUART3_SPI_CS_GPIO_Port;  pin=EXP_DUART3_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart4: {port=EXP_DUART4_SPI_CS_GPIO_Port;  pin=EXP_DUART4_SPI_CS_Pin;  break;}
      default: {LogEntry(FuncName,"Device(%d) does not have a chip-select signal.\n",dev); status=SpiInvalid;}
   }

   /* check if the port assignment was successful */
   if (port && status>0)
   {
      /* assert the port's CS signal */
      if (Stm32GpioAssert(port,pin)<=0) {status=SpiFail;}

      /* make a logentry if the CS pulse failed */
      if (status<=0) {LogEntry(FuncName,"Attempt to assert chip-select failed "
                               "for SPI1 device %d.\n",dev);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear the chip-select signal on a device on the SPI1 bus   */
/*------------------------------------------------------------------------*/
/**
   This function clears the chip-select (CS) signal on a device
   attached to the SPI1 bus.  

   \begin{verbatim}
   input:
      dev.........The ID of one of the devices attached to the SPI1
                  bus.  The valid SpiDevice IDs are enumerated above
                  in the header section of this module.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Spi1CsClear(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Spi1CsClear()";
   
   /* initialize the return value */
   int status=SpiOk;

   /* define local work objects */
   GpioPort *port=NULL; unsigned int pin=0;

   /* select the port and pin for the specified device on SPI1 bus */
   switch (dev)
   {
      case Spi1DevSpDuart:   {port=SPARE_DUART_SPI_CS_GPIO_Port; pin=SPARE_DUART_SPI_CS_Pin; break;}
      case Spi1DevRfDuart:   {port=RF_DUART_SPI_CS_GPIO_Port;    pin=RF_DUART_SPI_CS_Pin;    break;}
      case Spi1DevExpDuart1: {port=EXP_DUART1_SPI_CS_GPIO_Port;  pin=EXP_DUART1_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart2: {port=EXP_DUART2_SPI_CS_GPIO_Port;  pin=EXP_DUART2_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart3: {port=EXP_DUART3_SPI_CS_GPIO_Port;  pin=EXP_DUART3_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart4: {port=EXP_DUART4_SPI_CS_GPIO_Port;  pin=EXP_DUART4_SPI_CS_Pin;  break;}
      default: {LogEntry(FuncName,"Device(%d) does not have a chip-select signal.\n",dev); status=SpiInvalid;}
   }

   /* check if the port assignment was successful */
   if (port && status>0)
   {
      /* clear the port's CS signal */
      if (Stm32GpioClear(port,pin)<=0) {status=SpiFail;}

      /* make a logentry if the CS pulse failed */
      if (status<=0) {LogEntry(FuncName,"Attempt to clear chip-select failed "
                               "for SPI1 device %d.\n",dev);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to pulse the chip-select signal on a device on the SPI1 bus   */
/*------------------------------------------------------------------------*/
/**
   This function pulses the chip-select (CS) signal on a device
   attached to the SPI1 bus.  The device's CS is asserted then cleared
   to form a square wave pulse.  After each of the assert and clear
   operations, a user-specified pause (0-255 milliseconds) is
   inserted; the pattern is assert, pause, clear, pause.  The end
   result is a square wave whose period is twice the user-specified
   pause.

   \begin{verbatim}
   input:
      dev.........The ID of one of the devices attached to the SPI1
                  bus.  The valid SpiDevice IDs are enumerated above
                  in the header section of this module.

      millisec...The number of milliseconds to pause after EACH of the
                 assert and clear operations.  The total period of the
                 square-wave pulse is twice this value.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Spi1CsPulse(SpiDevice dev, unsigned char millisec)
{
   /* define the logging signature */
   cc *FuncName="Spi1CsPulse()";
   
   /* initialize the return value */
   int status=SpiOk;

   /* define local work objects */
   GpioPort *port=NULL; unsigned int pin=0;

   /* select the port and pin for the specified device on SPI1 bus */
   switch (dev)
   {
      case Spi1DevSpDuart:   {port=SPARE_DUART_SPI_CS_GPIO_Port; pin=SPARE_DUART_SPI_CS_Pin; break;}
      case Spi1DevRfDuart:   {port=RF_DUART_SPI_CS_GPIO_Port;    pin=RF_DUART_SPI_CS_Pin;    break;}
      case Spi1DevExpDuart1: {port=EXP_DUART1_SPI_CS_GPIO_Port;  pin=EXP_DUART1_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart2: {port=EXP_DUART2_SPI_CS_GPIO_Port;  pin=EXP_DUART2_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart3: {port=EXP_DUART3_SPI_CS_GPIO_Port;  pin=EXP_DUART3_SPI_CS_Pin;  break;}
      case Spi1DevExpDuart4: {port=EXP_DUART4_SPI_CS_GPIO_Port;  pin=EXP_DUART4_SPI_CS_Pin;  break;}
      default: {LogEntry(FuncName,"Device(%d) does not have a chip-select signal.\n",dev); status=SpiInvalid;}
   }

   /* check if the port assignment was successful */
   if (port && status>0)
   {
      /* assert the port's CS signal */
      if (Stm32GpioAssert(port,pin)<=0) {status=SpiFail;}
      
      /* pause for a prescribed period (milliseconds) */
      if (millisec) {Wait(millisec);}

      /* clear the port's CS signal */
      if (Stm32GpioClear(port,pin)<=0) {status=SpiFail;}
  
      /* pause for a prescribed period (milliseconds) */
      if (millisec) {Wait(millisec);}

      /* make a logentry if the CS pulse failed */
      if (status<=0) {LogEntry(FuncName,"Chip-select pulse failed "
                               "for SPI1 device %d.\n",dev);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to select a device on SPI1                                    */
/*------------------------------------------------------------------------*/
/**
   This function is used to select a single device, deselect all
   devices, or query for the device that is currently selected on the
   SPI1 bus.  At most, one device can be selected at any given time;
   daisy-chained SPI devices are not permitted by the SwiftWare SPI
   model.

      \begin{verbatim}
      input:

         dev....Specifies the device to be activated.  Also used to
                deselect all devices or to query for the currently
                selected device.  The following are valid selections:

                SpiDevQuery:      This is a pseudo-device that will
                                  query for the currently active device.

                Spi1Dev:          This indicates that all devices on
                                  SPI1 are to be deselected.

                Spi1DevSpDuart:   The DUART for spare sensors.
                Spi1DevRfDuart:   The DUART for the RF port.
                Spi1DevExpDuart1: The DUART1 of the expansion port.
                Spi1DevExpDuart2: The DUART2 of the expansion port.
                Spi1DevExpDuart3: The DUART3 of the expansion port.
                Spi1DevExpDuart4: The DUART4 of the expansion port.

      output:
         This function will return a positive value if successful or
         else zero, on failure.  A negative return value indicates an
         exception was encountered.  If the SpiDevQuery argument was
         used then the return value will be the currently active
         device as indicated by SpiDevice enumeration.
      \end{verbatim}
*/
int Spi1Select(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Spi1Select()";

   /* define static object to maintain record of current selection of SPI1 bus */
   static SpiDevice Spi1DevNow=Spi1Dev;
   
   /* initialize the return value */
   int err,status=SpiOk;

   /* check for query of current SPI selection */
   if (dev==SpiDevQuery) {status=Spi1DevNow;}
     
   /* validate the function argument */
   else if (dev<Spi1Dev || dev>=Spi2Dev)
   {
      LogEntry(FuncName,"Nonexistent device on SPI1: %d\n",
               dev); status=SpiInvalid;
   }

   /* check if the requested device matches the active device */
   else if (dev==Spi1DevNow && Spi1DevNow!=Spi1Dev) {status=SpiOk;}

   /* check if SPI1 bus already busy with another device */
   else if (dev!=Spi1Dev && Spi1DevNow!=Spi1Dev)
   {
      LogEntry(FuncName,"SPI1 bus busy with device(%d).\n",
               Spi1DevNow); status=SpiBusy;
   }

   /* determine if the selection is to be changed or else disabled */
   else
   {
      /* disable SPI1: assert the six chip-selects for SPI1 */
      if ((err=Stm32GpioAssert((GpioPort *)GPIOA, EXP_DUART4_SPI_CS_Pin))<=0) {status=err;}
      if ((err=Stm32GpioAssert((GpioPort *)GPIOB, SPARE_DUART_SPI_CS_Pin | RF_DUART_SPI_CS_Pin))<=0) {status=err;}
      if ((err=Stm32GpioAssert((GpioPort *)GPIOE, EXP_DUART1_SPI_CS_Pin | EXP_DUART2_SPI_CS_Pin |
                                                  EXP_DUART3_SPI_CS_Pin))<=0) {status=err;}

      /* switch statement to select a device on SPI1 bus */
      if (status>0) switch (dev)
      {
         /* all chip-selects on SPI1 bus were asserted above */
         case Spi1Dev: {break;}

         /* clear the chip-select for the spare DUART */
         case Spi1DevSpDuart: {status=Stm32GpioClear((GpioPort *)SPARE_DUART_SPI_CS_GPIO_Port, SPARE_DUART_SPI_CS_Pin); break;}

         /* clear the chip-select for the RF DUART */
         case Spi1DevRfDuart: {status=Stm32GpioClear((GpioPort *)RF_DUART_SPI_CS_GPIO_Port, RF_DUART_SPI_CS_Pin); break;}

         /* clear the chip-select for DUART1 on the expansion port */
         case Spi1DevExpDuart1: {status=Stm32GpioClear((GpioPort *)EXP_DUART1_SPI_CS_GPIO_Port, EXP_DUART1_SPI_CS_Pin); break;}

         /* clear the chip-select for DUART2 on the expansion port */
         case Spi1DevExpDuart2: {status=Stm32GpioClear((GpioPort *)EXP_DUART2_SPI_CS_GPIO_Port, EXP_DUART2_SPI_CS_Pin); break;}
         
         /* clear the chip-select for DUART3 on the expansion port */
         case  Spi1DevExpDuart3: {status=Stm32GpioClear((GpioPort *)EXP_DUART3_SPI_CS_GPIO_Port, EXP_DUART3_SPI_CS_Pin); break;}
         
         /* clear the chip-select for DUART4 on the expansion port */
         case Spi1DevExpDuart4: {status=Stm32GpioClear((GpioPort *)EXP_DUART4_SPI_CS_GPIO_Port, EXP_DUART4_SPI_CS_Pin); break;}

         /* make a logentry regarding an unimplemented device on the SPI1 bus */
         default: {LogEntry(FuncName,"Unimplemented device[%d] on SPI1 bus.\n",dev);}
      }

      /* record the current selection of the SPI1 bus */
      Spi1DevNow = (status>0) ? dev : Spi1Dev;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to pulse the chip-select signal on a device on the SI2 bus   */
/*------------------------------------------------------------------------*/
/**
   This function pulses the chip-select (CS) signal on a device
   attached to the SI2 bus.  The device's CS is asserted then cleared
   to form a square wave pulse.  After each of the assert and clear
   operations, a user-specified pause (0-255 milliseconds) is
   inserted; the pattern is assert, pause, clear, pause.  The end
   result is a square wave whose period is twice the user-specified
   pause.

   \begin{verbatim}
   input:
      dev.........The ID of one of the devices attached to the SI2
                  bus.  The valid SpiDevice IDs are enumerated above
                  in the header section of this module.

      millisec...The number of milliseconds to pause after EACH of the
                 assert and clear operations.  The total period of the
                 square-wave pulse is twice this value.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Spi2CsPulse(SpiDevice dev, unsigned char millisec)
{
   /* define the logging signature */
   cc *FuncName="Spi2CsPulse()";
   
   /* initialize the return value */
   int err,status=SpiOk;

   /* select a device from the SPI2 bus */
   if ((err=Spi2Select(dev))<=0)
   {
      LogEntry(FuncName,"SPI2 device(%d) selection failed. "
               "[err=%d]\n",dev,err); status=err;
   }
   else
   {
      /* define local work objects */
      GpioPort *port=NULL; unsigned int pin=0;

      /* select the port and pin for the specified device on SPI2 bus */
      switch (dev)
      {
         case Spi2DevEeprom:  {port=EEPROM_CS_GPIO_Port;         pin=EEPROM_CS_Pin;         break;}
         case Spi2DevSpGpio:  {port=SPARE_GPIO_SPI_CS_GPIO_Port; pin=SPARE_GPIO_SPI_CS_Pin; break;}
         case Spi2DevRfGpio:  {port=RF_GPIO_SPI_CS_GPIO_Port;    pin=RF_GPIO_SPI_CS_Pin;    break;}
         case Spi2DevExpGpio: {port=EXP_GPIO_SPI_CS_GPIO_Port;   pin=EXP_GPIO_SPI_CS_Pin;   break;}
         default: {LogEntry(FuncName,"Device(%d) does not have a chip-select signal.\n",dev); status=SpiInvalid;}
      }

      /* check if the port assignment was successful */
      if (port && status>0)
      {
          /* assert the port's CS signal */
         if (Stm32GpioAssert(port,pin)<=0) {status=SpiFail;}

         /* pause for a prescribed period (milliseconds) */
         if (millisec) {Wait(millisec);}

         /* clear the port's CS signal */
         if (Stm32GpioClear(port,pin)<=0) {status=SpiFail;}
  
         /* pause for a prescribed period (milliseconds) */
         if (millisec) {Wait(millisec);}

         /* make a logentry if the CS pulse failed */
         if (status<=0) {LogEntry(FuncName,"Chip-select pulse failed "
                                  "for SPI2 device %d.\n",dev);}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to select a device on SPI2                                    */
/*------------------------------------------------------------------------*/
/**
   This function is used to select a single device, deselect all
   devices, or query for the device that is currently selected on the
   SPI2 bus.  At most, one device can be selected at any given time;
   daisy-chained SPI devices are not permitted by the SwiftWare SPI
   model.

      \begin{verbatim}
      input:

         dev....Specifies the device to be activated.  Also used to
                deselect all devices or to query for the currently
                selected device.  The following are valid selections:

                SpiDevQuery:      This is a pseudo-device that will
                                  query for the currently active device.

                Spi2Dev:          This indicates that all devices on
                                  SPI2 are to be deselected.

                Spi2DevEeprom:    The EEPROM.
                Spi2DevSpGpio:    The GPIO for the spare sensors.
                Spi2DevRfGpio:    The GPIO for the RF port.
                Spi2DevExpGpio:   The GPIO for the expansion port.

      output:
         This function will return a positive value if successful or
         else zero, on failure.  A negative return value indicates an
         exception was encountered.  If the SpiDevQuery argument was
         used then the return value will be the currently active
         device as indicated by SpiDevice enumeration.
      \end{verbatim}
*/
int Spi2Select(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Spi2Select()";
   
   /* define static object to maintain record of current selection of SPI2 bus */
   static SpiDevice Spi2DevNow=Spi2Dev;

   /* initialize the return value */
   int err,status=SpiOk;
   
   /* check for query of current SPI selection */
   if (dev==SpiDevQuery) {status=Spi2DevNow;}
     
   /* validate the function argument */
   else if (dev<Spi2Dev || dev>=SpiDevNone)
   {
      LogEntry(FuncName,"Nonexistent device on SPI2: %d\n",
               dev); status=SpiInvalid;
   }

   /* check if the requested device matches the active device */
   else if (dev==Spi2DevNow && Spi2DevNow!=Spi2Dev) {status=SpiOk;}

   /* determine if the selection is to be changed or else disabled */
   else if (dev==Spi2Dev || dev!=Spi2DevNow)
   {
      /* assert the four chip-selects for SPI2 */
      if ((err=Stm32GpioAssert((GpioPort *)GPIOB, SPARE_GPIO_SPI_CS_Pin | RF_GPIO_SPI_CS_Pin))<=0) {status=err;}
      if ((err=Stm32GpioAssert((GpioPort *)GPIOD, EEPROM_CS_Pin))<=0) {status=err;}
      if ((err=Stm32GpioAssert((GpioPort *)GPIOE, EXP_GPIO_SPI_CS_Pin))<=0) {status=err;}

      /* switch statement to select a device on SPI2 bus */
      if (status>0) switch (dev)
      {
         /* all chip-selects on SPI2 bus were asserted above */
         case Spi2Dev: {break;}

         /* clear the chip-select for the EEPROM */
         case Spi2DevEeprom: {status=Stm32GpioClear((GpioPort *)EEPROM_CS_GPIO_Port, EEPROM_CS_Pin); break;}

         /* clear the chip-select for the spare GPIO on the SPI2 bus */
         case Spi2DevSpGpio: {status=Stm32GpioClear((GpioPort *)SPARE_GPIO_SPI_CS_GPIO_Port, SPARE_GPIO_SPI_CS_Pin); break;}

         /* clear the chip-select for the RF GPIO on the SPI2 bus */
         case Spi2DevRfGpio: {status=Stm32GpioClear((GpioPort *)RF_GPIO_SPI_CS_GPIO_Port, RF_GPIO_SPI_CS_Pin); break;}

         /* clear the chip-select for the expansion GPIO on the SPI2 bus */
         case Spi2DevExpGpio: {status=Stm32GpioClear((GpioPort *)EXP_GPIO_SPI_CS_GPIO_Port, EXP_GPIO_SPI_CS_Pin); break;}

         /* make a logentry regarding an unimplemented device on the SPI2 bus */
         default: {LogEntry(FuncName,"Unimplemented device[%d] on SPI2 bus.\n",dev);}
      }

      /* record the current selection of the SPI2 bus */
      Spi2DevNow = (status>0) ? dev : Spi2Dev;
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to initialize the Stm32 SPI1 bus                              */
/*------------------------------------------------------------------------*/
/**
   This function initializes the Stm32 SPI1 bus.
*/
int Stm32Spi1Init(void)
{
   /* enable the clock on the SPI1 peripheral */
   __HAL_RCC_SPI1_CLK_ENABLE();
   
   /* reinitialize the Stm32 SPI1 configuration */
   SPI1->CR1=0; SPI1->CR2=0; SPI1->CRCPR=0;

   return 1;
}

/*------------------------------------------------------------------------*/
/* function to initialize the Stm32 SPI1 bus                              */
/*------------------------------------------------------------------------*/
/**
   This function initializes the Stm32 SPI2 bus.
*/
int Stm32Spi2Init(void)
{
   /* enable the clock on the SPI2 peripheral */
   __HAL_RCC_SPI2_CLK_ENABLE();
   
   /* reinitialize the Stm32 SPI2 configuration */
   SPI2->CR1=0; SPI2->CR2=0; SPI2->CRCPR=0;

   return 1;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if the SPI2 bus is busy                */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function to determine if the SPI2 is busy.  A
   time-out feature is built-in to protect against an infinite loop.

   \begin{verbati}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

      millisec...The time-out period in milliseconds.

   output:
      This function returns a positive value if the SPI bus is busy or
      else zero if not busy.  A negative return value indicates that an
      exception was encountered.
   \end{verbati}
*/
int Stm32SpiBusy(SpiBus *bus, unsigned char millisec)
{
   /* initialize the return value */
   int busy=1;

   /* initialize the timeout feature */
   unsigned long int To=HAL_GetTick();
      
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {busy=SpiInvalid;}

   /* implement timeout loop */
   else while ((HAL_GetTick()-To)<millisec)
   {
      /* break out of the loop if the SPI bus is not busy */
      if (!((bus->SR)&SPI_SR_BSY)) {busy=0; break;}
   }

   return busy;
}

/*------------------------------------------------------------------------*/
/* function to disable a SPI bus                                          */
/*------------------------------------------------------------------------*/
/**
   This function disables a SPI bus by clearing the SPE bit of the
   bus's CR1 register.  The bus address is validated prior to the
   enable operation.

   \begin{varbatim}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

   output:
      This function returns a positive value if successful or else
      zero on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Stm32SpiDisable(SpiBus *bus)
{
   /* initialize the return value */
   int status=SpiOk;
   
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {status=SpiInvalid;}

   /* disable the SPI bus */
   else {CLEAR_BIT(bus->CR1, SPI_CR1_SPE);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable a SPI bus                                           */
/*------------------------------------------------------------------------*/
/**
   This function enables a SPI bus by asserting the SPE bit of the
   bus's CR1 register.  The bus address is validated prior to the
   enable operation.

   \begin{varbatim}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

   output:
      This function returns a positive value if successful or else
      zero on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Stm32SpiEnable(SpiBus *bus)
{
   /* initialize the return value */
   int status=SpiOk;
   
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {status=SpiInvalid;}

   /* enable the SPI bus */
   else {SET_BIT(bus->CR1, SPI_CR1_SPE);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to configure an SPI bus                                       */
/*------------------------------------------------------------------------*/
/**
   This function configures the SPI bus' CR1, CR2, and CRCPR registers.

   \begin{varbatim}
   input:
      bus.......The SPI bus of the Stm32; either SPI1 or SPI2.

      cr1.......The value to use for the SPI's CR1 registers.

      cr2.......The value to use for the SPI's CR2 registers.

      crcpoly...The value to use for the SPI's CRCPR registers.

   output:
      This function returns a positive value if successful or else
      zero on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Stm32SpiConfig(SpiBus *bus, unsigned int cr1, unsigned int cr2, unsigned int crcpoly)
{
   /* initialize the return value */
   int status=SpiOk;
   
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {status=SpiInvalid;}

   else
   {
      /* disable the SPI bus prior to reconfiguration */
      CLEAR_BIT(bus->CR1, SPI_CR1_SPE);

      /* clear SPE & SSI bits; assert SSM & MSTR bits */
      cr1 &= (~(SPI_CR1_SPE | SPI_CR1_SSI)); cr1 |= (SPI_CR1_SSM | SPI_CR1_MSTR);

      /* clear reserved & DMA bits; assert SSOE bit */
      cr2 &= (~(SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN | SPI_CR2_RESRV)); cr2 |= SPI_CR2_SSOE;

      /* configure SPI bus */
      bus->CR1 = cr1; bus->CR2 = cr2; bus->CRCPR = crcpoly&(0xffffUL);

      /* enable the SPI bus */
      SET_BIT(bus->CR1, SPI_CR1_SPE);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine validity of a SpiBus                   */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function to determine if the function argument
   represents a valid SPI bus.  On the Apf11 controller, only SPI{1,2}
   are implemented.

   \begin{varbatim}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

   output:
      This function returns a positive value if the function's
      argument represents a valid SPI bus or else zero is returned if
      the bus is invalid.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Stm32SpiIsValidBus(SpiBus *bus)
{
   /* define the logging signature */
   cc *FuncName="Stm32SpiIsValidBus()";
  
   /* initialize the return value */
   int status=SpiFail;
   
   /* validate the pointer to the SPI bus */
   if (!bus) {LogEntry(FuncName,"Missing SPI bus."); status=SpiNull;}

   /* validate the SPI bus address */
   else if (bus==(SpiBus *)SPI1 || bus==(SpiBus *)SPI2) {status=SpiOk;}

   /* log the error */
   else {LogEntry(FuncName,"Invalid Stm32 SPI bus: 0x08lx\n",(unsigned long int)bus);}

   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if the SPI2 Rx buffer is not empty     */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function to determine if the SPI2 Rx buffer is
   not empty.  A time-out feature is built-in to protect against an
   infinite loop.

   \begin{verbati}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

      millisec...The time-out period in milliseconds.

   output:
      This function returns a positive value if the SPI bus Rx buffer
      is not empty.  A negative return value indicates that an
      exception was encountered.
   \end{verbati}
*/
int Stm32SpiRxne(SpiBus *bus, unsigned char millisec)
{
   /* initialize the return value */
   int status=SpiFail;

   /* initialize the timeout feature */
   unsigned long int To=HAL_GetTick();
      
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {status=SpiInvalid;}

   /* implement timeout loop */
   else while ((HAL_GetTick()-To)<millisec)
   {
      /* break out of the loop if the SPI Rx buffer contains a byte */
      if (((bus->SR)&SPI_SR_RXNE)) {status=SpiOk; break;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* predicate function to determine if the SPI2 Tx buffer is empty         */
/*------------------------------------------------------------------------*/
/**
   This is a predicate function to determine if the SPI2 Tx buffer is
   empty.  A time-out feature is built-in to protect against an
   infinite loop.

   \begin{verbati}
   input:
      bus........The SPI bus of the Stm32; either SPI1 or SPI2.

      millisec...The time-out period in milliseconds.

   output:
      This function returns a positive value if the SPI bus Tx buffer
      is empty.  A negative return value indicates that an exception
      was encountered.
   \end{verbati}
*/
int Stm32SpiTxe(SpiBus *bus, unsigned char millisec)
{
   /* initialize the return value */
   int status=SpiFail;

   /* initialize the timeout feature */
   unsigned long int To=HAL_GetTick();
      
   /* validate the SPI port */
   if (Stm32SpiIsValidBus(bus)<=0) {status=SpiInvalid;}

   /* implement timeout loop */
   else while ((HAL_GetTick()-To)<millisec)
   {
      /* break out of the loop if the SPI Tx buffer is empty */
      if (((bus->SR)&SPI_SR_TXE)) {status=SpiOk; break;}
   }

   return status;
}

#endif /* STM32F103SPI_C */
