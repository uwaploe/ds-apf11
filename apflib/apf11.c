#ifndef APF11_H
#define APF11_H (0x0010U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define apf11ChangeLog "$RCSfile$  $Revision$  $Date$"

#include <time.h>

/* declare functions with external linkage */
int               AirPumpRun(time_t seconds, unsigned short *VoltCnt, unsigned short* AmpCnt);
int               Apf11_3p3vDisable(void);
int               Apf11_3p3vEnable(void);
int               Apf11_3p3vQuery(void);
int               Apf11FsmcConfig(void);
long int          Apf11HeapAvailable(void);
unsigned long int Apf11HeapHigh(void);
void              Apf11HeapLog(void);
unsigned long int Apf11HeapUsed(void);
void              Apf11Init(void);
void              Apf11MemoryMap(void);
void              Apf11PowerOff(time_t AlarmSec);
int               Apf11SpSensorPowerUp(void);
int               Apf11SpSensorPowerDown(void);
int               Apf11SpSensorPowerQuery(void);
long int          Apf11StackAvailable(void);
unsigned long int Apf11StackLow(void);
unsigned long int Apf11StackSize(void);
time_t            Apf11StandbyTime(void);
int               Apf11ValidateLogSignature(void);
unsigned char     Apf11WakeUpByRtc(void);
time_t            Apf11WakeTime(void);
unsigned long int msTimer(unsigned long int msTimeRef);
void              HAL_MPU_Init();
unsigned long int RamTest(void);
void             *_sbrk(int size);
int               SetAlarm(time_t alarm);
int               StackOk(void);
unsigned int      sleep(unsigned int seconds);
int               usleep(unsigned long int usec);
int               Wait(unsigned long int millisec);

/* lowest address for external SRAM */
extern const unsigned long int Apf11ExtRamLo;

/* highest address of external SRAM */
extern const unsigned long int Apf11ExtRamHi;
 
/* starting address of the program heap */
extern const unsigned long int Apf11HeapBase;

/* current address of the top of the heap */
extern unsigned long int Apf11HeapTop;

#endif /* APF11_H */
#ifdef APF11_C
#undef APF11_C

#include <apf11ad.h>
#include <assert.h>
#include <conio.h>
#include <ctdio.h>
#include <ds2740.h>
#include <errno.h>
#include <limits.h>
#include <logger.h>
#include <max7301.h>
#include <nan.h>
#include <Stm32f103Exti.h>
#include <Stm32f103Fsmc.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Pwr.h>
#include <Stm32f103Rtc.h>
#include <Stm32f103Spi.h>

/* declare loader segments */
extern unsigned char _sbss[],  _ebss[];
extern unsigned char _sdata[], _edata[];
extern unsigned char _spersistent[], _epersistent[];
extern unsigned char _estack[], _Min_Stack_Size[];
extern unsigned char _end[];

/* lowest address for external SRAM */
const unsigned long int Apf11ExtRamLo=0x60000000UL;

/* highest address of external SRAM */
const unsigned long int Apf11ExtRamHi=0x600fffffUL; 

/* starting address of the program heap */
const unsigned long int Apf11HeapBase=(unsigned long int)_end;
 
/* current address of the top of the heap */
unsigned long int Apf11HeapTop=(unsigned long int)_end;

/* define variable to hold the mission times at wakeup */
static time_t WakeTime;

/* persistent variable to record the mission time when induced into standby mode */
persistent static time_t StandbyTime;

/* declare external functions that are used locally */
int      streamok(int handle);
void     HAL_Delay(__IO uint32_t Delay);
uint32_t HAL_GetTick(void);
int      Stm32RtcAlarmWriteTics(unsigned long int AlarmTics);

/*------------------------------------------------------------------------*/
/* function to run the air pump for a specified length of time            */
/*------------------------------------------------------------------------*/
/**
   This function activates the air-pump motor for a specified length of
   time.  The battery voltage and current are measured just prior to turning
   off the air pump.

      \begin{verbatim}
      input:
         seconds....The length of time to run the air pump.

      output:
         VoltCnt....The 12-bit AD measurement of the battery voltage just
                    prior to turning off the air pump.
         AmpCnt.....The 12-bit AD meeasurement of the battery current just
                    prior to turning off the air pump.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exceptional
         condition.
      \end{verbatim}
*/
int AirPumpRun(time_t seconds, unsigned short *VoltCnt, unsigned short* AmpCnt)
{
    /* turn on the air pump motor */
    AirPumpPowerOn();

    /* let the air pump run for the specified number of seconds */
    Wait(seconds * 1000);

    /* measure the 8-bit voltage count */
    if (VoltCnt)
    {
        *VoltCnt = BatVoltsAdc();
    }

    /* measure the 8-bit current count */
    if (AmpCnt)
    {
        *AmpCnt = AirPumpAmpsAdc();
    }

    /* turn off the air pump motor */
    AirPumpPowerOff();
    return 1;
}

/*------------------------------------------------------------------------*/
/* disable the Apf11's main switching regulator (3P3V)                    */
/*------------------------------------------------------------------------*/
/**
   This function disables the Apf11's main switching regulator that
   feeds the (3.5V) 3P3V line and the 3P3V_CPU line which is
   diode-OR'd with a lower power (3.3V) LDO regulator.

      \begin{verbatim}
      output:
         This function returns a positive value if the 3P3V signal is
         successfully disabled.  Zero is returned on failure.  A
         negative value indicates an exception was encountered.
      \end{verbatim}
*/
int Apf11_3p3vDisable(void)
{
   /* clear the ACTIVE_ENABLE_3P3V pin of the Stm32 */
   int status=Stm32GpioClear(ACTIVE_ENABLE_3P3V_GPIO_Port, ACTIVE_ENABLE_3P3V_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* enable the Apf11's main switching regulator (3P3V)                     */
/*------------------------------------------------------------------------*/
/**
   This function enables the Apf11's main switching regulator that
   feeds the (3.5V) 3P3V line and the 3P3V_CPU line which is
   diode-OR'd with a lower power (3.3V) LDO regulator.

      \begin{verbatim}
      output:
         This function returns a positive value if the 3P3V signal is
         successfully enabled.  Zero is returned on failure.  A
         negative value indicates an exception was encountered.
      \end{verbatim}
*/
int Apf11_3p3vEnable(void)
{
   /* assert the ACTIVE_ENABLE_3P3V pin of the Stm32 */
   int status=Stm32GpioAssert(ACTIVE_ENABLE_3P3V_GPIO_Port, ACTIVE_ENABLE_3P3V_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* query the state of the Apf11's main switching regulator (3P3V)         */
/*------------------------------------------------------------------------*/
/**
   This function queries the state of the Apf11's main switching
   regulator that feeds the (3.5V) 3P3V line and the 3P3V_CPU line
   which is diode-OR'd with a lower power (3.3V) LDO regulator.

      \begin{verbatim}
      output:
         This function returns a positive value if the 3P3V signal is
         enabled or zero if the 3P3V signal is disabled.  A negative
         value indicates an exception was encountered.
      \end{verbatim}
*/
int Apf11_3p3vQuery(void)
{
   /* read the state of the ACTIVE_ENABLE_3P3V pin of the Stm32 */
   int state=Stm32GpioIsAssert(ACTIVE_ENABLE_3P3V_GPIO_Port,ACTIVE_ENABLE_3P3V_Pin);

   return state;
}

/*------------------------------------------------------------------------*/
/* function to compute the number of unallocated bytes on the heap        */
/*------------------------------------------------------------------------*/
/**
   The function computes the number of unallocated bytes that remain
   available from the heap.  A negative value would indicate that the
   heap has encroached on the region of SRAM that is reserved for the
   stack. 
*/
long int Apf11HeapAvailable(void)
{
   /* initialize the calculation with the upper limit of the heap */
   long int available = Apf11HeapHigh()+1;

   /* subtract the address of the top of the heap */
   available -= Apf11HeapTop; 

   return available;
}

/*------------------------------------------------------------------------*/
/* function to return the address of the upper limit of the heap          */
/*------------------------------------------------------------------------*/
/**
   This function returns the address of the upper limit of the area of
   volatile SRAM that is reserved for the heap.
*/
unsigned long int Apf11HeapHigh(void) {return Apf11StackLow()-1;}

/*------------------------------------------------------------------------*/
/* function to log the address range and current usage of the heap space  */
/*------------------------------------------------------------------------*/
/**
   This function logs the address range of the heap, the current
   address of the top of the heap as well as current consumption and
   availability of heap space.  The heap is based at the upper end of
   the BSS segment and the address at the top of the heap grows upward
   as heap is consumed. 
*/
void Apf11HeapLog(void)
{
   /* define the logging signature */
   cc *FuncName = "Apf11HeapLog()";
      
   /* log heap statistics */
   LogEntry(FuncName,"Heap: [base: 0x%08lx, top: 0x%08lx, high: 0x%08lx "
            "used:%lu available: %ld]\n",Apf11HeapBase,Apf11HeapTop,
            Apf11HeapHigh(),Apf11HeapUsed(),Apf11HeapAvailable());
}

/*------------------------------------------------------------------------*/
/* function to compute the number of bytes allocated from the heap        */
/*------------------------------------------------------------------------*/
/**
   The function computes the number of bytes that have been allocated
   from the heap.  
*/
unsigned long int Apf11HeapUsed(void) {return Apf11HeapTop-Apf11HeapBase;}

/*------------------------------------------------------------------------*/
/* perform post-reset apf11 initialization                                */
/*------------------------------------------------------------------------*/
/**
   This function performs post-reset apf11 initializations.
*/
void Apf11Init(void)
{
   /* define the logging signature */
   cc *FuncName = "Apf11Init()";

   /* define local work objects to record the post-reset RTC and alarm time */
   unsigned long int AlarmTics,RtcTics;
     
   /* disable logging until startup code has executed */
   LogEnable(0);
 
   /* initialize the Stm32 GPIO */
   Stm32GpioInit();

   /* configure the FSMC controller to use the external SRAM */
   Stm32FsmcConfig();

   /* initialize the SwiftWare STM32F103 RTC model */
   Stm32RtcConfig();

   /* retrieve the alarm time recorded just prior to entering standby mode */
   if (Stm32AlarmRecall(&AlarmTics)<=0) {AlarmTics=ULONG_MAX;}

   /* write the alarm tics back to the RTC alarm registers */
   else Stm32RtcAlarmWriteTics(AlarmTics);
   
   /* transfer the alarm time to the PWR module */
   Stm32AlarmTicsAtWakeSet(AlarmTics);

   /* retrieve the post-reset RTC tics */
   RtcTics=IntervalTimerTics();

   /* transfer the post-reset RTC tics to the PWR module */
   Stm32RtcTicsAtWakeSet(RtcTics);

   /* enable console IO services */
   ConioEnable();

   /* validate logging parameters and enable logging */
   Apf11ValidateLogSignature(); LogEnable(1);
   
   /* configure and enable the CTD serial port and pull-up */
   CtdConfig(9600); CtdDisableRx(); Apf11_3p3vEnable();

   /* initialize the SPI{1,2} busses */
   Stm32Spi1Init(); Stm32Spi2Init();

   /* initialize the spare GPIO interface */
   SpGpioInit();

   /* pet the watchdog and put the spare GPIO back to sleep */
   WatchDogPet(); SpGpioSleep();

   /* initialize the EXTI peripheral */
   Stm32f103ExtiInit();

   /* Initialize the MPU */
   HAL_MPU_Init();
   
   /* initialize the wakeup time */
   WakeTime=itimer();

   /* accumulate the cummulative length of time that the Apf11 was in standy mode */
   ChargeModelAccumulate((WakeTime-StandbyTime),0,NaN());
   
   /* validate the RTC */
   if (time(NULL)<0) {RtcSet(0);}

   /* check for wake-up by the watchdog (RTC) alarm signal */
   if (WatchDogEvent()>0) {LogEntry(FuncName,"Warning: Wake-up initiated "
                                    "by watch-dog.\n");}
   
   /* check for user-initiated wake-up */
   else if (!Apf11WakeUpByRtc()) {LogEntry(FuncName,"Asynchronous wake-up detected (ie., "
                                           "wake-up not initiated by alarm signal).\n");}
}

/*------------------------------------------------------------------------*/
/* function to log the availability and usage of SRAM memory              */
/*------------------------------------------------------------------------*/
/**
   This function logs the availability and usage of both volatile and
   persistent SRAM.  The Stm32 includes 96KB of SRAM that loses power
   while in standby mode and, hence, is volatile.  The Apf11 also
   includes external SRAM that is accessed via the Stm32's FSMC
   peripheral.  This external SRAM is persistent because battery power
   is maintained regardless of the state of the Stm32.
*/
void Apf11MemoryMap(void)
{
   /* define the logging signature */
   cc *FuncName = "Apf11MemoryMap()";

   /* type definition for addresses */
   typedef unsigned long int addr_t;

   /* log the address ranges of volatile and persistent SRAM */
   LogEntry(FuncName,"SRAM: Volatile[%08lx,%08lx] Persisent[%08lx,%08lx]\n",
            (addr_t)_sdata,(uint32_t)_estack,Apf11ExtRamLo,Apf11ExtRamHi);

   /* log the address ranges of the DATA and BSS segments */
   LogEntry(FuncName,"SRAM usage: Data[%08lx,%08lx] Bss[%08lx,%08lx]\n",
            (addr_t)_sdata,(addr_t)_edata-1,(addr_t)_sbss,(addr_t)_ebss-1);

   /* log the address ranges of the heap and stack */
   LogEntry(FuncName,"SRAM usage: Heap[%08lx,%08lx] Stack,[%08lx,%08lx]\n",
            Apf11HeapBase,Apf11HeapHigh(),Apf11StackLow(),(uint32_t)_estack);

   /* log the address ranges of used persistent SRAM */
   LogEntry(FuncName,"SRAM usage: Persistent[%08lx,%08lx]\n",
            (addr_t)_spersistent,(addr_t)_epersistent-1);
}

/*------------------------------------------------------------------------*/
/* function to turn off power                                             */
/*------------------------------------------------------------------------*/
/**
   This function powers down the APF11 controller after setting the alarm for
   the next wake-up.  If the sleep period is not in the closed interval
   [5sec,3Hr] then this function will reset the sleep period.

      \begin{verbatim}
      input:
         AlarmSec ... This is the number of seconds in the future for which
                      the interval-timer alarm will be set.  The Stm32 RTC
                      will wake up the APF11 when the sleep period expires.
                      If the sleep period is not in the closed interval
                      [5sec,3Hr] then this function will reset the sleep
                      period.
      \end{verbatim}
*/
void Apf11PowerOff(time_t AlarmSec)
{
   /* define the logging signature */
   cc *FuncName = "Apf11PowerOff()";

   /* compute the sleep time */
   time_t sleep = (AlarmSec-itimer());

   /* add one conversion period if awake greater than two conversion periods */
   ChargeModelAccumulate(0,1,BatCharge(ADC));

   /* record the mission time when the Apf11 was induced into standby mode */
   StandbyTime=itimer();
   
   /* validate the sleep time */
   if (sleep<5L || sleep>WDogTimeOut)
   {
      /* reset the sleep period */
      AlarmSec = itimer() + ((sleep<5L) ? 5L : WDogTimeOut);

      /* make the log-entry */
      LogEntry(FuncName,"Sleep period (%ldsec) not in valid "
               "range [5sec,105Min].  Resetting to %s.\n",
               sleep,(sleep<5L)?"5sec":"105Min");
   }

   /* set the alarm for the next wake-up */
   SetAlarm(AlarmSec); 

   /* pet the watchdog circuit */
   WatchDogPet(); WatchDogLatchReset();
   
   /* protect the FSMC against corruptions */
   Stm32FsmcDisable();

   /* initialize the EXTI interface */
   Stm32f103ExtiInit();
   
   /* disable the Max7301 */
   SpGpioDisable();

   /* disable the 3P3V signal */
   Apf11_3p3vDisable(); Wait(25);
   
   /* induce the apf11 into standby mode */
   Stm32Standby();
}

/*------------------------------------------------------------------------*/
/* function to power-up the Apf11's spare sensor interface                */
/*------------------------------------------------------------------------*/
/**
   This function powers-up the Apf11's spare sensor interface.  This
   function returns a positive value on success or else zero on
   failure.  A negative return value indicates an exeception was
   encountered. 
*/
int Apf11SpSensorPowerUp(void)
{
   /* enable the high-side switch that supplies power to the spare sensor circuits of the Apf11 */
   int status=Stm32GpioAssert(SPARE_SENSOR_PWR_EN_GPIO_Port, SPARE_SENSOR_PWR_EN_Pin);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to power-down the Apf11's spare sensor interface              */
/*------------------------------------------------------------------------*/
/**
   This function powers-down the Apf11's spare sensor interface.  This
   function returns a positive value on success or else zero on
   failure.  A negative return value indicates an exeception was
   encountered.
*/
int Apf11SpSensorPowerDown(void)
{
   /* disable the high-side switch that supplies power to the spare sensor circuits of the Apf11 */
   int status=Stm32GpioClear(SPARE_SENSOR_PWR_EN_GPIO_Port, SPARE_SENSOR_PWR_EN_Pin);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine if the Apf11 spare sensor interface is powered   */
/*------------------------------------------------------------------------*/
/**
   This function determins if the Apf11 spare sensor interface is
   currently powered up or down.  This function returns a positive
   value if the spare sensor interface is powered-up or else zero if
   it is powered-down.  A negative return value indicates and
   exception was encountered.
*/
int Apf11SpSensorPowerQuery(void)
{
   int status=Stm32GpioIsAssert(SPARE_SENSOR_PWR_EN_GPIO_Port, SPARE_SENSOR_PWR_EN_Pin);

   return status;
}

/*------------------------------------------------------------------------*/
/* return the address of the lower limit of area reserved for the stack   */
/*------------------------------------------------------------------------*/
/**
   This function returns the address of the lower limit of the area of
   volatile SRAM that is reserved for the stack.
*/
unsigned long int Apf11StackLow(void) {return (unsigned long int)_estack-Apf11StackSize();}

/*------------------------------------------------------------------------*/
/* function to return the maximum safe stack size                         */
/*------------------------------------------------------------------------*/
/**
   This function returns the maximum safe stack size that will
   guarantee avoidance of a heap/stack collision.  The Swiftware heap
   manager (sbrk()) prevents the heap from growing beyond its reserved
   space in volatile SRAM.  However, no mechanism exists to prevent
   the stack from growing downward into the heap space.  If the stack
   size grows larger than the value returned by this function then the
   stack will encroach on the heap and introduce the potential for a
   heap/stack collision.

     \begin{verbatim}
        output:
           This function returns the maximum safe stack size that will
           ensure that the stack will not encroach into the segment of
           volatile SRAM that is reserved for the heap.
     \end{verbatim}
*/
unsigned long int Apf11StackSize(void) {return (unsigned long int)_Min_Stack_Size;}

/*------------------------------------------------------------------------*/
/* return the interval timer when the Apf11 was induced into standby mode */
/*------------------------------------------------------------------------*/
time_t Apf11StandbyTime(void) {return StandbyTime;}

/*------------------------------------------------------------------------*/
/* function to validate logging parameters                                */
/*------------------------------------------------------------------------*/
/**
   By design and out of necessity, the Apf11's persistent SRAM is not
   automatically initialized by firmware start-up code. Persistent
   SRAM retains its state across boot cycles unless battery power is
   removed from the Apf11.  However, persistent SRAM is still volatile
   and so if battery power is disconnected from the Apf11 then what
   was stored is irretrievably lost.  This function implements a
   method to determine if the logging parameters that are stored in
   persistent memory have previously been initialized (ie., since
   battery power was last connected).

   The firmware revision is a constant and unique 32-bit string that
   is computed at compile time and so represents a useful signature,
   if stored in a predefined location of persistent SRAM.  When
   battery power is applied to the Apf11, the contents of SRAM will be
   either random or else all zeros.  If random, then the probability
   that the predefined location will contain the correct logging
   signature merely by chance is 2.3e-10 (ie., 1/2^32).

   This function is intened to be called during the boot process. If
   the predefined location is found not to contain the correct
   signature then it is quite certain that the logging parameters have
   not been previously initialized.  In that case, this function
   initializes the logging parameters and stores the correct signature
   at the predefine location of SRAM.  On the other hand, if the
   predefined location does contain the correct signature then it is
   (all but) certain that the logging parameters were previously
   initialized.  In this case, the logging parameters are not altered
   unless found to be invalid.

   \begin{verbatim}
   output:
      This function returns a positive value if the correct logging
      signature (ie., the 32-bit firmware revision) was found in the
      predefined location of persistent SRAM and if all logging
      parameters were within valid ranges.  Zero is returned if the
      signature or any of the logging parameters were invalid.
   \end{verbatim}
*/
int Apf11ValidateLogSignature(void)
{
   /* initialize the return value */
   int status=1;

   /* declare a persistent 2-byte word to contain the log signature */
   persistent static unsigned long int LogSignature;

   /* use the firmware revision as the signature */
   extern const unsigned long FwRev;

   /* check if logging parameters were previously initialized */
   if (LogSignature!=FwRev)
   {
      /* initialize the logging parameters and the log-signature */
      loghandle=-1; MaxLogSize=40960L; debugbits=2;
      LogPredicate(1); LogSignature=FwRev; status=0;
   }

   else
   {
      /* validate the logstream */      
      if (streamok(loghandle)<=0) {loghandle=-1; status=0;}

      /* validate the minimum size of the logfile */
      if (MaxLogSize<5120L) {MaxLogSize=5120L;}
      
      /* validate the maximum size of the logfile */
      else if (MaxLogSize>64512L) {MaxLogSize=64512L;}

      /* validate the debuglevel */
      if (debuglevel>5) {debugbits=2; LogPredicate(1); status=0;}
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* return the interval timer when the Apf11 was awakened                  */
/*------------------------------------------------------------------------*/
time_t Apf11WakeTime(void) {return WakeTime;}

/*------------------------------------------------------------------------*/
/* determine if the last wake cycle was induced by the RTC alarm          */
/*------------------------------------------------------------------------*/
/**
   This function determines if the most recent wake cycle was induced
   by the RTC alarm.  Just prior to entering standby mode,
   Stm32Standby() executes Stm32AlarmSave().  Upon waking, Apf11Init()
   induces a record of the alarm and RTC tics just after completion of
   the reset initializations; the elapsed time represents the duration
   of boot-up operations.  If the wake-time is after the alarm-time
   and the difference is less than 256 RTC tics (ie., one second),
   then this function determines that the wake cycle was induced by
   the RTC alarm.

      \begin{verbatim}
      output:
         This function returns zero to indicate that the wake cycle
         was not induced by the RTC alarm.  If it was determined that
         the RTC alarm induced the wake cycle then this function
         returns a positive return value that represents the number of
         RTC tics (in the closed interval [1,255]) that were required
         to complete the reset initializations.  Each tic represents
         3.9 milliseconds.
      \end{verbatim}
*/
unsigned char Apf11WakeUpByRtc(void)
{
   /* initialize return value */
   int ResetTics=0;

   /* retrieve the alarm time for the wake cycle */
   unsigned long int WakeAlarmTics=Stm32AlarmTicsAtWake();

   /* retrieve the RTC tics after reset cycle was complete */
   unsigned long int WakeRtcTics=Stm32RtcTicsAtWake();

   /* compute the RTC tics required to complete the reset cycle */
   long int WakeSkew=(WakeRtcTics-WakeAlarmTics);

   /* validate the RTC and alarm times */
   if (WakeAlarmTics!=ULONG_MAX && WakeRtcTics!=ULONG_MAX)
   {
      /* determine if wake was caused by the RTC alarm */
      if (WakeSkew>0 && WakeSkew<256) ResetTics=WakeSkew;
   }
   
   return ResetTics;
}

/*------------------------------------------------------------------------*/
/* accurate stopwatch with millisecond resolution                         */
/*------------------------------------------------------------------------*/
/**
   This function represents an accurate stopwatch with millisecond
   resolution.

   \begin{verbatim}
   input:
      msTimeRef...This is the reference time (milliseconds) used as
                  the start-time for the stop watch. 

   output:
      This function returns the number of milliseconds that has
      elapsed since the reference time given by the argument of this
      function.  If msTimeRef is zero then this function returns the
      current time as measured by the number of milliseconds since the
      SYS timer was enabled.  If the current SYS timer is less than
      msTimeRef then the timer is assumed to have wrapped around and
      so this function adds the amount of time prior to the
      wrap-around to the current SYS timer.
   \end{verbatim}
*/
unsigned long int msTimer(unsigned long int msTimeRef)
{
   /* get the current HAL tick counter */
   unsigned long int msTime=HAL_GetTick();

   /* compute the time interval (milliseconds) since the reference time */
   if (msTime>=msTimeRef) {msTime -= msTimeRef;}

   /* add the elapsed time prior to wrap-around of 32-bit word */
   else {msTime += ((ULONG_MAX-msTimeRef)+1);}
   
   return msTime;
}

/*------------------------------------------------------------------------*/
/* function to exectute a (destructive) test of the external SRAM         */
/*------------------------------------------------------------------------*/
/**
   This function writes a test pattern to external SRAM and rereads
   the SRAM to verify the expected test pattern.  This process
   involves the destruction of all data in the segment of memory with
   addresses in the range [Apf11ExtRamLo:Apf11ExtRamHi].

   This function returns a the number of errors detected.
*/
unsigned long int RamTest(void)
{
   /* define some local work objects */
   char buf[80]; unsigned int block; unsigned long int i,addr,nErrors=0,high,indx;
   
   /* initialize the block size for reads/writes */
   const unsigned long int BlockSize=0x4000;

   /* initialize a pointer to the external SRAM */
   unsigned char *extSRam = (unsigned char *)Apf11ExtRamLo;

   /* write the test pattern block by block */
   for(addr=Apf11ExtRamLo; addr<=Apf11ExtRamHi; addr+=BlockSize)
   {
      /* compute the current block number */
      block=(addr-Apf11ExtRamLo)/BlockSize;
      high=((addr+BlockSize)>Apf11ExtRamHi)?Apf11ExtRamHi:(addr+BlockSize-1);

      snprintf(buf,sizeof(buf),"Writing test pattern: (block %02u) "
               "[0x%08lx:0x%08lx].",block,addr,high);
      pputs(&conio,buf,2,"\r\n");

      /* loop thru each address in the block */
      for (i=0; i<BlockSize; i++)
	  {
         /* compute the index relative to the beginning of SRAM */
         indx=(addr-Apf11ExtRamLo) + i;
         
         /* prevent overindexing errors */
         if ((addr+i)>Apf11ExtRamHi) break;
         
         /* write the test pattern into the current address */
         extSRam[indx] = (unsigned char)((i + 10 + block) & 0xff);
      }
   }

   /* read the test pattern block by block */
   for(addr=Apf11ExtRamLo; addr<=Apf11ExtRamHi; addr+=BlockSize)
   {
      /* compute the current block number */
      block=(addr-Apf11ExtRamLo)/BlockSize;
      high=((addr+BlockSize)>Apf11ExtRamHi)?Apf11ExtRamHi:(addr+BlockSize-1);
      
      snprintf(buf,sizeof(buf),"Reading test pattern: (block %02u) "
                "[0x%08lx:0x%08lx].",block,addr,high);
      pputs(&conio,buf,2,"\r\n");
      
      for (i=0; i<BlockSize; i++)
		{
         /* compute the index relative to the beginning for far RAM */
         indx=(addr-Apf11ExtRamLo) + i;
         
         /* prevent overindexing errors */
         if ((addr+i)>Apf11ExtRamHi) break;

         /* verify the test pattern */
         if (extSRam[indx] != (unsigned char)((i + 10 + block) & 0xff))
			{
            /* indicate failure and count the number of errors */
            ++nErrors;
            
            snprintf(buf,sizeof(buf),"Error at addr=0x%08lx: wr=%02x, rd=%02x",
                     (unsigned long int)(addr+i),(unsigned char)((i+10+block)&0xff),
                     (unsigned char)extSRam[indx]);
            pputs(&conio,buf,2,"\r\n");
			}
		}
	}
   
   return nErrors;
}

/*------------------------------------------------------------------------*/
/* function to activate the interval alarm                                */
/*------------------------------------------------------------------------*/
/**
   This function sets and activates an alarm that will generate a wake
   sigal when the alarm expires.  Various sanity checks are done to
   ensure that the 24-bit interval-timer will not roll over before the
   alarm expires.  The alarm periods are also limited to the closed
   interval [MinAlarmSec, MaxAlarmSec] as a failsafe against a float
   inadvertently committing suicide.  The interval-alarm interrupt is
   also enabled so that when the alarm expires, a wake event will be
   generated.

      \begin{verbatim}
      input:
         alarm ... The time (seconds) when the alarm is supposed to expire.
                   When the value of the interval-timer register equals the
                   value of the interval-alarm register then the alarm
                   expires and a wake event is generated (unless the
                   interval alarm is subsequently disabled).

      output:
         This function returns a positive value on success or zero on
         failure.  A negative return value indicates that an exception
         was encountered.
      \end{verbatim}
*/
int SetAlarm(time_t alarm)
{
   /* define the logging signature */
   cc *FuncName = "SetAlarm()";

   /* initialize return value */
   int err,status = RtcOk;

   /* define some local work objects */
   unsigned char tics; time_t now,alarmcheck=-1; int tries=0;

   /* define the minimum and the maximum alarm periods */
   const time_t MinAlarmSec=5, MaxAlarmSec=WDogTimeOut;
   
   /* disable interval-alarm interrupts */
   if ((err=IntervalAlarmDisable())<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Attempted to disable interval "
               "alarm failed. [err=%d]\n",err);
   }

   do
   {
      /* initialize return value */
      status=RtcOk;
      
      /* get the current time from the interval timer */
      if ((err=IntervalTimerGet(&now,&tics))<=0)
      {
         /* log the message */
         LogEntry(FuncName,"Attempt to read interval "
                  "timer failed. [err=%d]\n",err);
         
         /* indicate failure */
         status=RtcAlarmReadFail;
      }

      /* validate the interval-timer */
      if (now<0 || (now+MaxAlarmSec)>=MTimeMax || (now+MaxAlarmSec)<now)
      {
         /* log the error */
         LogEntry(FuncName,"Resetting interval-timer to 0 sec.\n");
         
         /* reset the interval-time to zero */
         if ((err=IntervalTimerSet((now=0),0))<=0)
         {
            /* log the message */
            LogEntry(FuncName,"Attempt to reset interval "
                     "timer failed. [err=%d]\n",err);

            /* indicate failure */
            status=RtcWriteFail;
         }
      }

      /* sanity check of alarm time */
      if (difftime(alarm,now)<MinAlarmSec)
      {
         /* log the message */
         LogEntry(FuncName,"Alarm time (%ld sec) less than %ld sec after "
                  "interval-timer (%ld sec).  Resetting to %ld sec.\n",
                  alarm,MinAlarmSec,now,now+MinAlarmSec);

         /* reset the alarm time */
         alarm = now + MinAlarmSec;
      }

      /* sanity check of alarm time */
      else if (difftime(alarm,now)>MaxAlarmSec)
      {
         /* log the message */
         LogEntry(FuncName,"Alarm time (%ld sec) more than %ld sec after "
                  "interval-timer (%ld sec).  Resetting to %ld sec.\n",
                  alarm,MaxAlarmSec,now,now+MaxAlarmSec);
         
         /* reset the alarm time */
         alarm = now + MaxAlarmSec;
      }

      /* write the alarm to the Stm32 and then read it back */
      if ((err=IntervalAlarmSet(alarm,0))<=0 || (err=IntervalAlarmGet(&alarmcheck,&tics))<=0)
      {
         /* log the message */
         LogEntry(FuncName,"Attempt to set interval "
                  "alarm failed. [err=%d]\n",err);

         /* indicate failure */
         status=RtcFail;
      }

      /* verify that the alarm is properly set */
      else if (alarm!=alarmcheck)
      {
         /* log the message */
         LogEntry(FuncName,"Verification of interval alarm failed: "
                  "%ld sec != %ld sec.\n",alarm,alarmcheck);

         /* indicate failure */
         status=RtcFail;
      }
      
      /* log the current interval-timer value and alarm */
      else if (debuglevel>=3)
      {
         /* log the message */
         LogEntry(FuncName,"Success: itimer=%ld sec, "
                  "ialarm=%ld sec\n",now,alarm);
      }
   }
   while (status<RtcOk && (tries++)<3);

   /* enable interval-alarm interrupts */
   if ((err=IntervalAlarmEnable())<=0)
   {
      /* log the message */
      LogEntry(FuncName,"Attempted to enable interval "
               "alarm failed. [err=%d]\n",err);

      /* indicate failure */
      status=RtcAlarmNotSet;
   }
   
   return status; 
}
 
/*------------------------------------------------------------------------*/
/* APF9 heap manager                                                      */
/*------------------------------------------------------------------------*/
/**
  This function allocates blocks of memory from the heap to be used
  for dynamic memory allocation. The heap is based at the upper end of
  the BSS segment and the address at the top of the heap grows upward
  as heap is consumed.  The stack is based at the upper end of the
  Stm32's volatile SRAM and grows downward towards the top of the
  heap.  The boundary between the heap and the stack is merely
  advisory.  This function enforces the upper boundary on the top of
  the heap; it will fail to allocate heap space if a request is larger
  than the available heap space.  However, nothing enforces the
  downward growth of the stack and so it is possible for a heap/stack
  collision to happen if the stack grows too large.

     \begin{verbatim}
        input:
           size....The number of bytes to be reserved from the heap.

        output:
           If sufficient heap space is available to fulfill the request,
           this function returns a pointer to the beginning of the newly
           allocated block.  If insufficient heapspace is available then
           this function returns the value: (void *)(-1);
     \end{verbatim}
*/
void *_sbrk(int size)
{
   /* define the logging signature */
   cc *FuncName = "sbrk()";

   /* initialize the return value */
   unsigned char *cp=(unsigned char *)(-1);

    /* static variable to point to the top of the heap */
   static void *HeapTop=0; if (!HeapTop) HeapTop=(unsigned char *)Apf11HeapBase;

   /* allocate more space from the heap only if size is positive */
	if (size>0)
   {
      /* compute the amount of available heap space */
      long int available = Apf11HeapHigh()+1;  available -= (unsigned long int)HeapTop;
      
      /* check if enough heap space exists to fulfill the request */
		if (available < size)
      {
         /* make the logentry */
         LogEntry(FuncName,"Error: Request for %d bytes exceeds available"
                  "(%ld bytes) heap space.\n",size,available);

         /* log the heap parameters */
         Apf11HeapLog();

         /* record the error via the standard C error facility */
         errno = ENOMEM;
      }
      
      /* reset the heap pointer */
      else {cp=HeapTop; HeapTop=(cp+size);}
   }
   else {cp=HeapTop;}
   
   /* record the address of the top of the heap */
   Apf11HeapTop=(unsigned long int)HeapTop;
   
   /* log the heap parameters */
   if (debuglevel>=4 || (debugbits&APF11_H)) Apf11HeapLog();

   /* return a pointer to the beginning of the newly allocated space */
	return cp;
}

/*------------------------------------------------------------------------*/
/* function to pause program execution for a specified number of seconds  */
/*------------------------------------------------------------------------*/
/**
   This function pauses execution of the program for a specified number of
   seconds.  This function is compliant with the POSIX standard.

      \begin{verbatim}
      input:
         sec ... The number of seconds to pause.

      output:
         The POSIX standard allows requires that the sleep() function return the
         number of seconds left in the sleep period.  For UNIX systems this
         can be nonzero but for the APF9 this function will always return zero.
      \end{verbatim}
*/
unsigned int sleep(unsigned int sec)
{
   /* wait for the specified number of seconds */
   while (sec>0) {Wait(1000); --sec;}
   
   return sec;
}

/*------------------------------------------------------------------------*/
/* pause program execution for a specified number of microseconds         */
/*------------------------------------------------------------------------*/
/**
   This function pauses execution of the program for a specified number of
   microseconds.  This function is compliant with the POSIX standard.

      \begin{verbatim}
      input:
         usec ... The number of useconds to pause.
      \end{verbatim}
*/
int usleep(unsigned long int usec)
{
   unsigned long sec = usec/1000000; usec%=1000000;
   
   /* wait for the specified number of seconds */
   while (sec>0) {Wait(1000); --sec;}

   /* wait for the fractional parts of seconds */
   if (usec>0) Wait(usec/1000);

   return 0;
}

/*------------------------------------------------------------------------*/
/* function to pause for a specified number of milliseconds               */
/*------------------------------------------------------------------------*/
int Wait(unsigned long int millisec) {HAL_Delay(millisec); return 1;}

#endif /* APF11_C */
