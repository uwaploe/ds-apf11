#ifndef APF11COM_H
#define APF11COM_H (0x0020U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Description of the SwiftWare API for the spare UARTS 
   ----------------------------------------------------

   The SwiftWare UART model is non-blocking for both received and
   transmitted data but is interrupt-driven only for received data.
   Even at the slowest clock speed (8MHz), the Apf11 core remains
   considerably underutilized with respect to processor cycles.
   Moreover, the Max3109 includes a 128 byte FIFO which further
   relieves processor loads.
  
   For received data, an interrupt handler manages the transfer of
   each byte from the Max3109's Rx register to the UART's FIFO buffer.
   The interrupt handler also detects and maintains a record of
   over-run, noise, framing, and parity errors.  If too many errors
   are detected, the handler stops counting when the number of any
   given type of error exceeds 255 (ie., the maximum capacity of
   unsigned char).
  
   For transmitted data, the SwiftWare UART model is not
   interrupt-driven so that the precise timing of transmitted
   characters can be known by higher level software that uses this
   low-level API.  Instead of allowing an interrupt-handler to manage
   the transmission of each byte, this API implements a function to
   transmit a single character by loading the byte in the Tx register
   of the Max3109 and then monitoring to detect completion of the
   transmission of the byte.  After each byte is transmitted then the
   function returns so that the next byte can be transmitted.  A
   time-out feature prevents blocking.

   The Apf11's spare sensor interface consists of two serial ports
   (com1, com2) on connectors J11 and J14, respectively.  A single
   Max3109 DUART services both serial ports.  The two serial ports are
   completely independent from each other and may be used either alone
   or in combination.  The implemented baud rates are 1200, 2400,
   4800, 9600, 19200, 38400 and the implemented modes are N81, E71, O71.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define apf11ComChangeLog "$RCSfile$  $Revision$  $Date$"

#include <serial.h>

/* declare functions with external linkage */
int Com1Cts(void);
int Com1Disable(void);
int Com1Enable(unsigned int BaudRate);
int Com1Errors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
               unsigned char *ovrun, unsigned char *noise);
int Com1IsEnable(void);
int Com1RtsAssert(void);
int Com1RtsClear(void);
int Com1TxBreak(int millisec);
int Com1TxBreakAssert(void);
int Com1TxBreakClear(void);
int Com2Cts(void);
int Com2Disable(void);
int Com2Enable(unsigned int BaudRate);
int Com2Errors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
               unsigned char *ovrun, unsigned char *noise);
int Com2IsEnable(void);
int Com2RtsAssert(void);
int Com2RtsClear(void);
int Com2TxBreak(int millisec);
int Com2TxBreakAssert(void);
int Com2TxBreakClear(void);

/* extern references to expansion serial ports */
extern const struct SerialPort com1,com2;

#endif /* APF11COM_H */
#ifdef APF11COM_C

#include <fifo.h>
#include <limits.h>
#include <logger.h>
#include <max3109.h>
#include <serial.h>
#include <string.h>
#include <Stm32f103Exti.h>
#include <Stm32f103Gpio.h>

/* prototypes for functions with static linkage */
static long int Com1(long int BaudRate);
static int      Com1Getb(unsigned char *byte);
static int      Com1Iflush(void);
static int      Com1IBytes(void);
static int      Com1Putb(unsigned char byte);
static int      Com1Rts(int state);
static int      Com1TransceiverEnable(void);
static int      Com1TransceiverDisable(void);
static int      Com1TransceiverIsEnable(void);
static long int Com2(long int BaudRate);
static int      Com2Getb(unsigned char *byte);
static int      Com2IBytes(void);
static int      Com2Iflush(void);
static int      Com2Putb(unsigned char byte);
static int      Com2Rts(int state);
static int      Com2TransceiverEnable(void);
static int      Com2TransceiverDisable(void);
static int      Com2TransceiverIsEnable(void);
static int      na(void);
static int      na_(int state);

/* define a fifo buffer for the Com1 serial port */
persistent static unsigned char Com1FifoBuf[10240U];

/* define a Fifo object for the Com1 serial port */
static struct Fifo Com1Fifo = {Com1FifoBuf, sizeof(Com1FifoBuf), 0, 0, 0, 0};

/* define a serial port for the Com1 interface */
const struct SerialPort com1 = {Com1Getb, Com1Putb, Com1Iflush, Com1Iflush, na, Com1IBytes,
                                na, na, Com1Rts, Com1Cts, na_, na, Com1};

/* define a fifo buffer for the Com2 serial port */
persistent static unsigned char Com2FifoBuf[10240U];
 
/* define a Fifo object for the Com2 serial port */
static struct Fifo Com2Fifo ={Com2FifoBuf, sizeof(Com2FifoBuf), 0, 0, 0, 0};

/* define a serial port for the Com2 interface */
const struct SerialPort com2 = {Com2Getb, Com2Putb, Com2Iflush, Com2Iflush, na, Com2IBytes,
                                na, na, Com2Rts, Com2Cts, na_, na, Com2};

/* declare external functions used locally */
int GpsDisable(void);
unsigned long int HAL_GetTick(void);
int ModemDisable(void);
int SpareDuartIsr(void);
int Wait(unsigned long int millisec);

/* define objects to contain counts of Max3109 line status errors */
UartStatus Com1Status = {0,0,0,0,0}, Com2Status = {0,0,0,0,0};

/*------------------------------------------------------------------------*/
/* function to enable/disable Com1                                        */
/*------------------------------------------------------------------------*/
/**
   This function is used to enable or disable Com1.  To enable the com port
   then set the argument equal to the desired baud rate.  To disable the com
   port then set the argument less than or equal to zero. 

      \begin{verbatim}
      input:

         BaudRate...This parameter controls functionality.

                    - The value LONG_MAX is a sentinel value that
                      requests the SerialPort identifer to be returned.
                    - Zero requests that the com port be disabled.
                    - A positive value requests that the com port be
                      enabled with the specified baud rate.
                    - A negative value determines the state of the
                      serial port.

      output:
         If the argument of this function is non-negative then a
         positive value is returned if successful or else zero upon
         failure.  If the argument is negative then the baud rate is
         returned if the serial port is enabled or else zero is
         returned if the serial port is disabled. In all cases, a
         negative return value indicates an exception.
      \end{verbatim}
*/
static long int Com1(long int BaudRate)
{
   int status=1;

   /* check for sentinel value to query com id */
   if (BaudRate==LONG_MAX) {status=1;}

   /* configure the serial port if BaudRate is positive */
   else if (BaudRate>0) {status=Com1Enable(BaudRate);}

   /* disable the serial port if BaudRate is zero */
   else if (!BaudRate) {status=Com1Disable();}

   /* determine serial port state */
   else {status=Com1IsEnable();}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the state of the CTS line of com1                   */
/*------------------------------------------------------------------------*/
/**
   This function retuns the state of the CTS line of the com1 serial
   port.  If CTS is asserted, pin 8 of J11 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J11:8 is
   asserted if the Max3109 CTS signalis clear.  This function returns
   1 if CTS is asserted or zero otherwise.
*/
int Com1Cts(void)
{
   /* read the CTS pin of the spare-sensor Max3109:UART0 */
   return ((Max3109CtsIsClear(Spi1DevSpDuart,UART0)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to disable com1                                               */
/*------------------------------------------------------------------------*/
/**
   This function disables UART0 of the spare-sensor DUART (Max3109).
   If the UART1 transceiver is enabled then the UART0 transceiver is
   disabled.  On the other hand, if the UART1 transceiver is disabled
   then both the Max3109 and the UART0 are disabled.  If successful,
   then this function returns a positive value; zero is returned, on
   failure. 
*/
int Com1Disable(void)
{
   /* initialize return value */
   int status=1;

   /* flush the Tx/Rx buffers of UART0 */
   Max3109Flush(Spi1DevSpDuart,UART0); 

   /* if COM2 transceiver is not enabled then disable the Max3109 */
   if (Com2TransceiverIsEnable()<=0)
   {
      /* disable the IRQ for the spare duart */
      Stm32f103ExtiIrqDisable(SpareDuartIrq);
      
      /* disable the Max3109 */
      status = ((Max3109Disable(Spi1DevSpDuart)>0) ? 1 : 0);
   }

   /* COM2 transceiver is enabled so disable only the COM1 transceiver */
   else {status = ((Com1TransceiverDisable()>0) ? 1 : 0);}

   /* flush the fifo for com1 */
   Wait(50); flush(&Com1Fifo);

   /* reinitialize the error counters */
   memset((void *)(&Com1Status),0,sizeof(Com1Status));

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable com1                                                */
/*------------------------------------------------------------------------*/
/**
   The function enables UART0 of the spare-sensor DUART (Max3109).
   The baud rate is capped at the upper end by 38400 due to the fact
   that, at higher speeds, the Max3109 interrupts can not be serviced
   fast enough to prevent overflow of the Max3109's Rx FIFOs.

   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int Com1Enable(unsigned int BaudRate)
{
   /* define the logging signature */
   cc *FuncName="Com1Enable()";

   /* initialize the return value */
   int err[4],dev,status = 0;

   /* avoid conflicts on the SPI bus */
   switch ((dev=Spi1Select(SpiDevQuery)))
   {
      /* ignore meta-devices */
      case SpiDevQuery: case Spi1Dev: {break;}

      /* ignore the Spare DUART on SPI bus 1 */
      case Spi1DevSpDuart: {break;}

      /* steal the SPI bus away from the Modem/Gps */
      case Spi1DevRfDuart: {ModemDisable(); GpsDisable(); Spi1Select(Spi1Dev); break;}

      /* manage unimplemented SPI devices */
      default: {LogEntry(FuncName,"Device(%d) not implemented on SPI bus 1.\n",dev);}
   }

   /* switch statement to enforce valid configurations */
   switch (BaudRate)
   {
      /* enumerate the implemented baud rates */
      case 1200: case 2400:  case 4800:
      case 9600: case 19200: case 38400:
      {
         /* configure the communications paramters */
         if ((err[0]=Max3109Config(Spi1DevSpDuart,UART0,BaudRate,N81))>0      &&
             (err[1]=Max3109RegWrite(Spi1DevSpDuart,UART0,GPIOConfig,0x0f))>0 &&
             (err[2]=Max3109RtsAssert(Spi1DevSpDuart,UART0))>0                &&
             (err[3]=Com1TransceiverEnable())>0)
         {
            /* enable the IRQ for the spare duart */
            Stm32f103ExtiIrqEnable(SpareDuartIrq,Falling);

            /* flush the Tx/Rx buffers of UART0 */
            Wait(50); Max3109Flush(Spi1DevSpDuart,UART0);
            
            /* reinitialize the error counters */
            memset((void *)(&Com1Status),0,sizeof(Com1Status));
            
            /* flush the FIFO */
            flush(&Com1Fifo); status=1;
         }

         /* log the configuration error */
         else {LogEntry(FuncName,"Attempt to configure Com1 failed. "
                        "[err={%d,%d,%d,%d}]\n",err[0],err[1],err[2],err[3]);}
         
         break;
      }

      /* catch invalid or nonimplemented baud rate configuration */
      default: {LogEntry(FuncName,"Nonimplemented baud rate: %u\n",BaudRate);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the Com1 interrupt         */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Com1
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int Com1Errors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
               unsigned char *ovrun, unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      Com1Status.nbreak=0; Com1Status.parity=0; Com1Status.frame=0;
      Com1Status.ovrun=0;  Com1Status.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=Com1Status.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=Com1Status.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=Com1Status.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=Com1Status.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=Com1Status.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the Com1 FIFO queue                       */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the Com1 serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the Com1 FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int Com1Getb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "Com1Getb()";

   /* initialize the return value */
   int status=0;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   /* pop the next byte out of the fifo queue */
   else {status=pop(&Com1Fifo,byte);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue            */
/*------------------------------------------------------------------------*/
static int Com1IBytes(void)
{
   return Com1Fifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the Com1's Rx FIFO queue                             */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the Com1 serial port.  This
   function returns a positive value on success and zero on failure.  A
   negative return value indicates an invalid or corrupt Fifo object.
*/
static int Com1Iflush(void)
{
   int status=0;

   /* flush any bytes in the Max3109:UART0's FIFOs */
   Max3109Flush(Spi1DevSpDuart,UART0);

   /* flush the Com1 serial port's fifo buffer */
   status=flush(&Com1Fifo);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* test if com1 is enabled                                                */
/*------------------------------------------------------------------------*/
/**
   This function determines if the transceiver for UART0 is enabled.
   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int Com1IsEnable(void)
{
   /* initialize the return value */
   int status=0; unsigned long int baud;

   /* test if the UART0 transceiver is enabled */
   if (Com1TransceiverIsEnable()>0)
   {
      /* retrieve the baud rate from UART0 */
      status = (Max3109BaudRateGet(Spi1DevSpDuart,UART0,&baud)>0) ? baud : 0;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write one byte to spare-sensor Max3109:UART0 serial port   */
/*------------------------------------------------------------------------*/
/**
   This function writes one byte to the spare-sensor Max3109:UART0 and
   pauses until that byte is transmitted by the serial port.  

   \begin{verbatim}
   input:
      byte...This is the byte to add to the Tx fifo.

   output:
      This function returns a positive value on success or zero on
      failure. 
   \end{verbatim}
*/
static int Com1Putb(unsigned char byte)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a timeout limit */
   const unsigned long int timeout=20;

   /* initialize the reference time for the timeout loop */
   const unsigned long int To=HAL_GetTick();

   /* write the byte to the spare-sensor Max3109:UART0 */
   if ((err=Max3109Putb(Spi1DevSpDuart,UART0,byte))<=0) {status=Max3109Fail;}

   /* loop to wait until the byte has been transmitted */
   else while ((HAL_GetTick()-To)<timeout)
   {
      /* wait until the Tx FIFO is empty */
      if (!Max3109OBytes(Spi1DevSpDuart,UART0)) {break;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert/clear the RTS signal of Com1                        */
/*------------------------------------------------------------------------*/
/**
   This function is used to assert or clear the RTS signal for Com1.  To
   assert the RTS line then set the argument to be positive.  To clear the
   RTS line then set the argument to be zero or negative.  This function
   returns a positive value on success; otherwise zero is returned.
*/
static int Com1Rts(int state)
{
   int status = (state>0) ? Com1RtsAssert() : Com1RtsClear();

   return ((status>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert the RTS line of the com1 serial port                */
/*------------------------------------------------------------------------*/
/**
   This function asserts the RTS line of the com1 serial port; pin 6
   of J11 will measure +5V.  The transceiver inverts the sense of the
   Max3109 signal and so asserting J11:6 requires clearing the Max3109
   signal. If successful, a positive value is returned; zero is
   returned on failure.
*/
int Com1RtsAssert(void)
{
   /* assert the RTS pin of the spare-sensor Max3109:UART0 */
   return ((Max3109RtsClear(Spi1DevSpDuart,UART0)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the RTS line of the com1 serial port                 */
/*------------------------------------------------------------------------*/
/**
   This function clears the RTS line of the com1 serial port; pin 6
   of J11 will measure -5V.  The transceiver inverts the sense of the
   Max3109 signal and so clearing J11:6 requires asserting the Max3109
   signal.  If successful, a positive value is returned; zero is
   returned on failure.
*/
int Com1RtsClear(void)
{
   /* clear the RTS pin of the spare-sensor Max3109:UART0 */
   return ((Max3109RtsAssert(Spi1DevSpDuart,UART0)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to disable the transceiver for the COM1 UART                  */
/*------------------------------------------------------------------------*/
/**
   The function to disable the transceiver for the COM1 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com1TransceiverDisable(void)
{
   /* initialize return value */
   int status=Max3109RegWrite(Spi1DevSpDuart,UART0,GPIOData,0x00U); 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the transceiver for the COM1 UART                   */
/*------------------------------------------------------------------------*/
/**
   The function to enable the transceiver for the COM1 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com1TransceiverEnable(void)
{
   /* initialize return value */
   int status=Max3109RegWrite(Spi1DevSpDuart,UART0,GPIOData,0x0eU); 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the transceiver for the COM1 UART                   */
/*------------------------------------------------------------------------*/
/**
   The function to enable the transceiver for the COM1 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com1TransceiverIsEnable(void)
{
   /* initialize return value */
   int err,status=Max3109Ok; unsigned char byte;

   /* read the GPIO register from the Max3109 */
   if ((err=Max3109RegRead(Spi1DevSpDuart,UART0,GPIOData,&byte))<=0) {status=err;}

   /* criteria to determine if transceiver is enabled */
   else {status = (((byte&0x0aU)==0x0aU) ? 1 : 0);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute a Tx-break signal for a specified period of time   */
/*------------------------------------------------------------------------*/
/**
   This function executes a Tx-break signal for a specified period of time.

   \begin{verbatim}
   input:

      millisec...This specifies the period of time (millisec) that the
                 break signal is maintained.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Com1TxBreak(int millisec)
{
   return ((Max3109TxBreak(Spi1DevSpDuart,UART0,millisec)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert the break signal on the Tx pin of com1              */
/*------------------------------------------------------------------------*/
/**
   This function asserts the break signal on the Tx pin of com1.  If
   successful, a positive value is returned or else zero, on failure.
*/
int Com1TxBreakAssert(void)
{
   return ((Max3109TxBreakAssert(Spi1DevSpDuart,UART0)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the break signal on the Tx pin of com1               */
/*------------------------------------------------------------------------*/
/**
   This function clears the break signal on the Tx pin of com1.  If
   successful, a positive value is returned or else zero, on failure.
*/
int Com1TxBreakClear(void)
{
   return ((Max3109TxBreakClear(Spi1DevSpDuart,UART0)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to enable/disable Com2                                        */
/*------------------------------------------------------------------------*/
/**
   This function is used to enable or disable Com2.  To enable the com port
   then set the argument equal to the desired baud rate.  To disable the com
   port then set the argument less than or equal to zero. 

      \begin{verbatim}
      input:

         BaudRate...This parameter controls functionality.

                    - The value LONG_MAX is a sentinel value that
                      requests the SerialPort identifer to be returned.
                    - Zero requests that the com port be disabled.
                    - A positive value requests that the com port be
                      enabled with the specified baud rate.
                    - A negative value determines the state of the
                      serial port.

      output:
         If the argument of this function is non-negative then a
         positive value is returned if successful or else zero upon
         failure.  If the argument is negative then the baud rate is
         returned if the serial port is enabled or else zero is
         returned if the serial port is disabled. In all cases, a
         negative return value indicates an exception.
      \end{verbatim}
*/
static long int Com2(long int BaudRate)
{
   int status=1;

   /* check for sentinel value to query com id */
   if (BaudRate==LONG_MAX) {status=2;}

   /* configure the serial port if BaudRate is positive */
   else if (BaudRate>0) {status=Com2Enable(BaudRate);}

   /* disable the serial port if BaudRate is zero */
   else if (!BaudRate) {status=Com2Disable();}

   /* determine serial port state */
   else {status=Com2IsEnable();}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the state of the CTS line of com2                   */
/*------------------------------------------------------------------------*/
/**
   This function retuns the state of the CTS line of the com2 serial
   port.  If CTS is asserted, pin 8 of J14 will measure +5V. The
   transceiver inverts the sense of the Max3109 signal and so J11:8 is
   asserted if the Max3109 CTS signalis clear.  This function returns
   1 if CTS is asserted or zero otherwise.
*/
int Com2Cts(void)
{
   /* read the CTS pin of the spare-sensor Max3109:UART1 */
   return ((Max3109CtsIsClear(Spi1DevSpDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to disable com2                                               */
/*------------------------------------------------------------------------*/
/**
   This function disables UART1 of the spare-sensor DUART (Max3109).
   If the UART0 transceiver is enabled then the UART1 transceiver is
   disabled.  On the other hand, if the UART0 transceiver is disabled
   then both the Max3109 and the UART1 are disabled.  If successful,
   then this function returns a positive value; zero is returned, on
   failure. 
*/
int Com2Disable(void)
{
   /* initialize return value */
   int status=1;

   /* flush the Tx/Rx buffers of UART0 */
   Wait(50); Max3109Flush(Spi1DevSpDuart,UART1);

   /* if COM1 transceiver is not enabled then disable the Max3109 */
   if (Com1TransceiverIsEnable()<=0)
   {
      /* disable the IRQ for the spare duart */
      Stm32f103ExtiIrqDisable(SpareDuartIrq);
      
      /* disable the Max3109  */
      status = ((Max3109Disable(Spi1DevSpDuart)>0) ? 1 : 0);
   }

   /* COM1 transceiver is enabled so disable only the UART1 transceiver */
   else {status = ((Com2TransceiverDisable()>0) ? 1 : 0);}

   /* flush the fifo for com2 */
   Wait(50); flush(&Com2Fifo);

   /* reinitialize the error counters */
   memset((void *)(&Com2Status),0,sizeof(Com2Status));

   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable com2                                                */
/*------------------------------------------------------------------------*/
/**
   The function enables UART1 of the spare-sensor DUART (Max3109).
   The baud rate is capped at the upper end by 38400 due to the fact
   that, at higher speeds, the Max3109 interrupts can not be serviced
   fast enough to prevent overflow of the Max3109's Rx FIFOs.  

   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int Com2Enable(unsigned int BaudRate)
{
   /* define the logging signature */
   cc *FuncName="Com2Enable()";

   /* initialize the return value */
   int err[4],dev,status = 0;

   /* avoid conflicts on the SPI bus */
   switch ((dev=Spi1Select(SpiDevQuery)))
   {
      /* ignore meta-devices */
      case SpiDevQuery: case Spi1Dev: {break;}

      /* ignore the Spare DUART on SPI bus 1 */
      case Spi1DevSpDuart: {break;}

      /* steal the SPI bus away from the Modem/Gps */
      case Spi1DevRfDuart: {ModemDisable(); GpsDisable(); Spi1Select(Spi1Dev); break;}

      /* manage unimplemented SPI devices */
      default: {LogEntry(FuncName,"Device(%d) not implemented on SPI bus 1.\n",dev);}
   }

   /* switch statement to enforce valid configurations */
   switch (BaudRate)
   {
      /* enumerate the implemented baud rates */
      case 1200: case 2400:  case 4800:
      case 9600: case 19200: case 38400:
      {
         /* configure the communications paramters */
         if ((err[0]=Max3109Config(Spi1DevSpDuart,UART1,BaudRate,N81))>0      &&
             (err[1]=Max3109RegWrite(Spi1DevSpDuart,UART1,GPIOConfig,0x0f))>0 &&
             (err[2]=Max3109RtsAssert(Spi1DevSpDuart,UART1))>0                &&
             (err[3]=Com2TransceiverEnable())>0)
         {
            /* enable the IRQ for the spare duart */
            Stm32f103ExtiIrqEnable(SpareDuartIrq,Falling);
            
            /* flush the Tx/Rx buffers of UART1 */
            Wait(50); Max3109Flush(Spi1DevSpDuart,UART1);
            
            /* reinitialize the error counters */
            memset((void *)(&Com2Status),0,sizeof(Com2Status));
            
            /* flush the FIFO */
            flush(&Com2Fifo); status=1;
         }

         /* log the configuration error */
         else {LogEntry(FuncName,"Attempt to configure Com2 failed. "
                        "[err={%d,%d,%d,%d}]\n",err[0],err[1],err[2],err[3]);}
         
         break;
      }

      /* catch invalid or nonimplemented baud rate configuration */
      default: {LogEntry(FuncName,"Nonimplemented baud rate: %u\n",BaudRate);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* report the number of errors encountered by the Com2 interrupt         */
/*------------------------------------------------------------------------*/
/**
   This function reports the number of errors encountered by the Com2
   interrupt service routine.  The number of parity, framing,
   over-run, and noise errors as well as the number of
   break-conditions encountered are counted, up to 255 each.

      \begin{verbatim}
      input:
         nbreak....The number of break-conditions enountered.  
         parity....The number of parity errors encountered.
         frame.....The number of framing errors encountered.
         ovrun.....The number of over-run errors encountered.
         noise.....The number of noise errors encountered.

         Any given argument that is NULL will be safely ignored, if
         only a subset of error counters are needed.  If all arguments
         are NULL then this function reinitializes all of the counters
         to zero.

      output:

         This function returns the total of errors for non-NULL
         arguments; zero indicates no errors were encountered.  A
         negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
int Com2Errors(unsigned char *nbreak, unsigned char *parity, unsigned char *frame,
               unsigned char *ovrun, unsigned char *noise)
{
   /* initialize the return value */
   int nerror=0;
   
   /* if all arguments are NULL then reinitialize the error counters to zero */
   if (!nbreak && !parity && !frame && !ovrun && !noise)
   {
      /* initialize the error counters to zero */
      Com2Status.nbreak=0; Com2Status.parity=0; Com2Status.frame=0;
      Com2Status.ovrun=0;  Com2Status.noise=0; 
   }
   else
   {
      /* check request for number of breaks encountered */      
      if (nbreak) {if (((*nbreak)=Com2Status.nbreak) > 0) {nerror+=(*nbreak);}}

      /* check request for number of parity errors encountered */
      if (parity) {if (((*parity)=Com2Status.parity) > 0) {nerror+=(*parity);}}

      /* check request for number of framing errors encountered */
      if (frame)  {if (((*frame)=Com2Status.frame)   > 0) {nerror+=(*frame);}}
      
      /* check request for number of over-run errors encountered */
      if (ovrun)  {if (((*ovrun)=Com2Status.ovrun)   > 0) {nerror+=(*ovrun);}}

      /* check request for number of noise errors encountered */
      if (noise)  {if (((*noise)=Com2Status.noise)   > 0) {nerror+=(*noise);}}
   }
   
   return nerror;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the Com2 FIFO queue                       */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the FIFO queue of the Com2 serial
   port.  

      \begin{verbatim}
      output:
         byte...The next byte from the Com2 FIFO queue.

         This function returns a positive number on success and zero on
         failure.  A negative return value indicates an exception was
         encountered.
      \end{verbatim}
*/
static int Com2Getb(unsigned char *byte)
{
   /* define the logging signature */
   cc *FuncName = "Com2Getb()";

   /* initialize the return value */
   int status=0;

   /* initialize the return value */
   if (byte) {*byte=0;}
   
   /* validate the function parameter */
   if (!byte) {LogEntry(FuncName,"NULL function argument.\n"); status=-1;}

   /* pop the next byte out of the fifo queue */
   else {status=pop(&Com2Fifo,byte);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Rx FIFO queue            */
/*------------------------------------------------------------------------*/
static int Com2IBytes(void)
{
   return Com2Fifo.length;
}

/*------------------------------------------------------------------------*/
/* function to flush the Com2's Rx FIFO queue                             */
/*------------------------------------------------------------------------*/
/**
   This function flushes the Rx FIFO queue of the Com2 serial port.  This
   function returns a positive value on success and zero on failure.  A
   negative return value indicates an invalid or corrupt Fifo object.
*/
static int Com2Iflush(void)
{
   int status=0;

   /* flush any bytes in the Max3109:UART1's FIFOs */
   Max3109Flush(Spi1DevSpDuart,UART1);

   /* flush the Com2 serial port's fifo buffer */
   status=flush(&Com2Fifo);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* test if com2 is enabled                                                */
/*------------------------------------------------------------------------*/
/**
   This function determines if the transceiver for UART1 is enabled.
   If successful, then this function returns a positive value; zero is
   returned, on failure.
*/
int Com2IsEnable(void)
{
   /* initialize the return value */
   int status=0; unsigned long int baud;

   /* test if the COM2 transceiver is enabled */
   if (Com2TransceiverIsEnable()>0)
   {
      /* retrieve the baud rate from UART1 */
      status = ((Max3109BaudRateGet(Spi1DevSpDuart,UART1,&baud)>0) ? baud : 0);
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to write one byte to spare-sensor Max3109:UART1 serial port   */
/*------------------------------------------------------------------------*/
/**
   This function writes one byte to the spare-sensor Max3109:UART1 and
   pauses until that byte is transmitted by the serial port.  

   \begin{verbatim}
   input:
      byte...This is the byte to add to the Tx fifo.

   output:
      This function returns a positive value on success or zero on
      failure.
   \end{verbatim}
*/
static int Com2Putb(unsigned char byte)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a timeout limit */
   const unsigned long int timeout=20;

   /* initialize the reference time for the timeout loop */
   const unsigned long int To=HAL_GetTick();

   /* write the byte to the spare-sensor Max3109:UART0 */
   if ((err=Max3109Putb(Spi1DevSpDuart,UART1,byte))<=0) {status=Max3109Fail;}

   /* loop to wait until the byte has been transmitted */
   else while ((HAL_GetTick()-To)<timeout)
   {
      /* wait until the Tx FIFO is empty */
      if (!Max3109OBytes(Spi1DevSpDuart,UART1)) {break;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert the RTS line of the com2 serial port                */
/*------------------------------------------------------------------------*/
/**
   This function asserts the RTS line of the com2 serial port; pin 6
   of J14 will measure +5V.  The transceiver inverts the sense of the
   Max3109 signal and so asserting J11:6 requires clearing the Max3109
   signal. If successful, a positive value is
   returned; zero is returned on failure.
*/
int Com2RtsAssert(void)
{
   /* assert the RTS pin of the spare-sensor Max3109:UART1 */
   return ((Max3109RtsClear(Spi1DevSpDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the RTS line of the com2 serial port                 */
/*------------------------------------------------------------------------*/
/**
   This function asserts the RTS line of the com2 serial port; pin 6
   of J14 will measure +5V.  The transceiver inverts the sense of the
   Max3109 signal and so clearing J11:6 requires asserting the Max3109
   signal.  If successful, a positive value is
   returned; zero is returned on failure.
*/
int Com2RtsClear(void)
{
   /* clear the RTS pin of the spare-sensor Max3109:UART1 */
   return ((Max3109RtsAssert(Spi1DevSpDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert/clear the RTS signal of Com2                        */
/*------------------------------------------------------------------------*/
/**
   This function is used to assert or clear the RTS signal for Com2.  To
   assert the RTS line then set the argument to be positive.  To clear the
   RTS line then set the argument to be zero or negative.  This function
   returns a positive value on success; otherwise zero is returned.
*/
static int Com2Rts(int state)
{
   int status = (state>0) ? Com2RtsAssert() : Com2RtsClear();

   return ((status>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to disable the transceiver for the COM2 UART                  */
/*------------------------------------------------------------------------*/
/**
   The function to disable the transceiver for the COM2 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com2TransceiverDisable(void)
{
   /* initialize return value */
   int status=Max3109RegWrite(Spi1DevSpDuart,UART1,GPIOData,0x00U); 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the transceiver for the COM2 UART                   */
/*------------------------------------------------------------------------*/
/**
   The function to enable the transceiver for the COM2 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com2TransceiverEnable(void)
{
   /* initialize return value */
   int status=Max3109RegWrite(Spi1DevSpDuart,UART1,GPIOData,0x0eU); 
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable the transceiver for the COM2 UART                   */
/*------------------------------------------------------------------------*/
/**
   The function to enable the transceiver for the COM2 UART. If
   successful, this function returns a positive value or else zero, on
   failure.  A negative return value indicates that an exception was
   encountered.
*/
int Com2TransceiverIsEnable(void)
{
   /* initialize return value */
   int err,status=Max3109Ok; unsigned char byte;

   /* read the GPIO register from the Max3109 */
   if ((err=Max3109RegRead(Spi1DevSpDuart,UART1,GPIOData,&byte))<=0) {status=err;}

   /* criteria to determine if transceiver is enabled */
   else {status = (((byte&0x0aU)==0x0aU) ? 1 : 0);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute a Tx-break signal for a specified period of time   */
/*------------------------------------------------------------------------*/
/**
   This function executes a Tx-break signal for a specified period of time.

   \begin{verbatim}
   input:

      millisec...This specifies the period of time (millisec) that the
                 break signal is maintained.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Com2TxBreak(int millisec)
{
   return ((Max3109TxBreak(Spi1DevSpDuart,UART1,millisec)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to assert the break signal on the Tx pin of com2              */
/*------------------------------------------------------------------------*/
/**
   This function asserts the break signal on the Tx pin of com2.  If
   successful, a positive value is returned or else zero, on failure.
*/
int Com2TxBreakAssert(void)
{
   return ((Max3109TxBreakAssert(Spi1DevSpDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to clear the break signal on the Tx pin of com2               */
/*------------------------------------------------------------------------*/
/**
   This function clears the break signal on the Tx pin of com2.  If
   successful, a positive value is returned or else zero, on failure.
*/
int Com2TxBreakClear(void)
{
   return ((Max3109TxBreakClear(Spi1DevSpDuart,UART1)>0) ? 1 : 0);
}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na(void) {return -1;}

/*------------------------------------------------------------------------*/
/* function to indicate that a serial port abstractionis NOT AVAILABLE    */
/*------------------------------------------------------------------------*/
/**
   This function is used to fulfill the requirements of the serial port
   abstraction, In the case where the serial port lacks the ability to
   perform a given function, this function should be substituted in the
   initialization of the SerialPort object.
*/
static int na_(int state)
{
   return -1;
}

/*------------------------------------------------------------------------*/
/* interrupt handler for the Apf11's spare sensor serial interface        */
/*------------------------------------------------------------------------*/
/*
   This is the interrupt handler for the Apf11's spare sensor serial
   interface.  The SwiftWare UART model is interrupt-driven and
   non-blocking with respect to received data.  For transmitted data,
   the SwiftWare UART model is non-blocking but not interrupt-driven.
   This function also manages error interrupts and maintains a record
   of UART errors.

   This function returns a positive value on success or else zero on
   failure.  A negative return value indicates that an exception was
   encountered. 
*/
int SpareDuartIsr(void)
{
   /* initialize the return value */
   int status=0;

   /* define local work objects */
   int Xen; unsigned char irq,isr;
   
   /* read the IRQ bits from the Max3109 */
   if (Max3109Irq(Spi1DevSpDuart,&irq)>0)
   {
      /* test for IRQ on UART0 */
      if (irq&0x1)
      {
         /* clear the interrupt status register by reading it */
         Max3109RegRead(Spi1DevSpDuart,UART0,ISR,&isr);
         
         /* test if transceiver is enabled and for serviceable ISR conditions */
         if ((Xen=Com1TransceiverIsEnable())>0 && isr&0x09U)
         {
            /* pass a pointer to the UartStatus object if errors exist */
            UartStatus *comstat = (isr&0x01) ? &Com1Status : NULL;
            
            /* transfer the contents of the Max3109:UART0 Rx FIFO */
            Max3109BurstRx(Spi1DevSpDuart,UART0,&Com1Fifo,comstat);
         }

         /* clear the Rx/Tx FIFOs if the transceiver is not enabled */
         else if (!Xen) {Max3109Flush(Spi1DevSpDuart,UART0);}
      }
      
      /* test for IRQ on UART1 */
      if (irq&0x2)
      {
         /* clear the interrupt status register by reading it */
         Max3109RegRead(Spi1DevSpDuart,UART1,ISR,&isr);
         
         /* test if transceiver is enabled and for serviceable ISR conditions */
         if ((Xen=Com2TransceiverIsEnable())>0 && isr&0x09U)
         {
            /* pass a pointer to the UartStatus object if errors exist */
            UartStatus *comstat = (isr&0x01) ? &Com2Status : NULL;
            
            /* transfer the contents of the Max3109:UART1 Rx FIFO */
            Max3109BurstRx(Spi1DevSpDuart,UART1,&Com2Fifo,comstat);
         }

         /* clear the Rx/Tx FIFOs if the transceiver is not enabled */
         else if (!Xen) {Max3109Flush(Spi1DevSpDuart,UART1);}
      }
   }

   return status;
}

#endif /* APF11COM_C */
