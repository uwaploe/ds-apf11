#ifndef DS2740_H
#define DS2740_H (0x0008U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 
 * Description of the DS2740 Coulomb counter and associated API model
 * ------------------------------------------------------------------

   The Maxim DS2740 Coulomb counter measures the electrical current
   (e-current) passing through a spy-resistor (R=20mOhm) by measuring
   the voltage drop across that resistor.  An ADC samples the input
   differentially at the two ends of the spy resistor with an 18.6kHz
   sample clock.  The register that stores the e-current is updated at
   the completion of each 3.515sec conversion cycle.  The following
   paragraph was lifted from the DS2740 data sheet:

      Every 1024th conversion, the ADC measures its input offset to
      facilitate offset correction. Offset correction occurs
      approximately once per hour in the DS2740U. The resulting
      correction factor is applied to the subsequent 1023
      measurements. During the offset correction conversion, the ADC
      does not measure the IS1 to IS2 signal. A maximum error of
      1/1024 in the accumulated current register (ACR) is possible.
      However, to reduce the error, the current measurement just prior
      to the offset conversion is displayed in the current register
      and is substituted for the dropped current measurement in the
      current accumulation process. The typical error due to offset
      correction is much less than 1/1024.

   The data sheet does not say so explicitly but, based on observed
   behavior, the offset correction appears also to be measured during
   the first conversion cycle after power-up.  The first nonzero
   register contents was measured to be available 7.0sec after
   power-up.

   The DS2740 is capable of bidirectional measurement of e-current
   flow and so half of the 16-bit range is positive and the other half
   negative.  That is, the 16-bit word constructed from the MSB,LSB of
   the e-current and Coulomb registers is a signed 16-bit integer that
   is represented in 2's-complement form.

   The Apf11 incorporates the DS2740 as a low-side e-current
   measurement on the return-path to the negative terminal of the
   batteries.  Moreover, the DS2740 is integrated into the Apf11 with
   polarity such that e-currents are always negative and the Coulomb
   counter decrements starting from zero.  Since e-currents in the
   Apf11 always flow in one direction from the positive battery
   terminal to the negative battery terminal then measured e-currents
   are always negative; half the range of the e-current register is
   wasted.  Hypothetically, the Coulomb counter could be initialized
   to its maximum value so that it would have the full range of the
   16-bit counter to decrement before under-flowing.  However, the
   DS2740 is powered-down during while the Stm32 is in standby mode
   and the Coulomb register is automatically initialized to zero on
   power-up.  The Apf11 never consumes enough charge in one wake-cycle
   to require even half the available range of the Coulomb counter.

   The least-significant-bit (LSb) of the e-current register
   represents a a voltage differential of 1.5625uV across the
   spy-resistor.  Since the spy-resistor is 20mOhm then the LSb
   represents 78.125uA (=1.5625uV/20mOhm) of e-current.  With 15-bits
   of range, the maximum measurable current is 2.56A
   (=78.125uA*(2^15)).

   The LSb of the Coulomb (C) counter register represents 6.25uVh of
   time-integrated voltage differential.  Since the spy-resistor is
   20mOhm then the LSb represents 1.125C 
   [=6.25e-6Vh*(3600s/h)/(0.02Ohm)] of charge.  With 15-bits of range,
   the maximum measurable range of charge is 36.864kC
   (=1.125C*(2^15)).
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define ds2740ChangeLog "$RCSfile$  $Revision$  $Date$"

#include <mcu.h>

/*------------------------------------------------------------------------*/
/* structure to define the charge consumption model                       */
/*------------------------------------------------------------------------*/
typedef struct
{
   unsigned long int StandbySec, WakeCycles;
   float Coulombs;
} ChargeModel;
   
/* declare functions with external linkage */
unsigned short BatAmpsAdc(void);
unsigned short BatChargeAdc(void);
int            BatChargeReset(void);
unsigned char  DowCrc(const unsigned char *netaddr,unsigned int len);
int            DowNetAddress(unsigned long int *serno, unsigned short int *group,
                             unsigned char *family, unsigned char *crc);
int            DowReadByte(unsigned char *byte);
int            DowReset(void);
int            DowWriteByte(unsigned char byte);
int            ChargeModelAccumulate(unsigned long int StandbySec, unsigned char WakeCycles, float Coulombs);
int            ChargeModelCompute(float *charge);
int            ChargeModelInit(void);

/* external declarations for global objects */
extern persistent ChargeModel cmodel; 

/* define the conversion period (seconds) */
#define DowConvPeriod (3.515)

/* define the return states of the Stm32 ADC API */
extern const char DowNull;      /* NULL function argument */
extern const char DowFail;      /* general failure */
extern const char DowOk;        /* general success */

#endif /* DS2740_H */
#ifdef DS2740_C
#undef DS2740_C

#include <logger.h>
#include <nan.h>
#include <Stm32f103Gpio.h>
#include <Stm32f103Rcc.h>
#include <Stm32f103Timer.h>
#include <string.h>

/* define the return states of the Stm32 ADC API */
const char DowNull = -1; /* NULL function argument */
const char DowFail =  0; /* general failure */
const char DowOk   =  1; /* general success */

/* define a persistent object to maintain parameters needed for the charge model */
persistent ChargeModel cmodel; 

/* record the APB1 frequency used to drive Timer {2-6,12-14} */
static unsigned long int Apb1Hertz = 0;

/* use inline assembly NOPs to create a time delay of 2usec */
#define Delay2us {asm volatile("NOP"); asm volatile("NOP"); asm volatile("NOP");\
                  asm volatile("NOP"); asm volatile("NOP"); asm volatile("NOP");\
                  asm volatile("NOP");}

/* declare functions with static linkage */
static int DowReadBit(unsigned char *bit);
static int DowWriteBit(unsigned char bit);

/* prototypes for functions with external linkage */
int Wait(unsigned long int millisec);

/*------------------------------------------------------------------------*/
/* read the electrical current measurement from the DS2740                */
/*------------------------------------------------------------------------*/
/**
   This function reads the electrical current measurement from the
   DS2740.  Refer to "Description of the DS2740 Coulomb counter and
   associated API model" above for measurement details.  

   \begin{verbatim}
      This function returns the contents of the DS2740's 16-bit
      e-current register.  Three sentinel values are implemented to
      reflect special conditions:

         0x7fff: This sentinel value represents missing data, invalid
                 data, or some other pathological exception.  For
                 example, if the presence-signal is not received after
                 a reset of the Dallas One-Wire (DOW) interface then
                 this sentinel value is returned.

         0x8000: This sentinel value indicates an under-flow of the
                 e-current measurement.  An under-flow indicates that
                 the e-current was negative and out-of-range;
                 (e-current <= -2.56A).

         0x7ffe: This sentinel value indicates an over-flow of the
                 e-current measurement.  An over-flow indicates that
                 the e-current was positive and out-of-range;
                 (e-current >= +2.56A).  This condition should never
                 happen with the Apf11 because e-current flows from
                 the positive battery terminal to the negative battery
                 terminal is measured as negative.

      Except for the above three sentinel values, each bit of the
      return value represents 78.125uA of e-current.  That is, the
      total current consumed by the Apf11 is computed according to the
      formula: Amps = [(78.125e-6Amps)*BatAmpsAd16()].  A negative
      value means that e-current is consumed from the batteries.
   \end{verbatim}
*/
unsigned short BatAmpsAdc(void)
{
    /* initialize return value with invalid-sentinel */
    unsigned short ad16 = 0x7fff;

    /* induce the DS2740 into command-ready state */
    if (DowReset() > 0)
    {
        /* create local work objects */
        unsigned char msb = 0, lsb = 0;

        /* enumerate DS2740 commands and addresses */
        enum {SkipNetAddr = 0xCC, ReadData = 0x69, AmpMsbAddr = 0x0E};

        /* bypass addressing since DS2740 is only one-wire device */
        DowWriteByte(SkipNetAddr);

        /* execute the command to read the MSB of measured current */
        DowWriteByte(ReadData); DowWriteByte(AmpMsbAddr);

        /* read both MSB and LSB of the 16-bit current measurement */
        DowReadByte(&msb); DowReadByte(&lsb);

        /* assemble the 16-bit ADC of the current measurement */
        ad16 = ((msb << 8) | lsb);

        /* avoid conflating out-of-range with invalid-sentinel */
        if (ad16 == 0x7fff)
        {
            ad16 = 0x7ffe;
        }
    }

    return ad16;
}

/*------------------------------------------------------------------------*/
/* read accumulated electrical charge (Coulombs) measurement by DS2740    */
/*------------------------------------------------------------------------*/
/**
   This function reads the accumulated electrical charge (Coulombs)
   measured by the DS2740.  Refer to "Description of the DS2740
   Coulomb counter and associated API model" above for measurement
   details.

   \begin{verbatim}
      This function returns the contents of the DS2740's 16-bit
      Coulomb (C) counter register.  Three sentinel values are
      implemented to reflect special conditions:

         0x7fff: This sentinel value represents missing data, invalid
                 data, or some other pathological exception.  For
                 example, if the presence-signal is not received after
                 a reset of the Dallas One-Wire (DOW) interface then
                 this sentinel value is returned.

         0x8000: This sentinel value indicates an under-flow of the
                 Coulomb counter.  An under-flow indicates that the
                 electric charge consumption was negative and
                 out-of-range; (charge <= -36.864kC).

         0x7ffe: This sentinel value indicates an over-flow of the
                 Coulomb counter.  An over-flow indicates that the
                 charge accumulation was positive and out-of-range;
                 (charge >= +36.864kC).  This condition should never
                 happen with the Apf11 because e-current flows from
                 the positive battery terminal to the negative battery
                 terminal is measured as negative.

      Except for the above three sentinel values, each bit of the
      return value represents 1.125C of charge.  That is, the total
      charge consumed by the Apf11 is computed according to the
      formula: Coulombs = [(1.125C)*BatChargeAd16()].  A negative
      value indicates that charge was consumed from the batteries.
   \end{verbatim}
*/
unsigned short BatChargeAdc(void)
{
    /* initialize return value with invalid-sentinel */
    unsigned short ad16 = 0x7fff;

    /* induce the DS2740 into command-ready state */
    if (DowReset() > 0)
    {
        /* create local work objects */
        unsigned char msb = 0, lsb = 0;

        /* enumerate DS2740 commands and addresses */
        enum {SkipNetAddr = 0xCC, ReadData = 0x69, ChargeMsbAddr = 0x10};

        /* bypass addressing since DS2740 is only one-wire device */
        DowWriteByte(SkipNetAddr);

        /* execute the command to read the MSB of measured charge */
        DowWriteByte(ReadData); DowWriteByte(ChargeMsbAddr);

        /* read both MSB and LSB of the 16-bit charge measurement */
        DowReadByte(&msb); DowReadByte(&lsb);

        /* assemble the 16-bit ADC of the charge measurement */
        ad16 = ((msb << 8) | lsb);

        /* avoid conflating out-of-range with invalid-sentinel */
        if (ad16 == 0x7fff)
        {
            ad16 = 0x7ffe;
        }
    }

    return ad16;
}

/*------------------------------------------------------------------------*/
/* reset the DS2740's 16-bit Coulomb counter register to zero             */
/*------------------------------------------------------------------------*/
/**
   This function resets the DS2740's Coulomb counter register to zero.
   If successful, this function returns a positive value.  Zero is
   returned on failure.  A negative return value indicates an
   exception was encountered.
*/
int BatChargeReset(void)
{
   /* initialize return value */
   int status=DowFail;

   /* induce the DS2740 into command-ready state */
   if ((status=DowReset())>0)
   {
      /* enumerate DS2740 commands and addresses */
      enum {SkipNetAddr=0xCC, WriteData=0x6C, ChargeMsbAddr=0x10};

      /* bypass addressing since DS2740 is only one-wire device */
      DowWriteByte(SkipNetAddr);

      /* execute the command to write to MSB of charge register */
      DowWriteByte(WriteData); DowWriteByte(ChargeMsbAddr); 

      /* write zero to MSB and LSB of the 16-bit charge register */
      DowWriteByte(0x00); DowWriteByte(0x00); Wait(50);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* compute the Dallas One-Wire CRC of the 7-byte net address              */
/*------------------------------------------------------------------------*/
/**
   This function computes the Dallas/Maxim One-Wire 8-bit CRC of the
   least significant 56-bits of the Net Address.  The net address must
   be written to the 'netaddr' function argument in the same order as
   read from the DS2740.  Refer to the comment section of
   DowNetAddress() for a description of the format of the 64-bit Net
   Address.  For example, if the eight net address bytes are read in
   the following order {0x36,0x59,0xbf,0x5f,0x03,0x00,0x00,0xa4} then
   the family code is 0x36, the 48-bit address is 0x0000035fbf59, and
   the 8-bit CRC is 0xa4.  In this case, the netaddr buffer must
   contain the 7-bytes: netaddr={0x36,0x59,0xbf,0x5f,0x03,0x00,0x00}.
   Given this function argument then this function will compute and
   return the CRC: 0xa4.

   \begin{verbatim}
      This function returns the Dallas/Maxim One-Wire 8-bit CRC of its
      'netaddr' function argument.
   \end{verbatim}
*/
unsigned char DowCrc(const unsigned char *netaddr,unsigned int len)
{
   unsigned int i,n;

   /* initialize (rotated) kernel with `1 0011 0001' (less the msb) */
   const unsigned char kernel=0x8c;

   /* initialize the crc with all 0's */
   unsigned char crc=0;

   /* process the message, one byte at a time */
   for (n=0; n<len; n++)
   {
      /* copy the next byte for processing */
      unsigned char byte=netaddr[n];

      /* loop through each bit of the current byte */
      for (i=0; i<8; i++)
      {
         /* test criteria for applying XOR operation */
         unsigned char xor = (crc^byte)&0x01;

         /* right shift the CRC and the message by 1 bit */
         crc >>= 1; byte>>=1;
         
         /* apply the XOR operation */
         if (xor) {crc = (crc ^ kernel);}
      }
   }
   
   return crc;
}

/*------------------------------------------------------------------------*/
/* query the DS2740 for its 64-bit Net Address                            */
/*------------------------------------------------------------------------*/
/**
   This function queries the DS2740 for its 64-bit Net Address.  The
   net address consists of three sections of data:  The
   most-significant-byte (MSB) represents an 8-bit CRC of the
   remaining 56 bits, the least-significant-byte (LSB) represents an
   8-bit family code (0x36) for the DS2740, and the remaining six
   bytes of data represent a unique 48-bit address:

      \begin{verbatim}
      +-------+-------------------------------------+--------+
      | 8-bit |          48-bit address             | 8-bit  |
      | CRC   +--------------+----------------------+ family |
      |       | 16-bit group | 32-bit serial number | code   |         
      +-------+--------------+----------------------+--------+
       msb                                                lsb
      \end{verbatim}

   This function further sections the unique 48-bit address into a
   32-bit serial number at the least-significant end and a 16-bit
   group number at the most-significant end.

   \begin{verbatim}
   output:

      serno......The 32-bit serial number which is the
                 least-signficant 32-bits of the unique 48-bit address
                 as shown in the schematic above.

      group......The 16-bit group number which is the most-signficant
                 16-bits of the unique 48-bit address as shown in the
                 schematic above.

      family.....The 8-bit family code for the DS2740.  This will
                 always be 0x36.

      crc........The 8-bit CRC of the least-significant 56-bits of the
                 Net Address.

      Any or all of the function arguments described above may be a
      NULL pointer which will cause the argument to be safely ignored.
      Only non-NULL arguments will be written-to and returned.

      This function returns a positive value if successful; zero
      indicates failure.A negative return value indicates that an
      exception was encounted.
   \end{verbatim}
*/
int DowNetAddress(unsigned long int *serno, unsigned short int *group,
                  unsigned char *family, unsigned char *crc)
{
   /* define the logging signature */
   cc *FuncName="DowNetAddress()";

   /* initialize the return value */
   int n,status=DowOk;

   /* define some local work objects */
   unsigned char Crc,addr[8];

   /* initialize the function arguments */
   if (serno) {(*serno)=0;}
   if (group) {(*group)=0;}
   if (family) {(*family)=0;}
   if (crc) {(*crc)=0;}

   /* execute the reset command to get the DS2740's attention */
   if ((status=DowReset())>0)
   {
      /* execute the DS2740's Read-Net-Address (0x33) command */
      DowWriteByte(0x33);

      /* read 8 bytes from the DS2740 */
      for (n=0; n<8; n++) {DowReadByte(addr+n);}

      /* compute and validate the CRC of the net address */
      if ((Crc=DowCrc(addr,7))==addr[7])
      {
         /* assign the family code of the net address */
         if (family) {(*family)=addr[0];}

         /* assign the CRC of the net address */
         if (crc) {(*crc)=addr[7];}

         /* assign the 32 least-significant-bits of the 48-bit serial number */
         if (serno) {for ((*serno)=0,n=4; n>0; n--) {(*serno)<<=8; (*serno) |= addr[n];}}

         /* assign the 16 most-signficant-bits of the 48-bit serial number */
         if (group) {for ((*group)=0,n=6; n>4; n--) {(*group)<<=8; (*group) |= addr[n];}}
      }
      
      else
      {
         /* log the CRC failure */
         LogEntry(FuncName,"CRC(0x%02x) failure for Net Address: ",Crc);

         /* add the entire 8-bit net address */
         for (status=DowFail, n=0; n<8; n++) {LogAdd("%02x",addr[n]);} LogAdd("\n");
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* read one byte to the DS2740 per the Dallas one-wire protocol           */
/*------------------------------------------------------------------------*/
/**
   This function reads one byte from the DS2740 via the Dallas one-wire
   protocol (DOW).  It is the user's responsibility to have previously
   initiated a reset/presence sequence so that the DS2740 is already
   in a responsive state.

   \begin{verbatim}
      This function returns a positive value if successful.  Zero is
      returned if the reset was not acknowledged by the presence
      pulse.
   \end{verbatim}
*/
int DowReadByte(unsigned char *byte)
{ 
   /* initialize return value */
   int n,status=DowFail; unsigned char bit;

   /* validate the function argument */
   if (byte)
   {
      /* initialize the function argument */
      (*byte)=0;

      /* loop thru each bit of the byte */
      for (status=DowOk,n=0; n<8; n++)
      {
         /* read the bit using the DOW protocol */
         DowReadBit(&bit);

         /* transfer the bit to the function argument */
         (*byte)|=(((bit)?1:0)<<n);
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* write one byte to the DS2740 per the Dallas one-wire protocol          */
/*------------------------------------------------------------------------*/
/**
   This function writes one byte to the DS2740 via the Dallas one-wire
   protocol (DOW).  It is the user's responsibility to have previously
   initiated a reset/presence sequence so that the DS2740 is already
   in a responsive state.

   \begin{verbatim}
      This function returns a positive value if successful.  Zero is
      returned if the reset was not acknowledged by the presence
      pulse.
   \end{verbatim}

*/
int DowWriteByte(unsigned char byte)
{
   /* initialize return value */
   int n,status=DowOk;

   /* loop thru each bit of the byte */
   for (n=0; n<8; n++) 
   {
      /* extact the current bit from the byte */
      unsigned char bit=byte&(0x1<<n);

      /* write the bit using the DOW protocol */
      DowWriteBit(bit);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* read one bit from the DS2740 per the Dallas one-wire protocol          */
/*------------------------------------------------------------------------*/
/**
   This function reads one bit from the DS2740 via the Dallas one-wire
   protocol.  Timing is per the overdrive mode (125kbps) of the
   one-wire protocol as described in Maxim's Application Note 126
   (Document 021604) and shown in the schematics below:

      \begin{verbatim}
               |--- Start --|---------- Logic-1 ----------+-- Pace ---|
      Read-1       (1usec)              (9usec)              (2usec) 
      ---------+            +---------+-------------------+-----------+---
               |            |         C                   D           E
               |            |
               |            |
               +-------+----+
               A       F    B            

      A: Master pulls bus line low.
      F: DS2740 releases line less than 1usec after <A>.
      B: Master releases bus line 1usec after <A>.
      C: Master samples bus line 3usec after <A>.
      D: Master pauses until expiration of timeslot (10usec after <A>).
      E: Master pauses for timeslot pacing (at least 2usec after <D>).


               |--- Start --|---------- Logic-0 ----------+-- Pace ---|
      Read-0       (1usec)              (9usec)              (2usec)
      ---------+                                     +----+-----------+---
               |                                     |    D           E
               |                                     |
               |                                     |
               +------------+---------+--------------+
               A            B         C              F

      A: Master pulls bus line low.
      B: Master releases bus line 1usec after <A>.
      C: Master samples bus line 3usec after <A>.
      F: DS2740 releases line 9usec after <A>.
      D: Master pauses until expiration of timeslot (10usec after <A>).
      E: Master pauses for timeslot pacing (at least 2usec after <D>).
      \end{verbatim}

   The timing of events A,B,C,D are critical and specified by the
   one-wire protocol for the Master.  The timing of event E is not
   critical except that it must be a minimum of 2usec after D; the
   pacing pause between timeslots can be variable and of any duration
   greater than 2usec.

   The critical timing elements of this initialization sequence is
   controlled using the Stm32's TIM5 timer which clocks at 8MHz from
   APB1.  Time is expressed in units of 'winks' which are 1 usec.
   However, the delays are implemented by polling TIM5 with a loop
   that uses a counter to prevent the possibility of infinite loops.
   The processor time/cycles required to maintain the iteration
   counter limits the granularity/precision of the delay.  An
   oscilloscope was used to select the iteration parameters that tune
   the precise timing of events A,B,C,D in the schematic above.

   Note: All interrupts are disabled at event <A> and re-enabled at
   event <E> (total span is 12usec) in order to enforce the strict
   timing requirements of the one-wire protocol.

   \begin{verbatim}
      This function returns a positive value if the one-wire read
      sequence was successful.  Zero is returned if the reset was not
      acknowledged by the presence pulse.
   \end{verbatim}
*/
__attribute__((optimize("O0"))) int DowReadBit(unsigned char *bit)
{
   /* initialize the return value */
   int status=DowOk; unsigned short int n,idr=0,winks=Apb1Hertz/1000000UL;

   /* enumerate the input & output configurations */
   enum {In=0x04U,Out=0x07U};
   
   /* clear mode and configuration bits for pin Apf11:A.8 */
   const unsigned long int CRH=(GPIOA->CRH&(~0xf));
   
   /* configure the Apf11 1-wire interface for input mode */
   GPIOA->CRH=(CRH|In);
   
   /* assert the output bit and configure for open-drain output */
   GPIOA->ODR|=COULOMB_OWD_Pin; GPIOA->CRH=(CRH|Out);

   /* disable IRQs during the time-sensitive phase of the reset */
   __disable_irq();

   /* initiate the read sequence by forcing the output low for 2usec */
   TIM5->CNT=0; GPIOA->ODR &= ~COULOMB_OWD_Pin; 
      
   /* read the bit from the one-wire bus */
   GPIOA->CRH=(CRH|In); idr=GPIOA->IDR;

   /* terminate the timeslot 12usec after its start */
   for (n=0; TIM5->CNT<9*winks && n<9*winks; n++) {}
   
   /* re-enable IRQs */
   __enable_irq();

   /* read the bit from the IDR */
   (*bit) = (idr&COULOMB_OWD_Pin) ? 1 : 0;

   return status;
}

/*------------------------------------------------------------------------*/
/* write one bit to the DS2740 per the Dallas one-wire protocol           */
/*------------------------------------------------------------------------*/
/**
   This function writes one bit to the DS2740 via the Dallas one-wire
   protocol.  Timing is per the overdrive mode (125kbps) of the
   one-wire protocol as described in Maxim's Application Note 126
   (Document 021604) and shown in the schematics below:

      \begin{verbatim}
               |--- Start --|---------- Logic-1 -----+-- Pace ---|
      Write-1      (2usec)              (8usec)         (2usec) 
      ---------+            +------------------------+-----------+---
               |            |                        C           D
               |            |
               |            |
               +------------+
               A            B            

      A: Master pulls bus line low.
      B: Master releases bus line 2usec after <A>.
      C: Master pauses until expiration of Logic-1 pulse (8usec after <B>).
      D: Master pauses for timeslot pacing (at least 2usec after <C>).


               |--------- Logic-0 -------+--- Stop --+-- Pace ---|
      Write-0             (8usec)            (2usec)    (2usec)
      ---------+                         +-----------+-----------+---
               |                         |           C           D
               |                         |
               |                         |
               +-------------------------+
               A                         B            

      A: Master pulls bus line low.
      B: Master releases bus line 8usec after <A>.
      C: Master pauses until expiration of Logic-0 pulse (10usec after <A>).
      D: Master pauses until expiration of pacing between timeslots (2usec after <C>).
      \end{verbatim}

   The timing of events A,B,C are critical and specified by the
   one-wire protocol for the Master.  The timing of event D is not
   critical except that it must be a minimum of 2usec after C; the
   pacing pause between timeslots can be variable and of any duration
   greater than 2usec.

   The critical timing elements of this initialization sequence is
   controlled using the Stm32's TIM5 timer which clocks at 8MHz from
   APB1.  Time is expressed in units of 'winks' which are 1 usec.
   However, the delays are implemented by polling TIM5 with a loop
   that uses a counter to prevent the possibility of infinite loops.
   The processor time/cycles required to maintain the iteration
   counter limits the granularity/precision of the delay.  An
   oscilloscope was used to select the iteration parameters that tune
   the precise timing of events A,B,C,D in the schematic above.

   Note: All interrupts are disabled at event <A> and re-enabled at
   event <D> (total span is 12usec) in order to enforce the strict
   timing requirements of the one-wire protocol.

   \begin{verbatim}
      This function returns a positive value if the one-wire write
      sequence was successful.  Zero is returned if the reset was not
      acknowledged by the presence pulse.
   \end{verbatim}
*/
__attribute__((optimize("O0"))) static int DowWriteBit(unsigned char bit)
{
   /* initialize the return value */
   int status=DowOk; unsigned short int n,winks=Apb1Hertz/1000000UL;

   /* enumerate the input & output configurations */
   enum {In=0x04U,Out=0x07U};
   
   /* clear mode and configuration bits for pin Apf11:A.8 */
   const unsigned long int CRH=(GPIOA->CRH&(~0xf));
 
   /* configure the Apf11 1-wire interface for input mode */
   GPIOA->CRH=(CRH|In);
   
   /* assert the output bit and configure for open-drain output */
   GPIOA->ODR|=COULOMB_OWD_Pin; GPIOA->CRH=(CRH|Out);

   /* disable IRQs during the time-sensitive phase of the reset */
   __disable_irq();

   if (bit)
   {
      /* initiate the write-one sequence by forcing the output low for 2usec */
      TIM5->CNT=0; GPIOA->ODR &= ~COULOMB_OWD_Pin; Delay2us;

      /* assert the bus line to encode a logical value of one */
      GPIOA->ODR |= COULOMB_OWD_Pin;
      
      /* terminate the timeslot 12usec after its start */
      for (n=0; TIM5->CNT<9*winks && n<9*winks; n++) {}
   }
   else
   {
      /* initiate the write-zero sequence by forcing the output low */
      TIM5->CNT=0; GPIOA->ODR &= ~COULOMB_OWD_Pin;
          
      /* pause until output low for 8usec */
      for (n=0; TIM5->CNT<4*winks && n<4*winks; n++) {}

      /* assert the bus line 2usec to create a 8usec write-zero timeslot */
      GPIOA->ODR |= COULOMB_OWD_Pin; 
      
      /* terminate the timeslot 12usec after its start */
      for (n=0; TIM5->CNT<9*winks && n<9*winks; n++) {}
   }
   
   /* re-enable IRQs */
   __enable_irq();
   
   /* configure the Apf11 1-wire interface for input mode */
   GPIOA->CRH=(CRH|In);

   return status;
}

/*------------------------------------------------------------------------*/
/* function to initiate a reset/presence pulse for the one-wire protocol  */
/*------------------------------------------------------------------------*/
/**
   This function initiates a reset/presence pulse for any device on
   the one-wire bus.  Timing is per the overdrive mode (125kbps) of
   the one-wire protocol as described in Maxim's Application Note 126
   (Document 021604) and shown in the schematic below.

      \begin{verbatim}
               |--- Reset ----|---- Presence --|
                   (64usec)        (60usec)
      ---------+              +-+         +----+---
               |              | |         |    F
               |              | |         |
               |              | |         |
               +--------------+ +----+----+
               A              B C    D    E    

      A: Master pulls bus line low.
      B: Master releases bus line 64usec after <A>.
      C: DS2740 pulls bus line low (4usec after <B>).
      D: Master reads state of presence pulse 10usec after <B>.
      E: DS2740 releases bus line (18usec after <B>).
      F: Master pauses until expiration of presence pulse (60usec after <B>).
      \end{verbatim}

   The timing of events B,D,F are critical and specified by the
   one-wire protocol for the Master.  The timing of events C,E are
   controlled by the DS2740 and were measured with an oscilloscope for
   the sake of curiosity and information.

   The critical timing elements of this initialization sequence is
   controlled using the Stm32's TIM5 timer which clocks at 8MHz from
   APB1.  Time is expressed in units of 'winks' which are 1 usec.
   However, the delays are implemented by polling TIM5 with a loop
   that uses a counter to prevent the possibility of infinite loops.
   The processor time/cycles required to maintain the iteration
   counter limits the granularity/precision of the delay.  An
   oscilloscope was used to select the iteration parameters that tune
   the precise timing of events B,D,F in the schematic above.

   Note: All interrupts are disabled at event <A> and re-enabled at
   event <F> (total span is 164usec) in order to enforce the strict
   timing requirements of the one-wire protocol.

   \begin{verbatim}
      This function returns a positive value if the one-wire
      initialization sequence was successful (ie., the DS2740
      acknowledged the reset by announcing its presence).  Zero is
      returned if the reset was not acknowledged by the presence
      pulse. 
   \end{verbatim}
*/
__attribute__((optimize("O0"))) int DowReset(void)
{
   /* initialize the return value */
   int n,winks,status=DowOk;

   /* enumerate the input & output configurations */
   enum {In=0x04U,Out=0x07U};
   
   /* clear mode and configuration bits for pin Apf11:A.8 */
   const unsigned long int CRH=(GPIOA->CRH&(~0xf));

   /* check the one-time switch for timer configuration */
   if (!Apb1Hertz) {Apb1Hertz=Stm32Apb1Freq(); uTimerConfig();}

   /* compute the number of APB1 cycles in a wink of time */
   winks = Apb1Hertz/1000000UL;
   
   /* configure the Apf11 1-wire interface for input mode */
   GPIOA->CRH=(CRH|In);
   
   /* assert the output bit and configure for open-drain output */
   GPIOA->ODR|=COULOMB_OWD_Pin; GPIOA->CRH=(CRH|Out);
   
   /* disable IRQs during the time-sensitive phase of the reset */
   __disable_irq();
   
   /* Event A: pause 2usec, then force output low to initiate the reset pulse */
   Delay2us; GPIOA->ODR &= ~COULOMB_OWD_Pin;

   /* hold the reset pulse low for 70usec */
   for (TIM5->CNT=0, n=0; TIM5->CNT<61*winks && n<61*winks; n++) {}

   /* Event B: terminate the reset pulse by driving the pin high */
   GPIOA->ODR|=COULOMB_OWD_Pin;

   /* pause for 10usec before reading the presence pulse */
   for (TIM5->CNT=0, n=0; TIM5->CNT<4*winks && n<4*winks; n++) {}

   /* Event D: configure Apf11 for input mode and detect the presence pulse */
   GPIOA->CRH=(CRH|In); if (GPIOA->IDR&COULOMB_OWD_Pin) {status=DowFail;}

   /* Event F: pause for 50usec to permit presence pulse to expire */
   for (TIM5->CNT=0, n=0; TIM5->CNT<45*winks && n<45*winks; n++) {}
  
   /* re-enable IRQs */
   __enable_irq();
   
   return status;
}

/*------------------------------------------------------------------------*/
/* accumulate summed parameters to the charge model                       */
/*------------------------------------------------------------------------*/
/**
   This function accumulates parameters used to compute the amount of
   charge consumed by an Apex.
*/
int ChargeModelAccumulate(unsigned long int StandbySec, unsigned char WakeCycles, float Coulombs)
{
   /* initialize the return value */
   int status=DowOk;

   /* accumulate parameters of the charge budget model */
   cmodel.StandbySec+=StandbySec; cmodel.WakeCycles+=WakeCycles;
   if (Finite(Coulombs)) {cmodel.Coulombs+=Coulombs;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* evaluate the charge model                                              */
/*------------------------------------------------------------------------*/
int ChargeModelCompute(float *charge)
{
   /* initialize the return value */
   int status=DowOk;

   /* define currents consumed during standby & quiescent wake periods */
   const float MetabolicCurrent=110e-6, QuiescentCurrent=12e-3, WakeSec=3.5;
   
   /* compute the total charge comsumed (kC) */
   if (charge) {(*charge) = (cmodel.Coulombs + cmodel.StandbySec*MetabolicCurrent +
                             cmodel.WakeCycles*WakeSec*QuiescentCurrent)/1000;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* initialize the charge budget model to start integrals at zero          */
/*------------------------------------------------------------------------*/
int ChargeModelInit(void)
{
   /* initialize the return value */
   int status=DowOk;

   /* initialize the charge budget model */
   memset((void *)(&cmodel),0,sizeof(cmodel));

   return status;
}

#endif /* DS2740_C */
