#ifndef MAX3109_H
#define MAX3109_H (0x0020U)

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Description of the SwiftWare API for the MAX3109 DUART
   ------------------------------------------------------

   Due to the fact that all six of the Apf11 DUARTS (ie., Spare, RF,
   Expansion{1,2,3,4}) are on the Apf11's SPI1 bus, the SwiftWare API
   uses this single module to access and control all six of them.
   Each function implemented by this API is parameterized by the
   SPI device identifier and the Max3109's UART identifier (ie.,
   {0,1}), if needed.   

   The Max3109 is accessed and controlled via its thirty-five
   registers which are tersely described in the table below (ie.,
   taken from the Max3109 data sheet).

   Register Map    (Note: Unless otherwise noted, all default reset
                    values are 0x00 and all registers are R/W.)
   +------------------------------------------------------------------------------------------------------+
   | REGISTER       ADDR  Bit7      Bit6      Bit5      Bit4      Bit3      Bit2      Bit1      Bit0      |
   |------------------------------------------------------------------------------------------------------|
   | RHR(1)         0x00  RData7    RData6    RData5    RData4    RData3    RData2    RData1    RData0    |
   | THR(1)         0x00  TData7    TData6    TData5    TData4    TData3    TData2    TData1    TData0    |
   |---- Interrupts --------------------------------------------------------------------------------------|
   | IRQEn          0x01  CTSIEn    RxEmtyIEn TFifoEmEn TxTrgIEn  RxTrgIEn  STSIEn    SpChrIEn  LSRErrIEn |
   | ISR(1,2)       0x02  CTSInt    RxEmpty   TFifoEmpt TxTrgInt  RxTrigInt STSInt    SpCharInt LSRErrInt |
   | LSRIntEn       0x03  —         —         NoiseEn   RBreakIEn FramErIEn ParityIEn ROverrIEn RTimoutIEn|
   | LSR(1,2)       0x04  /CTSbit   —         RxNoise   RxBreak   FrameErr  RxParitEr RxOverrun RTimeout  |
   | SpclChrIntEn   0x05  —         —         MltDrpEn  BREAKInEn XOFF2InEn XOFF1InEn XON2IntEn XON1IntEn |
   | SpclCharInt    0x06  —         —         MultDropI BREAKInt  XOFF2Int  XOFF1Int  XON2Int   XON1Int   |
   | STSIntEn(3)    0x07  TxEmptyEn SleepEn   ClkRdyEn  —         GPI3IntEn GPI2IntEn GPI1IntEn GPI0IntEn |
   | STSInt(1,2,3)  0x08  TxEmpty   Sleep     ClkReady  —         GPI3Int   GPI2Int   GPI1Int   GPI0Int   |
   |---- UART modes --------------------------------------------------------------------------------------|
   | MODE1          0x09  —         AutoSleep ForcSleep TrnscvCtl RTSHiZ    TxHiZ     TxDisabl  RxDisabl  |
   | MODE2          0x0a  EchoSuprs MultiDrop Loopback  SpeclChr  RFifoEmIn RxTrgInv  FIFORst   RST       |
   | LCR(2)         0x0b  /RTSbit   TxBreak   ForcParit EvenParit ParityEn  StopBits  Length1   Length0   |
   | RxTimeOut      0x0c  TimOut7   TimOut6   TimOut5   TimOut4   TimOut3   TimOut2   TimOut1   TimOut0   |
   | HDplxDelay     0x0d  Setup3    Setup2    Setup1    Setup0    Hold3     Hold2     Hold1     Hold0     |
   | IrDA           0x0e  —         —         TxInv     RxInv     MIR       —         SIR       IrDAEn    |
   |---- FIFOs control -----------------------------------------------------------------------------------|
   | FlowLvl        0x0f  Resume3   Resume2   Resume1   Resume0   Halt3     Halt2     Halt1     Halt0     |
   | FIFOTrLvl(2)   0x10  RxTrig3   RxTrig2   RxTrig1   RxTrig0   TxTrig3   TxTrig2   TxTrig1   TxTrig0   |
   | TxFIFOLvl(1)   0x11  TxFL7     TxFL6     TxFL5     TxFL4     TxFL3     TxFL2     TxFL1     TxFL0     |
   | RxFIFOLvl(1)   0x12  RxFL7     RxFL6     RxFL5     RxFL4     RxFL3     RxFL2     RxFL1     RxFL0     |
   |---- Flow control ------------------------------------------------------------------------------------|
   | FlowCtrl       0x13  SwFlow3   SwFlow2   SwFlow1   SwFlow0   SwFlowEn  GPIAddr   AutoCTS   AutoRTS   |
   | XON1           0x14  Bit7      Bit6      Bit5      Bit4      Bit3      Bit2      Bit1      Bit0      |
   | XON2           0x15  Bit7      Bit6      Bit5      Bit4      Bit3      Bit2      Bit1      Bit0      |
   | XOFF1          0x16  Bit7      Bit6      Bit5      Bit4      Bit3      Bit2      Bit1      Bit0      |
   | XOFF2          0x17  Bit7      Bit6      Bit5      Bit4      Bit3      Bit2      Bit1      Bit0      |
   |---- GPIOs -------------------------------------------------------------------------------------------|
   | GPIOConfig(3)  0x18  GP3OD     GP2OD     GP1OD     GP0OD     GP3Out    GP2Out    GP1Out    GP0Out    |
   | GPIOData(3)    0x19  GPI3Dat   GPI2Dat   GPI1Dat   GPI0Dat   GPO3Dat   GPO2Dat   GPO1Dat   GPO0Dat   |
   |---- Clock configuration -----------------------------------------------------------------------------|
   | PLLConfig(2,4) 0x1a  PLLFactr1 PLLFactr0 PreDiv5   PreDiv4   PreDiv3   PreDiv2   PreDiv1   PreDiv0   |
   | BRGConfig      0x1b  —         —         4xMode    2xMode    FRACT3    FRACT2    FRACT1    FRACT0    |
   | DIVLSB(2)      0x1c  Div7      Div6      Div5      Div4      Div3      Div2      Div1      Div0      |
   | DIVMSB         0x1d  Div15     Div14     Div13     Div12     Div11     Div10     Div9      Div8      |
   | CLKSource(2,4) 0x1e  CLKtoRTS  —         —         —         PLLBypass PLLEn     CystalEn  —         |
   |---- Global register ---------------------------------------------------------------------------------|
   | GlobalIRQ(1,2) 0x1f  0         0         0         0         0         0         0         0         |
   | GlobalCmnd(1)  0x1f  GlbCom7   GlbCom6   GlbCom5   GlbCom4   GlbCom3   GlbCom2   GlbCom1   GlbCom0   |
   |---- Synchronization ---------------------------------------------------------------------------------|
   | TxSynch(5)     0x20  CLKtoGPIO TxAutoDis TrigDelay SynchEn   TrigSel3  TrigSel2  TrigSel1  TrigSel0  |
   | SynchDelay1(5) 0x21  SDelay7   SDelay6   SDelay5   SDelay4   SDelay3   SDelay2   SDelay1   SDelay0   |
   | SynchDelay2(5) 0x22  SDelay15  SDelay14  SDelay13  SDelay12  SDelay11  SDelay10  SDelay9   SDelay8   |
   |---- Timer registers ---------------------------------------------------------------------------------|
   | TIMER1(5)      0x23  Timer7    Timer6    Timer5    Timer4    Timer3    Timer2    Timer1    Timer0    |
   | TIMER2(5)      0x24  TmrToGPIO Timer14   Timer13   Timer12   Timer11   Timer10   Timer9    Timer8    |
   |---- Revision ----------------------------------------------------------------------------------------|
   | RevID(1,2,5)   0x25  1         1         0         0         0         0         1         0         |
   +------------------------------------------------------------------------------------------------------+

   Footnotes:

      1 Denotes nonread/write mode: RHR=R, THR=W, ISR=COR, LSR=R,
        SpclCharInt=COR, STSInt=R/COR, TxFIFOLvl=R, RxFIFOLvl=R,
        GlobalIRQ=R, GloblComnd=W, RevID=R.
   
      2 Denotes nonzero default reset value: ISR=0x60, LCR=0x05,
        FIFOTrgLvl=0xFF, PLLConfig=0x01, DIVLSB=0x01, CLKSource=0x18,
        GlobalIRQ=0x03, RevID=0xC1.
   
      3 Each UART has four individually assigned GPIO outputs as
        follows: UART0: GPIO0–GPIO3, UART1: GPIO4–GPIO7.
   
      4 Denotes a register that can only be programmed by accessing
        UART0.
   
      5 Use extended addressing when operating in SPI mode.

   The data sheet for the Max3109 is: MAX3109 Dual Serial UART with
   128-Word FIFOs. Document 19-5806; Rev 5; 8/16.
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define max3109ChangeLog "$RCSfile$  $Revision$  $Date$"

#include <Stm32f103Spi.h>
#include <fifo.h>
#include <stdio.h>

/*========================================================================*/
/* enumeration of registers in the MAX3109 DUART with SPI interface       */
/*========================================================================*/
typedef enum
{
   /* -- FIFO data -------------------------------------------------------*/
   RHR          = 0x00U, /* Receive hold register: Bottom of Rx FIFO.     */
   THR          = 0x00U, /* Transmit hold register: Top of Tx FIFO.       */
   /* -- Interrupts ----------------------------------------------------- */
   IRQEn        = 0x01U, /* IRQ enable register.                          */
   ISR          = 0x02U, /* Interrupt status register.                    */
   LSRIntEn     = 0x03U, /* Line status interrupt enable register.        */
   LSR          = 0x04U, /* Line status register.                         */
   SpclChrIntEn = 0x05U, /* Special character interrupt enable resiter.   */
   SpclCharInt  = 0x06U, /* Special character interrupt register.         */
   STSIntEn     = 0x07U, /* STS interrupt enable register.                */
   STSInt       = 0x08U, /* Status interrupt register.                    */
   /* -- UART modes ----------------------------------------------------- */
   MODE1        = 0x09U, /* MODE1 register.                               */
   MODE2        = 0x0aU, /* MODE2 register.                               */
   LCR          = 0x0bU, /* Line control register.                        */
   RxTimeOut    = 0x0cU, /* Receiver timeout register.                    */
   HDplxDelay   = 0x0dU, /* Hardware handshaking delay control register.  */
   IrDA         = 0x0eU, /* Inverse logic control register.               */
   /* -- FIFOs control -------------------------------------------------- */
   FlowLvl      = 0x0fU, /* Flow level register.                          */
   FIFOTrLvl    = 0x10U, /* FIFO interrupt trigger level register.        */
   TxFIFOLvl    = 0x11U, /* Transmit FIFO level register.                 */
   RxFIFOLvl    = 0x12U, /* Receive FIFO level register.                  */
   /* -- Flow control --------------------------------------------------- */
   FlowCtrl     = 0x13U, /* Flow control register.                        */
   XON1         = 0x14U, /* XON1 register.                                */
   XON2         = 0x15U, /* XON2 register.                                */
   XOFF1        = 0x16U, /* XOFF1 register.                               */
   XOFF2        = 0x17U, /* XOFF2 register.                               */
   /* -- GPIOs ---------------------------------------------------------- */
   GPIOConfig   = 0x18U, /* GPIO configuration register.                  */
   GPIOData     = 0x19U, /* GPIO data register.                           */
   /* -- Clock configuration -------------------------------------------- */
   PLLConfig    = 0x1aU, /* PLL configuration register.                   */
   BRGConfig    = 0x1bU, /* Baud rate generator configuration register.   */
   DIVLSB       = 0x1cU, /* Baud rate generator LSB divisor register.     */
   DIVMSB       = 0x1dU, /* Baud rate generator MSB divisor register.     */
   CLKSource    = 0x1eU, /* Clock source register                         */
   /* -- Global registers ----------------------------------------------- */
   GlobalIRQ    = 0x1fU, /* Global IRQ register.                          */
   GlobalCmnd   = 0x1fU, /* Global command register.                      */
   /* -- Synchronization ------------------------------------------------ */
   TxSynch      = 0x20U, /* Transmitter synchronization register.         */
   SynchDelay1  = 0x21U, /* Synchronization delay register 1.             */
   SynchDelay2  = 0x22U, /* Synchronization delay register 2.             */
   /* -- Timer registers ------------------------------------------------ */
   TIMER1       = 0x23U, /* Timer register 1.                             */
   TIMER2       = 0x24U, /* Timer register 2.                             */
   /* -- Revision ------------------------------------------------------- */
   RevID        = 0x25U, /* Revision identification register.             */
} Max3109RegMap;

/*========================================================================*/
/* enumerate the Max3109 UART identifiers                                 */
/*========================================================================*/
typedef enum {UART0, UART1} Max3109UartId;

/*========================================================================*/
/* enumerate the implemented Max3109 communications modes                 */
/*========================================================================*/
typedef enum {N81, E71, O71} Max3109Mode;

/* counts of various error conditions */
typedef struct {unsigned char nbreak,parity,frame,noise,ovrun;} UartStatus;

/* declare functions with external linkage */
int Max3109BaudRateGet(SpiDevice dev, Max3109UartId uart, unsigned long int *baud);
int Max3109BaudRateSet(SpiDevice dev, Max3109UartId uart, unsigned long int baud);
int Max3109BurstRx(SpiDevice dev, Max3109UartId uart, struct Fifo *fifo, UartStatus *status);
int Max3109Config(SpiDevice dev, Max3109UartId uart, unsigned long int baud, Max3109Mode mode);
int Max3109CtsHandshakeClear(SpiDevice dev, Max3109UartId uart);
int Max3109CtsHandshakeSet(SpiDevice dev, Max3109UartId uart);
int Max3109CtsIsAssert(SpiDevice dev, Max3109UartId uart);
int Max3109CtsIsClear(SpiDevice dev, Max3109UartId uart);
int Max3109Disable(SpiDevice dev);
int Max3109Enable(SpiDevice dev);
int Max3109Flush(SpiDevice dev, Max3109UartId uart);
int Max3109IBytes(SpiDevice dev, Max3109UartId uart);
int Max3109Init(SpiDevice dev, Max3109UartId uart);
int Max3109Irq(SpiDevice dev, unsigned char *value);
int Max3109ModeSet(SpiDevice dev, Max3109UartId uart, Max3109Mode mode);
int Max3109OBytes(SpiDevice dev, Max3109UartId uart);
int Max3109Putb(SpiDevice dev, Max3109UartId uart, unsigned char byte);
int Max3109RegModify(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg,
                     unsigned char assert, unsigned char clear);
int Max3109RegRead(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg, unsigned char *value);
int Max3109Regs(SpiDevice dev, FILE *fp);
int Max3109RegWrite(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg, unsigned char value);
int Max3109Reset(SpiDevice dev, Max3109UartId uart);
int Max3109RevId(SpiDevice dev);
int Max3109RtsAssert(SpiDevice dev, Max3109UartId uart);
int Max3109RtsClear(SpiDevice dev, Max3109UartId uart);
int Max3109RtsCtsHandshakeOff(SpiDevice dev, Max3109UartId uart);
int Max3109RtsCtsHandshakeOn(SpiDevice dev, Max3109UartId uart);
int Max3109RtsHandshakeClear(SpiDevice dev, Max3109UartId uart);
int Max3109RtsHandshakeSet(SpiDevice dev, Max3109UartId uart);
int Max3109RtsIsAssert(SpiDevice dev, Max3109UartId uart);
int Max3109TxBreak(SpiDevice dev, Max3109UartId uart, unsigned short int millisec);
int Max3109TxBreakAssert(SpiDevice dev, Max3109UartId uart);
int Max3109TxBreakClear(SpiDevice dev, Max3109UartId uart);

/* define the return states of the Max3109 DUART API */
extern const char Max3109TimeOut;        /* timeout error */
extern const char Max3109Invalid;        /* invalid selection */
extern const char Max3109NotImplemented; /* not implemented */
extern const char Max3109Cfg;            /* MAX3901 configuration fault */
extern const char Max3109SpiCfg;         /* SPI configuration fault */
extern const char Max3109Null;           /* NULL function argument */
extern const char Max3109Fail;           /* general failure */
extern const char Max3109Ok;             /* general success */
   
#endif /* MAX3109_H */
#ifdef MAX3109_C

#include <logger.h>
#include <max7301.h>
#include <Stm32f103Gpio.h>

/* define the return states of the Max3109 DUART API */
const char Max3109TimeOut           = -6; /* timeout error */
const char Max3109Invalid           = -5; /* invalid selection */
const char Max3109NotImplemented    = -4; /* not implemented */
const char Max3109Cfg               = -3; /* MAX3901 configuration fault */
const char Max3109SpiCfg            = -2; /* SPI configuration fault */
const char Max3109Null              = -1; /* NULL function argument */
const char Max3109Fail              =  0; /* general failure */
const char Max3109Ok                =  1; /* general success */

/* define configuration parameters */
#define BaudRate (0x0000U) 
#define WordMode (0x0800U)
#define Cr2      (0x0000U)
#define NoCrc    (0x0000U)

/* define MAX3109 command masks */
#define WRITE    (0x8000U)
#define READ     (0x0000U)

/* define some masks */
#define CtsMask (0x80U)
#define RtsMask (0x80U)

/* define the frequency of the external oscillator for the Max3109 */
#define fREF (3.6864e6) /* Apf11 Schematic 304770 Rev A, sheet 15, region 3C */

/* declare external functions used locally */
unsigned long int HAL_GetTick(void);
int Wait(unsigned long int millisec);

/*========================================================================*/
/* Initializer list for the registers of the Max3109 DUART                */
/*========================================================================*/
/**
   The Max3109RegInit array of unsigned char is used to configure the
   Max3901 DUART registers.   The Max3109 contains 35 registers some
   of which are read-only, some are read/write, and some are
   clear-on-read.  Those registers which can not be configured include
   sentinel values of 0xff in the list below.
*/
   #define InitRHR          (0xffU) /* Read          */
   #define InitIRQEn        (0x09U) /* Read/Write    */
   #define InitISR          (0xffU) /* Clear on read */
   #define InitLSRIntEn     (0x3fU) /* Read/Write    */
   #define InitLSR          (0xffU) /* Read          */
   #define InitSpclChrIntEn (0x00U) /* Read/Write    */
   #define InitSpclCharInt  (0xffU) /* Clear on read */
   #define InitSTSIntEn     (0x00U) /* Read/Write    */
   #define InitSTSInt       (0xffU) /* Clear on read */
   #define InitMODE1        (0x00U) /* Read/Write    */
   #define InitMODE2        (0x00U) /* Read/Write    */
   #define InitLCR          (0x03U) /* Read/Write    */
   #define InitRxTimeOut    (0x14U) /* Read/Write    */
   #define InitHDplxDelay   (0x00U) /* Read/Write    */
   #define InitIrDA         (0x00U) /* Read/Write    */
   #define InitFlowLvl      (0x1aU) /* Read/Write    */
   #define InitFIFOTrLvl    (0x8cU) /* Read/Write    */
   #define InitTxFIFOLvl    (0xffU) /* Read          */
   #define InitRxFIFOLvl    (0xffU) /* Read          */
   #define InitFlowCtrl     (0x00U) /* Read/Write    */
   #define InitXON1         (0x00U) /* Read/Write    */
   #define InitXON2         (0x00U) /* Read/Write    */
   #define InitXOFF1        (0x00U) /* Read/Write    */
   #define InitXOFF2        (0x00U) /* Read/Write    */
   #define InitGPIOConfig   (0x00U) /* Read/Write    */
   #define InitGPIOData     (0x00U) /* Read/Write    */
   #define InitPLLConfig    (0x01U) /* Read/Write    */
   #define InitBRGConfig    (0x00U) /* Read/Write    */
   #define InitDIVLSB       (0x18U) /* Read/Write    */
   #define InitDIVMSB       (0x00U) /* Read/Write    */
   #define InitCLKSource    (0x1aU) /* Read/Write    */
/*
   The thirty-one configurations above are integrated into the array
   of unsigned char's below.  The remaining four configurations will
   not be used.
*/
const unsigned char Max3109RegInit[] =
{
   InitRHR,         InitIRQEn,        InitISR,          InitLSRIntEn,
   InitLSR,         InitSpclChrIntEn, InitSpclCharInt,  InitSTSIntEn,
   InitSTSInt,      InitMODE1,        InitMODE2,        InitLCR,
   InitRxTimeOut,   InitHDplxDelay,   InitIrDA,         InitFlowLvl,
   InitFIFOTrLvl,   InitTxFIFOLvl,    InitRxFIFOLvl,    InitFlowCtrl,
   InitXON1,        InitXON2,         InitXOFF1,        InitXOFF2,
   InitGPIOConfig,  InitGPIOData,     InitPLLConfig,    InitBRGConfig,
   InitDIVLSB,      InitDIVMSB,       InitCLKSource      
};

/*------------------------------------------------------------------------*/
/* function to read Max3109 interrupt source                              */
/*------------------------------------------------------------------------*/
/**
  This function queries the Max3109 to determine if an interrupt
  source was from UART0 or UART1 of the Max3109.  The Max3109's Fast
  Read Cycle is exploited to retrieve the interrupt source.

  \begin{verbatim}
  output:
     This function returns a positive value on success or zero on
     failure.  A negative return value indicates that an exception
     was encountered.
  \end{verbatim}
*/
int Max3109Irq(SpiDevice dev, unsigned char *irq)
{
    /* define the logging signature */
   cc *FuncName="Max3109Irq()";

   /* initialize return value */
   int err,status=Max3109Null;

   /* initialize the return value of function parameter */
   if (irq) {(*irq)=0xffU;}

   /* validate the argument */
   if (!irq) {LogEntry(FuncName,"NULL function argument.\n");}
   
   /* enable the specified DUART */
   else if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int config;

      /* clear the Stm32's status flags */ 
      config=SPI1->DR; err=SPI1->SR;
               
      /* transmit the command to read the GlobalIRQ register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (READ | (GlobalIRQ<<8)); Stm32SpiRxne(SPI1,timeout);

      /* read the IRQs from the data register */
      config=SPI1->DR; err=SPI1->SR;

      /* convert the two least significant bits from active-low to active-high */
      (*irq)=((~config)&0x3); status=Max3109Ok;
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to query the Max3109 for a UART's baud rate                   */
/*------------------------------------------------------------------------*/
/**
   This function queries the Max3109 for a UART's baud rate.  

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:

      baud.....This specifies the baud rate (ie., bits per second) to
               used for serial communications.  The valid range for
               the baud rate is [16,230400].

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109BaudRateGet(SpiDevice dev, Max3109UartId uart, unsigned long int *baud)
{
   /* define the logging signature */
   cc *FuncName="Max3109BaudRateGet()";

   /* initialize return value */
   int err[3],status=Max3109Ok;

   /* SwiftWare model uses only rate:1x, rates 2x & 4x not used */
   const unsigned char RateMode=1;

   /* initialize the value of the function argument */
   if (baud) {(*baud)=Max3109Null;}

   /* validate the function argument  */
   if (!baud) {LogEntry(FuncName,"NULL function argument.\n"); status=Max3109Null;}

   else
   {
      /* define some local work objects */
      unsigned char msb, lsb, frac; unsigned long int BR;

      /* read the baud rate configuration */
      if ((err[2]=Max3109RegRead(dev,uart,DIVMSB,&msb))<=0 ||
          (err[1]=Max3109RegRead(dev,uart,DIVLSB,&lsb))<=0 ||
          (err[0]=Max3109RegRead(dev,uart,BRGConfig,&frac))<=0)
      {
         /* log the error */
         LogEntry(FuncName,"Attempt to read baud rate(%lu) configuration"
                  "(msb:0x%02x,lsb:0x%02x,frac:0x%x) for DUART(dev:%d,uart:%d) "
                  "failed. [err={%d,%d,%d}]\n",baud,msb,lsb,frac,dev,uart,
                  err[2],err[1],err[0]); status=Max3109Cfg;
      }

      /* compute the baud rate divisor */
      else {BR=((msb<<12) | (lsb<<4) | (frac&0xf)); (*baud) = RateMode*fREF/BR;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* configure the Max3109's baud rate                                      */
/*------------------------------------------------------------------------*/
/**
   This function configures the baud rate for the specified Max3109
   device and UART.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

      baud.....This specifies the baud rate (ie., bits per second) to
               used for serial communications.  The valid range for
               the baud rate is [16,230400].

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109BaudRateSet(SpiDevice dev, Max3109UartId uart, unsigned long int baud)
{
   /* define the logging signature */
   cc *FuncName="Max3109BaudRateSet()";

   /* initialize return value */
   int err[3],status=Max3109Ok;

   /* SwiftWare model uses only rate:1x, rates 2x & 4x not used */
   const unsigned char RateMode=1;

   /* validate the specified baud rate */
   if (baud<16UL || baud>230400UL) 
   {
      /* log the failure */
      LogEntry(FuncName,"Invalid baud rate :%d\n",baud); status=Max3109Invalid;
   }
   
   /* enable the specified DUART */
   else if ((err[0]=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err[0]); status=Max3109Fail;
   }

   else
   {
      /* compute the baud rate divisor with 20-bits of resolution */
      const unsigned long int BR=((unsigned long int)(RateMode*fREF/baud))&0xfffffUL;

      /* decimate the 20-bits of baud-rate divisor into bytes */
      const unsigned char msb=((BR>>12)&0xffU), lsb=((BR>>4)&0xffU), frac=(BR&0xfU);

      /* configure the baud-rate generation registers */
      if ((err[2]=Max3109RegWrite(dev,uart,DIVMSB,msb))<=0 ||
          (err[1]=Max3109RegWrite(dev,uart,DIVLSB,lsb))<=0 ||
          (err[0]=Max3109RegWrite(dev,uart,BRGConfig,frac))<=0)
      {
         /* log the failure */
         LogEntry(FuncName,"Baud rate(%lu) configuration(msb:0x%02x,lsb:0x%02x,frac:0x%x) "
                  "for DUART(dev:%d,uart:%d) failed. [err={%d,%d,%d}]\n",baud,msb,lsb,frac,
                  dev,uart,err[2],err[1],err[0]); status=Max3109Cfg;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the contents of the Max3109's Rx FIFO register        */
/*------------------------------------------------------------------------*/
/**
   This function reads the contents of the Max3109's Rx FIFO
   register.  It is intended to be called from within the interrupt
   service routine (ISR). 

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:

      fifo.....This is the Fifo object used to store the contents of
               the Max3109's Rx FIFO.

      errors...This is the UartStatus object to contain the line
               status errors associated with each byte in the
               Max3109's Rx FIFO.

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109BurstRx(SpiDevice dev, Max3109UartId uart, struct Fifo *fifo, UartStatus *errors)
{
   /* define the logging signature */
   cc *FuncName="Max3109BurstRx()";

   /* initialize return value */
   int err,status=Max3109Null; 

   /* define local work objects */
   unsigned char n,N;

   /* validate the argument */
   if (!fifo) {LogEntry(FuncName,"NULL function argument.\n");}
   
   /* read the number of bytes in the Rx FIFO of the specified UART */
   else if ((N=Max3109IBytes(dev,uart))<0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to read number of bytes in UART%d of "
               "SPI1 device(%d) failed.\n",uart,dev); status=Max3109Fail;
   }

   /* check if the Max3109's Rx FIFO is nonempty */
   else if (N>0) 
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      unsigned short int byte;

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;
      
      /* clear the Stm32's status flags */ 
      byte=SPI1->DR; err=SPI1->SR; ((void)err);

      /* loop to read the contents of the Max3109's Rx FIFO */
      for (status=Max3109Ok,n=0; n<N; n++)
      {
         /* transmit the command to read a specified register of the max3109 */
         Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (RHR<<8)); Stm32SpiRxne(SPI1,timeout);

         /* read the byte from the data register and push it to the FIFO */
         byte=(SPI1->DR)&0xff; err=SPI1->SR; push(fifo,byte);

         /* check if the line status register should be read for errors */
         if (errors)
         {
            /* transmit the command to read a specified register of the max3109 */
            Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (LSR<<8)); Stm32SpiRxne(SPI1,timeout);
  
            /* read the byte from the data register */
            byte=(SPI1->DR)&0xff; err=SPI1->SR;
         
            /* accumulate any errors */
            if (byte&0x02U && (errors->ovrun)<0xffU)  {errors->ovrun++;}
            if (byte&0x04U && (errors->parity)<0xffU) {errors->parity++;}
            if (byte&0x08U && (errors->frame)<0xffU)  {errors->frame++;}
            if (byte&0x10U && (errors->nbreak)<0xffU) {errors->nbreak++;}
            if (byte&0x20U && (errors->noise)<0xffU)  {errors->noise++;}
         }
      }
   }

   /* check logging criteria for reporting an empty Rx FIFO */
   else if (debuglevel>=3 || (debugbits&MAX3109_H))
   {
      LogEntry(FuncName,"Empty Rx FIFO for UART%d of SPI1 "
               "device(%d).\n",uart,dev); status=Max3109Fail;
   }
      
   return status;
}

/*------------------------------------------------------------------------*/
/* configure the baud rate and communications parameters of a Max3901     */
/*------------------------------------------------------------------------*/
/**
   This function configures the communications parameters for a
   Max3109 DUART including baud rate, number of data bits, number of
   stop bits, and parity generation.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

      baud.....This specifies the baud rate (ie., bits per second) to
               used for serial communications.  The valid range for
               the baud rate is [16,230400].

      mode.....This specifies the communications parameters; number of
               data bits, stop bits, and parity generation.  The mode
               must be selected from the Max3109Mode enumeration near
               the top of this file.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Config(SpiDevice dev, Max3109UartId uart, unsigned long int baud, Max3109Mode mode)
{
   /* define the logging signature */
   cc *FuncName="Max3109Config()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* initialize the Max3109 */
   if ((err=Max3109Init(dev,uart))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Initialization of DUART(dev:%d,uart:%d) failed. "
               "[err=%d]\n",dev,uart,err); status=Max3109Fail;
   }

   /* adjust the baud rate */
   else if ((err=Max3109BaudRateSet(dev,uart,baud))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Baud rate(%lu) configuration of DUART(dev:%d,uart:%d) failed. "
               "[err=%d]\n",baud,dev,uart,err); status=Max3109Cfg;
   }

   /* set the communications parameters */
   else if ((err=Max3109ModeSet(dev,uart,mode))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Mode(%d) configuration of DUART(dev:%d,uart:%d) failed. "
               "[err=%d]\n",mode,dev,uart,err); status=Max3109Cfg;
   }
   
   return status;
}
 
/*------------------------------------------------------------------------*/
/* function to disable hardware CTS handshaking                           */
/*------------------------------------------------------------------------*/
/**
   This function disables hardware CTS handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109CtsHandshakeClear(SpiDevice dev, Max3109UartId uart)
{
   /* clear the 0x02 bit of the FlowCtrl register to disable auto-CTS generation */
   return Max3109RegModify(dev,uart,FlowCtrl,0x00U,0x02U);
}

/*------------------------------------------------------------------------*/
/* function to enable hardware RTS handshaking                            */
/*------------------------------------------------------------------------*/
/**
   This function enables hardware RTS handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109CtsHandshakeSet(SpiDevice dev, Max3109UartId uart)
{
   /* assert the 0x02 bit of the FlowCtrl register to enable auto-CTS generation */
   return Max3109RegModify(dev,uart,FlowCtrl,0x02U,0x00U);
}

/*------------------------------------------------------------------------*/
/* function to read the state of the CTS pin from a Max3109 UART          */
/*------------------------------------------------------------------------*/
/**
   This function reads the state of the CTS pin from a specified UART
   of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value if the CTS pin is asserted
      or zero if clear.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Max3109CtsIsAssert(SpiDevice dev, Max3109UartId uart)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a local work object */
   unsigned char lsr;

   /* read the line status register */
   if ((err=Max3109RegRead(dev,uart,LSR,&lsr))<=0) {status=err;}

   /* determine if the CTS bit is clear or asserted */
   else {status = (lsr&CtsMask) ? 1 : 0;}
      
   return status;
}
 
/*------------------------------------------------------------------------*/
/* function to read the state of the CTS pin from a Max3109 UART          */
/*------------------------------------------------------------------------*/
/**
   This function reads the state of the CTS pin from a specified UART
   of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value if the CTS pin is clear
      or zero if asserted.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Max3109CtsIsClear(SpiDevice dev, Max3109UartId uart)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a local work object */
   unsigned char lsr;

   /* read the line status register */
   if ((err=Max3109RegRead(dev,uart,LSR,&lsr))<=0) {status=err;}

   /* determine if the CTS bit is clear or asserted */
   else {status = (lsr&CtsMask) ? 0 : 1;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable a specified Max3109 on the Apf11's SPI bus         */
/*------------------------------------------------------------------------*/
/**
   This function disables a specified Max3109 on the Apf11's SPI
   bus. The device is deselected and the SPI bus is induced into an
   inactive state.

   \begin{verbatim}
   input:

      dev.....This specifies one of the Max3109 DUARTs on the SPI1 bus
              of the Apf11.  The device id must be one of those
              specified in the SpiDevice enumeration of Stmf103Spi.c.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Disable(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Max3109Disable()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* switch to activate the specified DUART */
   switch (dev)
   {
      /* spare DUART interface */
      case Spi1DevSpDuart: {status=SpDuartPowerDisable(); break;}

      /* RF interface */
      case Spi1DevRfDuart:  

      /* expansion interface */   
      case Spi1DevExpDuart1: case Spi1DevExpDuart2:
      case Spi1DevExpDuart3: case Spi1DevExpDuart4:
      {
         /* make a logentry that the expansion interface is not implemented */
         LogEntry(FuncName,"Unimplemented SPI1 device: %d\n",dev);

         /* reinitialize the return value */
         status=Max3109NotImplemented; break;
      }

      /* invalid SPI1 DUART device */
      default:
      {
         /* make a logentry that the device is not a valid SPI1 DUART */
         LogEntry(FuncName,"Invalid SPI1 DUART device: %d\n",dev);

         /* reinitialize the return value */
         status=Max3109Invalid;
      }
   }

   /* check if the DUART was successfully disabled */
   if (status>0)
   {
      /* deselect the specified DUART */
      if ((err=Spi1Select(Spi1Dev))<=0)
      {
         /* log the failure */
         LogEntry(FuncName,"Attempt to select DUART(%d) failed. "
                  "[err=%d]\n",dev,err); status=Max3109Fail;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable a specified Max3109 on the Apf11's SPI bus          */
/*------------------------------------------------------------------------*/
/**
   This function enables a specified Max3109 on the Apf11's SPI
   bus. The device is selected and the SPI bus is configured for
   operation. 

   \begin{verbatim}
   input:

      dev.....This specifies one of the Max3109 DUARTs on the SPI1 bus
              of the Apf11.  The device id must be one of those
              specified in the SpiDevice enumeration of Stmf103Spi.c.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Enable(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Max3109Enable()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* criteria to determine if the Max3109 is already enabled */
   if (dev==Spi1Dev || dev!=Spi1Select(SpiDevQuery))
   {
      /* switch to activate the specified DUART */
      switch (dev)
      {
         /* spare DUART interface */
         case Spi1DevSpDuart: {status=SpDuartPowerEnable(); break;}

         /* RF interface */
         case Spi1DevRfDuart:
         {
            /* prototype for function with external linkage */
            int RfIsEnable(void);

            /* verify that the RF interface has been powered-up */
            if ((err=RfIsEnable())<=0)
            {
               LogEntry(FuncName,"Can not enable Max3109 due to unpowered "
                        "RF interface. [err=%d]\n",err); status=Max3109Cfg;
            }
            
            break;
         }

         /* expansion interface */   
         case Spi1DevExpDuart1: case Spi1DevExpDuart2:
         case Spi1DevExpDuart3: case Spi1DevExpDuart4:
         {
            /* make a logentry that the expansion interface is not implemented */
            LogEntry(FuncName,"Unimplemented SPI1 device: %d\n",dev);

            /* reinitialize the return value */
            status=Max3109NotImplemented; break;
         }

         /* invalid SPI1 DUART device */
         default:
         {
            /* make a logentry that the device is not a valid SPI1 DUART */
            LogEntry(FuncName,"Invalid SPI1 DUART device: %d\n",dev);

            /* reinitialize the return value */
            status=Max3109Invalid;
         }
      }

      /* test if the device was found to exist */
      if (status>0)
      {
         /* configure the Stm32 for communication with the MAX3109 DUART */
         if ((err=Stm32SpiConfig(SPI1,(WordMode|BaudRate),Cr2,NoCrc))<=0)
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Attempt to configure SPI1 failed. "
                     "[err=%d]\n",err); status=Max3109SpiCfg;
         }
         
         /* select the specified DUART */
         else if ((err=Spi1Select(dev))<=0) {LogEntry(FuncName,"Attempt to select DUART(%d) failed. "
                                                      "[err=%d]\n",dev,err); status=Max3109Fail;}
      }
   }

   return status;
}

/*------------------------------------------------------------------------*/
/* function to flush the FIFOs of the Max3109                             */
/*------------------------------------------------------------------------*/
/**
   This is a function to flush the FIFOs of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Flush(SpiDevice dev, Max3109UartId uart)
{
   /* define the logging signature */
   cc *FuncName="Max3109Flush()";

   /* initialize return value */
   int err[2]={0,0},status=Max3109Ok;
      
   /* flush the FIFOs of the Max3109 */
   if ((err[0]=Max3109RegModify(dev,uart,MODE2,0x02U,0x00U))<=0 && Wait(50)) {status=Max3109Fail;}

   /* revert to normal operation after the flush */
   else if ((err[1]=Max3109RegModify(dev,uart,MODE2,0x00U,0x02U))<=0) {status=Max3109Fail;}

   /* check if the Max3109 FIFO flush failed */
   if (status<=0) {LogEntry(FuncName,"Max3109 IO FIFOs flush failed. "
                            "[err={%d,%d}]\n",err[0],err[1]);}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Max3109:uart's Rx FIFO   */
/*------------------------------------------------------------------------*/
/**
   This function retrieves the number of bytes in the Rx FIFO of a
   specified UART of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109IBytes(SpiDevice dev, Max3109UartId uart)
{
   /* define a local work object */
   unsigned char ibytes;
   
   /* read the number of bytes remaining in the Max3109:uart's Rx FIFO */
   return ((Max3109RegRead(dev,uart,RxFIFOLvl,&ibytes)>0) ? ibytes : Max3109Null);
}

/*------------------------------------------------------------------------*/
/* function to initialize a Max3109 DUART with SPI interface              */
/*------------------------------------------------------------------------*/
/**
   This function initializes a Max3109 DUART using the register
   initializations given in the Max3109RegInit array which is defined
   near the top of this source code file.  These initializations
   configure both UARTs for operation using the following parameters:
   9600 baud, 8-bit words, 1 stop-bin, no parity, no RTS/CTS hardware
   handshaking.  To configure the DUART for operation, it is
   recommended to initialize it with this function and then modify the
   configuration as needed using functions that configure individual
   registers.

   \begin{verbatim}
   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Init(SpiDevice dev, Max3109UartId uart)
{
   /* define the logging signature */
   cc *FuncName="Max3109Init()";

   /* initialize return value */
   int i,err,status=Max3109Ok;
   
   /* enable the specified DUART */
   if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int config; 

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;

      /* loop through the Max3109's registers */
      for (i=IRQEn; i<GlobalIRQ; i++)
      {
         /* define the default mask for each register */
         unsigned char mask=0xffU;

         /* some registers can be programmed using only UART0 */
         Max3109UartId U = (i==CLKSource || i==PLLConfig) ? 0 : uart;
         
         /* bypass registers whose configuration is the sentinel value: 0xff */
         if (Max3109RegInit[i]==0xffU) continue;

         /* some registers need special masks */
         if (i==CLKSource || i==GPIOData) {mask=0x0fU;} 
         
         /* transmit the command to write the configuration to the i(th) register of the max3109 */
         Spi1CsPulse(dev,0); SPI1->DR = (WRITE | (U<<13) | (i<<8) | (Max3109RegInit[i]&mask)); Stm32SpiBusy(SPI1,timeout);
      
         /* clear the Stm32's status flags */ 
         config=SPI1->DR; err=SPI1->SR;
         
         /* transmit the command to read the configuration from the i(th) register of the max3109 */
         Spi1CsPulse(dev,0); SPI1->DR = (READ | (U<<13) | (i<<8)); Stm32SpiRxne(SPI1,timeout);

         /* read the configuration from the data register */
         config=SPI1->DR; err=SPI1->SR;

         /* confirm the new configuration and the UART */
         if ((config&mask)!=(Max3109RegInit[i]&mask))
         {
            /* make a logentry of the configuration failure */
            LogEntry(FuncName,"Configuration failure for register 0x%02x: 0x%02x != 0x%02x "
                     "(expected).\n",i,(config&mask),(Max3109RegInit[i]&mask)); status=Max3109Cfg;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* set the line-control parameters for the specified device and UART      */
/*------------------------------------------------------------------------*/
/**
   This function configures the contents of the line control register
   (LCR) for the specified Max3109 and UART.  The existing contents
   are read and all except for the 2 most significant bits are
   reconfigured; the 2 most significant bits are retained.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

      mode.....This specifies the communications parameters; number of
               data bits, stop bits, and parity generation.  The mode
               must be selected from the Max3109Mode enumeration near
               the top of this file.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109ModeSet(SpiDevice dev, Max3109UartId uart, Max3109Mode mode)
{
   /* define the logging signature */
   cc *FuncName="Max3109ModeSet()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a local work object */
   unsigned char lcr;

   /* enable the specified DUART */
   if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   /* read the LCR register from the specified device and UART */
   else if ((err=Max3109RegRead(dev,uart,LCR,&lcr))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to read LCR register from DUART(dev:%d,uart:%d) "
               "failed. [err=%d]\n",dev,uart,err); status=Max3109Fail;
   }
   
   else
   {
      /* clear all except the 2 most-significant-bits */
      lcr&=0xc0;
      
      /* select the line-control parameters */
      switch (mode)
      {
         /* select 8-bit word, 1 stop-bit, no parity */
         case N81: {lcr|=0x03U; break;}

         /* select 7-bit word, 1 stop-bit, even parity */
         case E71: {lcr|=0x1fU; break;}

         /* select 7-bit word, 1 stop-bit, odd parity */            
         case O71: {lcr|=0x0fU; break;}

         /* trap unimplemented line-control modes */
         default: {LogEntry(FuncName,"Unimplemented line-control mode: %d\n",
                            mode); status=Max3109NotImplemented;}
      }

      /* validate the mode selection */
      if (status>=0)
      {
         /* configure the Max3109's line control register */
         if ((err=Max3109RegWrite(dev,uart,LCR,lcr))<=0)
         {
            /* log the failure */
            LogEntry(FuncName,"Mode configuration(%d) for DUART(dev:%d,uart:%d) "
                     "failed. [err=%d]\n",mode,dev,uart,err); status=Max3109Cfg;
         }
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the Max3109:uart's Tx FIFO   */
/*------------------------------------------------------------------------*/
/**
   This function retrieves the number of bytes in the Tx FIFO of a
   specified UART of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109OBytes(SpiDevice dev, Max3109UartId uart)
{
   /* define a local work object */
   unsigned char obytes;
   
   /* read the number of bytes remaining in the Max3109:uart's Tx FIFO */
   return ((Max3109RegRead(dev,uart,TxFIFOLvl,&obytes)>0) ? obytes : Max3109Null);
}

/*------------------------------------------------------------------------*/
/* function to write a single byte to the Max3109 transmit-hold register  */
/*------------------------------------------------------------------------*/
/**
  This function writes a single byte to the Max3109's transmit-hold
  register (THR).  Before writing the byte, this function pauses until
  the Tx fifo is below a high-water mark to ensure that the fifo does
  not overflow.  If the Tx fifo contains fewer bytes than the
  high-water mark, this function immediately adds the new byte to the
  fifo and then immediately returns to the calling function.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

      byte.....This is the byte to add to the Tx fifo.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Putb(SpiDevice dev, Max3109UartId uart, unsigned char byte)
{
   /* define the logging signature */
   cc *FuncName="Max3109Putb()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* enable the specified DUART */
   if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define a reference time to implement a timeout feature */
      const unsigned long int To=HAL_GetTick();
      
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;

      /* loop to wait until the Tx queue has sufficient reserve capacity */
      for (status=Max3109TimeOut; (HAL_GetTick()-To)<10*timeout;)
      {
         /* clear the Stm32's status flags */ 
         unsigned short int TxFifoLvl=SPI1->DR; err=SPI1->SR;
              
         /* transmit the command to read the TxFIFOLvl register of the max3109 */
         Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (TxFIFOLvl<<8)); Stm32SpiRxne(SPI1,timeout);

         /* compute the number of bytes in the transmit-hold fifo */
         TxFifoLvl=((SPI1->DR)&0xffU); err=SPI1->SR;

         /* break from the wait-loop if the Tx queue has sufficient reserve capacity */
         if (TxFifoLvl<120) {status=Max3109Ok; break;}
      }

      /* check if the Tx queue has space to receive the byte */
      if (status>=0)
      {
         /* write the byte to the transmit-hold register (THR) of the max3109 */
         Spi1CsPulse(dev,0); SPI1->DR = (WRITE | (uart<<13) | (THR<<8) | byte);

         /* wait until the SPI bus not busy and clear the Stm32's status flags  */
         Stm32SpiBusy(SPI1,timeout); err=SPI1->DR; err=SPI1->SR;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to modify the contents of a specified Max3109 register        */
/*------------------------------------------------------------------------*/
/**
   This function modifies the contents of a specified register of one
   of the Max3109's UARTs.  The existing contents are read, then
   specified bits are asserted while other specified bits are cleared,
   and then the modified value is written back to the register.  After
   writing to the register, the same register is read back to confirm
   that it contains the specified value.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

      reg......This specifies which of the Max3109's registers is to be
               configured.  The register specifier must be in the range
               [0x01,0x1e] and must be writable.

      clear....This specifies the bits that are to be cleared when
               written back to the register.

      assert...This specifies the bits that are to be asserted when
               written Max3109's register.  If a conflict exists
               between the bits to be cleared and those to be asserted
               then resolution is in favor of assertion.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RegModify(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg,
                     unsigned char assert, unsigned char clear)
{
   /* define the logging signature */
   cc *FuncName="Max3109RegModify()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* validate the register as configurable */
   if (reg<IRQEn || reg>CLKSource || Max3109RegInit[reg]==0xffU)
   {
      /* log the failure */
      LogEntry(FuncName,"Invalid or nonconfigurable register: "
               "0x%02x\n",reg); status=Max3109Null;
   }
   
   /* enable the specified DUART */
   else if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int config,new;
                   
      /* define the default mask for each register */
      unsigned char mask=0xffU;

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;

      /* some registers can be programmed using only UART0 */
      if (reg==CLKSource || reg==PLLConfig) {uart=0;}

      /* some registers need special masks */
      if (reg==CLKSource || reg==GPIOData) {mask=0x0fU;}

      /* clear the Stm32's status flags */ 
      config=SPI1->DR; err=SPI1->SR;
 
      /* transmit the command to read the configuration from the specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (reg<<8)); Stm32SpiRxne(SPI1,timeout);
      
      /* read the configuration from the data register */
      config=SPI1->DR; err=SPI1->SR;

      /* compute the new configuration */
      new = (config&mask); new &= ((~clear)&mask); new |= (assert&mask);

      /* transmit the command to write the configuration to the specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (WRITE | (uart<<13) | (reg<<8) | new); Stm32SpiBusy(SPI1,timeout);
      
      /* clear the Stm32's status flags */ 
      config=SPI1->DR; err=SPI1->SR;
              
      /* transmit the command to read the configuration from the specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (reg<<8)); Stm32SpiRxne(SPI1,timeout);

      /* read the configuration from the data register */
      config=SPI1->DR; err=SPI1->SR;

      /* confirm the new configuration and the UART */
      if ((config&(mask))!=(new&mask))
      {
         /* make a logentry of the configuration failure */
         LogEntry(FuncName,"Configuration failure for register(0x%02x): 0x%02x != 0x%02x "
                  "(expected).\n",reg,(config&0xffU),(new&mask)); status=Max3109Cfg;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the contents of a specified Max3109 register          */
/*------------------------------------------------------------------------*/
/**
   This function reads the contents of a specified register of
   one of the Max3109's UARTs.  

   \begin{verbatim}
   input:

      dev.....This specifies one of the Max3109 DUARTs on the SPI1 bus
              of the Apf11.  The device id must be one of those
              specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart....This specifies which of the two UARTs is to receive the
              register configuration.  If this value is zero then
              UART0 is configured, else UART1 is configured.

      reg.....This specifies which of the Max3109's registers is to be
              configured.  The register specifier must be in the range
              [0x01,0x1e] and must be writable.

      value...The contents of the specified Max3109's register is
              written into this object.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RegRead(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg, unsigned char *value)
{
   /* define the logging signature */
   cc *FuncName="Max3109RegRead()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* initialize the return value of function parameter */
   if (value) {(*value)=0xff;}
   
   /* validate the register as readable */
   if (!value || reg<RHR || reg>GlobalIRQ)
   {
      /* log the failure */
      LogEntry(FuncName,"Invalid register(0x%02x) or function "
               "argument(0x%04x).\n",reg,value); status=Max3109Null;
   }

   /* enable the specified DUART */
   else if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int config;
      
      /* define the default mask for each register */
      unsigned char mask=0xffU;

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;

      /* some registers can be programmed using only UART0 */
      if (reg==CLKSource || reg==PLLConfig) {uart=0;}

      /* some registers need special masks */
      if (reg==InitCLKSource) {mask=0x0fU;}

      /* clear the Stm32's status flags */ 
      config=SPI1->DR; err=SPI1->SR;
              
      /* transmit the command to read a specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (reg<<8)); Stm32SpiRxne(SPI1,timeout);

      /* read the configuration from the data register */
      config=SPI1->DR; err=SPI1->SR; (*value)=(config&mask);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read configuration registers from a Max3109 DUART          */
/*------------------------------------------------------------------------*/
/**
   This function reads the configuration registers from a Max3109
   DUART and writes a report of their values to a user specified
   output stream.

   \begin{verbatim}
   input:

      dev....This specifies one of the Max3109 DUARTs on the SPI1
             bus of the Apf11.  The device id must be one of those
             specified in the SpiDevice enumeration of Stmf103Spi.c.

   output:

      dest...The output stream to which the report will be written.

      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Regs(SpiDevice dev, FILE *dest)
{
   /* define the logging signature */
   cc *FuncName="Max3109Regs()";

   /* initialize return value */
   int status=Max3109Null;

   /* define some local work objects */
   unsigned char byte[2]; int n,err[2];

   /* define the names of the Max3109 registers */
   const char *name[]={"RHR/THR", "IRQEn", "ISR", "LSRIntEn", "LSR", "SpclChrIntEn",
                       "SpclCharInt", "STSIntEn", "STSInt", "MODE1", "MODE2", "LCR",
                       "RxTimeOut", "HDplxDelay", "IrDA", "FlowLvl", "FIFOTrLvl",
                       "TxFIFOLvl", "RxFIFOLvl", "FlowCtrl", "XON1", "XON2", "XOFF1",
                       "XOFF2", "GPIOConfig", "GPIOData", "PLLConfig", "BRGConfig",
                       "DIVLSB", "DIVMSB", "CLKSource"};

   /* write headers to the output stream */
   fprintf(dest,"Registers [0x%02x,0x%02x] for Max3109 device(%d).\n",IRQEn,CLKSource,dev);
   fprintf(dest,"%6s %13s   %4s %4s\n","Offset","Register","Uart0","Uart1");
   fprintf(dest,"----------------------------------\n");

   /* loop over each register in the range [IRQEn,CLKSource] */
   for (n=IRQEn; n<=CLKSource; n++)
   {
      /* ignore registers that are not configuration registers (ie., read-only) */
      if (Max3109RegInit[n]!=0xffU)
      {
         /* read the current register from each UART */
         if ((err[0]=Max3109RegRead(dev,UART0,n,&byte[0]))>0 &&
             (err[1]=Max3109RegRead(dev,UART1,n,&byte[1]))>0)
         {
            /* append current register values to the report */
            fprintf(dest,"  0x%02x %13s :  0x%02x  0x%02x\n",n,name[n],byte[0],byte[1]);
         }

         /* log the read failure */
         else {LogEntry(FuncName,"Failed attempt to read register 0x%02x from Max3109 "
                        "device(%d). err=[%d,%d]\n",n,dev,err[0],err[1]);}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a specified value to a specified Max3109 register    */
/*------------------------------------------------------------------------*/
/**
   This function writes a specified value to a specified register of
   one of the Max3109's UARTs.  After writing to the register, the
   same register is read back to confirm that it contains the
   specified value.

   \begin{verbatim}
   input:

      dev.....This specifies one of the Max3109 DUARTs on the SPI1 bus
              of the Apf11.  The device id must be one of those
              specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart....This specifies which of the two UARTs is to receive the
              register configuration.  If this value is zero then
              UART0 is configured, else UART1 is configured.

      reg.....This specifies which of the Max3109's registers is to be
              configured.  The register specifier must be in the range
              [0x01,0x1e] and must be writable.

      value...This specifies the value that is to be written into the
              Max3109's register.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RegWrite(SpiDevice dev, Max3109UartId uart, Max3109RegMap reg, unsigned char value)
{
   /* define the logging signature */
   cc *FuncName="Max3109RegWrite()";

   /* initialize return value */
   int err,status=Max3109Ok;
   
   /* validate the register as configurable */
   if (reg<IRQEn || reg>CLKSource || Max3109RegInit[reg]==0xffU)
   {
      /* log the failure */
      LogEntry(FuncName,"Invalid or nonconfigurable register: "
               "0x%02x\n",reg); status=Max3109Null;
   }

   /* enable the specified DUART */
   else if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int config;

      /* define the default mask for each register */
      unsigned char mask=0xffU;

      /* ensure that the UART is specified as either zero or one */
      uart=(uart)?1:0;

      /* some registers can be programmed using only UART0 */
      if (reg==CLKSource || reg==PLLConfig) {uart=0;}

      /* some registers need special masks */
      if (reg==CLKSource || reg==GPIOData) {mask=0x0fU;}
      
      /* transmit the command to write the configuration to a specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (WRITE | (uart<<13) | (reg<<8) | value); Stm32SpiBusy(SPI1,timeout);
      
      /* clear the Stm32's status flags */ 
      config=SPI1->DR; err=SPI1->SR;
              
      /* transmit the command to read a specified register of the max3109 */
      Spi1CsPulse(dev,0); SPI1->DR = (READ | (uart<<13) | (reg<<8)); Stm32SpiRxne(SPI1,timeout);

      /* read the configuration from the data register */
      config=SPI1->DR; err=SPI1->SR;

      /* confirm the new configuration and the UART */
      if ((config&mask)!=(value&mask))
      {
         /* make a logentry of the configuration failure */
         LogEntry(FuncName,"Configuration failure for register(0x%02x): 0x%02x != 0x%02x "
                  "(expected).\n",reg,(config&0xffU),(value&mask)); status=Max3109Cfg;
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to reset the Max3109                                          */
/*------------------------------------------------------------------------*/
/**
   This is a function to reset and initialize the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109Reset(SpiDevice dev, Max3109UartId uart)
{
   /* define the logging signature */
   cc *FuncName="Max3109Reset()";

   /* initialize return value */
   int err[3],status=Max3109Ok;
      
   /* reset the Max3109 */
   if ((err[0]=Max3109RegModify(dev,uart,MODE2,0x01U,0x00U))<=0 && Wait(50)) {status=Max3109Fail;}

   /* revert to normal operation after the reset */
   else if ((err[1]=Max3109RegModify(dev,uart,MODE2,0x00U,0x01U))<=0) {status=Max3109Fail;}

   /* reinitialize the Max3109 */
   else if ((err[2]=Max3109Init(dev,uart))<=0) {status=Max3109Fail;}

   /* check if the Max3109 reset failed */
   if (status<=0) {LogEntry(FuncName,"Max3109 reset & initialization failed. "
                            "[err={%d,%d,%d}]\n",err[0],err[1],err[2]);}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read the revision id from the Max3109                      */
/*------------------------------------------------------------------------*/
/**
   This function reads the hardware revision id from the Max3109. The
   revision id is a known reference that started with 0xC0 but is
   currently up to 0xC2. 

   \begin{verbatim}
   output:
      If successful, this function returns a positive value that is
      the hardware revision id of the Max3109.  Zero is retured on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RevId(SpiDevice dev)
{
   /* define the logging signature */
   cc *FuncName="Max3109RevId()";

   /* initialize return value */
   int err,status=Max3109Ok;

   /* enable the specified DUART */
   if ((err=Max3109Enable(dev))<=0)
   {
      /* log the failure */
      LogEntry(FuncName,"Attempt to enable SPI1 device(%d) failed. "
               "[err=%d]\n",dev,err); status=Max3109Fail;
   }

   else
   {
      /* define the timeout period for SPI activity */
      const unsigned char timeout=10;

      /* define local work object */
      volatile unsigned short int RevId;

      /* define the commands to induce the max3109 into and out-of extended mode */
      const unsigned char EXTMODEON=0xceU, EXTMODEOFF=0xcdU;
      
      /* transmit the command to induce the max3109 into extended mode */
      Spi1CsPulse(dev,0); SPI1->DR = (WRITE | (GlobalCmnd<<8) | EXTMODEON); Stm32SpiBusy(SPI1,timeout);
      
      /* clear the Stm32's status flags */ 
      RevId=SPI1->DR; err=SPI1->SR;
      
      /* transmit the command to induce the max3109 into extended mode */
      Spi1CsPulse(dev,0);  SPI1->DR = (READ | ((RevID&0x0fU)<<8)); Stm32SpiRxne(SPI1,timeout);

      /* read the revision id */
      RevId=SPI1->DR; err=SPI1->SR; status=(RevId&0xffU);
      
      /* transmit the command to induce the max3109 into extended mode */
      Spi1CsPulse(dev,0);  SPI1->DR = (WRITE | (GlobalCmnd<<8) | EXTMODEOFF); Stm32SpiBusy(SPI1,timeout);
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert the RTS signal of a specified UART                  */
/*------------------------------------------------------------------------*/
/**
   This function asserts the RTS signal of a specified Max3109 device
   and UART.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsAssert(SpiDevice dev, Max3109UartId uart)
{
   /* initialize return value */
   int status=Max3109RegModify(dev,uart,LCR,RtsMask,0x00);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to clear the RTS signal of a specified UART                   */
/*------------------------------------------------------------------------*/
/**
   This function clears the RTS signal of a specified Max3109 device
   and UART.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsClear(SpiDevice dev, Max3109UartId uart)
{
   /* initialize return value */
   int status=Max3109RegModify(dev,uart,LCR,0x00,RtsMask);
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable RTS/CTS hardware handshaking                       */
/*------------------------------------------------------------------------*/
/**
   This function disables RTS/CTS hardware handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsCtsHandshakeOff(SpiDevice dev, Max3109UartId uart)
{
   /* define the logging signature */
   cc *FuncName="Max3109RtsCtsHandshakeOff()";

   /* initialize return value */
   int err[2],status=Max3109Ok;

   /* enable RTS handshaking */
   if ((err[0]=Max3109RtsHandshakeSet(dev,uart))<=0) {status=Max3109Fail;}

   /* enable CTS handshaking */
   else if ((err[1]=Max3109CtsHandshakeSet(dev,uart))<=0) {status=Max3109Fail;}

   /* check if RTS/CTS handshaking was successfully enabled */
   if (status<=0) {LogEntry(FuncName,"Attempt to disable RTS/CTS handshaking "
                            "failed. [RtsErr=%d,CtsErr=%d]\n",err[0],err[1]);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to enable RTS/CTS hardware handshaking                        */
/*------------------------------------------------------------------------*/
/**
   This function enables RTS/CTS hardware handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsCtsHandshakeOn(SpiDevice dev, Max3109UartId uart)
{
   /* define the logging signature */
   cc *FuncName="Max3109RtsCtsHandshakeOn()";

   /* initialize return value */
   int err[2],status=Max3109Ok;

   /* enable RTS handshaking */
   if ((err[0]=Max3109RtsHandshakeSet(dev,uart))<=0) {status=Max3109Fail;}

   /* enable CTS handshaking */
   else if ((err[1]=Max3109CtsHandshakeSet(dev,uart))<=0) {status=Max3109Fail;}

   /* check if RTS/CTS handshaking was successfully enabled */
   if (status<=0) {LogEntry(FuncName,"Attempt to enable RTS/CTS handshaking "
                            "failed. [RtsErr=%d,CtsErr=%d]\n",err[0],err[1]);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to disable hardware RTS handshaking                           */
/*------------------------------------------------------------------------*/
/**
   This function disables hardware RTS handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsHandshakeClear(SpiDevice dev, Max3109UartId uart)
{
   /* clear the 0x01 bit of the FlowCtrl register to disable auto-RTS generation */
   return Max3109RegModify(dev,uart,FlowCtrl,0x00U,0x01U);
}

/*------------------------------------------------------------------------*/
/* function to enable hardware RTS handshaking                            */
/*------------------------------------------------------------------------*/
/**
   This function enables hardware RTS handshaking.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value on success or zero on
      failure.  A negative return value indicates that an exception
      was encountered.
   \end{verbatim}
*/
int Max3109RtsHandshakeSet(SpiDevice dev, Max3109UartId uart)
{
   /* assert the 0x01 bit of the FlowCtrl register to enable auto-RTS generation */
   return Max3109RegModify(dev,uart,FlowCtrl,0x01U,0x00U);
}

/*------------------------------------------------------------------------*/
/* function to read the state of the RTS pin from a Max3109 UART          */
/*------------------------------------------------------------------------*/
/**
   This function reads the state of the RTS pin from a specified UART
   of the Max3109.

   \begin{verbatim}
   input:

      dev......This specifies one of the Max3109 DUARTs on the SPI1
               bus of the Apf11.  The device id must be one of those
               specified in the SpiDevice enumeration of Stmf103Spi.c.

      uart.....This specifies which of the two UARTs is to receive the
               register configuration.  If this value is zero then
               UART0 is configured, else UART1 is configured.

   output:
      This function returns a positive value if the RTS pin is asserted
      or zero if clear.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Max3109RtsIsAssert(SpiDevice dev, Max3109UartId uart)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* define a local work object */
   unsigned char lcr;

   /* read the line status register */
   if ((err=Max3109RegRead(dev,uart,LCR,&lcr))<=0) {status=err;}

   /* determine if the RTS bit is clear or asserted */
   else {status = (lcr&RtsMask) ? 1 : 0;}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to execute a Tx-break signal for a specified period           */
/*------------------------------------------------------------------------*/
/**
   This function executes a Tx-break signal for a specified period of time.

   \begin{verbatim}
   input:

      dev........This specifies one of the Max3109 DUARTs on the SPI1
                 bus of the Apf11.  The device id must be one of those
                 specified in the SpiDevice enumeration of
                 Stmf103Spi.c.

      uart.......This specifies which of the two UARTs is to receive
                 the register configuration.  If this value is zero
                 then UART0 is configured, else UART1 is configured.

      millisec...This specifies the period of time (millisec) that the
                 break signal is maintained.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Max3109TxBreak(SpiDevice dev, Max3109UartId uart, unsigned short int millisec)
{
   /* initialize return value */
   int err,status=Max3109Ok;

   /* assert the break condition on the Tx pin of the specified UART */
   if ((err=Max3109TxBreakAssert(dev,uart))<=0) {status=err;}

   /* pause for the user specified period of time */
   Wait(millisec);
   
   /* clear the break condition on the Tx pin of the specified UART */
   if ((err=Max3109TxBreakClear(dev,uart))<=0) {status=err;}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to assert the Tx-break bit of the 3109's LCR register         */
/*------------------------------------------------------------------------*/
/**
   This function asserts the break signal on the Tx pin of the Max3109.

   \begin{verbatim}
   input:

      dev........This specifies one of the Max3109 DUARTs on the SPI1
                 bus of the Apf11.  The device id must be one of those
                 specified in the SpiDevice enumeration of
                 Stmf103Spi.c.

      uart.......This specifies which of the two UARTs is to receive
                 the register configuration.  If this value is zero
                 then UART0 is configured, else UART1 is configured.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}

*/
int Max3109TxBreakAssert(SpiDevice dev, Max3109UartId uart)
{
   /* assert the Tx-break bit of the 3109's LCR register */
   return Max3109RegModify(dev,uart,LCR,0x40U,0x00U);
}

/*------------------------------------------------------------------------*/
/* function to clear the Tx-break bit of the 3109's LCR register          */
/*------------------------------------------------------------------------*/
/**
   This function clears the break signal on the Tx pin of the Max3109.

   \begin{verbatim}
   input:

      dev........This specifies one of the Max3109 DUARTs on the SPI1
                 bus of the Apf11.  The device id must be one of those
                 specified in the SpiDevice enumeration of
                 Stmf103Spi.c.

      uart.......This specifies which of the two UARTs is to receive
                 the register configuration.  If this value is zero
                 then UART0 is configured, else UART1 is configured.

   output:
      If successful, this function returns a positive value or else
      zero, on failure.  A negative return value indicates that an
      exception was encountered.
   \end{verbatim}
*/
int Max3109TxBreakClear(SpiDevice dev, Max3109UartId uart)
{
   /* clear the Tx-break bit of the 3109's LCR register */
   return Max3109RegModify(dev,uart,LCR,0x00U,0x40U);
}

#endif /* MAX3109_C */

