Note this is actually the compression library 'zlib' (https://zlib.net) but the 'zlib' directory was already used for zmodem.  Note that gzip uses zlib, but they aren't the same thing, and we only use 'gzlib' to differentiate from 'zlib' (for zmodem) which was already in use.

The initial zlib checkin uses the 1.2.11 version of the library.

For testing, to do a basic file deflate, compiling with the c compiler (clang) worked with the following files:

adler32.c
crc32.c
crc32.h
deflate.c
deflate.h
gzguts.h
readme.txt
trees.c
trees.h
zconf.h
zipfile.c
zlib.h
zutil.c
zutil.h

The tests are only deflating, as that's all we need to do, so some files weren't needed.  The files left out were these:

compress.c
gzclose.c
gzlib.c
gzread.c
gzwrite.c
infback.c
inffast.c
inffast.h
inffixed.h
inflate.c
inflate.h
inftrees.c
inftrees.h
uncompr.c


But when compiling these with the arm compiler (arm-none-eabi-gcc) it gave several errors (undefined referece to '_xxx'), but it was because a compiler flag was needed:

    --specs=nosys.specs

This flag was already part of the main SwiftWare compiler options.
